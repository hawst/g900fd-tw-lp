.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread;
.super Ljava/lang/Object;
.source "SnsFbParserThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread$FacebookThread;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 71
    const/4 v5, 0x0

    .line 74
    .local v5, "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    :try_start_0
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;-><init>()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 75
    .end local v5    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    .local v6, "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 77
    .local v2, "ja":Lorg/json/JSONArray;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v1, v7, :cond_2

    .line 79
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "name"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 80
    .local v3, "queryName":Ljava/lang/String;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v8, "fql_result_set"

    invoke-virtual {v7, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "queryObject":Ljava/lang/String;
    const-string v7, "thread_query"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 85
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread;->parseMsg(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;->mMsg:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    .line 77
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_1
    const-string v7, "user_query"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 89
    invoke-static {v4}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserThread;->parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;->mUser:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 93
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    .end local v3    # "queryName":Ljava/lang/String;
    .end local v4    # "queryObject":Ljava/lang/String;
    :catch_0
    move-exception v0

    move-object v5, v6

    .line 95
    .end local v6    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v5    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    :goto_2
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 98
    .end local v0    # "e":Lorg/json/JSONException;
    :goto_3
    return-object v5

    .end local v5    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    .restart local v1    # "i":I
    .restart local v2    # "ja":Lorg/json/JSONArray;
    .restart local v6    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    :cond_2
    move-object v5, v6

    .line 96
    .end local v6    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    .restart local v5    # "thread":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseThread;
    goto :goto_3

    .line 93
    .end local v1    # "i":I
    .end local v2    # "ja":Lorg/json/JSONArray;
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method private static parseMsg(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    .locals 10
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 102
    const/4 v5, 0x0

    .line 103
    .local v5, "msg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    const/4 v0, 0x0

    .line 106
    .local v0, "curMsg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 108
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 109
    new-instance v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    invoke-direct {v6}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;-><init>()V

    .line 111
    .local v6, "newMsg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 113
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "message_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mMessageId:Ljava/lang/String;

    .line 115
    const-string v7, "thread_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mThreadId:Ljava/lang/String;

    .line 116
    const-string v7, "author_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mAuthorId:Ljava/lang/String;

    .line 117
    const-string v7, "body"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mBody:Ljava/lang/String;

    .line 118
    const-string v7, "created_time"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mCreatedTime:Ljava/lang/Long;

    .line 120
    const-string v7, "viewer_id"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mViwerId:Ljava/lang/String;

    .line 122
    if-nez v5, :cond_0

    .line 123
    move-object v5, v6

    .line 124
    move-object v0, v5

    .line 108
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 126
    :cond_0
    iput-object v6, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    .line 127
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 130
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v6    # "newMsg":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 135
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v5
.end method

.method private static parseUser(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    .locals 8
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 139
    const/4 v6, 0x0

    .line 140
    .local v6, "user":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    const/4 v0, 0x0

    .line 143
    .local v0, "curUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 145
    .local v3, "ja":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 146
    new-instance v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    invoke-direct {v5}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;-><init>()V

    .line 148
    .local v5, "newUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 150
    .local v4, "jsonObj":Lorg/json/JSONObject;
    const-string v7, "uid"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mUserId:Ljava/lang/String;

    .line 151
    const-string v7, "name"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mName:Ljava/lang/String;

    .line 152
    const-string v7, "pic"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mPicSquare:Ljava/lang/String;

    .line 154
    if-nez v6, :cond_0

    .line 155
    move-object v6, v5

    .line 156
    move-object v0, v6

    .line 145
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 158
    :cond_0
    iput-object v5, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;

    .line 159
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 162
    .end local v2    # "i":I
    .end local v3    # "ja":Lorg/json/JSONArray;
    .end local v4    # "jsonObj":Lorg/json/JSONObject;
    .end local v5    # "newUser":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseUserQueryResult;
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    .line 167
    .end local v1    # "e":Lorg/json/JSONException;
    :cond_1
    return-object v6
.end method
