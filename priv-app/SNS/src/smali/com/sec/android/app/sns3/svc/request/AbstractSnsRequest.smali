.class public abstract Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;
.super Ljava/lang/Object;
.source "AbstractSnsRequest.java"


# static fields
.field public static final INVALID_ID:I = -0x1

.field private static final REQUEST_RETRY_MAX:I = 0x2

.field private static final REQUEST_TIMEOUT_MEDIA_DURATION:J = 0x927c0L

.field private static final REQUEST_TIMEOUT_NOTMAL_DURATION:J = 0x9c40L

.field private static mReqID_Base:I


# instance fields
.field protected mCategory:I

.field protected mReqID:I

.field private mRequestTimer:Ljava/util/Timer;

.field private mRetryCnt:I

.field private mSendingThread:Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

.field protected mSpType:Ljava/lang/String;

.field protected mState:I

.field protected mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID_Base:I

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;I)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "spType"    # Ljava/lang/String;
    .param p3, "reqCategory"    # I

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mState:I

    .line 68
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    .line 75
    monitor-enter p0

    .line 76
    :try_start_0
    sget v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID_Base:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID_Base:I

    .line 77
    sget v0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID_Base:I

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    .line 78
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    iput-object p2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    .line 81
    iput p3, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    .line 83
    iput-object p1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 84
    return-void

    .line 78
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public abort()Z
    .locals 1

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->cancelRequestTimer()V

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSendingThread:Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSendingThread:Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->abort()V

    .line 257
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public cancelRequestTimer()V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRequestTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelRequestTimer() : reqID = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] Timer CANCELED!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRequestTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 304
    :cond_0
    return-void
.end method

.method protected abstract check(IILcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)I
.end method

.method protected abstract compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
.end method

.method public decreaseRetryCnt()V
    .locals 3

    .prologue
    .line 317
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    .line 319
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    if-gez v0, :cond_0

    .line 320
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    .line 322
    :cond_0
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********************* decreaseRetryCnt : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *********************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    return-void
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    return v0
.end method

.method public getReqID()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    return v0
.end method

.method public getSpType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    return-object v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mState:I

    return v0
.end method

.method public increaseRetryCnt()V
    .locals 3

    .prologue
    .line 327
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    .line 329
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********************* increaseRetryCnt : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *********************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    return-void
.end method

.method public isMaxRetry()Z
    .locals 3

    .prologue
    .line 307
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********************* isMaxRetry : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " *********************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    iget v0, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRetryCnt:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 311
    const/4 v0, 0x1

    .line 313
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
.end method

.method public printResponseLog(IILjava/lang/String;)V
    .locals 8
    .param p1, "httpStatus"    # I
    .param p2, "errorCode"    # I
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    .line 335
    const/16 v0, 0x190

    .line 336
    .local v0, "MAX_ONE_lINE_BUFFER":I
    const/16 v1, 0x7530

    .line 338
    .local v1, "MAX_TOTAL_BUFFER":I
    const-string v5, "SNS"

    const-string v6, "##########################################################################################"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    const-string v5, "SNS"

    const-string v6, "<< RESPONSE >>"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    const-string v5, "SNS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "reqID = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const-string v5, "SNS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HTTP status = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    if-lez p2, :cond_0

    .line 344
    const-string v5, "SNS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error Code = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    move-object v4, p3

    .line 347
    .local v4, "response":Ljava/lang/String;
    const/4 v3, 0x0

    .line 348
    .local v3, "offset":I
    const/4 v2, 0x0

    .line 350
    .local v2, "length":I
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 351
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 352
    const-string v5, "SNS"

    const-string v6, "content = "

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    :goto_0
    const/16 v5, 0x7530

    if-gt v3, v5, :cond_2

    .line 354
    add-int/lit16 v5, v3, 0x190

    if-ge v5, v2, :cond_1

    .line 355
    const-string v5, "SNS"

    add-int/lit16 v6, v3, 0x190

    invoke-virtual {v4, v3, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    add-int/lit16 v3, v3, 0x190

    goto :goto_0

    .line 359
    :cond_1
    const-string v5, "SNS"

    invoke-virtual {v4, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_2
    :goto_1
    const-string v5, "SNS"

    const-string v6, "##########################################################################################"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    return-void

    .line 364
    :cond_3
    const-string v5, "SNS"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "content length = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public process(Landroid/os/Handler;)Z
    .locals 9
    .param p1, "reqMgrHandler"    # Landroid/os/Handler;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 128
    iget v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    const/4 v3, -0x1

    if-ne v1, v3, :cond_0

    .line 129
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "reqID is invalid or callback is not registered."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v8

    .line 133
    .local v8, "tokenMgr":Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    invoke-virtual {v8, v1}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getTokenState(Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    const/16 v3, 0x14

    if-eq v1, v3, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    const/16 v3, 0x18

    if-eq v1, v3, :cond_1

    .line 136
    const-string v1, "SNS"

    const-string v3, "process return false due to invalid token state"

    invoke-static {v1, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->cancelRequestTimer()V

    .line 140
    new-instance v0, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    const/16 v4, 0x7d9

    move v3, v2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;-><init>(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;)V

    .line 143
    .local v0, "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    const/16 v1, 0x15

    invoke-virtual {p1, v1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    .line 144
    .local v7, "msg":Landroid/os/Message;
    invoke-virtual {p1, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 216
    .end local v0    # "result":Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;
    .end local v7    # "msg":Landroid/os/Message;
    :goto_0
    return v2

    .line 149
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;

    invoke-direct {v2, p0, p1}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$2;-><init>(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;Landroid/os/Handler;)V

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->send(Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;)V

    .line 216
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public request()Z
    .locals 9

    .prologue
    const/16 v8, 0x14

    const/4 v4, 0x1

    const/4 v7, -0x1

    .line 89
    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    if-eq v5, v7, :cond_0

    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    if-eq v5, v8, :cond_0

    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    const/16 v6, 0x18

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getTokenState(Ljava/lang/String;)I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    :cond_0
    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    if-ne v5, v7, :cond_2

    .line 95
    :cond_1
    const-string v4, "SNS"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AbstractSnsRequest : request() FAIL!!! : mSpType = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSpType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], mCategory = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const/4 v4, 0x0

    .line 123
    :goto_0
    return v4

    .line 100
    :cond_2
    const-wide/32 v2, 0x9c40

    .line 101
    .local v2, "timeout":J
    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mCategory:I

    const/16 v6, 0x17

    if-ne v5, v6, :cond_3

    .line 102
    const-wide/32 v2, 0x927c0

    .line 104
    :cond_3
    new-instance v5, Ljava/util/Timer;

    invoke-direct {v5}, Ljava/util/Timer;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRequestTimer:Ljava/util/Timer;

    .line 105
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mRequestTimer:Ljava/util/Timer;

    new-instance v6, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest$1;-><init>(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    invoke-virtual {v5, v6, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 118
    invoke-virtual {p0, v4}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 120
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgrHandler()Landroid/os/Handler;

    move-result-object v1

    .line 121
    .local v1, "reqMgrHandler":Landroid/os/Handler;
    invoke-virtual {v1, v8, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 122
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected abstract respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
.end method

.method protected abstract restoreToken()V
.end method

.method public retryRequest()V
    .locals 5

    .prologue
    .line 261
    const-string v2, "SNS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Retry Request : reqID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->setState(I)V

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgrHandler()Landroid/os/Handler;

    move-result-object v1

    .line 266
    .local v1, "reqMgrHandler":Landroid/os/Handler;
    const/16 v2, 0x14

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 267
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 268
    return-void
.end method

.method public send(Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;)V
    .locals 7
    .param p1, "httpRequest"    # Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .param p2, "callback"    # Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;

    .prologue
    .line 220
    if-nez p1, :cond_0

    .line 221
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getRequestMgrHandler()Landroid/os/Handler;

    move-result-object v3

    .line 222
    .local v3, "reqMgrHandler":Landroid/os/Handler;
    const/16 v5, 0x17

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 223
    .local v2, "msg":Landroid/os/Message;
    iget v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    iput v5, v2, Landroid/os/Message;->arg1:I

    .line 224
    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 241
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "reqMgrHandler":Landroid/os/Handler;
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 228
    const-string v5, "SNS"

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getHttpMgr()Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;

    move-result-object v1

    .line 231
    .local v1, "httpMgr":Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;->getURI()Ljava/net/URI;

    move-result-object v4

    .line 233
    .local v4, "uri":Ljava/net/URI;
    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    .line 234
    invoke-virtual {v4}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpMgr;->getHttpClient(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;

    move-result-object v0

    .line 236
    .local v0, "httpClient":Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
    new-instance v5, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

    invoke-direct {v5, v0, p1, p2}, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;-><init>(Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;Lcom/sec/android/app/sns3/svc/http/ISnsHttpCallback;)V

    iput-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSendingThread:Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

    .line 237
    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mSendingThread:Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpThread;->start()V

    goto :goto_0

    .line 239
    .end local v0    # "httpClient":Lcom/sec/android/app/sns3/svc/http/SnsHttpClient;
    :cond_2
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "httpMgr or uri is null."

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method public setReqID(I)V
    .locals 0
    .param p1, "reqID"    # I

    .prologue
    .line 277
    iput p1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    .line 278
    return-void
.end method

.method public setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 285
    iput p1, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mState:I

    .line 286
    const-string v0, "SNS"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request state is changed. : reqID = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mReqID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;->mState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return-void
.end method
