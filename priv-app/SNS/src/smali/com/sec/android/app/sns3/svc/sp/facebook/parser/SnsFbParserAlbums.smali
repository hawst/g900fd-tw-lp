.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbums;
.super Ljava/lang/Object;
.source "SnsFbParserAlbums.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbums$FacebookAlbums;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;-><init>()V

    .line 41
    .local v0, "albumList":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;
    const/4 v1, 0x0

    .line 42
    .local v1, "albums":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    const/4 v2, 0x0

    .line 46
    .local v2, "curAlbums":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 48
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 50
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 52
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 53
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserAlbum;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    move-result-object v7

    .line 55
    .local v7, "newAlbum":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    if-nez v1, :cond_0

    .line 56
    move-object v1, v7

    .line 57
    move-object v2, v1

    .line 52
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 59
    :cond_0
    iput-object v7, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .line 60
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    goto :goto_1

    .line 65
    .end local v4    # "i":I
    .end local v7    # "newAlbum":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;
    :cond_1
    iput-object v1, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;->mAlbums:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .line 67
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 68
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbums;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 76
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v0

    .line 71
    :catch_0
    move-exception v3

    .line 73
    .local v3, "e":Lorg/json/JSONException;
    invoke-virtual {v3}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
