.class Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsLiSyncAdapterProfileFeedService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    .line 83
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 84
    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$002(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/Context;)Landroid/content/Context;

    .line 85
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 3
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 91
    const-string v1, "SnsLiSync"

    const-string v2, "***************** SnsLiSyncAdapterProfileFeedService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$102(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 96
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$202(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Ljava/lang/String;)Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$302(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->performSync()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 108
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileFeedService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {v0}, Landroid/accounts/OperationCanceledException;->printStackTrace()V

    goto :goto_0

    .line 103
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileFeedService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;->access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileFeedService;)V

    .line 113
    return-void
.end method
