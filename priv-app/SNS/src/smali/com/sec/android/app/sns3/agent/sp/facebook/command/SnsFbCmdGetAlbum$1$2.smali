.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAlbum;
.source "SnsFbCmdGetAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseConvertID;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetAlbum;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;)Z
    .locals 6
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "album"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;

    .prologue
    const/4 v5, 0x0

    .line 116
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 117
    .local v1, "values":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 119
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_2

    .line 120
    if-eqz p6, :cond_1

    .line 122
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Album;->CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "id= \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 125
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    .line 127
    const-string v2, "id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v2, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    if-eqz v2, :cond_0

    .line 129
    const-string v2, "from_id"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromID:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    const-string v2, "from_name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mFrom:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;

    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFrom;->mFromName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_0
    const-string v2, "name"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const-string v2, "description"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mDescription:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v2, "location"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLocation:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const-string v2, "link"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mLink:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v2, "cover_photo"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCoverPhoto:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v2, "privacy"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mPrivacy:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v2, "count"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCount:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v2, "created_time"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mCreatedTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 141
    const-string v2, "updated_time"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mUpdatedTime:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->toTimestamp(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 143
    const-string v2, "type"

    iget-object v3, p6, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseAlbum;->mType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v2, "like_done"

    iget-object v3, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v3, v3, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    # getter for: Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->mLikeDone:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->access$100(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    sget-object v2, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Album;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 148
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Album;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setUri(Ljava/lang/String;)V

    .line 155
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-virtual {v2, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setSuccess(Z)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-virtual {v2, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 158
    const/4 v2, 0x1

    return v2

    .line 150
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    new-instance v3, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v4, "facebook"

    invoke-direct {v3, v4, p3, p4, p5}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1$2;->this$1:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;

    iget-object v2, v2, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetAlbum;->setUri(Ljava/lang/String;)V

    goto :goto_0
.end method
