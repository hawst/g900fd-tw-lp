.class Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;
.super Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNotification;
.source "SnsFbCmdGetNotifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p3, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/sns3/svc/sp/facebook/request/SnsFbReqGetNotification;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;)Z
    .locals 11
    .param p1, "reqID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "httpStatus"    # I
    .param p4, "errorCode"    # I
    .param p5, "reason"    # Landroid/os/Bundle;
    .param p6, "noti"    # Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;

    .prologue
    .line 62
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 63
    .local v5, "values":Landroid/content/ContentValues;
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 64
    .local v7, "valuesMessage":Landroid/content/ContentValues;
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 65
    .local v6, "valuesFriendRequests":Landroid/content/ContentValues;
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/sns3/SnsApplication;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 67
    .local v1, "cr":Landroid/content/ContentResolver;
    if-eqz p2, :cond_3

    .line 68
    if-eqz p6, :cond_2

    .line 70
    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mNoti:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    if-eqz v8, :cond_0

    .line 71
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Notifications;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v1, v8, v9, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 72
    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mNoti:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    .line 74
    .local v4, "curNotiQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :goto_0
    if-eqz v4, :cond_0

    .line 75
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 77
    const-string v8, "notification_id"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNotificationId:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v8, "sender_id"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mSenderId:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v8, "recipient_id"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mRecipientID:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    const-string v8, "object_id"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectID:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v8, "object_type"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mObjectType:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v8, "created_time"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 84
    const-string v8, "updated_time"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mUpdatedTime:Ljava/lang/Long;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 85
    const-string v8, "title_html"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleHtml:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v8, "title_text"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mTitleText:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    const-string v8, "body_html"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyHtml:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v8, "body_text"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mBodyText:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v8, "href"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mHref:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v8, "app_id"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mAppId:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v8, "is_unread"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsUnread:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v8, "is_hidden"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIsHidden:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v8, "icon_url"

    iget-object v9, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mIconUrl:Ljava/lang/String;

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Notifications;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 97
    iget-object v4, v4, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;

    goto :goto_0

    .line 102
    .end local v4    # "curNotiQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotiQueryResult;
    :cond_0
    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mMsg:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    if-eqz v8, :cond_1

    .line 103
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$MessageNotifications;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v1, v8, v9, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 104
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mMsg:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    .line 106
    .local v3, "curMsgQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :goto_1
    if-eqz v3, :cond_1

    .line 107
    invoke-virtual {v7}, Landroid/content/ContentValues;->clear()V

    .line 108
    const-string v8, "message_id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mMessageId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v8, "thread_id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mThreadId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    const-string v8, "author_id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mAuthorId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v8, "body"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mBody:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    const-string v8, "created_time"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mCreatedTime:Ljava/lang/Long;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 117
    const-string v8, "viewer_id"

    iget-object v9, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mViwerId:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$MessageNotifications;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v8, v7}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 122
    iget-object v3, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;

    goto :goto_1

    .line 126
    .end local v3    # "curMsgQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseMsgQueryResult;
    :cond_1
    move-object/from16 v0, p6

    iget-object v8, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mFreq:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    if-eqz v8, :cond_2

    .line 127
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendRequestNotifications;->CONTENT_URI:Landroid/net/Uri;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v1, v8, v9, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 128
    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseNotification;->mFreq:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    .line 130
    .local v2, "curFreqQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    :goto_2
    if-eqz v2, :cond_2

    .line 131
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    .line 132
    const-string v8, "user_id_from"

    iget-object v9, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUserIdFrom:Ljava/lang/String;

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const-string v8, "user_id_to"

    iget-object v9, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUserIdTo:Ljava/lang/String;

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v8, "message"

    iget-object v9, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mMessage:Ljava/lang/String;

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const-string v8, "time"

    iget-object v9, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mTime:Ljava/lang/Long;

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 140
    const-string v8, "unread"

    iget-object v9, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mUnread:Ljava/lang/String;

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    sget-object v8, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$FriendRequestNotifications;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 145
    iget-object v2, v2, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;

    goto :goto_2

    .line 149
    .end local v2    # "curFreqQuery":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseFreqQueryResult;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    sget-object v9, Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$Notifications;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;->setUri(Ljava/lang/String;)V

    .line 155
    :goto_3
    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    invoke-virtual {v8, p2}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;->setSuccess(Z)V

    .line 156
    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    invoke-virtual {v8, p0}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;->receive(Lcom/sec/android/app/sns3/svc/request/AbstractSnsRequest;)V

    .line 158
    const/4 v8, 0x1

    return v8

    .line 151
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    new-instance v9, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;

    const-string v10, "facebook"

    move-object/from16 v0, p5

    invoke-direct {v9, v10, p3, p4, v0}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Ljava/lang/String;IILandroid/os/Bundle;)V

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;->setResponseList(Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;)Z

    .line 153
    iget-object v8, p0, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications$1;->this$0:Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/app/sns3/agent/sp/facebook/command/SnsFbCmdGetNotifications;->setUri(Ljava/lang/String;)V

    goto :goto_3
.end method
