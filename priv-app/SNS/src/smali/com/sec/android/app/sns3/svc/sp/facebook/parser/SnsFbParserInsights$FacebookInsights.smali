.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserInsights$FacebookInsights;
.super Ljava/lang/Object;
.source "SnsFbParserInsights.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserInsights;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FacebookInsights"
.end annotation


# static fields
.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final PAGING:Ljava/lang/String; = "paging"

.field public static final PERIOD:Ljava/lang/String; = "period"

.field public static final VALUES:Ljava/lang/String; = "values"
