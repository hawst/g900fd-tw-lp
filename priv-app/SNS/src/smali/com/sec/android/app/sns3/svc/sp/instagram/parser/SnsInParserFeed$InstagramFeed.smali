.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFeed$InstagramFeed;
.super Ljava/lang/Object;
.source "SnsInParserFeed.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/instagram/parser/SnsInParserFeed;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InstagramFeed"
.end annotation


# static fields
.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CAPTION_TEXT:Ljava/lang/String; = "text"

.field public static final COMMENTS:Ljava/lang/String; = "comments"

.field public static final COUNT:Ljava/lang/String; = "count"

.field public static final CREATE_TIME:Ljava/lang/String; = "created_time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final ID:Ljava/lang/String; = "id"

.field public static final IMAGES:Ljava/lang/String; = "images"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LIKES:Ljava/lang/String; = "likes"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final LOCATION:Ljava/lang/String; = "location"

.field public static final LOC_NAME:Ljava/lang/String; = "name"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final STANDARD_RESOLUTION:Ljava/lang/String; = "standard_resolution"

.field public static final THUMBNAIL:Ljava/lang/String; = "thumbnail"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final URL:Ljava/lang/String; = "url"

.field public static final USER:Ljava/lang/String; = "user"

.field public static final USER_NAME:Ljava/lang/String; = "username"

.field public static final WIDTH:Ljava/lang/String; = "width"
