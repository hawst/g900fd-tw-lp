.class public Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
.super Landroid/app/Service;
.source "SnsLiSyncAdapterProfileService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SnsLiSync"

.field private static mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAuthority:Ljava/lang/String;

.field private mBundle:Landroid/os/Bundle;

.field private mBundleUpdatesQuery:Landroid/os/Bundle;

.field private mContext:Landroid/content/Context;

.field private mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

.field private mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

.field private mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

.field private mSyncResult:Landroid/content/SyncResult;

.field private mSyncState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundle:Landroid/os/Bundle;

    .line 83
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/accounts/Account;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    return-object p1
.end method

.method static synthetic access$202(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/SyncResult;)Landroid/content/SyncResult;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/content/SyncResult;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->onSyncCanceled()V

    return-void
.end method

.method static synthetic access$702(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    return p1
.end method

.method private handleSessionExpired()I
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 275
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    const-string v8, "com.sec.android.app.sns3.life"

    invoke-static {v7, v8}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 277
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 280
    .local v4, "notiMgr":Landroid/app/NotificationManager;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 282
    .local v3, "notiIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.sns3.RETRY_SSO_LINKEDIN"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    const/high16 v7, 0x800000

    invoke-virtual {v3, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 287
    const-string v7, "RetryLogin"

    invoke-virtual {v3, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 288
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-static {v7, v10, v3, v10}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 290
    .local v1, "launchIntent":Landroid/app/PendingIntent;
    const v6, 0x7f08003f

    .line 291
    .local v6, "titleID":I
    const v5, 0x7f08002e

    .line 292
    .local v5, "spID":I
    const v0, 0x7f02001e

    .line 294
    .local v0, "iconID":I
    new-instance v2, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 295
    .local v2, "notiBuilder":Landroid/app/Notification$Builder;
    iget-object v7, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    new-array v8, v9, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v7, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    const v9, 0x7f08003e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    .line 300
    const/16 v7, 0xce4

    invoke-virtual {v2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 302
    const/4 v7, -0x1

    return v7
.end method

.method private invokeBroadcast()V
    .locals 7

    .prologue
    .line 320
    const/4 v2, 0x0

    .line 322
    .local v2, "intent":Landroid/content/Intent;
    sget-object v3, Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$UserBasicInfo;->CONTENT_URI:Landroid/net/Uri;

    .line 323
    .local v3, "uri_profile":Landroid/net/Uri;
    const-string v0, "com.sec.android.app.sns3.action.SYNC_LI_PROFILES"

    .line 324
    .local v0, "action":Ljava/lang/String;
    iget v4, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 326
    .local v1, "bResult":Z
    :goto_0
    new-instance v2, Landroid/content/Intent;

    .end local v2    # "intent":Landroid/content/Intent;
    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 327
    .restart local v2    # "intent":Landroid/content/Intent;
    const-string v4, "SNS3_CONTENT_URI_PROFILE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 328
    const-string v4, "SNS_RESULT"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 330
    const-string v4, "com.sec.android.app.sns3.permission.RECEIVE_SNS_BROADCAST"

    invoke-virtual {p0, v2, v4}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 331
    invoke-static {}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isLoggable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 332
    const-string v4, "SnsLiSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SnsLiSyncAdapterProfileService - invokeBroadcast() : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], uri(profile) = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "], result = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    return-void

    .line 324
    .end local v1    # "bResult":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onSyncCanceled()V
    .locals 2

    .prologue
    .line 307
    const-string v0, "SnsLiSync"

    const-string v1, "***************** SnsLiSyncAdapterProfileService : onSyncCanceled !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundle:Landroid/os/Bundle;

    .line 313
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;

    if-eqz v0, :cond_0

    .line 314
    sget-object v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->abort()Z

    .line 316
    :cond_0
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 144
    const-string v0, "SnsLiSync"

    const-string v1, "***************** SnsLiSyncAdapterProfileService : onBind !!! *****************"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 130
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

    invoke-direct {v0, p0, p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;-><init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncAdapter:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$SyncAdapterImpl;

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    if-nez v0, :cond_1

    .line 137
    new-instance v0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    .line 139
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "bActive":Z
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAccount:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mAuthority:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/content/ContentResolver;->isSyncActive(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    .line 158
    :cond_0
    const-string v1, "SnsLiSync"

    const-string v2, "***************** SnsLiSyncAdapterProfileService : onUnbind !!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    if-eqz v0, :cond_2

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 163
    :cond_1
    const-string v1, "SnsLiSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsLiSyncAdapterProfileService : onUnbind : SYNC ERROR : performSync was stopped by forced abort or pending problem!!! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v1

    return v1

    .line 168
    :cond_2
    const-string v1, "SnsLiSync"

    const-string v2, "SnsLiSyncAdapterProfileService : onUnbind : COMPLETE STATE!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public performSync()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/OperationCanceledException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v7, -0x1

    .line 175
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileService : performSync - START !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iput v5, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    .line 179
    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v3

    const-string v4, "linkedin"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;

    .line 182
    .local v1, "mLiToken":Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;
    :try_start_0
    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getTokenState()I

    move-result v3

    if-eq v3, v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/sp/linkedin/SnsLiToken;->getTokenState()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 184
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->handleSessionExpired()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    .line 185
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Session expired or invalid!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 260
    const-string v3, "SnsLiSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SnsLiSyncAdapterProfileService : EXCEPTION !!! "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncResult:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x1

    iput-wide v4, v3, Landroid/content/SyncStats;->numConflictDetectedExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 266
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->invokeBroadcast()V

    .line 268
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 187
    :cond_1
    :try_start_2
    new-instance v3, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;

    iget-object v4, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const-string v5, "profile"

    iget-object v6, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundle:Landroid/os/Bundle;

    invoke-direct {v3, p0, v4, v5, v6}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$1;-><init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    sput-object v3, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;

    .line 211
    sget-object v3, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mReq:Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->request()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 212
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->suspendSync()V

    .line 216
    :goto_1
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    if-ne v3, v7, :cond_3

    .line 217
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "SnsFbSyncAdapterEventService is failed!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 266
    :catchall_0
    move-exception v3

    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->invokeBroadcast()V

    .line 268
    const-string v4, "SnsLiSync"

    const-string v5, "***************** SnsLiSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    throw v3

    .line 214
    :cond_2
    const/4 v3, -0x1

    :try_start_3
    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    goto :goto_1

    .line 220
    :cond_3
    new-instance v2, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService$2;-><init>(Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 246
    .local v2, "requestUpdates":Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;
    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/linkedin/request/SnsLiReqBase;->request()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;

    invoke-virtual {v3}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncDataMgr;->suspendSync()V

    .line 251
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mBundleUpdatesQuery:Landroid/os/Bundle;

    if-nez v3, :cond_3

    .line 254
    iget v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I

    if-ne v3, v7, :cond_5

    .line 255
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "[SnsLiSyncAdapterProfileService] updates sync is failed!!!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3

    .line 249
    :cond_4
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->mSyncState:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 266
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/sns3/sync/sp/linkedin/SnsLiSyncAdapterProfileService;->invokeBroadcast()V

    .line 268
    const-string v3, "SnsLiSync"

    const-string v4, "***************** SnsLiSyncAdapterProfileService : performSync - FINISHED !!! *****************"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
