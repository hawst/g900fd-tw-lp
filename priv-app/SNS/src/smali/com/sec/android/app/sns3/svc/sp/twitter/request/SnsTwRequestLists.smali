.class public abstract Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;
.super Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;
.source "SnsTwRequestLists.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/twitter/callback/ISnsTwReqCbList;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "lists"    # Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;
    .param p3, "param"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const/16 v0, 0x16

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 40
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mUrl:Ljava/lang/String;

    .line 41
    invoke-virtual {p2}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwListsAPI;->getHttpMethod()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mHttpMethod:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mReqParams:Landroid/os/Bundle;

    .line 43
    return-void
.end method


# virtual methods
.method protected composeMore()V
    .locals 6

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mParams:Landroid/os/Bundle;

    const-string v1, "oauth_token"

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrlEx(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mHttpMethod:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mReqUri:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mParams:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mSubParams:Landroid/os/Bundle;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwitter;->CONSUMER_SECRET:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mToken:Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;

    invoke-virtual {v5}, Lcom/sec/android/app/sns3/svc/sp/twitter/SnsTwToken;->getAccessTokenSecret()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/composer/SnsTwComposerUtils;->generateSignature(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mSig:Ljava/lang/String;

    .line 51
    return-void
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v1, "SNS"

    const-string v2, "<SnsTwRequestLists> parse()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 58
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 61
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserList;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseList;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 66
    const-string v0, "SNS"

    const-string v1, "<SnsTwRequestLists> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReqID()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseList;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseList;)Z

    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method protected setUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "1.1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/twitter/request/SnsTwRequestLists;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
