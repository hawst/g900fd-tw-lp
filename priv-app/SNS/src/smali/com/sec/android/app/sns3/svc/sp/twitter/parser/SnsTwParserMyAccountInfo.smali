.class public Lcom/sec/android/app/sns3/svc/sp/twitter/parser/SnsTwParserMyAccountInfo;
.super Ljava/lang/Object;
.source "SnsTwParserMyAccountInfo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;
    .locals 4
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 13
    const/4 v2, 0x0

    .line 15
    .local v2, "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;
    invoke-static {p0}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->isJsonEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 16
    const/4 v3, 0x0

    .line 32
    :goto_0
    return-object v3

    .line 19
    :cond_0
    new-instance v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;

    .end local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;
    invoke-direct {v2}, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;-><init>()V

    .line 21
    .restart local v2    # "response":Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 23
    .local v1, "jsonObj":Lorg/json/JSONObject;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mId:Ljava/lang/String;

    .line 24
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mName:Ljava/lang/String;

    .line 25
    const-string v3, "profile_image_url"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mPic:Ljava/lang/String;

    .line 26
    const-string v3, "screen_name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/app/sns3/svc/sp/twitter/response/SnsTwResponseMyAccountInfo;->mUserName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "jsonObj":Lorg/json/JSONObject;
    :goto_1
    move-object v3, v2

    .line 32
    goto :goto_0

    .line 28
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Lorg/json/JSONException;
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_1
.end method
