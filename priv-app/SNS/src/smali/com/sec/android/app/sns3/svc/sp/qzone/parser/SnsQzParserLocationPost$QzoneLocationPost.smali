.class public interface abstract Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserLocationPost$QzoneLocationPost;
.super Ljava/lang/Object;
.source "SnsQzParserLocationPost.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserLocationPost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "QzoneLocationPost"
.end annotation


# static fields
.field public static final CAPTION:Ljava/lang/String; = "caption"

.field public static final CHECKINS:Ljava/lang/String; = "checkins"

.field public static final CHECKIN_ID:Ljava/lang/String; = "cellid"

.field public static final CREATED_TIME:Ljava/lang/String; = "time"

.field public static final DATA:Ljava/lang/String; = "data"

.field public static final DETAIL_PARAM:Ljava/lang/String; = "detailparam"

.field public static final HOST_OPENID:Ljava/lang/String; = "hostopenid"

.field public static final ICON:Ljava/lang/String; = "portraiturl"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final NAME:Ljava/lang/String; = "nickname"

.field public static final OPENID:Ljava/lang/String; = "openid"

.field public static final PICTURE:Ljava/lang/String; = "pictureurl"

.field public static final PICTUREURL_ORIGIN:Ljava/lang/String; = "pictureurl_origin"

.field public static final PLACE:Ljava/lang/String; = "positionname"
