.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB$GroupColumns;
.super Ljava/lang/Object;
.source "SnsFacebookDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/facebook/db/SnsFacebookDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "GroupColumns"
.end annotation


# static fields
.field public static final ADMINISTRATOR:Ljava/lang/String; = "administrator"

.field public static final BOOKMARK_ORDER:Ljava/lang/String; = "bookmark_order"

.field public static final DESCRIPTION:Ljava/lang/String; = "description"

.field public static final GROUP_ID:Ljava/lang/String; = "id"

.field public static final GROUP_NAME:Ljava/lang/String; = "name"

.field public static final ICON:Ljava/lang/String; = "icon"

.field public static final LINK:Ljava/lang/String; = "link"

.field public static final OWNER_ID:Ljava/lang/String; = "owner_id"

.field public static final OWNER_NAME:Ljava/lang/String; = "owner_name"

.field public static final PRIVACY:Ljava/lang/String; = "privacy"

.field public static final UPDATED_TIME:Ljava/lang/String; = "updated_time"

.field public static final VERSION:Ljava/lang/String; = "versioin"
