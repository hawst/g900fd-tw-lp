.class public interface abstract Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB$CompanyUpdatesColumns;
.super Ljava/lang/Object;
.source "SnsLinkedInDB.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/agent/sp/linkedin/db/SnsLinkedInDB;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CompanyUpdatesColumns"
.end annotation


# static fields
.field public static final COMPANY_ID:Ljava/lang/String; = "company_id"

.field public static final COMPANY_NAME:Ljava/lang/String; = "company_name"

.field public static final COMPANY_UPDATE_KEY:Ljava/lang/String; = "update_key"

.field public static final COMPANY_UPDATE_SUB_TYPE:Ljava/lang/String; = "update_sub_type"

.field public static final JOB_ID:Ljava/lang/String; = "job_id"

.field public static final NEW_POSITION_COMPANY:Ljava/lang/String; = "new_position_company"

.field public static final NEW_POSITION_TITLE:Ljava/lang/String; = "new_position_title"

.field public static final OLD_POSITION_COMPANY:Ljava/lang/String; = "old_position_company"

.field public static final OLD_POSITION_TITLE:Ljava/lang/String; = "old_position_title"

.field public static final PERSON_ID:Ljava/lang/String; = "person_id"

.field public static final SHARE_ID:Ljava/lang/String; = "share_id"
