.class Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "SnsFbSyncAdapterGalleryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    .line 96
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    .line 97
    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$002(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/Context;)Landroid/content/Context;

    .line 98
    return-void
.end method


# virtual methods
.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 4
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "extras"    # Landroid/os/Bundle;
    .param p3, "authority"    # Ljava/lang/String;
    .param p4, "provider"    # Landroid/content/ContentProviderClient;
    .param p5, "syncResult"    # Landroid/content/SyncResult;

    .prologue
    .line 104
    const-string v1, "SnsFbSync"

    const-string v2, "***************** SnsFbSyncAdapterGalleryService : onPerformSync!!! *****************"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    invoke-static {}, Lcom/sec/android/app/sns3/SnsApplication;->getInstance()Lcom/sec/android/app/sns3/SnsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/SnsApplication;->getSvcMgr()Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v3, "facebook"

    invoke-virtual {v1, v3}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mFbToken:Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;
    invoke-static {v2, v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$102(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;)Lcom/sec/android/app/sns3/svc/sp/facebook/SnsFbToken;

    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAccount:Landroid/accounts/Account;
    invoke-static {v1, p1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$202(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/accounts/Account;)Landroid/accounts/Account;

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mAuthority:Ljava/lang/String;
    invoke-static {v1, p3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$302(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Ljava/lang/String;)Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # setter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncResult:Landroid/content/SyncResult;
    invoke-static {v1, p5}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$402(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;Landroid/content/SyncResult;)Landroid/content/SyncResult;

    .line 114
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mBundle:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$600(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "limit"

    iget-object v3, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # getter for: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->mSyncDataMgr:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;
    invoke-static {v3}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$500(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncDataMgr;

    const-string v3, "50"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->performSync()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 123
    :goto_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterGalleryService : onPerformSync is CANCELED!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 119
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v0

    .line 120
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SnsFbSync"

    const-string v2, "SnsFbSyncAdapterGalleryService : Abnormal Syncing!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSyncCanceled()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService$SyncAdapterImpl;->this$0:Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;

    # invokes: Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->onSyncCanceled()V
    invoke-static {v0}, Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;->access$700(Lcom/sec/android/app/sns3/sync/sp/facebook/SnsFbSyncAdapterGalleryService;)V

    .line 128
    return-void
.end method
