.class public Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserGroupList;
.super Ljava/lang/Object;
.source "SnsFbParserGroupList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserGroupList$FacebookGroupList;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;
    .locals 9
    .param p0, "contentObj"    # Ljava/lang/String;

    .prologue
    .line 39
    new-instance v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;

    invoke-direct {v3}, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;-><init>()V

    .line 41
    .local v3, "groupsObj":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;
    const/4 v2, 0x0

    .line 42
    .local v2, "groups":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    const/4 v0, 0x0

    .line 45
    .local v0, "curGroups":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    :try_start_0
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 47
    .local v6, "jsonObject":Lorg/json/JSONObject;
    new-instance v5, Lorg/json/JSONArray;

    const-string v8, "data"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 50
    .local v5, "ja":Lorg/json/JSONArray;
    if-eqz v5, :cond_1

    .line 52
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-ge v4, v8, :cond_1

    .line 53
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->optString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserGroup;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    move-result-object v7

    .line 56
    .local v7, "newGroups":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    if-nez v2, :cond_0

    .line 57
    move-object v2, v7

    .line 58
    move-object v0, v2

    .line 52
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 60
    :cond_0
    iput-object v7, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    .line 61
    iget-object v0, v0, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;->mNext:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    goto :goto_1

    .line 66
    .end local v4    # "i":I
    .end local v7    # "newGroups":Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;
    :cond_1
    iput-object v2, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;->mGroups:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroup;

    .line 68
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 69
    const-string v8, "paging"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/sns3/svc/sp/facebook/parser/SnsFbParserPaging;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;

    move-result-object v8

    iput-object v8, v3, Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponseGroupList;->mPaging:Lcom/sec/android/app/sns3/svc/sp/facebook/response/SnsFbResponsePaging;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    .end local v5    # "ja":Lorg/json/JSONArray;
    .end local v6    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :goto_2
    return-object v3

    .line 73
    :catch_0
    move-exception v1

    .line 75
    .local v1, "e":Lorg/json/JSONException;
    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
