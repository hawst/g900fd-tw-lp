.class Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$1;
.super Ljava/lang/Object;
.source "SnsTwSyncAdapterHomeTimelineService.java"

# interfaces
.implements Lcom/sec/android/app/sns3/agent/command/ISnsCommandCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->performSync()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCmdRespond(IZLjava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1, "cmdID"    # I
    .param p2, "bSuccess"    # Z
    .param p3, "uri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    .local p4, "responseList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;>;"
    if-eqz p2, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    const/4 v1, 0x2

    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mSyncState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$602(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;I)I

    .line 180
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    invoke-virtual {v0}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->resumeSync()V

    .line 182
    return-void

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService$1;->this$0:Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;

    const/4 v1, -0x1

    # setter for: Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->mSyncState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;->access$602(Lcom/sec/android/app/sns3/sync/sp/twitter/SnsTwSyncAdapterHomeTimelineService;I)I

    goto :goto_0
.end method
