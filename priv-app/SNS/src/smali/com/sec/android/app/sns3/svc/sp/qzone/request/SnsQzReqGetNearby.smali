.class public abstract Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;
.super Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;
.source "SnsQzReqGetNearby.java"

# interfaces
.implements Lcom/sec/android/app/sns3/svc/sp/qzone/callback/ISnsQzReqCbNearby;


# static fields
.field private static DEFAULT_DISTANCE:Ljava/lang/String;


# instance fields
.field private final mAccessToken:Ljava/lang/String;

.field private mDistance:Ljava/lang/String;

.field private mEndTime:Ljava/lang/String;

.field private mLatitude:Ljava/lang/String;

.field private mLongitude:Ljava/lang/String;

.field private mStartTime:Ljava/lang/String;

.field private final mUserID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "1000"

    sput-object v0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->DEFAULT_DISTANCE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "svcMgr"    # Lcom/sec/android/app/sns3/svc/SnsSvcMgr;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 51
    const/16 v1, 0x16

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;-><init>(Lcom/sec/android/app/sns3/svc/SnsSvcMgr;I)V

    .line 40
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mDistance:Ljava/lang/String;

    .line 41
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 42
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 43
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mStartTime:Ljava/lang/String;

    .line 44
    iput-object v2, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mEndTime:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mSvcMgr:Lcom/sec/android/app/sns3/svc/SnsSvcMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/sns3/svc/SnsSvcMgr;->getTokenMgr()Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;

    move-result-object v1

    const-string v2, "qzone"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/sns3/svc/token/SnsTokenMgr;->getToken(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/token/SnsTokenBase;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;

    .line 54
    .local v0, "token":Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;
    if-eqz p2, :cond_0

    .line 55
    const-string v1, "distance"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mDistance:Ljava/lang/String;

    .line 57
    const-string v1, "latitude"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLatitude:Ljava/lang/String;

    .line 58
    const-string v1, "longitude"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLongitude:Ljava/lang/String;

    .line 60
    const-string v1, "start_time"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mStartTime:Ljava/lang/String;

    .line 61
    const-string v1, "end_time"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mEndTime:Ljava/lang/String;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mDistance:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 64
    sget-object v1, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->DEFAULT_DISTANCE:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mDistance:Ljava/lang/String;

    .line 67
    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getUserID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mUserID:Ljava/lang/String;

    .line 68
    invoke-virtual {v0}, Lcom/sec/android/app/sns3/svc/sp/qzone/SnsQzToken;->getAccessToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/sns3/svc/util/SnsUtil;->encodeUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mAccessToken:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method protected compose()Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;
    .locals 6

    .prologue
    .line 75
    const-string v2, "GET"

    .line 76
    .local v2, "method":Ljava/lang/String;
    const/4 v3, 0x0

    .line 77
    .local v3, "uri":Ljava/lang/String;
    const/4 v4, 0x0

    .line 78
    .local v4, "header":Landroid/os/Bundle;
    const/4 v5, 0x0

    .line 80
    .local v5, "body":Landroid/os/Bundle;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://openmobile.qq.com/qzone/mobile/get_friends_poi_checkin?oauth_consumer_key=100350160&access_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mAccessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&openid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mUserID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&flag="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&starttime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mStartTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&endtime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mEndTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLongitude:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mLatitude:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&distance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mDistance:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 85
    new-instance v0, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;

    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mReqID:I

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/sns3/svc/http/SnsHttpRequest;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-object v0
.end method

.method protected parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    .locals 3
    .param p1, "content"    # Ljava/lang/String;

    .prologue
    .line 90
    const-string v1, "SNS"

    const-string v2, "Qzone  SnsQzReqGetNearby response SUCCESS!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqBase;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v0

    .line 93
    .local v0, "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    if-eqz v0, :cond_0

    .line 96
    .end local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :goto_0
    return-object v0

    .restart local v0    # "response":Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;
    :cond_0
    new-instance v1, Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserNearby;

    invoke-direct {v1}, Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserNearby;-><init>()V

    invoke-virtual {v1, p1}, Lcom/sec/android/app/sns3/svc/sp/qzone/parser/SnsQzParserNearby;->parse(Ljava/lang/String;)Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    move-result-object v0

    goto :goto_0
.end method

.method protected respond(Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;)Z
    .locals 7
    .param p1, "result"    # Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;

    .prologue
    .line 102
    const-string v0, "SNS"

    const-string v1, "<SnsQzReqGetNearby> respond()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget v1, p0, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->mReqID:I

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->isSuccess()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getHttpstatus()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getErrorCode()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getReason()Landroid/os/Bundle;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/app/sns3/svc/request/SnsRequestResult;->getResponse()Lcom/sec/android/app/sns3/svc/sp/SnsSpResponse;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/sns3/svc/sp/qzone/request/SnsQzReqGetNearby;->onReqRespond(IZIILandroid/os/Bundle;Lcom/sec/android/app/sns3/svc/sp/qzone/response/SnsQzResponseNearby;)Z

    .line 106
    const/4 v0, 0x1

    return v0
.end method
