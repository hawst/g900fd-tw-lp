.class public Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;
.super Ljava/lang/Thread;
.source "SnsLogging.java"


# static fields
.field public static final GET_LOGS:Z = true

.field private static final HISTORY_FILE_NAME:Ljava/lang/String; = "_history_"

.field private static final LOG_DIRECTORY:Ljava/lang/String; = "/data/data/com.sec.android.app.sns3/cache"

.field private static final MAX_HISTORY_LOGS:I = 0x7

.field private static final MAX_JSON_LOGS:I = 0xc8

.field private static mCategory:I

.field private static mContent:Ljava/lang/String;

.field private static mHistoryDate:Ljava/lang/String;

.field private static mHttpStatus:I

.field private static mPath:Ljava/io/File;

.field private static mPostDate:Ljava/lang/String;

.field private static mReqId:I

.field private static mSpType:Ljava/lang/String;

.field private static mUri:Ljava/net/URI;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mSpType:Ljava/lang/String;

    .line 62
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mContent:Ljava/lang/String;

    .line 64
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mUri:Ljava/net/URI;

    .line 67
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHistoryDate:Ljava/lang/String;

    .line 69
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPostDate:Ljava/lang/String;

    .line 71
    sput-object v0, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;ILjava/net/URI;Ljava/lang/String;)V
    .locals 0
    .param p1, "reqID"    # I
    .param p2, "category"    # I
    .param p3, "spType"    # Ljava/lang/String;
    .param p4, "httpStatus"    # I
    .param p5, "uri"    # Ljava/net/URI;
    .param p6, "content"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 76
    sput p1, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mReqId:I

    .line 77
    sput p2, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mCategory:I

    .line 78
    sput p4, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHttpStatus:I

    .line 79
    sput-object p3, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mSpType:Ljava/lang/String;

    .line 80
    sput-object p6, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mContent:Ljava/lang/String;

    .line 81
    sput-object p5, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mUri:Ljava/net/URI;

    .line 83
    return-void
.end method

.method private deleteOldLogs(Z)V
    .locals 20
    .param p1, "isHistory"    # Z

    .prologue
    .line 185
    if-eqz p1, :cond_1

    const-string v8, "log"

    .line 187
    .local v8, "ends":Ljava/lang/String;
    :goto_0
    new-instance v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v8}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging$1;-><init>(Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;Ljava/lang/String;)V

    .line 193
    .local v10, "fileFilter":Ljava/io/FilenameFilter;
    new-instance v9, Ljava/io/File;

    sget-object v13, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    invoke-virtual {v13}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v9, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 194
    .local v9, "f":Ljava/io/File;
    invoke-virtual {v9, v10}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v11

    .line 196
    .local v11, "files":[Ljava/io/File;
    if-eqz p1, :cond_2

    const/4 v12, 0x7

    .line 198
    .local v12, "maxCount":I
    :goto_1
    if-eqz v11, :cond_4

    array-length v13, v11

    if-le v13, v12, :cond_4

    .line 200
    new-instance v13, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging$2;-><init>(Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;)V

    invoke-static {v11, v13}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 218
    if-eqz p1, :cond_3

    .line 220
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    .line 221
    .local v3, "date":Ljava/util/Date;
    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 223
    .local v4, "curDate":J
    const/4 v2, 0x0

    .local v2, "cnt":I
    :goto_2
    array-length v13, v11

    if-ge v2, v13, :cond_4

    .line 224
    aget-object v13, v11, v2

    invoke-virtual {v13}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    .line 225
    .local v14, "targetDate":J
    sub-long v16, v4, v14

    const-wide/32 v18, 0x5265c00

    div-long v6, v16, v18

    .line 227
    .local v6, "diffDays":J
    const-wide/16 v16, 0x7

    cmp-long v13, v6, v16

    if-ltz v13, :cond_0

    .line 228
    aget-object v13, v11, v2

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 223
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 185
    .end local v2    # "cnt":I
    .end local v3    # "date":Ljava/util/Date;
    .end local v4    # "curDate":J
    .end local v6    # "diffDays":J
    .end local v8    # "ends":Ljava/lang/String;
    .end local v9    # "f":Ljava/io/File;
    .end local v10    # "fileFilter":Ljava/io/FilenameFilter;
    .end local v11    # "files":[Ljava/io/File;
    .end local v12    # "maxCount":I
    .end local v14    # "targetDate":J
    :cond_1
    const-string v8, "json"

    goto :goto_0

    .line 196
    .restart local v8    # "ends":Ljava/lang/String;
    .restart local v9    # "f":Ljava/io/File;
    .restart local v10    # "fileFilter":Ljava/io/FilenameFilter;
    .restart local v11    # "files":[Ljava/io/File;
    :cond_2
    const/16 v12, 0xc8

    goto :goto_1

    .line 233
    .restart local v12    # "maxCount":I
    :cond_3
    const/16 v2, 0x64

    .restart local v2    # "cnt":I
    :goto_3
    array-length v13, v11

    if-ge v2, v13, :cond_4

    .line 234
    aget-object v13, v11, v2

    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 233
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 238
    .end local v2    # "cnt":I
    :cond_4
    return-void
.end method

.method private saveResponseLog()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x16

    const/4 v7, 0x0

    .line 151
    new-instance v3, Ljava/io/File;

    sget-object v4, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resp_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mSpType:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v6, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mReqId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPostDate:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 152
    .local v3, "logs":Ljava/io/File;
    const/4 v1, 0x0

    .line 156
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v4, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mContent:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 173
    :goto_0
    if-eqz v2, :cond_0

    .line 174
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    .line 177
    :cond_0
    sget v4, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mCategory:I

    if-ne v4, v8, :cond_5

    .line 178
    invoke-direct {p0, v7}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->deleteOldLogs(Z)V

    move-object v1, v2

    .line 181
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    :cond_1
    :goto_1
    return-void

    .line 162
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "SNS"

    const-string v5, "SnsLogging : write() Error!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 167
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    move-object v1, v2

    .line 169
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v0, "e":Ljava/io/FileNotFoundException;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    :goto_2
    :try_start_3
    const-string v4, "SNS"

    const-string v5, "SnsLogging : FileOutputStream() Error!!"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 173
    if-eqz v1, :cond_2

    .line 174
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 177
    :cond_2
    sget v4, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mCategory:I

    if-ne v4, v8, :cond_1

    .line 178
    invoke-direct {p0, v7}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->deleteOldLogs(Z)V

    goto :goto_1

    .line 173
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v4

    :goto_3
    if-eqz v1, :cond_3

    .line 174
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 177
    :cond_3
    sget v5, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mCategory:I

    if-ne v5, v8, :cond_4

    .line 178
    invoke-direct {p0, v7}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->deleteOldLogs(Z)V

    :cond_4
    throw v4

    .line 173
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 167
    :catch_2
    move-exception v0

    goto :goto_2

    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :cond_5
    move-object v1, v2

    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "fos":Ljava/io/FileOutputStream;
    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/16 v13, 0xc8

    .line 92
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 93
    .local v2, "date":Ljava/util/Date;
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 95
    .local v0, "curTime":J
    const-string v10, "yy-MM-dd"

    invoke-static {v10, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHistoryDate:Ljava/lang/String;

    .line 96
    const-string v10, "yy-MM-dd-hh-mm-ss"

    invoke-static {v10, v0, v1}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPostDate:Ljava/lang/String;

    .line 98
    const-string v9, "X"

    .line 100
    .local v9, "succ":Ljava/lang/String;
    sget v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHttpStatus:I

    if-ne v10, v13, :cond_0

    .line 101
    const-string v9, "O"

    .line 103
    :cond_0
    new-instance v6, Ljava/io/File;

    const-string v10, "/data/data/com.sec.android.app.sns3/cache"

    invoke-direct {v6, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .local v6, "path":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/logs/"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    .line 106
    sget-object v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->mkdir()Z

    .line 108
    sget-object v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 110
    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v11, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPath:Ljava/io/File;

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "_history_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHistoryDate:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".log"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 112
    .local v5, "historyFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 116
    .local v7, "raf":Ljava/io/RandomAccessFile;
    :try_start_0
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    .line 117
    invoke-virtual {v5}, Ljava/io/File;->createNewFile()Z

    .line 119
    :cond_1
    new-instance v8, Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "rw"

    invoke-direct {v8, v10, v11}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .local v8, "raf":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 121
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "\r\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mReqId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mPostDate:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mUri:Ljava/net/URI;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/io/RandomAccessFile;->writeBytes(Ljava/lang/String;)V

    .line 123
    sget v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mCategory:I

    const/16 v11, 0x16

    if-ne v10, v11, :cond_2

    .line 124
    const/4 v10, 0x1

    invoke-direct {p0, v10}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->deleteOldLogs(Z)V

    .line 126
    :cond_2
    sget v10, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->mHttpStatus:I

    if-ne v10, v13, :cond_3

    .line 127
    invoke-direct {p0}, Lcom/sec/android/app/sns3/svc/util/logging/SnsLogging;->saveResponseLog()V

    .line 129
    :cond_3
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 130
    const/4 v7, 0x0

    .line 143
    .end local v5    # "historyFile":Ljava/io/File;
    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    :cond_4
    :goto_0
    return-void

    .line 132
    .restart local v5    # "historyFile":Ljava/io/File;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v3

    .line 133
    .local v3, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 136
    if-eqz v7, :cond_4

    .line 137
    :try_start_2
    invoke-virtual {v7}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 138
    :catch_1
    move-exception v4

    .line 139
    .local v4, "ex":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 132
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "ex":Ljava/lang/Exception;
    .end local v7    # "raf":Ljava/io/RandomAccessFile;
    .restart local v8    # "raf":Ljava/io/RandomAccessFile;
    :catch_2
    move-exception v3

    move-object v7, v8

    .end local v8    # "raf":Ljava/io/RandomAccessFile;
    .restart local v7    # "raf":Ljava/io/RandomAccessFile;
    goto :goto_1
.end method
