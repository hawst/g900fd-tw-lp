.class public Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;
.super Ljava/lang/Object;
.source "SnsCommandResponse.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mErrorCode:I

.field private mHttpStatus:I

.field private mReason:Landroid/os/Bundle;

.field private mSpType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse$1;

    invoke-direct {v0}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse$1;-><init>()V

    sput-object v0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->readFromParcel(Landroid/os/Parcel;)V

    .line 74
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILandroid/os/Bundle;)V
    .locals 0
    .param p1, "spType"    # Ljava/lang/String;
    .param p2, "httpStatus"    # I
    .param p3, "errorCode"    # I
    .param p4, "reason"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mSpType:Ljava/lang/String;

    .line 51
    iput p2, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mHttpStatus:I

    .line 52
    iput p3, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mErrorCode:I

    .line 53
    iput-object p4, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mReason:Landroid/os/Bundle;

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorCode()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mErrorCode:I

    return v0
.end method

.method public getHttpStatus()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mHttpStatus:I

    return v0
.end method

.method public getReason()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mReason:Landroid/os/Bundle;

    return-object v0
.end method

.method public getSpType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mSpType:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mSpType:Ljava/lang/String;

    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mHttpStatus:I

    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mErrorCode:I

    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mReason:Landroid/os/Bundle;

    .line 89
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mSpType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mHttpStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    iget v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mErrorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/sns3/agent/command/response/SnsCommandResponse;->mReason:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 82
    return-void
.end method
