.class public Lcom/skp/tcloud/service/lib/TcloudData$Files;
.super Ljava/lang/Object;
.source "TcloudData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Files"
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public albumArtist:Ljava/lang/String;

.field public albumId:J

.field public artist:Ljava/lang/String;

.field public artistId:J

.field public bookmark:I

.field public bucketDisplayName:Ljava/lang/String;

.field public bucketId:Ljava/lang/String;

.field public category:Ljava/lang/String;

.field public cloudAlbumArtPath:Ljava/lang/String;

.field public cloudCachedPath:Ljava/lang/String;

.field public cloudEtag:Ljava/lang/String;

.field public cloudIsAvailableOffLine:Z

.field public cloudIsAvailableThumb:Z

.field public cloudIsCached:Z

.field public cloudIsDir:Z

.field public cloudLastViewed:Ljava/lang/String;

.field public cloudRevision:I

.field public cloudServerId:Ljava/lang/String;

.field public cloudServerPath:Ljava/lang/String;

.field public cloudThumbPath:Ljava/lang/String;

.field public composer:Ljava/lang/String;

.field public contentId:Ljava/lang/String;

.field public data:Ljava/lang/String;

.field public dateAdded:J

.field public dateModified:J

.field public dateTaken:J

.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public duration:J

.field public format:I

.field public genreName:Ljava/lang/String;

.field public height:I

.field public id:J

.field public isAlaram:Z

.field public isDrm:Z

.field public isFavorite:Z

.field public isMusic:Z

.field public isNotification:Z

.field public isPlayed:Z

.field public isPodCast:Z

.field public isPrivate:Z

.field public isRingTone:Z

.field public isSound:Z

.field public language:Ljava/lang/String;

.field public latitude:D

.field public longitude:D

.field public mediaType:I

.field public mimeType:Ljava/lang/String;

.field public miniThumbData:Ljava/lang/String;

.field public miniThumbMagic:I

.field public name:Ljava/lang/String;

.field public orientation:I

.field public parent:I

.field public recentlyAddedRemoveFlag:I

.field public recentlyPlayed:I

.field public resolution:Ljava/lang/String;

.field public resumePos:I

.field public size:J

.field public tags:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public titleKey:Ljava/lang/String;

.field public track:I

.field public width:I

.field public year:I

.field public yearName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->id:J

    .line 20
    const-string v0, "content_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->contentId:Ljava/lang/String;

    .line 21
    const-string v0, "name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->name:Ljava/lang/String;

    .line 22
    const-string v0, "tcloud_server_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->cloudServerId:Ljava/lang/String;

    .line 23
    const-string v0, "tcloud_server_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->cloudServerPath:Ljava/lang/String;

    .line 24
    const-string v0, "tcloud_thumb_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->cloudThumbPath:Ljava/lang/String;

    .line 26
    const-string v0, "tcloud_is_cached"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->cloudIsCached:Z

    .line 27
    const-string v0, "mini_thumb_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->miniThumbData:Ljava/lang/String;

    .line 28
    const-string v0, "date_added"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->dateAdded:J

    .line 29
    const-string v0, "date_modified"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->dateModified:J

    .line 30
    const-string v0, "media_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->mediaType:I

    .line 31
    const-string v0, "_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->displayName:Ljava/lang/String;

    .line 32
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->mimeType:Ljava/lang/String;

    .line 33
    const-string v0, "datetaken"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->dateTaken:J

    .line 34
    const-string v0, "_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Files;->size:J

    .line 35
    return-void

    .line 26
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
