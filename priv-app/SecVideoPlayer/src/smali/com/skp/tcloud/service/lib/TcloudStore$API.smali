.class public Lcom/skp/tcloud/service/lib/TcloudStore$API;
.super Ljava/lang/Object;
.source "TcloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "API"
.end annotation


# static fields
.field public static KEY_CLOUD_AVAILABLE:Ljava/lang/String;

.field public static KEY_DOWNLOAD:Ljava/lang/String;

.field public static KEY_EXCEPTION:Ljava/lang/String;

.field public static KEY_GETSTREAMING_URL:Ljava/lang/String;

.field public static KEY_GETTHUMBNAIL:Ljava/lang/String;

.field public static KEY_MAKE_AVAILABLE_OFFLINE:Ljava/lang/String;

.field public static KEY_PREFETCH:Ljava/lang/String;

.field public static KEY_REVERT_AVAILABLE_OFFLINE:Ljava/lang/String;

.field public static KEY_SHARE_URL:Ljava/lang/String;

.field public static KEY_SYNC:Ljava/lang/String;

.field public static api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    const-string v0, "cloud_available"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_CLOUD_AVAILABLE:Ljava/lang/String;

    .line 118
    const-string v0, "download"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_DOWNLOAD:Ljava/lang/String;

    .line 119
    const-string v0, "exception"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_EXCEPTION:Ljava/lang/String;

    .line 120
    const-string v0, "get_streaming_URL"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_GETSTREAMING_URL:Ljava/lang/String;

    .line 121
    const-string v0, "get_thumbnail"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_GETTHUMBNAIL:Ljava/lang/String;

    .line 122
    const-string v0, "make_available_offline"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_MAKE_AVAILABLE_OFFLINE:Ljava/lang/String;

    .line 123
    const-string v0, "prefetch"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_PREFETCH:Ljava/lang/String;

    .line 124
    const-string v0, "revert_available_offline"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_REVERT_AVAILABLE_OFFLINE:Ljava/lang/String;

    .line 125
    const-string v0, "get_share_URL"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_SHARE_URL:Ljava/lang/String;

    .line 126
    const-string v0, "sync"

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->KEY_SYNC:Ljava/lang/String;

    .line 128
    new-instance v0, Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-direct {v0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;-><init>()V

    sput-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    .line 116
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 135
    invoke-static {p0}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->init(Landroid/content/Context;)V

    .line 136
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->download(Landroid/content/Context;Landroid/net/Uri;)V

    .line 137
    return-void
.end method

.method public static getImage(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 164
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getImage(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSharedURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 140
    const/4 v0, 0x0

    return-object v0
.end method

.method public static getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 144
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 148
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0, p1}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 131
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->init(Landroid/content/Context;)V

    .line 132
    return-void
.end method

.method public static isCloudAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 152
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->isCloudAvailable(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 157
    return-void
.end method

.method public static revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 161
    return-void
.end method

.method public static sync(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/skp/tcloud/service/lib/TcloudException;
        }
    .end annotation

    .prologue
    .line 168
    sget-object v0, Lcom/skp/tcloud/service/lib/TcloudStore$API;->api:Lcom/skp/tcloud/service/lib/TcloudAgentApi;

    invoke-virtual {v0, p0}, Lcom/skp/tcloud/service/lib/TcloudAgentApi;->sync(Landroid/content/Context;)V

    .line 169
    return-void
.end method
