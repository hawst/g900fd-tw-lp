.class public Lcom/skp/tcloud/service/lib/TcloudException;
.super Ljava/lang/Exception;
.source "TcloudException.java"


# static fields
.field public static final TCLOUD_AGENT_NOT_AVAILABLE:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private final code:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 11
    iput p1, p0, Lcom/skp/tcloud/service/lib/TcloudException;->code:I

    .line 12
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 16
    const-string v0, "Unknown Exception"

    .line 17
    .local v0, "message":Ljava/lang/String;
    iget v1, p0, Lcom/skp/tcloud/service/lib/TcloudException;->code:I

    packed-switch v1, :pswitch_data_0

    .line 22
    :goto_0
    return-object v0

    .line 19
    :pswitch_0
    const-string v0, "Tcloud Agent service is not available"

    goto :goto_0

    .line 17
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
