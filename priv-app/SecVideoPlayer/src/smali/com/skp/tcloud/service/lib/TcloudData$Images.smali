.class public Lcom/skp/tcloud/service/lib/TcloudData$Images;
.super Ljava/lang/Object;
.source "TcloudData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/skp/tcloud/service/lib/TcloudData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Images"
.end annotation


# instance fields
.field public bucketDisplayName:Ljava/lang/String;

.field public bucketId:Ljava/lang/String;

.field public cloudCachedPath:Ljava/lang/String;

.field public cloudIsCached:Z

.field public cloudIsDir:Z

.field public cloudLastViewed:Ljava/lang/String;

.field public cloudRevision:I

.field public cloudServerId:Ljava/lang/String;

.field public cloudThumbPath:Ljava/lang/String;

.field public contentId:Ljava/lang/String;

.field public data:Ljava/lang/String;

.field public dateAdded:J

.field public dateModified:J

.field public dateTaken:J

.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public height:I

.field public id:J

.field public isPrivate:Z

.field public latitude:D

.field public longitude:D

.field public mediaType:I

.field public mimeType:Ljava/lang/String;

.field public miniThumbData:Ljava/lang/String;

.field public miniThumbMagic:I

.field public orientation:I

.field public size:J

.field public title:Ljava/lang/String;

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->id:J

    .line 111
    iput v1, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->mediaType:I

    .line 112
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->data:Ljava/lang/String;

    .line 113
    const-string v0, "bucket_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->bucketDisplayName:Ljava/lang/String;

    .line 114
    const-string v0, "bucket_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->bucketId:Ljava/lang/String;

    .line 115
    const-string v0, "tcloud_cached_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudCachedPath:Ljava/lang/String;

    .line 116
    const-string v0, "tcloud_is_cached"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudIsCached:Z

    .line 117
    const-string v0, "tcloud_is_dir"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudIsDir:Z

    .line 118
    const-string v0, "tcloud_last_viewed"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudLastViewed:Ljava/lang/String;

    .line 119
    const-string v0, "tcloud_revision"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudRevision:I

    .line 120
    const-string v0, "tcloud_server_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudServerId:Ljava/lang/String;

    .line 121
    const-string v0, "tcloud_thumb_path"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->cloudThumbPath:Ljava/lang/String;

    .line 122
    const-string v0, "content_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->contentId:Ljava/lang/String;

    .line 123
    const-string v0, "date_added"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->dateAdded:J

    .line 124
    const-string v0, "date_modified"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->dateModified:J

    .line 125
    const-string v0, "datetaken"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->dateTaken:J

    .line 126
    const-string v0, "description"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->description:Ljava/lang/String;

    .line 127
    const-string v0, "_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->displayName:Ljava/lang/String;

    .line 128
    const-string v0, "height"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->height:I

    .line 129
    const-string v0, "isprivate"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->isPrivate:Z

    .line 130
    const-string v0, "latitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->latitude:D

    .line 131
    const-string v0, "longitude"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->longitude:D

    .line 132
    const-string v0, "mime_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->mimeType:Ljava/lang/String;

    .line 133
    const-string v0, "mini_thumb_magic"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->miniThumbMagic:I

    .line 134
    const-string v0, "orientation"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->orientation:I

    .line 135
    const-string v0, "_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->size:J

    .line 136
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->title:Ljava/lang/String;

    .line 137
    const-string v0, "width"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->width:I

    .line 138
    const-string v0, "mini_thumb_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/skp/tcloud/service/lib/TcloudData$Images;->miniThumbData:Ljava/lang/String;

    .line 139
    return-void

    :cond_0
    move v0, v2

    .line 116
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 117
    goto/16 :goto_1

    :cond_2
    move v1, v2

    .line 129
    goto :goto_2
.end method
