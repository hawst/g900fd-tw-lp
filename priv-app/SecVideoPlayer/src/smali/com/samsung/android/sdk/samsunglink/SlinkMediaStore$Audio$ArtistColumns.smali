.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$ArtistColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"

# interfaces
.implements Landroid/provider/MediaStore$Audio$ArtistColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$BaseSamsungLinkColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ArtistColumns"
.end annotation


# static fields
.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ARTIST_INDEX_CHAR:Ljava/lang/String; = "artist_index_char"

.field public static final NUMBER_OF_DUP_REDUCED_TRACKS:Ljava/lang/String; = "num_dup_reduced_tracks"
