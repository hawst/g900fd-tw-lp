.class public interface abstract Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AudioColumns;
.super Ljava/lang/Object;
.source "SlinkMediaStore.java"

# interfaces
.implements Landroid/provider/MediaStore$Audio$AudioColumns;
.implements Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$MediaColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioColumns"
.end annotation


# static fields
.field public static final ALBUM_INDEX_CHAR:Ljava/lang/String; = "album_index_char"

.field public static final ARTIST_INDEX_CHAR:Ljava/lang/String; = "artist_index_char"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final LOCAL_SOURCE_ALBUM_ID:Ljava/lang/String; = "local_source_album_id"

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"
