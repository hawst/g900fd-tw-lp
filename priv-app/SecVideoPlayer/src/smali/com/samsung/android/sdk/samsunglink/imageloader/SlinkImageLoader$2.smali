.class Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;
.super Ljava/lang/Object;
.source "SlinkImageLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->loadDeviceIcon(JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageListener;)Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

.field final synthetic val$deviceId:J

.field final synthetic val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

.field final synthetic val$size:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

.field final synthetic val$states:[I

.field final synthetic val$theme:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[ILcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    iput-wide p2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$deviceId:J

    iput-object p4, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$size:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    iput-object p5, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$theme:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    iput-object p6, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$states:[I

    iput-object p7, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$imageContainer:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$ImageContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 309
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_0

    .line 310
    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load device icon start for device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$deviceId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$200(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$deviceId:J

    iget-object v4, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$size:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    iget-object v5, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$theme:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    iget-object v6, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$states:[I

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDeviceIcon(Landroid/content/Context;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 335
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$600(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;

    invoke-direct {v2, p0, v0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$2;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 351
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v7

    .line 321
    .local v7, "e":Ljava/io/FileNotFoundException;
    sget-boolean v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore;->ENABLE_LOGGING:Z

    if-eqz v1, :cond_1

    .line 322
    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FileNotFoundException for device icon for device "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->val$deviceId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;->this$0:Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;

    # getter for: Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;->access$600(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2$1;-><init>(Lcom/samsung/android/sdk/samsunglink/imageloader/SlinkImageLoader$2;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
