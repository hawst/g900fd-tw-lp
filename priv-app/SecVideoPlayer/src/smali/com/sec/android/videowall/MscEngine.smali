.class public Lcom/sec/android/videowall/MscEngine;
.super Ljava/lang/Object;
.source "MscEngine.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "savsff"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 10
    const-string v0, "savsvc"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 11
    const-string v0, "savscmn"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    const-string v0, "sthmb"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 13
    const-string v0, "vwengine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 14
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native close()I
.end method

.method public native getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I
.end method

.method public native getDurationTime(Ljava/lang/String;Z)I
.end method

.method public native getKeyframeTime(Ljava/lang/String;I)I
.end method

.method public native initView(Landroid/graphics/Bitmap;)I
.end method

.method public native nativeGetProgressPercent()I
.end method

.method public native open(Z)I
.end method

.method public native render(Landroid/graphics/Bitmap;I)I
.end method

.method public native setKeyframeTime(Ljava/lang/String;I)I
.end method

.method public native setOption(I)I
.end method

.method public native setThumbnail(ILjava/lang/String;)I
.end method

.method public native stopTranscoding(I)I
.end method

.method public native texture(I)I
.end method

.method public native transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I
.end method

.method public native useHWDecoder(I)I
.end method
