.class public Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;
.super Landroid/widget/RelativeLayout;
.source "MiniVideoSubtitleView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MiniVideoSubtitleView"

.field public static final TEXT_SIZE_RATIO_MULTIPLIER:I = 0x12


# instance fields
.field private mContext:Landroid/content/Context;

.field private mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

.field private mSubtitleLayout:Landroid/widget/RelativeLayout;

.field public mSubtitleText:Landroid/widget/TextView;

.field private mSubtitleView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    .line 40
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->initView()V

    .line 42
    return-void
.end method

.method private initView()V
    .locals 4

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->removeAllViews()V

    .line 47
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 49
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 51
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->makeView()Landroid/view/View;

    move-result-object v1

    .line 52
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->addView(Landroid/view/View;)V

    .line 53
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    return-void
.end method

.method private makeView()Landroid/view/View;
    .locals 6

    .prologue
    const v4, 0x7f0d01a5

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v2, 0x7f030032

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    .line 59
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 63
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v2, :cond_0

    .line 64
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 65
    .local v1, "lSubtitle":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080077

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x5

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 67
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    .end local v1    # "lSubtitle":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    const v3, 0x7f0d01a6

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleView:Landroid/view/View;

    return-object v2
.end method


# virtual methods
.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 102
    return-void
.end method

.method public updateLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 155
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGOpacity()I

    move-result v2

    if-nez v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080178

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 164
    .local v0, "bottomMargin":I
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 165
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v4, v4, v4, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 166
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 167
    const-string v2, "MiniVideoSubtitleView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shiftLayout() updated bottomMargin:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-void

    .line 158
    .end local v0    # "bottomMargin":I
    .end local v1    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080177

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .restart local v0    # "bottomMargin":I
    goto :goto_0

    .line 161
    .end local v0    # "bottomMargin":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "bottomMargin":I
    goto :goto_0
.end method

.method public updateSubTitle(Ljava/lang/String;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 76
    const-string v3, "MiniVideoSubtitleView"

    const-string v4, "updateSubTitle"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 79
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    :goto_0
    return-void

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080286

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 82
    .local v2, "text_padding":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 83
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGColor()I

    move-result v0

    .line 84
    .local v0, "color":I
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 85
    .local v1, "textSpan":Landroid/text/Spannable;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 86
    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v3, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v1, v3, v6, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateSubTitleAlignment(I)V
    .locals 5
    .param p1, "alignment"    # I

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0x9

    .line 129
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getGravity()I

    move-result v2

    and-int/lit8 v1, v2, -0x8

    .line 130
    .local v1, "textGravity":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 132
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x1e

    if-ne p1, v2, :cond_1

    .line 133
    or-int/lit8 v1, v1, 0x3

    .line 134
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 135
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 146
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->refreshDrawableState()V

    .line 149
    return-void

    .line 136
    :cond_1
    const/16 v2, 0x1f

    if-ne p1, v2, :cond_2

    .line 137
    or-int/lit8 v1, v1, 0x1

    .line 138
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 139
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_0

    .line 140
    :cond_2
    const/16 v2, 0x20

    if-ne p1, v2, :cond_0

    .line 141
    or-int/lit8 v1, v1, 0x5

    .line 142
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 143
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_0
.end method

.method public updateSubtitleSettings()V
    .locals 5

    .prologue
    .line 105
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    if-nez v3, :cond_0

    .line 106
    const-string v3, "MiniVideoSubtitleView"

    const-string v4, "updateSubtitleSettings : mSubtitleText is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    .line 112
    .local v1, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinColor()I

    move-result v0

    .line 113
    .local v0, "color":I
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 114
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 116
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontColor()I

    move-result v0

    .line 117
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 120
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontEdge(Landroid/widget/TextView;)V

    .line 122
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    .line 123
    .local v2, "typeface":Landroid/graphics/Typeface;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 125
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getTextAlignment()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubTitleAlignment(I)V

    goto :goto_0
.end method

.method public updateSubtitleSize(I)V
    .locals 4
    .param p1, "size"    # I

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 95
    .local v0, "textSizeDelta":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v2, 0x1

    add-int v3, p1, v0

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 97
    .end local v0    # "textSizeDelta":I
    :cond_0
    return-void
.end method
