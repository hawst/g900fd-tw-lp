.class public Lcom/sec/android/app/videoplayer/videowall/ChapterView;
.super Landroid/widget/ImageView;
.source "ChapterView.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "videowall-ChapterView"


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;

.field private count:J

.field private delayTime:J

.field private fps:I

.field private ht:I

.field private image:Landroid/graphics/Bitmap;

.field public mcontext:Landroid/content/Context;

.field private movieindex:I

.field private nextFrame:Z

.field private nowTime:J

.field private prebitmap:Landroid/graphics/Bitmap;

.field private pretime:J

.field private ptX:I

.field private ptY:I

.field private setImage:Z

.field private setVideo:Z

.field private vcount:J

.field private wd:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    .line 19
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->prebitmap:Landroid/graphics/Bitmap;

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nextFrame:Z

    .line 23
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVideo:Z

    .line 24
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setImage:Z

    .line 25
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->count:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->vcount:J

    .line 26
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nowTime:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->delayTime:J

    .line 27
    iput v1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->movieindex:I

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->mcontext:Landroid/content/Context;

    .line 35
    return-void
.end method


# virtual methods
.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 48
    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/16 v10, 0x1

    const/16 v8, 0x3e8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 84
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 86
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setImage:Z

    if-eqz v2, :cond_6

    .line 87
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->count:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 88
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->wd:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptX:I

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ht:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptY:I

    .line 93
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVideo:Z

    if-eqz v2, :cond_b

    .line 94
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nextFrame:Z

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWPLAY:Z

    if-eqz v2, :cond_2

    .line 95
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->prebitmap:Landroid/graphics/Bitmap;

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->movieindex:I

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_render(Landroid/graphics/Bitmap;I)I

    .line 99
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->vcount:J

    add-long/2addr v2, v10

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->vcount:J

    .line 102
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->delayTime:J

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-lez v2, :cond_3

    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    .line 105
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nowTime:J

    .line 106
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nowTime:J

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->delayTime:J

    .line 108
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->vcount:J

    const-wide/16 v4, 0x3

    cmp-long v2, v2, v4

    if-lez v2, :cond_9

    .line 109
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->delayTime:J

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->fps:I

    div-int v4, v8, v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_7

    .line 110
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->prebitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->prebitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_4

    .line 111
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->prebitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 113
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nextFrame:Z

    .line 134
    :cond_5
    :goto_0
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->count:J

    add-long/2addr v2, v10

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->count:J

    .line 136
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->invalidate()V

    .line 137
    return-void

    .line 115
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_8

    .line 116
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 118
    :cond_8
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->delayTime:J

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->fps:I

    div-int v4, v8, v4

    int-to-long v4, v4

    sub-long v0, v2, v4

    .line 119
    .local v0, "tmp":J
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nextFrame:Z

    .line 120
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    goto :goto_0

    .line 123
    .end local v0    # "tmp":J
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_a

    .line 124
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 126
    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->pretime:J

    .line 127
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->nextFrame:Z

    goto :goto_0

    .line 130
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_5

    .line 131
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public pauseView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVideo:Z

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setImage:Z

    .line 80
    return-void
.end method

.method public setBitmapImage(Landroid/graphics/Bitmap;IIII)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "index"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "framerate"    # I

    .prologue
    .line 67
    const-string v0, "videowall-ChapterView"

    const-string v1, "--> setBitmapImage()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iput p2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->movieindex:I

    .line 69
    iput p5, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->fps:I

    .line 70
    iput p3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->wd:I

    .line 71
    iput p4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ht:I

    .line 72
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setImage:Z

    .line 74
    return-void
.end method

.method public setBitmapView(IIII)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "framerate"    # I

    .prologue
    const/4 v3, 0x1

    .line 52
    const-string v0, "videowall-ChapterView"

    const-string v1, "--> setBitmapView()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->movieindex:I

    .line 54
    iput p4, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->fps:I

    .line 55
    iput p2, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->wd:I

    .line 56
    iput p3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ht:I

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->wd:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ht:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->bitmap:Landroid/graphics/Bitmap;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->wd:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->ht:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->image:Landroid/graphics/Bitmap;

    .line 61
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVideo:Z

    .line 62
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setImage:Z

    .line 63
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    return-void
.end method
