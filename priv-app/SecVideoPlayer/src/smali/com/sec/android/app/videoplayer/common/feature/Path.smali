.class public Lcom/sec/android/app/videoplayer/common/feature/Path;
.super Ljava/lang/Object;
.source "Path.java"


# static fields
.field public static final CAPTURE_INTERNAL:Ljava/lang/String;

.field public static final CLOUD:Ljava/lang/String; = "content://cloud/"

.field public static final CLOUD_INVALID:Ljava/lang/String; = "content://cloud/data/video/media/-1"

.field public static final DEFAULT_WIDGET_VIDEO:Ljava/lang/String;

.field public static final EXTERNAL_ICS_ROOT_PATH:Ljava/lang/String; = "/mnt/extSdCard"

.field public static final EXTERNAL_ROOT_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field public static final FILE:Ljava/lang/String; = "file://"

.field public static final HELP_VIDEO:Ljava/lang/String; = "file:///system/media/video/video_help.mp4"

.field public static final HIDE_PATH:Ljava/lang/String;

.field public static final INTERNAL_ICS_ROOT_PATH:Ljava/lang/String; = "/mnt/sdcard"

.field public static final INTERNAL_JB_ROOT_PATH:Ljava/lang/String; = "/storage/sdcard0"

.field public static final INTERNAL_ROOT_PATH:Ljava/lang/String;

.field public static final MEDIAHUB:Ljava/lang/String;

.field public static final MEDIAHUB_DOWNLOAD:Ljava/lang/String;

.field public static final MEDIAHUB_EXTERNAL:Ljava/lang/String; = "/storage/extSdCard/.samsungmediahub"

.field public static final MEDIAHUB_EXTERNAL_FILE:Ljava/lang/String; = "file:///storage/extSdCard/.samsungmediahub"

.field public static final MEDIAHUB_FILE:Ljava/lang/String;

.field public static final MMS_CONTENT_URI:Ljava/lang/String; = "content://mms/part"

.field public static final MNT:Ljava/lang/String; = "/mnt"

.field public static final PCLOUD:Ljava/lang/String; = "content://com.sec.pcw/"

.field public static final RES:Ljava/lang/String; = "android.resource://"

.field public static final RES_SCHEME:Ljava/lang/String; = "android.resource"

.field public static final SECURITY_MMS_CONTENT_URI:Ljava/lang/String; = "content://security_mms/part"

.field public static final SKTCLOUD:Ljava/lang/String; = "content://com.skp.tcloud/"

.field public static final STORAGE:Ljava/lang/String; = "/storage"

.field public static final STORAGE_ICS_USB_DRIVE:Ljava/lang/String; = "/mnt/UsbDrive"

.field public static final STORAGE_USB_DRIVE:Ljava/lang/String; = "/storage/UsbDrive"

.field public static final SUBTITLE_PATH:Ljava/lang/String;

.field public static final SYSTEM:Ljava/lang/String; = "/system/"

.field public static final VIDEOHUB:Ljava/lang/String;

.field public static final VIDEOHUB_DOWNLOAD:Ljava/lang/String;

.field public static final VIDEOHUB_EXTERNAL:Ljava/lang/String; = "/storage/extSdCard/.samsungvideohub"

.field public static final VIDEOHUB_EXTERNAL_FILE:Ljava/lang/String; = "file:///storage/extSdCard/.samsungvideohub"

.field public static final VIDEOHUB_FILE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 9
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.SubTitle"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->SUBTITLE_PATH:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Pictures/Screenshots"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->CAPTURE_INTERNAL:Ljava/lang/String;

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.samsungmediahub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB_FILE:Ljava/lang/String;

    .line 33
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Download/.SamsungMediaHub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB_DOWNLOAD:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.samsungvideohub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB_FILE:Ljava/lang/String;

    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Download/.SamsungVideoHub"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB_DOWNLOAD:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Video/Wonder of Nature.mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->DEFAULT_WIDGET_VIDEO:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.hide"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->HIDE_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ReplaceOldPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 62
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-object v2

    .line 65
    :cond_1
    const-string v0, ""

    .line 66
    .local v0, "newPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 68
    .local v1, "pos":I
    const-string v3, "/storage/sdcard0"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 69
    const-string v2, "/storage/sdcard0"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 70
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    .line 84
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 71
    :cond_2
    const-string v3, "/mnt/sdcard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 72
    const-string v2, "/mnt/sdcard"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 73
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    goto :goto_1

    .line 74
    :cond_3
    const-string v3, "/mnt/extSdCard"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 75
    const-string v2, "/mnt/extSdCard"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 76
    const-string v0, "/storage/extSdCard"

    goto :goto_1

    .line 77
    :cond_4
    const-string v3, "/mnt/UsbDrive"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    const-string v2, "/mnt/UsbDrive"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    .line 79
    const-string v0, "/storage/UsbDrive"

    goto :goto_1
.end method
