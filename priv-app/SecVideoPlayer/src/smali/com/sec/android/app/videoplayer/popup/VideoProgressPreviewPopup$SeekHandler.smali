.class Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;
.super Landroid/os/Handler;
.source "VideoProgressPreviewPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeekHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 186
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SeekHandler msg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 189
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)I

    move-result v1

    .line 191
    .local v1, "position":I
    if-gtz v1, :cond_1

    .line 192
    const/4 v1, 0x0

    .line 194
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 196
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->pause()V

    .line 199
    :cond_2
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "seek() position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
