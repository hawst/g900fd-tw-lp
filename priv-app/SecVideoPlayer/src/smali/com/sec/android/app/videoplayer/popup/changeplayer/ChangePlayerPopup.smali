.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
.implements Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerPopup;


# static fields
.field private static final REFRESH_INTERVAL:I = 0x3e8

.field private static final REFRESH_TIMEOUT:I = 0x2710

.field private static final SCAN_MODE_ALL:I = 0x1

.field private static final SCAN_MODE_DLNA_ONLY:I = 0x2

.field private static final SCAN_MODE_NONE:I = 0x0

.field private static final SCAN_MODE_WFD_ONLY:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static final WIFI_TURN_ON_DELAY:I = 0x1f4

.field private static sChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

.field private static sScanMode:[Ljava/lang/String;


# instance fields
.field private bConnectingScreenMirroring:Z

.field private mAsfDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

.field private mChangePlayerListener:Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

.field private mCheckDeviceIndex:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectDeviceName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDeviceLayout:Landroid/widget/LinearLayout;

.field private mDeviceType:I

.field private mDialog:Landroid/app/AlertDialog;

.field private mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private final mHandler:Landroid/os/Handler;

.field private mPlayerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;",
            ">;"
        }
    .end annotation
.end field

.field private mRefreshDevice:Ljava/lang/Runnable;

.field private mRefreshProgressBar:Landroid/widget/ProgressBar;

.field private mRefreshingCheckTime:I

.field private mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

.field private mScanMode:I

.field private mSelectedAsfDeviceIndex:I

.field private mSelectedPlayer:I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field protected sDeviceTypeImages:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 51
    const-class v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    .line 107
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SCAN_MODE_NONE"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "SCAN_MODE_ALL(DLNA+WFD)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SCAN_MODE_DLNA_ONLY"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SCAN_MODE_WFD_ONLY"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sScanMode:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedAsfDeviceIndex:I

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceLayout:Landroid/widget/LinearLayout;

    .line 111
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    .line 126
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    .line 128
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z

    .line 717
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 777
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;

    .line 805
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$8;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    .line 132
    return-void

    .line 111
    :array_0
    .array-data 4
        0x7f02007b
        0x7f02007e
        0x7f020081
        0x7f02007a
        0x7f020082
        0x7f020080
        0x7f02007c
        0x7f02007f
        0x7f02007d
        0x7f020084
        0x7f020079
        0x7f020083
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I

    return p1
.end method

.method static synthetic access$1420(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeChangePlayerList()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->showDevicePopup()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->dismissPopup()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->disableAllTogether()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .param p1, "x1"    # I

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->handleChangePlayer(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerListener:Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private availableDLNAdisplay()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 740
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 743
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private availableWifidisplay()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 747
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 750
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkConstraint()V
    .locals 7

    .prologue
    const/16 v6, 0x12c

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->checkExceptionalCase()I

    move-result v0

    .line 240
    .local v0, "exceptionalCase":I
    sget-object v3, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DisplayManager.checkExceptionalCase() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    .line 243
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 279
    :goto_0
    return-void

    .line 247
    :cond_1
    const/4 v1, 0x0

    .line 249
    .local v1, "keyString":Ljava/lang/String;
    packed-switch v0, :pswitch_data_0

    .line 269
    :goto_1
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v1, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v2

    .line 271
    .local v2, "show":Z
    if-eqz v2, :cond_2

    .line 272
    sget-object v3, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkConstraint() : show for exceptionalCase: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v4, v5, v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->setContext(Landroid/content/Context;Landroid/os/Handler;I)V

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->show()V

    goto :goto_0

    .line 251
    .end local v2    # "show":Z
    :pswitch_1
    const-string v1, "showscreenmirroringguildepopup_hotspot"

    .line 252
    goto :goto_1

    .line 254
    :pswitch_2
    const-string v1, "showscreenmirroringguildepopup_wifidirect"

    .line 255
    goto :goto_1

    .line 257
    :pswitch_3
    const-string v1, "showscreenmirroringguildepopup_sidesync_running"

    .line 258
    goto :goto_1

    .line 260
    :pswitch_4
    const-string v1, "showscreenmirroringguildepopup_power_saving_mode"

    .line 261
    goto :goto_1

    .line 263
    :pswitch_5
    const-string v1, "showscreenmirroringguildepopup_limited_contents"

    .line 264
    goto :goto_1

    .line 277
    .restart local v2    # "show":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 249
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private checkSupportDlna()Z
    .locals 2

    .prologue
    .line 768
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    .line 770
    .local v0, "support":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isMovieStoreContent()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isMMSContent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 771
    :cond_0
    const/4 v0, 0x0

    .line 774
    :cond_1
    return v0
.end method

.method private disableAllTogether()V
    .locals 4

    .prologue
    .line 853
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_0

    .line 854
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiIBSSEnabled(Z)Z

    .line 856
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 862
    :cond_0
    return-void

    .line 857
    :catch_0
    move-exception v0

    .line 858
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method

.method private dismissPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 846
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    sget-object v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 849
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 850
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 138
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object v0
.end method

.method private getScanMode()I
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x0

    .line 755
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->checkExceptionalCase()I

    move-result v2

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IsNotAllowedScreenMirroring()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 757
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->checkSupportDlna()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 763
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 757
    goto :goto_0

    .line 760
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v2, :cond_5

    .line 761
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->checkSupportDlna()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    goto :goto_0

    .line 763
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->checkSupportDlna()Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method private handleChangePlayer(I)V
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 596
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v1, "handleChangePlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSelectedbyChangePlayer(Z)V

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 601
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->selectMyDevice()V

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 602
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 603
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->selectAsfDevice(I)V

    goto :goto_0

    .line 604
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    .line 605
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->selectWfdDevice(I)V

    goto :goto_0
.end method

.method private initChangePlayerList()V
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 445
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    .line 450
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    .line 451
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    .line 456
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getPlayerList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    .line 458
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initChangePlayerList. AsfDeviceCount : ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    :cond_0
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method private makeChangePlayerList()V
    .locals 0

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->initChangePlayerList()V

    .line 438
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeSelectedDeviceList()V

    .line 439
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeDlnaDeviceList()V

    .line 440
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeWfdDeviceList()V

    .line 441
    return-void
.end method

.method private makeDlnaDeviceList()V
    .locals 8

    .prologue
    const/16 v7, 0xb

    .line 521
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 522
    const/4 v1, 0x0

    .line 523
    .local v1, "dmrName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 525
    .local v0, "asfDevice":Lcom/samsung/android/allshare/Device;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 527
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Device;>;"
    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeChangePlayerList(asf). mAsfDeviceList size : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 529
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 531
    .restart local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    iget v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    if-ne v4, v7, :cond_2

    .line 532
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v2

    .line 533
    .local v2, "id":Ljava/lang/String;
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 534
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v1

    .line 535
    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeChangePlayerList(asf). getName : ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v5, v1, v6, v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/net/Uri;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 532
    .end local v2    # "id":Ljava/lang/String;
    :cond_1
    const-string v2, ""

    goto :goto_1

    .line 540
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v1

    .line 541
    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "makeChangePlayerList(asf). deviceName : ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v6

    invoke-direct {v5, v1, v6, v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/net/Uri;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 547
    .end local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    .end local v1    # "dmrName":Ljava/lang/String;
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/Device;>;"
    :cond_3
    return-void
.end method

.method private makeSelectedDeviceList()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v11, 0x0

    const/16 v10, 0xc

    const/16 v9, 0x9

    const/16 v12, 0xa

    .line 463
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    const v7, 0x7f0a00ce

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 466
    .local v3, "mydeviceName":Ljava/lang/String;
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v6, :cond_0

    .line 467
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    const/16 v8, 0xb

    aget v7, v7, v8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 472
    .local v2, "mydeviceIcon":Landroid/graphics/drawable/Drawable;
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 473
    const/16 v6, 0xb

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    .line 475
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfDeviceList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 476
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    .line 477
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getSelectedPlayer()Lcom/samsung/android/allshare/Device;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v1

    .line 478
    .local v1, "iconUri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    invoke-direct {v7, v8, v1, v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/net/Uri;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    .end local v1    # "iconUri":Landroid/net/Uri;
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-direct {v7, v3, v2, v12}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :goto_2
    return-void

    .line 469
    .end local v2    # "mydeviceIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    aget v7, v7, v9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .restart local v2    # "mydeviceIcon":Landroid/graphics/drawable/Drawable;
    goto :goto_0

    .line 480
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    const/4 v11, 0x6

    aget v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    invoke-direct {v7, v8, v9, v10}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 486
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnected()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 487
    iput v10, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    .line 488
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getConnectedName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    .line 490
    const/4 v4, 0x0

    .line 491
    .local v4, "tokens":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 492
    .local v5, "wfdIcon":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getConnectedDisplayInfo()Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "displayInfo":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 494
    sget-object v6, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "makeSelectedDeviceList. getConnectedDisplayInfo : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v6, "-"

    invoke-virtual {v0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 498
    :cond_3
    if-eqz v4, :cond_4

    array-length v6, v4

    if-lt v6, v13, :cond_4

    aget-object v6, v4, v11

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 499
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    aget v7, v7, v9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 509
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    invoke-direct {v7, v8, v5, v10}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 510
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-direct {v7, v3, v2, v12}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 512
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 501
    :cond_5
    aget-object v6, v4, v11

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    .line 502
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    if-lt v6, v13, :cond_6

    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    if-ge v6, v10, :cond_6

    .line 503
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_3

    .line 505
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    aget v7, v7, v9

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_3

    .line 514
    .end local v0    # "displayInfo":Ljava/lang/String;
    .end local v4    # "tokens":[Ljava/lang/String;
    .end local v5    # "wfdIcon":Landroid/graphics/drawable/Drawable;
    :cond_7
    iput v12, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    .line 515
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-direct {v7, v3, v2, v12}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2
.end method

.method private makeWfdDeviceList()V
    .locals 14

    .prologue
    const/16 v13, 0x9

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/16 v10, 0xc

    .line 550
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 551
    const/4 v4, 0x0

    .line 552
    .local v4, "tokens":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 553
    .local v5, "wfdIcon":Landroid/graphics/drawable/Drawable;
    const/4 v6, 0x0

    .line 554
    .local v6, "wfdName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 555
    .local v1, "deviceType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 557
    .local v2, "display":Landroid/os/Parcelable;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v7, v11}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getAvailableDisplays(Z)Ljava/util/List;

    move-result-object v0

    .line 558
    .local v0, "availableDisplays":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/os/Parcelable;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 560
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Parcelable;>;"
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 561
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "display":Landroid/os/Parcelable;
    check-cast v2, Landroid/os/Parcelable;

    .line 563
    .restart local v2    # "display":Landroid/os/Parcelable;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v7, v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getDeviceName(Landroid/os/Parcelable;)Ljava/lang/String;

    move-result-object v6

    .line 564
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "makeWfdDeviceList. deviceName : ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedPlayer:I

    if-ne v7, v10, :cond_1

    if-eqz v6, :cond_1

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 570
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v7, v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getPrimaryDeviceType(Landroid/os/Parcelable;)Ljava/lang/String;

    move-result-object v1

    .line 572
    if-eqz v1, :cond_2

    .line 573
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "makeWfdDeviceList. primaryDeviceType : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    const-string v7, "-"

    invoke-virtual {v1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 577
    :cond_2
    if-eqz v1, :cond_3

    if-eqz v4, :cond_3

    array-length v7, v4

    if-lt v7, v11, :cond_3

    aget-object v7, v4, v12

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 578
    :cond_3
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v8, "makeWfdDeviceList. Malformed primaryDeviceType"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    aget v8, v8, v13

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 589
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    new-instance v8, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-direct {v8, v6, v5, v10}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mCheckDeviceIndex:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 581
    :cond_4
    aget-object v7, v4, v12

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    .line 582
    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    if-lt v7, v11, :cond_5

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    if-ge v7, v10, :cond_5

    .line 583
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceType:I

    add-int/lit8 v9, v9, -0x1

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_1

    .line 585
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sDeviceTypeImages:[I

    aget v8, v8, v13

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_1

    .line 593
    .end local v0    # "availableDisplays":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/os/Parcelable;>;"
    .end local v1    # "deviceType":Ljava/lang/String;
    .end local v2    # "display":Landroid/os/Parcelable;
    .end local v3    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/os/Parcelable;>;"
    .end local v4    # "tokens":[Ljava/lang/String;
    .end local v5    # "wfdIcon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "wfdName":Ljava/lang/String;
    :cond_6
    return-void
.end method

.method private selectAsfDevice(I)V
    .locals 12
    .param p1, "which"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 628
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v8, "handleChangePlayer. ASF_DEVICE"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnected()Z

    move-result v3

    .line 632
    .local v3, "bWfdConnected":Z
    if-eqz v3, :cond_0

    .line 633
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 634
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->forceTerminateWfd()V

    .line 637
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v7, :cond_2

    .line 638
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getPlayerList()Ljava/util/List;

    move-result-object v1

    .line 639
    .local v1, "asfDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Device;>;"
    const/4 v7, -0x1

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedAsfDeviceIndex:I

    .line 640
    const-string v8, "%s"

    new-array v9, v11, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    .line 641
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v8, "selectAsfDevice."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    sget-object v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "selectAsfDevice. mConnectDeviceName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 644
    const/4 v5, 0x0

    .line 645
    .local v5, "i":I
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 646
    .local v0, "asfDevice":Lcom/samsung/android/allshare/Device;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 647
    iput v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedAsfDeviceIndex:I

    .line 653
    .end local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    :cond_1
    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedAsfDeviceIndex:I

    if-gez v7, :cond_4

    .line 654
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v7, v10}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setPlayerState(I)V

    .line 655
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const-wide/16 v8, 0x0

    invoke-virtual {v7, v10, v8, v9, v10}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    .line 673
    .end local v1    # "asfDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Device;>;"
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    return-void

    .line 650
    .restart local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    .restart local v1    # "asfDeviceList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/Device;>;"
    .restart local v5    # "i":I
    .restart local v6    # "i$":Ljava/util/Iterator;
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 651
    goto :goto_0

    .line 657
    .end local v0    # "asfDevice":Lcom/samsung/android/allshare/Device;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    .line 659
    .local v2, "bDlnaConnected":Z
    const/4 v4, 0x0

    .line 660
    .local v4, "curPosition":I
    if-eqz v2, :cond_6

    .line 661
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getCurrentPosition()I

    move-result v4

    .line 662
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->release()V

    .line 669
    :cond_5
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mSelectedAsfDeviceIndex:I

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setPlayer(I)V

    .line 670
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    int-to-long v8, v4

    invoke-virtual {v7, v11, v8, v9, v11}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    goto :goto_1

    .line 664
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v7, :cond_5

    .line 665
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v4

    .line 666
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    goto :goto_2
.end method

.method private selectMyDevice()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 610
    sget-object v2, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v3, "handleChangePlayer. MY_DEVICE"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnected()Z

    move-result v1

    .line 613
    .local v1, "bWfdConnected":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    .line 615
    .local v0, "bDlnaConnected":Z
    if-eqz v0, :cond_1

    .line 616
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v6, v4, v5, v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    if-eqz v1, :cond_0

    .line 618
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseOrStopPlaying()V

    .line 619
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 620
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->forceTerminateWfd()V

    .line 621
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v2, :cond_0

    .line 622
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    const/16 v3, 0x320

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->notifyChanged(II)V

    goto :goto_0
.end method

.method private selectWfdDevice(I)V
    .locals 9
    .param p1, "which"    # I

    .prologue
    const/4 v8, 0x0

    .line 676
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v6, "handleChangePlayer. WFD_DEVICE"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    .line 679
    .local v1, "bDlnaConnected":Z
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 681
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isVideoList()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 682
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v4

    .line 683
    .local v4, "videoDB":Lcom/sec/android/app/videoplayer/db/VideoDB;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 688
    .end local v4    # "videoDB":Lcom/sec/android/app/videoplayer/db/VideoDB;
    :goto_0
    if-eqz v1, :cond_0

    .line 689
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 690
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const/16 v6, 0x2ea

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->notify(II)V

    .line 691
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->release()V

    .line 694
    :cond_0
    const-string v6, "%s"

    const/4 v5, 0x1

    new-array v7, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    .line 695
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v6, "selectWfdDevice."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    sget-object v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "selectWfdDevice. mConnectDeviceName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mConnectDeviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v5, v8}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getAvailableDisplays(Z)Ljava/util/List;

    move-result-object v0

    .line 700
    .local v0, "availableDisplays":Ljava/util/List;, "Ljava/util/List<Landroid/os/Parcelable;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Parcelable;

    .line 701
    .local v2, "d":Landroid/os/Parcelable;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v6, v2}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getDeviceName(Landroid/os/Parcelable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 702
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->startWifiDisplay(Landroid/os/Parcelable;)V

    .line 706
    .end local v2    # "d":Landroid/os/Parcelable;
    :cond_2
    return-void

    .line 685
    .end local v0    # "availableDisplays":Ljava/util/List;, "Ljava/util/List<Landroid/os/Parcelable;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    goto/16 :goto_0
.end method

.method private showDevicePopup()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 282
    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v5, "showDevicePopup. E"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getScanMode()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    .line 286
    sget-object v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "showDevicePopup. mSearchMode : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sScanMode:[Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanMode:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    const/4 v4, 0x1

    sput-boolean v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->UPDATE:Z

    .line 289
    sput-boolean v8, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->STARTED:Z

    .line 291
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 292
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->prepare()V

    .line 295
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 296
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->updatePlayerList()V

    .line 299
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 301
    .local v1, "dialog":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 303
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03001d

    invoke-virtual {v2, v4, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 304
    .local v0, "bodyLayout":Landroid/view/View;
    const v4, 0x7f0d0123

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 306
    .local v3, "mDeviceListView":Landroid/widget/ListView;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceLayout:Landroid/widget/LinearLayout;

    if-eqz v4, :cond_2

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDeviceLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 310
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a0184

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 311
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 313
    const v4, 0x7f0d0122

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    .line 314
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 316
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeChangePlayerList()V

    .line 318
    new-instance v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 321
    new-instance v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 341
    const v4, 0x7f0a0101

    invoke-virtual {v1, v4, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 342
    const v4, 0x7f0a0026

    new-instance v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 349
    new-instance v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 388
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    .line 390
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    new-instance v5, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 425
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 426
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->show()V

    .line 430
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v4, :cond_3

    .line 431
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    iput-object v5, v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 433
    :cond_3
    return-void
.end method

.method private startWifiDisplay(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "dev"    # Landroid/os/Parcelable;

    .prologue
    .line 709
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v1, "startWifiDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->connect(Landroid/os/Parcelable;)V

    .line 713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z

    .line 714
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 715
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 168
    :cond_0
    return-void
.end method

.method public createDevicePopup()V
    .locals 2

    .prologue
    .line 201
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v1, "createDevicePopup. E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    sget-object v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 209
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->checkConstraint()V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mScanGuildeDialog:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->dismiss()V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 162
    :cond_1
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 180
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 233
    return-void
.end method

.method public setContext(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->getInstance(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerListener:Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

    .line 145
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 146
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 147
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 150
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->sChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object v0
.end method

.method public show()V
    .locals 3

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 185
    sget-object v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v2, "show. but already shows"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 190
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_2

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 195
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;

    const-string v2, "show. but activity is not visible state"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateChangePlayerList()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mPlayerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    if-eqz v0, :cond_0

    .line 214
    sget-boolean v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->UPDATE:Z

    if-nez v0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeChangePlayerList()V

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
