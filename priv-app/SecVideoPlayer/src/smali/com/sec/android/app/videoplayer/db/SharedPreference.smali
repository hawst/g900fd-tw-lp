.class public Lcom/sec/android/app/videoplayer/db/SharedPreference;
.super Ljava/lang/Object;
.source "SharedPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/db/SharedPreference$VERSION_CODES;
    }
.end annotation


# static fields
.field private static final APPPREFS:Ljava/lang/String; = "P1SharedPreferences"

.field public static final AUTO_PLAY:Ljava/lang/String; = "autoPlay"

.field public static final JBP_MINI_CONTROLLER:Ljava/lang/String; = "miniController"

.field public static final LAST_PLAYED_ITEM:Ljava/lang/String; = "lastPlayedItem"

.field public static final LAST_PLAYED_ITEM_OF_AIA:Ljava/lang/String; = "lastPlayedItemOfAIA"

.field public static final L_SCREEN_CONTROLLER_POS:Ljava/lang/String; = "l_screen_controller_pos"

.field public static final MINI_CONTROLLER_ON:Ljava/lang/String; = "mini_controller_on"

.field public static final MINI_CONTROLLER_RIGHT:Ljava/lang/String; = "mini_controller_right"

.field public static final MOVIE_SCREEN_MODE:Ljava/lang/String; = "screen_mode_settings"

.field public static final NEWLY_LAUNCHED:Ljava/lang/String; = "newlyLaunched"

.field private static final OS_VERSION_CODE:Ljava/lang/String; = "version_code"

.field public static final SCREEN_MODE:Ljava/lang/String; = "screen_mode"

.field public static final SCS_CONFIG_STRING:Ljava/lang/String; = "scs_config_string"

.field public static final SHOW_ALLSHARECAST_POPUP:Ljava/lang/String; = "showallsharecastpopup"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_GROUP_PLAY_RUNNING:Ljava/lang/String; = "showscreenmirroringguildepopup_groupplay_running"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_HOTSPOT:Ljava/lang/String; = "showscreenmirroringguildepopup_hotspot"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_LIMITED_CONTENTS:Ljava/lang/String; = "showscreenmirroringguildepopup_limited_contents"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_POWER_SAVING_MODE_ON:Ljava/lang/String; = "showscreenmirroringguildepopup_power_saving_mode"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_SIDE_SYNC_RUNNING:Ljava/lang/String; = "showscreenmirroringguildepopup_sidesync_running"

.field public static final SHOW_SCREENMIRRORING_GUIDE_POPUP_WIFI_DIRECT:Ljava/lang/String; = "showscreenmirroringguildepopup_wifidirect"

.field public static final SHOW_WIFI_POPUP_CHANGEPLAYER:Ljava/lang/String; = "showwifipopup_changeplayer"

.field public static final SORT_ORDER:Ljava/lang/String; = "sortorder"

.field public static final SOUND_EFFECT:Ljava/lang/String; = "sound_effect"

.field public static final SUBTITLE_CUSTOMMODE:Ljava/lang/String; = "subtitle_smpte_custom"

.field public static final SUBTITLE_FONT:Ljava/lang/String; = "subtitle_font"

.field public static final SUBTITLE_FONTBGCOLOR:Ljava/lang/String; = "subtitle_font_backgroundcolor"

.field public static final SUBTITLE_FONTBGOPACITY:Ljava/lang/String; = "subtitle_font_background_opacity"

.field public static final SUBTITLE_FONTCOLOR:Ljava/lang/String; = "subtitle_font_color"

.field public static final SUBTITLE_FONTOPACITY:Ljava/lang/String; = "subtitle_font_opacity"

.field public static final SUBTITLE_FONTPACKAGE:Ljava/lang/String; = "subtitle_font_pakcagename"

.field public static final SUBTITLE_FONTSTRING:Ljava/lang/String; = "subtitle_font_string"

.field public static final SUBTITLE_FONT_EDGE:Ljava/lang/String; = "subtitle_font_edge"

.field public static final SUBTITLE_SIZE:Ljava/lang/String; = "subtitle_size"

.field public static final SUBTITLE_TEXT_ALIGNMENT:Ljava/lang/String; = "subtitle_text_alignment"

.field public static final SUBTITLE_WINDOWCOLOR:Ljava/lang/String; = "subtitle_window_color"

.field public static final SUBTITLE_WINDOWOPACITY:Ljava/lang/String; = "subtitle_window_opacity"

.field private static final TAG:Ljava/lang/String; = "SharedPreferenceManager"

.field public static final TAG_BUDDY_VISIBILITY:Ljava/lang/String; = "showsswitchstate_contextual"

.field public static final TEXT_DATE_STATUS:Ljava/lang/String; = "showscontextualdatestatus"

.field public static final TEXT_LOCATION_STATUS:Ljava/lang/String; = "showscontextuallocationstatus"

.field public static final VIDEO_CAPTURE:Ljava/lang/String; = "videoCapture"

.field public static final VIDEO_PLAY_SPEED_MODE:Ljava/lang/String; = "playspeed"

.field public static final VIEW_BY_OPTIONS_IN_CLOUD:Ljava/lang/String; = "list_view_by_cloud"

.field private static mContext:Landroid/content/Context;

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/db/SharedPreference;


# instance fields
.field private final mMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mMode:I

    .line 83
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Lcom/sec/android/app/videoplayer/db/SharedPreference;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 89
    :cond_0
    sput-object p0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    .line 90
    sget-object v0, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mUniqueInstance:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-object v0
.end method

.method private getVersion()I
    .locals 2

    .prologue
    .line 211
    const-string v0, "version_code"

    const/16 v1, 0x10

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private isOldVersion()Z
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getVersion()I

    move-result v0

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateLastPlayedItem()V
    .locals 5

    .prologue
    .line 253
    const-string v2, "lastPlayedItem"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 254
    .local v1, "lastPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/feature/Path;->ReplaceOldPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 255
    .local v0, "checkOldOSPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 256
    move-object v1, v0

    .line 257
    const-string v2, "SharedPreferenceManager"

    const-string v3, "updateLastPlayedItem() Path is from OLD OS! it is now change to API#:19 format"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    const-string v2, "SharedPreferenceManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateLastPlayedItem() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const-string v2, "lastPlayedItem"

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    return-void
.end method

.method private updateMiniModeSettings()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v2, 0x1

    .line 265
    const-string v1, "miniController"

    invoke-virtual {p0, v1, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 266
    .local v0, "mode":I
    if-ne v0, v4, :cond_0

    .line 271
    :goto_0
    return-void

    .line 269
    :cond_0
    const-string v4, "mini_controller_on"

    if-lez v0, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 270
    const-string v1, "mini_controller_right"

    if-le v0, v2, :cond_2

    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v1, v3

    .line 269
    goto :goto_1

    :cond_2
    move v2, v3

    .line 270
    goto :goto_2
.end method

.method private updateScreenMode()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 221
    const-string v1, "screen_mode"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 222
    .local v0, "scrMode":I
    if-eq v0, v2, :cond_0

    .line 223
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getVersion()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 227
    :pswitch_0
    packed-switch v0, :pswitch_data_1

    .line 242
    :goto_1
    const-string v1, "SharedPreferenceManager"

    const-string v2, "updateScreenMode Change it to API#19 setting value."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const-string v1, "screen_mode"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 244
    sget-object v1, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setScreenMode(I)V

    goto :goto_0

    .line 230
    :pswitch_1
    const/4 v0, 0x3

    .line 231
    goto :goto_1

    .line 234
    :pswitch_2
    const/4 v0, 0x0

    .line 235
    goto :goto_1

    .line 238
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_1

    .line 223
    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 227
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateSettingValues()V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateScreenMode()V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateLastPlayedItem()V

    .line 217
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateMiniModeSettings()V

    .line 218
    return-void
.end method


# virtual methods
.method public loadBooleanKey(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # Z

    .prologue
    .line 186
    :try_start_0
    sget-object v2, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 187
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 191
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return p2

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadIntKey(Ljava/lang/String;I)I
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # I

    .prologue
    .line 163
    const/4 v1, 0x0

    .line 165
    .local v1, "key":I
    :try_start_0
    sget-object v3, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 166
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 171
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return v1

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Ljava/lang/Exception;
    move v1, p2

    .line 169
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadLongKey(Ljava/lang/String;J)J
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "returnValue"    # J

    .prologue
    .line 176
    :try_start_0
    sget-object v2, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 177
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    .line 181
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-wide p2

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadStringKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;

    .prologue
    .line 152
    const-string v1, ""

    .line 154
    .local v1, "key":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 155
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v3, ""

    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 159
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-object v1

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;I)V
    .locals 7
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # I

    .prologue
    .line 101
    :try_start_0
    sget-object v4, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v5, "P1SharedPreferences"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 102
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 103
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 104
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 105
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 111
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 108
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v2

    .line 109
    .local v2, "oome":Ljava/lang/OutOfMemoryError;
    const-string v4, "SharedPreferenceManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveState"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;J)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # J

    .prologue
    .line 131
    sget-object v2, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 132
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 133
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 134
    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    return-void
.end method

.method public saveState(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 120
    :try_start_0
    sget-object v3, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v4, "P1SharedPreferences"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 121
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 122
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v1, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "SharedPreferenceManager"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public saveState(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "keyString"    # Ljava/lang/String;
    .param p2, "key"    # Z

    .prologue
    .line 139
    sget-object v2, Lcom/sec/android/app/videoplayer/db/SharedPreference;->mContext:Landroid/content/Context;

    const-string v3, "P1SharedPreferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 140
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 141
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 142
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 143
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 144
    return-void
.end method

.method public updateVersion()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 196
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->isOldVersion()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 197
    const-string v1, "screen_mode"

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 198
    .local v0, "scrMode":I
    if-eq v0, v2, :cond_0

    .line 199
    const-string v1, "SharedPreferenceManager"

    const-string v2, "updateVersion : This could be old OS setting value!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateSettingValues()V

    .line 202
    :cond_0
    const-string v1, "version_code"

    const/16 v2, 0x13

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 204
    .end local v0    # "scrMode":I
    :cond_1
    return-void
.end method
