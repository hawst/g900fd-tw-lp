.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;
.super Ljava/lang/Object;
.source "SubtitleSplitView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V
    .locals 0

    .prologue
    .line 241
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v6, 0x0

    .line 244
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$200()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick-position"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 247
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    const-string v1, "SUBTITLE_START_TIME"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J
    invoke-static {v3, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;Ljava/lang/String;)J

    move-result-wide v4

    long-to-int v1, v4

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setControllerProgress()V

    .line 249
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Landroid/view/View;->setSelected(Z)V

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 252
    return-void
.end method
