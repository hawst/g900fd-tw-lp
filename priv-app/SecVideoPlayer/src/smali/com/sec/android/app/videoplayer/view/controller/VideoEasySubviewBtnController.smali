.class public Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;
.super Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.source "VideoEasySubviewBtnController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoEasySubviewBtnController"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mVolumeBtn:Landroid/widget/ImageButton;

.field private mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;-><init>(Landroid/content/Context;)V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    .line 158
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 28
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    .line 29
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public changeControllerLayout()V
    .locals 12

    .prologue
    const v11, 0x7f080109

    const/16 v10, 0xd

    const/4 v9, -0x1

    const v8, 0x7f0d01c0

    const/4 v7, 0x0

    .line 82
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01bf

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 83
    .local v0, "button_layout":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01ca

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 84
    .local v3, "subview_ctrl_button":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01cc

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 85
    .local v4, "subview_videoplayer_btn_pause":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 86
    .local v2, "easy_volume_btn":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01c1

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 88
    .local v1, "display_change_button":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 90
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08010b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 91
    const/16 v5, 0xb

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 92
    const/16 v5, 0x9

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 93
    const/4 v5, 0x1

    invoke-virtual {v1, v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 103
    :goto_0
    invoke-virtual {v4, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 104
    invoke-virtual {v1, v10, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 106
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01bf

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 107
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01ca

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 108
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01cc

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 109
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 110
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01c1

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 111
    return-void

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08010b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 97
    const/16 v5, 0xc

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 98
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v6, 0x7f0d01be

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v7, v7, v7, v7}, Landroid/view/View;->setPadding(IIII)V

    .line 99
    const/16 v5, 0xa

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 100
    const/4 v5, 0x3

    invoke-virtual {v1, v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0
.end method

.method public forceHide()V
    .locals 0

    .prologue
    .line 155
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->forceHide()V

    .line 156
    return-void
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->hide()V

    .line 152
    return-void
.end method

.method protected initControllerView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initControllerView(Landroid/view/View;)V

    .line 43
    const v0, 0x7f0d01c0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 47
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v0

    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f020100

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 56
    :cond_0
    return-void

    .line 50
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v1, 0x7f020101

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    .line 34
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->changeControllerLayout()V

    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->setInvisibleAllViews()V

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    return-object v0
.end method

.method public setVisibleAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->updatePausePlayBtn()V

    .line 78
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->updateBtnControllerBtns()V

    .line 79
    return-void
.end method

.method public setVolumeBtn()V
    .locals 6

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 131
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    .line 133
    .local v1, "vol":I
    if-nez v1, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v3, 0x7f020100

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 139
    :goto_0
    if-nez v1, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0088

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    :goto_1
    return-void

    .line 136
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v3, 0x7f020101

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mVolumeBtn:Landroid/widget/ImageButton;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a008d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public show()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->show()V

    .line 148
    return-void
.end method

.method public updateBtnControllerBtns()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 121
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateBtnControllerBtns()V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 125
    return-void
.end method

.method public updatePausePlayBtn()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 114
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updatePausePlayBtn()V

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 118
    return-void
.end method
