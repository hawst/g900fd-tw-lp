.class Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;
.super Ljava/lang/Object;
.source "VideoVisualSeekView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->ShowVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 121
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnPreparedListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mCurrentPosition:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$600(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$502(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)Z

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->start()V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->pause()V

    .line 127
    return-void
.end method
