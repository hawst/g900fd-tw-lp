.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0a001d

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 152
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "callSelectSubtitlePopup. onClick"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v2, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/io/File;)Ljava/io/File;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 159
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->saveSubtitleActivation(Z)V

    .line 187
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFileType()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 189
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 190
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const-string v2, "VideoSubtitlePopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 235
    :goto_1
    return-void

    .line 162
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 163
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "callSelectSubtitlePopup. file is different"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v3, 0x7f0a0035

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 166
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "callSelectSubtitlePopup : onClick INBAND Subtitle"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->stopSubtitle()V

    .line 168
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setInbandSubtitle()V

    .line 183
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->saveSubtitleActivation(Z)V

    goto/16 :goto_0

    .line 169
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->existFile(Ljava/lang/String;)Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$600(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleLang()V

    .line 172
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$402(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/lang/String;)Ljava/lang/String;

    .line 173
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->selectOutbandSubtitle()Z

    .line 175
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 178
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "callSelectSubtitlePopup() - iputFilePath has an exception."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 193
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setOnSubtitleInfoListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;)V

    goto/16 :goto_1

    .line 220
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleCount()I

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v3, 0x7f0a0138

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v1, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/io/File;)Ljava/io/File;

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->callSelectSubtitlePopup()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0092

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 230
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setSubtitleTrackChangedOnPresentationMode(Z)V

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const-string v2, "VideoSubtitlePopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto/16 :goto_1
.end method
