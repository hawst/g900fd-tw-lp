.class Lcom/sec/android/app/videoplayer/view/MainVideoView$4;
.super Ljava/lang/Object;
.source "MainVideoView.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;->createTalkBackOffDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 2602
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 2604
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->turnOffTalkBack(Landroid/content/Context;)Z

    .line 2605
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2606
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "MainVideoView"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 2607
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->openPopupPlayer(Landroid/content/Context;)V

    .line 2608
    return-void
.end method
