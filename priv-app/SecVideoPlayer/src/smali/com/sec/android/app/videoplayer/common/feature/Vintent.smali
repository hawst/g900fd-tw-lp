.class public Lcom/sec/android/app/videoplayer/common/feature/Vintent;
.super Ljava/lang/Object;
.source "Vintent.java"


# static fields
.field public static final ABEAM_SHOW_DIRECT_SHARE_POPUP:Ljava/lang/String; = "com.android.nfc.AndroidBeamPopUp"

.field public static final ACCESSIBILITY_SETTINGS_ACTIVITY:Ljava/lang/String; = "com.android.settings.Settings$CaptioningSettingsActivity"

.field public static final ACCESSIBILITY_SETTINGS_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final ACTION_ACCESSIBILITY_SETTINGS:Ljava/lang/String; = "android.settings.CAPTIONING_SETTINGS"

.field public static final ACTION_ALLSHARE_INTENT_API:Ljava/lang/String; = "com.sec.android.allshare.intent.action.VIDEOPLAYER"

.field public static final ACTION_ALL_SHARE_PLAY_DIRECT_DMC:Ljava/lang/String; = "PCLOUD.EXECUTE_DIRECT_DMC"

.field public static final ACTION_AUTO_PLAY_NEXT:Ljava/lang/String; = "com.sec.android.app.videoplayer.AUTO_PLAY_NEXT"

.field public static final ACTION_CC_SETTING:Ljava/lang/String; = "com.sec.android.app.videoplayer.ACTION_START_CC_SETTING"

.field public static final ACTION_CLOUD_OPTION:Ljava/lang/String; = "com.sec.android.app.videoplayer.CLOUD_OPTION"

.field public static final ACTION_CONFIGURATION_CHANGED:Ljava/lang/String; = "android.intent.action.CONFIGURATION_CHANGED"

.field public static final ACTION_CRITICAL_LOW_BATT:Ljava/lang/String; = "videoplayer.critical.low.batt"

.field public static final ACTION_DELETE_CONTENT:Ljava/lang/String; = "com.samsung.everglades.video.ACTION_DELETE_CONTENT"

.field public static final ACTION_DISMISS_PRESENTATION:Ljava/lang/String; = "videoplayer.dismiss.presentation"

.field public static final ACTION_EASY_MODE_CHANGE:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final ACTION_EXIT:Ljava/lang/String; = "videoplayer.exit"

.field public static final ACTION_GLANCEVIEW_EVENT_INFO:Ljava/lang/String; = "com.android.internal.policy.impl.sec.glanceview.eventinfo"

.field public static final ACTION_HDMI_PLUG:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field public static final ACTION_HOME_RESUME:Ljava/lang/String; = "com.sec.android.intent.action.HOME_RESUME"

.field public static final ACTION_INTENT_SLINK_PLAYBACK:Ljava/lang/String; = "android.intent.action.START_SLINK_PLAYBACK"

.field public static final ACTION_LAST_FILE_NAME:Ljava/lang/String; = "com.samsung.everglades.video.myvideo.LAST_FILE_NAME"

.field public static ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String; = null

.field public static final ACTION_LINE_OUT:Ljava/lang/String; = "android.intent.action.USB_ANLG_HEADSET_PLUG"

.field public static final ACTION_MEDIA_MOUNTED:Ljava/lang/String; = "android.intent.action.MEDIA_SCAN"

.field public static final ACTION_MINIMODE_VIDEO:Ljava/lang/String; = "ActionVideoMinimodeService"

.field public static final ACTION_MULTIWINDOW:Ljava/lang/String; = "com.sec.android.action.NOTIFY_SPLIT_WINDOWS"

.field public static final ACTION_NOT_ALLOWED_SCREEN_MIRRORING:Ljava/lang/String; = "android.intent.action.NOT_ALLOWED_SCREEN_MIRRORING"

.field public static final ACTION_PAUSEBY:Ljava/lang/String; = "videoplayer.pauseby"

.field public static ACTION_PAUSE_BY_SPEN:Ljava/lang/String; = null

.field public static ACTION_PLAY_BY_SPEN:Ljava/lang/String; = null

.field public static final ACTION_PLAY_VIA_GROUPPLAY:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_VIDEO"

.field public static final ACTION_PRESENTATION_FOCUS_CHANGED:Ljava/lang/String; = "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

.field public static final ACTION_RESUMEBY:Ljava/lang/String; = "videoplayer.resumeby"

.field public static final ACTION_SECURE_LOCK:Ljava/lang/String; = "android.intent.action.SECURE_LOCK"

.field public static final ACTION_SEND_PLAYED_CONTENT_INFO:Ljava/lang/String; = "com.samsung.everglades.video.ACTION_PLAYED_CONTENT_INFO"

.field public static final ACTION_SEND_TO_BT_INFO:Ljava/lang/String; = "com.sec.android.videoplayer.info"

.field public static final ACTION_SEND_TO_BT_METADATA:Ljava/lang/String; = "com.sec.android.videoplayer.metachanged"

.field public static final ACTION_SEND_TO_BT_PLAYSTATUS:Ljava/lang/String; = "com.sec.android.videoplayer.playerstatus"

.field public static final ACTION_SET_CONTROLLER_PLAY_STOP:Ljava/lang/String; = "videoplayer.set.controller.play.stop"

.field public static final ACTION_SET_LOCK:Ljava/lang/String; = "videoplayer.set.lock"

.field public static final ACTION_SHOW_ERROR_POPUP:Ljava/lang/String; = "videoplayer.show.error.popup"

.field public static final ACTION_SHOW_PRESENTATION_VIDEO:Ljava/lang/String; = "com.samsung.action.SHOW_PRESENTATION"

.field public static final ACTION_SIDESYNC_DISABLE_PLAY_VIDEO:Ljava/lang/String; = "com.sec.android.sidesync.common.DISABLE_PLAY_VIDEO"

.field public static final ACTION_SIDESYNC_START:Ljava/lang/String; = "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

.field public static final ACTION_SMARTPAUSE_SHOW_CONTROLLER:Ljava/lang/String; = "videoplayer.show.controller.smartpause"

.field public static final ACTION_SORT_BY:Ljava/lang/String; = "com.sec.android.app.videoplayer.SORT_BY"

.field public static final ACTION_START_DIRECT_DMC:Ljava/lang/String; = "android.intent.action.START_DIRECT_DMC"

.field public static final ACTION_START_SCONNECT_BEZEL:Ljava/lang/String; = "com.samsung.android.sconnect.START"

.field public static final ACTION_START_SCONNECT_DMC:Ljava/lang/String; = "com.samsung.android.sconnect.action.VIDEO_DMR"

.field public static final ACTION_STATE_VIEW_VISIBILITY:Ljava/lang/String; = "videoplayer.stateview.visibility"

.field public static final ACTION_SWIPE:Ljava/lang/String; = "videoplayer.swipe"

.field public static final ACTION_TALKBACK:Ljava/lang/String; = "com.android.settings.action.talkback_off"

.field public static final ACTION_UPDATE_TITLE:Ljava/lang/String; = "videoplayer.update.title"

.field public static final ACTION_VIDEO_FROM_TRIM:Ljava/lang/String; = "android.intent.action.START_VIDEO_FROM_TRIM"

.field public static final ACTION_VIDEO_PLAYBACK_STOP:Ljava/lang/String; = "android.intent.action.VIDEO_PLAYBACK_STOP"

.field public static final ACTION_WATCH_ON_RESPONSE:Ljava/lang/String; = "com.samsung.intent.RESPONSE_FROM_YOSEMITE"

.field public static ACTION_WIFI_DISPLAY:Ljava/lang/String; = null

.field public static final ACTIVITY_BEST_VIDEO_SHOT:Ljava/lang/String; = "com.sec.android.app.bestvideoshot"

.field public static final ACTIVITY_CONTEXTUALTITLE:Ljava/lang/String; = ".contextual.ContextualTitleActivity"

.field public static final ACTIVITY_CONTEXTUALWEATHER:Ljava/lang/String; = ".contextual.ContextualWeatherActivity"

.field public static final ACTIVITY_EVERGLADES:Ljava/lang/String; = "com.samsung.everglades.video"

.field public static final ACTIVITY_EVERGLADES_VIDEOMAIN:Ljava/lang/String; = "com.samsung.everglades.video.VideoMain"

.field public static final ACTIVITY_HOME:Ljava/lang/String; = ".zmyvideo.activities.VideoListEntryActivity"

.field public static final ACTIVITY_SAMSUNG_APPS:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field public static final ACTIVITY_SAMSUNG_APPS_DOWNLOAD_LINK:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static final ACTIVITY_TABLIST:Ljava/lang/String; = "com.sec.android.app.videolist"

.field public static final ACTIVITY_TABLIST_MAINTAB:Ljava/lang/String; = "com.sec.android.app.videolist.activity.VPMainTab"

.field public static final ACTIVITY_TRIM_APP_INTENT:Ljava/lang/String; = "android.intent.action.TRIM"

.field public static final ACTIVITY_TRIM_APP_MP:Ljava/lang/String; = "com.samsung.app.video"

.field public static final ACTIVITY_TRIM_APP_NXP:Ljava/lang/String; = "com.lifevibes.trimapp"

.field public static final ACTIVITY_VIDEO_EDITOR:Ljava/lang/String; = "com.sec.android.app.ve"

.field public static final ACTIVITY_VIDEO_EDITOR_FULL:Ljava/lang/String; = "com.sec.android.app.vefull"

.field public static final ACTIVITY_VIDEO_EDITOR_KONA:Ljava/lang/String; = "com.sec.android.app.veditor"

.field public static final ALLSHARE_INTENT_API_ITEM:Ljava/lang/String; = "com.sec.android.allshare.intent.extra.ITEM"

.field public static final ALLSHARE_INTENT_API_STATE:Ljava/lang/String; = "com.sec.android.allshare.intent.extra.ALLSHARE_ENABLED"

.field public static final APP_IN_APP_RESUME_POS_REPLY:Ljava/lang/String; = "AppInAppResumePositionReply"

.field public static final APP_IN_APP_RESUME_POS_REQUEST:Ljava/lang/String; = "AppInAppResumePositionRequest"

.field public static final B2B_SOULTION:Ljava/lang/String; = "android.intent.action.POSITION_INFO_FROM_MSC_APP"

.field public static final DIRECT_DMC_DMRUDN:Ljava/lang/String; = "DMRUDN"

.field public static final EXTRA_POPUP_TYPE:Ljava/lang/String; = "popup.type"

.field public static FROM_ASP_NEAR_BY_DEVICE:Ljava/lang/String; = null

.field public static FROM_ASP_SOURCE_DEVICE:Ljava/lang/String; = null

.field public static FROM_GALLERY:Ljava/lang/String; = null

.field public static FROM_GALLERY_SECURE_LOCK:Ljava/lang/String; = null

.field public static FROM_HELP_CLIP:Ljava/lang/String; = null

.field public static FROM_HELP_CLIP_DESCRIPTION:Ljava/lang/String; = null

.field public static FROM_HELP_CLIP_TIME_STEMP:Ljava/lang/String; = null

.field public static FROM_HELP_CLIP_TITLE:Ljava/lang/String; = null

.field public static FROM_HELP_START_CLIP:Ljava/lang/String; = null

.field public static FROM_HELP_VIDEO_MOTION_PEEK:Ljava/lang/String; = null

.field public static FROM_HELP_VIDEO_PROGRESS_BAR_PREVIEW:Ljava/lang/String; = null

.field public static FROM_HELP_VIDEO_SMART_PAUSE:Ljava/lang/String; = null

.field public static FROM_MYFILES:Ljava/lang/String; = null

.field public static FROM_PHOTORING:Ljava/lang/String; = null

.field public static FROM_SETUP_WIZARD:Ljava/lang/String; = null

.field public static FROM_VZW_GUIDE_TOUR:Ljava/lang/String; = null

.field public static FROM_WIDGET:Ljava/lang/String; = null

.field public static final HIDE:Ljava/lang/String; = "hide"

.field public static final HOME_THEME_CHANGED:Ljava/lang/String; = "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

.field public static final IMEDIA_PLAYER_VIDEO_EXIST:Ljava/lang/String; = "android.media.IMediaPlayer.videoexist"

.field public static final INTENT_AUTO_PLAY_NEXT:Ljava/lang/String; = "autoplay"

.field public static final INTENT_CLOUD_OPTION:Ljava/lang/String; = "cloud_option"

.field public static final INTENT_FINISH_APP_IN_APP:Ljava/lang/String; = "intent.finished.app-in-app"

.field public static final INTENT_INFO_SEEK_TIME:Ljava/lang/String; = "video_time"

.field public static final INTENT_SORT_BY:Ljava/lang/String; = "sortby"

.field public static final INTENT_STOP_APP_IN_APP:Ljava/lang/String; = "intent.stop.app-in-app"

.field public static final INTENT_STOP_APP_IN_APP_SEND_APP:Ljava/lang/String; = "intent.stop.app-in-app.send_app"

.field public static final INTENT_TITLE_NAME:Ljava/lang/String; = "title_name"

.field public static final IS_LOCKED:Ljava/lang/String; = "isLocked"

.field public static final KEY_APP_NAME:Ljava/lang/String; = "app_name"

.field public static final KEY_LAUNCH_HOME:Ljava/lang/String; = "launch_home"

.field public static final KEY_USER_ID:Ljava/lang/String; = "user_id"

.field public static final MINIMODE_AUDIO_TRACK:Ljava/lang/String; = "audioTrack"

.field public static final MINIMODE_BUCKETID:Ljava/lang/String; = "bucketid"

.field public static final MINIMODE_CURRENTPROVIDERNAME:Ljava/lang/String; = "CurrentProviderName"

.field public static final MINIMODE_CURRENT_ID:Ljava/lang/String; = "currentID"

.field public static final MINIMODE_DVFS_GENERAL:Ljava/lang/String; = "com.sec.android.intent.action.DVFS_GENERAL"

.field public static final MINIMODE_FILE_PATH:Ljava/lang/String; = "filePath"

.field public static final MINIMODE_ISCALLEDSCALEWINDOW:Ljava/lang/String; = "calledScalewindow"

.field public static final MINIMODE_ISFROMMOVIESTORE:Ljava/lang/String; = "isfromMovieStore"

.field public static final MINIMODE_LIST_KEY:Ljava/lang/String; = "ListKey"

.field public static final MINIMODE_LIST_TYPE:Ljava/lang/String; = "ListType"

.field public static final MINIMODE_PLAY_SPEED:Ljava/lang/String; = "playSpeed"

.field public static final MINIMODE_RESUME_POS:Ljava/lang/String; = "resumePos"

.field public static final MINIMODE_SCALEWINDOWRECT:Ljava/lang/String; = "ScalewindowRect"

.field public static final MINIMODE_SCREEN_HEIGHT:Ljava/lang/String; = "screenHeight"

.field public static final MINIMODE_SCREEN_WIDTH:Ljava/lang/String; = "screenWidth"

.field public static final MINIMODE_START_FROM_LIST:Ljava/lang/String; = "startFromList"

.field public static final MINIMODE_STOP_FROM:Ljava/lang/String; = "stopFrom"

.field public static final MINIMODE_STOP_FROM_INTERNAL:Ljava/lang/String; = "stopFromInternal"

.field public static final MINIMODE_SUBTITLE_ACTVATION:Ljava/lang/String; = "subtitleActivation"

.field public static final MINIMODE_SUBTITLE_FILE:Ljava/lang/String; = "subtitleFile"

.field public static final MINIMODE_SUBTITLE_IS_MULTI:Ljava/lang/String; = "subtitleIsMulti"

.field public static final MINIMODE_SUBTITLE_LANGUAGE_INDEX:Ljava/lang/String; = "subtitleLanguageIdx"

.field public static final MINIMODE_SUBTITLE_MULTI_COUNT:Ljava/lang/String; = "subtitleMultiCount"

.field public static final MINIMODE_SUBTITLE_MULTI_SELECTED_INDEX:Ljava/lang/String; = "subtitleMultiIndex"

.field public static final MINIMODE_SUBTITLE_SYNC_TIME:Ljava/lang/String; = "subttileSyncTime"

.field public static final MINIMODE_SUBTITLE_TEXT_SIZE:Ljava/lang/String; = "subtitleTextSize"

.field public static final MINIMODE_TITLE:Ljava/lang/String; = "title"

.field public static final MINIMODE_URI:Ljava/lang/String; = "uri"

.field public static final MINIMODE_VIDEO_HEIGHT:Ljava/lang/String; = "videoHeight"

.field public static final MINIMODE_VIDEO_WIDTH:Ljava/lang/String; = "videoWidth"

.field public static final MINIMODE_WATCH_ON_BTN_ENABLE:Ljava/lang/String; = "yosemite_enable"

.field public static final MINIMODE_WHERE_FROM:Ljava/lang/String; = "WhereFrom"

.field public static final PACKAGE_VIDEOLIST:Ljava/lang/String; = "com.samsung.everglades.video"

.field public static final PLAYER_LOCK:Ljava/lang/String; = "com.sec.android.app.videoplayer.PLAYER_LOCK"

.field public static final SAMSUNG_ACCOUNT_REQUEST_SERVICE:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_SERVICE"

.field public static final SBEAM_APPLICATION_PACKAGE:Ljava/lang/String; = "com.sec.android.directshare"

.field public static final SBEAM_ON_OFF:Ljava/lang/String; = "SBeam_on_off"

.field public static final SBEAM_POPUP_MODE:Ljava/lang/String; = "POPUP_MODE"

.field public static final SBEAM_PREFERENCE_NAME:Ljava/lang/String; = "pref_sbeam"

.field public static final SBEAM_PREFERENCE_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final SBEAM_SHOW_DIRECT_SHARE_POPUP:Ljava/lang/String; = "com.sec.android.directshare.DirectSharePopUp"

.field public static final SBEAM_START_ACTION:Ljava/lang/String; = "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

.field public static final SCREEN_MIRRORING_CANCELED:Ljava/lang/String; = "com.samsung.wfd.CHANGEPLAYER_CANCELED"

.field public static final SECURE_MODE_UPDATE:Ljava/lang/String; = "com.sec.videoplayer.SECURE_MODE_UPDATE"

.field public static final SHOW:Ljava/lang/String; = "show"

.field public static final VALUE_VIDEO:Ljava/lang/String; = "video"

.field public static final VIDEOLIST_CLASS_NAME:Ljava/lang/String; = "com.samsung.everglades.video.VideoMain"

.field public static final VIDEOWIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field public static final VIDEOWIDGET_SPANY:Ljava/lang/String; = "widgetspany"

.field public static final VIDEOWIDGET_UPDATE:Ljava/lang/String; = "videowidget.update"

.field public static final VIDEOWIDGET_WIDGETID:Ljava/lang/String; = "widgetId"

.field public static final VIDEO_PAUSE:Ljava/lang/String; = "video_pause"

.field public static final VIDEO_PLAY:Ljava/lang/String; = "video_play"

.field public static final VIDEO_PLAYER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.videoplayer"

.field public static final VIDEO_SEEK:Ljava/lang/String; = "video_seek"

.field public static final VIDEO_STOP:Ljava/lang/String; = "video_stop"

.field public static final VISIBILITY:Ljava/lang/String; = "visibility"

.field public static final WATCH_ON_CONTINUE_PLAYING:Ljava/lang/String; = "continue_playing"

.field public static final WATCH_ON_SERVICE_NAME:Ljava/lang/String; = "com.sec.msc.android.yosemite.intent.action.YosemiteService"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "com.sec.android.app.videoplayer.VIDEOPLAYER_PAUSE"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    .line 27
    const-string v0, "com.sec.android.app.videoplayer.VIDEOPLAYER_PLAY"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    .line 29
    const-string v0, "from_gallery_to_videoplayer"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_GALLERY:Ljava/lang/String;

    .line 30
    const-string v0, "from-sw"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_SETUP_WIZARD:Ljava/lang/String;

    .line 31
    const-string v0, "isFromWidget"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_WIDGET:Ljava/lang/String;

    .line 32
    const-string v0, "from_asp_near_by_device_to_videoplayer"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_ASP_NEAR_BY_DEVICE:Ljava/lang/String;

    .line 33
    const-string v0, "sourceDeviceTypeFromASP"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_ASP_SOURCE_DEVICE:Ljava/lang/String;

    .line 34
    const-string v0, "android.intent.action.START_VIDEO_FROM_GUIDED_TOUR"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_VZW_GUIDE_TOUR:Ljava/lang/String;

    .line 35
    const-string v0, "from-myfiles"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_MYFILES:Ljava/lang/String;

    .line 36
    const-string v0, "createBySecureLock"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_GALLERY_SECURE_LOCK:Ljava/lang/String;

    .line 37
    const-string v0, "from_photoring_to_videoplayer"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_PHOTORING:Ljava/lang/String;

    .line 40
    const-string v0, "notification.launch.videoplayer"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    .line 43
    const-string v0, "android.intent.action.WIFI_DISPLAY"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_WIFI_DISPLAY:Ljava/lang/String;

    .line 46
    const-string v0, "android.intent.action.START_HELP_VIDEO_PROGRESS_BAR_PREVIEW"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_PROGRESS_BAR_PREVIEW:Ljava/lang/String;

    .line 47
    const-string v0, "android.intent.action.START_HELP_VIDEO_SMART_PAUSE"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_SMART_PAUSE:Ljava/lang/String;

    .line 48
    const-string v0, "android.intent.action.START_HELP_VIDEO_MOTION_PEEK"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_MOTION_PEEK:Ljava/lang/String;

    .line 49
    const-string v0, "android.intent.action.START_HELP_CLIP"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP:Ljava/lang/String;

    .line 50
    const-string v0, "HELP_TIME_STEMP"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_TIME_STEMP:Ljava/lang/String;

    .line 51
    const-string v0, "HELP_DESCRIPTION"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_DESCRIPTION:Ljava/lang/String;

    .line 52
    const-string v0, "HELP_TITLE"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_TITLE:Ljava/lang/String;

    .line 53
    const-string v0, "HELP_START_CLIP"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_START_CLIP:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
