.class Lcom/sec/android/app/videoplayer/view/VideoSurface$1;
.super Ljava/lang/Object;
.source "VideoSurface.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/VideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoSurface;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 196
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 199
    const-string v0, "VideoSurface"

    const-string v1, ">>>>>>>>surfaceCreated<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$002(Lcom/sec/android/app/videoplayer/view/VideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 201
    # operator++ for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$108()I

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$202(Lcom/sec/android/app/videoplayer/view/VideoSurface;Z)Z

    .line 203
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 206
    const-string v0, "VideoSurface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>>>surfaceDestroyed<<<<<<<<<<<<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$200(Lcom/sec/android/app/videoplayer/view/VideoSurface;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$002(Lcom/sec/android/app/videoplayer/view/VideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceExists:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$202(Lcom/sec/android/app/videoplayer/view/VideoSurface;Z)Z

    .line 210
    # operator-- for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$110()I

    .line 212
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoSurface;->mSurfaceCount:I
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->access$100()I

    move-result v0

    if-lez v0, :cond_0

    .line 224
    :goto_0
    return-void

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurface$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->resetSubtitleController()V

    .line 217
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    .line 219
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->restorScreenMode()V

    goto :goto_0
.end method
