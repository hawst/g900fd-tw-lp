.class public Lcom/sec/android/app/videoplayer/common/VUtils;
.super Ljava/lang/Object;
.source "VUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;,
        Lcom/sec/android/app/videoplayer/common/VUtils$myView;,
        Lcom/sec/android/app/videoplayer/common/VUtils$Color;
    }
.end annotation


# static fields
.field private static final EASY_MODE:I = 0x0

.field public static final FLAG_TYPE_MULTIWINDOW:I = 0x309

.field public static final FLAG_TYPE_SAMSUNG:I = 0x308

.field public static final HEIGHT:I = 0x1

.field public static final LOCAL_PLAYER_MODE:I = 0x0

.field public static final ROTATION_DEGREE_90:I = 0x1

.field public static final SECAV_PLAYER_MODE:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static final WIDTH:I = 0x0

.field public static final WIFI_DISPLAY_MODE:I = 0x2

.field public static final XPOS:I = 0x0

.field public static final YPOS:I = 0x1

.field private static mScreenOrientation:I

.field private static mSecondScreenOrientation:I

.field private static mSecondScreenUserOrientation:I

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/common/VUtils;

.field private static mUniqueInstance4PopupPlayer:Lcom/sec/android/app/videoplayer/common/VUtils;

.field private static mUserOrientation:I


# instance fields
.field private SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

.field private mBusBooster:Landroid/os/DVFSHelper;

.field private mContext:Landroid/content/Context;

.field private mControllerMode:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field private mCpuBooster:Landroid/os/DVFSHelper;

.field private mHasWindowFocus:Z

.field mIsCriticalLowBatteryStatus:Z

.field private mIsKnoxMode:I

.field private mIsSbeamAvailable:I

.field private mPlayerMode:I

.field private mPrevColor:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

.field private mPrevIsAdaptive:I

.field private mPrevScreenMode:I

.field private mSelectedbyChangePlayer:Z

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 83
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUniqueInstance:Lcom/sec/android/app/videoplayer/common/VUtils;

    .line 84
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUniqueInstance4PopupPlayer:Lcom/sec/android/app/videoplayer/common/VUtils;

    .line 86
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    .line 1020
    sput v1, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I

    .line 1021
    sput v1, Lcom/sec/android/app/videoplayer/common/VUtils;->mUserOrientation:I

    .line 1022
    sput v1, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenOrientation:I

    .line 1023
    sput v1, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenUserOrientation:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    .line 90
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    .line 92
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    .line 94
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    .line 97
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mCpuBooster:Landroid/os/DVFSHelper;

    .line 100
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mBusBooster:Landroid/os/DVFSHelper;

    .line 102
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mHasWindowFocus:Z

    .line 104
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsSbeamAvailable:I

    .line 866
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NONE:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevColor:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 956
    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPlayerMode:I

    .line 974
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSelectedbyChangePlayer:Z

    .line 1320
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mControllerMode:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1784
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsCriticalLowBatteryStatus:Z

    .line 1972
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevScreenMode:I

    .line 1974
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevIsAdaptive:I

    return-void
.end method

.method public static getEasyModeSwitch(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1374
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_switch"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getEasyModeVideo(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1378
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "easy_mode_video"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUniqueInstance:Lcom/sec/android/app/videoplayer/common/VUtils;

    return-object v0
.end method

.method public static getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUniqueInstance4PopupPlayer:Lcom/sec/android/app/videoplayer/common/VUtils;

    return-object v0
.end method

.method public static getScreenOrientation()I
    .locals 1

    .prologue
    .line 1043
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I

    return v0
.end method

.method public static getSecondScreenOrientation()I
    .locals 1

    .prologue
    .line 1101
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenOrientation:I

    return v0
.end method

.method public static getSecondScreenUserOrientation()I
    .locals 1

    .prologue
    .line 1093
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenUserOrientation:I

    return v0
.end method

.method public static getUserOrientation()I
    .locals 1

    .prologue
    .line 1035
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUserOrientation:I

    return v0
.end method

.method public static isAdaptiveMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 1051
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_mode_automatic_setting"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isAutoRotation(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1047
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isDrmFileNotSupportAIA(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1953
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_DRM_AIA_FUNCTION:Z

    if-eqz v1, :cond_2

    .line 1954
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getDRMType()I

    move-result v0

    .line 1957
    .local v0, "DRMType":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1959
    :cond_0
    const/4 v1, 0x0

    .line 1964
    .end local v0    # "DRMType":I
    :goto_0
    return v1

    .line 1961
    .restart local v0    # "DRMType":I
    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1964
    .end local v0    # "DRMType":I
    :cond_2
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v1

    goto :goto_0
.end method

.method public static isEasyMode(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 1383
    if-nez p0, :cond_1

    .line 1386
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getEasyModeSwitch(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getEasyModeVideo(Landroid/content/Context;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static isFolderClose(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1121
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1122
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1123
    const/4 v1, 0x1

    .line 1125
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isFolderOpen(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 1130
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1131
    .local v0, "config":Landroid/content/res/Configuration;
    iget v2, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v1, :cond_0

    .line 1134
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isLandscape()Z
    .locals 2

    .prologue
    .line 1076
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLandscape(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1065
    if-nez p0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v1

    .line 1072
    :goto_0
    return v1

    .line 1067
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1068
    .local v0, "configuration":Landroid/content/res/Configuration;
    sget v1, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 1069
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setScreenOrientation(I)V

    .line 1072
    :cond_1
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSbeamPkgAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1734
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.directshare"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1739
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "SBeam exists!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1740
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 1735
    :catch_0
    move-exception v0

    .line 1736
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "SBeam doesn\'t exist!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSecondScreenAutoRotation(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1105
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation_second"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isSecondScreenLandscape()Z
    .locals 2

    .prologue
    .line 1117
    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenOrientation:I

    if-eqz v0, :cond_0

    sget v0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenOrientation:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecondScreenLandscape(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1109
    if-nez p0, :cond_0

    .line 1110
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenLandscape()Z

    move-result v1

    .line 1113
    :goto_0
    return v1

    .line 1112
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1113
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isStandardMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 1058
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_mode_setting"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 1059
    const/4 v0, 0x1

    .line 1061
    :cond_0
    return v0
.end method

.method public static declared-synchronized setScreenOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 1039
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mScreenOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1040
    monitor-exit v0

    return-void

    .line 1039
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSecondScreenOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 1097
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1098
    monitor-exit v0

    return-void

    .line 1097
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setSecondScreenUserOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 1089
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSecondScreenUserOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1090
    monitor-exit v0

    return-void

    .line 1089
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setUserOrientation(I)V
    .locals 2
    .param p0, "orientation"    # I

    .prologue
    .line 1031
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mUserOrientation:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1032
    monitor-exit v0

    return-void

    .line 1031
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isDirectDmcMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPlusCallActive()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public StringArrayToString([Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "strArray"    # [Ljava/lang/String;

    .prologue
    .line 1391
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1392
    .local v1, "sb":Ljava/lang/StringBuilder;
    if-eqz p1, :cond_0

    array-length v2, p1

    if-lez v2, :cond_0

    .line 1393
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 1394
    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1393
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1398
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public acquireBusBooster()V
    .locals 0

    .prologue
    .line 316
    return-void
.end method

.method public acquireCpuBooster()V
    .locals 0

    .prologue
    .line 284
    return-void
.end method

.method public blockPlaySpeed()Z
    .locals 3

    .prologue
    .line 1912
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    .line 1913
    .local v0, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    .line 1915
    .local v1, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSlowCPU()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isMovieStoreContent()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isVideoOnlyClip()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiOrNoAudioChannel()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromHelpClip()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public canCaptureVideoFrame()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1886
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    .line 1888
    .local v2, "svcUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 1908
    :cond_0
    :goto_0
    return v3

    .line 1892
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    .line 1893
    .local v0, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    .line 1895
    .local v1, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAudioOnlyClip()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1902
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isVideoList()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isDropboxHLS()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallery()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isHLS()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromLocalFilePath()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFileExternal()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1906
    :cond_4
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V
    .locals 2
    .param p1, "win"    # Landroid/view/Window;
    .param p2, "color"    # Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .prologue
    .line 873
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevColor:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    if-eq v0, p2, :cond_0

    .line 874
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NULL:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    if-ne p2, v0, :cond_1

    .line 875
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 876
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "changeWindowBackgroundColor NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    :cond_0
    :goto_0
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevColor:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 886
    return-void

    .line 877
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    if-ne p2, v0, :cond_2

    .line 878
    const v0, 0x7f070009

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 879
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "changeWindowBackgroundColor BLACK"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 880
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->TRANSPARENCY:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    if-ne p2, v0, :cond_0

    .line 881
    const v0, 0x7f07006f

    invoke-virtual {p1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 882
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "changeWindowBackgroundColor TRANSPARENCY"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public changeWindowBackgroundColor(Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V
    .locals 1
    .param p1, "color"    # Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .prologue
    .line 889
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 892
    :goto_0
    return-void

    .line 891
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    goto :goto_0
.end method

.method public checkIsCalling(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 160
    if-nez p1, :cond_0

    const/4 v2, 0x0

    .line 180
    :goto_0
    return v2

    .line 163
    :cond_0
    :try_start_0
    const-string v2, "audio"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    invoke-virtual {v2}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    .line 164
    .local v0, "audio_mode":I
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkIsCalling() - audiomode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    if-eq v0, v3, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 166
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "checkIsCalling() - An audio/video chat is established."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isCallState(Landroid/content/Context;)Z

    move-result v2

    goto :goto_0

    .line 169
    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 170
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkIsCalling() - A VoIP call is established."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 171
    goto :goto_0

    .line 172
    :cond_3
    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 173
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkIsCalling() - A Video call is established."

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 174
    goto :goto_0

    .line 176
    .end local v0    # "audio_mode":I
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "checkIsCalling() - Fail to get an AUDIO_SERVICE !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isCallState(Landroid/content/Context;)Z

    move-result v2

    goto :goto_0
.end method

.method public checkKnoxMode(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1604
    if-nez p1, :cond_0

    .line 1613
    :goto_0
    return-void

    .line 1607
    :cond_0
    const/4 v0, 0x0

    .line 1608
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 1609
    if-eqz v0, :cond_2

    .line 1610
    const-string v1, "2.0"

    const-string v2, "version"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1612
    :cond_2
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    goto :goto_0
.end method

.method public checkLockScreenOn(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 1139
    if-nez p1, :cond_1

    .line 1140
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "checkLockScreenOn() - context is NULL"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1163
    :cond_0
    :goto_0
    return v3

    .line 1144
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    .line 1146
    .local v2, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromSetupWizard()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1147
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "checkLockScreenOn() - in case of the playback through SetupWizard, do not check the LockScreen"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1149
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1150
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "checkLockScreenOn() - in case of the playback in secure lock state from gallery, do not check the LockScreen"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1154
    :cond_3
    const-string v4, "keyguard"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1155
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    const-string v5, "persona"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 1157
    .local v1, "pm":Landroid/os/PersonaManager;
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    if-eqz v1, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    const/16 v5, 0x64

    if-lt v4, v5, :cond_0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/os/PersonaManager;->getKeyguardShowState(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1159
    :cond_5
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkLockScreenOn() - isKeyguardLocked TRUE. Lock Screen ON"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public checkLockScreenOn(Landroid/content/Context;Z)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "homeResume"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1167
    if-nez p1, :cond_1

    .line 1168
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkLockScreenOn() - context is NULL"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    :cond_0
    :goto_0
    return v2

    .line 1172
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    .line 1174
    .local v1, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    if-nez p2, :cond_3

    .line 1175
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromSetupWizard()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1176
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkLockScreenOn() - in case of the playback through SetupWizard, do not check the LockScreen"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1178
    :cond_2
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1179
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "checkLockScreenOn() - in case of the playback in secure lock state from gallery, do not check the LockScreen"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1184
    :cond_3
    const-string v3, "keyguard"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 1186
    .local v0, "mKeyguardManager":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1187
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "checkLockScreenOn() - isKeyguardLocked TRUE. Lock Screen ON"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public checkPrevScreenmode()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1982
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_mode_automatic_setting"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevIsAdaptive:I

    .line 1984
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkPrevScreenmode() mPrevIsAdaptive : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevIsAdaptive:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " screenmode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_mode_setting"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1987
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevIsAdaptive:I

    if-nez v0, :cond_0

    .line 1988
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_mode_setting"

    invoke-static {v0, v1, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevScreenMode:I

    .line 1989
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkPrevScreenmode() mPrevScreenMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevScreenMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1991
    :cond_0
    return-void
.end method

.method public converTimeToString(I)Ljava/lang/String;
    .locals 11
    .param p1, "timeMs"    # I

    .prologue
    const/4 v10, 0x0

    .line 778
    if-gez p1, :cond_0

    .line 779
    const/4 p1, 0x0

    .line 782
    :cond_0
    div-int/lit16 v6, p1, 0x3e8

    .line 783
    .local v6, "totalSeconds":I
    rem-int/lit8 v5, v6, 0x3c

    .line 784
    .local v5, "seconds":I
    div-int/lit8 v7, v6, 0x3c

    rem-int/lit8 v4, v7, 0x3c

    .line 785
    .local v4, "minutes":I
    div-int/lit16 v1, v6, 0xe10

    .line 787
    .local v1, "hours":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 788
    .local v2, "mFormatBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 790
    new-instance v3, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v3, v2, v7}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 791
    .local v3, "mFormatter":Ljava/util/Formatter;
    const-string v7, "%02d:%02d:%02d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    const/4 v9, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 792
    .local v0, "CurTime":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 794
    return-object v0
.end method

.method public disableAppInAppBtn()Z
    .locals 4

    .prologue
    .line 1927
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    .line 1928
    .local v0, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    .line 1929
    .local v2, "svcUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    .line 1931
    .local v1, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiwindowMinimumControllerMode()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDrmFileNotSupportAIA(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGuidedTour()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromASPNearByDevices()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromHelpClip()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAIAMWNotSupportedOnHEVC1080P()Z

    move-result v3

    if-nez v3, :cond_5

    sget-boolean v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isLongSeekMode()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v3

    if-gtz v3, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAudioOnlyClip()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v3

    if-nez v3, :cond_5

    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_6
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public dropLCDfps(Z)Z
    .locals 4
    .param p1, "set"    # Z

    .prologue
    .line 134
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 135
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dropLCDfps : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 137
    .local v0, "boostIntent":Landroid/content/Intent;
    const-string v1, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 138
    const-string v1, "SSRM_STATUS_NAME"

    const-string v2, "MoviePlayer_play"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    const-string v1, "SSRM_STATUS_VALUE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    const-string v1, "PackageName"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v1, "PID"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 142
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 143
    const/4 v1, 0x1

    .line 147
    .end local v0    # "boostIntent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 146
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "dropLCDfps : mApp is null please re-call setVUtilsData()"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public extractData(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dataType"    # I

    .prologue
    .line 1863
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "extractData() type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    .line 1879
    :goto_0
    return-object v1

    .line 1867
    :cond_0
    const/4 v1, 0x0

    .line 1868
    .local v1, "extracted":Ljava/lang/String;
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 1871
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 1872
    invoke-virtual {v2, p2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1876
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .line 1873
    :catch_0
    move-exception v0

    .line 1874
    .local v0, "ex":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RuntimeException occured : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1876
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_0

    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v3
.end method

.method public getAudioTrackChannels(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1858
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1859
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->extractData(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getAudioTrackCount()I
    .locals 3

    .prologue
    .line 1848
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    .line 1849
    .local v1, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getAudioTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v0

    .line 1850
    .local v0, "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-eqz v0, :cond_1

    .line 1851
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    .line 1854
    :goto_1
    return v2

    .line 1849
    .end local v0    # "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1854
    .restart local v0    # "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;
    .locals 1

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mControllerMode:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    return-object v0
.end method

.method public getCurrentRotation()I
    .locals 4

    .prologue
    .line 1082
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 1083
    .local v1, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1085
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    return v2
.end method

.method public getErrorStringResID(I)I
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 1652
    const/4 v0, -0x1

    .line 1654
    .local v0, "resId":I
    sparse-switch p1, :sswitch_data_0

    .line 1729
    :goto_0
    return v0

    .line 1658
    :sswitch_0
    const v0, 0x7f0a00f7

    .line 1659
    goto :goto_0

    .line 1662
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1663
    const v0, 0x7f0a00f6

    goto :goto_0

    .line 1665
    :cond_0
    const v0, 0x7f0a00f7

    .line 1667
    goto :goto_0

    .line 1670
    :sswitch_2
    const v0, 0x7f0a0073

    .line 1671
    goto :goto_0

    .line 1674
    :sswitch_3
    const v0, 0x7f0a006b

    .line 1675
    goto :goto_0

    .line 1678
    :sswitch_4
    const v0, 0x7f0a0071

    .line 1679
    goto :goto_0

    .line 1682
    :sswitch_5
    const v0, 0x7f0a00f3

    .line 1683
    goto :goto_0

    .line 1686
    :sswitch_6
    const v0, 0x7f0a00f2

    .line 1687
    goto :goto_0

    .line 1690
    :sswitch_7
    const v0, 0x7f0a00f0

    .line 1691
    goto :goto_0

    .line 1694
    :sswitch_8
    const v0, 0x7f0a00ed

    .line 1695
    goto :goto_0

    .line 1698
    :sswitch_9
    const v0, 0x7f0a00ec

    .line 1699
    goto :goto_0

    .line 1702
    :sswitch_a
    const v0, 0x7f0a00d0

    .line 1703
    goto :goto_0

    .line 1706
    :sswitch_b
    const v0, 0x7f0a00f4

    .line 1707
    goto :goto_0

    .line 1710
    :sswitch_c
    const v0, 0x7f0a00f1

    .line 1711
    goto :goto_0

    .line 1714
    :sswitch_d
    const v0, 0x7f0a01a4

    .line 1715
    goto :goto_0

    .line 1718
    :sswitch_e
    const v0, 0x7f0a01a7

    .line 1719
    goto :goto_0

    .line 1722
    :sswitch_f
    const v0, 0x7f0a00ee

    .line 1723
    goto :goto_0

    .line 1726
    :sswitch_10
    const v0, 0x7f0a00ef

    goto :goto_0

    .line 1654
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fffbffa -> :sswitch_10
        -0x7fffbff9 -> :sswitch_f
        -0x17d4 -> :sswitch_0
        -0x177d -> :sswitch_4
        -0x177c -> :sswitch_3
        -0x177b -> :sswitch_2
        -0x3fd -> :sswitch_1
        -0x3f2 -> :sswitch_b
        -0x3ef -> :sswitch_c
        -0x3ed -> :sswitch_5
        -0x63 -> :sswitch_e
        -0x62 -> :sswitch_d
        0x6d -> :sswitch_7
        0x70 -> :sswitch_a
        0x71 -> :sswitch_9
        0x72 -> :sswitch_8
        0xc8 -> :sswitch_6
        0x2714 -> :sswitch_0
        0x15f90 -> :sswitch_0
    .end sparse-switch
.end method

.method public getHTMLString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 1760
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1761
    :cond_0
    const-string v3, ""

    .line 1781
    :goto_0
    return-object v3

    .line 1763
    :cond_1
    const-string v3, ""

    .line 1765
    .local v3, "subString":Ljava/lang/String;
    const-string v5, "\n"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1766
    const-string v5, "\n"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1767
    .local v4, "text":[Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1768
    .local v2, "strBuilder":Ljava/lang/StringBuilder;
    array-length v1, v4

    .line 1770
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 1771
    aget-object v5, v4, v0

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1772
    add-int/lit8 v5, v1, -0x1

    if-ge v0, v5, :cond_2

    .line 1773
    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1770
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1776
    :cond_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1777
    goto :goto_0

    .line 1778
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "strBuilder":Ljava/lang/StringBuilder;
    .end local v4    # "text":[Ljava/lang/String;
    :cond_4
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getInitAIAxyPosition(III)[I
    .locals 7
    .param p1, "screenWidth"    # I
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 681
    const/4 v2, 0x2

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 682
    .local v0, "i":[I
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(II)[I

    move-result-object v1

    .line 683
    .local v1, "w":[I
    aget v2, v1, v5

    sub-int v2, p1, v2

    add-int/lit8 v2, v2, -0xa

    aput v2, v0, v5

    .line 684
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const/high16 v3, 0x41200000    # 10.0f

    add-float/2addr v2, v3

    float-to-int v2, v2

    aput v2, v0, v6

    .line 685
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInitAIAxyPosition : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 686
    return-object v0

    .line 681
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getPackageVersion(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 634
    const/4 v3, 0x0

    .line 636
    .local v3, "versionName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 638
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/16 v4, 0x80

    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 639
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 643
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    :goto_0
    return-object v3

    .line 640
    :catch_0
    move-exception v0

    .line 641
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to find Package : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getRectInfo()Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 391
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 393
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 403
    :goto_0
    return-object v1

    .line 394
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 402
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :cond_0
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "getRectInfo returns null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v1, 0x0

    goto :goto_0

    .line 396
    :catch_1
    move-exception v0

    .line 397
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 398
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 399
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 1196
    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    if-gez v2, :cond_0

    .line 1198
    :try_start_0
    const-class v2, Landroid/view/View;

    const-string v3, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 1199
    .local v1, "field":Ljava/lang/reflect/Field;
    if-nez v1, :cond_1

    .line 1200
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1209
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    return v2

    .line 1202
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_1
    :try_start_1
    const-string v2, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1204
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 1205
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setRemoveSystemUI not in build package : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    iput v5, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->SYSTEM_UI_FLAG_REMOVE_NAVIGATION:I

    goto :goto_0
.end method

.method public getScreenWidth()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 700
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v1

    aget v0, v1, v0

    .line 701
    :cond_0
    return v0
.end method

.method public getScreenWidthHeight(Landroid/content/Context;)[I
    .locals 5
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 690
    const/4 v3, 0x2

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 691
    .local v1, "i":[I
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 692
    .local v0, "fullScreen":Landroid/graphics/Point;
    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    .line 693
    .local v2, "windowManager":Landroid/view/WindowManager;
    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 694
    const/4 v3, 0x0

    iget v4, v0, Landroid/graphics/Point;->x:I

    aput v4, v1, v3

    .line 695
    const/4 v3, 0x1

    iget v4, v0, Landroid/graphics/Point;->y:I

    aput v4, v1, v3

    .line 696
    return-object v1

    .line 690
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getShareViaIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1335
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    .line 1336
    .local v1, "videoUri":Landroid/net/Uri;
    if-eqz p1, :cond_0

    if-nez v1, :cond_1

    .line 1337
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "onOptionsItemSelected() - uri is null."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1338
    const/4 v0, 0x0

    .line 1345
    :goto_0
    return-object v0

    .line 1341
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1342
    .local v0, "shareIntent":Landroid/content/Intent;
    const-string v2, "video/*"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1343
    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1344
    const-string v2, "theme"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method public getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f0a0162

    const v7, 0x7f0a0171

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 798
    const-string v2, "-"

    invoke-virtual {p2, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 799
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 833
    :goto_0
    return-object v2

    .line 802
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 803
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, ":"

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 804
    .local v0, "list":[Ljava/lang/String;
    array-length v2, v0

    packed-switch v2, :pswitch_data_0

    .line 833
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 806
    :pswitch_0
    aget-object v2, v0, v5

    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 807
    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 811
    :cond_1
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 815
    :pswitch_1
    aget-object v2, v0, v5

    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 816
    const v2, 0x7f0a015f

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 818
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 821
    :cond_2
    aget-object v2, v0, v6

    const-string v3, "00"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 822
    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    aget-object v4, v0, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 824
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    :cond_3
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x2

    aget-object v4, v0, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    .line 804
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getVersionOfContextProviders()I
    .locals 6

    .prologue
    .line 1592
    const/4 v2, -0x1

    .line 1594
    .local v2, "version":I
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.providers.context"

    const/16 v5, 0x80

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1596
    .local v1, "pInfo":Landroid/content/pm/PackageInfo;
    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1600
    .end local v1    # "pInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v2

    .line 1597
    :catch_0
    move-exception v0

    .line 1598
    .local v0, "e1":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "[SW] Could not find ContextProvider"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getVideoClipWidthHeight(Ljava/lang/String;)[I
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 985
    const/4 v9, 0x2

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    .line 986
    .local v2, "i":[I
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 987
    new-instance v4, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v4}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 989
    .local v4, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v4, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 990
    const/16 v9, 0x12

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    .line 991
    .local v7, "sWidth":Ljava/lang/String;
    const/16 v9, 0x13

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    .line 992
    .local v5, "sHeight":Ljava/lang/String;
    const/16 v9, 0x18

    invoke-virtual {v4, v9}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v6

    .line 994
    .local v6, "sOrientation":Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 995
    .local v8, "width":I
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 996
    .local v1, "height":I
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 998
    .local v3, "orientation":I
    if-eqz v3, :cond_0

    const/16 v9, 0xb4

    if-ne v3, v9, :cond_1

    .line 999
    :cond_0
    const/4 v9, 0x0

    aput v8, v2, v9

    .line 1000
    const/4 v9, 0x1

    aput v1, v2, v9

    .line 1006
    :goto_0
    sget-object v9, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getVideoClipWidthHeight "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " & "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 1015
    .end local v1    # "height":I
    .end local v3    # "orientation":I
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v5    # "sHeight":Ljava/lang/String;
    .end local v6    # "sOrientation":Ljava/lang/String;
    .end local v7    # "sWidth":Ljava/lang/String;
    .end local v8    # "width":I
    :goto_1
    return-object v2

    .line 1002
    .restart local v1    # "height":I
    .restart local v3    # "orientation":I
    .restart local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v5    # "sHeight":Ljava/lang/String;
    .restart local v6    # "sOrientation":Ljava/lang/String;
    .restart local v7    # "sWidth":Ljava/lang/String;
    .restart local v8    # "width":I
    :cond_1
    const/4 v9, 0x0

    :try_start_1
    aput v1, v2, v9

    .line 1003
    const/4 v9, 0x1

    aput v8, v2, v9
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1007
    .end local v1    # "height":I
    .end local v3    # "orientation":I
    .end local v5    # "sHeight":Ljava/lang/String;
    .end local v6    # "sOrientation":Ljava/lang/String;
    .end local v7    # "sWidth":Ljava/lang/String;
    .end local v8    # "width":I
    :catch_0
    move-exception v0

    .line 1008
    .local v0, "ex":Ljava/lang/RuntimeException;
    :try_start_2
    sget-object v9, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getVideoClipWidthHeight RuntimeException occured : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1010
    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    goto :goto_1

    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v9

    invoke-virtual {v4}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v9

    .line 1013
    .end local v4    # "retriever":Landroid/media/MediaMetadataRetriever;
    :cond_2
    sget-object v9, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v10, "getVideoClipWidthHeight path is null or empty!!!"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 985
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public getWidthAndHeight(II)[I
    .locals 4
    .param p1, "videoWidth"    # I
    .param p2, "videoHeight"    # I

    .prologue
    .line 652
    const/4 v0, 0x0

    .line 653
    .local v0, "initWidth":I
    if-le p1, p2, :cond_1

    const/4 v1, 0x1

    .line 654
    .local v1, "isWideClip":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 655
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v2, :cond_0

    if-nez v1, :cond_0

    .line 656
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 658
    :cond_0
    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(III)[I

    move-result-object v2

    return-object v2

    .line 653
    .end local v1    # "isWideClip":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWidthAndHeight(III)[I
    .locals 7
    .param p1, "windowWidth"    # I
    .param p2, "videoWidth"    # I
    .param p3, "videoHeight"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 662
    const/4 v4, 0x2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    .line 664
    .local v0, "i":[I
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 665
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080039

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    aput v4, v0, v3

    .line 666
    aget v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    aput v3, v0, v2

    .line 677
    :goto_0
    return-object v0

    .line 668
    :cond_1
    if-le p2, p3, :cond_2

    move v1, v2

    .line 669
    .local v1, "isWideClip":Z
    :goto_1
    if-eqz v1, :cond_3

    .line 670
    aput p1, v0, v3

    .line 671
    int-to-float v3, p1

    int-to-float v4, p2

    int-to-float v5, p3

    div-float/2addr v4, v5

    div-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v0, v2

    goto :goto_0

    .end local v1    # "isWideClip":Z
    :cond_2
    move v1, v3

    .line 668
    goto :goto_1

    .line 673
    .restart local v1    # "isWideClip":Z
    :cond_3
    int-to-float v4, p1

    int-to-float v5, p3

    int-to-float v6, p2

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v0, v3

    .line 674
    aput p1, v0, v2

    goto :goto_0

    .line 662
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public hasHwMenukey()Z
    .locals 4

    .prologue
    .line 330
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    .line 331
    .local v0, "hasMenu":Z
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hasHwMenukey. hasMenu : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    return v0
.end method

.method public hasNFCSystemFeature()Z
    .locals 2

    .prologue
    .line 1744
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VideoPlayerApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1746
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-nez v0, :cond_0

    .line 1747
    const/4 v1, 0x0

    .line 1749
    :goto_0
    return v1

    :cond_0
    const-string v1, "android.hardware.nfc"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public insertLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "appId"    # Ljava/lang/String;
    .param p2, "feature"    # Ljava/lang/String;

    .prologue
    .line 1570
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getVersionOfContextProviders()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    .line 1588
    :goto_0
    return-void

    .line 1574
    :cond_0
    const-string v4, "content://com.samsung.android.providers.context.log.use_app_feature_survey"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1578
    .local v3, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1579
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1580
    .local v2, "row":Landroid/content/ContentValues;
    const-string v4, "app_id"

    invoke-virtual {v2, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    const-string v4, "feature"

    invoke-virtual {v2, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1582
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 1583
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "ContextProvider insertion operation is performed."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1584
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "row":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    .line 1585
    .local v1, "ex":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "Error while using the ContextProvider"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public is2ndScreenMode()Z
    .locals 1

    .prologue
    .line 1797
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1798
    const/4 v0, 0x1

    .line 1799
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBrowser(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 837
    const-string v0, "http"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rtsp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sshttp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 838
    :cond_0
    const/4 v0, 0x1

    .line 840
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCallState(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 184
    if-nez p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return v1

    .line 186
    :cond_1
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 187
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "TelephonyManager checkIsCalling tm.getCallState() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isCriticalLowBatteryStatus()Z
    .locals 1

    .prologue
    .line 1793
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsCriticalLowBatteryStatus:Z

    return v0
.end method

.method public isDLNAmode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 959
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v2, :cond_1

    .line 960
    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPlayerMode:I

    if-ne v2, v0, :cond_0

    .line 963
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 960
    goto :goto_0

    :cond_1
    move v0, v1

    .line 963
    goto :goto_0
.end method

.method public isEmergencymode(Landroid/content/Context;)Z
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1555
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 1565
    :goto_0
    return v1

    .line 1556
    :catch_0
    move-exception v0

    .line 1557
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 1564
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "isEmergencymode flag is false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1565
    const/4 v1, 0x0

    goto :goto_0

    .line 1558
    :catch_1
    move-exception v0

    .line 1559
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 1560
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 1561
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public isHdmiConnected(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1275
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isHdmiConnected()Z

    move-result v0

    return v0
.end method

.method public isInLockTaskMode(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2018
    if-nez p1, :cond_0

    .line 2019
    const/4 v1, 0x0

    .line 2022
    :goto_0
    return v1

    .line 2021
    :cond_0
    const-string v1, "activity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2022
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->isInLockTaskMode()Z

    move-result v1

    goto :goto_0
.end method

.method public isInformationPreviewEnable(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1279
    if-nez p1, :cond_0

    .line 1280
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "isInformationPreviewEnable context is NULL"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1288
    :goto_0
    return v3

    .line 1284
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_1

    move v0, v2

    .line 1285
    .local v0, "fingerAirView":Z
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "finger_air_view_information_preview"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_2

    move v1, v2

    .line 1287
    .local v1, "fingerAirViewInformationPreview":Z
    :goto_2
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isInformationPreviewEnable E. fingerAirView : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", fingerAirViewInformationPreview : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1288
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    :goto_3
    move v3, v2

    goto :goto_0

    .end local v0    # "fingerAirView":Z
    .end local v1    # "fingerAirViewInformationPreview":Z
    :cond_1
    move v0, v3

    .line 1284
    goto :goto_1

    .restart local v0    # "fingerAirView":Z
    :cond_2
    move v1, v3

    .line 1285
    goto :goto_2

    .restart local v1    # "fingerAirViewInformationPreview":Z
    :cond_3
    move v2, v3

    .line 1288
    goto :goto_3
.end method

.method public isKnoxMode(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1616
    if-nez p1, :cond_0

    .line 1622
    :goto_0
    return v1

    .line 1619
    :cond_0
    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 1620
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkKnoxMode(Landroid/content/Context;)V

    .line 1622
    :cond_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    if-ne v2, v0, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public isLayoutShowing(Lcom/sec/android/app/videoplayer/common/VUtils$myView;)Z
    .locals 4
    .param p1, "view"    # Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    .prologue
    .line 1298
    const/4 v0, 0x0

    .line 1300
    .local v0, "cont":I
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    if-eq p1, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1301
    add-int/lit8 v0, v0, 0x1

    .line 1304
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->SUBTITLE_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    if-eq p1, v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSubtitleViewVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1305
    add-int/lit8 v0, v0, 0x1

    .line 1308
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isLayoutShowing : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1309
    if-lez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMWTrayOpen(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 936
    const/4 v3, 0x0

    .line 937
    .local v3, "retVal":Z
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v6, :cond_0

    .line 939
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "multi_window_expanded"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v6

    if-ne v6, v4, :cond_1

    move v2, v4

    .line 940
    .local v2, "isExpanded":Z
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "multi_window_enabled"

    invoke-static {v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v6

    if-ne v6, v4, :cond_2

    move v1, v4

    .line 941
    .local v1, "isEnabled":Z
    :goto_1
    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    move v3, v4

    .line 948
    .end local v1    # "isEnabled":Z
    .end local v2    # "isExpanded":Z
    :cond_0
    :goto_2
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isMWTrayOpen : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    return v3

    :cond_1
    move v2, v5

    .line 939
    goto :goto_0

    .restart local v2    # "isExpanded":Z
    :cond_2
    move v1, v5

    .line 940
    goto :goto_1

    .restart local v1    # "isEnabled":Z
    :cond_3
    move v3, v5

    .line 941
    goto :goto_2

    .line 942
    .end local v1    # "isEnabled":Z
    .end local v2    # "isExpanded":Z
    :catch_0
    move-exception v0

    .line 943
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "isMWTrayOpen : NullPointerException"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 944
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 945
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v5, "isMWTrayOpen : SettingNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public isMiniModeServiceRunning(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 770
    const-string v0, "com.sec.android.app.videoplayer.miniapp.MiniVideoPlayerService"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isMoviePlayerHasWindowFocus()Z
    .locals 1

    .prologue
    .line 1254
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mHasWindowFocus:Z

    return v0
.end method

.method public isMultiOrNoAudioChannel()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1811
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1844
    :cond_0
    :goto_0
    return v9

    .line 1813
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v5

    .line 1814
    .local v5, "path":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getAudioTrackChannels(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1815
    .local v4, "multiAudioChannel":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getAudioTrackCount()I

    move-result v2

    .line 1817
    .local v2, "audioTrackCount":I
    if-eqz v4, :cond_7

    .line 1818
    if-lez v2, :cond_3

    move v7, v2

    :goto_1
    new-array v0, v7, [Ljava/lang/String;

    .line 1819
    .local v0, "AudioChannelArray":[Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 1820
    .local v6, "temp":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 1821
    .local v1, "audioChannel":I
    sget-object v7, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isMultiAudioChannel() temp.length: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v6

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " audioTrackCount="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v2, :cond_0

    .line 1824
    array-length v7, v6

    if-ge v7, v2, :cond_5

    .line 1825
    array-length v7, v6

    if-ge v3, v7, :cond_4

    .line 1826
    aget-object v7, v6, v3

    aput-object v7, v0, v3

    .line 1834
    :goto_3
    aget-object v7, v0, v3

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1835
    sget-object v7, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isMultiAudioChannel() AudioChannelArray["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v11, v0, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " audioChannel="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1837
    const/4 v7, 0x2

    if-gt v1, v7, :cond_2

    if-nez v1, :cond_6

    :cond_2
    move v9, v8

    goto/16 :goto_0

    .end local v0    # "AudioChannelArray":[Ljava/lang/String;
    .end local v1    # "audioChannel":I
    .end local v3    # "i":I
    .end local v6    # "temp":[Ljava/lang/String;
    :cond_3
    move v7, v8

    .line 1818
    goto :goto_1

    .line 1828
    .restart local v0    # "AudioChannelArray":[Ljava/lang/String;
    .restart local v1    # "audioChannel":I
    .restart local v3    # "i":I
    .restart local v6    # "temp":[Ljava/lang/String;
    :cond_4
    const-string v7, "0"

    aput-object v7, v0, v3

    goto :goto_3

    .line 1831
    :cond_5
    aget-object v7, v6, v3

    aput-object v7, v0, v3

    goto :goto_3

    .line 1823
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1840
    .end local v0    # "AudioChannelArray":[Ljava/lang/String;
    .end local v1    # "audioChannel":I
    .end local v3    # "i":I
    .end local v6    # "temp":[Ljava/lang/String;
    :cond_7
    sget-object v7, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isMultiAudioChannel() multiAudioChannel is NULL : audioTrackCount="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841
    if-gtz v2, :cond_8

    :goto_4
    move v9, v8

    goto/16 :goto_0

    :cond_8
    move v8, v9

    goto :goto_4
.end method

.method public isMultiWindow()Z
    .locals 3

    .prologue
    .line 348
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 350
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 361
    :goto_0
    return v1

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 360
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :cond_0
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "isMultiWindow flag is false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const/4 v1, 0x0

    goto :goto_0

    .line 353
    :catch_1
    move-exception v0

    .line 354
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 355
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 356
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public isMultiwindowMinimumControllerMode()Z
    .locals 2

    .prologue
    .line 1331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mControllerMode:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_MINIMUM:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 6
    .param p1, "PackageName"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 622
    if-nez p2, :cond_0

    .line 630
    :goto_0
    return v2

    .line 623
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 625
    .local v1, "pkm":Landroid/content/pm/PackageManager;
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v1, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 630
    const/4 v2, 0x1

    goto :goto_0

    .line 626
    :catch_0
    move-exception v0

    .line 627
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to find Package : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isPenWindow()Z
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method public isPlayingFromPersonalPage(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1626
    const/4 v2, 0x0

    .line 1627
    .local v2, "secretDir":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 1630
    .local v1, "filePath":Ljava/lang/String;
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 1637
    :goto_0
    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    return v3

    .line 1631
    :catch_0
    move-exception v0

    .line 1632
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "isPlayingFromPersonalPage : NoClassDefFoundError"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1633
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 1634
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "isPlayingFromPersonalPage : NoSuchMethodError"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1637
    .end local v0    # "e":Ljava/lang/NoSuchMethodError;
    :cond_0
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public isPlusCallActive()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 195
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->UWA_CALL:Z

    if-nez v1, :cond_0

    .line 219
    :goto_0
    return v8

    .line 197
    :cond_0
    const/4 v8, 0x0

    .line 198
    .local v8, "ret":Z
    const/4 v6, 0x0

    .line 200
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 201
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 202
    const-string v1, "content://com.uplus.ipagent.SettingsProvider/system"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v10, "setting_pluscall_active"

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 203
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    const-string v1, "value"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 205
    .local v9, "value":Ljava/lang/String;
    if-eqz v9, :cond_1

    const-string v1, "1"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 206
    const/4 v8, 0x1

    .line 212
    .end local v9    # "value":Ljava/lang/String;
    :cond_1
    if-eqz v6, :cond_2

    .line 213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 214
    const/4 v6, 0x0

    .line 218
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPlusCallActive() :: ret = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 209
    :catch_0
    move-exception v7

    .line 210
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    if-eqz v6, :cond_2

    .line 213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 214
    const/4 v6, 0x0

    goto :goto_1

    .line 212
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 213
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 214
    const/4 v6, 0x0

    :cond_3
    throw v1
.end method

.method public isPortraitVideoClip(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1026
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getVideoClipWidthHeight(Ljava/lang/String;)[I

    move-result-object v0

    .line 1027
    .local v0, "i":[I
    aget v3, v0, v1

    aget v4, v0, v2

    if-le v3, v4, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public isPresentationServiceRunning(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 774
    const-string v0, "com.sec.android.app.videoplayer.service.Presentationservice"

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRestrictedProfileMode(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1642
    if-nez p1, :cond_0

    .line 1643
    const/4 v1, 0x0

    .line 1648
    :goto_0
    return v1

    .line 1645
    :cond_0
    const-string v2, "user"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 1646
    .local v0, "mUserManager":Landroid/os/UserManager;
    invoke-virtual {v0}, Landroid/os/UserManager;->isLinkedUser()Z

    move-result v1

    .line 1647
    .local v1, "ret":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isRestrictedProfileMode.  ret : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isRmsConnected(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 223
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MIRROR_CALL:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v1, v10

    .line 245
    :goto_0
    return v1

    .line 225
    :cond_1
    const/4 v8, 0x0

    .line 226
    .local v8, "status":I
    const/4 v6, 0x0

    .line 228
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 229
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_2

    .line 230
    const-string v1, "content://com.lguplus.rms/service"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 231
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 232
    const-string v1, "connected"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 238
    :cond_2
    if-eqz v6, :cond_3

    .line 239
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 240
    const/4 v6, 0x0

    .line 244
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_3
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isRmsConnected() :: status = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    if-ne v8, v9, :cond_5

    move v1, v9

    goto :goto_0

    .line 235
    :catch_0
    move-exception v7

    .line 236
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    if-eqz v6, :cond_3

    .line 239
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 240
    const/4 v6, 0x0

    goto :goto_1

    .line 238
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_4

    .line 239
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 240
    const/4 v6, 0x0

    :cond_4
    throw v1

    :cond_5
    move v1, v10

    .line 245
    goto :goto_0
.end method

.method public isSbeamAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1753
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsSbeamAvailable:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 1754
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSbeamPkgAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasNFCSystemFeature()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsSbeamAvailable:I

    .line 1756
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsSbeamAvailable:I

    if-ne v0, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 1754
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1756
    goto :goto_1
.end method

.method public isScaleWindow()Z
    .locals 3

    .prologue
    .line 375
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 377
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isScaleWindow()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    .line 387
    :goto_0
    return v1

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    .line 386
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :cond_0
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "isScaleWindow flag is false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    const/4 v1, 0x0

    goto :goto_0

    .line 380
    :catch_1
    move-exception v0

    .line 381
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_1

    .line 382
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 383
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public isSelectedbyChangePlayer()Z
    .locals 1

    .prologue
    .line 977
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSelectedbyChangePlayer:Z

    return v0
.end method

.method public isServiceRunning(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "serviceName"    # Ljava/lang/String;

    .prologue
    .line 844
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 845
    const-string v4, "activity"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 847
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v4, 0x64

    :try_start_0
    invoke-virtual {v0, v4}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 848
    .local v3, "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    iget-object v4, v3, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-eqz v4, :cond_0

    .line 849
    const/4 v4, 0x1

    .line 856
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "runningServiceInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :goto_0
    return v4

    .line 852
    .restart local v0    # "am":Landroid/app/ActivityManager;
    :catch_0
    move-exception v1

    .line 853
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 856
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isSingleAudioTrack()Z
    .locals 2

    .prologue
    .line 1803
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getAudioTrackCount()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 1804
    const/4 v0, 0x0

    .line 1806
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSlowCPU()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1246
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_readCoreNum()I

    move-result v1

    if-eq v1, v0, :cond_0

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLOW_CPU:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSmartPauseOn(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1402
    if-nez p1, :cond_1

    move v2, v3

    .line 1409
    :cond_0
    :goto_0
    return v2

    .line 1404
    :cond_1
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SETTINGS_MOTION_CONCEPT_2014:Z

    if-eqz v4, :cond_5

    .line 1405
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "motion_merged_mute_pause"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-lez v4, :cond_3

    move v0, v2

    .line 1406
    .local v0, "motionState":Z
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "smart_pause"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    .line 1407
    .local v1, "pauseState":Z
    :goto_2
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v4, :cond_2

    if-eqz v0, :cond_2

    if-nez v1, :cond_0

    :cond_2
    move v2, v3

    goto :goto_0

    .end local v0    # "motionState":Z
    .end local v1    # "pauseState":Z
    :cond_3
    move v0, v3

    .line 1405
    goto :goto_1

    .restart local v0    # "motionState":Z
    :cond_4
    move v1, v3

    .line 1406
    goto :goto_2

    .line 1409
    .end local v0    # "motionState":Z
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v4, :cond_6

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "smart_pause"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-nez v4, :cond_0

    :cond_6
    move v2, v3

    goto :goto_0
.end method

.method public isTalkBackOn(Landroid/content/Context;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 514
    const/16 v1, 0x3a

    .line 515
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 516
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v8, 0x3a

    invoke-direct {v6, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 517
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p1, :cond_1

    .line 518
    sget-object v8, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v9, "isTalkBackOn E. context is null"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    :cond_0
    :goto_0
    return v7

    .line 521
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 523
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 524
    const-string v5, ""

    .line 527
    :cond_2
    move-object v2, v6

    .line 529
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 531
    :cond_3
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 532
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 533
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 535
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_3

    .line 536
    const-string v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 537
    sget-object v7, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v8, "isTalkBackOn() TRUE"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const/4 v7, 0x1

    goto :goto_0
.end method

.method public isTopLocationMultiwindow()Z
    .locals 3

    .prologue
    .line 336
    const/4 v0, 0x0

    .line 337
    .local v0, "ret":Z
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 338
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v1

    sget v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_A:I

    if-ne v1, v2, :cond_0

    .line 340
    const/4 v0, 0x1

    .line 344
    :cond_0
    return v0
.end method

.method public isWFDmode()Z
    .locals 2

    .prologue
    .line 967
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPlayerMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public launchHome(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 608
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "launchHome E"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    if-nez p1, :cond_0

    .line 611
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "launchHome context is Null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :goto_0
    return-void

    .line 615
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 616
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 617
    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 618
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public launchVideoList(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 584
    if-nez p1, :cond_0

    .line 585
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "launchVideoList context is Null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :goto_0
    return-void

    .line 589
    :cond_0
    const/4 v0, 0x0

    .line 591
    .local v0, "compName":Landroid/content/ComponentName;
    const-string v2, "com.samsung.everglades.video"

    invoke-virtual {p0, v2, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 592
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "compName":Landroid/content/ComponentName;
    const-string v2, "com.samsung.everglades.video"

    const-string v3, "com.samsung.everglades.video.VideoMain"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    .restart local v0    # "compName":Landroid/content/ComponentName;
    :goto_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.MAIN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 601
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 602
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 603
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 604
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 593
    .end local v1    # "i":Landroid/content/Intent;
    :cond_1
    const-string v2, "com.sec.android.app.videolist"

    invoke-virtual {p0, v2, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 594
    new-instance v0, Landroid/content/ComponentName;

    .end local v0    # "compName":Landroid/content/ComponentName;
    const-string v2, "com.sec.android.app.videolist"

    const-string v3, "com.sec.android.app.videolist.activity.VPMainTab"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .restart local v0    # "compName":Landroid/content/ComponentName;
    goto :goto_1

    .line 596
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v3, "launchVideoList List not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public normalWindow()V
    .locals 3

    .prologue
    .line 407
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 409
    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->normalWindow()V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 418
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "Change to normal window"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    return-void

    .line 410
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_0

    .line 412
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 414
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 415
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public openPopupPlayer(Landroid/content/Context;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1413
    const-string v21, "com.sec.android.app.videoplayer"

    const-string v22, "VPPP"

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1414
    if-nez p1, :cond_1

    .line 1415
    sget-object v21, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v22, "openAppInApp : context is null "

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    :cond_0
    :goto_0
    return-void

    .line 1419
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v21

    if-nez v21, :cond_0

    .line 1423
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v14

    .line 1424
    .local v14, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v10

    .local v10, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    move-object/from16 v21, p1

    .line 1426
    check-cast v21, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getExitByAppinApp()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 1427
    sget-object v21, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v22, "openAppInApp. ((MoviePlayer)mContext).getExitByAppinApp() "

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1431
    :cond_2
    new-instance v4, Landroid/content/ComponentName;

    const-class v21, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-direct {v4, v0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1432
    .local v4, "cn":Landroid/content/ComponentName;
    new-instance v21, Landroid/content/Intent;

    const-string v22, "com.samsung.action.MINI_MODE_SERVICE"

    invoke-direct/range {v21 .. v22}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v7

    .line 1434
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v18

    .line 1435
    .local v18, "videoWidth":I
    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v17

    .line 1436
    .local v17, "videoHeight":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v11

    .line 1437
    .local v11, "screen":[I
    const/16 v21, 0x0

    aget v13, v11, v21

    .line 1438
    .local v13, "screenWidth":I
    const/16 v21, 0x1

    aget v12, v11, v21

    .line 1440
    .local v12, "screenHeight":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v13, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInitAIAxyPosition(III)[I

    move-result-object v6

    .line 1441
    .local v6, "i":[I
    const/16 v21, 0x0

    aget v19, v6, v21

    .line 1442
    .local v19, "x":I
    const/16 v21, 0x1

    aget v20, v6, v21

    .line 1444
    .local v20, "y":I
    sget-boolean v21, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v21, :cond_d

    .line 1445
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1446
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1447
    const/16 v21, 0x1

    aget v13, v11, v21

    .line 1448
    const/16 v21, 0x0

    aget v12, v11, v21

    .line 1450
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->setUserOrientation(I)V

    .line 1466
    :goto_1
    const-string v21, "window.pos.x"

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1467
    const-string v21, "window.pos.y"

    move-object/from16 v0, v21

    move/from16 v1, v20

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1468
    const-string v21, "videoWidth"

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1469
    const-string v21, "videoHeight"

    move-object/from16 v0, v21

    move/from16 v1, v17

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1470
    const-string v21, "screenWidth"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1471
    const-string v21, "screenHeight"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1472
    const-string v21, "filePath"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1473
    const-string v21, "currentID"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v22

    move-object/from16 v0, v21

    move-wide/from16 v1, v22

    invoke-virtual {v7, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1474
    const-string v21, "ListKey"

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getKeyType()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1475
    const-string v21, "WhereFrom"

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getFrom()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1476
    const-string v21, "playSpeed"

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1477
    const-string v21, "audioTrack"

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSelectedAudioTrack()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1478
    const-string v21, "ListType"

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getListType()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1479
    const-string v21, "uri"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1480
    const-string v21, "bucketid"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getBucketID()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1481
    const-string v21, "calledScalewindow"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isScaleWindow()Z

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1482
    const-string v21, "ScalewindowRect"

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1484
    const/4 v9, 0x0

    .line 1486
    .local v9, "isSideSyncPlaying":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v21

    if-eqz v21, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isFromSideSync()Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1487
    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->getTitleofSideSync()Ljava/lang/String;

    move-result-object v16

    .line 1488
    .local v16, "titleName":Ljava/lang/String;
    if-nez v16, :cond_4

    .line 1489
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v16

    .line 1492
    :cond_4
    const-string v21, "title_of_sidesync"

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1493
    const-string v21, "needSaveResumePos"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isNeedSaveResumePos()Z

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1494
    const-string v21, "isFromSideSync"

    const/16 v22, 0x1

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1495
    const/4 v9, 0x1

    .line 1498
    .end local v16    # "titleName":Ljava/lang/String;
    :cond_5
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1499
    const-string v21, "resumePos"

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1500
    sget-object v21, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "openAIA. MINIMODE_RESUME_POS : "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    if-nez v9, :cond_6

    .line 1503
    const/16 v21, 0x1

    const/16 v22, 0x0

    move/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1509
    :cond_6
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v5

    .line 1510
    .local v5, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isFromStore()Z

    move-result v8

    .line 1511
    .local v8, "isFromStore":Z
    const-string v21, "isfromMovieStore"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1513
    if-eqz v8, :cond_10

    .line 1514
    const-string v21, "title"

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1515
    const-string v21, "yosemite_enable"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->isWatchONEnable()Z

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1521
    :goto_3
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromDms()Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1522
    const-string v21, "title_name"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getDmsContentTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1526
    :cond_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v15

    .line 1528
    .local v15, "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    const-string v22, "subtitleActivation"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v21

    if-eqz v21, :cond_11

    const/16 v21, 0x1

    :goto_4
    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1529
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1530
    const-string v21, "subtitleFile"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1531
    const-string v21, "subtitleLanguageIdx"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1532
    const-string v21, "subtitleIsMulti"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1533
    const-string v21, "subtitleMultiCount"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1534
    const-string v21, "subtitleMultiIndex"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 1535
    const-string v21, "subtitleTextSize"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1536
    const-string v21, "subttileSyncTime"

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v22

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1539
    :cond_8
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isMovieStoreContent()Z

    move-result v21

    if-eqz v21, :cond_9

    move-object/from16 v21, p1

    .line 1540
    check-cast v21, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastPausedPosition(Z)V

    .line 1543
    :cond_9
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1544
    const-string v21, "CurrentProviderName"

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getProvider()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1547
    :cond_a
    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    move-object/from16 v21, p1

    .line 1549
    check-cast v21, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitMovieplayer4AppInApp()V

    .line 1550
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 1452
    .end local v5    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .end local v8    # "isFromStore":Z
    .end local v9    # "isSideSyncPlaying":Z
    .end local v15    # "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    :cond_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenLandscape()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1453
    const/16 v21, 0x1

    aget v13, v11, v21

    .line 1454
    const/16 v21, 0x0

    aget v12, v11, v21

    .line 1456
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenOrientation()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSecondScreenUserOrientation(I)V

    goto/16 :goto_1

    .line 1459
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1460
    const/16 v21, 0x1

    aget v13, v11, v21

    .line 1461
    const/16 v21, 0x0

    aget v12, v11, v21

    .line 1463
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->setUserOrientation(I)V

    goto/16 :goto_1

    .line 1506
    .restart local v9    # "isSideSyncPlaying":Z
    :cond_f
    const-string v21, "resumePos"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 1517
    .restart local v5    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .restart local v8    # "isFromStore":Z
    :cond_10
    const-string v21, "title"

    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1518
    const-string v21, "yosemite_enable"

    const/16 v22, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3

    .line 1528
    .restart local v15    # "subtitleUtil":Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    :cond_11
    const/16 v21, 0x0

    goto/16 :goto_4
.end method

.method public openShareVia(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1349
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->openShareVia(Landroid/content/Context;Landroid/net/Uri;)V

    .line 1350
    return-void
.end method

.method public openShareVia(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "videoUri"    # Landroid/net/Uri;

    .prologue
    .line 1353
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1354
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "onOptionsItemSelected() - uri is null."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    :goto_0
    return-void

    .line 1358
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1359
    .local v0, "shareIntent":Landroid/content/Intent;
    const-string v1, "video/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1360
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1367
    const v1, 0x7f0a00a3

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public releaseBusBooster()V
    .locals 0

    .prologue
    .line 327
    return-void
.end method

.method public releaseCpuBooster()V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method public requestSystemKeyEvent(Landroid/content/Context;IZ)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "keyCode"    # I
    .param p3, "request"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1258
    if-nez p1, :cond_1

    .line 1271
    .end local p1    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v2

    .line 1262
    .restart local p1    # "context":Landroid/content/Context;
    :cond_1
    const-string v3, "window"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 1265
    .local v1, "windowmanager":Landroid/view/IWindowManager;
    :try_start_0
    instance-of v3, p1, Landroid/app/Activity;

    if-eqz v3, :cond_0

    .line 1266
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-interface {v1, p2, v3, p3}, Landroid/view/IWindowManager;->requestSystemKeyEvent(ILandroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1267
    :catch_0
    move-exception v0

    .line 1268
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public resetColor()V
    .locals 1

    .prologue
    .line 869
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NONE:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevColor:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 870
    return-void
.end method

.method public restorScreenMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1998
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUIMode(I)V

    .line 1999
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setTconUIMode(I)V

    .line 2000
    const-string v0, "video"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 2002
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v0, :cond_0

    .line 2003
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevIsAdaptive:I

    if-nez v0, :cond_1

    .line 2004
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPrevScreenMode:I

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    .line 2009
    :cond_0
    :goto_0
    return-void

    .line 2006
    :cond_1
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    goto :goto_0
.end method

.method public setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V
    .locals 0
    .param p1, "controllerMode"    # Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .prologue
    .line 1323
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mControllerMode:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1324
    return-void
.end method

.method public setHapticEffect(Landroid/content/Context;Landroid/view/View;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 1238
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_sound_and_haptic_feedback"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1239
    const-string v1, "audio"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 1240
    .local v0, "mAudioManager":Landroid/media/AudioManager;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1241
    const/16 v1, 0x9

    invoke-virtual {p2, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 1243
    .end local v0    # "mAudioManager":Landroid/media/AudioManager;
    :cond_0
    return-void
.end method

.method public setIsCriticalLowBatteryStatus(Landroid/content/Context;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "status"    # Z

    .prologue
    .line 1786
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsCriticalLowBatteryStatus:Z

    if-nez v0, :cond_0

    .line 1787
    invoke-static {p1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.critical.low.batt"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1789
    :cond_0
    iput-boolean p2, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsCriticalLowBatteryStatus:Z

    .line 1790
    return-void
.end method

.method public setMoviePlayerWindowFocus(Z)V
    .locals 0
    .param p1, "focus"    # Z

    .prologue
    .line 1250
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mHasWindowFocus:Z

    .line 1251
    return-void
.end method

.method public setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    .prologue
    .line 930
    if-eqz p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLayoutShowing(Lcom/sec/android/app/videoplayer/common/VUtils$myView;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 931
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    .line 933
    :cond_0
    return-void
.end method

.method public setPlayerMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 971
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mPlayerMode:I

    .line 972
    return-void
.end method

.method public setRemoveSystemUI(Z)V
    .locals 6
    .param p1, "remove"    # Z

    .prologue
    .line 1213
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_2

    .line 1214
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 1215
    .local v2, "window":Landroid/view/Window;
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1216
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSystemUIRemove ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") current systemUiVisibility : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v1

    .line 1220
    .local v1, "systemUiVisibility":I
    if-eqz p1, :cond_1

    .line 1222
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    or-int/2addr v3, v1

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 1230
    :cond_0
    :goto_0
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setRemoveSystemUI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1235
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "systemUiVisibility":I
    .end local v2    # "window":Landroid/view/Window;
    :goto_1
    return-void

    .line 1225
    .restart local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v1    # "systemUiVisibility":I
    .restart local v2    # "window":Landroid/view/Window;
    :cond_1
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    and-int/2addr v3, v1

    if-eqz v3, :cond_0

    .line 1226
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    xor-int/2addr v3, v1

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    goto :goto_0

    .line 1233
    .end local v0    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v1    # "systemUiVisibility":I
    .end local v2    # "window":Landroid/view/Window;
    :cond_2
    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v4, "setRemoveSystemUI : mApp is NULL!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setSelectedbyChangePlayer(Z)V
    .locals 0
    .param p1, "selected"    # Z

    .prologue
    .line 981
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mSelectedbyChangePlayer:Z

    .line 982
    return-void
.end method

.method public setVUtilsData(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    .line 116
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->resetColor()V

    .line 119
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsKnoxMode:I

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mIsCriticalLowBatteryStatus:Z

    .line 121
    return-void
.end method

.method public setWindowBackgroundColor()V
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 927
    :goto_0
    return-void

    .line 926
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    goto :goto_0
.end method

.method public setWindowBackgroundColor(Landroid/view/Window;)V
    .locals 3
    .param p1, "win"    # Landroid/view/Window;

    .prologue
    .line 895
    if-nez p1, :cond_0

    .line 896
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "setWindowBackgroundColor win is NULL!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    :goto_0
    return-void

    .line 900
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isStateViewVisible()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 904
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 905
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "setWindowBackgroundColor black"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 910
    :cond_4
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NULL:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 911
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v1, "setWindowBackgroundColor null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setWindowBackgroundColor : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ro.product.board"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setWindowFlag(ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V
    .locals 7
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "type"    # I
    .param p3, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 433
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v4, p3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 435
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 436
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 437
    .local v3, "privateFlags":I
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 439
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    packed-switch p2, :pswitch_data_0

    .line 447
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 451
    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 452
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :goto_1
    return-void

    .line 441
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v3    # "privateFlags":I
    :pswitch_0
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 456
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - SecurityException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 444
    .end local v0    # "e":Ljava/lang/SecurityException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v3    # "privateFlags":I
    :pswitch_1
    :try_start_1
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/2addr v4, v3

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    .line 458
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :catch_1
    move-exception v0

    .line 459
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NullPointerException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 454
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :try_start_2
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 460
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NoSuchFieldException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 462
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v0

    .line 463
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalArgumentException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 464
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 465
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalAccessException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 439
    nop

    :pswitch_data_0
    .packed-switch 0x308
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 726
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    .line 727
    return-void
.end method

.method public showToast(II)V
    .locals 5
    .param p1, "resID"    # I
    .param p2, "Length"    # I

    .prologue
    .line 730
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 749
    :goto_0
    return-void

    .line 732
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 733
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v2, "showToast : Lock Screen is on! Do not show"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 737
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_2

    .line 738
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    .line 744
    :goto_1
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "toast_y_offset"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 745
    .local v0, "rID":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    const/16 v2, 0x51

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 748
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 740
    .end local v0    # "rID":I
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_1
.end method

.method public showToast(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/CharSequence;

    .prologue
    .line 705
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(Ljava/lang/CharSequence;I)V

    .line 706
    return-void
.end method

.method public showToast(Ljava/lang/CharSequence;I)V
    .locals 5
    .param p1, "str"    # Ljava/lang/CharSequence;
    .param p2, "Length"    # I

    .prologue
    .line 709
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 723
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_1

    .line 712
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    .line 718
    :goto_1
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "toast_y_offset"

    const-string v3, "dimen"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 719
    .local v0, "rID":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    const/16 v2, 0x51

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 722
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 714
    .end local v0    # "rID":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public showToastTitleController(I)V
    .locals 4
    .param p1, "resID"    # I

    .prologue
    const/4 v3, 0x0

    .line 752
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 767
    :goto_0
    return-void

    .line 754
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 756
    .local v0, "height":I
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v1, :cond_1

    .line 757
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x14

    add-int/2addr v0, v1

    .line 760
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    if-eqz v1, :cond_2

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->cancel()V

    .line 764
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    const/16 v2, 0x51

    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public turnOffTalkBack(Landroid/content/Context;)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v10, 0x3a

    .line 546
    const/16 v1, 0x3a

    .line 547
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 548
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-direct {v6, v10}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 550
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 552
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 553
    const/4 v8, 0x0

    .line 580
    :goto_0
    return v8

    .line 556
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 557
    .local v2, "builder":Ljava/lang/StringBuilder;
    move-object v3, v6

    .line 560
    .local v3, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-eqz v5, :cond_1

    .line 561
    invoke-virtual {v3, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 564
    :cond_1
    :goto_1
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 565
    invoke-virtual {v3}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v4

    .line 567
    .local v4, "componentNameString":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 568
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 571
    :cond_2
    if-eqz v4, :cond_1

    const-string v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 572
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 576
    .end local v4    # "componentNameString":Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 577
    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.android.settings.action.talkback_off"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 578
    .local v7, "talk_back_off":Landroid/content/Intent;
    invoke-virtual {p1, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 579
    sget-object v8, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    const-string v9, "turnOffTalkBack() TRUE"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    const/4 v8, 0x1

    goto :goto_0
.end method

.method public unsetWindowFlag(ILjava/lang/String;)V
    .locals 1
    .param p1, "type"    # I
    .param p2, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 473
    :goto_0
    return-void

    .line 472
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VUtils;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/app/videoplayer/common/VUtils;->unsetWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public unsetWindowFlag(Landroid/view/Window;ILjava/lang/String;)V
    .locals 7
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "type"    # I
    .param p3, "fieldString"    # Ljava/lang/String;

    .prologue
    .line 477
    :try_start_0
    const-class v4, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v4, p3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 479
    .local v1, "field":Ljava/lang/reflect/Field;
    if-eqz v1, :cond_0

    .line 480
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    .line 481
    .local v3, "privateFlags":I
    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 483
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    packed-switch p2, :pswitch_data_0

    .line 491
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    xor-int/lit8 v5, v3, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 495
    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 496
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unsetWindowFlag :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 511
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :goto_1
    return-void

    .line 485
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v3    # "privateFlags":I
    :pswitch_0
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    xor-int/lit8 v5, v3, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 500
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :catch_0
    move-exception v0

    .line 501
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - SecurityException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 488
    .end local v0    # "e":Ljava/lang/SecurityException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .restart local v3    # "privateFlags":I
    :pswitch_1
    :try_start_1
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    xor-int/lit8 v5, v3, -0x1

    and-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_0

    .line 502
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v3    # "privateFlags":I
    :catch_1
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NullPointerException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 498
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :try_start_2
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unsetWindowFlag - \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' not exist"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 504
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - NoSuchFieldException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 506
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_3
    move-exception v0

    .line 507
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalArgumentException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 508
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 509
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWindowFlag - IllegalAccessException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 483
    nop

    :pswitch_data_0
    .packed-switch 0x308
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
