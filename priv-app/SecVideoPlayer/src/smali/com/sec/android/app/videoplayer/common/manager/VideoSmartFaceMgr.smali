.class public Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;
.super Ljava/lang/Object;
.source "VideoSmartFaceMgr.java"


# static fields
.field private static final START_SMART_PAUSE:I = 0x2710

.field private static mBlockSmartPause:Z

.field private static mPausedbySmartPause:Z

.field private static mPrevNeedtoPause:I

.field private static mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

.field private static mSmartPauseFunctionON:Z


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 31
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I

    .line 35
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    .line 37
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mBlockSmartPause:Z

    .line 39
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-class v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    .line 82
    new-instance v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$1;-><init>(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mHandler:Landroid/os/Handler;

    .line 42
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mContext:Landroid/content/Context;

    .line 43
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 44
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->runSmartPause()V

    return-void
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 20
    sget v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 20
    sput p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->isBlockSmartPause()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$600()Z
    .locals 1

    .prologue
    .line 20
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z

    return v0
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 20
    sput-boolean p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPausedbySmartPause:Z

    return p0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private isBlockSmartPause()Z
    .locals 2

    .prologue
    .line 74
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mBlockSmartPause:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMoviePlayerHasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private runSmartPause()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 97
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSmartPauseOn(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "runSmartPause. mSmartPauseFunctionON "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    if-eqz v0, :cond_1

    .line 101
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-nez v0, :cond_0

    .line 102
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceManager;->getSmartFaceManager()Lcom/samsung/android/smartface/SmartFaceManager;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    .line 105
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_2

    .line 106
    sput-boolean v3, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mBlockSmartPause:Z

    .line 108
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    new-instance v1, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr$2;-><init>(Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceManager;->setListener(Lcom/samsung/android/smartface/SmartFaceManager$SmartFaceInfoListener;)V

    .line 144
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceManager;->start(I)Z

    .line 150
    :cond_1
    :goto_0
    return-void

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    const-string v1, "fail to get SmartFaceManager. Set mSmartPause as false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    sput-boolean v3, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSmartPauseFunctionON:Z

    goto :goto_0
.end method


# virtual methods
.method public blockSmartPause()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mBlockSmartPause:Z

    .line 67
    return-void
.end method

.method public resumeSmartPause()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mBlockSmartPause:Z

    .line 71
    return-void
.end method

.method public startSmartPause()V
    .locals 4

    .prologue
    .line 48
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromHelpProgressBarPreview()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromHelpMotionPeek()Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    const-string v1, "startSmartPause E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x2710

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 53
    :cond_1
    return-void
.end method

.method public stopSmartPause()V
    .locals 2

    .prologue
    .line 56
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v0, :cond_0

    .line 57
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->TAG:Ljava/lang/String;

    const-string v1, "mSFManager.stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    sget-object v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mSFManager:Lcom/samsung/android/smartface/SmartFaceManager;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceManager;->stop()V

    .line 60
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->mPrevNeedtoPause:I

    .line 63
    :cond_0
    return-void
.end method
