.class public Lcom/sec/android/app/videoplayer/view/AudioOnlyView;
.super Landroid/widget/RelativeLayout;
.source "AudioOnlyView.java"


# static fields
.field private static final HLS_EMBEDDED_IMAGE_PATH:Ljava/lang/String; = "/data/.image.jpeg"

.field private static final TAG:Ljava/lang/String; = "AudioOnlyView"


# instance fields
.field private mAudioOnlyImage:Landroid/widget/ImageView;

.field private mAudioOnlyText:Landroid/widget/TextView;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mParentView:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mParentView:Landroid/widget/RelativeLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    .line 39
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mContext:Landroid/content/Context;

    .line 40
    return-void
.end method

.method private addAudioOnlyView()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 53
    const-string v2, "AudioOnlyView"

    const-string v3, "AudioOnlyView - addAudioOnlyView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 57
    .local v0, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->makeView()Landroid/view/View;

    move-result-object v1

    .line 58
    .local v1, "v":Landroid/view/View;
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    return-void
.end method

.method private getAudioOnlyDefaultBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201c1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getAudioOnlyEmbeddedBitmap()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 123
    new-instance v0, Ljava/io/File;

    const-string v1, "/data/.image.jpeg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    const-string v1, "AudioOnlyView"

    const-string v2, "embedded HLS image available"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v1, "/data/.image.jpeg"

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 128
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeView()Landroid/view/View;
    .locals 4

    .prologue
    .line 62
    const-string v2, "AudioOnlyView"

    const-string v3, "AudioOnlyView - makeView"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 65
    .local v1, "inflate":Landroid/view/LayoutInflater;
    const v2, 0x7f03001b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "audioOnlyView":Landroid/view/View;
    const v2, 0x7f0d0115

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    .line 68
    const v2, 0x7f0d0116

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    .line 69
    return-object v0
.end method

.method private setLayoutParams()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 107
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 109
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 110
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 113
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 114
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x1

    .line 43
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 46
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mParentView:Landroid/widget/RelativeLayout;

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->addAudioOnlyView()V

    .line 50
    return-void
.end method

.method public setImage()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "AudioOnlyView"

    const-string v1, "AudioOnlyView - setImage()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 81
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 85
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isHLS()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->getAudioOnlyEmbeddedBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 89
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_5

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->getAudioOnlyDefaultBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 104
    :cond_4
    return-void

    .line 95
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->mAudioOnlyText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 98
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->setLayoutParams()V

    goto :goto_0
.end method
