.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 4
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$102(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnSizeChangedListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;->onSizeChanged(II)V

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v0

    if-gtz v0, :cond_2

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    .line 444
    :goto_0
    return-void

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$2;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    goto :goto_0
.end method
