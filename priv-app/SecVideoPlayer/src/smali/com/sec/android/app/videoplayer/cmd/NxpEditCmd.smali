.class public Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;
.super Ljava/lang/Object;
.source "NxpEditCmd.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;


# static fields
.field private static final INTENT_ACTION:Ljava/lang/String; = "android.intent.action.TRIM"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.lifevibes.trimapp"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    sget-object v4, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;->TAG:Ljava/lang/String;

    const-string v5, "execute"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    const-string v5, "com.sec.android.app.videoplayer"

    const-string v6, "TRIM"

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    if-nez p1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 37
    .local v1, "filePath":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 38
    sget-object v4, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;->TAG:Ljava/lang/String;

    const-string v5, "execute. filePath is NULL"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.TRIM"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 46
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "uri"

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 48
    sget v4, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 49
    sget-object v4, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;->TAG:Ljava/lang/String;

    const-string v5, "If NXP trim exists, send an intent directly to NXP trim"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    const-string v4, "com.lifevibes.trimapp"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    :cond_2
    :try_start_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "excp":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;->TAG:Ljava/lang/String;

    const-string v5, "ActivityNotFoundException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
