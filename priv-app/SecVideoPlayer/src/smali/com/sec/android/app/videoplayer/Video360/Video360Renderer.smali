.class public Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;
.super Ljava/lang/Object;
.source "Video360Renderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# static fields
.field public static final DEG2RAD:F = 0.017453292f

.field public static final PI:F = 3.1415927f

.field public static final RAD2DEG:F = 57.295776f

.field public static final TAG:Ljava/lang/String; = "Video360Renderer"


# instance fields
.field protected m90DegAboutZ:[F

.field private mContext:Landroid/content/Context;

.field protected mEnableSensor:Z

.field private mGyroMode:Z

.field private mLastX1:F

.field private mLastX2:F

.field private mLastY1:F

.field private mLastY2:F

.field private mLocker:Ljava/lang/Object;

.field protected mPMatrix:[F

.field private mPointerId1:I

.field private mPointerId2:I

.field private mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

.field protected mSensorListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

.field mShaders:Lcom/sec/android/app/videoplayer/Video360/Shader;

.field private mShapeHint:I

.field private mSurfaceHeight:I

.field private mSurfaceRatio:F

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mSurfaceWidth:I

.field protected mTMatrix:[F

.field private mTextureID:I

.field protected mTouchSFGLXRotation:F

.field protected mTouchSFGLYRotation:F

.field protected mTouchSFGLZRotation:F

.field protected mVMatrix:[F

.field protected mWordlXYRotation:F

.field protected mXAxis:[F

.field protected mYAxis:[F

.field protected mZAxis:[F

.field protected mtXAxis:[F

.field protected mtYAxis:[F

.field protected mtZAxis:[F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x10

    const/4 v3, 0x0

    const/high16 v7, -0x40800000    # -1.0f

    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mGyroMode:Z

    .line 27
    iput v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX1:F

    .line 28
    iput v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY1:F

    .line 29
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    .line 30
    iput v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX2:F

    .line 31
    iput v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY2:F

    .line 32
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    .line 36
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    .line 37
    iput v5, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceRatio:F

    .line 38
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTextureID:I

    .line 46
    new-array v2, v4, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    .line 47
    new-array v2, v4, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPMatrix:[F

    .line 48
    new-array v2, v4, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTMatrix:[F

    .line 51
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    .line 58
    iput v5, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    .line 61
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mXAxis:[F

    .line 62
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mYAxis:[F

    .line 63
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mZAxis:[F

    .line 64
    new-array v2, v4, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    .line 70
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLocker:Ljava/lang/Object;

    .line 72
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtXAxis:[F

    .line 73
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtYAxis:[F

    .line 74
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtZAxis:[F

    .line 78
    new-instance v2, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;-><init>(Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSensorListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    .line 146
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mContext:Landroid/content/Context;

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTMatrix:[F

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 150
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setGyroModeOn()V

    .line 152
    const/high16 v1, 0x3f800000    # 1.0f

    .line 153
    .local v1, "sinA":F
    const/4 v0, 0x0

    .line 156
    .local v0, "cosA":F
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x1

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x2

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    aput v5, v2, v6

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x4

    aput v7, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x5

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x6

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v3, 0x7

    aput v5, v2, v3

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0x8

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0x9

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xa

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xb

    aput v5, v2, v3

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xc

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xd

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xe

    aput v5, v2, v3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/16 v3, 0xf

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, v2, v3

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLocker:Ljava/lang/Object;

    return-object v0
.end method

.method private declared-synchronized loadTexture()V
    .locals 3

    .prologue
    .line 502
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/graphics/SurfaceTexture;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getTextureID()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 505
    const v0, 0x8d65

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getTextureID()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 507
    const v0, 0x8d65

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 509
    const v0, 0x8d65

    const/16 v1, 0x2800

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 512
    const v0, 0x8d65

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 514
    const v0, 0x8d65

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    monitor-exit p0

    return-void

    .line 502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private setGyroModeOff()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mGyroMode:Z

    .line 202
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->toggleSensor()V

    .line 204
    :cond_0
    return-void
.end method

.method private setGyroModeOn()V
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mGyroMode:Z

    .line 193
    const-string v0, "Video360Renderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setGyroModeOn() mEnableSensor:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    if-nez v0, :cond_0

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->toggleSensor()V

    .line 197
    :cond_0
    return-void
.end method


# virtual methods
.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getGyroMode()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mGyroMode:Z

    return v0
.end method

.method public getMeshShape()I
    .locals 1

    .prologue
    .line 351
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    return v0
.end method

.method protected getScreenMesh()Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    return-object v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method protected getTextureID()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTextureID:I

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 10
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_0

    .line 290
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->loadTexture()V

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTMatrix:[F

    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 297
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getScreenMesh()Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getShape()I

    move-result v2

    if-eq v0, v2, :cond_2

    .line 298
    const-string v0, "Video360Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Changing shape hint: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->releaseBuffer()V

    .line 302
    :cond_1
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->init(ILandroid/content/Context;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->setVisible(Z)V

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setFrustum()V

    .line 308
    const/4 v7, 0x1

    .line 309
    .local v7, "isSet":Z
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShaders:Lcom/sec/android/app/videoplayer/Video360/Shader;

    iget v2, v2, Lcom/sec/android/app/videoplayer/Video360/Shader;->m360PlayerClearProgram:I

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->setProgram(I)Z

    move-result v7

    .line 310
    if-nez v7, :cond_2

    .line 311
    const-string v0, "Video360Renderer"

    const-string v2, "Effect failed"

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    .end local v7    # "isSet":Z
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v4, v4, v4, v0}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 316
    const/16 v0, 0x4100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 318
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->syncVMatrix()V

    .line 321
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getVMatrix()[F

    move-result-object v9

    .line 325
    .local v9, "vMatrix":[F
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v8

    .line 326
    .local v8, "mvpMatrix":[F
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    const/16 v0, 0x10

    if-ge v6, v0, :cond_3

    .line 327
    aget v0, v8, v6

    aput v0, v9, v6

    .line 326
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 330
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPMatrix:[F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->getMVPMatrix()[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 333
    .end local v6    # "i":I
    .end local v8    # "mvpMatrix":[F
    .end local v9    # "vMatrix":[F
    :cond_4
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceHeight:I

    invoke-static {v1, v1, v0, v2}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 334
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getScreenMesh()Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTextureID:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTMatrix:[F

    invoke-virtual {v0, v2, v1, v3}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->draw(II[F)V

    .line 335
    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 12
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 258
    const-string v0, "Video360Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "surface width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " surface height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-static {v1, v1, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 264
    int-to-float v0, p2

    int-to-float v3, p3

    div-float/2addr v0, v3

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceRatio:F

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setFrustum()V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    const/high16 v7, 0x40a00000    # 5.0f

    const/high16 v9, 0x3f800000    # 1.0f

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v8, v2

    move v10, v2

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 270
    iput p2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceWidth:I

    .line 271
    iput p3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceHeight:I

    .line 273
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Shader;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/Video360/Shader;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShaders:Lcom/sec/android/app/videoplayer/Video360/Shader;

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShaders:Lcom/sec/android/app/videoplayer/Video360/Shader;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Shader;->initPrograms(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->init(ILandroid/content/Context;)V

    .line 277
    const/4 v11, 0x1

    .line 279
    .local v11, "isSet":Z
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShaders:Lcom/sec/android/app/videoplayer/Video360/Shader;

    iget v1, v1, Lcom/sec/android/app/videoplayer/Video360/Shader;->m360PlayerClearProgram:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->setProgram(I)Z

    move-result v11

    .line 281
    if-nez v11, :cond_0

    .line 282
    const-string v0, "Video360Renderer"

    const-string v1, "Unable to load"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->setVisible(Z)V

    .line 285
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 6
    .param p1, "glUnused"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 231
    const-string v1, "Video360Renderer"

    const-string v2, "onSurfaceCreated"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/16 v1, 0xbe2

    invoke-static {v1}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 235
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v3, v3, v3, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 236
    const/16 v1, 0xb71

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 237
    const/16 v1, 0x203

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 239
    new-array v0, v5, [I

    .line 240
    .local v0, "textures":[I
    invoke-static {v5, v0, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 242
    aget v1, v0, v4

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTextureID:I

    .line 244
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->loadTexture()V

    .line 246
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    .line 247
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    .line 248
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    .line 250
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    if-eqz v1, :cond_0

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->releaseBuffer()V

    .line 253
    :cond_0
    new-instance v1, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-direct {v1}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    .line 254
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 18
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 378
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v5

    .line 379
    .local v5, "index":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    .line 380
    .local v7, "pointer":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    .line 381
    .local v2, "action":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    .line 382
    .local v10, "x":F
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v12

    .line 387
    .local v12, "y":F
    if-eqz v2, :cond_0

    const/4 v15, 0x5

    if-ne v2, v15, :cond_3

    .line 388
    :cond_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_2

    .line 389
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    .line 390
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX1:F

    .line 391
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY1:F

    .line 499
    :cond_1
    :goto_0
    return-void

    .line 392
    :cond_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_1

    .line 393
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    .line 394
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX2:F

    .line 395
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY2:F

    goto :goto_0

    .line 401
    :cond_3
    const/4 v15, 0x2

    if-eq v2, v15, :cond_4

    const/16 v15, 0x8

    if-ne v2, v15, :cond_a

    .line 403
    :cond_4
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    if-ne v15, v7, :cond_a

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    const/16 v16, -0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_a

    .line 405
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX1:F

    sub-float v15, v10, v15

    const/high16 v16, 0x41400000    # 12.0f

    div-float v11, v15, v16

    .line 406
    .local v11, "xDiff":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY1:F

    sub-float v15, v12, v15

    const/high16 v16, 0x41400000    # 12.0f

    div-float v13, v15, v16

    .line 408
    .local v13, "yDiff":F
    const-wide/16 v16, 0x0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v4, v0

    .line 409
    .local v4, "deg":F
    float-to-double v0, v4

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v8, v0

    .line 410
    .local v8, "sinA":F
    float-to-double v0, v4

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v3, v0

    .line 412
    .local v3, "cosA":F
    mul-float v15, v13, v3

    mul-float v16, v11, v8

    add-float v6, v15, v16

    .line 413
    .local v6, "pitch":F
    mul-float v15, v11, v3

    mul-float v16, v13, v8

    add-float v14, v15, v16

    .line 415
    .local v14, "yaw":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    const-wide v16, 0x4046800000000000L    # 45.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_5

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    const-wide v16, 0x4060e00000000000L    # 135.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_5

    .line 416
    move v9, v6

    .line 417
    .local v9, "temp":F
    move v6, v14

    .line 418
    move v14, v9

    .line 421
    .end local v9    # "temp":F
    :cond_5
    const v15, 0x3c23d70a    # 0.01f

    mul-float/2addr v14, v15

    .line 422
    const v15, 0x3c23d70a    # 0.01f

    mul-float/2addr v6, v15

    .line 424
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const/16 v16, 0x0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_6

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x3f490fdb

    cmpg-float v15, v15, v16

    if-lez v15, :cond_7

    :cond_6
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x3f490fdb

    cmpl-float v15, v15, v16

    if-lez v15, :cond_d

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x4016cbe4

    cmpg-float v15, v15, v16

    if-gez v15, :cond_d

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    const v16, 0x4016cbe4

    cmpl-float v15, v15, v16

    if-ltz v15, :cond_d

    .line 425
    :cond_7
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    sub-float/2addr v15, v14

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    .line 429
    :goto_1
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x4016cbe4

    cmpl-float v15, v15, v16

    if-gez v15, :cond_8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x3f490fdb

    cmpl-float v15, v15, v16

    if-lez v15, :cond_e

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    const v16, 0x4016cbe4

    cmpg-float v15, v15, v16

    if-gez v15, :cond_e

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    const v16, 0x4016cbe4

    cmpl-float v15, v15, v16

    if-ltz v15, :cond_e

    .line 430
    :cond_8
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    sub-float/2addr v15, v6

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    .line 434
    :goto_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    const-wide v16, 0x4056800000000000L    # 90.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    cmpl-float v15, v15, v16

    if-lez v15, :cond_f

    .line 435
    const-wide v16, 0x4056800000000000L    # 90.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    .line 439
    :cond_9
    :goto_3
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX1:F

    .line 440
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY1:F

    .line 481
    .end local v3    # "cosA":F
    .end local v4    # "deg":F
    .end local v6    # "pitch":F
    .end local v8    # "sinA":F
    .end local v11    # "xDiff":F
    .end local v13    # "yDiff":F
    .end local v14    # "yaw":F
    :cond_a
    const/4 v15, 0x3

    if-eq v2, v15, :cond_b

    const/4 v15, 0x1

    if-eq v2, v15, :cond_b

    const/4 v15, 0x6

    if-ne v2, v15, :cond_1

    .line 483
    :cond_b
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    if-ne v7, v15, :cond_c

    .line 484
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId1:I

    .line 485
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX2:F

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX1:F

    .line 486
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY2:F

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY1:F

    .line 487
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    .line 488
    const/high16 v15, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX2:F

    .line 489
    const/high16 v15, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY2:F

    .line 493
    :cond_c
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    if-ne v7, v15, :cond_1

    .line 494
    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPointerId2:I

    .line 495
    const/high16 v15, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastX2:F

    .line 496
    const/high16 v15, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLastY2:F

    goto/16 :goto_0

    .line 427
    .restart local v3    # "cosA":F
    .restart local v4    # "deg":F
    .restart local v6    # "pitch":F
    .restart local v8    # "sinA":F
    .restart local v11    # "xDiff":F
    .restart local v13    # "yDiff":F
    .restart local v14    # "yaw":F
    :cond_d
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    add-float/2addr v15, v14

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    goto/16 :goto_1

    .line 432
    :cond_e
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    add-float/2addr v15, v6

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    goto/16 :goto_2

    .line 436
    :cond_f
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    const-wide v16, -0x3fa9800000000000L    # -90.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    cmpg-float v15, v15, v16

    if-gez v15, :cond_9

    .line 437
    const-wide v16, -0x3fa9800000000000L    # -90.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v15, v0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    goto/16 :goto_3
.end method

.method public release()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mScreenMesh:Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->releaseBuffer()V

    .line 364
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setGyroModeOff()V

    .line 365
    return-void
.end method

.method protected setFrustum()V
    .locals 8

    .prologue
    .line 223
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 224
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSurfaceRatio:F

    const/high16 v1, 0x41000000    # 8.0f

    div-float v3, v0, v1

    .line 225
    .local v3, "ratio":F
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mPMatrix:[F

    const/4 v1, 0x0

    neg-float v2, v3

    const/high16 v4, -0x42000000    # -0.125f

    const/high16 v5, 0x3e000000    # 0.125f

    const/high16 v6, 0x3e800000    # 0.25f

    const v7, 0x461c4000    # 10000.0f

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    .line 227
    .end local v3    # "ratio":F
    :cond_1
    return-void
.end method

.method public setGyroMode(Z)V
    .locals 0
    .param p1, "mode"    # Z

    .prologue
    .line 184
    if-eqz p1, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setGyroModeOn()V

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setGyroModeOff()V

    goto :goto_0
.end method

.method public setMeshShape(I)V
    .locals 3
    .param p1, "shapehint"    # I

    .prologue
    .line 346
    const-string v0, "Video360Renderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMeshShape() shapehint:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    .line 348
    return-void
.end method

.method public syncVMatrix()V
    .locals 20

    .prologue
    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLocker:Ljava/lang/Object;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 91
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mXAxis:[F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mZAxis:[F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mYAxis:[F

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->setMatrix([F[F[F[F)V

    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->m90DegAboutZ:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 96
    const/16 v2, 0x10

    new-array v13, v2, [F

    .line 97
    .local v13, "tm":[F
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    const/4 v4, 0x0

    invoke-static {v13, v2, v3, v4}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    .line 98
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtXAxis:[F

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtYAxis:[F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtZAxis:[F

    invoke-static {v13, v2, v3, v4}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->extractBasis([F[F[F[F)V

    .line 100
    new-instance v15, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtXAxis:[F

    invoke-direct {v15, v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>([F)V

    .line 101
    .local v15, "xxx":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    new-instance v17, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtYAxis:[F

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>([F)V

    .line 102
    .local v17, "yyy":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    new-instance v18, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mtZAxis:[F

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>([F)V

    .line 105
    .local v18, "zzz":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    new-instance v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->cross(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v12

    .line 107
    .local v12, "axis":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    invoke-static {v12, v15}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->AngleBetween(Lcom/sec/android/app/videoplayer/Video360/Vector3;Lcom/sec/android/app/videoplayer/Video360/Vector3;)F

    move-result v10

    .line 108
    .local v10, "anglexx":F
    move-object/from16 v0, v17

    invoke-static {v12, v0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->AngleBetween(Lcom/sec/android/app/videoplayer/Video360/Vector3;Lcom/sec/android/app/videoplayer/Video360/Vector3;)F

    move-result v11

    .line 110
    .local v11, "anglexy":F
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLZRotation:F

    .line 111
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mWordlXYRotation:F

    .line 113
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLXRotation:F

    .line 116
    .local v14, "xRotation":F
    new-instance v16, Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    new-instance v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mTouchSFGLYRotation:F

    float-to-double v4, v3

    move-object/from16 v0, v16

    invoke-direct {v0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;-><init>(Lcom/sec/android/app/videoplayer/Video360/Vector3;D)V

    .line 118
    .local v16, "yAsix":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v15

    .line 119
    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v17

    .line 120
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v18

    .line 122
    invoke-static {v13}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->orthonormalize([F)V

    .line 124
    new-instance v8, Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    const-wide v2, 0x4056800000000000L    # 90.0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v2, v2

    sub-float/2addr v2, v10

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, v14

    float-to-double v2, v2

    invoke-direct {v8, v15, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;-><init>(Lcom/sec/android/app/videoplayer/Video360/Vector3;D)V

    .line 125
    .local v8, "Mat":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v17

    .line 126
    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v18

    .line 128
    const-wide v2, 0x4056800000000000L    # 90.0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v2, v2

    cmpl-float v2, v11, v2

    if-lez v2, :cond_0

    .line 129
    neg-float v14, v14

    .line 131
    :cond_0
    new-instance v9, Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    float-to-double v2, v10

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v2, v14

    float-to-double v2, v2

    move-object/from16 v0, v17

    invoke-direct {v9, v0, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;-><init>(Lcom/sec/android/app/videoplayer/Video360/Vector3;D)V

    .line 132
    .local v9, "Mat2":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    invoke-virtual {v9, v15}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v15

    .line 133
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v18

    .line 135
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->asArray()[F

    move-result-object v2

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->asArray()[F

    move-result-object v3

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->asArray()[F

    move-result-object v4

    invoke-static {v13, v2, v3, v4}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->setMatrix([F[F[F[F)V

    .line 136
    invoke-static {v13}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->orthonormalize([F)V

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mVMatrix:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v13, v4}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    .line 142
    monitor-exit v19

    .line 143
    return-void

    .line 142
    .end local v8    # "Mat":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    .end local v9    # "Mat2":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    .end local v10    # "anglexx":F
    .end local v11    # "anglexy":F
    .end local v12    # "axis":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .end local v13    # "tm":[F
    .end local v14    # "xRotation":F
    .end local v15    # "xxx":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .end local v16    # "yAsix":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    .end local v17    # "yyy":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .end local v18    # "zzz":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    :catchall_0
    move-exception v2

    monitor-exit v19
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public toggleGyroMode()V
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mGyroMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setGyroMode(Z)V

    .line 181
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toggleMeshShape()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 338
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    if-eq v0, v1, :cond_0

    .line 339
    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    .line 343
    :goto_0
    return-void

    .line 341
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mShapeHint:I

    goto :goto_0
.end method

.method public toggleSensor()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 207
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    .line 208
    const-string v0, "Video360Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "toggleSensor() SensorEnabled:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mEnableSensor:Z

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->initialize(Landroid/content/Context;)V

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSensorListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->registerRotationListener(Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;)V

    .line 212
    invoke-static {v1, v1, v1}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->resumeSensor(III)V

    .line 217
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 207
    goto :goto_0

    .line 214
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->pauseSensor()V

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mSensorListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->unregisterRotationListener(Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;)V

    goto :goto_1
.end method
