.class Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;
.super Ljava/lang/Thread;
.source "TranscodeService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/videowall/TranscodeService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TranscodeThread"
.end annotation


# instance fields
.field private chapter:I

.field private filePath:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

.field private thumbTime:I

.field private videoId:J


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 437
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    .line 438
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 427
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->path:Ljava/lang/String;

    .line 429
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->name:Ljava/lang/String;

    .line 431
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->filePath:Ljava/lang/String;

    .line 439
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 452
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->name:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->videoId:J

    iget v6, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->chapter:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->thumbTime:I

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->filePath:Ljava/lang/String;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V
    invoke-static/range {v1 .. v8}, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;->access$1000(Lcom/sec/android/app/videoplayer/videowall/TranscodeService;Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V

    .line 453
    return-void
.end method

.method public setArgs(Ljava/lang/String;Ljava/lang/String;JIILjava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "videoId"    # J
    .param p5, "chapter"    # I
    .param p6, "thumbTime"    # I
    .param p7, "filePath"    # Ljava/lang/String;

    .prologue
    .line 442
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->path:Ljava/lang/String;

    .line 443
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->name:Ljava/lang/String;

    .line 444
    iput-wide p3, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->videoId:J

    .line 445
    iput p5, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->chapter:I

    .line 446
    iput p6, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->thumbTime:I

    .line 447
    iput-object p7, p0, Lcom/sec/android/app/videoplayer/videowall/TranscodeService$TranscodeThread;->filePath:Ljava/lang/String;

    .line 448
    return-void
.end method
