.class Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;
.super Ljava/lang/Object;
.source "VideoPlaySpeedPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isOutside:Z

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V
    .locals 1

    .prologue
    .line 271
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->isOutside:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 275
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 306
    :cond_0
    :goto_0
    return v5

    .line 277
    :pswitch_0
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "mMinusButton. ACTION_DOWN"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->isOutside:Z

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    const-wide/16 v2, 0x1f4

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->sendMessage(IJ)V
    invoke-static {v0, v4, v2, v3}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;IJ)V

    goto :goto_0

    .line 283
    :pswitch_1
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "mMinusButton. ACTION_UP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    const/4 v1, -0x1

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->changePlaySpeed(I)Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)Z

    goto :goto_0

    .line 289
    :pswitch_2
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->isOutside:Z

    if-nez v0, :cond_0

    .line 290
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpl-float v0, v2, v0

    if-gtz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V

    .line 293
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->isOutside:Z

    goto :goto_0

    .line 299
    :pswitch_3
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "mMinusButton. ACTION_CANCEL"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V

    goto :goto_0

    .line 275
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
