.class Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;
.super Ljava/lang/Object;
.source "VideoPlaySpeedPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 147
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "setPositiveButton"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)I

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setPlaySpeed(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed_temp:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)I

    move-result v0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$202(I)I

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mOnUpdatedListener:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;->onUpdated()V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$402(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 160
    :cond_1
    return-void
.end method
