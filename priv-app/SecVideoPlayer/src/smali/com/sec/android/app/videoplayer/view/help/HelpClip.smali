.class public Lcom/sec/android/app/videoplayer/view/help/HelpClip;
.super Ljava/lang/Object;
.source "HelpClip.java"


# static fields
.field private static final CHECK_CURRENT_POSITION:I = 0x64

.field private static final MOVE_BUTTON_DOWN:I = 0xc8

.field private static final NORMAL_POSITION:I = -0x1

.field private static final TAG:Ljava/lang/String; = "HelpClip"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

.field private mDescription:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mHelpClipBtnLayout:Landroid/widget/RelativeLayout;

.field private mHelpClipTextLayout:Landroid/widget/RelativeLayout;

.field private mHelpClipView:Landroid/widget/RelativeLayout;

.field private mJumpButton:[Landroid/widget/ImageView;

.field private mJumpID:[I

.field private mNormalBtnRes:[I

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPressBtnRes:[I

.field private mPrevPos:I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mStopHandler:Z

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mParentView:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    .line 35
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipBtnLayout:Landroid/widget/RelativeLayout;

    .line 36
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipTextLayout:Landroid/widget/RelativeLayout;

    .line 38
    new-array v0, v3, [Landroid/widget/ImageView;

    aput-object v2, v0, v4

    const/4 v1, 0x1

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    .line 39
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpID:[I

    .line 41
    new-array v0, v3, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPressBtnRes:[I

    .line 42
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mNormalBtnRes:[I

    .line 44
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mTitle:Landroid/widget/TextView;

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mDescription:Landroid/widget/TextView;

    .line 47
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 51
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mStopHandler:Z

    .line 52
    iput v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPrevPos:I

    .line 237
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip$2;-><init>(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    .line 56
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getInstance()Lcom/sec/android/app/videoplayer/view/help/HelpData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    .line 57
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 58
    return-void

    .line 39
    nop

    :array_0
    .array-data 4
        0x7f0d00ec
        0x7f0d00ed
        0x7f0d00ee
    .end array-data

    .line 41
    :array_1
    .array-data 4
        0x7f020065
        0x7f020068
        0x7f02006b
    .end array-data

    .line 42
    :array_2
    .array-data 4
        0x7f020060
        0x7f020061
        0x7f020062
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/view/help/HelpData;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpID:[I

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->getBtnPosition()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPrevPos:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runHandler()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/help/HelpClip;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/HelpClip;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->changeButtonLayout(Z)V

    return-void
.end method

.method private changeButtonLayout(Z)V
    .locals 5
    .param p1, "moveDown"    # Z

    .prologue
    const v4, 0x7f0d00eb

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    if-nez v2, :cond_2

    .line 200
    :cond_0
    const-string v2, "HelpClip"

    const-string v3, "moveButtonDown - not ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :cond_1
    :goto_0
    return-void

    .line 203
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->refreshTextLayout()V

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipBtnLayout:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_3

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipBtnLayout:Landroid/widget/RelativeLayout;

    .line 208
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipBtnLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 212
    .local v0, "LP":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 214
    .local v1, "margin_Bottom":I
    if-eqz p1, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 215
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0801ad

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 217
    :cond_4
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 218
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getBtnPosition()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->getBtnPosition(I)I

    move-result v0

    return v0
.end method

.method private getBtnPosition(I)I
    .locals 8
    .param p1, "seekPos"    # I

    .prologue
    .line 272
    move v3, p1

    .line 274
    .local v3, "postion":I
    const/4 v4, -0x1

    if-ne p1, v4, :cond_0

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v3

    .line 277
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v4

    array-length v1, v4

    .line 278
    .local v1, "length":I
    add-int/lit8 v2, v1, -0x1

    .line 279
    .local v2, "pos":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 280
    const-string v4, "HelpClip"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBtnPosition "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " / "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v6

    aget-wide v6, v6, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    int-to-long v4, v3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v6

    aget-wide v6, v6, v0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_2

    .line 282
    add-int/lit8 v2, v0, -0x1

    .line 285
    .end local v2    # "pos":I
    :cond_1
    return v2

    .line 279
    .restart local v2    # "pos":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private refreshTextLayout()V
    .locals 4

    .prologue
    const v3, 0x7f0d00e8

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipTextLayout:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_2

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipTextLayout:Landroid/widget/RelativeLayout;

    .line 228
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipTextLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 230
    .local v0, "LP":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801b1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801b0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0801af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private runHandler()V
    .locals 4

    .prologue
    const/16 v1, 0x64

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 308
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mStopHandler:Z

    if-eqz v0, :cond_0

    .line 310
    :goto_0
    return-void

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private setDscription(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mDescription:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->createDescriptionAndTitle()V

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mDescription:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    return-void
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mTitle:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->createDescriptionAndTitle()V

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 61
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 67
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 62
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mParentView:Landroid/widget/RelativeLayout;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 64
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030014

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    .line 65
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 175
    :cond_0
    return-void
.end method

.method public createDescriptionAndTitle()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d00e9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mTitle:Landroid/widget/TextView;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d00ea

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mDescription:Landroid/widget/TextView;

    .line 85
    return-void
.end method

.method public createHelpButton()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 126
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    if-nez v4, :cond_1

    .line 127
    const-string v4, "HelpClip"

    const-string v5, "createHelpButton E : mData is null"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    return-void

    .line 131
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpID:[I

    array-length v2, v4

    .line 132
    .local v2, "length":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v4

    array-length v0, v4

    .line 133
    .local v0, "btnCnt":I
    const-string v4, "HelpClip"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createHelpButton E : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 135
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpID:[I

    aget v6, v6, v1

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    aput-object v4, v5, v1

    .line 136
    if-ge v1, v0, :cond_2

    .line 137
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    new-instance v5, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;-><init>(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 165
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    const v5, 0x7f0a007f

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    add-int/lit8 v7, v1, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 166
    .local v3, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 134
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 168
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v4, v4, v1

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method public moveButtonDown()V
    .locals 2

    .prologue
    .line 191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 196
    :goto_0
    return-void

    .line 193
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    .line 195
    .local v0, "show":Z
    if-nez v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->changeButtonLayout(Z)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public moveButtonDown(Z)V
    .locals 4
    .param p1, "moveDown"    # Z

    .prologue
    const/16 v1, 0xc8

    .line 178
    if-eqz p1, :cond_0

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 186
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->changeButtonLayout(Z)V

    goto :goto_0
.end method

.method public reStart()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    .line 79
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->moveButtonDown()V

    .line 80
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 313
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    .line 324
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHelpClipView:Landroid/widget/RelativeLayout;

    .line 325
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mParentView:Landroid/widget/RelativeLayout;

    .line 326
    return-void
.end method

.method public runOrStopHandler()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    .line 294
    :goto_0
    return-void

    .line 292
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    goto :goto_0
.end method

.method public runOrStopHandler(Z)V
    .locals 2
    .param p1, "run"    # Z

    .prologue
    .line 297
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mStopHandler:Z

    .line 299
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mStopHandler:Z

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 303
    :goto_1
    return-void

    .line 297
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 302
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runHandler()V

    goto :goto_1
.end method

.method public setDescriptionAndTitle4SeekMode(I)V
    .locals 1
    .param p1, "seekPos"    # I

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->getBtnPosition(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->updateContents(I)V

    .line 109
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->createHelpButton()V

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->createDescriptionAndTitle()V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getStartClip()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->updateContents(I)V

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->moveButtonDown()V

    .line 75
    return-void
.end method

.method public updateContents(I)V
    .locals 5
    .param p1, "pos"    # I

    .prologue
    .line 88
    const-string v2, "HelpClip"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDescriptionAndTitle E :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    if-nez v2, :cond_1

    .line 90
    const-string v2, "HelpClip"

    const-string v3, "setDescriptionAndTitle mData is NULL"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    return-void

    .line 94
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v2

    array-length v0, v2

    .line 95
    .local v0, "btnCnt":I
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPrevPos:I

    .line 96
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getDescription()[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->setDscription(Ljava/lang/String;)V

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTitle()[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->setTitle(Ljava/lang/String;)V

    .line 98
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 99
    if-ne v1, p1, :cond_2

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mPressBtnRes:[I

    aget v4, v4, v1

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpButton:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mNormalBtnRes:[I

    aget v3, v3, v1

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
