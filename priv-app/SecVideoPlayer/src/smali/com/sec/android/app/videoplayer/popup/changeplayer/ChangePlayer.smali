.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;
.super Ljava/lang/Object;
.source "ChangePlayer.java"


# instance fields
.field private mDeviceIcon:Landroid/graphics/drawable/Drawable;

.field private mDeviceIconUri:Landroid/net/Uri;

.field private mDeviceName:Ljava/lang/String;

.field private mDeviceType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceName:Ljava/lang/String;

    .line 18
    iput p2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceType:I

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p3, "type"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceName:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceIcon:Landroid/graphics/drawable/Drawable;

    .line 30
    iput p3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceType:I

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/net/Uri;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "iconUri"    # Landroid/net/Uri;
    .param p3, "type"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceName:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceIconUri:Landroid/net/Uri;

    .line 24
    iput p3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceType:I

    .line 25
    return-void
.end method


# virtual methods
.method public getDeviceIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDeviceIconUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceIconUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceType:I

    return v0
.end method

.method public setDeviceIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "icon"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 34
    if-eqz p1, :cond_0

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayer;->mDeviceIcon:Landroid/graphics/drawable/Drawable;

    .line 37
    :cond_0
    return-void
.end method
