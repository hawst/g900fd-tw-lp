.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;
.super Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
.source "ChangePlayerListenerNew.java"


# static fields
.field private static sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private bRegistered:Z

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private final mIntentFilter:Landroid/content/IntentFilter;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mSettingsObserver:Landroid/database/ContentObserver;

.field private mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;-><init>()V

    .line 23
    const-class v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->TAG:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mHandler:Landroid/os/Handler;

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->bRegistered:Z

    .line 35
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mIntentFilter:Landroid/content/IntentFilter;

    .line 108
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$1;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 125
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew$2;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    .line 52
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    return-object v0
.end method


# virtual methods
.method public registerReceiver()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "wifi_display_on"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->bRegistered:Z

    .line 88
    return-void
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    .line 63
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 64
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    return-object v0
.end method

.method public setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mHandler:Landroid/os/Handler;

    .line 75
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->sChangePlayerListenerNew:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    return-object v0
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->bRegistered:Z

    if-eqz v0, :cond_1

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->bRegistered:Z

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mSettingsObserver:Landroid/database/ContentObserver;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 106
    :cond_1
    return-void
.end method
