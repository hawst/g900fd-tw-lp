.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private detailSeekStartedInCocktailBar:Z

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 1

    .prologue
    .line 2759
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2761
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->detailSeekStartedInCocktailBar:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/4 v8, 0x4

    .line 2764
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    .line 2765
    .local v2, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    .line 2767
    .local v3, "y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 2769
    .local v0, "action":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionX:F
    invoke-static {v4, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F

    .line 2770
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionY:F
    invoke-static {v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4202(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F

    .line 2773
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2849
    :cond_0
    :goto_0
    return v9

    .line 2776
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initDetailSeekViewStub()V
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    .line 2777
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->changeLayoutForDetailSeekView(F)V
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)V

    .line 2780
    packed-switch v0, :pswitch_data_0

    .line 2847
    :goto_1
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I

    move-result v5

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4002(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I

    goto :goto_0

    .line 2786
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isCocktailBarVisible()Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->detailSeekStartedInCocktailBar:Z

    if-eqz v4, :cond_3

    .line 2787
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->detailSeekStartedInCocktailBar:Z

    goto :goto_0

    .line 2792
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2793
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v6

    .line 2798
    .local v1, "detailedSeeklyWidth":F
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/SeekBar;->getRight()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getLeft()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    sub-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getProgress()I

    move-result v6

    mul-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/SeekBar;->getMax()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0800ff

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F

    .line 2803
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 2804
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2805
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)F

    move-result v5

    sub-float/2addr v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setX(F)V

    .line 2810
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2811
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 2795
    .end local v1    # "detailedSeeklyWidth":F
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v6

    .restart local v1    # "detailedSeeklyWidth":F
    goto/16 :goto_2

    .line 2807
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)F

    move-result v5

    sub-float/2addr v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setY(F)V

    goto :goto_3

    .line 2812
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v4

    if-ne v4, v8, :cond_8

    .line 2813
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2814
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)F

    move-result v5

    sub-float/2addr v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setX(F)V

    .line 2819
    :goto_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2820
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 2816
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)F

    move-result v5

    sub-float/2addr v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setY(F)V

    goto :goto_4

    .line 2822
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2823
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    .line 2830
    .end local v1    # "detailedSeeklyWidth":F
    :pswitch_2
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->detailSeekStartedInCocktailBar:Z

    if-eqz v4, :cond_9

    .line 2831
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->detailSeekStartedInCocktailBar:Z

    goto/16 :goto_0

    .line 2834
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2835
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 2836
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 2837
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubPortrait:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 2839
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubLandscape:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 2780
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
