.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileSearch"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;
    }
.end annotation


# instance fields
.field private final mThreadPool:Ljava/util/concurrent/ExecutorService;

.field private mVectorList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 4

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    .line 429
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 430
    .local v0, "threads":I
    const-string v1, "FileSearch"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Threads :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    .line 432
    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 424
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchFiles(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    .prologue
    .line 424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    return-object v0
.end method

.method private afterSearch()V
    .locals 14

    .prologue
    .line 586
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v10

    instance-of v10, v10, Landroid/app/Activity;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v10

    check-cast v10, Landroid/app/Activity;

    invoke-virtual {v10}, Landroid/app/Activity;->isResumed()Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    if-eqz v10, :cond_3

    .line 587
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    new-instance v11, Ljava/util/ArrayList;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    invoke-direct {v11, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    if-eqz v10, :cond_0

    .line 593
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->clear()V

    .line 594
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    .line 598
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v9

    .line 600
    .local v9, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 601
    const-string v10, "FileSearch"

    const-string v11, "afterSearch()-INBAND subtitle"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    :try_start_1
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    new-instance v11, Ljava/io/File;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v13, 0x7f0a0035

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v12, v13}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    .line 610
    :cond_1
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_7

    .line 611
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 612
    .local v3, "filePath":Ljava/lang/String;
    const/4 v8, 0x0

    .line 613
    .local v8, "lastIndex":I
    if-eqz v3, :cond_6

    .line 614
    const/16 v10, 0x2f

    invoke-virtual {v3, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    .line 615
    const/4 v10, 0x0

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v3, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 617
    .local v0, "displayName":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v4, v10, :cond_7

    .line 618
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 619
    .local v6, "itemMove":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 620
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 621
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 617
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 592
    .end local v0    # "displayName":Ljava/lang/String;
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v4    # "i":I
    .end local v6    # "itemMove":Ljava/io/File;
    .end local v8    # "lastIndex":I
    .end local v9    # "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    if-eqz v10, :cond_4

    .line 593
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->clear()V

    .line 594
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    .line 652
    :cond_4
    :goto_2
    return-void

    .line 592
    :catchall_0
    move-exception v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    if-eqz v11, :cond_5

    .line 593
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    invoke-virtual {v11}, Ljava/util/Vector;->clear()V

    .line 594
    const/4 v11, 0x0

    iput-object v11, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;

    :cond_5
    throw v10

    .line 604
    .restart local v9    # "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :catch_0
    move-exception v1

    .line 605
    .local v1, "error":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 606
    const-string v10, "FileSearch"

    const-string v11, "afterSearch :: OutOfMemoryError ex"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 625
    .end local v1    # "error":Ljava/lang/OutOfMemoryError;
    .restart local v3    # "filePath":Ljava/lang/String;
    .restart local v8    # "lastIndex":I
    :cond_6
    const-string v10, "FileSearch"

    const-string v11, "afterSearch() - filePath is NULL"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    .end local v3    # "filePath":Ljava/lang/String;
    .end local v8    # "lastIndex":I
    :cond_7
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    new-instance v11, Ljava/io/File;

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v13, 0x7f0a001d

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v12, v13}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    const-string v10, "FileSearch"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "afterSearch()-searched counts are :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v11, 0x1

    if-le v10, v11, :cond_4

    .line 633
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    .line 635
    .local v2, "fileName":Ljava/lang/String;
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_9

    .line 636
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v10

    if-nez v10, :cond_9

    .line 637
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v11, 0x7f0a0035

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v2

    .line 641
    :cond_9
    if-eqz v2, :cond_4

    .line 643
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 645
    .local v5, "index":I
    const/4 v10, -0x1

    if-eq v5, v10, :cond_4

    .line 646
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/io/File;

    .line 648
    .local v7, "itemToMove":Ljava/io/File;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 649
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto/16 :goto_2
.end method

.method private newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;
    .locals 8
    .param p1, "nThreads"    # I

    .prologue
    .line 435
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    move v2, p1

    move v3, p1

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    .line 436
    .local v1, "threadPool":Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;
    invoke-virtual {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool;->setAllTaskDoneListener(Lcom/sec/android/app/videoplayer/subtitle/threadpool/AutoShutdownThreadPool$OnAllTaskDoneListener;)V

    .line 437
    return-object v1
.end method

.method private searchFiles(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 474
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 475
    return-void
.end method

.method private searchFilesOnSingle(Ljava/io/File;)V
    .locals 2
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 479
    return-void
.end method


# virtual methods
.method public done()V
    .locals 2

    .prologue
    .line 570
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->afterSearch()V

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 573
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->post(Ljava/lang/Runnable;)Z

    .line 580
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->finish()V

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 582
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mThreadPool:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 471
    :cond_0
    return-void
.end method

.method public searchSubtitles()V
    .locals 10

    .prologue
    .line 441
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "storage"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/storage/StorageManager;

    .line 442
    .local v2, "storageManager":Landroid/os/storage/StorageManager;
    invoke-virtual {v2}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    .line 444
    .local v5, "storageVolumes":[Landroid/os/storage/StorageVolume;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v7, v5

    if-ge v1, v7, :cond_4

    .line 445
    aget-object v4, v5, v1

    .line 446
    .local v4, "storageVolume":Landroid/os/storage/StorageVolume;
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v6

    .line 447
    .local v6, "subsystem":Ljava/lang/String;
    const-string v3, ""

    .line 449
    .local v3, "storagePath":Ljava/lang/String;
    if-eqz v6, :cond_3

    const-string v7, "fuse"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "sd"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "usb"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 450
    :cond_0
    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 452
    invoke-virtual {v2, v3}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mounted"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 453
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 454
    .local v0, "file":Ljava/io/File;
    const-string v7, "FileSearch"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "root : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ext"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 456
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchFilesOnSingle(Ljava/io/File;)V

    .line 444
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 458
    .restart local v0    # "file":Ljava/io/File;
    :cond_2
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchFiles(Ljava/io/File;)V

    goto :goto_1

    .line 462
    .end local v0    # "file":Ljava/io/File;
    :cond_3
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchFiles(Ljava/io/File;)V

    goto :goto_1

    .line 465
    .end local v3    # "storagePath":Ljava/lang/String;
    .end local v4    # "storageVolume":Landroid/os/storage/StorageVolume;
    .end local v6    # "subsystem":Ljava/lang/String;
    :cond_4
    return-void
.end method
