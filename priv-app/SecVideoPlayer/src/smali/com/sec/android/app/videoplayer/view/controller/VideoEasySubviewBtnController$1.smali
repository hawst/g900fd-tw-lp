.class Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;
.super Ljava/lang/Object;
.source "VideoEasySubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x0

    const/16 v9, 0xbb8

    const/4 v8, 0x0

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_0

    .line 161
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 197
    :cond_0
    :goto_0
    :pswitch_0
    return v8

    .line 163
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 167
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v3, v5, v3

    if-gtz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 168
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 169
    .local v0, "pressTime":J
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 170
    invoke-virtual {p1, v8}, Landroid/view/View;->playSoundEffect(I)V

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 172
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isVolumeCtrlShowing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 174
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getTitleController()Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-result-object v2

    .line 178
    .local v2, "titleController":Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    if-eqz v2, :cond_0

    .line 179
    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupVolbar(Z)V

    .line 180
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 181
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_0

    .line 190
    .end local v0    # "pressTime":J
    .end local v2    # "titleController":Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
