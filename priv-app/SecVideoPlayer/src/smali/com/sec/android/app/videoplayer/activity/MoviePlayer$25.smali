.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;
.super Landroid/os/Handler;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 3901
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v6, 0xd5

    const/16 v5, 0xcf

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3903
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 4132
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 3905
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3906
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3907
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleSyncTime()V

    goto :goto_0

    .line 3909
    :cond_1
    const/16 v1, 0xc9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 3914
    :sswitch_2
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler. case EXITMOVIEPLAYER E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3915
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 3919
    :sswitch_3
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler. case SHOW_CONTROLLER E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3920
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xcb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3921
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3922
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto :goto_0

    .line 3927
    :sswitch_4
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler. case HIDE_CONTROLLER E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3928
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3929
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3930
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    goto/16 :goto_0

    .line 3941
    :sswitch_5
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler. case VOLUME_UPDATE E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3943
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xcd

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3945
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    .line 3946
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateVolume()V

    goto/16 :goto_0

    .line 3951
    :sswitch_6
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler. case MOVIEPLAYER_EXIT_ON_ERRORPOPUP E"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3953
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 3954
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 3957
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 3961
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 3962
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLayoutForMultiWindow()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 3966
    :sswitch_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3968
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 3969
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachProgressbarPreview()V

    .line 3970
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 3975
    :sswitch_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3976
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkKnoxMode(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 3980
    :sswitch_a
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3981
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler - set DNIe : UI mode."

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3982
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUIMode(I)V

    .line 3983
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setTconUIMode(I)V

    .line 3990
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3991
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 3985
    :cond_3
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mHandler - set DNIe : Video mode."

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3986
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUIMode(I)V

    .line 3987
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setTconUIMode(I)V

    goto :goto_1

    .line 3996
    :sswitch_b
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$2602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    goto/16 :goto_0

    .line 4001
    :sswitch_c
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2e9

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4002
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 4004
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 4008
    :sswitch_d
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2ea

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4009
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 4011
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    if-eqz v1, :cond_4

    .line 4012
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSubviewBtnControllerForTvOutView(Z)V

    .line 4013
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onResume()V

    .line 4014
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 4017
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 4018
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 4019
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showAllshareControls(Z)V

    goto/16 :goto_0

    .line 4024
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->invalidateOptionsMenu()V

    .line 4026
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setStaticView()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4028
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 4029
    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_7

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 4031
    :cond_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 4032
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 4033
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLayoutForMultiWindow()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4036
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 4037
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showAllshareControls(Z)V

    goto/16 :goto_0

    .line 4042
    :sswitch_e
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2eb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4044
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_9

    .line 4045
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->dismissChangePlayerPopup()V

    .line 4048
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_a

    .line 4049
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 4050
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iput-boolean v4, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 4052
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 4053
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 4058
    :sswitch_f
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2e8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4059
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    iput-boolean v3, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 4061
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    if-eqz v1, :cond_b

    .line 4062
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSubviewBtnControllerForTvOutView(Z)V

    .line 4063
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onResume()V

    .line 4066
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 4067
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 4068
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 4069
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLayoutForMultiWindow()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4071
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 4072
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTVOutView()V

    .line 4073
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showAllshareControls(Z)V

    goto/16 :goto_0

    .line 4078
    :sswitch_10
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2e7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4079
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    .line 4080
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->onDmrListChanged()V

    goto/16 :goto_0

    .line 4085
    :sswitch_11
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x2e5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4087
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v1

    if-nez v1, :cond_d

    .line 4088
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 4089
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    goto/16 :goto_0

    .line 4092
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v1, :cond_e

    .line 4093
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updatePausePlayBtn()V

    goto/16 :goto_0

    .line 4094
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_0

    .line 4095
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePausePlayBtn()V

    goto/16 :goto_0

    .line 4102
    :sswitch_12
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    .line 4103
    .local v0, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 4104
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 4105
    :cond_f
    iget v1, p1, Landroid/os/Message;->arg1:I

    if-ltz v1, :cond_11

    .line 4106
    iget v1, p1, Landroid/os/Message;->arg1:I

    mul-int/lit16 v1, v1, 0x3e8

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 4107
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPauseSet()V

    .line 4108
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 4116
    :cond_10
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x384

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 4110
    :cond_11
    sget-object v1, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_2

    .line 4113
    :cond_12
    if-eqz v0, :cond_10

    .line 4114
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPauseSet()V

    goto :goto_2

    .line 4120
    .end local v0    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    :sswitch_13
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x385

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4121
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)V

    goto/16 :goto_0

    .line 4125
    :sswitch_14
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xd7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4126
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->checkAvailable(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 3903
    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_1
        0xca -> :sswitch_2
        0xcb -> :sswitch_3
        0xcc -> :sswitch_0
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0xd0 -> :sswitch_8
        0xd4 -> :sswitch_9
        0xd5 -> :sswitch_a
        0xd6 -> :sswitch_b
        0xd7 -> :sswitch_14
        0xd8 -> :sswitch_4
        0x2e5 -> :sswitch_11
        0x2e7 -> :sswitch_10
        0x2e8 -> :sswitch_f
        0x2e9 -> :sswitch_c
        0x2ea -> :sswitch_d
        0x2eb -> :sswitch_e
        0x384 -> :sswitch_12
        0x385 -> :sswitch_13
    .end sparse-switch
.end method
