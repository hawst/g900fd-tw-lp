.class Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 2570
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/view/MainVideoView$1;

    .prologue
    .line 2570
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v2, 0x1

    .line 2573
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2589
    :cond_0
    :goto_0
    return v2

    .line 2576
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1100(Lcom/sec/android/app/videoplayer/view/MainVideoView;)F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v4

    mul-float v0, v3, v4

    .line 2577
    .local v0, "ScaleFactor":F
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1100(Lcom/sec/android/app/videoplayer/view/MainVideoView;)F

    move-result v3

    sub-float v3, v0, v3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int v1, v3

    .line 2579
    .local v1, "scaleValue":I
    const-string v3, "MainVideoView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onScale : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1100(Lcom/sec/android/app/videoplayer/view/MainVideoView;)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2581
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F
    invoke-static {v3, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1102(Lcom/sec/android/app/videoplayer/view/MainVideoView;F)F

    .line 2583
    const/16 v3, -0x1f4

    if-le v1, v3, :cond_2

    .line 2584
    const/4 v2, 0x0

    goto :goto_0

    .line 2586
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z
    invoke-static {v3, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$702(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z

    goto :goto_0
.end method
