.class public final Lcom/sec/android/app/videoplayer/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final action_bar_back_btn_height:I = 0x7f080000

.field public static final action_bar_back_btn_layout_height:I = 0x7f080001

.field public static final action_bar_back_btn_layout_width:I = 0x7f080002

.field public static final action_bar_back_btn_margin_left:I = 0x7f080003

.field public static final action_bar_back_btn_width:I = 0x7f080004

.field public static final action_bar_switch_divider_height:I = 0x7f080005

.field public static final action_bar_switch_divider_margin_left:I = 0x7f080006

.field public static final action_bar_switch_divider_width:I = 0x7f080007

.field public static final action_bar_title_margin_left:I = 0x7f080008

.field public static final action_bar_title_padding_right:I = 0x7f080009

.field public static final advertisement_image_layout_height:I = 0x7f08000a

.field public static final advertisement_image_layout_width:I = 0x7f08000b

.field public static final advertisement_text_layout_bottomMargin:I = 0x7f08000c

.field public static final advertisement_text_layout_height:I = 0x7f08000d

.field public static final advertisement_text_layout_rightMargin:I = 0x7f08000e

.field public static final advertisement_text_layout_topMargin:I = 0x7f08000f

.field public static final advertisement_text_layout_width:I = 0x7f080010

.field public static final allshare_cast_check_layout_paddingBottom:I = 0x7f080011

.field public static final allshare_cast_check_layout_paddingLeft:I = 0x7f080012

.field public static final allshare_cast_check_marginRight:I = 0x7f080013

.field public static final allshare_cast_explain_paddingBottom:I = 0x7f080014

.field public static final allshare_cast_explain_paddingLeft:I = 0x7f080015

.field public static final allshare_cast_explain_paddingRight:I = 0x7f080016

.field public static final allshare_cast_explain_paddingTop:I = 0x7f080017

.field public static final allshare_device_list_margin_bottom:I = 0x7f080018

.field public static final allshare_list_height:I = 0x7f080019

.field public static final allshare_list_icon_margin_left:I = 0x7f08001a

.field public static final allshare_list_left_margin:I = 0x7f08001b

.field public static final allshare_list_player_icon_height:I = 0x7f08001c

.field public static final allshare_list_playername_margin_left:I = 0x7f08001d

.field public static final allshare_list_right_margin:I = 0x7f08001e

.field public static final allshare_list_separator_padding:I = 0x7f08001f

.field public static final allshare_player_desc_text__bottom_margin:I = 0x7f080020

.field public static final allshare_player_desc_text__size:I = 0x7f080021

.field public static final allshare_player_name_text__size:I = 0x7f080022

.field public static final allshare_player_name_text__top_margin:I = 0x7f080023

.field public static final allshare_refresh_progressbar_height:I = 0x7f080024

.field public static final allshare_refresh_progressbar_width:I = 0x7f080025

.field public static final allshare_title_bottom_margin:I = 0x7f080026

.field public static final allshare_title_layout_height:I = 0x7f080027

.field public static final allshare_title_layout_padding_top:I = 0x7f080028

.field public static final allshare_title_layout_side_margin:I = 0x7f080029

.field public static final allshare_title_left_margin:I = 0x7f08002a

.field public static final allshare_title_right_margin:I = 0x7f08002b

.field public static final allshare_title_subtext_size:I = 0x7f08002c

.field public static final allshare_title_top_margin:I = 0x7f08002d

.field public static final app_defaultsize_h:I = 0x7f08002e

.field public static final app_defaultsize_w:I = 0x7f08002f

.field public static final appinapp_init_width:I = 0x7f080030

.field public static final appinapp_init_width_video_portrait:I = 0x7f080031

.field public static final appinapp_loading_progressBar_top_margin:I = 0x7f080032

.field public static final appinapp_loading_text_size:I = 0x7f080033

.field public static final appinapp_loading_text_top_margin:I = 0x7f080034

.field public static final appinapp_max_margin:I = 0x7f080035

.field public static final appinapp_max_margin_potrait:I = 0x7f080036

.field public static final appinapp_max_width:I = 0x7f080037

.field public static final appinapp_max_width_potrait:I = 0x7f080038

.field public static final appinapp_min_width:I = 0x7f080039

.field public static final appinapp_min_width_video_portrait:I = 0x7f08003a

.field public static final appinapp_scale_factor:I = 0x7f08003b

.field public static final asf_title_text_right_margin:I = 0x7f08003c

.field public static final capture_bottom_margin:I = 0x7f08003d

.field public static final capture_bottom_margin_for_multiwindow:I = 0x7f08003e

.field public static final capture_image_width:I = 0x7f08003f

.field public static final capture_right_margin:I = 0x7f080040

.field public static final chapter_view_layout_padding:I = 0x7f080041

.field public static final common_FF_REW_margin4MiniController:I = 0x7f080042

.field public static final common_ProgressBar_margin:I = 0x7f080043

.field public static final common_ProgressBar_margin4MiniController:I = 0x7f080044

.field public static final common_ProgressBar_margin4MultiwindowLandscape:I = 0x7f080045

.field public static final common_ProgressPreviewPopupY_Position:I = 0x7f080046

.field public static final common_Side_margin4MiniController:I = 0x7f080047

.field public static final common_Side_margin4MultiwindowLandscape:I = 0x7f080048

.field public static final common_miniController_width:I = 0x7f080049

.field public static final common_top_margin4MultiwindowLandscape:I = 0x7f08004a

.field public static final contextual_actionbar_title_size:I = 0x7f08004b

.field public static final contextual_basic_margin:I = 0x7f08004c

.field public static final contextual_list_margintop:I = 0x7f08004d

.field public static final contextual_list_one_row_height:I = 0x7f08004e

.field public static final contextual_list_subtitle:I = 0x7f08004f

.field public static final contextual_list_title:I = 0x7f080050

.field public static final contextual_list_two_row_height:I = 0x7f080051

.field public static final contextual_tag_alignment_right_margin:I = 0x7f080052

.field public static final contextual_title_popup_paddingLeft:I = 0x7f080053

.field public static final contextual_title_popup_paddingRight:I = 0x7f080054

.field public static final contextual_weather_image_width:I = 0x7f0802f9

.field public static final default_surface_height_for_subtitle:I = 0x7f080055

.field public static final detailed_seek_arrow_height:I = 0x7f080056

.field public static final detailed_seek_arrow_layout_height:I = 0x7f080057

.field public static final detailed_seek_arrow_margin_Bottom:I = 0x7f080058

.field public static final detailed_seek_arrow_margin_Top:I = 0x7f080059

.field public static final detailed_seek_arrow_width:I = 0x7f08005a

.field public static final detailed_seek_layout_bottom_margin:I = 0x7f08005b

.field public static final detailed_seek_layout_bottom_margin_port:I = 0x7f08005c

.field public static final detailed_seek_layout_height:I = 0x7f08005d

.field public static final detailed_seek_layout_height_port:I = 0x7f08005e

.field public static final detailed_seek_layout_width:I = 0x7f08005f

.field public static final detailed_seek_layout_width_port:I = 0x7f080060

.field public static final detailed_seek_text_bottom_margin:I = 0x7f080061

.field public static final detailed_seek_text_size:I = 0x7f080062

.field public static final details_listview_padding:I = 0x7f080063

.field public static final details_row_title_textsize:I = 0x7f080064

.field public static final details_seek_text_height:I = 0x7f080065

.field public static final details_seek_text_width_port:I = 0x7f080066

.field public static final detailseek_slide_textsize:I = 0x7f080067

.field public static final detailseek_textsize:I = 0x7f080068

.field public static final edit_text_contextual_height:I = 0x7f080069

.field public static final edit_text_contextual_marginLeft:I = 0x7f08006a

.field public static final edit_text_contextual_marginTop:I = 0x7f08006b

.field public static final edit_text_contextual_paddingLeft:I = 0x7f08006c

.field public static final edit_text_contextual_paddingRight:I = 0x7f08006d

.field public static final half_detailed_seek_arrow_end_bottom_margin:I = 0x7f08006e

.field public static final hover_popup_left_right_padding:I = 0x7f08006f

.field public static final hover_popup_white_bottom_margin:I = 0x7f080070

.field public static final hover_popup_white_top_margin:I = 0x7f080071

.field public static final hover_preview_text_size:I = 0x7f080072

.field public static final list_main_height:I = 0x7f080073

.field public static final list_main_width:I = 0x7f0802fa

.field public static final lockscreen_volumebtn_rightmargin:I = 0x7f080074

.field public static final mini_video_exit_layout_right_margin:I = 0x7f080075

.field public static final mini_video_exit_layout_top_margin:I = 0x7f080076

.field public static final mini_video_layout_bottom_margin:I = 0x7f080077

.field public static final mini_video_layout_left_margin:I = 0x7f080078

.field public static final mini_video_layout_right_margin:I = 0x7f080079

.field public static final notificatio_btncontroller_close_margineleft:I = 0x7f08007a

.field public static final notificatio_btncontroller_ff_margineleft:I = 0x7f08007b

.field public static final notificatio_btncontroller_layout_margineright:I = 0x7f08007c

.field public static final notificatio_btncontroller_layout_marginetop:I = 0x7f08007d

.field public static final notificatio_btncontroller_play_pause_margineleft:I = 0x7f08007e

.field public static final notificatio_btncontroller_rew_margineleft:I = 0x7f08007f

.field public static final notificatio_button_height:I = 0x7f080080

.field public static final notificatio_button_width:I = 0x7f080081

.field public static final notificatio_layout_height:I = 0x7f080082

.field public static final notificatio_thumbnail_layout_marginebottom:I = 0x7f080083

.field public static final notificatio_thumbnail_layout_margineleft:I = 0x7f080084

.field public static final notificatio_thumbnail_layout_margineright:I = 0x7f080085

.field public static final notificatio_thumbnail_layout_marginetop:I = 0x7f080086

.field public static final notificatio_thumbnail_layout_width:I = 0x7f080087

.field public static final notificatio_thumbnail_width_heigth:I = 0x7f080088

.field public static final notification_height:I = 0x7f080089

.field public static final notification_thumbnail_layout_left_margin:I = 0x7f08008a

.field public static final notification_thumbnail_size:I = 0x7f08008b

.field public static final preview_each_margin:I = 0x7f08008c

.field public static final preview_framelayout_item_height:I = 0x7f08008d

.field public static final preview_framelayout_item_marginleft:I = 0x7f08008e

.field public static final preview_framelayout_item_margintop:I = 0x7f08008f

.field public static final preview_framelayout_item_width:I = 0x7f080090

.field public static final preview_item_height:I = 0x7f080091

.field public static final preview_item_text_height:I = 0x7f080092

.field public static final preview_item_text_size:I = 0x7f080093

.field public static final preview_item_width:I = 0x7f080094

.field public static final preview_margin:I = 0x7f080095

.field public static final prv_guide_map_height:I = 0x7f080096

.field public static final prv_guide_map_left:I = 0x7f080097

.field public static final prv_guide_map_right:I = 0x7f080098

.field public static final prv_guide_map_top:I = 0x7f080099

.field public static final prv_guide_map_width:I = 0x7f08009a

.field public static final pscomm_plus_minus_layout_height:I = 0x7f08009b

.field public static final pscomm_plus_minus_layout_marginLeft_or_right:I = 0x7f08009c

.field public static final pscomm_plus_minus_layout_width:I = 0x7f08009d

.field public static final pscomm_seekbar_layout_height:I = 0x7f08009e

.field public static final pscomm_seekbar_layout_width:I = 0x7f08009f

.field public static final pscomm_speed_ctrl_layout_layout_height:I = 0x7f0800a0

.field public static final quarter_detailed_seek_arrow_end_bottom_margin:I = 0x7f0800a1

.field public static final quick_panel_btn_layout_margin_left:I = 0x7f0800a2

.field public static final quick_panel_button_close_marginleft:I = 0x7f0800a3

.field public static final quick_panel_button_close_marginright:I = 0x7f0800a4

.field public static final quick_panel_button_marginleft:I = 0x7f0800a5

.field public static final quick_panel_button_marginright:I = 0x7f0800a6

.field public static final quick_panel_button_margintop:I = 0x7f0800a7

.field public static final quick_panel_control_layout_height:I = 0x7f0800a8

.field public static final quick_panel_default_marginebottom:I = 0x7f0800a9

.field public static final quick_panel_default_margineleft:I = 0x7f0800aa

.field public static final quick_panel_default_margineright:I = 0x7f0800ab

.field public static final quick_panel_default_marginetop:I = 0x7f0800ac

.field public static final quick_panel_default_size_height:I = 0x7f0800ad

.field public static final quick_panel_default_size_width:I = 0x7f0800ae

.field public static final quick_panel_exit_btn_marginright:I = 0x7f0800af

.field public static final quick_panel_ff_button_marginright:I = 0x7f0800b0

.field public static final quick_panel_height:I = 0x7f0800b1

.field public static final quick_panel_left_side_app_name_margin_top:I = 0x7f0800b2

.field public static final quick_panel_left_side_app_name_text_size:I = 0x7f0800b3

.field public static final quick_panel_left_side_default_button_height:I = 0x7f0800b4

.field public static final quick_panel_left_side_layout_width:I = 0x7f0800b5

.field public static final quick_panel_left_side_rew_button_margin:I = 0x7f0800b6

.field public static final quick_panel_rew_button_marginleft:I = 0x7f0800b7

.field public static final quick_panel_right_sid_title_layout_width:I = 0x7f0800b8

.field public static final quick_panel_right_side_center_button:I = 0x7f0800b9

.field public static final quick_panel_right_side_close_button_margin:I = 0x7f0800ba

.field public static final quick_panel_right_side_each_button:I = 0x7f0800bb

.field public static final quick_panel_right_side_ff_button_margin:I = 0x7f0800bc

.field public static final quick_panel_right_side_layout_margin_left:I = 0x7f0800bd

.field public static final quick_panel_right_side_play_pause_button_margin:I = 0x7f0800be

.field public static final quick_panel_right_side_title_margin_left:I = 0x7f0800bf

.field public static final quick_panel_right_side_title_text_size:I = 0x7f0800c0

.field public static final quick_panel_root_layout_height:I = 0x7f0800c1

.field public static final quick_panel_root_layout_padding_left:I = 0x7f0800c2

.field public static final quick_panel_root_layout_padding_right:I = 0x7f0800c3

.field public static final quick_panel_root_layout_padding_vertical:I = 0x7f0800c4

.field public static final quick_panel_root_layout_padding_vertical_top:I = 0x7f0800c5

.field public static final quick_panel_text_margin_top:I = 0x7f0800c6

.field public static final quick_panel_thumbnail_marginebottom:I = 0x7f0800c7

.field public static final quick_panel_thumbnail_marginetop:I = 0x7f0800c8

.field public static final quick_panel_thumbnail_size_height:I = 0x7f0800c9

.field public static final quick_panel_thumbnail_size_width:I = 0x7f0800ca

.field public static final quick_panel_title_margin_left:I = 0x7f0800cb

.field public static final quick_panel_title_margin_right:I = 0x7f0800cc

.field public static final quick_panel_title_text_height:I = 0x7f0800cd

.field public static final quick_panel_title_text_margine:I = 0x7f0800ce

.field public static final quick_panel_title_text_maxwidth:I = 0x7f0800cf

.field public static final quick_panel_title_text_width:I = 0x7f0800d0

.field public static final s_YaSeekBar_maxHeight:I = 0x7f0802fd

.field public static final s_YaSeekBar_minHeight:I = 0x7f0802fe

.field public static final s_YaSeekBar_thumbOffSet:I = 0x7f0802ff

.field public static final s_videoActionBarNoDisplayOptions_actionbar_height:I = 0x7f0800d1

.field public static final sbeamguide_popup_checkbox_text_margin_left:I = 0x7f0800d2

.field public static final sbeamguide_popup_explaintext_padding_left:I = 0x7f0800d3

.field public static final sbeamguide_popup_text_size:I = 0x7f0800d4

.field public static final scover_close_btn_margin_right:I = 0x7f0800d5

.field public static final scover_close_btn_margin_top:I = 0x7f0800d6

.field public static final scover_close_btn_width:I = 0x7f0800d7

.field public static final scover_main_height:I = 0x7f0800d8

.field public static final scover_root_window_height:I = 0x7f0800d9

.field public static final scover_root_window_width:I = 0x7f0800da

.field public static final setcomm_2line_layout_marginBottom:I = 0x7f0800db

.field public static final setcomm_2line_layout_marginTop:I = 0x7f0800dc

.field public static final setcomm_dialog_layout_1line_height:I = 0x7f0800dd

.field public static final setcomm_dialog_layout_height:I = 0x7f0800de

.field public static final setcomm_dialog_list_row_top_bottom_margin:I = 0x7f0800df

.field public static final setcomm_dialog_popup_layout_left_margin:I = 0x7f0800e0

.field public static final setcomm_dialog_popup_layout_margin_left:I = 0x7f0800e1

.field public static final setcomm_dialog_popup_layout_margin_right:I = 0x7f0800e2

.field public static final setcomm_dialog_popup_layout_marginright:I = 0x7f0800e3

.field public static final setcomm_dialog_popup_layout_right_margin:I = 0x7f0800e4

.field public static final setcomm_layout_dialog_listview_padding:I = 0x7f0800e5

.field public static final setcomm_layout_height:I = 0x7f0800e6

.field public static final setcomm_layout_margin_10:I = 0x7f0800e7

.field public static final setcomm_layout_margin_13:I = 0x7f0800e8

.field public static final setcomm_layout_margin_5:I = 0x7f0800e9

.field public static final setcomm_layout_margin_8:I = 0x7f0800ea

.field public static final setcomm_layout_paddingBottom:I = 0x7f0800eb

.field public static final setcomm_layout_paddingLeft:I = 0x7f0800ec

.field public static final setcomm_layout_paddingRight:I = 0x7f0800ed

.field public static final setcomm_sub_textSize:I = 0x7f0800ee

.field public static final setcomm_textSize:I = 0x7f0800ef

.field public static final setcomm_value_textSize:I = 0x7f0800f0

.field public static final share_icon_width:I = 0x7f0800f1

.field public static final share_popup_blank_height:I = 0x7f0800f2

.field public static final share_popup_height:I = 0x7f0800f3

.field public static final share_popup_offset:I = 0x7f0800f4

.field public static final share_popup_padding:I = 0x7f0800f5

.field public static final share_popup_total_height:I = 0x7f0800f6

.field public static final share_popup_width:I = 0x7f0800f7

.field public static final share_text_left_padding:I = 0x7f0800f8

.field public static final share_via_text_size:I = 0x7f0800f9

.field public static final soundalive_btn_height:I = 0x7f0800fa

.field public static final soundalive_btn_width:I = 0x7f0800fb

.field public static final subtitle_list_remove_last_divider:I = 0x7f0800fc

.field public static final subtitle_text_size:I = 0x7f0800fd

.field public static final subview_additionalview_height:I = 0x7f0800fe

.field public static final subview_button_layout_size:I = 0x7f0800ff

.field public static final subview_button_margin:I = 0x7f080100

.field public static final subview_button_size:I = 0x7f080101

.field public static final subview_ctrl_button_layout_width:I = 0x7f080102

.field public static final subview_ctrl_button_size:I = 0x7f080103

.field public static final subview_ctrl_playtime_text_margin:I = 0x7f080104

.field public static final subview_ctrl_playtime_text_size:I = 0x7f080105

.field public static final subview_ctrl_playtime_text_width:I = 0x7f080106

.field public static final subview_ctrl_progressbar_height:I = 0x7f080107

.field public static final subview_ctrl_progressbar_width:I = 0x7f080108

.field public static final subview_easymode_button_layout_size:I = 0x7f080109

.field public static final subview_easymode_button_margin:I = 0x7f08010a

.field public static final subview_easymode_ctrl_button_layout_size:I = 0x7f08010b

.field public static final subview_play_speed_img_layout_marginBottom:I = 0x7f08010c

.field public static final subview_play_speed_img_layout_marginLeft:I = 0x7f08010d

.field public static final subview_rotation_btn_marginBottom:I = 0x7f08010e

.field public static final subview_rotation_btn_marginRight:I = 0x7f08010f

.field public static final subview_search_speed_img_layout_height:I = 0x7f080110

.field public static final subview_search_speed_img_layout_width:I = 0x7f080111

.field public static final subview_speed_text_marginBottom:I = 0x7f080112

.field public static final subview_title_right_margin:I = 0x7f080113

.field public static final tag_buddy_text_margintop:I = 0x7f080114

.field public static final tag_buddy_text_paddingLeft:I = 0x7f080115

.field public static final text_button_paddingLeft:I = 0x7f080116

.field public static final text_button_paddingRight:I = 0x7f080117

.field public static final text_button_size:I = 0x7f080118

.field public static final text_contextual_accuweather_marginTop:I = 0x7f080119

.field public static final text_contextual_date_height:I = 0x7f08011a

.field public static final text_contextual_date_textSize:I = 0x7f08011b

.field public static final text_contextual_location_height:I = 0x7f08011c

.field public static final text_contextual_location_textSize:I = 0x7f08011d

.field public static final text_contextual_popup_date_height:I = 0x7f08011e

.field public static final text_contextual_popup_date_textSize:I = 0x7f08011f

.field public static final text_contextual_popup_location_height:I = 0x7f080120

.field public static final text_contextual_popup_location_textSize:I = 0x7f080121

.field public static final text_contextual_popup_weather_height:I = 0x7f080122

.field public static final text_contextual_popup_weather_textSize:I = 0x7f080123

.field public static final text_contextual_weather_height:I = 0x7f080124

.field public static final text_contextual_weather_textSize:I = 0x7f080125

.field public static final text_left_margin:I = 0x7f080126

.field public static final text_right_margin:I = 0x7f080127

.field public static final thumb_row_content_width:I = 0x7f080128

.field public static final thumb_row_effect_height:I = 0x7f080129

.field public static final thumb_row_effect_top_margin:I = 0x7f08012a

.field public static final thumb_row_image_frame_height:I = 0x7f08012b

.field public static final thumb_row_image_top_margin:I = 0x7f08012c

.field public static final thumb_row_item_bottom_margin:I = 0x7f08012d

.field public static final thumb_row_item_left_margin:I = 0x7f08012e

.field public static final thumb_row_item_right_margin:I = 0x7f08012f

.field public static final thumb_row_item_side_margin:I = 0x7f080300

.field public static final thumb_row_item_top_margin:I = 0x7f080130

.field public static final thumb_row_text_first_row_height:I = 0x7f080131

.field public static final thumb_row_text_first_row_text_size:I = 0x7f080132

.field public static final thumb_row_text_layout_top_margin:I = 0x7f080133

.field public static final thumb_row_text_second_row_height:I = 0x7f080134

.field public static final thumb_row_text_second_row_marginTop:I = 0x7f080135

.field public static final thumb_row_text_second_row_text_size:I = 0x7f080136

.field public static final thumb_row_total_width:I = 0x7f080137

.field public static final thumb_view_horizontal_spacing_port:I = 0x7f080138

.field public static final vao_embedded_image_height:I = 0x7f080139

.field public static final vao_embedded_image_width:I = 0x7f08013a

.field public static final vbc_bottom_padding_progress_view:I = 0x7f08013b

.field public static final vbc_current_time_text_height:I = 0x7f08013c

.field public static final vbc_current_time_text_size:I = 0x7f08013d

.field public static final vbc_current_time_text_width:I = 0x7f08013e

.field public static final vbc_hover_arrow_bottom_height:I = 0x7f08013f

.field public static final vbc_hover_custom_popup_offset:I = 0x7f080140

.field public static final vbc_hover_tooltip_top_above_position:I = 0x7f080141

.field public static final vbc_left_padding_progress_view:I = 0x7f080142

.field public static final vbc_playpause_tooltip_offset:I = 0x7f080143

.field public static final vbc_ps_hover_tooltip_top_above_position:I = 0x7f080144

.field public static final vbc_right_padding_progress_view:I = 0x7f080145

.field public static final vbc_snap_shot_layout_height:I = 0x7f080146

.field public static final vbc_snap_shot_layout_image_height:I = 0x7f080147

.field public static final vbc_snap_shot_layout_image_width:I = 0x7f080148

.field public static final vbc_snap_shot_layout_margin_bottom:I = 0x7f080149

.field public static final vbc_snap_shot_layout_margin_left:I = 0x7f08014a

.field public static final vbc_snap_shot_layout_margin_right:I = 0x7f08014b

.field public static final vbc_snap_shot_layout_margin_top:I = 0x7f08014c

.field public static final vbc_snap_shot_layout_width:I = 0x7f08014d

.field public static final vbc_thumb_image_height_for_wide_video:I = 0x7f08014e

.field public static final vbc_thumb_image_width_for_narrow_video:I = 0x7f08014f

.field public static final vbc_top_padding_progress_view:I = 0x7f080150

.field public static final vbc_vertical_seekbar_width:I = 0x7f080151

.field public static final vbcp_index_view_height:I = 0x7f080152

.field public static final vbcp_index_view_width:I = 0x7f080153

.field public static final vbcp_progressbar_padding:I = 0x7f080154

.field public static final vcv_btn_layout_height:I = 0x7f080155

.field public static final vcv_btn_layout_width:I = 0x7f080156

.field public static final vcv_cheatsheet_margin:I = 0x7f080157

.field public static final vcv_controller_layout_background_height:I = 0x7f080158

.field public static final vcv_controller_layout_background_marginTop:I = 0x7f080159

.field public static final vcv_controller_layout_height:I = 0x7f08015a

.field public static final vcv_controller_layout_marginBottom:I = 0x7f08015b

.field public static final vcv_controller_layout_marginBottom_on_VideoList:I = 0x7f08015c

.field public static final vcv_controller_layout_width:I = 0x7f08015d

.field public static final vcv_controller_second_layout_marginBottom_on_VideoList:I = 0x7f08015e

.field public static final vcv_ctrl_playtime_and_buttons_marginTop:I = 0x7f08015f

.field public static final vcv_fit_to_src_btn_height:I = 0x7f080160

.field public static final vcv_fit_to_src_btn_width:I = 0x7f080161

.field public static final vcv_more_btn_height:I = 0x7f080162

.field public static final vcv_more_btn_width:I = 0x7f080163

.field public static final vcv_multiwindow_bar_Margin:I = 0x7f080164

.field public static final vcv_multiwindow_controller_button_margin:I = 0x7f080165

.field public static final vcv_multiwindow_title_button_margin_left:I = 0x7f080166

.field public static final vcv_multiwindow_title_button_margin_right:I = 0x7f080167

.field public static final vcv_play_speed_img_layout_height:I = 0x7f080168

.field public static final vcv_play_speed_img_layout_marginBottom:I = 0x7f080169

.field public static final vcv_play_speed_img_layout_width:I = 0x7f08016a

.field public static final vcv_play_speed_mode_marginLeft:I = 0x7f08016b

.field public static final vcv_play_speed_mode_textSize:I = 0x7f08016c

.field public static final vcv_play_speed_mode_text_PaddingBottom:I = 0x7f08016d

.field public static final vcv_player_list_btn_height:I = 0x7f08016e

.field public static final vcv_player_list_btn_width:I = 0x7f08016f

.field public static final vcv_popup_player_controller_layout_margin_bottom:I = 0x7f080170

.field public static final vcv_popup_player_ctrl_btn_dimen:I = 0x7f080171

.field public static final vcv_popup_player_ctrl_btn_margin_left:I = 0x7f080172

.field public static final vcv_popup_player_ctrl_btn_min_margin_left:I = 0x7f080173

.field public static final vcv_popup_player_ctrl_height:I = 0x7f080174

.field public static final vcv_popup_player_ctrl_width:I = 0x7f080175

.field public static final vcv_popup_player_play_speed_layout_margin_bottom:I = 0x7f080176

.field public static final vcv_popup_player_subtitle_visible_ctrl_bg_bottom_margin:I = 0x7f080177

.field public static final vcv_popup_player_subtitle_visible_ctrl_no_bg_bottom_margin:I = 0x7f080178

.field public static final vcv_search_speed_img_layout_height:I = 0x7f080179

.field public static final vcv_search_speed_img_layout_width:I = 0x7f08017a

.field public static final vcv_search_speed_textSize:I = 0x7f08017b

.field public static final vcv_subtitle_height_from_controller:I = 0x7f08017c

.field public static final vcv_top_Layout_marginBottom:I = 0x7f08017d

.field public static final vcv_top_layout_height:I = 0x7f08017e

.field public static final vcv_videoplayer_btn_app_in_app_height:I = 0x7f08017f

.field public static final vcv_videoplayer_btn_app_in_app_marginBottom:I = 0x7f080301

.field public static final vcv_videoplayer_btn_app_in_app_marginRight:I = 0x7f0802fb

.field public static final vcv_videoplayer_btn_app_in_app_width:I = 0x7f080180

.field public static final vcv_videoplayer_btn_control_mini_arrow_height:I = 0x7f080181

.field public static final vcv_videoplayer_btn_control_mini_arrow_marginBottom:I = 0x7f080182

.field public static final vcv_videoplayer_btn_control_mini_arrow_marginLeftRight:I = 0x7f080183

.field public static final vcv_videoplayer_btn_control_mini_arrow_width:I = 0x7f080184

.field public static final vcv_videoplayer_btn_easy_play_button_height:I = 0x7f080185

.field public static final vcv_videoplayer_btn_easy_play_button_width:I = 0x7f080186

.field public static final vcv_videoplayer_btn_ff_margin_Bottom:I = 0x7f080187

.field public static final vcv_videoplayer_btn_ff_margin_Left:I = 0x7f080188

.field public static final vcv_videoplayer_btn_list_btn_margin_Left:I = 0x7f080189

.field public static final vcv_videoplayer_btn_margin_Bottom:I = 0x7f08018a

.field public static final vcv_videoplayer_btn_pause_layout_height:I = 0x7f08018b

.field public static final vcv_videoplayer_btn_pause_layout_width:I = 0x7f08018c

.field public static final vcv_videoplayer_btn_pause_margin_Bottom:I = 0x7f08018d

.field public static final vcv_videoplayer_btn_rew_layout_height:I = 0x7f08018e

.field public static final vcv_videoplayer_btn_rew_layout_width:I = 0x7f08018f

.field public static final vcv_videoplayer_btn_rew_margin_Bottom:I = 0x7f080302

.field public static final vcv_videoplayer_btn_rew_margin_Right:I = 0x7f080190

.field public static final vcv_videoplayer_btn_side_margin:I = 0x7f080191

.field public static final vcv_videoplayer_easymode_height:I = 0x7f080192

.field public static final vcv_videoplayer_easymode_progress_marginSide:I = 0x7f080193

.field public static final vcv_videoplayer_easymode_progress_marginTop:I = 0x7f080194

.field public static final vcv_videoplayer_easymode_time_marginSide:I = 0x7f080195

.field public static final vcv_videoplayer_easymode_time_marginTop:I = 0x7f080196

.field public static final vcv_videoplayer_easymode_time_width:I = 0x7f080197

.field public static final vcv_videoplayer_easymode_top_Layout_marginBottom:I = 0x7f080198

.field public static final vcv_videoplayer_progressbar_marginTop:I = 0x7f080199

.field public static final vcv_videoplayer_progressbar_timetext_size:I = 0x7f08019a

.field public static final vcv_videoplayer_time_current_layout_width:I = 0x7f08019b

.field public static final vcv_videoplayer_time_total_layout_width:I = 0x7f08019c

.field public static final vcv_volume_btn_marginRight:I = 0x7f08019d

.field public static final vgl_gesture_layout_height:I = 0x7f08019e

.field public static final vgl_gesture_layout_image_height:I = 0x7f08019f

.field public static final vgl_gesture_layout_image_layout_height:I = 0x7f0801a0

.field public static final vgl_gesture_layout_image_layout_marginTop:I = 0x7f0801a1

.field public static final vgl_gesture_layout_image_width:I = 0x7f0801a2

.field public static final vgl_gesture_layout_margin:I = 0x7f0801a3

.field public static final vgl_gesture_layout_marginTop:I = 0x7f0801a4

.field public static final vgl_gesture_layout_progressbar_layout_marginBottom:I = 0x7f0801a5

.field public static final vgl_gesture_layout_progressbar_layout_marginTop:I = 0x7f0801a6

.field public static final vgl_gesture_layout_progressbar_padding:I = 0x7f0801a7

.field public static final vgl_gesture_layout_progressbar_thickness:I = 0x7f0801a8

.field public static final vgl_gesture_layout_text_layout_height:I = 0x7f0801a9

.field public static final vgl_gesture_layout_text_layout_marginBottom:I = 0x7f0801aa

.field public static final vgl_gesture_layout_text_layout_marginTop:I = 0x7f080303

.field public static final vgl_gesture_layout_text_layout_textSize:I = 0x7f0801ab

.field public static final vgl_gesture_layout_width:I = 0x7f0801ac

.field public static final vhc_help_btn_bottom_margin_no_controller:I = 0x7f0801ad

.field public static final vhc_help_btn_bottom_margin_normal:I = 0x7f0801ae

.field public static final vhc_help_text_margin_left_right:I = 0x7f0801af

.field public static final vhc_help_text_margin_top:I = 0x7f0801b0

.field public static final vhc_help_text_width:I = 0x7f0801b1

.field public static final vhpp_block_bottom_height:I = 0x7f0801b2

.field public static final vhpp_block_top_height:I = 0x7f0801b3

.field public static final vhpp_progressbar_Preview_help_arrow_height:I = 0x7f0801b4

.field public static final vhpp_progressbar_Preview_help_arrow_layout_margin_bottom:I = 0x7f0801b5

.field public static final vhpp_progressbar_Preview_help_arrow_width:I = 0x7f0801b6

.field public static final vhpp_progressbar_Preview_help_text_inner_layout_height:I = 0x7f0801b7

.field public static final vhpp_progressbar_Preview_help_text_layout_height:I = 0x7f0801b8

.field public static final vhpp_progressbar_Preview_help_text_layout_margin_bottom:I = 0x7f0801b9

.field public static final vhpp_progressbar_Preview_help_text_layout_width:I = 0x7f0801ba

.field public static final vhpp_progressbar_Preview_help_text_margin_bottom:I = 0x7f0801bb

.field public static final vhpp_progressbar_Preview_help_text_margin_left_right:I = 0x7f0801bc

.field public static final vhpp_progressbar_Preview_help_text_size:I = 0x7f0801bd

.field public static final vhpp_smart_pause_help_text_size:I = 0x7f0801be

.field public static final vhpp_smart_pause_help_text_width:I = 0x7f0801bf

.field public static final video_player_play_pause_btn_circle_radius:I = 0x7f0801c0

.field public static final video_player_play_pause_btn_edge_width:I = 0x7f0801c1

.field public static final video_widget_addbtn_paddingBottom:I = 0x7f0801c2

.field public static final video_widget_addbtn_paddingRight:I = 0x7f0801c3

.field public static final video_widget_addbtn_width_height:I = 0x7f0801c4

.field public static final video_widget_ctrlbutton_height:I = 0x7f0801c5

.field public static final video_widget_default_height:I = 0x7f0801c6

.field public static final video_widget_default_width:I = 0x7f0801c7

.field public static final video_widget_height:I = 0x7f0801c8

.field public static final video_widget_magazine_details_height:I = 0x7f0801c9

.field public static final video_widget_magazine_details_margin:I = 0x7f0801ca

.field public static final video_widget_magazine_duration_marginLeft:I = 0x7f0801cb

.field public static final video_widget_magazine_fileinfo_height:I = 0x7f0801cc

.field public static final video_widget_magazine_filename_textSize:I = 0x7f0801cd

.field public static final video_widget_magazine_novideo1_marginTop:I = 0x7f0801ce

.field public static final video_widget_magazine_novideo1_textSize:I = 0x7f0801cf

.field public static final video_widget_magazine_novideo2_height:I = 0x7f0801d0

.field public static final video_widget_magazine_novideo2_textSize:I = 0x7f0801d1

.field public static final video_widget_magazine_remain_marginTop:I = 0x7f0801d2

.field public static final video_widget_magazine_time_textSize:I = 0x7f0801d3

.field public static final video_widget_magazine_title_marginLeft:I = 0x7f0801d4

.field public static final video_widget_magazine_title_marginTop:I = 0x7f0801d5

.field public static final video_widget_magazine_title_textSize:I = 0x7f0801d6

.field public static final video_widget_main_novideo1_marginBottom:I = 0x7f0801d7

.field public static final video_widget_main_novideo1_marginLeft:I = 0x7f0801d8

.field public static final video_widget_main_novideo1_textSize:I = 0x7f0801d9

.field public static final video_widget_main_vzw_novideo1_margin_bottom:I = 0x7f0801da

.field public static final video_widget_main_vzw_novideo1_padding_left:I = 0x7f0801db

.field public static final video_widget_main_vzw_novideo1_textSize:I = 0x7f0801dc

.field public static final video_widget_main_vzw_novideo2_left_margin:I = 0x7f0801dd

.field public static final video_widget_main_vzw_novideo2_textSize:I = 0x7f0801de

.field public static final video_widget_main_vzw_novideo2_top_margin:I = 0x7f0801df

.field public static final video_widget_novideo1_height:I = 0x7f0801e0

.field public static final video_widget_novideo1_marginLeft:I = 0x7f0801e1

.field public static final video_widget_novideo1_marginTop:I = 0x7f0801e2

.field public static final video_widget_novideo1_textSize:I = 0x7f0801e3

.field public static final video_widget_novideo1_text_bottom_margin:I = 0x7f0801e4

.field public static final video_widget_novideo1_text_sidemargin:I = 0x7f0801e5

.field public static final video_widget_novideo_shadow_height:I = 0x7f0801e6

.field public static final video_widget_play_marginBottom:I = 0x7f0801e7

.field public static final video_widget_player_bg_height:I = 0x7f0801e8

.field public static final video_widget_player_bottom_margin:I = 0x7f0801e9

.field public static final video_widget_player_details_height:I = 0x7f0801ea

.field public static final video_widget_player_details_marginLeft:I = 0x7f0801eb

.field public static final video_widget_player_details_marginRight:I = 0x7f0801ec

.field public static final video_widget_player_duration_height:I = 0x7f0801ed

.field public static final video_widget_player_duration_marginLeft:I = 0x7f0801ee

.field public static final video_widget_player_duration_marginTop:I = 0x7f0801ef

.field public static final video_widget_player_filename_height:I = 0x7f0801f0

.field public static final video_widget_player_filename_marginBottom:I = 0x7f0801f1

.field public static final video_widget_player_filename_marginTop:I = 0x7f0801f2

.field public static final video_widget_player_filename_paddingLeft:I = 0x7f0801f3

.field public static final video_widget_player_filename_textSize:I = 0x7f0801f4

.field public static final video_widget_player_filename_text_height:I = 0x7f0801f5

.field public static final video_widget_player_innerline_margin:I = 0x7f0801f6

.field public static final video_widget_player_innerline_margin_bottom:I = 0x7f0801f7

.field public static final video_widget_player_remain_height:I = 0x7f0801f8

.field public static final video_widget_player_remain_marginTop:I = 0x7f0801f9

.field public static final video_widget_player_shadow_height:I = 0x7f0801fa

.field public static final video_widget_player_time_textSize:I = 0x7f0801fb

.field public static final video_widget_player_time_text_height:I = 0x7f0801fc

.field public static final video_widget_progressbar_height:I = 0x7f0801fd

.field public static final videoplayer_list_height:I = 0x7f0801fe

.field public static final videoplayer_list_left_padding_land:I = 0x7f0801ff

.field public static final videoplayer_list_left_padding_port:I = 0x7f080200

.field public static final videoplayer_list_right_margin:I = 0x7f080201

.field public static final videoplayer_list_right_padding_land:I = 0x7f080202

.field public static final videoplayer_list_right_padding_port:I = 0x7f080203

.field public static final videoplayer_list_seekbar_margin:I = 0x7f080204

.field public static final videoplayer_list_seekbar_margin_bottom:I = 0x7f080205

.field public static final videoplayer_list_surface_height_land:I = 0x7f080206

.field public static final videoplayer_list_surface_height_port:I = 0x7f080207

.field public static final videoplayer_list_title2_bottom_margin:I = 0x7f080208

.field public static final videoplayer_list_title2_top_margin:I = 0x7f080209

.field public static final videoplayer_list_title_bottom_margin:I = 0x7f08020a

.field public static final videoplayer_list_title_height:I = 0x7f08020b

.field public static final videoplayer_list_title_padding_land:I = 0x7f08020c

.field public static final videoplayer_list_title_padding_port:I = 0x7f08020d

.field public static final videoplayer_list_title_textsize:I = 0x7f08020e

.field public static final videoplayer_list_title_top_margin:I = 0x7f08020f

.field public static final videoplayer_list_top_margin:I = 0x7f080210

.field public static final videoplayer_list_width:I = 0x7f080211

.field public static final videoplayer_lock_btn_layout_height:I = 0x7f080212

.field public static final videoplayer_lock_btn_marginRight:I = 0x7f080213

.field public static final videoplayer_lock_btn_marginTop:I = 0x7f080214

.field public static final vp_allshare_popup_volume_height:I = 0x7f080215

.field public static final vp_allshare_popup_volume_width:I = 0x7f080216

.field public static final vp_auto_brightness_description_text_size:I = 0x7f080217

.field public static final vp_brightness_controller_layout_height:I = 0x7f080218

.field public static final vp_brightness_description_height:I = 0x7f080219

.field public static final vp_brightness_description_margin:I = 0x7f08021a

.field public static final vp_brightness_description_text_size:I = 0x7f08021b

.field public static final vp_brightness_layout_checkbox_marginLeft:I = 0x7f08021c

.field public static final vp_brightness_layout_level_marginLeft:I = 0x7f08021d

.field public static final vp_brightness_layout_marginLeft:I = 0x7f080308

.field public static final vp_brightness_layout_marginRight:I = 0x7f08021e

.field public static final vp_brightness_layout_marginTop:I = 0x7f08021f

.field public static final vp_brightness_level_marginTop:I = 0x7f080220

.field public static final vp_brightness_seekbar_text_height:I = 0x7f080221

.field public static final vp_brightness_seekbar_text_size:I = 0x7f080222

.field public static final vp_brightness_seekbar_text_width:I = 0x7f080223

.field public static final vp_brightness_text_paddingRight:I = 0x7f080224

.field public static final vp_ctrl_vol_seekbar_layout_popup_height:I = 0x7f080225

.field public static final vp_ctrl_vol_seekbar_layout_popup_height_2nd_screen:I = 0x7f080226

.field public static final vp_ctrl_vol_seekbar_layout_popup_height_playerlist:I = 0x7f080227

.field public static final vp_ctrl_vol_seekbar_layout_popup_marginBottom:I = 0x7f080228

.field public static final vp_ctrl_vol_seekbar_layout_popup_marginTop:I = 0x7f080229

.field public static final vp_ctrl_vol_soundalive_btn_margin_bottom:I = 0x7f08022a

.field public static final vp_ctrl_vol_text_layout2_layout_height:I = 0x7f08022b

.field public static final vp_ctrl_vol_text_layout2_layout_marginBottom:I = 0x7f08022c

.field public static final vp_ctrl_vol_text_layout2_layout_marginBottom_playerlist:I = 0x7f08022d

.field public static final vp_ctrl_vol_vertical_popup_height:I = 0x7f08022e

.field public static final vp_ctrl_vol_vertical_popup_height_2ndscreen:I = 0x7f08022f

.field public static final vp_ctrl_vol_vertical_popup_height_playerlist:I = 0x7f080230

.field public static final vp_ctrl_vol_vertical_popup_maringTop:I = 0x7f080231

.field public static final vp_ctrl_vol_vertical_popup_rightMargin:I = 0x7f080232

.field public static final vp_ctrl_vol_vertical_popup_width:I = 0x7f080233

.field public static final vp_flick_text_size:I = 0x7f080234

.field public static final vp_layout_marginTop:I = 0x7f080235

.field public static final vp_playspeed_btn_size:I = 0x7f080236

.field public static final vp_playspeed_marginBottom:I = 0x7f080237

.field public static final vp_playspeed_marginRightLeft:I = 0x7f080238

.field public static final vp_playspeed_textHeight:I = 0x7f080239

.field public static final vp_playspeed_textMarginBottom:I = 0x7f08023a

.field public static final vp_playspeed_textMarginTop:I = 0x7f08023b

.field public static final vp_playspeed_textSize:I = 0x7f08023c

.field public static final vp_seekbar_Layout_marginBottom:I = 0x7f08023d

.field public static final vp_seekbar_Layout_marginLeft:I = 0x7f08023e

.field public static final vp_seekbar_Layout_marginRight:I = 0x7f08023f

.field public static final vp_seekbar_Layout_marginTop:I = 0x7f080240

.field public static final vp_seekbar_auto_height:I = 0x7f080241

.field public static final vp_seekbar_auto_marginRight:I = 0x7f080242

.field public static final vp_seekbar_auto_paddingLeft:I = 0x7f080243

.field public static final vp_seekbar_auto_paddingRight:I = 0x7f080244

.field public static final vp_seekbar_bright_paddingRight:I = 0x7f080245

.field public static final vp_seekbar_marginLeft:I = 0x7f080304

.field public static final vp_seekbar_marginRight:I = 0x7f080246

.field public static final vp_seekbar_text_marginRight:I = 0x7f080247

.field public static final vp_tw_split_seekbar_marginRight:I = 0x7f080248

.field public static final vp_vol_seekbar_popup_paddingBottom:I = 0x7f080249

.field public static final vp_vol_seekbar_popup_paddingEnd:I = 0x7f08024a

.field public static final vp_vol_seekbar_popup_paddingStart:I = 0x7f08024b

.field public static final vp_vol_seekbar_popup_paddingTop:I = 0x7f08024c

.field public static final vp_vol_seekbar_popup_twIndicatorThickness:I = 0x7f08024d

.field public static final vp_vol_seekbar_thumb_height:I = 0x7f08024e

.field public static final vp_vol_seekbar_thumb_width:I = 0x7f08024f

.field public static final vp_vol_text_popup_textSize:I = 0x7f080250

.field public static final vppp_addtional_margin:I = 0x7f080251

.field public static final vppp_adjust_popup_multi:I = 0x7f080252

.field public static final vppp_adjust_popup_y:I = 0x7f080253

.field public static final vppp_arrow_left_margin:I = 0x7f080254

.field public static final vppp_arrow_right_margin:I = 0x7f080255

.field public static final vppp_arrow_side_margin:I = 0x7f080256

.field public static final vppp_arrow_top_margin:I = 0x7f080257

.field public static final vppp_arrow_width:I = 0x7f080258

.field public static final vppp_height_for_wide_video:I = 0x7f080259

.field public static final vppp_left_margin:I = 0x7f08025a

.field public static final vppp_leftside_popup_left_margin:I = 0x7f08025b

.field public static final vppp_right_margin:I = 0x7f08025c

.field public static final vppp_rightside_popup_text_right_margin:I = 0x7f08025d

.field public static final vppp_seek_bar_rect_margin:I = 0x7f08025e

.field public static final vppp_surface_height_for_wide_video:I = 0x7f08025f

.field public static final vppp_surface_left_margin:I = 0x7f080260

.field public static final vppp_surface_left_margin_thumbnail:I = 0x7f080261

.field public static final vppp_surface_right_margin:I = 0x7f080262

.field public static final vppp_surface_right_margin_thumbnail:I = 0x7f080263

.field public static final vppp_surface_top_margin:I = 0x7f080264

.field public static final vppp_surface_top_margin_thumbnail:I = 0x7f080265

.field public static final vppp_surface_width_for_narrow_video:I = 0x7f080266

.field public static final vppp_time_textview_height:I = 0x7f080267

.field public static final vppp_time_textview_size:I = 0x7f080268

.field public static final vppp_time_textview_top_margin:I = 0x7f080269

.field public static final vppp_top_margin:I = 0x7f08026a

.field public static final vppp_video_preview_dialog:I = 0x7f08026b

.field public static final vppp_video_preview_dialog_for_penwindow:I = 0x7f08026c

.field public static final vppp_width_for_narrow_video:I = 0x7f08026d

.field public static final vscao_btn_size:I = 0x7f08026e

.field public static final vscao_colorPalette_height:I = 0x7f08026f

.field public static final vscao_colorPalette_marginBottom:I = 0x7f080270

.field public static final vscao_colorPalette_marginTop:I = 0x7f080271

.field public static final vscao_item_margin:I = 0x7f080272

.field public static final vscao_item_margin_land:I = 0x7f080273

.field public static final vscao_lable_height:I = 0x7f080274

.field public static final vscao_opacity_gridview_height:I = 0x7f080275

.field public static final vscao_opacity_layout_height:I = 0x7f080276

.field public static final vscao_opacity_marginBottom:I = 0x7f080277

.field public static final vscao_opacity_marginLeft:I = 0x7f080278

.field public static final vscao_opacity_marginRight:I = 0x7f080279

.field public static final vscao_opacity_text_marginTop:I = 0x7f08027a

.field public static final vscao_presetColor_horizontalSpacing:I = 0x7f08027b

.field public static final vscao_presetColor_marginSide:I = 0x7f08027c

.field public static final vscao_presetColor_marginTop:I = 0x7f08027d

.field public static final vscao_presetColor_verticalSpacing:I = 0x7f08027e

.field public static final vscao_seekbar_marginside:I = 0x7f08027f

.field public static final vscp_layout_size:I = 0x7f080280

.field public static final vscp_preset_marginSide:I = 0x7f080281

.field public static final vscp_preset_marginTop:I = 0x7f080282

.field public static final vscp_preset_size:I = 0x7f080283

.field public static final vsl_subtitle_font_glow_size:I = 0x7f080284

.field public static final vsl_subtitle_font_shadow_size:I = 0x7f080285

.field public static final vsl_subtitle_text_padding:I = 0x7f080286

.field public static final vsl_subtitle_view_side_margin:I = 0x7f080287

.field public static final vsm_actionbar_icon_width:I = 0x7f080288

.field public static final vsm_actionbar_switch_padding:I = 0x7f080289

.field public static final vsm_actionbar_switch_width:I = 0x7f08028a

.field public static final vsm_actionbar_title_size:I = 0x7f08028b

.field public static final vsm_actionbar_title_size_land:I = 0x7f08028c

.field public static final vsm_list_main_top_padding:I = 0x7f08028d

.field public static final vsm_list_main_width:I = 0x7f08028e

.field public static final vsm_list_main_width_land:I = 0x7f080307

.field public static final vsm_preview_caption_bottom_margin:I = 0x7f08028f

.field public static final vsm_preview_image_height:I = 0x7f080290

.field public static final vsm_preview_image_margin_top:I = 0x7f080291

.field public static final vsm_preview_image_top_margin:I = 0x7f080292

.field public static final vsm_preview_image_width:I = 0x7f080293

.field public static final vsm_preview_marginBottom:I = 0x7f080294

.field public static final vsm_preview_marginLeft:I = 0x7f080295

.field public static final vsm_preview_marginRight:I = 0x7f080296

.field public static final vsm_preview_marginTop:I = 0x7f080297

.field public static final vsm_preview_split_width:I = 0x7f080298

.field public static final vsml_cbSubtitle_layout_marginRight:I = 0x7f080299

.field public static final vsml_divider_height:I = 0x7f08029a

.field public static final vsml_layout_height:I = 0x7f08029b

.field public static final vsmp_colortone_ProgressBar_margin:I = 0x7f08029c

.field public static final vsmp_colortone_layout_height:I = 0x7f08029d

.field public static final vsmp_colortone_textSize:I = 0x7f08029e

.field public static final vsmp_colortone_text_marginLeft:I = 0x7f08029f

.field public static final vsmp_colortone_text_marginRight:I = 0x7f0802a0

.field public static final vsmp_colortone_title_marginLeft:I = 0x7f0802a1

.field public static final vsmp_colortone_title_marginTop:I = 0x7f0802a2

.field public static final vsmp_colortone_title_textSize:I = 0x7f0802a3

.field public static final vsmp_ourdoorvisibility_CheckBox_height:I = 0x7f0802a4

.field public static final vsmp_ourdoorvisibility_CheckBox_marginRight:I = 0x7f0802a5

.field public static final vsmp_ourdoorvisibility_CheckBox_width:I = 0x7f0802a6

.field public static final vsmp_ourdoorvisibility_layout_height:I = 0x7f0802a7

.field public static final vsmp_ourdoorvisibility_title_marginLeft:I = 0x7f0802a8

.field public static final vsmp_ourdoorvisibility_title_textSize:I = 0x7f0802a9

.field public static final vsmp_thumbnail_height:I = 0x7f0802aa

.field public static final vsmp_thumbnail_width:I = 0x7f0802ab

.field public static final vss_list_row_minHeight:I = 0x7f0802ac

.field public static final vssl_bottom_margin:I = 0x7f0802ad

.field public static final vssl_dialog_margin_top:I = 0x7f0802ae

.field public static final vssl_div_height:I = 0x7f0802af

.field public static final vssl_font_size:I = 0x7f0802b0

.field public static final vssl_lable_font_size:I = 0x7f0802b1

.field public static final vssl_lable_height:I = 0x7f0802b2

.field public static final vssl_lable_list_layout_top_margin:I = 0x7f080305

.field public static final vssl_lable_top_margin:I = 0x7f080306

.field public static final vssl_layout_minHeight:I = 0x7f0802b3

.field public static final vssl_list_item_height:I = 0x7f0802b4

.field public static final vssl_list_margin:I = 0x7f0802b5

.field public static final vssl_text_height:I = 0x7f0802b6

.field public static final vssl_text_margin:I = 0x7f0802b7

.field public static final vssl_text_popup_margin:I = 0x7f0802b8

.field public static final vssl_top_margin:I = 0x7f0802b9

.field public static final vssl_value_font_size:I = 0x7f0802ba

.field public static final vssl_value_height:I = 0x7f0802bb

.field public static final vst_videoplayer_progress_btn_width_n_height:I = 0x7f0802bc

.field public static final vt_3d_ctrl_layout_height:I = 0x7f0802bd

.field public static final vt_3d_ctrl_layout_marginLeft:I = 0x7f0802be

.field public static final vt_3d_ctrl_layout_width:I = 0x7f0802bf

.field public static final vt_allshare_popup_volume_button_height:I = 0x7f0802c0

.field public static final vt_allshare_popup_volume_button_width:I = 0x7f0802c1

.field public static final vt_allshare_volume_down_height:I = 0x7f0802c2

.field public static final vt_allshare_volume_down_marginLeft:I = 0x7f0802c3

.field public static final vt_allshare_volume_down_marginLeft_playerList:I = 0x7f0802c4

.field public static final vt_allshare_volume_down_width:I = 0x7f0802c5

.field public static final vt_allshare_volume_mute_height:I = 0x7f0802c6

.field public static final vt_allshare_volume_mute_marginRight:I = 0x7f0802c7

.field public static final vt_allshare_volume_mute_width:I = 0x7f0802c8

.field public static final vt_allshare_volume_up_height:I = 0x7f0802c9

.field public static final vt_allshare_volume_up_marginRight:I = 0x7f0802ca

.field public static final vt_allshare_volume_up_marginRight_playerList:I = 0x7f0802cb

.field public static final vt_allshare_volume_up_width:I = 0x7f0802cc

.field public static final vt_asf_display_change_button_marginLeft:I = 0x7f0802cd

.field public static final vt_asf_first_layout_height:I = 0x7f0802ce

.field public static final vt_asf_first_layout_margin:I = 0x7f0802cf

.field public static final vt_asf_second_layout_height:I = 0x7f0802d0

.field public static final vt_asf_second_layout_marginTop:I = 0x7f0802d1

.field public static final vt_button_margin:I = 0x7f0802d2

.field public static final vt_capture_btn_height:I = 0x7f0802d3

.field public static final vt_capture_btn_width:I = 0x7f0802d4

.field public static final vt_ctrl_layout_margin:I = 0x7f0802d5

.field public static final vt_display_change_button_height:I = 0x7f0802d6

.field public static final vt_display_change_button_marginLeft:I = 0x7f0802d7

.field public static final vt_display_change_button_width:I = 0x7f0802d8

.field public static final vt_divider_height:I = 0x7f0802d9

.field public static final vt_divider_margin:I = 0x7f0802da

.field public static final vt_divider_width:I = 0x7f0802db

.field public static final vt_first_layout_height:I = 0x7f0802dc

.field public static final vt_first_layout_margin_top:I = 0x7f0802dd

.field public static final vt_more_button_right_margin:I = 0x7f0802de

.field public static final vt_one_frame_forward_layout_margin:I = 0x7f0802df

.field public static final vt_one_frame_height:I = 0x7f0802e0

.field public static final vt_one_frame_width:I = 0x7f0802e1

.field public static final vt_personal_page_icon_margin_left:I = 0x7f0802e2

.field public static final vt_rotate_ctrl_layout_height:I = 0x7f0802e3

.field public static final vt_rotate_ctrl_layout_marginRight:I = 0x7f0802fc

.field public static final vt_rotate_ctrl_layout_width:I = 0x7f0802e4

.field public static final vt_second_layout_height:I = 0x7f0802e5

.field public static final vt_second_layout_marginTop:I = 0x7f0802e6

.field public static final vt_title_layout_height:I = 0x7f0802e7

.field public static final vt_title_text_layout_height:I = 0x7f0802e8

.field public static final vt_title_text_layout_margin_left:I = 0x7f0802e9

.field public static final vt_title_text_layout_margin_left_multiwindow:I = 0x7f0802ea

.field public static final vt_title_text_layout_margin_top:I = 0x7f0802eb

.field public static final vt_title_text_layout_width:I = 0x7f0802ec

.field public static final vt_title_text_textsize:I = 0x7f0802ed

.field public static final vt_videohub_resolution_layout_marginLeft:I = 0x7f0802ee

.field public static final vt_videohub_title_layout_marginLeft:I = 0x7f0802ef

.field public static final vt_videohub_title_layout_width:I = 0x7f0802f0

.field public static final vt_videoplayer_btn_ctr_height:I = 0x7f0802f1

.field public static final vt_videoplayer_btn_ctr_width:I = 0x7f0802f2

.field public static final vt_volume_btn_height:I = 0x7f0802f3

.field public static final vt_volume_btn_margin_right:I = 0x7f0802f4

.field public static final vt_volume_btn_width:I = 0x7f0802f5

.field public static final vt_watermark_layout_height:I = 0x7f0802f6

.field public static final weather_list_height:I = 0x7f0802f7

.field public static final zoom_reset_doubletap_margin:I = 0x7f0802f8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
