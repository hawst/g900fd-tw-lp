.class Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;->this$1:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "view"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 321
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;->this$1:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlMode()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;->this$1:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a00bb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter$1;->this$1:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v1

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setMiniCtrlMode(I)V

    .line 325
    return-void

    .line 324
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
