.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;
.super Lcom/sec/android/app/minimode/MiniModeService$TouchListener;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnTouchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 707
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0, p1}, Lcom/sec/android/app/minimode/MiniModeService$TouchListener;-><init>(Lcom/sec/android/app/minimode/MiniModeService;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v10, 0x96

    const/16 v9, 0x8

    const/4 v1, 0x1

    const/4 v8, 0x0

    .line 709
    invoke-super {p0, p1, p2}, Lcom/sec/android/app/minimode/MiniModeService$TouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 710
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "onTouch"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 713
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 715
    .local v2, "pressTime":J
    packed-switch v0, :pswitch_data_0

    .line 761
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v4, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isOutsideOfScreenLimit(Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 762
    :goto_1
    return v1

    .line 717
    :pswitch_0
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "OnTouchListener. ACTION_UP"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 719
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v4, :cond_1

    cmp-long v4, v2, v10

    if-ltz v4, :cond_0

    .line 723
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitByMoviePlayer:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 724
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 725
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 728
    :cond_2
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v4, :cond_3

    .line 729
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setProgressBarVisibility(I)V
    invoke-static {v4, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)V

    goto :goto_0

    .line 730
    :cond_3
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v4, :cond_0

    .line 731
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v4

    const/16 v5, 0xbb8

    invoke-virtual {v4, v8, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_0

    .line 737
    :pswitch_1
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "OnTouchListener. ACTION_DOWN"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    goto :goto_0

    .line 742
    :pswitch_2
    const-string v4, "MiniVideoPlayerService"

    const-string v5, "OnTouchListener. ACTION_MOVE"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    cmp-long v4, v2, v10

    if-ltz v4, :cond_0

    .line 747
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z
    invoke-static {v4, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 748
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 749
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 752
    :cond_4
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v4, :cond_5

    .line 753
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setProgressBarVisibility(I)V
    invoke-static {v4, v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)V

    goto/16 :goto_0

    .line 754
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v4, :cond_0

    .line 755
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->forceHide()V

    goto/16 :goto_0

    .line 762
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$OnTouchListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getGestureDetector()Landroid/view/GestureDetector;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto/16 :goto_1

    .line 715
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
