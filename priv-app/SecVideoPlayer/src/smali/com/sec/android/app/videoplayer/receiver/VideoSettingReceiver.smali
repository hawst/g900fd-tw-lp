.class public Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VideoSettingReceiver.java"


# static fields
.field private static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 19
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 20
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->isFromSLinkApp()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 21
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;->TAG:Ljava/lang/String;

    const-string v3, "onReceive. ACTION_AUTO_PLAY_NEXT: Video is Playing from SLinkApp don\'t save values."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 28
    const-string v2, "com.sec.android.app.videoplayer.AUTO_PLAY_NEXT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 29
    const-string v2, "autoplay"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 30
    .local v0, "autoNextPlay":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive. ACTION_AUTO_PLAY_NEXT: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "autoPlay"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    goto :goto_0

    .line 32
    .end local v0    # "autoNextPlay":Z
    :cond_2
    const-string v2, "com.sec.android.app.videoplayer.CLOUD_OPTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    const-string v2, "cloud_option"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 34
    .local v1, "contentShowOpt":I
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoSettingReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive. ACTION_UPDATE_CONTENT_SHOW: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "list_view_by_cloud"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto :goto_0
.end method
