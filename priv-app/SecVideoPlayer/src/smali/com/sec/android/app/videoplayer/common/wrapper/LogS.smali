.class public Lcom/sec/android/app/videoplayer/common/wrapper/LogS;
.super Ljava/lang/Object;
.source "LogS.java"


# static fields
.field public static final DEBUG:Z

.field public static final IS_ENG_MODE:Z

.field public static final IS_PRODUCT_SHIP:Z

.field public static PRINT_ALL_TRACE:I = 0x0

.field public static final TAG:Ljava/lang/String; = "LogS"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 6
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->IS_PRODUCT_SHIP:Z

    .line 7
    const-string v0, "eng"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->IS_ENG_MODE:Z

    .line 10
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->PRINT_ALL_TRACE:I

    .line 13
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->IS_PRODUCT_SHIP:Z

    if-eqz v0, :cond_0

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->IS_ENG_MODE:Z

    :cond_0
    sput-boolean v1, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    return-void

    .line 6
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static _stackTrace(Ljava/lang/String;I)V
    .locals 7
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "length"    # I

    .prologue
    .line 63
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 64
    .local v3, "stacktrace":Ljava/lang/StringBuffer;
    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    .line 65
    .local v2, "stackTrace":[Ljava/lang/StackTraceElement;
    array-length v1, v2

    .line 66
    .local v1, "len":I
    const-string v0, "LogS"

    .line 67
    .local v0, "Tag":Ljava/lang/String;
    sget v5, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->PRINT_ALL_TRACE:I

    if-eq p1, v5, :cond_0

    if-le v1, p1, :cond_0

    .line 68
    move v1, p1

    .line 70
    :cond_0
    if-eqz p0, :cond_1

    .line 71
    move-object v0, p0

    .line 73
    :cond_1
    const/4 v4, 0x2

    .local v4, "x":I
    :goto_0
    if-ge v4, v1, :cond_2

    .line 74
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v2, v4

    invoke-virtual {v6}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0xa

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 75
    :cond_2
    const-string v5, "------------ Stacktrace ---------------"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 23
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 24
    invoke-static {p0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    :goto_0
    return-void

    .line 26
    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 45
    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 30
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 31
    invoke-static {p0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    :goto_0
    return-void

    .line 33
    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static stackTrace()V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    sget v1, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->PRINT_ALL_TRACE:I

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->_stackTrace(Ljava/lang/String;I)V

    .line 52
    return-void
.end method

.method public static stackTrace(I)V
    .locals 2
    .param p0, "n"    # I

    .prologue
    .line 55
    const/4 v0, 0x0

    add-int/lit8 v1, p0, 0x2

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->_stackTrace(Ljava/lang/String;I)V

    .line 56
    return-void
.end method

.method public static stackTrace(Ljava/lang/String;I)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "n"    # I

    .prologue
    .line 59
    add-int/lit8 v0, p1, 0x2

    invoke-static {p0, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->_stackTrace(Ljava/lang/String;I)V

    .line 60
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 16
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 17
    invoke-static {p0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    :goto_0
    return-void

    .line 19
    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "log"    # Ljava/lang/String;

    .prologue
    .line 37
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 38
    invoke-static {p0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-static {p0, p1}, Landroid/util/Log;->secW(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
