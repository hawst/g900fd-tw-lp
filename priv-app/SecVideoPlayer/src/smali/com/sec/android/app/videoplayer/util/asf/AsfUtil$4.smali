.class Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;
.super Ljava/lang/Object;
.source "AsfUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0

    .prologue
    .line 877
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBuffering()V
    .locals 4

    .prologue
    .line 971
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSecAvPlayerEventListener. onBuffering"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/4 v1, 0x6

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 973
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 975
    return-void
.end method

.method public onError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "e"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v4, 0x1

    .line 962
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSecAvPlayerEventListener. onError : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 964
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 966
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 967
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mContentChanged:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1902(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z

    .line 968
    return-void
.end method

.method public onFinish()V
    .locals 12

    .prologue
    const/16 v11, 0x2eb

    const/16 v10, 0x2dc

    const/4 v6, 0x0

    const/4 v0, 0x1

    .line 916
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mSecAvPlayerEventListener. onFinish."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v7, v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 918
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 920
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 921
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mSecAvPlayerEventListener. onFinish. mExitAfterCurrentMovie: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1700(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v7, v11, v10}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 923
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mExitAfterCurrentMovie:Z
    invoke-static {v7, v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1702(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Z)Z

    .line 959
    :cond_0
    :goto_0
    return-void

    .line 927
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    .line 928
    .local v3, "schemeType":Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    .line 929
    .local v4, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v2

    .line 931
    .local v2, "playlistUtil":Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;
    const/4 v1, 0x1

    .line 932
    .local v1, "finish":Z
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getAutoPlayNext()Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1800(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 933
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isVideoList()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isDirectDMC()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 934
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mSecAvPlayerEventListener. onFinish - try to play next content via Asf"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->stop()V

    .line 937
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 938
    const/4 v1, 0x0

    .line 955
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    .line 956
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mSecAvPlayerEventListener. onFinish. try to finish playing via Asf. type: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getKeyType()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 957
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v6, v11, v10}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    goto :goto_0

    .line 942
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v5

    .line 943
    .local v5, "totalCnt":I
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isDirectDMC()Z

    move-result v7

    if-eqz v7, :cond_3

    if-le v5, v0, :cond_3

    .line 944
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mSecAvPlayerEventListener. onFinish - try to play next content via Asf"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 945
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->stop()V

    .line 947
    add-int/lit8 v7, v5, -0x1

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getCurIdx()I

    move-result v8

    if-ne v7, v8, :cond_6

    .line 948
    .local v0, "completed":Z
    :goto_2
    if-nez v0, :cond_5

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 949
    const/4 v1, 0x0

    .line 951
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mSecAvPlayerEventListener. onFinish. playing all selected contents: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .end local v0    # "completed":Z
    :cond_6
    move v0, v6

    .line 947
    goto :goto_2
.end method

.method public onPause()V
    .locals 4

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSecAvPlayerEventListener. onPause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 910
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/4 v1, 0x5

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 911
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 913
    return-void
.end method

.method public onPlay()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSecAvPlayerEventListener. onPlay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 901
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e9

    const/4 v2, 0x0

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 902
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 903
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 904
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 906
    :cond_0
    return-void
.end method

.method public onProgress(J)V
    .locals 5
    .param p1, "pos"    # J

    .prologue
    .line 886
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSecAvPlayerEventListener. onProgress. pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 888
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->handlingSeek:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1400(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v0

    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 889
    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, p1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerDuration:J
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1500(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 890
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mAvPlayerCurrentPosition:J
    invoke-static {v0, p1, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1602(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;J)J

    .line 896
    :cond_0
    :goto_0
    return-void

    .line 893
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSecAvPlayerEventListener. onProgress setting pos is skipped "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 879
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSecAvPlayerEventListener. onStop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 881
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2e5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$4;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 883
    return-void
.end method
