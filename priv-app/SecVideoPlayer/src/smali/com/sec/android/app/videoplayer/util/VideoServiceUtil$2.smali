.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;
.super Landroid/os/Handler;
.source "VideoServiceUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 1493
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1495
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 1588
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1497
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayingChecker()V

    .line 1498
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1500
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 1513
    .local v0, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isSurfaceExist()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isSurfaceTextureExist()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1514
    :cond_1
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler - STARTPLAYBACK"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1517
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->checkDRMFile()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1518
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->openPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1519
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_0

    .line 1521
    :cond_2
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler. openPath return false."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1522
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    goto :goto_0

    .line 1525
    :cond_3
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler() - checkDRMFile return false."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1526
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    goto/16 :goto_0

    .line 1530
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v1

    if-gez v1, :cond_5

    .line 1531
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler. mPlayingCheckTime is below 0. stopPlayingChecker() called"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1532
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 1533
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 1534
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v1

    invoke-interface {v1, v5, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1538
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v2, 0x12c

    # -= operator for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$020(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I

    .line 1540
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler Error state. mPlayingCheckTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1541
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler Error state. surfaceExist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sService : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1542
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler Error state. surfaceGLExist : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", sService : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayingChecker()V

    .line 1547
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1552
    .end local v0    # "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$500(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v1

    if-ne v1, v4, :cond_6

    .line 1553
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ffSeek()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$600(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    goto/16 :goto_0

    .line 1554
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$500(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v1

    if-ne v1, v6, :cond_0

    .line 1555
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->rewSeek()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$700(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    goto/16 :goto_0

    .line 1559
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1560
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 1562
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1563
    const/4 v2, 0x7

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v2, v1, :cond_8

    .line 1564
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$800(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v6, v2, v3, v5}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    .line 1572
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const v2, 0xc350

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$002(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I

    goto/16 :goto_0

    .line 1566
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->checkDRMFile()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1567
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->openPath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1568
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v1

    const/16 v2, 0x8

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_1

    .line 1574
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I

    move-result v1

    if-gez v1, :cond_a

    .line 1575
    const-string v1, "VideoServiceUtil"

    const-string v2, "mHandler. mPlayingCheckTime is below 0. stopPlayingChecker() called"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1576
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const v2, 0xc350

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$002(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I

    .line 1577
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    move-result-object v1

    invoke-interface {v1, v5, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1580
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v2, 0x12c

    # -= operator for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$020(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I

    .line 1581
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v2, v7, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1495
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
