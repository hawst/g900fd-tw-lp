.class Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;
.super Ljava/lang/Object;
.source "PlayerListView.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 453
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$000(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 454
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 455
    .local v0, "value":Ljava/lang/Integer;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 456
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 457
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    .line 464
    :goto_0
    return-void

    .line 460
    .end local v0    # "value":Ljava/lang/Integer;
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 461
    .restart local v0    # "value":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 462
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    goto :goto_0
.end method
