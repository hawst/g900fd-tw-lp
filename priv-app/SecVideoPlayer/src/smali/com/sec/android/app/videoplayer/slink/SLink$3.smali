.class Lcom/sec/android/app/videoplayer/slink/SLink$3;
.super Landroid/os/Handler;
.source "SLink.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/slink/SLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/slink/SLink;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/slink/SLink;)V
    .locals 0

    .prologue
    .line 887
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 889
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 928
    :goto_0
    return-void

    .line 891
    :pswitch_0
    const-string v0, "SLink"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HANDLE_WAIT_Scs_DATA : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$000(Lcom/sec/android/app/videoplayer/slink/SLink;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$100(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 893
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 894
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->startPlayback()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$200(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    goto :goto_0

    .line 896
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$300(Lcom/sec/android/app/videoplayer/slink/SLink;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 897
    const-string v0, "SLink"

    const-string v1, "HANDLE_WAIT_Scs_DATA : SLink Data is not exist try to get again"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 898
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$400(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->updateSlinkScsCoreConfig(Landroid/content/Context;)V

    .line 899
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 900
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->startPlayback()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$200(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    goto :goto_0

    .line 905
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$000(Lcom/sec/android/app/videoplayer/slink/SLink;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    .line 906
    const-string v0, "SLink"

    const-string v1, "HANDLE_WAIT_Scs_DATA : Data not comming from samsung account... try to request again"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$500(Lcom/sec/android/app/videoplayer/slink/SLink;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$500(Lcom/sec/android/app/videoplayer/slink/SLink;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 908
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->destroySAService()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$600(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    .line 910
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->requestAccessToken()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$700(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    .line 923
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$100(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 924
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # operator++ for: Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$008(Lcom/sec/android/app/videoplayer/slink/SLink;)I

    goto/16 :goto_0

    .line 911
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$000(Lcom/sec/android/app/videoplayer/slink/SLink;)I

    move-result v0

    const/16 v1, 0xa

    if-le v0, v1, :cond_3

    .line 912
    const-string v0, "SLink"

    const-string v1, "HANDLE_WAIT_Scs_DATA : Data not comming from samsung account... Stop waiting... just make error!!!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->unBindSAService()V

    .line 915
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$400(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # getter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$400(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const-string v1, "http://ERROR"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 917
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    const-string v1, "http://ERROR"

    # setter for: Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$802(Lcom/sec/android/app/videoplayer/slink/SLink;Ljava/lang/String;)Ljava/lang/String;

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink$3;->this$0:Lcom/sec/android/app/videoplayer/slink/SLink;

    # invokes: Lcom/sec/android/app/videoplayer/slink/SLink;->startPlayback()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->access$200(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    goto/16 :goto_0

    .line 889
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
