.class public Lcom/sec/android/app/videoplayer/view/help/HelpData;
.super Ljava/lang/Object;
.source "HelpData.java"


# static fields
.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/view/help/HelpData;


# instance fields
.field private mDescription:[Ljava/lang/String;

.field private mStartClip:I

.field private mTimeStemp:[J

.field private mTitle:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/HelpData;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpData;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mUniqueInstance:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTimeStemp:[J

    .line 5
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mDescription:[Ljava/lang/String;

    .line 6
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTitle:[Ljava/lang/String;

    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mStartClip:I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/view/help/HelpData;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mUniqueInstance:Lcom/sec/android/app/videoplayer/view/help/HelpData;

    return-object v0
.end method


# virtual methods
.method public getDescription()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mDescription:[Ljava/lang/String;

    return-object v0
.end method

.method public getStartClip()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mStartClip:I

    return v0
.end method

.method public getTimeStemp()[J
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTimeStemp:[J

    return-object v0
.end method

.method public getTitle()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTitle:[Ljava/lang/String;

    return-object v0
.end method

.method public setHelpData([J[Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0
    .param p1, "timeStemp"    # [J
    .param p2, "description"    # [Ljava/lang/String;
    .param p3, "title"    # [Ljava/lang/String;
    .param p4, "startClip"    # I

    .prologue
    .line 16
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTimeStemp:[J

    .line 17
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mDescription:[Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mTitle:[Ljava/lang/String;

    .line 19
    iput p4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpData;->mStartClip:I

    .line 20
    return-void
.end method
