.class public Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
.super Ljava/lang/Object;
.source "TrackInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Tracks"
.end annotation


# static fields
.field private static final MAX_INDEX:I = 0x64


# instance fields
.field public Languages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public TrackIndex:[I

.field public mTrackType:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;)V
    .locals 3

    .prologue
    const/16 v2, 0x64

    const/4 v1, -0x1

    .line 217
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->this$0:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    .line 213
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    .line 215
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->mTrackType:I

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    .line 219
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    .line 220
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->mTrackType:I

    .line 221
    return-void
.end method


# virtual methods
.method public add(Ljava/lang/String;I)V
    .locals 4
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 224
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 225
    .local v0, "i":I
    const-string v1, "TrackInfoUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "add : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aput p2, v1, v0

    .line 229
    return-void
.end method

.method public add(Ljava/lang/String;II)V
    .locals 0
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "index"    # I
    .param p3, "trackType"    # I

    .prologue
    .line 233
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    .line 234
    iput p3, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->mTrackType:I

    .line 235
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 243
    const/16 v0, 0x64

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    .line 244
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->mTrackType:I

    .line 245
    return-void
.end method

.method public getTrackType()I
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->mTrackType:I

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
