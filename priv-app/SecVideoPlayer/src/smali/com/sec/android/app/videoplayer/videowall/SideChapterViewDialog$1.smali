.class Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;
.super Ljava/lang/Object;
.source "SideChapterViewDialog.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getIndex(I)I
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 260
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$200()[I

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 261
    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$200()[I

    move-result-object v1

    aget v1, v1, v0

    if-ne v1, p1, :cond_0

    .line 267
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 260
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    :cond_1
    const-string v1, "videowall-SideChapterViewDialog"

    const-string v2, "getIndex() INDEX NOT FOUND!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 233
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->getIndex(I)I

    move-result v1

    .line 234
    .local v1, "index":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 236
    .local v0, "action":I
    const-string v2, "videowall-SideChapterViewDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mbitview :: onTouch : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->ResetHideTimer()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    .line 239
    packed-switch v0, :pswitch_data_0

    .line 256
    :cond_0
    :goto_0
    :pswitch_0
    return v5

    .line 241
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    const v3, 0x7f020048

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 245
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 249
    :pswitch_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v6, v2

    if-gtz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v6, v2

    if-gtz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 250
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$1;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
