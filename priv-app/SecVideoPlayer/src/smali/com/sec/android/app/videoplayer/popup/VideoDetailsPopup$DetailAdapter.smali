.class Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;
.super Landroid/widget/BaseAdapter;
.source "VideoDetailsPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItem:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 515
    .local p3, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 516
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mItem:Ljava/util/ArrayList;

    .line 517
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mContext:Landroid/content/Context;

    .line 518
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 532
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 537
    if-nez p2, :cond_0

    .line 538
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 539
    .local v1, "vi":Landroid/view/LayoutInflater;
    const v2, 0x7f030022

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 542
    .end local v1    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0d0151

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 543
    .local v0, "menu":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup$DetailAdapter;->mItem:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 544
    const-string v2, "persist.sys.language"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "iw"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "persist.sys.language"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ur"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "persist.sys.language"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 547
    :cond_1
    const/16 v2, 0x15

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 551
    :goto_0
    return-object p2

    .line 549
    :cond_2
    const/16 v2, 0x13

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method
