.class Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initTouchAndKeyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V
    .locals 0

    .prologue
    .line 336
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "i"    # I
    .param p3, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isEnterDown(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$700(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->performPressWithCustomBitmap()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$200(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    .line 348
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isEnterDown(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$700(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->startDefaultPressedAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$300(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0

    .line 343
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isEnterUp(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$800(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->performReleaseWithCustomBitmap()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$500(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0

    .line 345
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isEnterUp(Landroid/view/KeyEvent;)Z
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$800(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$2;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->revertDefaultPressedAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$600(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0
.end method
