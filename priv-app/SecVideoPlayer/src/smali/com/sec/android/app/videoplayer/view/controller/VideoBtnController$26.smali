.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field IsFirstTouchOnProgressbar:Z

.field position:J

.field prev_position:J

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 2829
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2830
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    .line 2831
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->prev_position:J

    .line 2833
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->IsFirstTouchOnProgressbar:Z

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v8, 0x0

    .line 2836
    if-eqz p3, :cond_0

    .line 2837
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2903
    :cond_0
    :goto_0
    return-void

    .line 2842
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->reprocessProgress(I)I

    move-result p2

    .line 2843
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2846
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)J

    move-result-wide v2

    int-to-long v4, p2

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x186a0

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    .line 2848
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 2850
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->prev_position:J

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2853
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->prev_position:J

    .line 2855
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->IsFirstTouchOnProgressbar:Z

    if-eqz v2, :cond_4

    .line 2856
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->IsFirstTouchOnProgressbar:Z

    .line 2895
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    .line 2896
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v4, v4

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v3, v4, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2897
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v6, v6

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v5, v6, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;IZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2900
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2901
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setHelpClip4SeekMode(I)V

    goto/16 :goto_0

    .line 2858
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2859
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2860
    const-string v2, "VideoBtnController"

    const-string v3, "onProgressChanged - DASH - S K I P"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2887
    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2890
    const-string v2, "VideoBtnController"

    const-string v3, "updateVideoVisualSeek"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateVideoVisualSeek(I)V

    goto/16 :goto_1

    .line 2861
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2862
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v1

    .line 2863
    .local v1, "secProgress":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 2864
    .local v0, "currentProgress":I
    if-ge v0, v1, :cond_5

    .line 2865
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_2

    .line 2868
    .end local v0    # "currentProgress":I
    .end local v1    # "secProgress":I
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_2

    .line 2871
    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2874
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2876
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2877
    :cond_9
    const-string v2, "VideoBtnController"

    const-string v3, "onProgressChanged - streaming - S K I P"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2879
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_2

    .line 2882
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v4, 0x1

    .line 2959
    const-string v0, "VideoBtnController"

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2961
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2987
    :cond_0
    :goto_0
    return-void

    .line 2964
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->resetHoldLongSeek()V

    .line 2965
    invoke-virtual {p1}, Landroid/widget/SeekBar;->invalidate()V

    .line 2966
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z

    .line 2967
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v1

    int-to-long v2, v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3902(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;J)J

    .line 2968
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->IsFirstTouchOnProgressbar:Z

    .line 2976
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const v1, 0x36ee80

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 2977
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stopUpdateProgress()V

    .line 2979
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2983
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTVOutView()V

    .line 2984
    const-string v0, "VideoBtnController"

    const-string v1, "setVideoVisualSeek"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2985
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoVisualSeek(ILandroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const-wide/16 v8, 0x1

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 2906
    const-string v2, "VideoBtnController"

    const-string v3, "onStopTrackingTouch"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2907
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2908
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setProgress(I)V

    .line 2956
    :goto_0
    return-void

    .line 2912
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2916
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->DismissVideoVisualSeek()V

    .line 2917
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTVOutView()V

    .line 2920
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z

    .line 2922
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2923
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2924
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v1

    .line 2925
    .local v1, "secProgress":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 2926
    .local v0, "currentProgress":I
    if-ge v0, v1, :cond_2

    .line 2927
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 2953
    .end local v0    # "currentProgress":I
    .end local v1    # "secProgress":I
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setProgress()I

    .line 2954
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->startUpdateProgress()V

    .line 2955
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto/16 :goto_0

    .line 2930
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_1

    .line 2933
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2935
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_1

    .line 2937
    :cond_7
    const-string v2, "VideoBtnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onStopTrackingTouch local play. mScrubbingSpeed = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2939
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$4100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I

    move-result v2

    if-ne v2, v6, :cond_8

    .line 2940
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    add-long/2addr v4, v8

    long-to-int v3, v4

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_1

    .line 2942
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;->position:J

    add-long/2addr v4, v8

    long-to-int v3, v4

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_1
.end method
