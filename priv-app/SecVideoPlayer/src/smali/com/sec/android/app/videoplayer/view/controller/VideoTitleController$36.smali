.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2551
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2553
    const/4 v1, 0x0

    .line 2554
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2555
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2556
    const/4 v2, 0x1

    .line 2603
    :goto_0
    return v2

    .line 2558
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 2584
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 2585
    .local v0, "isReturn":Z
    if-eqz v0, :cond_3

    .end local v0    # "isReturn":Z
    :cond_1
    :goto_1
    move v2, v1

    .line 2603
    goto :goto_0

    .line 2561
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 2563
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_1

    .line 2567
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 2568
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2569
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1502(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 2570
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;)V

    .line 2575
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->showPopup()V

    goto :goto_1

    .line 2588
    .restart local v0    # "isReturn":Z
    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_1

    .line 2590
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2591
    goto :goto_1

    .line 2594
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2595
    goto :goto_1

    .line 2558
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 2561
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2588
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
