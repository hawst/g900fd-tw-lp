.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;
.super Landroid/widget/SimpleAdapter;
.source "SubtitleSplitAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mlist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field mtextView:Landroid/widget/TextView;

.field private selected_position:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .param p4, "key"    # [Ljava/lang/String;
    .param p5, "values"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p2, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct/range {p0 .. p5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    .line 33
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 35
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mlist:Ljava/util/ArrayList;

    .line 36
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mlist:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v7, 0x7f07004e

    const v6, 0x7f070009

    .line 55
    if-nez p2, :cond_0

    .line 56
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 58
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f03002f

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 60
    .end local v3    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f0d0196

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 61
    .local v2, "text":Landroid/widget/TextView;
    const v4, 0x7f0d0194

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 62
    .local v1, "starttime":Landroid/widget/TextView;
    const v4, 0x7f0d0195

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    .local v0, "endtime":Landroid/widget/TextView;
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->selected_position:I

    if-ne p1, v4, :cond_1

    .line 65
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 66
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 67
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 73
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    return-object v4

    .line 69
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 70
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 71
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    .line 51
    return-void
.end method

.method public setselectionPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->selected_position:I

    .line 40
    return-void
.end method
