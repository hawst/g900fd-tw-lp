.class Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;
.super Ljava/lang/Object;
.source "PreviewLayout.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->addView()Lcom/sec/android/app/videoplayer/preview/Preview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v4, 0x7f0201ef

    const/4 v3, 0x0

    const v2, 0x7f0d008f

    .line 78
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 101
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 80
    :pswitch_1
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 81
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 84
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    .line 88
    :cond_1
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->access$000(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;->getChapterIdx()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->indexOfChild(Landroid/view/View;)I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 90
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 94
    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 92
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0201ec

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 78
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
