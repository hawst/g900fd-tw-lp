.class Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;
.super Landroid/os/Handler;
.source "VideoExtScreenController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0

    .prologue
    .line 783
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 785
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 787
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->hideController()V

    goto :goto_0

    .line 791
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)I

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->updatePausePlayBtn()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 795
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 796
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 801
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 802
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0074

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a0104

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 785
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
