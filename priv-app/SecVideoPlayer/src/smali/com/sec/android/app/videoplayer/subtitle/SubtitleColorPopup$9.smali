.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->createPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p3, v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    const/4 v2, 0x2

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorPosition(I)I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultOpacity()I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultOpacity()I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateOpacity(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1502(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Z)Z

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorFromPosition(I)I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$202(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->notifyDataSetChanged()V

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->invalidate()V

    .line 297
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorFromPosition(I)I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$202(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method
