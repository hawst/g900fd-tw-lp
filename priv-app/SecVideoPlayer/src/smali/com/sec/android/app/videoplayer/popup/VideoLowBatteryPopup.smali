.class public Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
.super Ljava/lang/Object;
.source "VideoLowBatteryPopup.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private final transient mDismissListener:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "VideoLowBatteryPopup"

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mContext:Landroid/content/Context;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    .line 119
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 26
    sget-object v1, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->TAG:Ljava/lang/String;

    const-string v2, "VideoLowBatteryPopup E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mContext:Landroid/content/Context;

    .line 29
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 31
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a009d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 32
    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 34
    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 42
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 49
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 80
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 82
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public deletePopup()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    .line 106
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 99
    :cond_0
    return-void
.end method

.method public getDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 112
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 113
    const/4 v0, 0x1

    .line 116
    :cond_0
    return v0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 93
    :cond_0
    return-void
.end method
