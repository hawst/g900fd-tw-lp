.class Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;
.super Ljava/lang/Object;
.source "AsfErrorPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->mType:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$200(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)I

    move-result v0

    const/16 v1, 0x2db

    if-ne v0, v1, :cond_0

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleStateChanged()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->handleAsfError()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;)V

    goto :goto_0
.end method
