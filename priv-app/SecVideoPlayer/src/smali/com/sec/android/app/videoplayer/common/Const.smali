.class public Lcom/sec/android/app/videoplayer/common/Const;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/common/Const$SeekType;
    }
.end annotation


# static fields
.field public static final CENTER:I = 0x0

.field public static final CHARACTER_SPACE:Ljava/lang/String; = " "

.field public static final DNIe_ADAPTIVE_MODE:I = 0x4

.field public static final DNIe_PREMIUM_MODE:I = 0x0

.field public static final DNIe_STANDARD_MODE:I = 0x2

.field public static final DNIe_UI_MODE:I = 0x0

.field public static final DNIe_VIDEO_MODE:I = 0x1

.field public static final FALSE:I = 0x0

.field public static final FILL_SCREEN_RATIO:I = 0x2

.field public static final FIT_TO_HEIGHT_RATIO:I = 0x1

.field public static final GROUPPLAY_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.groupcast"

.field public static final HOVER_MODE_AUTO:I = 0x2

.field public static final HOVER_MODE_FINGER:I = 0x1

.field public static final HOVER_MODE_PEN:I = 0x0

.field public static final ICS_JB_FILL_SCREEN_RATIO:I = 0x2

.field public static final ICS_JB_KEEP_ASPECT_RATIO:I = 0x1

.field public static final ICS_JB_ORIGINAL_SIZE:I = 0x0

.field public static final IS_LANDSCAPE:Ljava/lang/String; = "isLandscape"

.field public static final JBP_CONTROLLER_MODE_MINI_LEFT:I = 0x1

.field public static final JBP_CONTROLLER_MODE_NORMAL:I = 0x0

.field public static final KEEP_ASPECT_RATIO:I = 0x0

.field public static final LEFT:I = 0x2

.field public static final LONG_PRESS_TIME:I = 0x1f4

.field public static final MSG_FOR_ASF:I = 0x2bc

.field public static final MSG_FOR_PREVIEW:I = 0x384

.field public static final MSG_FOR_PREVIEW_SET_LOCKSTATE:I = 0x385

.field public static final MSG_FOR_WFD:I = 0x320

.field public static final ORIGINAL_SIZE:I = 0x3

.field public static final PAUSE:I = 0x1

.field public static final PLAY:I = 0x0

.field public static final RESULT_DOWNLOAD_MODAL_REQUEST:I = 0x1

.field public static final RIGHT:I = 0x1

.field public static final SCREEN_AUTO_BRIGHTNESS_DETAIL:Ljava/lang/String; = "auto_brightness_detail"

.field public static final SCREEN_MODE_DEFAULT:I = 0x0

.field public static final SCREEN_MODE_END:I = 0x3

.field public static final SCREEN_MODE_START:I = 0x0

.field public static final SERVICE_MEDIA:Ljava/lang/String; = "com.samsung.android.allshare.media"

.field public static final SERVICE_MINI_PLAYER:Ljava/lang/String; = "com.sec.android.app.videoplayer.miniapp.MiniVideoPlayerService"

.field public static final SERVICE_PRESENTATION:Ljava/lang/String; = "com.sec.android.app.videoplayer.service.Presentationservice"

.field public static final SETTINGS_SCREEN_ADAPTIVE_MODE:Ljava/lang/String; = "screen_mode_automatic_setting"

.field public static final SETTINGS_SCREEN__MODE:Ljava/lang/String; = "screen_mode_setting"

.field public static final SHAREVIDEO_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.mv.player"

.field public static final STOP:I = 0x2

.field public static final SYSTEM_UI_FLAG_REMOVE_NAVIGATION:Ljava/lang/String; = "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

.field public static final THEME_CHOOSER:Ljava/lang/String; = "theme"

.field public static final TRUE:I = 0x1

.field public static final Tcon_UI_MODE:I = 0x0

.field public static final Tcon_VIDEO_MODE:I = 0x1

.field public static final UNDEFINED:I = -0x1

.field public static final VIDEOLIST_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.everglades.video"

.field public static final VIDEOLIST_WIDGET_CLASS_NAME:Ljava/lang/String; = "com.samsung.everglades.video.myvideo.list.FavoriteIconWidget"

.field public static final VIDEO_PLAYBACK_PROGRESS_BAR_STYLE:I = 0x101007a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    return-void
.end method
