.class public Lcom/sec/android/app/videoplayer/common/wrapper/VideoLogCapture;
.super Ljava/lang/Object;
.source "VideoLogCapture.java"


# static fields
.field private static final LOG_FILE_PATH:Ljava/lang/String; = "/sdcard/Movies/"

.field private static final TAG:Ljava/lang/String; = "VideoLogCapture"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "VideoLogCapture"

    const-string v1, "VideoLogCapture Please Add below permission in your application before using LogCapture"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 15
    const-string v0, "VideoLogCapture"

    const-string v1, "android.permission.DUMP, android.permission.READ_LOGS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    return-void
.end method


# virtual methods
.method public makeLogFile()V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    const-string v11, "VideoLogCapture"

    const-string v12, "makeLogFile start"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 21
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 22
    .local v1, "calendar":Ljava/util/Calendar;
    const-string v11, "%d-%d-%d-%d-%d-%d"

    const/4 v12, 0x6

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const/4 v14, 0x2

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    const/4 v14, 0x5

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x3

    const/16 v14, 0xb

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x4

    const/16 v14, 0xc

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x5

    const/16 v14, 0xd

    invoke-virtual {v1, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 27
    .local v9, "strTime":Ljava/lang/String;
    const-string v11, "VideoLogCapture"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "strTime "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    const/4 v11, 0x2

    new-array v2, v11, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "dumpsys > /sdcard/Movies/dumpsys.txt"

    aput-object v12, v2, v11

    const/4 v11, 0x1

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "logcat -d > /sdcard/Movies/log"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ".txt"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v2, v11

    .line 33
    .local v2, "commands":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 36
    .local v8, "p":Ljava/lang/Process;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v11

    const-string v12, "/system/bin/sh -"

    invoke-virtual {v11, v12}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 42
    const/4 v6, 0x0

    .line 44
    .local v6, "os":Ljava/io/DataOutputStream;
    :try_start_1
    new-instance v7, Ljava/io/DataOutputStream;

    invoke-virtual {v8}, Ljava/lang/Process;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    invoke-direct {v7, v11}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    .end local v6    # "os":Ljava/io/DataOutputStream;
    .local v7, "os":Ljava/io/DataOutputStream;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    :try_start_2
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v10, v0, v4

    .line 46
    .local v10, "tmpCmd":Ljava/lang/String;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 45
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 37
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "os":Ljava/io/DataOutputStream;
    .end local v10    # "tmpCmd":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 38
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    .line 55
    .end local v3    # "e":Ljava/io/IOException;
    :cond_0
    :goto_1
    return-void

    .line 51
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v7    # "os":Ljava/io/DataOutputStream;
    :cond_1
    if-eqz v7, :cond_3

    .line 52
    invoke-virtual {v7}, Ljava/io/DataOutputStream;->close()V

    move-object v6, v7

    .end local v7    # "os":Ljava/io/DataOutputStream;
    .restart local v6    # "os":Ljava/io/DataOutputStream;
    goto :goto_1

    .line 48
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_1
    move-exception v3

    .line 49
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 51
    if-eqz v6, :cond_0

    .line 52
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V

    goto :goto_1

    .line 51
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    :goto_3
    if-eqz v6, :cond_2

    .line 52
    invoke-virtual {v6}, Ljava/io/DataOutputStream;->close()V

    :cond_2
    throw v11

    .line 51
    .end local v6    # "os":Ljava/io/DataOutputStream;
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v7    # "os":Ljava/io/DataOutputStream;
    :catchall_1
    move-exception v11

    move-object v6, v7

    .end local v7    # "os":Ljava/io/DataOutputStream;
    .restart local v6    # "os":Ljava/io/DataOutputStream;
    goto :goto_3

    .line 48
    .end local v6    # "os":Ljava/io/DataOutputStream;
    .restart local v7    # "os":Ljava/io/DataOutputStream;
    :catch_2
    move-exception v3

    move-object v6, v7

    .end local v7    # "os":Ljava/io/DataOutputStream;
    .restart local v6    # "os":Ljava/io/DataOutputStream;
    goto :goto_2

    .end local v6    # "os":Ljava/io/DataOutputStream;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    .restart local v7    # "os":Ljava/io/DataOutputStream;
    :cond_3
    move-object v6, v7

    .end local v7    # "os":Ljava/io/DataOutputStream;
    .restart local v6    # "os":Ljava/io/DataOutputStream;
    goto :goto_1
.end method
