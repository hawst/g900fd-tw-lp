.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 1753
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/16 v7, 0xbb8

    const/4 v6, 0x0

    .line 1755
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1756
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 1793
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 1758
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 1762
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 1763
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 1764
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 1765
    invoke-virtual {p1, v6}, Landroid/view/View;->playSoundEffect(I)V

    .line 1766
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    .line 1767
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1768
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne v2, v3, :cond_1

    .line 1769
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupAllshareVolbar(Z)V

    .line 1770
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_0

    .line 1772
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1773
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    goto :goto_0

    .line 1776
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupVolbar(Z)V

    .line 1777
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 1778
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    goto/16 :goto_0

    .line 1786
    .end local v0    # "pressTime":J
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto/16 :goto_0

    .line 1756
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
