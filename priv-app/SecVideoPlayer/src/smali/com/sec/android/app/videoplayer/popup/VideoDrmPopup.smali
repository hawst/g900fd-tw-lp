.class public Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;
.super Ljava/lang/Object;
.source "VideoDrmPopup.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private btnEnable:Z

.field private leftBtn:I

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private final transient mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mDrmPopupType:I

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private popupStr:Ljava/lang/String;

.field private rightBtn:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->leftBtn:I

    .line 25
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->rightBtn:I

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->btnEnable:Z

    .line 29
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->popupStr:Ljava/lang/String;

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;

    .line 39
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 41
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 43
    const-string v0, "VideoDrmPopup"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;

    .line 227
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 46
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 47
    iput p2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->createDialog()V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    return-object v0
.end method


# virtual methods
.method public createDialog()V
    .locals 6

    .prologue
    const v5, 0x104000a

    const/16 v4, 0x23

    const/4 v3, 0x0

    .line 53
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x1f

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x22

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x21

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x27

    if-ne v1, v2, :cond_4

    .line 57
    :cond_0
    const v1, 0x1040013

    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->leftBtn:I

    .line 58
    const v1, 0x1040009

    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->rightBtn:I

    .line 59
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->btnEnable:Z

    .line 71
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    iget v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getPopupString(IZ)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->popupStr:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createDialog() - pop up String :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->popupStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 76
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    if-ne v1, v4, :cond_2

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a01af

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 79
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->popupStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 80
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->leftBtn:I

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->btnEnable:Z

    if-eqz v1, :cond_3

    .line 132
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->rightBtn:I

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    :cond_3
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 177
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 184
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    .line 185
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 186
    return-void

    .line 60
    .end local v0    # "popup":Landroid/app/AlertDialog$Builder;
    :cond_4
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x20

    if-eq v1, v2, :cond_5

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    if-ne v1, v4, :cond_6

    .line 62
    :cond_5
    iput v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->leftBtn:I

    .line 63
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->btnEnable:Z

    goto/16 :goto_0

    .line 64
    :cond_6
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x24

    if-eq v1, v2, :cond_7

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x25

    if-eq v1, v2, :cond_7

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    const/16 v2, 0x26

    if-ne v1, v2, :cond_1

    .line 67
    :cond_7
    iput v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->leftBtn:I

    .line 68
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->btnEnable:Z

    goto/16 :goto_0
.end method

.method public deletePopup()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    .line 214
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 207
    :cond_0
    return-void
.end method

.method public getDialog()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 221
    const/4 v0, 0x1

    .line 224
    :cond_0
    return v0
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 194
    iput p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I

    .line 195
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 201
    :cond_0
    return-void
.end method
