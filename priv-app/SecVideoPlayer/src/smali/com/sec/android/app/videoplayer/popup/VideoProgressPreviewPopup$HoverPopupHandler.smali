.class Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;
.super Landroid/os/Handler;
.source "VideoProgressPreviewPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HoverPopupHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 123
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 179
    :goto_0
    return-void

    .line 125
    :pswitch_0
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_SHOW_POPUP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getVideoViewSize()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "msg_show_popup drm file return"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->initViews()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 140
    :pswitch_1
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_SHOW"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->show()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    goto :goto_0

    .line 145
    :pswitch_2
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_SHOW_PLAY"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->start()V

    goto :goto_0

    .line 150
    :pswitch_3
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_SHOW_PAUSE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->pause()V

    goto :goto_0

    .line 155
    :pswitch_4
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_SHOW_SEEK"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->seek()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->sendDelayedSeekMessage(I)V

    goto :goto_0

    .line 161
    :pswitch_5
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_DISMISS_POPUP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->dismiss()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    goto/16 :goto_0

    .line 166
    :pswitch_6
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_ERROR_POPUP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 170
    :pswitch_7
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "MSG_MOVE_POPUP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->progressSeekto()V

    goto/16 :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
