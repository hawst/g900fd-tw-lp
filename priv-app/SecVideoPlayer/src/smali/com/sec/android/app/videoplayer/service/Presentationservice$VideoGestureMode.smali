.class final enum Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;
.super Ljava/lang/Enum;
.source "Presentationservice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/Presentationservice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VideoGestureMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

.field public static final enum MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

.field public static final enum VOLUME_MODE:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    const-string v1, "MODE_UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    new-instance v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    const-string v1, "VOLUME_MODE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    .line 65
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->$VALUES:[Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 65
    const-class v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->$VALUES:[Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    return-object v0
.end method
