.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;
.super Landroid/widget/SimpleAdapter;
.source "SubtitleSettingPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleMenuListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Ljava/util/ArrayList;[Ljava/lang/String;[I)V
    .locals 6
    .param p3, "key"    # [Ljava/lang/String;
    .param p4, "values"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .prologue
    .line 820
    .local p2, "al":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 821
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f030035

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 822
    return-void
.end method

.method private getValue(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    .locals 10
    .param p1, "t"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "i":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const v9, 0x7f0a0120

    const v5, 0x7f0a00e6

    const/4 v4, 0x1

    const/4 v8, 0x0

    .line 830
    const-string v0, ""

    .line 832
    .local v0, "value":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a0094

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 833
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v1, :cond_1

    .line 834
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 835
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getMultiSelectedSubtitleLanguage()Ljava/lang/String;

    move-result-object v0

    .line 873
    :goto_0
    return-object v0

    .line 837
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 840
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 842
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a0141

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 843
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v1, :cond_3

    .line 844
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 846
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%.1f"

    new-array v3, v4, [Ljava/lang/Object;

    const-wide v4, 0x3fb999999999999aL    # 0.1

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 849
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a000c

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 850
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextAlignmentValue()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 851
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a0078

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 852
    const-string v1, "MENU_LABLE"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v3, 0x7f0a00b7

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 853
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSelectedFont()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 855
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getColorValue(I)Ljava/lang/String;

    move-result-object v0

    .line 856
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontOpacity()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 858
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a007a

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 859
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextSizeValue()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 860
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a0079

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 861
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextEdgeValue()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 863
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a0029

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 864
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getColorValue(I)Ljava/lang/String;

    move-result-object v0

    .line 865
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinOpacity()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 866
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v2, 0x7f0a013a

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 867
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getColorValue(I)Ljava/lang/String;

    move-result-object v0

    .line 868
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGOpacity()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 870
    :cond_b
    const-string v1, "MENU_VALUE"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "value":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .restart local v0    # "value":Ljava/lang/String;
    goto/16 :goto_0
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 877
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 879
    .local v1, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/content/Context;

    move-result-object v8

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 880
    .local v7, "vi":Landroid/view/LayoutInflater;
    const v8, 0x7f030035

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 882
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->getCount()I

    move-result v8

    if-nez v8, :cond_1

    .line 938
    :cond_0
    :goto_0
    return-object p2

    .line 886
    :cond_1
    const-string v8, "MENU_TITLE"

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 887
    .local v5, "title":Ljava/lang/String;
    invoke-direct {p0, v5, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->getValue(Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v6

    .line 889
    .local v6, "value":Ljava/lang/String;
    const v8, 0x7f0d01b9

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 890
    .local v3, "menuName":Landroid/widget/TextView;
    const v8, 0x7f0d01ba

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 892
    .local v4, "menuValue":Landroid/widget/TextView;
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 893
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 895
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 896
    invoke-virtual {v3, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 897
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 903
    :goto_1
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v8, :cond_0

    .line 904
    const v8, 0x7f0d01b8

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 905
    .local v2, "layout":Landroid/widget/LinearLayout;
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 906
    .local v0, "LP":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v9, 0x7f0802b4

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getDimen(I)I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)I

    move-result v8

    iput v8, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 907
    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 909
    invoke-virtual {v3}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "LP":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 910
    .restart local v0    # "LP":Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 911
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 913
    invoke-virtual {v4}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .end local v0    # "LP":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 914
    .restart local v0    # "LP":Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 915
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 899
    .end local v0    # "LP":Landroid/widget/LinearLayout$LayoutParams;
    .end local v2    # "layout":Landroid/widget/LinearLayout;
    :cond_2
    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 900
    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 826
    const/4 v0, 0x2

    return v0
.end method
