.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 3104
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v11, 0x16

    const/16 v10, 0x15

    const/4 v5, 0x1

    .line 3106
    const/4 v4, 0x0

    .line 3107
    .local v4, "seekKey":I
    const/4 v1, 0x0

    .line 3108
    .local v1, "retVal":Z
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v6

    if-nez v6, :cond_1

    .line 3109
    const/4 v5, 0x0

    .line 3169
    :cond_0
    :goto_0
    :sswitch_0
    return v5

    .line 3111
    :cond_1
    if-ne p2, v10, :cond_3

    .line 3112
    const/4 v4, 0x7

    .line 3117
    :cond_2
    :goto_1
    sparse-switch p2, :sswitch_data_0

    .line 3151
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 3152
    .local v0, "isReturn":Z
    if-eqz v0, :cond_6

    .end local v0    # "isReturn":Z
    :goto_2
    move v5, v1

    .line 3169
    goto :goto_0

    .line 3113
    :cond_3
    if-ne p2, v11, :cond_2

    .line 3114
    const/4 v4, 0x6

    goto :goto_1

    .line 3120
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_2

    .line 3122
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v7

    int-to-long v8, v7

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J
    invoke-static {v6, v8, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$3902(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;J)J

    .line 3123
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 3128
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    const/16 v7, 0xa

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 3129
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 3130
    .local v2, "pressTime":J
    const-wide/16 v6, 0x1f4

    cmp-long v6, v2, v6

    if-gez v6, :cond_0

    .line 3131
    if-ne p2, v10, :cond_5

    .line 3132
    const/16 v4, 0xd

    .line 3136
    :cond_4
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 3133
    :cond_5
    if-ne p2, v11, :cond_4

    .line 3134
    const/16 v4, 0xc

    goto :goto_3

    .line 3155
    .end local v2    # "pressTime":J
    .restart local v0    # "isReturn":Z
    :cond_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto :goto_2

    .line 3157
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 3158
    goto :goto_2

    .line 3161
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v5

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 3162
    goto :goto_2

    .line 3117
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_1
        0x16 -> :sswitch_1
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 3120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 3155
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
