.class public Lcom/sec/android/app/videoplayer/Video360/Quaternion;
.super Ljava/lang/Object;
.source "Quaternion.java"


# instance fields
.field public mW:F

.field public mX:F

.field public mY:F

.field public mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 80
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    .line 81
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 85
    iput p2, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 86
    iput p3, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 87
    iput p4, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    .line 88
    return-void
.end method

.method public static mat3ToQuat([F)Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    .locals 10
    .param p0, "m"    # [F

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v8, 0x3e800000    # 0.25f

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x0

    .line 11
    aget v3, p0, v5

    add-float/2addr v3, v9

    aget v4, p0, v6

    add-float/2addr v3, v4

    aget v4, p0, v7

    add-float v0, v3, v4

    .line 12
    .local v0, "onePlusTrace":F
    new-instance v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    invoke-direct {v1}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;-><init>()V

    .line 14
    .local v1, "q":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    const v3, 0x3727c5ac    # 1.0E-5f

    cmpl-float v3, v0, v3

    if-lez v3, :cond_0

    .line 16
    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v2, v3, v4

    .line 17
    .local v2, "s":F
    const/4 v3, 0x7

    aget v3, p0, v3

    const/4 v4, 0x5

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 18
    const/4 v3, 0x2

    aget v3, p0, v3

    const/4 v4, 0x6

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 19
    const/4 v3, 0x3

    aget v3, p0, v3

    const/4 v4, 0x1

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 20
    mul-float v3, v8, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    .line 44
    :goto_0
    return-object v1

    .line 23
    .end local v2    # "s":F
    :cond_0
    aget v3, p0, v5

    aget v4, p0, v6

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    aget v3, p0, v5

    aget v4, p0, v7

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    .line 24
    aget v3, p0, v5

    add-float/2addr v3, v9

    aget v4, p0, v6

    sub-float/2addr v3, v4

    aget v4, p0, v7

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v2, v3, v4

    .line 25
    .restart local v2    # "s":F
    mul-float v3, v8, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 26
    const/4 v3, 0x1

    aget v3, p0, v3

    const/4 v4, 0x3

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 27
    const/4 v3, 0x2

    aget v3, p0, v3

    const/4 v4, 0x6

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 28
    const/4 v3, 0x5

    aget v3, p0, v3

    const/4 v4, 0x7

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    goto :goto_0

    .line 29
    .end local v2    # "s":F
    :cond_1
    aget v3, p0, v6

    aget v4, p0, v7

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    .line 30
    aget v3, p0, v6

    add-float/2addr v3, v9

    aget v4, p0, v5

    sub-float/2addr v3, v4

    aget v4, p0, v7

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v2, v3, v4

    .line 31
    .restart local v2    # "s":F
    const/4 v3, 0x1

    aget v3, p0, v3

    const/4 v4, 0x3

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 32
    mul-float v3, v8, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 33
    const/4 v3, 0x5

    aget v3, p0, v3

    const/4 v4, 0x7

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 34
    const/4 v3, 0x2

    aget v3, p0, v3

    const/4 v4, 0x6

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    goto/16 :goto_0

    .line 36
    .end local v2    # "s":F
    :cond_2
    aget v3, p0, v7

    add-float/2addr v3, v9

    aget v4, p0, v5

    sub-float/2addr v3, v4

    aget v4, p0, v6

    sub-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v2, v3, v4

    .line 37
    .restart local v2    # "s":F
    const/4 v3, 0x2

    aget v3, p0, v3

    const/4 v4, 0x6

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 38
    const/4 v3, 0x5

    aget v3, p0, v3

    const/4 v4, 0x7

    aget v4, p0, v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 39
    mul-float v3, v8, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 40
    const/4 v3, 0x1

    aget v3, p0, v3

    const/4 v4, 0x3

    aget v4, p0, v4

    sub-float/2addr v3, v4

    div-float/2addr v3, v2

    iput v3, v1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    goto/16 :goto_0
.end method

.method public static slerp(Lcom/sec/android/app/videoplayer/Video360/Quaternion;Lcom/sec/android/app/videoplayer/Video360/Quaternion;F)Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    .locals 12
    .param p0, "a"    # Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    .param p1, "b"    # Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    .param p2, "t"    # F

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    .line 49
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->dotProduct(Lcom/sec/android/app/videoplayer/Video360/Quaternion;)F

    move-result v4

    .line 50
    .local v4, "cosA":F
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 57
    .local v0, "absCosA":F
    sub-float v7, v10, v0

    const v8, 0x3c23d70a    # 0.01f

    cmpg-float v7, v7, v8

    if-gez v7, :cond_1

    .line 58
    sub-float v2, v10, p2

    .line 59
    .local v2, "c1":F
    move v3, p2

    .line 68
    .local v3, "c2":F
    :goto_0
    const/4 v7, 0x0

    cmpg-float v7, v4, v7

    if-gez v7, :cond_0

    .line 69
    neg-float v2, v2

    .line 71
    :cond_0
    new-instance v5, Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    iget v7, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    mul-float/2addr v7, v2

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    mul-float/2addr v8, v3

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    mul-float/2addr v8, v2

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    mul-float/2addr v9, v3

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    mul-float/2addr v9, v2

    iget v10, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    mul-float/2addr v10, v3

    add-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    mul-float/2addr v10, v2

    iget v11, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    mul-float/2addr v11, v3

    add-float/2addr v10, v11

    invoke-direct {v5, v7, v8, v9, v10}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;-><init>(FFFF)V

    .line 74
    .local v5, "q":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    return-object v5

    .line 61
    .end local v2    # "c1":F
    .end local v3    # "c2":F
    .end local v5    # "q":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    :cond_1
    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->acos(D)D

    move-result-wide v8

    double-to-float v1, v8

    .line 62
    .local v1, "angle":F
    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v6, v8

    .line 63
    .local v6, "sinA":F
    sub-float v7, v10, p2

    mul-float/2addr v7, v1

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v7, v8

    div-float v2, v7, v6

    .line 64
    .restart local v2    # "c1":F
    mul-float v7, v1, p2

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v7, v8

    div-float v3, v7, v6

    .restart local v3    # "c2":F
    goto :goto_0
.end method


# virtual methods
.method public asMatrix3x3()[F
    .locals 22

    .prologue
    .line 109
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v1, v20, v21

    .line 110
    .local v1, "c1":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v2, v20, v21

    .line 111
    .local v2, "c2":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v3, v20, v21

    .line 112
    .local v3, "c3":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v4, v20, v21

    .line 113
    .local v4, "c4":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v5, v20, v21

    .line 114
    .local v5, "c5":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v6, v20, v21

    .line 115
    .local v6, "c6":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 116
    .local v7, "c7":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v8, v20, v21

    .line 117
    .local v8, "c8":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 119
    .local v9, "c9":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v2

    sub-float v11, v20, v3

    .line 120
    .local v11, "m00":F
    sub-float v12, v4, v9

    .line 121
    .local v12, "m01":F
    add-float v13, v5, v8

    .line 123
    .local v13, "m02":F
    add-float v14, v4, v9

    .line 124
    .local v14, "m10":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v3

    sub-float v15, v20, v1

    .line 125
    .local v15, "m11":F
    sub-float v16, v7, v6

    .line 127
    .local v16, "m12":F
    sub-float v17, v5, v8

    .line 128
    .local v17, "m20":F
    add-float v18, v7, v6

    .line 129
    .local v18, "m21":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v2

    sub-float v19, v20, v1

    .line 131
    .local v19, "m22":F
    const/16 v20, 0x9

    move/from16 v0, v20

    new-array v10, v0, [F

    .line 133
    .local v10, "m":[F
    const/16 v20, 0x0

    aput v11, v10, v20

    const/16 v20, 0x1

    aput v12, v10, v20

    const/16 v20, 0x2

    aput v13, v10, v20

    .line 134
    const/16 v20, 0x3

    aput v14, v10, v20

    const/16 v20, 0x4

    aput v15, v10, v20

    const/16 v20, 0x5

    aput v16, v10, v20

    .line 135
    const/16 v20, 0x6

    aput v17, v10, v20

    const/16 v20, 0x7

    aput v18, v10, v20

    const/16 v20, 0x8

    aput v19, v10, v20

    .line 137
    return-object v10
.end method

.method public dotProduct(Lcom/sec/android/app/videoplayer/Video360/Quaternion;)F
    .locals 3
    .param p1, "q"    # Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public normalize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 93
    .local v0, "magnitude":F
    cmpl-float v1, v0, v4

    if-lez v1, :cond_0

    .line 94
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 95
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    .line 96
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    .line 97
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    .line 102
    :goto_0
    return-void

    .line 99
    :cond_0
    iput v4, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    iput v4, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    iput v4, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    .line 100
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    goto :goto_0
.end method
