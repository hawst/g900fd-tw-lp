.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.super Landroid/app/Dialog;
.source "SubtitleSettingPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;
    }
.end annotation


# static fields
.field private static final MENU_LABLE:Ljava/lang/String; = "MENU_LABLE"

.field private static final MENU_TITLE:Ljava/lang/String; = "MENU_TITLE"

.field public static final MENU_TYPE_TEXT_EDGE:I = 0xc

.field public static final MENU_TYPE_TEXT_FONT:I = 0xa

.field public static final MENU_TYPE_TEXT_SIZE:I = 0xb

.field private static final MENU_VALUE:Ljava/lang/String; = "MENU_VALUE"

.field private static final TAG:Ljava/lang/String; = "VideoSubtitlePopup"

.field private static final TEXT_CENTER:I = 0x1

.field public static final TEXT_LARGE:I = 0x2

.field private static final TEXT_LEFT:I = 0x0

.field public static final TEXT_MEDEUM:I = 0x1

.field private static final TEXT_RIGHT:I = 0x2

.field public static final TEXT_SMALL:I = 0x0

.field public static final VIDEO_SUBTITLE_MENU_CAPTION_WINDOW_COLOUR:I = 0x7

.field public static final VIDEO_SUBTITLE_MENU_FONT_BACKGROUND_COLOUR:I = 0x8

.field public static final VIDEO_SUBTITLE_MENU_FONT_COLOUR:I = 0x5


# instance fields
.field colorOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field public isFromVideo:Z

.field private isSubtitleSettingPopupActive:Z

.field private mActivation:Z

.field private mBackLayout:Landroid/widget/RelativeLayout;

.field private mButton:Landroid/widget/Switch;

.field private mCbSMPTEActivationListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mColorList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

.field private mContext:Landroid/content/Context;

.field private mDivider:Landroid/widget/RelativeLayout;

.field private mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOnDialogDisappearListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

.field private mOverviewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

.field private mSMPTEActivationListener:Landroid/view/View$OnClickListener;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mShowingSubtitleSubPopup:Z

.field private mSubDialogs:Landroid/app/AlertDialog;

.field private mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mSubtitleActive:Z

.field private mSubtitleBGColor:I

.field private mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

.field private mSubtitleEdge:I

.field private mSubtitleEdgeDialog:Landroid/app/AlertDialog;

.field private mSubtitleFilePath:Ljava/lang/String;

.field private mSubtitleFont:Ljava/lang/String;

.field private mSubtitleFontColor:I

.field private mSubtitleFontDialog:Landroid/app/AlertDialog;

.field private mSubtitleFontSize:I

.field private mSubtitleLangArray:[Z

.field private mSubtitleLangDialog:Landroid/app/AlertDialog;

.field private mSubtitleMenuDialog:Landroid/app/Dialog;

.field public mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

.field private mSubtitleSizeDialog:Landroid/app/AlertDialog;

.field private mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

.field private mSubtitleSyncTime:I

.field public mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field private mTextList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

.field private overviewOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private textOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private titleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 208
    const v0, 0x7f0b0005

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleActive:Z

    .line 132
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncTime:I

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFont:Ljava/lang/String;

    .line 140
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontColor:I

    .line 142
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontSize:I

    .line 144
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleBGColor:I

    .line 146
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdge:I

    .line 148
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mShowingSubtitleSubPopup:Z

    .line 150
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    .line 152
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    .line 154
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    .line 156
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    .line 158
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 160
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMenuDialog:Landroid/app/Dialog;

    .line 162
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 164
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 166
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 168
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 170
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mButton:Landroid/widget/Switch;

    .line 172
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mDivider:Landroid/widget/RelativeLayout;

    .line 174
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 176
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    .line 182
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 184
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    .line 188
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    .line 602
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mCbSMPTEActivationListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 623
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSMPTEActivationListener:Landroid/view/View$OnClickListener;

    .line 657
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$5;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->overviewOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 686
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$6;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->textOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 716
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->colorOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 798
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOnDialogDisappearListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

    .line 1534
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$25;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 1575
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$26;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 209
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    .line 210
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initUtils()V

    .line 212
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initViews()V

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleValues()V

    .line 215
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 216
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 192
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .line 130
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleActive:Z

    .line 132
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncTime:I

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFont:Ljava/lang/String;

    .line 140
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontColor:I

    .line 142
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontSize:I

    .line 144
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleBGColor:I

    .line 146
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdge:I

    .line 148
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mShowingSubtitleSubPopup:Z

    .line 150
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    .line 152
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    .line 154
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    .line 156
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    .line 158
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 160
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMenuDialog:Landroid/app/Dialog;

    .line 162
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 164
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 166
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 168
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 170
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mButton:Landroid/widget/Switch;

    .line 172
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mDivider:Landroid/widget/RelativeLayout;

    .line 174
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 176
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    .line 182
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 184
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    .line 186
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    .line 188
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    .line 602
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mCbSMPTEActivationListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 623
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSMPTEActivationListener:Landroid/view/View$OnClickListener;

    .line 657
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$5;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->overviewOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 686
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$6;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->textOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 716
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->colorOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 798
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOnDialogDisappearListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

    .line 1534
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$25;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 1575
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$26;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 193
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    .line 194
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    .line 195
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->requestWindowFeature(I)Z

    .line 196
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 199
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initUtils()V

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initViews()V

    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleValues()V

    .line 203
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 204
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setCustomLabelEnable(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->clickColorAndOpacityPopup(I)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->exitDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getDimen(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->makeFontList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Lcom/sec/android/app/videoplayer/view/VideoStateView;)Lcom/sec/android/app/videoplayer/view/VideoStateView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOnDialogDisappearListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->callSubtitleFontPopup()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/widget/TextView;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setFontEdge(Landroid/widget/TextView;IZ)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .param p1, "x1"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->clickSync()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleLanguagePopup()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleAlignmentPopup()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleFontPopup()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleFontEdgePopup()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleSizePopup()V

    return-void
.end method

.method private callSubtitleFontPopup()V
    .locals 12

    .prologue
    .line 1290
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSelectedFontIndex()I

    move-result v9

    .line 1291
    .local v9, "selected":I
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callSubtitleFontPopup() :: selected = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontList()[Ljava/lang/String;

    move-result-object v6

    .line 1295
    .local v6, "fonts":[Ljava/lang/String;
    if-eqz v6, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    if-nez v1, :cond_1

    .line 1296
    :cond_0
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "callSubtitleFontPopup() :: return reason isSubtitleSettingPopupActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1355
    :goto_0
    return-void

    .line 1301
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1302
    .local v4, "fontArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1303
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f030037

    const/16 v5, 0xa

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 1305
    .local v0, "subtitleTextSettingAdapter":Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;
    new-instance v8, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v8, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1306
    .local v8, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0078

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v8, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1308
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 1309
    .local v7, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030038

    const/4 v2, 0x0

    invoke-virtual {v7, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1310
    .local v11, "textMenuListView":Landroid/view/View;
    const v1, 0x7f0d01bd

    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ListView;

    .line 1311
    .local v10, "textFontListView":Landroid/widget/ListView;
    const/high16 v1, 0x2000000

    invoke-virtual {v10, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 1313
    invoke-virtual {v10, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1314
    const/4 v1, 0x1

    invoke-virtual {v10, v9, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1316
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$19;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v10, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1332
    invoke-virtual {v8, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1334
    const v1, 0x7f0a0026

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$20;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v8, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1340
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    .line 1341
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1342
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1343
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1344
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$21;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$21;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1354
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontDialog:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method private clickColorAndOpacityPopup(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 988
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleColorAndOpacityPopup(I)V

    .line 989
    return-void
.end method

.method private clickSync()V
    .locals 2

    .prologue
    .line 965
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "clickSync() - isShowingSubtitleSubPopup() is true"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    :goto_0
    return-void

    .line 970
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 971
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleSyncPopup()V

    goto :goto_0
.end method

.method private createSubtitleAlignmentPopup()V
    .locals 6

    .prologue
    .line 1101
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1102
    const-string v4, "VideoSubtitlePopup"

    const-string v5, "createSubtitleAlignmentPopup() - isShowingSubtitleSubPopup() is true"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1168
    :goto_0
    return-void

    .line 1106
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1108
    const/4 v1, 0x0

    .line 1109
    .local v1, "selected":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextAlignment()I

    move-result v3

    .line 1111
    .local v3, "textAlignment":I
    const/16 v4, 0x1e

    if-ne v3, v4, :cond_1

    .line 1112
    const/4 v1, 0x0

    .line 1118
    :goto_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1119
    .local v0, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0a000c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1120
    const v4, 0x7f0a0026

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$13;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$13;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1126
    const v4, 0x7f060013

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v0, v4, v1, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1152
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 1153
    .local v2, "subtitleAlignmentDialog":Landroid/app/AlertDialog;
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1154
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 1156
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1158
    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$15;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$15;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1167
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto :goto_0

    .line 1113
    .end local v0    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v2    # "subtitleAlignmentDialog":Landroid/app/AlertDialog;
    :cond_1
    const/16 v4, 0x20

    if-ne v3, v4, :cond_2

    .line 1114
    const/4 v1, 0x2

    goto :goto_1

    .line 1116
    :cond_2
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private createSubtitleColorAndOpacityPopup(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    if-nez v0, :cond_0

    .line 993
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;-><init>(Landroid/content/Context;Landroid/app/Dialog;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->isShow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 996
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setType(I)V

    .line 997
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->show()V

    .line 999
    :cond_1
    return-void
.end method

.method private createSubtitleFontEdgePopup()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 1171
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1172
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleFontEdgePopup() - isShowingSubtitleSubPopup() is true"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    :goto_0
    return-void

    .line 1176
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    if-nez v1, :cond_1

    .line 1177
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createSubtitleFontEdgePopup() :: return reason isSubtitleSettingPopupActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1178
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    goto :goto_0

    .line 1182
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 1184
    .local v9, "textEdgeList":[Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1185
    .local v4, "textEdgeArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1187
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f030037

    const/16 v5, 0xc

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 1189
    .local v0, "subtitleTextSettingAdapter":Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;
    invoke-virtual {p0, v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontEdge()I

    move-result v8

    .line 1193
    .local v8, "selected":I
    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v7, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1194
    .local v7, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0079

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1196
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1197
    .local v6, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030038

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1198
    .local v11, "textMenuListView":Landroid/view/View;
    const v1, 0x7f0d01bd

    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ListView;

    .line 1199
    .local v10, "textEdgeListView":Landroid/widget/ListView;
    const/high16 v1, 0x2000000

    invoke-virtual {v10, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 1201
    invoke-virtual {v10, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1202
    invoke-virtual {v10, v8, v12}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1204
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$16;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$16;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v10, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1218
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1221
    const v1, 0x7f0a0026

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$17;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$17;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v7, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1227
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    .line 1228
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1229
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1230
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1231
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$18;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$18;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1240
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdgeDialog:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method private createSubtitleFontPopup()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1244
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1245
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleFontPopup() :: isShowingSubtitleSubPopup() is true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    :goto_0
    return-void

    .line 1249
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1250
    const v1, 0x7f0d01a7

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1251
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-nez v1, :cond_1

    .line 1252
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoStateView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v3, 0x101007a

    invoke-direct {v1, v2, v5, v3}, Lcom/sec/android/app/videoplayer/view/VideoStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 1253
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 1256
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mFontStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 1257
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;

    invoke-direct {v1, p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;)V

    new-array v2, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$MakeFontListTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private createSubtitleLanguagePopup()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v8, 0x1

    .line 1410
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-nez v7, :cond_1

    .line 1411
    :cond_0
    const-string v7, "VideoSubtitlePopup"

    const-string v8, "craeteSubtitleLanguagePopup() - mSubtitleSettingUtil is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    :goto_0
    return-void

    .line 1415
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1416
    const-string v7, "VideoSubtitlePopup"

    const-string v8, "craeteSubtitleLanguagePopup() - isShowingSubtitleSubPopup() is true"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1420
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1422
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v5, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1423
    .local v5, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v9, 0x7f0a0094

    invoke-virtual {v7, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1425
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleCount()I

    move-result v4

    .line 1426
    .local v4, "length":I
    const-string v7, "VideoSubtitlePopup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createSubtitleLanguagePopup. LanguageArray.size() : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1428
    const/4 v6, 0x0

    .line 1430
    .local v6, "tempSelectIndex":[Z
    if-lez v4, :cond_3

    .line 1431
    new-array v6, v4, [Z

    .line 1434
    :cond_3
    move-object v1, v6

    .line 1435
    .local v1, "checkedItems":[Z
    if-nez v4, :cond_5

    move v7, v8

    :goto_1
    new-array v3, v7, [Ljava/lang/String;

    .line 1437
    .local v3, "lanArray":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v4, :cond_7

    .line 1438
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v0

    .line 1439
    .local v0, "LanguageType":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getLanguageTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v2

    .line 1441
    const-string v7, "VideoSubtitlePopup"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createSubtitleLanguagePopup.  array["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] LanguageType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1443
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1444
    aput-boolean v11, v1, v2

    .line 1437
    :cond_4
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v0    # "LanguageType":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "lanArray":[Ljava/lang/String;
    :cond_5
    move v7, v4

    .line 1435
    goto :goto_1

    .line 1446
    .restart local v0    # "LanguageType":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "lanArray":[Ljava/lang/String;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v7

    if-ne v2, v7, :cond_4

    .line 1447
    aput-boolean v8, v1, v2

    goto :goto_3

    .line 1452
    .end local v0    # "LanguageType":Ljava/lang/String;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    if-eqz v7, :cond_8

    if-eqz v1, :cond_8

    .line 1454
    const/4 v2, 0x0

    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    array-length v7, v7

    if-ge v2, v7, :cond_8

    .line 1455
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v7

    aget v7, v7, v2

    aput-boolean v8, v1, v7

    .line 1454
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1459
    :cond_8
    if-nez v4, :cond_9

    .line 1460
    const v7, 0x7f0a0145

    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v3, v11

    .line 1461
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v7, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1465
    :cond_9
    new-instance v7, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$23;

    invoke-direct {v7, p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$23;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;[Z)V

    invoke-virtual {v5, v3, v1, v7}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0a00dd

    new-instance v9, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$22;

    invoke-direct {v9, p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$22;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;[Z)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f0a0026

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1480
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    .line 1481
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1482
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 1483
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1485
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$24;

    invoke-direct {v8, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$24;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1495
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0
.end method

.method private createSubtitleSizePopup()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    .line 1006
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1007
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleSizePopup() - isShowingSubtitleSubPopup() is true"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    :goto_0
    return-void

    .line 1011
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    if-nez v1, :cond_1

    .line 1012
    const-string v1, "VideoSubtitlePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createSubtitleSizePopup() :: return reason isSubtitleSettingPopupActive = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    goto :goto_0

    .line 1017
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v11

    .line 1019
    .local v11, "textSizeList":[Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1020
    .local v4, "textSizeArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {v11}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1022
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f030037

    const/16 v5, 0xb

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/content/Context;ILjava/util/ArrayList;I)V

    .line 1024
    .local v0, "subtitleTextSettingAdapter":Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;
    invoke-virtual {p0, v13}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 1026
    const/4 v8, 0x1

    .line 1027
    .local v8, "selected":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontSize()I

    move-result v10

    .line 1029
    .local v10, "textSize":I
    const/16 v1, 0x1c

    if-ne v10, v1, :cond_3

    .line 1030
    const/4 v8, 0x2

    .line 1037
    :cond_2
    :goto_1
    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v7, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1038
    .local v7, "popup":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a007a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1040
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1041
    .local v6, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030038

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 1042
    .local v9, "textMenuListView":Landroid/view/View;
    const v1, 0x7f0d01bd

    invoke-virtual {v9, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ListView;

    .line 1044
    .local v12, "textSizeListView":Landroid/widget/ListView;
    invoke-virtual {v12, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1045
    invoke-virtual {v12, v8, v13}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 1047
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v12, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1073
    invoke-virtual {v7, v9}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1075
    const v1, 0x7f0a0026

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$11;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v7, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1081
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    .line 1082
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1083
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1084
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 1085
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$12;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1094
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    goto/16 :goto_0

    .line 1031
    .end local v6    # "inflate":Landroid/view/LayoutInflater;
    .end local v7    # "popup":Landroid/app/AlertDialog$Builder;
    .end local v9    # "textMenuListView":Landroid/view/View;
    .end local v12    # "textSizeListView":Landroid/widget/ListView;
    :cond_3
    const/16 v1, 0x16

    if-ne v10, v1, :cond_4

    .line 1032
    const/4 v8, 0x1

    goto :goto_1

    .line 1033
    :cond_4
    const/16 v1, 0x12

    if-ne v10, v1, :cond_2

    .line 1034
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method private createSubtitleSyncPopup()V
    .locals 2

    .prologue
    .line 975
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    if-nez v0, :cond_0

    .line 976
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .line 981
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 982
    return-void

    .line 978
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->show()V

    goto :goto_0
.end method

.method private exitDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 747
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "exitDialog E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 752
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubDialogs:Landroid/app/AlertDialog;

    .line 755
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    if-eqz v0, :cond_1

    .line 756
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->hide()V

    .line 757
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleColorPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .line 760
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMenuDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 761
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMenuDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 762
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMenuDialog:Landroid/app/Dialog;

    .line 765
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_3

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->saveSubtitleSetting()V

    .line 771
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->resume()V

    .line 772
    return-void

    .line 768
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->saveSubtitleSetting()V

    goto :goto_0
.end method

.method private getDimen(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 1620
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1622
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getLanguageTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "LanguageType"    # Ljava/lang/String;

    .prologue
    .line 1499
    const-string v1, "_"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1500
    .local v0, "temp":[Ljava/lang/String;
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v0, v1

    return-object v1
.end method

.method private getStr(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 1615
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initSubtitleMenu()V
    .locals 14

    .prologue
    const v13, 0x7f0a0078

    const v12, 0x7f0a00b7

    const v11, 0x7f0a00b2

    const v10, 0x7f0a00ab

    .line 403
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "initSubtitleMenu()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 406
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 409
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 419
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 420
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 421
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a0141

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    const-string v1, ""

    .line 423
    .local v1, "time":Ljava/lang/String;
    const-string v2, "%.1f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v5

    int-to-double v6, v5

    const-wide v8, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 424
    const-string v2, "MENU_VALUE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0a0120

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 429
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a0094

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    .end local v1    # "time":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 435
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a000c

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v11}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 441
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    invoke-direct {p0, v13}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 446
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a007a

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 450
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 451
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a0079

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v12}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 456
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 457
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    invoke-direct {p0, v13}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 462
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a013a

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 466
    new-instance v0, Ljava/util/HashMap;

    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 467
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    const v3, 0x7f0a0029

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    const-string v2, "MENU_LABLE"

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 470
    return-void
.end method

.method private initUtils()V
    .locals 2

    .prologue
    .line 219
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "initUtils E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 222
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 223
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCtx(Landroid/content/Context;)V

    .line 225
    return-void
.end method

.method private initViews()V
    .locals 7

    .prologue
    const v6, 0x7f0d01aa

    const v5, 0x7f0d01a9

    const v4, 0x7f0d01a8

    const/4 v3, -0x1

    .line 228
    const v2, 0x7f030033

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setContentView(I)V

    .line 230
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v2, :cond_0

    .line 231
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 232
    .local v0, "LP":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 233
    const v2, 0x7f080286

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getDimen(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 234
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 244
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setDisappearListener()V

    .line 245
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 247
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->updateActionbarView()V

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitlePreview()V

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleMenu()V

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleMenu()V

    .line 251
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->pause()V

    .line 252
    return-void

    .line 236
    .end local v0    # "LP":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 237
    .local v1, "LP_preview":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 238
    .restart local v0    # "LP":Landroid/widget/RelativeLayout$LayoutParams;
    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 239
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 240
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private makeFontList()Z
    .locals 2

    .prologue
    .line 1280
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "makeFontList()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1282
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-eqz v0, :cond_0

    .line 1283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->initFontList()V

    .line 1286
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private setCustomLabelEnable(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    .line 1678
    const v2, 0x7f0d01ab

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1680
    .local v1, "smpteActivation":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSMPTETTFileType()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    .line 1681
    const v2, 0x7f0d01ad

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1683
    .local v0, "cbSMPTE":Landroid/widget/CheckBox;
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1684
    invoke-virtual {v1, p1}, Landroid/view/View;->setClickable(Z)V

    .line 1686
    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1688
    if-eqz p1, :cond_1

    .line 1689
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1694
    .end local v0    # "cbSMPTE":Landroid/widget/CheckBox;
    :cond_0
    :goto_0
    return-void

    .line 1691
    .restart local v0    # "cbSMPTE":Landroid/widget/CheckBox;
    :cond_1
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private setDisappearListener()V
    .locals 1

    .prologue
    .line 775
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$8;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 786
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$9;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 796
    return-void
.end method

.method private setFontEdge(Landroid/widget/TextView;IZ)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "fontedge"    # I
    .param p3, "isMenuMode"    # Z

    .prologue
    .line 943
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontEdge(Landroid/widget/TextView;IZ)V

    .line 944
    return-void
.end method

.method private setLableEnable(Z)V
    .locals 9
    .param p1, "act"    # Z

    .prologue
    const v8, 0x7f0a015e

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 1627
    const v3, 0x7f0d01af

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1628
    .local v1, "lableOverviewView":Landroid/view/View;
    const v3, 0x7f0d01b2

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1629
    .local v2, "lableTextView":Landroid/view/View;
    const v3, 0x7f0d01b5

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1631
    .local v0, "lableColorView":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1632
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00b3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1634
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00b8

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1636
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00ac

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1639
    if-eqz p1, :cond_1

    .line 1640
    invoke-virtual {v1, v7}, Landroid/view/View;->setAlpha(F)V

    .line 1641
    invoke-virtual {v2, v7}, Landroid/view/View;->setAlpha(F)V

    .line 1642
    invoke-virtual {v0, v7}, Landroid/view/View;->setAlpha(F)V

    .line 1649
    :cond_0
    :goto_0
    return-void

    .line 1644
    :cond_1
    invoke-virtual {v1, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1645
    invoke-virtual {v2, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1646
    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method private setListViewHeight(Landroid/widget/ListView;)V
    .locals 8
    .param p1, "settinglistView"    # Landroid/widget/ListView;

    .prologue
    const/4 v6, 0x0

    .line 584
    invoke-virtual {p1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    .line 585
    .local v3, "myListAdapter":Landroid/widget/ListAdapter;
    if-nez v3, :cond_0

    .line 600
    :goto_0
    return-void

    .line 589
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getWidth()I

    move-result v5

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 590
    .local v0, "desiredWidth":I
    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 592
    .local v1, "itemCount":I
    const/4 v5, 0x0

    invoke-interface {v3, v6, v5, p1}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 593
    .local v2, "listItem":Landroid/view/View;
    invoke-virtual {v2, v0, v6}, Landroid/view/View;->measure(II)V

    .line 595
    invoke-virtual {p1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 596
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    mul-int/2addr v5, v1

    invoke-virtual {p1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v6

    add-int/lit8 v7, v1, -0x1

    mul-int/2addr v6, v7

    add-int/2addr v5, v6

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 597
    invoke-virtual {p1, v4}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 599
    invoke-virtual {p1}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method private setMenuEnable(Z)V
    .locals 6
    .param p1, "act"    # Z

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    .line 1652
    const v3, 0x7f0d01b1

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 1653
    .local v1, "overviewMenuList":Landroid/widget/ListView;
    const v3, 0x7f0d01b4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 1654
    .local v2, "textMenuList":Landroid/widget/ListView;
    const v3, 0x7f0d01b7

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 1656
    .local v0, "colorMenuList":Landroid/widget/ListView;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1657
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1658
    invoke-virtual {v2, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1659
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 1661
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setClickable(Z)V

    .line 1662
    invoke-virtual {v2, p1}, Landroid/widget/ListView;->setClickable(Z)V

    .line 1663
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setClickable(Z)V

    .line 1665
    if-eqz p1, :cond_1

    .line 1666
    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1667
    invoke-virtual {v2, v5}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1668
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1675
    :cond_0
    :goto_0
    return-void

    .line 1670
    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1671
    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1672
    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method checkSubtitleLangDialogDoneBtn()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1504
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 1505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 1506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1512
    :cond_0
    :goto_0
    return-void

    .line 1508
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1509
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a0124

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0
.end method

.method public createSubtitleMenu()V
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 474
    const v8, 0x7f0d01ab

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 475
    .local v5, "smpteActivation":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSMPTEActivationListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 477
    const v8, 0x7f0d01ad

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 478
    .local v0, "cbSMPTE":Landroid/widget/CheckBox;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSMPTETTCustomMode()Z

    move-result v8

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 479
    const v8, 0x7f0d01ae

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 481
    .local v3, "line":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mCbSMPTEActivationListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 483
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isSMPTETTFileType()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 484
    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 485
    invoke-virtual {v3, v9}, Landroid/view/View;->setVisibility(I)V

    .line 487
    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setCustomLabelEnable(Z)V

    .line 489
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSMPTETTCustomMode()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 490
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    .line 500
    :goto_0
    const v8, 0x7f0d01b1

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    .line 501
    .local v4, "overviewMenuList":Landroid/widget/ListView;
    const v8, 0x7f0d01b4

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 502
    .local v6, "textMenuList":Landroid/widget/ListView;
    const v8, 0x7f0d01b7

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 504
    .local v1, "colorMenuList":Landroid/widget/ListView;
    new-array v2, v11, [Ljava/lang/String;

    const-string v8, "MENU_TITLE"

    aput-object v8, v2, v9

    const-string v8, "MENU_VALUE"

    aput-object v8, v2, v10

    .line 508
    .local v2, "key":[Ljava/lang/String;
    new-array v7, v11, [I

    fill-array-data v7, :array_0

    .line 512
    .local v7, "values":[I
    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewList:Ljava/util/ArrayList;

    invoke-direct {v8, p0, v9, v2, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Ljava/util/ArrayList;[Ljava/lang/String;[I)V

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 513
    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextList:Ljava/util/ArrayList;

    invoke-direct {v8, p0, v9, v2, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Ljava/util/ArrayList;[Ljava/lang/String;[I)V

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 514
    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorList:Ljava/util/ArrayList;

    invoke-direct {v8, p0, v9, v2, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Ljava/util/ArrayList;[Ljava/lang/String;[I)V

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    .line 516
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 517
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 518
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 520
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->overviewOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v4, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 521
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->textOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 522
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->colorOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 524
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setListViewHeight(Landroid/widget/ListView;)V

    .line 525
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setListViewHeight(Landroid/widget/ListView;)V

    .line 526
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setListViewHeight(Landroid/widget/ListView;)V

    .line 528
    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setMenuEnable(Z)V

    .line 529
    return-void

    .line 492
    .end local v1    # "colorMenuList":Landroid/widget/ListView;
    .end local v2    # "key":[Ljava/lang/String;
    .end local v4    # "overviewMenuList":Landroid/widget/ListView;
    .end local v6    # "textMenuList":Landroid/widget/ListView;
    .end local v7    # "values":[I
    :cond_0
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    goto :goto_0

    .line 495
    :cond_1
    invoke-virtual {v5, v12}, Landroid/view/View;->setVisibility(I)V

    .line 496
    invoke-virtual {v3, v12}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 508
    :array_0
    .array-data 4
        0x7f0d01b9
        0x7f0d01ba
    .end array-data
.end method

.method public dismissSyncPopup()V
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->isShowSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->hide()V

    .line 815
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .line 817
    :cond_0
    return-void
.end method

.method public getMultiSelectedSubtitleLanguage()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1515
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-nez v3, :cond_0

    .line 1516
    const-string v3, ""

    .line 1531
    :goto_0
    return-object v3

    .line 1519
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1520
    .local v1, "retValue":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getMultiSelectSubtitle()I

    move-result v2

    .line 1521
    .local v2, "subtitle_cnt":I
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSubtitleSettingUtil.getMultiSelectSubtitle() - selected subtitle_cnt : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_2

    .line 1524
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v4

    aget v4, v4, v0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguage(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1525
    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    add-int/lit8 v3, v2, -0x1

    if-ge v0, v3, :cond_1

    .line 1526
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1523
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1530
    :cond_2
    const-string v3, "VideoSubtitlePopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMultiSelectedSubtitleLanguage()-return string : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1531
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public initSubtitlePreview()V
    .locals 13

    .prologue
    const/16 v12, 0xe

    const/16 v11, 0xb

    const/16 v9, 0x9

    const/4 v10, 0x0

    .line 346
    const-string v7, "VideoSubtitlePopup"

    const-string v8, "initSubtitlePreview E"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    const v7, 0x7f0d01aa

    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 350
    .local v2, "subtitlePreview":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 351
    iget-boolean v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    if-nez v7, :cond_1

    .line 352
    const/4 v7, 0x4

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 356
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTextAlignment()I

    move-result v3

    .line 357
    .local v3, "textAlignment":I
    invoke-virtual {v2}, Landroid/widget/TextView;->getGravity()I

    move-result v7

    and-int/lit8 v4, v7, -0x8

    .line 358
    .local v4, "textGravity":I
    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 360
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v7, 0x1e

    if-ne v3, v7, :cond_2

    .line 361
    or-int/lit8 v4, v4, 0x3

    .line 362
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 363
    invoke-virtual {v1, v11}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 364
    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 377
    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 378
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 380
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-direct {v5, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 382
    .local v5, "textSpan":Landroid/text/Spannable;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGColor()I

    move-result v0

    .line 383
    .local v0, "color":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGOpacity()I

    move-result v7

    invoke-static {v0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 384
    new-instance v7, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v7, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v8

    const/16 v9, 0x21

    invoke-interface {v5, v7, v10, v8, v9}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 385
    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontColor()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontOpacity()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 388
    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontSize()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v7, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 389
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinColor()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinOpacity()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 390
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 392
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontEdge()I

    move-result v7

    invoke-direct {p0, v2, v7, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setFontEdge(Landroid/widget/TextView;IZ)V

    .line 393
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    .line 395
    .local v6, "tf":Landroid/graphics/Typeface;
    if-eqz v6, :cond_0

    .line 396
    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto/16 :goto_0

    .line 365
    .end local v0    # "color":I
    .end local v5    # "textSpan":Landroid/text/Spannable;
    .end local v6    # "tf":Landroid/graphics/Typeface;
    :cond_2
    const/16 v7, 0x20

    if-ne v3, v7, :cond_3

    .line 366
    or-int/lit8 v4, v4, 0x5

    .line 367
    invoke-virtual {v1, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 368
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 369
    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto/16 :goto_1

    .line 371
    :cond_3
    or-int/lit8 v4, v4, 0x1

    .line 372
    invoke-virtual {v1, v12}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 373
    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 374
    invoke-virtual {v1, v11}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto/16 :goto_1
.end method

.method public initSubtitleValues()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 532
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "initSubtitleValues()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isSubtitleSettingPopupActive:Z

    .line 536
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-nez v2, :cond_0

    .line 537
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "initSubtitleValues() - mSubtitleSettingUtil is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :goto_0
    return-void

    .line 541
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-nez v2, :cond_3

    .line 542
    :cond_1
    iput v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncTime:I

    .line 543
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 568
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleActive:Z

    .line 569
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSelectedFont()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFont:Ljava/lang/String;

    .line 570
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontColor()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontColor:I

    .line 571
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontSize()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontSize:I

    .line 572
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinColor()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleBGColor:I

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontEdge()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdge:I

    .line 575
    const-string v2, "VideoSubtitlePopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initSubtitleValues() - mSubtitleFilePath = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFilePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleSyncTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncTime:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleActive = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleActive:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleFont = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFont:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleFontColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontColor:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleFontSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFontSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleBGColor = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleBGColor:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , mSubtitleFontEdge = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleEdge:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 545
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncTime:I

    .line 546
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleFilePath:Ljava/lang/String;

    .line 547
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleCount()I

    move-result v1

    .line 548
    .local v1, "length":I
    new-array v2, v1, [Z

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    .line 550
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_6

    .line 551
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 552
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    aput-boolean v5, v2, v0

    .line 550
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 554
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v2

    if-ne v0, v2, :cond_4

    .line 555
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    aput-boolean v4, v2, v0

    goto :goto_2

    .line 560
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getIsMultiSubtitle()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v2

    if-eqz v2, :cond_2

    .line 562
    const/4 v0, 0x0

    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v2

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleLangArray:[Z

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v3

    aget v3, v3, v0

    aput-boolean v4, v2, v3

    .line 562
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public isShowingSubtitleSubPopup()Z
    .locals 1

    .prologue
    .line 1726
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mShowingSubtitleSubPopup:Z

    return v0
.end method

.method public isShowingSyncPopup()Z
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSyncPopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->isShowSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOrientationChanged()V
    .locals 0

    .prologue
    .line 652
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initViews()V

    .line 653
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleValues()V

    .line 654
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refresh()V

    .line 655
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 1720
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 1721
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 1723
    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 1697
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setLableEnable(Z)V

    .line 1698
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setMenuEnable(Z)V

    .line 1700
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_0

    .line 1701
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 1704
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_1

    .line 1705
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 1708
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_2

    .line 1709
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 1711
    :cond_2
    return-void
.end method

.method public refreshSubtitleMenu()V
    .locals 2

    .prologue
    .line 947
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "refreshSubtitleMenu()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 949
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOverviewMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_1

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mTextMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 957
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    if-eqz v0, :cond_2

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mColorMenuAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleMenuListAdapter;->notifyDataSetChanged()V

    .line 961
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitlePreview()V

    .line 962
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 1714
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 1715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 1717
    :cond_0
    return-void
.end method

.method public setOnDialogDisappearListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

    .prologue
    .line 805
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mOnDialogDisappearListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$OnDialogDisappearListener;

    .line 806
    return-void
.end method

.method public setShowingSubtitleSubPopup(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 1730
    const-string v0, "VideoSubtitlePopup"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setShowingSubtitleSubPopup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1731
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mShowingSubtitleSubPopup:Z

    .line 1732
    return-void
.end method

.method public updateActionbarView()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 255
    const v2, 0x7f0d0002

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Switch;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mButton:Landroid/widget/Switch;

    .line 256
    const v2, 0x7f0d0004

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mBackLayout:Landroid/widget/RelativeLayout;

    .line 257
    const/4 v1, 0x0

    .line 258
    .local v1, "title":Ljava/lang/String;
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isFromVideo:Z

    if-eqz v2, :cond_3

    .line 259
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 263
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0164

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "navUp":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mBackLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 266
    const v2, 0x7f0d0006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    .line 267
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v2, :cond_0

    .line 269
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    const-string v3, "sec-roboto-light"

    invoke-static {v3, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 271
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v2, :cond_1

    .line 272
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 273
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    const v3, 0x7f08028c

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getDimen(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 279
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getSubtitleActivation()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 280
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    .line 285
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mButton:Landroid/widget/Switch;

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 286
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setLableEnable(Z)V

    .line 288
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mButton:Landroid/widget/Switch;

    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 330
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v2, :cond_2

    .line 331
    const v2, 0x7f0d0003

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mDivider:Landroid/widget/RelativeLayout;

    .line 332
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mDivider:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 334
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mBackLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 343
    return-void

    .line 261
    .end local v0    # "navUp":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 275
    .restart local v0    # "navUp":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->titleText:Landroid/widget/TextView;

    const v3, 0x7f08028b

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getDimen(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v5, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1

    .line 282
    :cond_5
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z

    goto :goto_2
.end method

.method public updateDialog()V
    .locals 2

    .prologue
    .line 1735
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "updateDialog E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1737
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->updateActionbarView()V

    .line 1738
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleValues()V

    .line 1739
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitlePreview()V

    .line 1740
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->initSubtitleMenu()V

    .line 1741
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleMenu()V

    .line 1742
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->pause()V

    .line 1743
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refresh()V

    .line 1744
    return-void
.end method
