.class public Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;
.super Ljava/lang/Object;
.source "MenuExecutor.java"


# static fields
.field private static sMenuExecutor:Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;


# instance fields
.field private menuCmdList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/sec/android/app/videoplayer/cmd/MenuCmdFactory;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/cmd/MenuCmdFactory;-><init>()V

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/cmd/MenuCmdFactory;->create()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->menuCmdList:Landroid/util/SparseArray;

    .line 13
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->sMenuExecutor:Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->sMenuExecutor:Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    .line 17
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->sMenuExecutor:Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    return-object v0
.end method


# virtual methods
.method public execute(ILandroid/content/Context;)V
    .locals 2
    .param p1, "mode"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->menuCmdList:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;

    .line 27
    .local v0, "menuCmd":Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;
    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;->execute(Landroid/content/Context;)V

    .line 28
    :cond_0
    return-void
.end method
