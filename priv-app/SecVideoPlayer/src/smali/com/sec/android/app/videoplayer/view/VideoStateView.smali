.class public Lcom/sec/android/app/videoplayer/view/VideoStateView;
.super Landroid/widget/ProgressBar;
.source "VideoStateView.java"


# instance fields
.field private mParentView:Landroid/widget/RelativeLayout;

.field private final viewID:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 16
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->viewID:I

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 16
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->viewID:I

    .line 20
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x2

    .line 26
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 27
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 28
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 29
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->viewID:I

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setId(I)V

    .line 31
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoStateView;->mParentView:Landroid/widget/RelativeLayout;

    .line 43
    return-void
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 34
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 35
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 36
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 37
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->requestLayout()V

    .line 38
    return-void
.end method
