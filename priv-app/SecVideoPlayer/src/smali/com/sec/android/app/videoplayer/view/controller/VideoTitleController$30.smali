.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2125
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 2127
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2128
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2150
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 2130
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2131
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 2135
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 2136
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2137
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 2138
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 2139
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->oneFrameBackward()V

    .line 2140
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a016f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToastTitleController(I)V

    goto :goto_0

    .line 2128
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
