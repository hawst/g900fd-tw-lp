.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDownPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 1

    .prologue
    .line 2094
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2095
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->mDownPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2098
    const/4 v4, 0x0

    .line 2099
    .local v4, "retVal":Z
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 2100
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    .line 2101
    .local v0, "isDlna":Z
    const-wide/16 v2, 0x0

    .line 2103
    .local v2, "pressTime":J
    sparse-switch p2, :sswitch_data_0

    .line 2168
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2169
    .local v1, "isReturn":Z
    if-eqz v1, :cond_9

    .line 2186
    .end local v0    # "isDlna":Z
    .end local v1    # "isReturn":Z
    .end local v2    # "pressTime":J
    :cond_0
    :goto_0
    return v4

    .line 2106
    .restart local v0    # "isDlna":Z
    .restart local v2    # "pressTime":J
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 2108
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 2109
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    .line 2110
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2113
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->blockSpeedSeek()Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2114
    const-string v5, "VideoSubViewBtnController"

    const-string v6, "mFfKeyListener. skip longseek"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2115
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v6

    const/16 v7, 0x1e

    const/16 v8, 0x8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v6

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2119
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->mDownPath:Ljava/lang/String;

    goto :goto_0

    .line 2117
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1

    .line 2123
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 2124
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const-wide/16 v6, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    .line 2126
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->blockSpeedSeek()Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2127
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_3

    .line 2128
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v5

    const/16 v6, 0x1e

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 2130
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_3

    .line 2131
    if-eqz v0, :cond_4

    .line 2132
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2159
    :cond_3
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v6, 0xbb8

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 2134
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 2135
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2136
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 2141
    :cond_5
    const-wide/16 v6, 0x1f4

    cmp-long v5, v2, v6

    if-gez v5, :cond_7

    .line 2142
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 2143
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->resetHoldLongSeek()V

    goto :goto_2

    .line 2145
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 2146
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2147
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 2150
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->mDownPath:Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->mDownPath:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2151
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const/4 v6, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1702(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 2152
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xf

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 2154
    :cond_8
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    const/16 v6, 0xb

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 2171
    .restart local v1    # "isReturn":Z
    :cond_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_1

    goto/16 :goto_0

    .line 2173
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    .line 2174
    goto/16 :goto_0

    .line 2177
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v4

    .line 2178
    goto/16 :goto_0

    .line 2103
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 2106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2171
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
