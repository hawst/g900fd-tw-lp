.class public final Lcom/sec/android/app/videoplayer/Video360/MathUtil;
.super Ljava/lang/Object;
.source "MathUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static extractBasis([F[F[F[F)V
    .locals 7
    .param p0, "rot"    # [F
    .param p1, "x"    # [F
    .param p2, "y"    # [F
    .param p3, "z"    # [F

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    array-length v0, p0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 6
    if-eqz p1, :cond_0

    aget v0, p0, v2

    aput v0, p1, v2

    aget v0, p0, v3

    aput v0, p1, v3

    aget v0, p0, v4

    aput v0, p1, v4

    .line 7
    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x3

    aget v0, p0, v0

    aput v0, p2, v2

    aget v0, p0, v5

    aput v0, p2, v3

    aget v0, p0, v6

    aput v0, p2, v4

    .line 8
    :cond_1
    if-eqz p3, :cond_2

    const/4 v0, 0x6

    aget v0, p0, v0

    aput v0, p3, v2

    const/4 v0, 0x7

    aget v0, p0, v0

    aput v0, p3, v3

    const/16 v0, 0x8

    aget v0, p0, v0

    aput v0, p3, v4

    .line 14
    :cond_2
    :goto_0
    return-void

    .line 9
    :cond_3
    array-length v0, p0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    .line 10
    if-eqz p1, :cond_4

    aget v0, p0, v2

    aput v0, p1, v2

    aget v0, p0, v3

    aput v0, p1, v3

    aget v0, p0, v4

    aput v0, p1, v4

    .line 11
    :cond_4
    if-eqz p2, :cond_5

    aget v0, p0, v5

    aput v0, p2, v2

    aget v0, p0, v6

    aput v0, p2, v3

    const/4 v0, 0x6

    aget v0, p0, v0

    aput v0, p2, v4

    .line 12
    :cond_5
    if-eqz p3, :cond_2

    const/16 v0, 0x8

    aget v0, p0, v0

    aput v0, p3, v2

    const/16 v0, 0x9

    aget v0, p0, v0

    aput v0, p3, v3

    const/16 v0, 0xa

    aget v0, p0, v0

    aput v0, p3, v4

    goto :goto_0
.end method

.method public static negate([F)V
    .locals 3
    .param p0, "v"    # [F

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    .line 30
    array-length v0, p0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 31
    const/4 v0, 0x0

    aget v1, p0, v0

    mul-float/2addr v1, v2

    aput v1, p0, v0

    .line 32
    const/4 v0, 0x1

    aget v1, p0, v0

    mul-float/2addr v1, v2

    aput v1, p0, v0

    .line 33
    const/4 v0, 0x2

    aget v1, p0, v0

    mul-float/2addr v1, v2

    aput v1, p0, v0

    .line 35
    :cond_0
    return-void
.end method

.method public static orthonormalize([F)V
    .locals 11
    .param p0, "m"    # [F

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 38
    array-length v3, p0

    const/16 v4, 0x10

    if-ne v3, v4, :cond_0

    .line 40
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    aget v3, p0, v7

    aget v4, p0, v8

    aget v5, p0, v9

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    .line 41
    .local v0, "xA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    new-instance v1, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    aget v3, p0, v10

    const/4 v4, 0x5

    aget v4, p0, v4

    const/4 v5, 0x6

    aget v5, p0, v5

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    .line 42
    .local v1, "yA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    new-instance v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    const/16 v3, 0x8

    aget v3, p0, v3

    const/16 v4, 0x9

    aget v4, p0, v4

    const/16 v5, 0xa

    aget v5, p0, v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    .line 44
    .local v2, "zA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->normalize()V

    .line 46
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->project(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->subtract(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v1

    .line 47
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->normalize()V

    .line 49
    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->project(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->subtract(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->project(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->subtract(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;

    move-result-object v2

    .line 50
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->normalize()V

    .line 52
    iget v3, v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    aput v3, p0, v7

    iget v3, v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    aput v3, p0, v8

    iget v3, v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    aput v3, p0, v9

    const/4 v3, 0x3

    aput v6, p0, v3

    .line 53
    iget v3, v1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    aput v3, p0, v10

    const/4 v3, 0x5

    iget v4, v1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    aput v4, p0, v3

    const/4 v3, 0x6

    iget v4, v1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    aput v4, p0, v3

    const/4 v3, 0x7

    aput v6, p0, v3

    .line 54
    const/16 v3, 0x8

    iget v4, v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    aput v4, p0, v3

    const/16 v3, 0x9

    iget v4, v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    aput v4, p0, v3

    const/16 v3, 0xa

    iget v4, v2, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    aput v4, p0, v3

    const/16 v3, 0xb

    aput v6, p0, v3

    .line 55
    const/16 v3, 0xc

    aput v6, p0, v3

    const/16 v3, 0xd

    aput v6, p0, v3

    const/16 v3, 0xe

    aput v6, p0, v3

    const/16 v3, 0xf

    const/high16 v4, 0x3f800000    # 1.0f

    aput v4, p0, v3

    .line 57
    .end local v0    # "xA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .end local v1    # "yA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .end local v2    # "zA":Lcom/sec/android/app/videoplayer/Video360/Vector3;
    :cond_0
    return-void
.end method

.method public static setMatrix([F[F[F[F)V
    .locals 7
    .param p0, "m"    # [F
    .param p1, "x"    # [F
    .param p2, "y"    # [F
    .param p3, "z"    # [F

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    array-length v0, p0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 18
    if-eqz p1, :cond_0

    aget v0, p1, v2

    aput v0, p0, v2

    aget v0, p1, v3

    aput v0, p0, v3

    aget v0, p1, v4

    aput v0, p0, v4

    aput v5, p0, v6

    .line 19
    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x4

    aget v1, p2, v2

    aput v1, p0, v0

    const/4 v0, 0x5

    aget v1, p2, v3

    aput v1, p0, v0

    const/4 v0, 0x6

    aget v1, p2, v4

    aput v1, p0, v0

    const/4 v0, 0x7

    aput v5, p0, v0

    .line 20
    :cond_1
    if-eqz p3, :cond_2

    const/16 v0, 0x8

    aget v1, p3, v2

    aput v1, p0, v0

    const/16 v0, 0x9

    aget v1, p3, v3

    aput v1, p0, v0

    const/16 v0, 0xa

    aget v1, p3, v4

    aput v1, p0, v0

    const/16 v0, 0xb

    aput v5, p0, v0

    .line 21
    :cond_2
    const/16 v0, 0xc

    aput v5, p0, v0

    const/16 v0, 0xd

    aput v5, p0, v0

    const/16 v0, 0xe

    aput v5, p0, v0

    const/16 v0, 0xf

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, p0, v0

    .line 27
    :cond_3
    :goto_0
    return-void

    .line 22
    :cond_4
    array-length v0, p0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_3

    .line 23
    if-eqz p1, :cond_5

    aget v0, p1, v2

    aput v0, p0, v2

    aget v0, p1, v3

    aput v0, p0, v3

    aget v0, p1, v4

    aput v0, p0, v4

    .line 24
    :cond_5
    if-eqz p2, :cond_6

    aget v0, p2, v2

    aput v0, p0, v6

    const/4 v0, 0x4

    aget v1, p2, v3

    aput v1, p0, v0

    const/4 v0, 0x5

    aget v1, p2, v4

    aput v1, p0, v0

    .line 25
    :cond_6
    if-eqz p3, :cond_3

    const/4 v0, 0x6

    aget v1, p3, v2

    aput v1, p0, v0

    const/4 v0, 0x7

    aget v1, p3, v3

    aput v1, p0, v0

    const/16 v0, 0x8

    aget v1, p3, v4

    aput v1, p0, v0

    goto :goto_0
.end method
