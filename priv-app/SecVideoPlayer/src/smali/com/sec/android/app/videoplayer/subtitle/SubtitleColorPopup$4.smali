.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->createPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 179
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    const-wide/16 v2, 0x1f4

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->sendMessage(IJ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;IJ)V

    goto :goto_0

    .line 183
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->removeMessage(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->clickPlusBtn()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    goto :goto_0

    .line 188
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->removeMessage(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    goto :goto_0

    .line 177
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
