.class public Lcom/sec/android/app/videoplayer/view/PanRectView;
.super Landroid/view/View;
.source "PanRectView.java"


# static fields
.field private static mGuidemapBottom:I

.field private static mGuidemapHeight:I

.field private static mGuidemapLeft:I

.field private static mGuidemapRight:I

.field private static mGuidemapTop:I

.field private static mGuidemapWidth:I

.field private static mMyPaint:Landroid/graphics/Paint;

.field private static mRectBig:Landroid/graphics/Rect;

.field private static mVideoHeight:I

.field private static mVideoWidth:I

.field private static mZoomMode:Z

.field private static mZoomedBottom:I

.field private static mZoomedLeft:I

.field private static mZoomedRight:I

.field private static mZoomedTop:I


# instance fields
.field private mDisplayMetrics:Landroid/util/DisplayMetrics;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoWidth:I

    .line 19
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoHeight:I

    .line 24
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedLeft:I

    .line 25
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedTop:I

    .line 26
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedRight:I

    .line 27
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedBottom:I

    .line 32
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    .line 33
    const/16 v0, 0xc8

    sput v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    .line 34
    const/16 v0, 0xfa

    sput v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    .line 35
    const/16 v0, 0x154

    sput v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapBottom:I

    .line 37
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    .line 38
    sput v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    .line 41
    sput-boolean v1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomMode:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayWidth"    # I
    .param p3, "displayHeight"    # I
    .param p4, "VideoWidth"    # I
    .param p5, "VideoHeight"    # I

    .prologue
    const v5, 0x7f080099

    const v4, 0x7f080098

    const/4 v2, 0x0

    .line 46
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 48
    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedLeft:I

    .line 49
    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedTop:I

    .line 50
    sput p2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedRight:I

    .line 51
    sput p3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedBottom:I

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08009a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 54
    .local v1, "mapWidth":I
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080096

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 56
    .local v0, "mapHeight":I
    if-le p4, p5, :cond_0

    move v2, v1

    :goto_0
    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    .line 57
    if-le p4, p5, :cond_1

    .end local v0    # "mapHeight":I
    :goto_1
    sput v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    .line 59
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v2, :cond_2

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080097

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    .line 63
    sget v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    sget v3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    add-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    .line 64
    sget v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    sget v3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    add-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapBottom:I

    .line 78
    :goto_2
    new-instance v2, Landroid/graphics/Rect;

    sget v3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    sget v4, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapBottom:I

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mRectBig:Landroid/graphics/Rect;

    .line 79
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    .line 80
    return-void

    .restart local v0    # "mapHeight":I
    :cond_0
    move v2, v0

    .line 56
    goto :goto_0

    :cond_1
    move v0, v1

    .line 57
    goto :goto_1

    .line 66
    .end local v0    # "mapHeight":I
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v2, :cond_3

    move-object v2, p1

    .line 67
    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getPreviousAppWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    .line 72
    :goto_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    .line 74
    sget v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    sget v3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    sub-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    .line 75
    sget v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    sget v3, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    add-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapBottom:I

    goto :goto_2

    .line 69
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 70
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    sput v2, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapRight:I

    goto :goto_3
.end method

.method public static isZoomEnabled()Z
    .locals 1

    .prologue
    .line 169
    sget-boolean v0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomMode:Z

    return v0
.end method

.method public static setZoomMode(Z)V
    .locals 0
    .param p0, "mode"    # Z

    .prologue
    .line 165
    sput-boolean p0, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomMode:Z

    .line 166
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x0

    .line 85
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 92
    sget-boolean v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomMode:Z

    if-nez v5, :cond_0

    .line 138
    :goto_0
    return-void

    .line 100
    :cond_0
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 102
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 103
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40400000    # 3.0f

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 107
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedLeft:I

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoWidth:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v6, v6

    mul-float v1, v5, v6

    .line 108
    .local v1, "left":F
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedTop:I

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoHeight:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v6, v6

    mul-float v4, v5, v6

    .line 110
    .local v4, "top":F
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedRight:I

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoWidth:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v6, v6

    mul-float v3, v5, v6

    .line 111
    .local v3, "right":F
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedBottom:I

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoHeight:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v6, v6

    mul-float v0, v5, v6

    .line 113
    .local v0, "bottom":F
    cmpg-float v5, v1, v7

    if-gez v5, :cond_1

    .line 114
    const/4 v1, 0x0

    .line 115
    :cond_1
    cmpg-float v5, v4, v7

    if-gez v5, :cond_2

    .line 116
    const/4 v4, 0x0

    .line 117
    :cond_2
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-lez v5, :cond_3

    .line 118
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v3, v5

    .line 119
    :cond_3
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v5, v5

    cmpl-float v5, v0, v5

    if-lez v5, :cond_4

    .line 120
    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v0, v5

    .line 122
    :cond_4
    cmpl-float v5, v1, v7

    if-nez v5, :cond_5

    cmpl-float v5, v4, v7

    if-nez v5, :cond_5

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-nez v5, :cond_5

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v5, v5

    cmpl-float v5, v0, v5

    if-nez v5, :cond_5

    .line 123
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 125
    :cond_5
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mRectBig:Landroid/graphics/Rect;

    sget-object v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 127
    cmpl-float v5, v1, v7

    if-nez v5, :cond_6

    cmpl-float v5, v4, v7

    if-nez v5, :cond_6

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapWidth:I

    int-to-float v5, v5

    cmpl-float v5, v3, v5

    if-nez v5, :cond_6

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapHeight:I

    int-to-float v5, v5

    cmpl-float v5, v0, v5

    if-eqz v5, :cond_7

    .line 130
    :cond_6
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    const/high16 v6, -0x10000

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    :cond_7
    new-instance v2, Landroid/graphics/Rect;

    sget v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    float-to-int v6, v1

    add-int/2addr v5, v6

    sget v6, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    float-to-int v7, v4

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapLeft:I

    float-to-int v8, v3

    add-int/2addr v7, v8

    sget v8, Lcom/sec/android/app/videoplayer/view/PanRectView;->mGuidemapTop:I

    float-to-int v9, v0

    add-int/2addr v8, v9

    invoke-direct {v2, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 137
    .local v2, "rectSmall":Landroid/graphics/Rect;
    sget-object v5, Lcom/sec/android/app/videoplayer/view/PanRectView;->mMyPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public setVideoHeight(I)V
    .locals 0
    .param p1, "VideoHeight"    # I

    .prologue
    .line 145
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoHeight:I

    .line 146
    return-void
.end method

.method public setVideoWidth(I)V
    .locals 0
    .param p1, "videoWidth"    # I

    .prologue
    .line 141
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mVideoWidth:I

    .line 142
    return-void
.end method

.method public setZoomedBottom(I)V
    .locals 0
    .param p1, "zoomedBottom"    # I

    .prologue
    .line 161
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedBottom:I

    .line 162
    return-void
.end method

.method public setZoomedLeft(I)V
    .locals 0
    .param p1, "zoomedLeft"    # I

    .prologue
    .line 149
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedLeft:I

    .line 150
    return-void
.end method

.method public setZoomedRight(I)V
    .locals 0
    .param p1, "zoomedRight"    # I

    .prologue
    .line 157
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedRight:I

    .line 158
    return-void
.end method

.method public setZoomedTop(I)V
    .locals 0
    .param p1, "zoomedTop"    # I

    .prologue
    .line 153
    sput p1, Lcom/sec/android/app/videoplayer/view/PanRectView;->mZoomedTop:I

    .line 154
    return-void
.end method
