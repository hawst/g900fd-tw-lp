.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2076
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2078
    const/4 v1, 0x0

    .line 2079
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2080
    sparse-switch p2, :sswitch_data_0

    .line 2102
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 2103
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 2121
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 2083
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2085
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2086
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 2090
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2091
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->oneFrameBackward()V

    .line 2092
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a016f

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToastTitleController(I)V

    goto :goto_0

    .line 2106
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 2108
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2109
    goto :goto_0

    .line 2112
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2113
    goto :goto_0

    .line 2080
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 2083
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2106
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
