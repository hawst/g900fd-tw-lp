.class public Lcom/sec/android/app/videoplayer/data/SideSyncInfo;
.super Ljava/lang/Object;
.source "SideSyncInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SideSyncInfo"

.field private static mIsFromSideSync:Z

.field private static mTitleofSideSync:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsNeedSaveResumePos:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mTitleofSideSync:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsFromSideSync:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsNeedSaveResumePos:Z

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mContext:Landroid/content/Context;

    .line 25
    return-void
.end method

.method public static getTitleofSideSync()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mTitleofSideSync:Ljava/lang/String;

    return-object v0
.end method

.method public static isFromSideSync()Z
    .locals 1

    .prologue
    .line 37
    sget-boolean v0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsFromSideSync:Z

    return v0
.end method

.method public static isSinkRunning(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 78
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_sink_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 81
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isTitleExist()Z
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mTitleofSideSync:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setFromSideSync(Z)V
    .locals 0
    .param p0, "mIsFromSideSync"    # Z

    .prologue
    .line 41
    sput-boolean p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsFromSideSync:Z

    .line 42
    return-void
.end method

.method public static setTitleofSideSync(Ljava/lang/String;)V
    .locals 3
    .param p0, "mTitleofSideSync"    # Ljava/lang/String;

    .prologue
    .line 32
    const-string v0, "SideSyncInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTitleofSideSync : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    sput-object p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mTitleofSideSync:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public isNeedSaveResumePos()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsNeedSaveResumePos:Z

    return v0
.end method

.method public isSinkRunning()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 71
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_sink_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 74
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isSourceRunning()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sidesync_source_connect"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 60
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public isSymmetricModeRunning()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sidesync_source_connect"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 65
    const/4 v0, 0x1

    .line 67
    :cond_0
    return v0
.end method

.method public setNeedSaveResumePos(Z)V
    .locals 0
    .param p1, "isNeedSaveResumePos"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->mIsNeedSaveResumePos:Z

    .line 50
    return-void
.end method

.method public setupSideSyncStream(Landroid/content/Intent;)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 85
    const/4 v1, 0x0

    .line 86
    .local v1, "title":Ljava/lang/String;
    const-string v6, "title_of_sidesync"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    const-string v6, "WEBMEDIA"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 89
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setTitleofSideSync(Ljava/lang/String;)V

    .line 90
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    .line 91
    const-string v5, "SideSyncInfo"

    const-string v6, "setupSideSyncStream WEBMEDIA type SideSync!!!"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_0
    return v4

    .line 93
    :cond_0
    if-eqz v1, :cond_2

    .line 94
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setTitleofSideSync(Ljava/lang/String;)V

    .line 95
    const-string v6, "SideSyncInfo"

    const-string v7, "setupSideSyncStream mSideSyncInfo.setTitleofSideSync!!!"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    const-string v6, "resumePos"

    const-wide/16 v8, -0x1

    invoke-virtual {p1, v6, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 98
    .local v2, "resumePos":J
    const-string v6, "needSaveResumePos"

    invoke-virtual {p1, v6, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 100
    .local v0, "needSaveResumePos":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 101
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 102
    const-string v5, "SideSyncInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setupSideSyncStream() setResumePosition "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setNeedSaveResumePos(Z)V

    .line 106
    const-string v5, "SideSyncInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setupSideSyncStream() needSaveResumePos "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    goto :goto_0

    .line 111
    .end local v0    # "needSaveResumePos":Z
    .end local v2    # "resumePos":J
    :cond_2
    const-string v4, "SideSyncInfo"

    const-string v6, "setupSideSyncStream Not From SideSync!!!"

    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    move v4, v5

    .line 113
    goto :goto_0
.end method
