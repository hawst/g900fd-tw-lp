.class public Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;
.super Ljava/lang/Object;
.source "VideoPlayListUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;


# instance fields
.field private mBucketId:I

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContext:Landroid/content/Context;

.field private mCurIdx:I

.field private mIdArrayListCnt:I

.field private mPlayList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

.field private mSearchStr:Ljava/lang/String;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mUriArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    .line 41
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    .line 43
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;
    .locals 2

    .prologue
    .line 58
    const-class v1, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mInstance:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mInstance:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    .line 61
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mInstance:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized createPlayArrayList()V
    .locals 18

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    const-string v3, "createPlayArrayList"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 200
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 89
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFileBrowsableMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 90
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    const-string v3, "createPlayArrayList. FileBrowsableMode."

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 93
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 98
    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    if-nez v2, :cond_3

    .line 99
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    .line 101
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 103
    const/4 v9, 0x0

    .line 104
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x1

    .line 106
    .local v8, "ListType":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_4

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getListType()I

    move-result v8

    .line 110
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList() (from ServiceUtil) ListType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isVideoList()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 113
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v2, :cond_5

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 117
    :cond_5
    const/16 v2, 0x9

    if-ne v8, v2, :cond_9

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v9

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    invoke-virtual {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v9

    .line 121
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v2, :cond_6

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    invoke-virtual {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v9

    .line 140
    :cond_6
    :goto_1
    if-eqz v9, :cond_10

    :try_start_3
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_10

    .line 141
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    .line 143
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v2, :cond_7

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 147
    :cond_7
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-ge v11, v2, :cond_f

    .line 148
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    .line 149
    .local v14, "id":J
    const/4 v13, 0x0

    .line 151
    .local v13, "uri":Landroid/net/Uri;
    const-wide/16 v2, 0x0

    cmp-long v2, v14, v2

    if-ltz v2, :cond_8

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v2, v9}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 153
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    .line 162
    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList. video id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", uri : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 147
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 125
    .end local v11    # "i":I
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v14    # "id":J
    :cond_9
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v9

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    invoke-virtual {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 128
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v2, :cond_6

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    invoke-virtual {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto/16 :goto_1

    .line 132
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isExternal()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursorExternalType(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto/16 :goto_1

    .line 134
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    const/4 v6, 0x0

    const/16 v7, 0xb

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;ZI)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v9

    goto/16 :goto_1

    .line 154
    .restart local v11    # "i":I
    .restart local v13    # "uri":Landroid/net/Uri;
    .restart local v14    # "id":J
    :cond_c
    :try_start_5
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v2, v9}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 155
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_3

    .line 156
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 157
    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->ALLSHARE_DB_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_3

    .line 159
    :cond_e
    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-static {v2, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v13

    goto/16 :goto_3

    .line 167
    .end local v13    # "uri":Landroid/net/Uri;
    .end local v14    # "id":J
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    .line 168
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList. size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 173
    .end local v11    # "i":I
    :cond_10
    if-eqz v9, :cond_11

    .line 174
    :try_start_6
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v9, 0x0

    .line 179
    :cond_11
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isDirectDMC()Z

    move-result v2

    if-eqz v2, :cond_14

    .line 180
    invoke-static {}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->getInstance()Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->getUriList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/net/Uri;

    .line 181
    .local v17, "videoUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_5

    .line 170
    .end local v12    # "i$":Ljava/util/Iterator;
    .end local v17    # "videoUri":Landroid/net/Uri;
    :catch_0
    move-exception v10

    .line 171
    .local v10, "e":Ljava/lang/NumberFormatException;
    :try_start_7
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList - NumberFormatException :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 173
    if-eqz v9, :cond_11

    .line 174
    :try_start_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v9, 0x0

    goto :goto_4

    .line 173
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :catchall_1
    move-exception v2

    if-eqz v9, :cond_12

    .line 174
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 175
    const/4 v9, 0x0

    :cond_12
    throw v2

    .line 183
    .restart local v12    # "i$":Ljava/util/Iterator;
    :cond_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    .line 184
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList. size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    .end local v12    # "i$":Ljava/util/Iterator;
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 188
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    const-string v3, "createPlayArrayList. isFromGallerySecureLock : true"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getUriArray()Ljava/util/ArrayList;

    move-result-object v16

    .line 191
    .local v16, "uriArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v16, :cond_15

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 193
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    .line 195
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createPlayArrayList. size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .end local v16    # "uriArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setCurFileIndex()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0
.end method

.method public getBucketID()I
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    return v0
.end method

.method public getCurIdx()I
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    return v0
.end method

.method public getPlayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTotalVideoFileCnt()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    return v0
.end method

.method public getUriArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getUriCurrentIdx()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 255
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 258
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeUri()V
    .locals 1

    .prologue
    .line 262
    const/16 v0, -0x64

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->removeUri(I)V

    .line 263
    return-void
.end method

.method public removeUri(I)V
    .locals 2
    .param p1, "idx"    # I

    .prologue
    .line 266
    const/16 v0, -0x64

    if-ne p1, v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 271
    :goto_0
    return-void

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setBucketID(I)V
    .locals 0
    .param p1, "bucketId"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    .line 80
    return-void
.end method

.method public setCurFileIndex()V
    .locals 4

    .prologue
    .line 203
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 204
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setCurFileIndex() - total size is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mPlayList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 210
    .local v0, "newIdx":I
    if-ltz v0, :cond_1

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    if-le v0, v1, :cond_2

    .line 211
    :cond_1
    const/4 v0, 0x0

    .line 213
    :cond_2
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    goto :goto_0
.end method

.method public setFirstItemIndex()V
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 236
    return-void
.end method

.method public setInfo(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    .line 67
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 68
    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 70
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 71
    return-void
.end method

.method public setLastItemIndex()V
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 232
    return-void
.end method

.method public setNextItemIndex()V
    .locals 2

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 226
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    .line 227
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 228
    :cond_0
    return-void
.end method

.method public setPlayListInfo(Ljava/lang/String;I)V
    .locals 0
    .param p1, "searchStr"    # Ljava/lang/String;
    .param p2, "bucketId"    # I

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mSearchStr:Ljava/lang/String;

    .line 75
    iput p2, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mBucketId:I

    .line 76
    return-void
.end method

.method public setPrevItemIndex()V
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 219
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    if-gez v0, :cond_0

    .line 220
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mIdArrayListCnt:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mCurIdx:I

    .line 221
    :cond_0
    return-void
.end method

.method public setUriArray(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    const-string v5, "VideoFileInfo : setUriArray ()"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 277
    .local v0, "cR":Landroid/content/ContentResolver;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    .line 278
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 283
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 284
    .local v3, "uri":Landroid/net/Uri;
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setUriArray () : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 286
    .local v2, "type":Ljava/lang/String;
    const-string v4, "video"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 287
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setUriArray () : add - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 279
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "type":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    if-nez v4, :cond_0

    .line 280
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->mUriArray:Ljava/util/ArrayList;

    goto :goto_0

    .line 291
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    return-void
.end method
