.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initCtrlButton(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 902
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "hoverWin"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const v6, 0x7f0a0089

    .line 905
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v5, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 906
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v4

    if-nez v4, :cond_1

    .line 907
    const v2, 0x7f030009

    .line 909
    .local v2, "layout":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object v3, v4

    check-cast v3, Landroid/widget/TextView;

    .line 910
    .local v3, "view":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileExternal()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isContentExternal()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 912
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPrevFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 931
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 933
    .end local v2    # "layout":I
    .end local v3    # "view":Landroid/widget/TextView;
    :cond_1
    const/4 v4, 0x1

    return v4

    .line 913
    .restart local v2    # "layout":I
    .restart local v3    # "view":Landroid/widget/TextView;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 914
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->getContentTitle(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;

    move-result-object v0

    .line 915
    .local v0, "fileTitle":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 916
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 918
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 920
    .end local v0    # "fileTitle":Ljava/lang/String;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 921
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v0

    .line 922
    .restart local v0    # "fileTitle":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v1

    .line 923
    .local v1, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isFromStore()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isTrailer()Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 924
    :cond_6
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 926
    :cond_7
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 928
    .end local v0    # "fileTitle":Ljava/lang/String;
    .end local v1    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
