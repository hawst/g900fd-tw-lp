.class public Lcom/sec/android/app/videoplayer/view/TVOutView;
.super Ljava/lang/Object;
.source "TVOutView.java"


# instance fields
.field private mBlack:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mTVOutIcon:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    .line 15
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mContext:Landroid/content/Context;

    .line 20
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mContext:Landroid/content/Context;

    .line 21
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x8

    const/4 v5, -0x1

    const/4 v4, -0x2

    .line 24
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 51
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 26
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_2

    .line 27
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    .line 28
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 29
    .local v0, "emptyViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 30
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    const v3, 0x7f070009

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 31
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 34
    .end local v0    # "emptyViewParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_3

    .line 35
    new-instance v2, Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    .line 36
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 38
    .local v1, "wfdParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 40
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    const v3, 0x7f020047

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 42
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 43
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 46
    .end local v1    # "wfdParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    .line 48
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_0

    .line 49
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 83
    :cond_1
    return-void
.end method

.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 75
    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 89
    :cond_0
    return v0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mParentView:Landroid/widget/RelativeLayout;

    .line 99
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    const v1, 0x7f020047

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mBlack:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/TVOutView;->mTVOutIcon:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 67
    :cond_2
    return-void
.end method
