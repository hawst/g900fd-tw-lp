.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;
.super Ljava/lang/Object;
.source "ChangePlayerScanGuidePopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# static fields
.field private static sChangePlayerScanGuidePopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;


# instance fields
.field private mCheckbox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mExceptionCase:I

.field private mHandler:Landroid/os/Handler;

.field private mPopupView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->sChangePlayerScanGuidePopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .prologue
    .line 23
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private create()V
    .locals 10

    .prologue
    const/4 v9, 0x7

    const/16 v8, 0x8

    .line 97
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 99
    .local v3, "popup":Landroid/app/AlertDialog$Builder;
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    if-eq v6, v9, :cond_0

    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    if-ne v6, v8, :cond_3

    .line 100
    :cond_0
    const v6, 0x7f0a0114

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 105
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 107
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030002

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    .line 109
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    const v7, 0x7f0d000c

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 111
    .local v5, "text":Landroid/widget/TextView;
    const/4 v4, 0x0

    .line 113
    .local v4, "resid":I
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    packed-switch v6, :pswitch_data_0

    .line 147
    :goto_1
    :pswitch_0
    if-eqz v4, :cond_1

    .line 148
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(I)V

    .line 151
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    const v7, 0x7f0d000d

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 153
    .local v1, "cl":Landroid/widget/LinearLayout;
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    if-eq v6, v9, :cond_6

    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    if-eq v6, v8, :cond_6

    .line 155
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    const v7, 0x7f0d000e

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    .line 156
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 158
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v6, v7}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    const v7, 0x7f0d000f

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    .local v0, "checkText":Landroid/widget/TextView;
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    .end local v0    # "checkText":Landroid/widget/TextView;
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    const v7, 0x7f0a00dd

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v3, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 216
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    if-ne v6, v8, :cond_2

    .line 217
    const v6, 0x7f0a0026

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$4;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v3, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 225
    :cond_2
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 236
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 242
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mPopupView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 244
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    .line 245
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 246
    return-void

    .line 102
    .end local v1    # "cl":Landroid/widget/LinearLayout;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v4    # "resid":I
    .end local v5    # "text":Landroid/widget/TextView;
    :cond_3
    const v6, 0x7f0a000e

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_0

    .line 115
    .restart local v2    # "inflater":Landroid/view/LayoutInflater;
    .restart local v4    # "resid":I
    .restart local v5    # "text":Landroid/widget/TextView;
    :pswitch_1
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v6, :cond_4

    .line 116
    const v4, 0x7f0a0118

    goto/16 :goto_1

    .line 118
    :cond_4
    const v4, 0x7f0a0116

    .line 120
    goto/16 :goto_1

    .line 122
    :pswitch_2
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v6, :cond_5

    .line 123
    const v4, 0x7f0a0117

    goto/16 :goto_1

    .line 125
    :cond_5
    const v4, 0x7f0a0115

    .line 127
    goto/16 :goto_1

    .line 129
    :pswitch_3
    const v4, 0x7f0a0113

    .line 130
    goto/16 :goto_1

    .line 132
    :pswitch_4
    const v4, 0x7f0a0110

    .line 133
    goto/16 :goto_1

    .line 135
    :pswitch_5
    const v4, 0x7f0a010f

    .line 136
    goto/16 :goto_1

    .line 138
    :pswitch_6
    const v4, 0x7f0a0112

    .line 139
    goto/16 :goto_1

    .line 141
    :pswitch_7
    const v4, 0x7f0a0111

    .line 142
    goto/16 :goto_1

    .line 176
    .restart local v1    # "cl":Landroid/widget/LinearLayout;
    :cond_6
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_2

    .line 113
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->sChangePlayerScanGuidePopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->sChangePlayerScanGuidePopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    .line 45
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->sChangePlayerScanGuidePopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 78
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 72
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;Landroid/os/Handler;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "type"    # I

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;

    .line 51
    iput p3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I

    .line 52
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 57
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 58
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->create()V

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 65
    :cond_1
    return-void
.end method
