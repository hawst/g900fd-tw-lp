.class public Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# static fields
.field private static final ID:I = 0x0

.field private static final TAG:Ljava/lang/String; = "VideoBrightnessDialog"


# instance fields
.field private bLocalReceiverRegistered:Z

.field private final transient cancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

.field private mAutoBrightnessText:Landroid/widget/TextView;

.field private mAutoCheckbox:Landroid/widget/CheckBox;

.field mAutoValText:Landroid/widget/TextView;

.field private mBrightProgressBar:Landroid/widget/SeekBar;

.field private mBrightProgressBarAuto:Landroid/widget/SeekBar;

.field private mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

.field private mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

.field mBrightnessText:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mObserverAutoBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightnessMode:Landroid/database/ContentObserver;

.field mRoot:Landroid/view/View;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mWindow:Landroid/view/Window;

.field private mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bUtil"    # Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->bLocalReceiverRegistered:Z

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    .line 60
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    .line 61
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    .line 62
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    .line 112
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 124
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->cancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 133
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 450
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$12;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 478
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$13;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 555
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$14;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 65
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    .line 66
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;

    .line 67
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->unregisterContentObserver()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->unregisterLocalReceiver()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->toggleBrightProgressBar()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessContentDescription()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->revertBrightness()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    return-object v0
.end method

.method private registerContentObserver()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 210
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-nez v2, :cond_0

    .line 212
    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 235
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "screen_brightness_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 236
    .local v1, "tmp":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 237
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightnessMode is now registered"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "tmp":Landroid/net/Uri;
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    if-nez v2, :cond_1

    .line 243
    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$6;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 248
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 249
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v2, "screen_brightness"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 250
    .restart local v1    # "tmp":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 251
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightness is now registered"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "tmp":Landroid/net/Uri;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-nez v2, :cond_2

    .line 257
    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 266
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v2, "auto_brightness_detail"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 267
    .restart local v1    # "tmp":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v4, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 268
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverAutoBrightness is now registered"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "tmp":Landroid/net/Uri;
    :goto_2
    return-void

    .line 239
    :cond_0
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightnessMode already exists"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :cond_1
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverBrightness already exists"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 270
    :cond_2
    const-string v2, "VideoBrightnessDialog"

    const-string v3, "registerContentObserver - mObserverAutoBrightness already exists"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private registerLocalReceiver()V
    .locals 3

    .prologue
    .line 174
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 178
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->bLocalReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 179
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->bLocalReceiverRegistered:Z

    .line 181
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 182
    .local v0, "Localfilter":Landroid/content/IntentFilter;
    const-string v1, "videoplayer.critical.low.batt"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 184
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto :goto_0
.end method

.method private revertBrightness()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->revertBrightness(Landroid/content/Context;)V

    .line 506
    :cond_0
    return-void
.end method

.method private setAutoBrightnessContentDescription()V
    .locals 5

    .prologue
    .line 514
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 515
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 516
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a014c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->brightnessValueLocale()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 519
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-void
.end method

.method private setAutoBrightnessDetailLevelText()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->brightnessValueLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 511
    :cond_0
    return-void
.end method

.method private setBrightProgressBarEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 522
    move v0, p1

    .line 523
    .local v0, "val":Z
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_0

    .line 524
    const/4 v0, 0x1

    .line 526
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 527
    return-void
.end method

.method private setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V
    .locals 7
    .param p1, "dialogBuilder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 299
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03001c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0117

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d011a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d011b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0119

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d011c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    .line 306
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d011e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    .line 307
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0120

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_1

    .line 313
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMode(I)V

    .line 317
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0118

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    .line 318
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 319
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 320
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setFocusableInTouchMode(Z)V

    .line 322
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_2

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 334
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v1, :cond_3

    .line 448
    :goto_1
    return-void

    .line 325
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessLevelLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 337
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightnessSupported()Z

    move-result v1

    if-nez v1, :cond_4

    .line 338
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 339
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 340
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 343
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_7

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->savePrevSystemBrightnessValuesInLocal(Landroid/content/Context;)V

    .line 346
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 347
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 348
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 349
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_5

    .line 350
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 351
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 352
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a0024

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 364
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessLayout:Landroid/widget/RelativeLayout;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$8;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a001b

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 384
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V

    .line 385
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessContentDescription()V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 414
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    if-eqz v1, :cond_6

    .line 415
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 416
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 417
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mbrightnessChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 421
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    if-eqz v1, :cond_7

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v2

    div-int/lit8 v2, v2, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 424
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mbrightnessAutoChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 428
    :cond_7
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_8

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 429
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 430
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 431
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 433
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 436
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mRoot:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 438
    const v1, 0x7f0a0055

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$10;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 443
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$11;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    invoke-virtual {p1, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 355
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 356
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 357
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_5

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 359
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v1, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v2, 0x7f0a0025

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2
.end method

.method private toggleBrightProgressBar()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 530
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 539
    :goto_0
    return-void

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoBrightnessText:Landroid/widget/TextView;

    const v1, 0x7f0a0025

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method private unregisterContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 275
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 278
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 280
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 281
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver - mObserverBrightnessMode is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 286
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 287
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver - mObserverBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_2

    .line 291
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 292
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 293
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "unregisterContentObserver - mObserverAutoBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    :cond_2
    return-void
.end method

.method private unregisterLocalReceiver()V
    .locals 2

    .prologue
    .line 202
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->bLocalReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->bLocalReceiverRegistered:Z

    .line 206
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 207
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 98
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->revertBrightness()V

    .line 93
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 108
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSensorChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 542
    const-string v0, "VideoBrightnessDialog"

    const-string v1, "onSensorChanged  "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 549
    :goto_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V

    .line 550
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 551
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 553
    :cond_0
    return-void

    .line 547
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    goto :goto_0
.end method

.method public show()V
    .locals 3

    .prologue
    .line 71
    const-string v1, "VideoBrightnessDialog"

    const-string v2, "show()!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const-string v2, "VideoBrightnessDialog"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 74
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 78
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V

    .line 79
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->registerContentObserver()V

    .line 80
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->registerLocalReceiver()V

    .line 81
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->cancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 86
    return-void
.end method
