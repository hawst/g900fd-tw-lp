.class public Lcom/sec/android/app/videoplayer/preview/PreviewManager;
.super Ljava/lang/Object;
.source "PreviewManager.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# static fields
.field public static final TAG:Ljava/lang/String;

.field private static sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

.field private mFileId:J

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->TAG:Ljava/lang/String;

    .line 24
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/preview/PreviewManager;Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 101
    return-void
.end method

.method public changeView()V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->TAG:Ljava/lang/String;

    const-string v1, "changeView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->changeView()V

    .line 89
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->dismiss()V

    .line 96
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->isShowing()Z

    move-result v0

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    .line 36
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    new-instance v1, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager$1;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    new-instance v1, Lcom/sec/android/app/videoplayer/preview/PreviewManager$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager$2;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewManager;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mContext:Landroid/content/Context;

    .line 54
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    return-object v0
.end method

.method public setFileId(J)Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    .locals 1
    .param p1, "fileId"    # J

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mFileId:J

    .line 60
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    return-object v0
.end method

.method public setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mHandler:Landroid/os/Handler;

    .line 66
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->sPreviewMgr:Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    return-object v0
.end method

.method public show(I)V
    .locals 4
    .param p1, "resumePos"    # I

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 76
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mFileId:J

    invoke-virtual {v1, v2, v3, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->initData(JI)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->mDialog:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->show()V

    .line 79
    sget-object v1, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->TAG:Ljava/lang/String;

    const-string v2, "show()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
