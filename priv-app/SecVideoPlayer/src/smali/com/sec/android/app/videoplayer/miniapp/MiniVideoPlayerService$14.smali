.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 2097
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdated(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2099
    if-eqz p1, :cond_1

    .line 2100
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->openVideo(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/net/Uri;)V

    .line 2105
    :cond_0
    :goto_0
    return-void

    .line 2102
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$14;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2, v2}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0
.end method
