.class public Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
.super Ljava/lang/Object;
.source "VideoBrightnessUtil.java"


# static fields
.field private static final AUTO_BRIGHTNESS_FLAG:I = 0x0

.field private static final AUTO_BRIGHTNESS_LEVEL:I = 0x1

.field private static final AUTO_BRIGHT_DETAIL_LEVEL_STEP_VAL:I = 0x14

.field private static final MANUAL_BRIGHTNESS_LEVEL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VideoBrightnessUtil"

.field private static mAutoBrightSupported:I


# instance fields
.field private mAutoBrightDetailLevel:I

.field private mBrightnessRange:I

.field private mContext:Landroid/content/Context;

.field private mHalfLevel:I

.field private mMaxAutoBrightness:I

.field private mMaxBrightness:I

.field private mMinBrightness:I

.field private mPrevBrightnessVal:[I

.field private mSystemAutoBrightness:Z

.field private mSystemBrightnessLevel:I

.field private mWindow:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 38
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 40
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 42
    const/16 v0, 0xeb

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 44
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 59
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getMaximumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getMinimumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 62
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 63
    sput v2, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 65
    return-void

    .line 46
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "window"    # Landroid/view/Window;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 31
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 38
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 40
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 42
    const/16 v0, 0xeb

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 44
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    .line 71
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getMaximumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getMinimumScreenBrightnessSetting()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    .line 75
    sput v2, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 77
    return-void

    .line 46
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private getAutoBrightDetailTextString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 318
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v1, "text":Ljava/lang/StringBuilder;
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getMaxBrightness()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mHalfLevel:I

    .line 324
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mHalfLevel:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x41a00000    # 20.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    .line 328
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    add-int/lit8 v0, v2, 0x5

    .line 333
    .local v0, "tempText":I
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 336
    .end local v0    # "tempText":I
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 188
    const/4 v0, -0x1

    .line 189
    .local v0, "autobright":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightnessSupported()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 191
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness_mode"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 197
    :cond_0
    :goto_0
    if-ne v0, v3, :cond_1

    :goto_1
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 200
    :try_start_1
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_2

    .line 201
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 207
    :goto_2
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSystemBrightenessLevel. mSystemBrightnessLevel = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    return-void

    .line 192
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 197
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 203
    :cond_2
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I
    :try_end_2
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 204
    :catch_1
    move-exception v2

    .line 205
    .local v2, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v2}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method private getWindowBrightnessValue(I)F
    .locals 2
    .param p1, "brightness"    # I

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int/2addr v0, p1

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxBrightness:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static resetWindowBrightness(Landroid/view/Window;)V
    .locals 2
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 108
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const/high16 v1, -0x40800000    # -1.0f

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 110
    invoke-virtual {p0, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 111
    return-void
.end method

.method private setWindowBrightness()V
    .locals 6

    .prologue
    .line 141
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 146
    .local v1, "config":Landroid/content/res/Configuration;
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowBrightness. mSystemBrightnessLevel = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v3, :cond_2

    iget v3, v1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_2

    .line 149
    const-string v3, "VideoBrightnessUtil"

    const-string v4, "Folder closed!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getWindowBrightnessValue(I)F

    move-result v0

    .line 153
    .local v0, "brightVal":F
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 154
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 155
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 157
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_0

    .line 158
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness_mode"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 160
    .end local v0    # "brightVal":F
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v3, :cond_3

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v3, :cond_3

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 163
    :cond_3
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getWindowBrightnessValue(I)F

    move-result v0

    .line 165
    .restart local v0    # "brightVal":F
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 166
    .restart local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 169
    const-string v3, "VideoBrightnessUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setWindowBrightness. brightVal = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public brightnessValueLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getAutoBrightDetailTextString()Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "brightnessLocale":Ljava/lang/String;
    const/4 v1, 0x0

    .line 311
    .local v1, "showBrightness":Ljava/lang/String;
    move-object v1, v0

    .line 314
    return-object v1
.end method

.method public getAutoBrightDetailLevelInSystemValue()I
    .locals 3

    .prologue
    .line 340
    const/4 v0, 0x0

    .line 341
    .local v0, "retVal":I
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    if-nez v1, :cond_0

    .line 342
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mHalfLevel:I

    .line 348
    :goto_0
    return v0

    .line 343
    :cond_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    if-lez v1, :cond_1

    .line 344
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mHalfLevel:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    mul-int/lit8 v2, v2, 0x14

    add-int v0, v1, v2

    goto :goto_0

    .line 346
    :cond_1
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightDetailLevel:I

    mul-int/lit8 v1, v1, 0x14

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mHalfLevel:I

    add-int v0, v1, v2

    goto :goto_0
.end method

.method public getBrightnessRange()I
    .locals 3

    .prologue
    .line 243
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    .line 244
    const-string v0, "VideoBrightnessUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBrightnessRange = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 248
    :goto_0
    return v0

    .line 247
    :cond_0
    const-string v0, "VideoBrightnessUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getBrightnessRange = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    goto :goto_0
.end method

.method public getDefaultScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 283
    const/16 v1, 0x6e

    .line 284
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 286
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getDefaultScreenBrightnessSetting()I

    move-result v1

    .line 289
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getMaxBrightness()I
    .locals 1

    .prologue
    .line 114
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    .line 115
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    .line 117
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    goto :goto_0
.end method

.method public getMaximumScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 263
    const/16 v1, 0xff

    .line 264
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 265
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 266
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMaximumScreenBrightnessSetting()I

    move-result v1

    .line 269
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getMinimumScreenBrightnessSetting()I
    .locals 4

    .prologue
    .line 273
    const/16 v1, 0x28

    .line 274
    .local v1, "val":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 275
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    const-string v3, "power"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 276
    .local v0, "power":Landroid/os/PowerManager;
    invoke-virtual {v0}, Landroid/os/PowerManager;->getMinimumScreenBrightnessSetting()I

    move-result v1

    .line 279
    .end local v0    # "power":Landroid/os/PowerManager;
    :cond_0
    return v1
.end method

.method public getSystemBrightnessLevel()I
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 184
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v0
.end method

.method public isAutoBrightness()Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevelandSaveinLocal(Landroid/content/Context;)V

    .line 179
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    return v0
.end method

.method public isAutoBrightnessSupported()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 92
    sget v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isLightSensorAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sput v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    .line 96
    :cond_0
    sget v0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mAutoBrightSupported:I

    if-ne v0, v1, :cond_2

    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 93
    goto :goto_0

    :cond_2
    move v1, v2

    .line 96
    goto :goto_1
.end method

.method public isLightSensorAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    .line 80
    .local v0, "available":Z
    const-string v5, "sensor"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    .line 81
    .local v3, "sensorMgr":Landroid/hardware/SensorManager;
    const/4 v5, -0x1

    invoke-virtual {v3, v5}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v2

    .line 82
    .local v2, "sensorList":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 83
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/Sensor;

    invoke-virtual {v5}, Landroid/hardware/Sensor;->getType()I

    move-result v4

    .line 84
    .local v4, "sensorType":I
    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 82
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    .end local v4    # "sensorType":I
    :cond_1
    return v0
.end method

.method public resetWindowBrightness()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "VideoBrightnessUtil"

    const-string v1, "call resetWindowBrightness"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mWindow:Landroid/view/Window;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 104
    :cond_0
    return-void
.end method

.method public revertBrightness(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 369
    const-string v0, "VideoBrightnessUtil"

    const-string v1, "revertBrightness E:"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    if-nez p1, :cond_1

    .line 371
    const-string v0, "VideoBrightnessUtil"

    const-string v1, "revertBrightness Context is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_2

    .line 376
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "auto_brightness_detail"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v2, v2, v4

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 378
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "screen_brightness"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v2, v2, v6

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v5

    if-ne v0, v4, :cond_3

    .line 381
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 382
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setBrightness(I)I

    goto :goto_0

    .line 386
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    aget v0, v0, v6

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setBrightness(I)I

    goto :goto_0
.end method

.method public savePrevSystemBrightnessValuesInLocal(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 352
    if-nez p1, :cond_0

    .line 353
    const-string v1, "VideoBrightnessUtil"

    const-string v2, "savePrevSystemBrightnessValuesInLocal Context is NULL!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :goto_0
    return-void

    .line 357
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    aput v1, v3, v2

    .line 359
    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_brightness_detail"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    .line 362
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mPrevBrightnessVal:[I

    const/4 v2, 0x2

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_brightness"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    sub-int/2addr v3, v4

    aput v3, v1, v2
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 363
    :catch_0
    move-exception v0

    .line 364
    .local v0, "snfe":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0

    .end local v0    # "snfe":Landroid/provider/Settings$SettingNotFoundException;
    :cond_2
    move v1, v2

    .line 357
    goto :goto_1
.end method

.method public saveSystemBrightnessLevelValueinLocal(I)I
    .locals 2
    .param p1, "val"    # I

    .prologue
    .line 253
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 254
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v0

    .line 255
    .local v0, "maxBrightness":I
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-gez v1, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 256
    :cond_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-le v1, v0, :cond_1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 257
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setWindowBrightness()V

    .line 259
    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v1
.end method

.method public setAutoBrightness(Z)V
    .locals 3
    .param p1, "setAuto"    # Z

    .prologue
    .line 293
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_brightness_mode"

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 296
    return-void

    .line 294
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBrightness(I)I
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 125
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    if-gez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 128
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v0, :cond_2

    .line 129
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    if-le v0, v1, :cond_1

    .line 130
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMaxAutoBrightness:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    .line 135
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setWindowBrightness()V

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setSystemBrightnessLevel()V

    .line 137
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    return v0

    .line 132
    :cond_2
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    if-le v0, v1, :cond_1

    .line 133
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mBrightnessRange:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    goto :goto_0
.end method

.method public setSystemBrightnessLevel()V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setSystemBrightnessLevel(Landroid/content/Context;)V

    .line 213
    :cond_0
    return-void
.end method

.method public setSystemBrightnessLevel(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 216
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AUTO_BRIGHTNESS_DETAIL:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v4, :cond_0

    .line 218
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "auto_brightness_detail"

    iget v6, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v1

    .line 220
    .local v1, "doe":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 224
    .end local v1    # "doe":Ljava/lang/SecurityException;
    :cond_0
    :try_start_1
    const-string v4, "power"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 225
    .local v2, "power":Landroid/os/PowerManager;
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    iget v5, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mMinBrightness:I

    add-int v0, v4, v5

    .line 226
    .local v0, "brightness":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 228
    .local v3, "resolver":Landroid/content/ContentResolver;
    if-eqz v2, :cond_1

    .line 229
    invoke-virtual {v2, v0}, Landroid/os/PowerManager;->setBacklightBrightness(I)V

    .line 231
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemAutoBrightness:Z

    if-eqz v4, :cond_2

    .line 232
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "screen_brightness_mode"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 234
    :cond_2
    const-string v4, "screen_brightness"

    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    .line 238
    .end local v0    # "brightness":I
    .end local v2    # "power":Landroid/os/PowerManager;
    .end local v3    # "resolver":Landroid/content/ContentResolver;
    :goto_1
    const-string v4, "VideoBrightnessUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setSystemBrightnessLevel : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->mSystemBrightnessLevel:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 235
    :catch_1
    move-exception v1

    .line 236
    .restart local v1    # "doe":Ljava/lang/SecurityException;
    invoke-virtual {v1}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1
.end method
