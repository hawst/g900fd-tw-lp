.class public Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;
.super Ljava/lang/Object;
.source "VideoCaptureUtil.java"


# static fields
.field public static final CENTER:I = 0x1

.field private static final FULL_VIDEO_RES:I = -0x270f

.field public static final LEFT:I = 0x0

.field public static final RIGHT:I = 0x2

.field public static final STORAGE_LIMIT:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "VideoCaptureUtil"

.field private static final TIME_DIFF:I = 0x1f4


# instance fields
.field private mContext:Landroid/content/Context;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mTime:I

    .line 33
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 35
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mContext:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mContext:Landroid/content/Context;

    .line 39
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 40
    return-void
.end method

.method private SaveImage()Landroid/graphics/Bitmap;
    .locals 20

    .prologue
    .line 58
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v17, v0

    if-nez v17, :cond_0

    .line 59
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 62
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v9

    .line 63
    .local v9, "isPlaying":Z
    if-eqz v9, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 65
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v16

    .line 66
    .local v16, "time":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v7

    .line 68
    .local v7, "image":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 70
    :cond_2
    if-nez v7, :cond_3

    const/16 v17, 0x0

    .line 133
    :goto_0
    return-object v17

    .line 72
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v6

    .line 74
    .local v6, "filePath":Ljava/lang/String;
    const/4 v8, 0x0

    .line 75
    .local v8, "imageName":Ljava/lang/String;
    const/16 v17, 0x2e

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v14

    .line 76
    .local v14, "pos":I
    const/16 v17, 0x2f

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v15

    .line 78
    .local v15, "pos1":I
    if-gt v14, v15, :cond_4

    .line 79
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v14

    .line 81
    :cond_4
    if-lez v14, :cond_5

    .line 82
    invoke-virtual {v6, v15, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 84
    :cond_5
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v18, Lcom/sec/android/app/videoplayer/common/feature/Path;->CAPTURE_INTERNAL:Ljava/lang/String;

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "_"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ".jpg"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 85
    .local v13, "path":Ljava/lang/String;
    const-string v17, "VideoCaptureUtil"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "CaptureVideo : path="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_7

    .line 88
    :cond_6
    const-string v17, "VideoCaptureUtil"

    const-string v18, "CaptureVideo : PATH is EMPTY!!! but why????"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 90
    const/4 v7, 0x0

    .line 91
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 94
    :cond_7
    const/4 v10, 0x1

    .line 97
    .local v10, "isSuccess":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/common/feature/Path;->CAPTURE_INTERNAL:Ljava/lang/String;

    .line 98
    .local v2, "dir":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 99
    .local v5, "f":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v17

    if-nez v17, :cond_8

    .line 100
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 102
    :cond_8
    const/4 v11, 0x0

    .line 103
    .local v11, "outStream":Ljava/io/FileOutputStream;
    new-instance v5, Ljava/io/File;

    .end local v5    # "f":Ljava/io/File;
    invoke-direct {v5, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 105
    .restart local v5    # "f":Ljava/io/File;
    :try_start_0
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    .end local v11    # "outStream":Ljava/io/FileOutputStream;
    .local v12, "outStream":Ljava/io/FileOutputStream;
    :try_start_1
    sget-object v17, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v18, 0x50

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v7, v0, v1, v12}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 112
    :try_start_2
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->flush()V

    .line 113
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 114
    const/4 v11, 0x0

    .line 126
    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outStream":Ljava/io/FileOutputStream;
    :goto_1
    if-eqz v10, :cond_9

    move-object/from16 v17, v7

    .line 127
    goto/16 :goto_0

    .line 115
    .end local v11    # "outStream":Ljava/io/FileOutputStream;
    .restart local v12    # "outStream":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v3

    .line 116
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 118
    :try_start_3
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_2
    move-object v11, v12

    .line 124
    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outStream":Ljava/io/FileOutputStream;
    goto :goto_1

    .line 119
    .end local v11    # "outStream":Ljava/io/FileOutputStream;
    .restart local v12    # "outStream":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v4

    .line 121
    .local v4, "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 107
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "e1":Ljava/io/IOException;
    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outStream":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v3

    .line 108
    .local v3, "e":Ljava/io/IOException;
    :goto_3
    const/4 v10, 0x0

    .line 109
    :try_start_4
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 112
    :try_start_5
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->flush()V

    .line 113
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 114
    const/4 v11, 0x0

    goto :goto_1

    .line 115
    :catch_3
    move-exception v3

    .line 116
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 118
    :try_start_6
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_1

    .line 119
    :catch_4
    move-exception v4

    .line 121
    .restart local v4    # "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 111
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v17

    .line 112
    :goto_4
    :try_start_7
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->flush()V

    .line 113
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_5

    .line 114
    const/4 v11, 0x0

    .line 123
    :goto_5
    throw v17

    .line 115
    :catch_5
    move-exception v3

    .line 116
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 118
    :try_start_8
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_5

    .line 119
    :catch_6
    move-exception v4

    .line 121
    .restart local v4    # "e1":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 129
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "e1":Ljava/io/IOException;
    :cond_9
    const-string v17, "VideoCaptureUtil"

    const-string v18, "CaptureVideo : Fail to save file!!! recycle bitmap"

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    .line 131
    const/4 v7, 0x0

    .line 133
    const/16 v17, 0x0

    goto/16 :goto_0

    .line 111
    .end local v11    # "outStream":Ljava/io/FileOutputStream;
    .restart local v12    # "outStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v17

    move-object v11, v12

    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outStream":Ljava/io/FileOutputStream;
    goto :goto_4

    .line 107
    .end local v11    # "outStream":Ljava/io/FileOutputStream;
    .restart local v12    # "outStream":Ljava/io/FileOutputStream;
    :catch_7
    move-exception v3

    move-object v11, v12

    .end local v12    # "outStream":Ljava/io/FileOutputStream;
    .restart local v11    # "outStream":Ljava/io/FileOutputStream;
    goto :goto_3
.end method

.method private getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v0, :cond_0

    .line 139
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getTimeValue(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mTime:I

    .line 193
    .local v0, "time":I
    packed-switch p1, :pswitch_data_0

    .line 210
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 195
    :pswitch_1
    add-int/lit16 v0, v0, -0x3e8

    .line 196
    if-gez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :pswitch_2
    add-int/lit16 v0, v0, -0x1f4

    .line 201
    if-gez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public CaptureVideo()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->SaveImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public CaptureVideo(II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "type"    # I
    .param p2, "time"    # I

    .prologue
    .line 44
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v1, :cond_0

    .line 45
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 47
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "filePath":Ljava/lang/String;
    iput p2, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mTime:I

    .line 49
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->getTimeValue(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->captureFrame(Ljava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public captureFrame(IILjava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "filePath"    # Ljava/lang/String;
    .param p4, "time"    # J

    .prologue
    .line 150
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v7, :cond_0

    .line 151
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 154
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v6

    .line 155
    .local v6, "width":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    .line 157
    .local v1, "height":I
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "captureFrame :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    if-ltz v6, :cond_1

    if-gez v1, :cond_2

    :cond_1
    const/4 v3, 0x0

    .line 187
    :goto_0
    return-object v3

    .line 160
    :cond_2
    const-wide/16 v4, 0x0

    .line 162
    .local v4, "thumbnailTime":J
    const-wide/16 v8, 0x0

    cmp-long v7, p4, v8

    if-lez v7, :cond_3

    .line 163
    const-wide/16 v8, 0x3e8

    mul-long v4, p4, v8

    .line 165
    :cond_3
    const/4 v3, 0x0

    .line 167
    .local v3, "thumb":Landroid/graphics/Bitmap;
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "captureFrame thumbnailTime  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 171
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 172
    const/16 v7, -0x270f

    if-ne p1, v7, :cond_4

    const/16 v7, -0x270f

    if-eq p2, v7, :cond_5

    .line 173
    :cond_4
    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-virtual {v2, p1, p2, v7, v8}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 174
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {v2, v4, v5, v7}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(JI)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 181
    :try_start_1
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 182
    const/4 v2, 0x0

    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RuntimeException occured  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 175
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 176
    .restart local v0    # "ex":Ljava/lang/RuntimeException;
    :try_start_2
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RuntimeException occured  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 181
    :try_start_3
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    .line 182
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 183
    :catch_2
    move-exception v0

    .line 184
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RuntimeException occured  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 177
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catch_3
    move-exception v0

    .line 178
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_4
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception occured  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 181
    :try_start_5
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_4

    .line 182
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 183
    :catch_4
    move-exception v0

    .line 184
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v7, "VideoCaptureUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RuntimeException occured  :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 180
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v7

    .line 181
    :try_start_6
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_5

    .line 182
    const/4 v2, 0x0

    .line 185
    :goto_1
    throw v7

    .line 183
    :catch_5
    move-exception v0

    .line 184
    .restart local v0    # "ex":Ljava/lang/RuntimeException;
    const-string v8, "VideoCaptureUtil"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "RuntimeException occured  :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public captureFrame(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    const/16 v1, -0x270f

    .line 146
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-wide v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/util/VideoCaptureUtil;->captureFrame(IILjava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public enoughSpace()Z
    .locals 8

    .prologue
    .line 214
    new-instance v0, Landroid/os/StatFs;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 215
    .local v0, "stat":Landroid/os/StatFs;
    const-string v1, "VideoCaptureUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enoughSpace. avaliableSize : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v4

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v6

    mul-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v2

    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v4

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x100000

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
