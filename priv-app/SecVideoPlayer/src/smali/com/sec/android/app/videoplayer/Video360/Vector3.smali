.class public Lcom/sec/android/app/videoplayer/Video360/Vector3;
.super Ljava/lang/Object;
.source "Vector3.java"


# static fields
.field private static final EPSILON:F = 1.0E-7f

.field public static ZERO:Lcom/sec/android/app/videoplayer/Video360/Vector3;


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 12
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    invoke-direct {v0, v1, v1, v1}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->ZERO:Lcom/sec/android/app/videoplayer/Video360/Vector3;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 21
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "xxx"    # F
    .param p2, "yyy"    # F
    .param p3, "zzz"    # F

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 25
    iput p2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 26
    iput p3, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 27
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Vector3;)V
    .locals 1
    .param p1, "v"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 31
    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 32
    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 33
    return-void
.end method

.method public constructor <init>([F)V
    .locals 2
    .param p1, "v"    # [F

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 37
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 38
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 39
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 41
    :cond_0
    return-void
.end method

.method public static AngleBetween(Lcom/sec/android/app/videoplayer/Video360/Vector3;Lcom/sec/android/app/videoplayer/Video360/Vector3;)F
    .locals 3
    .param p0, "v1"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .param p1, "v2"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->dot(Lcom/sec/android/app/videoplayer/Video360/Vector3;)F

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->getLength()F

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->getLength()F

    move-result v2

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public add(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 5
    .param p1, "v"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 107
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    add-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public asArray()[F
    .locals 3

    .prologue
    .line 45
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 47
    .local v0, "v":[F
    const/4 v1, 0x0

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    aput v2, v0, v1

    .line 48
    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    aput v2, v0, v1

    .line 49
    const/4 v1, 0x2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    aput v2, v0, v1

    .line 50
    return-object v0
.end method

.method public asString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "s":Ljava/lang/String;
    return-object v0
.end method

.method cross(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 6
    .param p1, "vec"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v4, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v5, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method dot(Lcom/sec/android/app/videoplayer/Video360/Vector3;)F
    .locals 3
    .param p1, "vec"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 115
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public equals(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Z
    .locals 3
    .param p1, "v"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    const v2, 0x33d6bf95    # 1.0E-7f

    .line 61
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    sub-float/2addr v0, v2

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    sub-float/2addr v0, v2

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    sub-float/2addr v0, v2

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 64
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->getLengthSquared()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getLengthSquared()F
    .locals 3

    .prologue
    .line 77
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public negate()V
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 90
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 91
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 92
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->getLength()F

    move-result v0

    .line 98
    .local v0, "len":F
    const v1, 0x33d6bf95    # 1.0E-7f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 99
    const/high16 v1, 0x3f800000    # 1.0f

    div-float v0, v1, v0

    .line 100
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 101
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 102
    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 104
    :cond_0
    return-void
.end method

.method project(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 7
    .param p1, "v"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 128
    invoke-virtual {p1, p0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->dot(Lcom/sec/android/app/videoplayer/Video360/Vector3;)F

    move-result v0

    .line 129
    .local v0, "dp":F
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Vector3;->getLengthSquared()F

    move-result v1

    .line 130
    .local v1, "lenSq":F
    div-float v2, v0, v1

    .line 132
    .local v2, "s":F
    new-instance v3, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v4, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v4, v2

    iget v5, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v5, v2

    iget v6, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v6, v2

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v3
.end method

.method public scale(F)V
    .locals 1
    .param p1, "s"    # F

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    .line 83
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    .line 84
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    .line 85
    return-void
.end method

.method public subtract(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 5
    .param p1, "v"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v1, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method
