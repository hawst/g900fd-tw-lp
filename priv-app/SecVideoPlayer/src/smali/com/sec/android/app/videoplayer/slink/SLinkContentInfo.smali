.class public Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;
.super Ljava/lang/Object;
.source "SLinkContentInfo.java"


# instance fields
.field private mCaptionType:Ljava/lang/String;

.field private mCursorPosition:I

.field private mDate:Ljava/lang/Long;

.field private mDeviceID:Ljava/lang/Long;

.field private mDeviceName:Ljava/lang/String;

.field private mDeviceType:Ljava/lang/String;

.field private mDuration:J

.field private mIndexOfArray:I

.field private mResumePosition:Ljava/lang/Long;

.field private mSize:Ljava/lang/Long;

.field private mType:Ljava/lang/String;

.field private mVideoID:Ljava/lang/Long;

.field private mVideoName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput v1, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mIndexOfArray:I

    .line 22
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceID:Ljava/lang/Long;

    .line 23
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    .line 24
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    .line 25
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoID:Ljava/lang/Long;

    .line 26
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    .line 27
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mSize:Ljava/lang/Long;

    .line 28
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    .line 29
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    .line 30
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDate:Ljava/lang/Long;

    .line 31
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mResumePosition:Ljava/lang/Long;

    .line 32
    iput v1, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCursorPosition:I

    .line 33
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDuration:J

    .line 34
    return-void
.end method

.method public constructor <init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;JI)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "deviceID"    # Ljava/lang/Long;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "deviceType"    # Ljava/lang/String;
    .param p5, "videoID"    # Ljava/lang/Long;
    .param p6, "videoName"    # Ljava/lang/String;
    .param p7, "size"    # Ljava/lang/Long;
    .param p8, "type"    # Ljava/lang/String;
    .param p9, "captionType"    # Ljava/lang/String;
    .param p10, "date"    # Ljava/lang/Long;
    .param p11, "resumePosition"    # Ljava/lang/Long;
    .param p12, "duration"    # J
    .param p14, "cursorpos"    # I

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-virtual/range {p0 .. p14}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->setInfo(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;JI)V

    .line 38
    return-void
.end method


# virtual methods
.method public getCaptionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    return-object v0
.end method

.method public getCursorPosition()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCursorPosition:I

    return v0
.end method

.method public getDate()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDate:Ljava/lang/Long;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceID:Ljava/lang/Long;

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDuration:J

    return-wide v0
.end method

.method public getIndexOfArray()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mIndexOfArray:I

    return v0
.end method

.method public getResumePosition()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mResumePosition:Ljava/lang/Long;

    return-object v0
.end method

.method public getSize()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mSize:Ljava/lang/Long;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoID()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoID:Ljava/lang/Long;

    return-object v0
.end method

.method public getVideoName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    return-object v0
.end method

.method public setInfo(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;JI)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "deviceID"    # Ljava/lang/Long;
    .param p3, "deviceName"    # Ljava/lang/String;
    .param p4, "deviceType"    # Ljava/lang/String;
    .param p5, "videoID"    # Ljava/lang/Long;
    .param p6, "videoName"    # Ljava/lang/String;
    .param p7, "size"    # Ljava/lang/Long;
    .param p8, "type"    # Ljava/lang/String;
    .param p9, "captionType"    # Ljava/lang/String;
    .param p10, "date"    # Ljava/lang/Long;
    .param p11, "resumePosition"    # Ljava/lang/Long;
    .param p12, "duration"    # J
    .param p14, "cursorpos"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mIndexOfArray:I

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceID:Ljava/lang/Long;

    .line 43
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    .line 45
    iput-object p5, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoID:Ljava/lang/Long;

    .line 46
    iput-object p6, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    .line 47
    iput-object p7, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mSize:Ljava/lang/Long;

    .line 48
    iput-object p8, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    .line 49
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mResumePosition:Ljava/lang/Long;

    .line 50
    move-wide/from16 v0, p12

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDuration:J

    .line 51
    move/from16 v0, p14

    iput v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCursorPosition:I

    .line 53
    if-eqz p9, :cond_a

    invoke-virtual {p9}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 54
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    .line 58
    :goto_0
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDate:Ljava/lang/Long;

    .line 60
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceName:Ljava/lang/String;

    .line 64
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mDeviceType:Ljava/lang/String;

    .line 68
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 69
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mVideoName:Ljava/lang/String;

    .line 72
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 73
    :cond_6
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mType:Ljava/lang/String;

    .line 76
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 77
    :cond_8
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    .line 79
    :cond_9
    return-void

    .line 56
    :cond_a
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mCaptionType:Ljava/lang/String;

    goto :goto_0
.end method

.method public setResumePosition(J)V
    .locals 1
    .param p1, "pos"    # J

    .prologue
    .line 92
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->mResumePosition:Ljava/lang/Long;

    return-void
.end method
