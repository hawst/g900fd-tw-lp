.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 716
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 718
    .local p1, "av":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-nez p1, :cond_1

    .line 719
    const-string v2, "VideoSubtitlePopup"

    const-string v3, "onItemClick() AdapterView is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 723
    :cond_1
    const-string v2, "VideoSubtitlePopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onItemClick() position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 725
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 727
    .local v1, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    .line 730
    const-string v2, "MENU_TITLE"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 731
    .local v0, "clickItem":Ljava/lang/String;
    const-string v2, "VideoSubtitlePopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onItemClick() item : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v3, 0x7f0a0078

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 734
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->clickColorAndOpacityPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)V

    goto :goto_0

    .line 736
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v3, 0x7f0a0029

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 737
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->clickColorAndOpacityPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)V

    goto :goto_0

    .line 739
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const v3, 0x7f0a013a

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 740
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const/4 v3, 0x2

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->clickColorAndOpacityPopup(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;I)V

    goto/16 :goto_0
.end method
