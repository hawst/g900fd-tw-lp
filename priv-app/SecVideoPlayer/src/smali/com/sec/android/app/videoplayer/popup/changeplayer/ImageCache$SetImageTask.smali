.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;
.super Landroid/os/AsyncTask;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SetImageTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private mBmp:Landroid/graphics/Bitmap;

.field private mImageKey:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Ljava/lang/String;)V
    .locals 0
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 91
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mImageKey:Ljava/lang/String;

    .line 92
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mImageKey:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mDmrIconSize:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mDmrIconSize:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)I

    move-result v3

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->getBitmapFromUrl(Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mBmp:Landroid/graphics/Bitmap;

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mBmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mImageKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mBmp:Landroid/graphics/Bitmap;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 104
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 106
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 85
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mCurrentTasks:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mImageKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 112
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mAdapter:Landroid/widget/BaseAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)Landroid/widget/BaseAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 115
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 85
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->mCurrentTasks:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache$SetImageTask;->mImageKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    return-void
.end method
