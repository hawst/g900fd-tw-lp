.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 447
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v5, 0x0

    .line 449
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "OnPreparedListener start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v3, 0x2

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$602(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 452
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 453
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 456
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v1

    .line 457
    .local v1, "vol":F
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPrepared() vol:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    invoke-virtual {p1, v1, v1}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 460
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v3

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$102(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 461
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v3

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 463
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v0

    .line 465
    .local v0, "seekToPosition":I
    if-eqz v0, :cond_1

    .line 466
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    .line 469
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v2

    if-gtz v2, :cond_3

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    .line 474
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 482
    :goto_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v2, :cond_2

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 484
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getBtnController()Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updatePausePlayBtn()V

    .line 488
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 489
    return-void

    .line 472
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsPauseEnable:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    goto :goto_0

    .line 477
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isBackgroundViewVisible()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v2

    if-nez v2, :cond_5

    .line 478
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 479
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    goto :goto_1
.end method
