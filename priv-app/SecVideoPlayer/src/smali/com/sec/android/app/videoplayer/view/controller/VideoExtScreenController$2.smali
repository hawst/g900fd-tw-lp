.class Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;
.super Ljava/lang/Object;
.source "VideoExtScreenController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 162
    const-string v0, "VideoExtScreenController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPlayBtnTouchListener E."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 181
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 165
    :pswitch_0
    const-string v0, "VideoExtScreenController"

    const-string v1, "mPlayPauseBtnTouchListener ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0

    .line 170
    :pswitch_1
    const-string v0, "VideoExtScreenController"

    const-string v1, "mPlayPauseBtnTouchListener ACTION_UP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->resetHoldLongSeek()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const/16 v1, 0x1770

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
