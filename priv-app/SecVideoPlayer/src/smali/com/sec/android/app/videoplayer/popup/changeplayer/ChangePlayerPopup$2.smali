.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->showDevicePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getCount()I

    move-result v0

    if-le v0, p3, :cond_0

    .line 325
    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "showDevicePopup. onItemClick."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showDevicePopup. onItemClick. which : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->UPDATE:Z

    .line 329
    if-lez p3, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPauseSet()V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->handleChangePlayer(I)V
    invoke-static {v0, p3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)V

    .line 335
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 338
    :cond_1
    return-void
.end method
