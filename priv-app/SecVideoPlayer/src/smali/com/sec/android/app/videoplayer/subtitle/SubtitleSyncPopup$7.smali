.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;
.super Ljava/lang/Object;
.source "SubtitleSyncPopup.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->createPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x2

    .line 159
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 179
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    .line 161
    :pswitch_1
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mMinusButton. ACTION_DOWN"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    const-wide/16 v2, 0xc8

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->sendMessage(IJ)V
    invoke-static {v0, v4, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;IJ)V

    goto :goto_0

    .line 166
    :pswitch_2
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mMinusButton. ACTION_UP"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;I)V

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->clickMINUSBtn()V

    goto :goto_0

    .line 173
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->removeMessage(I)V
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;I)V

    goto :goto_0

    .line 159
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
