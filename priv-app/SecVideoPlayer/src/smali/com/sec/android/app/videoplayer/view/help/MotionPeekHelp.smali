.class public Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;
.super Ljava/lang/Object;
.source "MotionPeekHelp.java"


# static fields
.field private static final ANIMATIOT_START_OFFSET:I = 0x2bc


# instance fields
.field private mBlockAll:Landroid/widget/RelativeLayout;

.field private mBlockKeyListener:Landroid/view/View$OnKeyListener;

.field private mBlockTouchListener:Landroid/view/View$OnTouchListener;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mHelpTextLayout:Landroid/widget/RelativeLayout;

.field private mHelpTextVisibility:I

.field private mMotionPeekHelpView:Landroid/view/View;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mTouchFlag:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    .line 30
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextVisibility:I

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mTouchFlag:Z

    .line 67
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp$1;-><init>(Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    .line 87
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp$2;-><init>(Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mContext:Landroid/content/Context;

    .line 36
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextVisibility:I

    .line 37
    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;
    .param p1, "x1"    # Z

    .prologue
    .line 15
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mTouchFlag:Z

    return p1
.end method

.method private setVisibilityHelpText(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 141
    if-eqz p1, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilityMotionPeek(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 125
    :cond_0
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 49
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 42
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 44
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030015

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    .line 45
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->initViews()V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->hide()V

    .line 47
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 120
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->setVisibilityMotionPeek(I)V

    .line 115
    return-void
.end method

.method public hideHelpText()V
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextVisibility:I

    .line 136
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextVisibility:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->setVisibilityHelpText(Z)V

    .line 137
    return-void

    .line 136
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initViews()V
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    const v1, 0x7f0d00f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    .line 58
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextVisibility:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->setVisibilityHelpText(Z)V

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mContext:Landroid/content/Context;

    const/high16 v1, 0x7f040000

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 64
    :cond_1
    return-void

    .line 58
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScreenTouched()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mTouchFlag:Z

    return v0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mMotionPeekHelpView:Landroid/view/View;

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 132
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->setVisibilityMotionPeek(I)V

    .line 111
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->show()V

    .line 105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 107
    return-void
.end method
