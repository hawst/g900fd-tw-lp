.class public Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;
.super Ljava/lang/Object;
.source "VideoAdditionalView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPlaySpeedButton:Landroid/widget/RelativeLayout;

.field private mPlaySpeedLayout:Landroid/widget/RelativeLayout;

.field private mPlaySpeedTextView:Landroid/widget/TextView;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private mRotateLayout:Landroid/widget/RelativeLayout;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSpeedTextLayout:Landroid/widget/RelativeLayout;

.field private mSpeedTextView:Landroid/widget/TextView;

.field private mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mParentView:Landroid/widget/RelativeLayout;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextLayout:Landroid/widget/RelativeLayout;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    .line 57
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 58
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 59
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 60
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->openPlaySpeed()V

    return-void
.end method

.method private initViews()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0109

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d010b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d010a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 79
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedBtn()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d010e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextLayout:Landroid/widget/RelativeLayout;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0110

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d010c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setRotateBtn(Landroid/content/Context;Landroid/view/View;)V

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPaddingForSpeedText()V

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPaddingForRotationBtn()V

    .line 104
    return-void
.end method

.method private openPlaySpeed()V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView$2;-><init>(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setOnUpdatedListener(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;)V

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->showPopup()V

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 122
    :cond_2
    return-void
.end method

.method private updatePlaySpeedText()Ljava/lang/String;
    .locals 7

    .prologue
    .line 146
    const/4 v1, 0x0

    .line 147
    .local v1, "playspeed":F
    const/4 v2, 0x0

    .line 148
    .local v2, "text":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    .line 150
    .local v0, "currentSpeed":I
    add-int/lit8 v3, v0, 0x5

    int-to-float v3, v3

    const v4, 0x3dcccccd    # 0.1f

    mul-float v1, v3, v4

    .line 151
    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 153
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mParentView:Landroid/widget/RelativeLayout;

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 67
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03001a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->initViews()V

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 70
    return-void
.end method

.method public blockPlaySpeed()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSlowCPU()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isVideoOnlyClip()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiOrNoAudioChannel()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 228
    :cond_0
    return-void
.end method

.method public dismissPlaySpeed()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->dismiss()V

    .line 143
    :cond_0
    return-void
.end method

.method public getRootView()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public getSpeedTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 288
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 291
    :cond_0
    return-void
.end method

.method public isBtnShow()Z
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 297
    :cond_0
    const/4 v0, 0x1

    .line 300
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 196
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    if-nez v8, :cond_1

    .line 217
    :cond_0
    :goto_0
    return v11

    .line 197
    :cond_1
    const/4 v8, 0x2

    new-array v4, v8, [I

    .line 198
    .local v4, "screenPos":[I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 199
    .local v2, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 200
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    .line 203
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 204
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 205
    .local v3, "height":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v5, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 207
    .local v5, "screenWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 208
    .local v6, "viewStr":Ljava/lang/String;
    invoke-static {v1, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 210
    .local v0, "cheatSheet":Landroid/widget/Toast;
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v8, :cond_3

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a00f8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->getCurrentRotation()I

    move-result v8

    if-eq v8, v11, :cond_3

    .line 212
    :cond_2
    mul-int/lit8 v3, v3, 0x2

    .line 215
    :cond_3
    const/16 v8, 0x35

    aget v9, v4, v10

    sub-int v9, v5, v9

    div-int/lit8 v10, v7, 0x2

    sub-int/2addr v9, v10

    aget v10, v4, v11

    sub-int/2addr v10, v3

    invoke-virtual {v0, v8, v9, v10}, Landroid/widget/Toast;->setGravity(III)V

    .line 216
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mParentView:Landroid/widget/RelativeLayout;

    .line 223
    return-void
.end method

.method public setPaddingForPlaySpeedBtn()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08010d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08010c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v4, v4, v2}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 237
    return-void
.end method

.method public setPaddingForRotationBtn()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08010f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08010e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 243
    return-void
.end method

.method public setPaddingForSpeedText()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080112

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v3, v3, v3, v1}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 268
    return-void
.end method

.method public setPlaySpeedViewVisibility(I)V
    .locals 1
    .param p1, "visible"    # I

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 232
    return-void
.end method

.method public setSpeedTextViewVisibility(ZF)V
    .locals 7
    .param p1, "visibility"    # Z
    .param p2, "speed"    # F

    .prologue
    const/4 v6, 0x0

    .line 246
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 247
    if-eqz p1, :cond_3

    .line 248
    new-instance v0, Ljava/lang/String;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 249
    .local v0, "speedString":Ljava/lang/String;
    float-to-double v2, p2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v2, v4

    if-nez v1, :cond_1

    .line 250
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1/2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 257
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0123

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    .end local v0    # "speedString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 251
    .restart local v0    # "speedString":Ljava/lang/String;
    :cond_1
    float-to-double v2, p2

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1/4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 254
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    float-to-int v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 261
    .end local v0    # "speedString":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mSpeedTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 278
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 281
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updateRotationBtn()V

    .line 282
    return-void
.end method

.method public updatePlaySpeedBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 158
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPlaySpeedViewVisibility(I)V

    .line 162
    :goto_0
    const/4 v0, 0x0

    .line 164
    .local v0, "playSeepdBtnVisible":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPlaySpeedVisibility()I

    move-result v2

    sget v3, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    if-ne v2, v3, :cond_0

    .line 165
    const/4 v0, 0x1

    .line 168
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 169
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->blockPlaySpeed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 170
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedText()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "speed":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    .end local v1    # "speed":Ljava/lang/String;
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPaddingForPlaySpeedBtn()V

    .line 180
    return-void

    .line 160
    .end local v0    # "playSeepdBtnVisible":Z
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPlaySpeedViewVisibility(I)V

    goto :goto_0

    .line 175
    .restart local v0    # "playSeepdBtnVisible":Z
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 176
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateRotationBtn()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 131
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPaddingForRotationBtn()V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateAutoRotationBtn()V

    .line 137
    :cond_1
    return-void

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->mRotateLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method
