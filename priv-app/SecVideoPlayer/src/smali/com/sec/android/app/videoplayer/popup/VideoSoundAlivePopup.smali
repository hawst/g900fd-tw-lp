.class public Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
.super Ljava/lang/Object;
.source "VideoSoundAlivePopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;
    }
.end annotation


# static fields
.field private static final ID:I = 0x2

.field public static final SOUNDALIVE_VIDEO_AUTO:I = 0x7f0a001a

.field public static final SOUNDALIVE_VIDEO_MUSIC:I = 0x7f0a00cd

.field public static final SOUNDALIVE_VIDEO_NORMAL:I = 0x7f0a00d8

.field public static final SOUNDALIVE_VIDEO_VIRT71:I = 0x7f0a0140

.field public static final SOUNDALIVE_VIDEO_VOICE:I = 0x7f0a01c6

.field private static final TAG:Ljava/lang/String; = "VideoSoundAlivePopup"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mAliveArray:Landroid/content/res/TypedArray;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

.field private mSoundAlive:I

.field private mSoundAliveTemp:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I

    .line 39
    iput v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAliveTemp:I

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    .line 45
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    .line 215
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 257
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 50
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->revertSoundEffect()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/content/res/TypedArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private revertSoundEffect()V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAliveTemp:I

    invoke-interface {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;->onSelected(I)V

    .line 273
    :cond_0
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 61
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 56
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x2

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    .line 213
    return-void
.end method

.method public showPopup()V
    .locals 8

    .prologue
    .line 75
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    const-string v7, "VideoSoundAlivePopup"

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 76
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getSAEffectMode()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAliveTemp:I

    .line 78
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 79
    .local v2, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    const v7, 0x7f0a012c

    invoke-virtual {v6, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 80
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0026

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$1;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_PATEK:Z

    if-nez v6, :cond_0

    .line 90
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    const v7, 0x7f0a0055

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V

    invoke-virtual {v2, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 97
    :cond_0
    const/4 v4, 0x0

    .line 99
    .local v4, "selected":I
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I

    packed-switch v6, :pswitch_data_0

    .line 121
    const v4, 0x7f0a00d8

    .line 125
    :goto_0
    const/4 v0, 0x0

    .line 127
    .local v0, "arrayEntries":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    .line 129
    .local v1, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    if-eqz v6, :cond_1

    .line 130
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 131
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    .line 134
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 136
    :cond_2
    const v4, 0x7f0a00d8

    .line 137
    const v0, 0x7f06000e

    .line 138
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f06000e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    .line 155
    :goto_1
    const/4 v5, -0x1

    .line 156
    .local v5, "selectedDialog":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->length()I

    move-result v6

    if-ge v3, v6, :cond_9

    .line 157
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    const/4 v7, -0x1

    invoke-virtual {v6, v3, v7}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v6

    if-ne v6, v4, :cond_3

    .line 158
    move v5, v3

    .line 156
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 101
    .end local v0    # "arrayEntries":I
    .end local v1    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .end local v3    # "i":I
    .end local v5    # "selectedDialog":I
    :pswitch_0
    const v4, 0x7f0a00d8

    .line 102
    goto :goto_0

    .line 105
    :pswitch_1
    const v4, 0x7f0a001a

    .line 106
    goto :goto_0

    .line 109
    :pswitch_2
    const v4, 0x7f0a01c6

    .line 110
    goto :goto_0

    .line 113
    :pswitch_3
    const v4, 0x7f0a00cd

    .line 114
    goto :goto_0

    .line 117
    :pswitch_4
    const v4, 0x7f0a0140

    .line 118
    goto :goto_0

    .line 139
    .restart local v0    # "arrayEntries":I
    .restart local v1    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_4
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v6

    if-nez v6, :cond_5

    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-eqz v6, :cond_7

    .line 140
    :cond_5
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v6, :cond_6

    .line 141
    const v0, 0x7f06000d

    .line 142
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f06000d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    goto :goto_1

    .line 144
    :cond_6
    const v0, 0x7f06000c

    .line 145
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f06000c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    goto :goto_1

    .line 148
    :cond_7
    const v6, 0x7f0a00d8

    if-eq v4, v6, :cond_8

    const v6, 0x7f0a01c6

    if-eq v4, v6, :cond_8

    .line 149
    const v4, 0x7f0a00d8

    .line 151
    :cond_8
    const v0, 0x7f06000f

    .line 152
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f06000f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;

    goto :goto_1

    .line 161
    .restart local v3    # "i":I
    .restart local v5    # "selectedDialog":I
    :cond_9
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V

    invoke-virtual {v2, v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 201
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    .line 202
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 203
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 204
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V

    .line 205
    return-void

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method
