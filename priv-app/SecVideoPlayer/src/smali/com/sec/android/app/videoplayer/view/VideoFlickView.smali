.class public Lcom/sec/android/app/videoplayer/view/VideoFlickView;
.super Ljava/lang/Object;
.source "VideoFlickView.java"


# static fields
.field private static final BASE_SECOND:I = 0x5

.field private static final SEC:Ljava/lang/String; = "5"


# instance fields
.field private frameAnimationleft:Landroid/graphics/drawable/AnimationDrawable;

.field private frameAnimationright:Landroid/graphics/drawable/AnimationDrawable;

.field private mContext:Landroid/content/Context;

.field private mFlickView:Landroid/widget/RelativeLayout;

.field private mLeftFlickCounter:I

.field private mLeftFlickLayout:Landroid/widget/RelativeLayout;

.field private mLeftImage:Landroid/widget/ImageView;

.field private mLeftText:Landroid/widget/TextView;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mRightFlickCounter:I

.field private mRightFlickLayout:Landroid/widget/RelativeLayout;

.field private mRightImage:Landroid/widget/ImageView;

.field private mRightText:Landroid/widget/TextView;

.field private mSecondsText:Ljava/lang/String;

.field mTempFlickLayout:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mContext:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mTempFlickLayout:Landroid/widget/RelativeLayout;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftImage:Landroid/widget/ImageView;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightImage:Landroid/widget/ImageView;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    .line 40
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I

    .line 41
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickCounter:I

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mContext:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I

    return p1
.end method

.method static synthetic access$104(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;
    .param p1, "x1"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->getFlickText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationleft:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickCounter:I

    return p1
.end method

.method static synthetic access$604(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickCounter:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/graphics/drawable/AnimationDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationright:Landroid/graphics/drawable/AnimationDrawable;

    return-object v0
.end method

.method private getFlickText(I)Ljava/lang/String;
    .locals 6
    .param p1, "counter"    # I

    .prologue
    .line 185
    new-instance v0, Ljava/util/Formatter;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 186
    .local v0, "mFormatter":Ljava/util/Formatter;
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    mul-int/lit8 v5, p1, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "number":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/util/Formatter;->close()V

    .line 188
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mSecondsText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private initViews()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0161

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0163

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0162

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftImage:Landroid/widget/ImageView;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftImage:Landroid/widget/ImageView;

    const v1, 0x7f040001

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationleft:Landroid/graphics/drawable/AnimationDrawable;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0164

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0166

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0165

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightImage:Landroid/widget/ImageView;

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightImage:Landroid/widget/ImageView;

    const v1, 0x7f040004

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationright:Landroid/graphics/drawable/AnimationDrawable;

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0120

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mSecondsText:Ljava/lang/String;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mSecondsText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mSecondsText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 49
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 50
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 51
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v2, 0x7f030025

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    .line 53
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 54
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->initViews()V

    .line 56
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    return-void
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mFlickView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 85
    :cond_0
    return-void
.end method

.method public leftFlickView()V
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 136
    return-void
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;

    .line 79
    return-void
.end method

.method public rightFlickView()V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;-><init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 182
    return-void
.end method
