.class public Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;
.super Ljava/lang/Object;
.source "TypefaceFinder.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TypefaceSortByName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/app/videoplayer/flipfont/Typeface;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;->this$0:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/app/videoplayer/flipfont/Typeface;Lcom/sec/android/app/videoplayer/flipfont/Typeface;)I
    .locals 2
    .param p1, "o1"    # Lcom/sec/android/app/videoplayer/flipfont/Typeface;
    .param p2, "o2"    # Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sec/android/app/videoplayer/flipfont/Typeface;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 124
    check-cast p1, Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/app/videoplayer/flipfont/Typeface;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefaceSortByName;->compare(Lcom/sec/android/app/videoplayer/flipfont/Typeface;Lcom/sec/android/app/videoplayer/flipfont/Typeface;)I

    move-result v0

    return v0
.end method
