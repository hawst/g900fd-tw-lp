.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;
.super Landroid/widget/RelativeLayout;
.source "SubtitleSplitView.java"


# static fields
.field private static final HIGHLIGHT_SUB_TEXT:I

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

.field private mContext:Landroid/content/Context;

.field mControllerWidth:I

.field mDm:Landroid/util/DisplayMetrics;

.field private final mHandler:Landroid/os/Handler;

.field mListHeight:I

.field mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field mListLeftPadding:I

.field mListRightPadding:I

.field private mListView:Landroid/widget/ListView;

.field mListWidth:I

.field mMainHeight:I

.field mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field mMainLeftPadding:I

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field mMainWidth:I

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSplitSubtitleView:Landroid/widget/RelativeLayout;

.field private mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

.field mSurfaceHeight:I

.field mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field mTitleLayout:Landroid/widget/LinearLayout;

.field mTitlePadding:I

.field map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mParentView:Landroid/widget/RelativeLayout;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 53
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLeftPadding:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListRightPadding:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitlePadding:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLeftPadding:I

    .line 205
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;

    .line 222
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 241
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    .line 65
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 66
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->scrollMyListViewTo()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method private initListView()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0192

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(I)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onWindowFocusChanged(Z)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 104
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 105
    return-void
.end method

.method private resetPlayerLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 378
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 380
    .local v0, "resetMainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v5, v5, v5, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setPadding(IIII)V

    .line 382
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 384
    .local v1, "resetSurfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    return-void
.end method

.method private scrollMyListViewTo()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 134
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    .line 138
    .local v0, "currentposition":I
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    if-nez v8, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 142
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    .line 143
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    const-string v9, "SUBTITLE_END_TIME"

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v2

    .line 144
    .local v2, "endtime":J
    int-to-long v8, v0

    cmp-long v8, v8, v2

    if-lez v8, :cond_2

    .line 145
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    const-string v9, "No more subtitle lines.so highlight last line"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setSelectionHighlight(I)V

    goto :goto_0

    .line 149
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    .line 150
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    const-string v9, "SUBTITLE_START_TIME"

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v4

    .line 151
    .local v4, "starttime":J
    int-to-long v8, v0

    cmp-long v8, v8, v4

    if-gez v8, :cond_3

    .line 152
    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    const-string v9, "Subtitle lines have not yet started,so higlight 1st"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setSelectionHighlight(I)V

    goto :goto_0

    .line 157
    :cond_3
    int-to-long v8, v0

    cmp-long v8, v8, v4

    if-lez v8, :cond_0

    .line 158
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_0

    .line 159
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    .line 160
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    const-string v9, "SUBTITLE_START_TIME"

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v4

    .line 161
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    const-string v9, "SUBTITLE_END_TIME"

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v2

    .line 162
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/HashMap;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    .line 163
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->map:Ljava/util/HashMap;

    const-string v9, "SUBTITLE_START_TIME"

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->timeinMilliSec(Ljava/lang/String;)J

    move-result-wide v6

    .line 164
    .local v6, "starttimenext":J
    int-to-long v8, v0

    cmp-long v8, v4, v8

    if-gtz v8, :cond_4

    int-to-long v8, v0

    cmp-long v8, v2, v8

    if-ltz v8, :cond_4

    .line 166
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setSelectionHighlight(I)V

    goto/16 :goto_0

    .line 168
    :cond_4
    int-to-long v8, v0

    cmp-long v8, v2, v8

    if-gez v8, :cond_5

    int-to-long v8, v0

    cmp-long v8, v6, v8

    if-lez v8, :cond_5

    .line 170
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setSelectionHighlight(I)V

    goto/16 :goto_0

    .line 158
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private setSelectionHighlight(I)V
    .locals 5
    .param p1, "i"    # I

    .prologue
    .line 179
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_2

    .line 180
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->setselectionPosition(I)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->notifyDataSetChanged()V

    .line 184
    :cond_0
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08020b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080209

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080208

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int v0, v1, v2

    .line 191
    .local v0, "titleheight":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    invoke-virtual {v1, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0801fe

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v0

    invoke-virtual {v1, p1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 203
    .end local v0    # "titleheight":I
    :goto_0
    return-void

    .line 196
    .restart local v0    # "titleheight":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v2, v0

    invoke-virtual {v1, p1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    .line 200
    .end local v0    # "titleheight":I
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    const-string v2, "mListView is null !!! "

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setSubtitleListView()V
    .locals 2

    .prologue
    .line 86
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    const-string v1, "setSubtitleListView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->resetSubtitleList()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFullDumpSubtitle()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->initdata([B)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->initListView()V

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setupAdapter()V

    .line 97
    return-void
.end method

.method private setupAdapter()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->getSubtitleList()Ljava/util/ArrayList;

    move-result-object v2

    const v3, 0x7f03002f

    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "SUBTITLE_START_TIME"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "SUBTITLE_END_TIME"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "SUBTITLE_TEXT"

    aput-object v6, v4, v5

    new-array v5, v7, [I

    fill-array-data v5, :array_0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 123
    :goto_0
    return-void

    .line 120
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    const-string v1, "setupAdapter. mListView is NULL. will be destroyed."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->hideView()V

    goto :goto_0

    .line 109
    :array_0
    .array-data 4
        0x7f0d0194
        0x7f0d0195
        0x7f0d0196
    .end array-data
.end method

.method private timeinMilliSec(Ljava/lang/String;)J
    .locals 9
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 256
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 257
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "s":[Ljava/lang/String;
    aget-object v2, v0, v4

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "s1":[Ljava/lang/String;
    aget-object v2, v1, v4

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    mul-long/2addr v2, v4

    aget-object v4, v1, v8

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/32 v6, 0xea60

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    const/4 v4, 0x2

    aget-object v4, v1, v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    aget-object v4, v0, v8

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    add-long/2addr v2, v4

    return-wide v2
.end method

.method private updatePlayerLayout(Z)V
    .locals 8
    .param p1, "isLandscape"    # Z

    .prologue
    const v7, 0x7f080206

    const/16 v6, 0x9

    const v5, 0x7f08015d

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 269
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updatePlayerLayout-isLandscape"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0190

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitleLayout:Landroid/widget/LinearLayout;

    .line 272
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 275
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080211

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    .line 278
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLeftPadding:I

    .line 280
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080202

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListRightPadding:I

    .line 282
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08020c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitlePadding:I

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    .line 288
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLeftPadding:I

    .line 289
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mControllerWidth:I

    .line 293
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 295
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 297
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 327
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLeftPadding:I

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setPadding(IIII)V

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLeftPadding:I

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitleLayout:Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitlePadding:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLeftPadding:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mTitlePadding:I

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 344
    :cond_0
    return-void

    .line 299
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 301
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLeftPadding:I

    .line 302
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0801fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListHeight:I

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListHeight:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainHeight:I

    .line 304
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080207

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    .line 306
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mControllerWidth:I

    .line 308
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 309
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 310
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 314
    :cond_2
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLeftPadding:I

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mControllerWidth:I

    .line 319
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 321
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListWidth:I

    invoke-direct {v0, v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 323
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 324
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 71
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mParentView:Landroid/widget/RelativeLayout;

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 75
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f03002e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method public hideView()V
    .locals 2

    .prologue
    .line 347
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    .line 348
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mDm:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 350
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080206

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSurfaceHeight:I

    .line 353
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->resetPlayerLayout()V

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 356
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 364
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mListView:Landroid/widget/ListView;

    .line 366
    :cond_0
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSplitSubtitleView:Landroid/widget/RelativeLayout;

    .line 367
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    .line 368
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->removeHandlerMessage()V

    .line 369
    return-void
.end method

.method public removeHandlerMessage()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 373
    return-void
.end method

.method public setSplitSubtitleView()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->setSubtitleListView()V

    .line 83
    return-void
.end method

.method public showView(Z)V
    .locals 4
    .param p1, "isLandscape"    # Z

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->updatePlayerLayout(Z)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 266
    return-void
.end method

.method public updateView()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->mAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitAdapter;->notifyDataSetChanged()V

    .line 131
    :cond_0
    return-void
.end method
