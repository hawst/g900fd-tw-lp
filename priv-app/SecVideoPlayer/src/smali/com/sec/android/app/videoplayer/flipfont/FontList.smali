.class public Lcom/sec/android/app/videoplayer/flipfont/FontList;
.super Ljava/lang/Object;
.source "FontList.java"


# static fields
.field private static final ANDALE_MONO:Ljava/lang/String; = "Andale Mono"

.field private static final APPLE_MINT:Ljava/lang/String; = "Apple mint"

.field private static final CHOCO:Ljava/lang/String; = "Choco"

.field private static final CHOCO_COOKY:Ljava/lang/String; = "Choco cooky"

.field private static final COOL:Ljava/lang/String; = "Cool"

.field private static final COOL_JAZZ:Ljava/lang/String; = "Cool jazz"

.field private static final COURIER:Ljava/lang/String; = "Courier New"

.field private static final FONT_ANDALEMO:Ljava/lang/String; = "andalemo.ttf"

.field private static final FONT_COURIER:Ljava/lang/String; = "/system/fonts/Cour.ttf"

.field private static final FONT_DIRECTORY:Ljava/lang/String; = "fonts/"

.field private static final FONT_DROID_SANS:Ljava/lang/String; = "/system/fonts/DroidSans.ttf"

.field private static final FONT_EXTENSION:Ljava/lang/String; = ".ttf"

.field private static final FONT_PACKAGE:Ljava/lang/String; = "com.monotype.android.font."

.field private static final FONT_SAMSUNG_SANS_SMALL_CAP:Ljava/lang/String; = "SamsungSansSmallCap.ttf"

.field private static final FONT_TIMES_NEW_ROMAN:Ljava/lang/String; = "/system/fonts/Times.ttf"

.field private static final KAITI:Ljava/lang/String; = "Kaiti"

.field private static final MARUBERI:Ljava/lang/String; = "Maruberi"

.field private static final MIAO:Ljava/lang/String; = "Miao"

.field private static final MINCHO:Ljava/lang/String; = "Mincho"

.field private static final POP:Ljava/lang/String; = "Pop"

.field private static final ROSE:Ljava/lang/String; = "Rose"

.field private static final ROSE_MARY:Ljava/lang/String; = "Rosemary"

.field private static final SAMSUNG_SANS_SMALL_CAP:Ljava/lang/String; = "Samsung Sans Small Cap"

.field private static final SHAONV:Ljava/lang/String; = "Shao nv"

.field private static final TAG:Ljava/lang/String; = "FontList"

.field private static final TIMES_NEW_ROMAN:Ljava/lang/String; = "Times New Roman"

.field private static final TINKER_BELL:Ljava/lang/String; = "Tinker bell"

.field private static final UDMINCHO:Ljava/lang/String; = "UDMincho"

.field private static final UDRGOTHIC:Ljava/lang/String; = "UDRGothic"


# instance fields
.field private mAndaleMonoFont:Landroid/graphics/Typeface;

.field private mContext:Landroid/content/Context;

.field private mCourierFont:Landroid/graphics/Typeface;

.field private mDroidSansFont:Landroid/graphics/Typeface;

.field private mFontAssetManager:Landroid/content/res/AssetManager;

.field private mFontNames:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFontPackageNames:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mInstalledApplications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mSSansSmallCapFont:Landroid/graphics/Typeface;

.field private mTimesNewRomanFont:Landroid/graphics/Typeface;

.field private mTypefaceFiles:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

.field private mTypefaces:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Landroid/graphics/Typeface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 136
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    .line 43
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 46
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    .line 52
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    .line 55
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    .line 58
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    .line 61
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    .line 64
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    .line 86
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 88
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mCourierFont:Landroid/graphics/Typeface;

    .line 90
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTimesNewRomanFont:Landroid/graphics/Typeface;

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mAndaleMonoFont:Landroid/graphics/Typeface;

    .line 94
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mSSansSmallCapFont:Landroid/graphics/Typeface;

    .line 137
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    .line 138
    new-instance v1, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    invoke-direct {v1, p1}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    .line 139
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 141
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/fonts/DroidSans.ttf"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 143
    const-string v1, "FontList"

    const-string v2, "fontListInit() :: ttf exists"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const-string v1, "/system/fonts/DroidSans.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 147
    :cond_0
    new-instance v0, Ljava/io/File;

    .end local v0    # "f":Ljava/io/File;
    const-string v1, "/system/fonts/Cour.ttf"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .restart local v0    # "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 149
    const-string v1, "FontList"

    const-string v2, "fontListInit() :: ttf exists"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const-string v1, "/system/fonts/Cour.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mCourierFont:Landroid/graphics/Typeface;

    .line 153
    :cond_1
    new-instance v0, Ljava/io/File;

    .end local v0    # "f":Ljava/io/File;
    const-string v1, "/system/fonts/Times.ttf"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 154
    .restart local v0    # "f":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    const-string v1, "FontList"

    const-string v2, "fontListInit() :: ttf exists"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-string v1, "/system/fonts/Times.ttf"

    invoke-static {v1}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTimesNewRomanFont:Landroid/graphics/Typeface;

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "andalemo.ttf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mAndaleMonoFont:Landroid/graphics/Typeface;

    .line 161
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string v2, "SamsungSansSmallCap.ttf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mSSansSmallCapFont:Landroid/graphics/Typeface;

    .line 162
    return-void
.end method


# virtual methods
.method public fontListInit()V
    .locals 12

    .prologue
    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->clearTypfaceArray()V

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    .line 178
    const/4 v9, 0x0

    .line 179
    .local v9, "fontPackageName":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v10, v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mInstalledApplications:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    iget-object v9, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 181
    const-string v0, "com.monotype.android.font."

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v1, 0x80

    invoke-virtual {v0, v9, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v7

    .line 183
    .local v7, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v0, v7, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v0, v7, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 184
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v11

    .line 186
    .local v11, "res":Landroid/content/res/Resources;
    invoke-virtual {v11}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    invoke-virtual {v0, v1, v9}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->findTypefaces(Landroid/content/res/AssetManager;Ljava/lang/String;)Z

    .line 179
    .end local v7    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v11    # "res":Landroid/content/res/Resources;
    :cond_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v0, :cond_3

    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFinder:Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mCourierFont:Landroid/graphics/Typeface;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTimesNewRomanFont:Landroid/graphics/Typeface;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder;->getSansEntries(Landroid/content/pm/PackageManager;Ljava/util/Vector;Ljava/util/Vector;Ljava/util/Vector;Landroid/graphics/Typeface;Landroid/graphics/Typeface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    .end local v9    # "fontPackageName":Ljava/lang/String;
    .end local v10    # "i":I
    :cond_3
    :goto_1
    return-void

    .line 193
    :catch_0
    move-exception v8

    .line 194
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "FontList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "font package not found, just use default font, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    .line 205
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFont(I)Landroid/graphics/Typeface;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    .line 368
    :goto_0
    return-object v0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-eqz v0, :cond_1

    .line 366
    const-string v0, "FontList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFont() :: input = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;
    .locals 9
    .param p1, "typeface"    # Ljava/lang/String;
    .param p2, "fontPackageName"    # Ljava/lang/String;

    .prologue
    .line 326
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "default"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    if-nez p2, :cond_1

    .line 330
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    .line 358
    :goto_0
    return-object v6

    .line 331
    :cond_1
    const-string v6, "Courier New"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 332
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mCourierFont:Landroid/graphics/Typeface;

    goto :goto_0

    .line 333
    :cond_2
    const-string v6, "Times New Roman"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 334
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTimesNewRomanFont:Landroid/graphics/Typeface;

    goto :goto_0

    .line 335
    :cond_3
    const-string v6, "Andale Mono"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 336
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mAndaleMonoFont:Landroid/graphics/Typeface;

    goto :goto_0

    .line 337
    :cond_4
    const-string v6, "Samsung Sans Small Cap"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 338
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mSSansSmallCapFont:Landroid/graphics/Typeface;

    goto :goto_0

    .line 340
    :cond_5
    const/16 v6, 0x2f

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 341
    .local v5, "start_pos":I
    const/16 v6, 0x2e

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 342
    .local v2, "end_pos":I
    if-gez v2, :cond_6

    .line 343
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 346
    :cond_6
    add-int/lit8 v6, v5, 0x1

    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 347
    .local v3, "loadTypeface":Ljava/lang/String;
    const-string v6, " "

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 349
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    const/16 v7, 0x80

    invoke-virtual {v6, p2, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 350
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iput-object v6, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 351
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;

    move-result-object v4

    .line 353
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    .end local v0    # "appInfo":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "res":Landroid/content/res/Resources;
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontAssetManager:Landroid/content/res/AssetManager;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fonts/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".ttf"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/flipfont/TypefaceFinder$TypefacesClass;->get(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v6

    goto/16 :goto_0

    .line 354
    :catch_0
    move-exception v1

    .line 355
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "FontList"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "font package not found, just use default font, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getFontName(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 213
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, p1, :cond_0

    .line 214
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 215
    .local v0, "tmpString":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 217
    .end local v0    # "tmpString":Ljava/lang/String;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "fontStringName"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0a00cb

    const v2, 0x7f0a00c4

    const v1, 0x7f0a00c3

    .line 222
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 263
    :goto_0
    return-object v0

    .line 226
    :cond_1
    const-string v0, "Courier New"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    :cond_2
    :goto_1
    move-object v0, p1

    .line 263
    goto :goto_0

    .line 228
    :cond_3
    const-string v0, "Times New Roman"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 230
    :cond_4
    const-string v0, "Andale Mono"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 232
    :cond_5
    const-string v0, "Samsung Sans Small Cap"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 233
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 234
    :cond_6
    const-string v0, "Cool"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 236
    :cond_7
    const-string v0, "Rose"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto :goto_1

    .line 238
    :cond_8
    const-string v0, "Choco"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 240
    :cond_9
    const-string v0, "Rosemary"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 242
    :cond_a
    const-string v0, "Choco cooky"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 244
    :cond_b
    const-string v0, "Cool jazz"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 245
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 246
    :cond_c
    const-string v0, "Apple mint"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 248
    :cond_d
    const-string v0, "Tinker bell"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 250
    :cond_e
    const-string v0, "Shao nv"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 252
    :cond_f
    const-string v0, "Kaiti"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 254
    :cond_10
    const-string v0, "Miao"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 256
    :cond_11
    const-string v0, "Maruberi"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    const-string v0, "UDRGothic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 257
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 258
    :cond_13
    const-string v0, "Mincho"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    const-string v0, "UDMincho"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 259
    :cond_14
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1

    .line 260
    :cond_15
    const-string v0, "Pop"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    .end local p1    # "fontStringName":Ljava/lang/String;
    check-cast p1, Ljava/lang/String;

    .restart local p1    # "fontStringName":Ljava/lang/String;
    goto/16 :goto_1
.end method

.method public getFontPackageName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 276
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 278
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFontStringName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontNames:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTypefaceName(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 270
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadTypefaces()V
    .locals 6

    .prologue
    .line 294
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    if-nez v4, :cond_1

    .line 318
    :cond_0
    return-void

    .line 297
    :cond_1
    const/4 v1, 0x0

    .line 298
    .local v1, "fontfile":Ljava/lang/String;
    const/4 v0, 0x0

    .line 299
    .local v0, "fontPackageName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 300
    .local v3, "newTypeface":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->clear()V

    .line 301
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mDroidSansFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 303
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-nez v4, :cond_2

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mCourierFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 305
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTimesNewRomanFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 306
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mAndaleMonoFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 307
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mSSansSmallCapFont:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v2

    .line 311
    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 312
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaceFiles:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 313
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mFontPackageNames:Ljava/util/Vector;

    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 314
    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/flipfont/FontList;->getFont(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    .line 315
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/flipfont/FontList;->mTypefaces:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 316
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
