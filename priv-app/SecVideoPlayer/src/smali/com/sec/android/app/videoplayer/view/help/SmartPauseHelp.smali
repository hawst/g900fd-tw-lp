.class public Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;
.super Ljava/lang/Object;
.source "SmartPauseHelp.java"


# static fields
.field private static final ANIMATIOT_START_OFFSET:I = 0x2bc


# instance fields
.field private mBlockAll:Landroid/widget/RelativeLayout;

.field private mBlockKeyListener:Landroid/view/View$OnKeyListener;

.field private mBlockTouchListener:Landroid/view/View$OnTouchListener;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mHelpTextLayout:Landroid/widget/RelativeLayout;

.field private mHelpTextVisibility:I

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mSmartPauseHelpView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mContext:Landroid/content/Context;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    .line 34
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextVisibility:I

    .line 67
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp$1;-><init>(Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    .line 73
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp$2;-><init>(Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mContext:Landroid/content/Context;

    .line 38
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextVisibility:I

    .line 39
    return-void
.end method

.method private setVisibilityHelpText(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 124
    if-eqz p1, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilitySmartPause(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 108
    :cond_0
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 42
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 50
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 43
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 44
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 45
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->initViews()V

    .line 47
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->hide()V

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 103
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->setVisibilitySmartPause(I)V

    .line 98
    return-void
.end method

.method public hideHelpText()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextVisibility:I

    .line 119
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextVisibility:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->setVisibilityHelpText(Z)V

    .line 120
    return-void

    .line 119
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initViews()V
    .locals 4

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockAll:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    const v1, 0x7f0d00f6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    .line 59
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextVisibility:I

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->setVisibilityHelpText(Z)V

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mContext:Landroid/content/Context;

    const/high16 v1, 0x7f040000

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 62
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 65
    :cond_1
    return-void

    .line 59
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mSmartPauseHelpView:Landroid/view/View;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 115
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->setVisibilitySmartPause(I)V

    .line 94
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->show()V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mHelpTextLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 90
    return-void
.end method
