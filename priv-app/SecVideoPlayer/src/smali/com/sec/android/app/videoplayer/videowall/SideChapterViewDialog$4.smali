.class Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;
.super Ljava/lang/Object;
.source "SideChapterViewDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->chapterviewResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v12, 0x0

    const/4 v14, 0x1

    .line 385
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    if-ge v6, v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mStop:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    if-nez v1, :cond_4

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v1

    if-nez v1, :cond_1

    .line 389
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    mul-int/2addr v1, v6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    div-int v8, v1, v2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    aget-object v9, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v10

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v11

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    div-int v13, v1, v2

    invoke-static/range {v7 .. v13}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I

    move-result v0

    .line 391
    .local v0, "res":I
    if-ne v0, v14, :cond_0

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setKeyframeTime(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 393
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler2:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 394
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v1

    aget-object v4, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    aget-object v5, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v9

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapImage(Landroid/graphics/Bitmap;IIII)V

    .line 385
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler3:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$2000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 399
    .end local v0    # "res":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setThumbnail(ILjava/lang/String;)I

    move-result v0

    .line 400
    .restart local v0    # "res":I
    if-ne v0, v14, :cond_2

    .line 401
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getKeyframeTime(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 402
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler2:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 403
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v1

    aget-object v1, v1, v6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v4

    invoke-virtual {v1, v6, v2, v3, v4}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapView(IIII)V

    goto :goto_1

    .line 405
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    mul-int/2addr v1, v6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    div-int v8, v1, v2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    aget-object v9, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v10

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v11

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    div-int v13, v1, v2

    invoke-static/range {v7 .. v13}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I

    move-result v0

    .line 408
    if-ne v0, v14, :cond_3

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setKeyframeTime(Ljava/lang/String;I)I

    move-result v2

    aput v2, v1, v6

    .line 410
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler2:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mbitview:[Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v1

    aget-object v4, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mchapterbit:[Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/graphics/Bitmap;

    move-result-object v1

    aget-object v5, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->width:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->height:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->fps:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1800(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v9

    invoke-virtual/range {v4 .. v9}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapImage(Landroid/graphics/Bitmap;IIII)V

    goto/16 :goto_1

    .line 413
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mHandler3:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$2000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 419
    .end local v0    # "res":I
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mfileManager:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1200(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 420
    const/4 v12, 0x0

    .line 421
    .local v12, "thumbtime":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getShortTimeDuration()I

    move-result v2

    if-lt v1, v2, :cond_7

    .line 422
    const/4 v12, 0x3

    .line 423
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getLongTimeDuration()I

    move-result v2

    if-lt v1, v2, :cond_5

    .line 424
    const/16 v12, 0xa

    .line 426
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J

    move-result-wide v10

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    div-int v13, v1, v2

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->CreateLiveThumbnail(Ljava/lang/String;Ljava/lang/String;JII)V
    invoke-static/range {v7 .. v13}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$2100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Ljava/lang/String;Ljava/lang/String;JII)V

    .line 431
    .end local v12    # "thumbtime":I
    :cond_6
    :goto_2
    return-void

    .line 428
    .restart local v12    # "thumbtime":I
    :cond_7
    const-string v1, "videowall-SideChapterViewDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duration time is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$4;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ==> not Live but Static thumbnail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
