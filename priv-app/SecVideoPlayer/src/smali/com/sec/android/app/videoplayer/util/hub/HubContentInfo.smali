.class public Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
.super Ljava/lang/Object;
.source "HubContentInfo.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static sHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;


# instance fields
.field private bFromStore:Z

.field private bTrailer:Z

.field private bWfdSupport:Z

.field private mAppID:Ljava/lang/String;

.field private mFileSize:J

.field private mImei:Ljava/lang/String;

.field private mMVID:Ljava/lang/String;

.field private mOrderID:Ljava/lang/String;

.field private mProductId:I

.field private mResumePos:J

.field private mSvrID:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mUserGuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    .line 32
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    .line 34
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    .line 36
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    .line 52
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->sHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->sHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    .line 63
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->sHubContentInfo:Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    return-object v0
.end method

.method private init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 298
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    .line 299
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    .line 300
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    .line 301
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    .line 302
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    .line 303
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    .line 304
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    .line 305
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mResumePos:J

    .line 306
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    .line 307
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    .line 308
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    .line 309
    return-void
.end method


# virtual methods
.method public getAppID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    return-object v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    return-wide v0
.end method

.method public getImei()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    return-object v0
.end method

.method public getMVID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mProductId:I

    return v0
.end method

.method public getResumePos()J
    .locals 2

    .prologue
    .line 264
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mResumePos:J

    return-wide v0
.end method

.method public getSvrID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getUserGuid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    return-object v0
.end method

.method public isFromStore()Z
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    return v0
.end method

.method public isTrailer()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    return v0
.end method

.method public isWfdSupport()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    return v0
.end method

.method public setCustomData(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->init()V

    .line 116
    const-string v0, "app_Id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    .line 117
    const-string v0, "user_guid"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    .line 118
    const-string v0, "imei"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    .line 119
    const-string v0, "mv_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    .line 120
    const-string v0, "svr_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setData(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x1

    .line 129
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    .line 130
    const-string v0, "product_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mProductId:I

    .line 131
    const-string v0, "video_attr_type_code_2"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    .line 132
    const-string v0, "Y"

    const-string v1, "wfd_yn"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    .line 133
    const-string v0, "resumePos"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mResumePos:J

    .line 134
    const-string v0, "%011d"

    new-array v1, v6, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "order_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    .line 137
    const-string v0, "00"

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    const-string v0, "fhd_file_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    .line 146
    :goto_0
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    .line 147
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void

    .line 139
    :cond_0
    const-string v0, "01"

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    const-string v0, "hd_file_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    goto :goto_0

    .line 141
    :cond_1
    const-string v0, "02"

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 142
    const-string v0, "sd_file_size"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    goto :goto_0

    .line 144
    :cond_2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    goto :goto_0
.end method

.method public setData(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->init()V

    .line 74
    const-string v2, "order_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    .line 75
    const-string v2, "isfromMovieStore"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 76
    const-string v2, "title"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    .line 81
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 82
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 83
    sget-object v1, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->TAG:Ljava/lang/String;

    const-string v2, "setupMovieStore Not From MOVIE STORE!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :goto_1
    return v0

    .line 78
    :cond_0
    const-string v2, "title_of_movie"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    goto :goto_0

    .line 86
    :cond_1
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    goto :goto_1

    .line 90
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    .line 94
    :cond_3
    const-string v2, "appId"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    .line 95
    const-string v2, "user_guid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    .line 96
    const-string v2, "deviceUid"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    .line 97
    const-string v2, "mv_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    .line 98
    const-string v2, "svr_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    .line 100
    const-string v2, "all_share_cast"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    .line 101
    const-string v0, "paused_position"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mResumePos:J

    .line 103
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    .line 105
    sget-object v0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 106
    goto :goto_1
.end method

.method public setOrderID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setWfdSupport(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    .line 256
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HubContentInfo [mUserGuid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mUserGuid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAppID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mAppID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mImei="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mImei:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOrderID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mOrderID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTitle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMVID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mMVID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSvrID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mSvrID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bTrailer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bTrailer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bFromStore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bFromStore:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bWfdSupport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->bWfdSupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mResumePos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mResumePos:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mFileSize:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mProductId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mProductId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
