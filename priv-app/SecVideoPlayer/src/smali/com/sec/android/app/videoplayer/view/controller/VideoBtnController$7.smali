.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 640
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 641
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 671
    :cond_0
    :goto_0
    return v1

    .line 643
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v2, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_0

    .line 647
    :pswitch_1
    const-string v2, "VideoBtnController"

    const-string v3, "mShiftCntlBtnTouchListener call"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    if-ne v2, v3, :cond_0

    .line 651
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlRightPos()I

    move-result v3

    if-ne v3, v0, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setMiniCtrlRightPos(I)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    goto :goto_0

    .line 641
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
