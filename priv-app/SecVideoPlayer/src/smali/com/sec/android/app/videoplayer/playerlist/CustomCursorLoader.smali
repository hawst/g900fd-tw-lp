.class public Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;
.super Landroid/content/AsyncTaskLoader;
.source "CustomCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/AsyncTaskLoader",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field mCancellationSignal:Landroid/os/CancellationSignal;

.field mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field mContext:Landroid/content/Context;

.field mCursor:Landroid/database/Cursor;

.field mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field final mObserver:Landroid/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 55
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 56
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 57
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mContext:Landroid/content/Context;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bucketId"    # I
    .param p3, "searchKey"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Landroid/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    .line 63
    new-instance v0, Landroid/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/content/Loader$ForceLoadContentObserver;-><init>(Landroid/content/Loader;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    .line 64
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 65
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 66
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mContext:Landroid/content/Context;

    .line 67
    return-void
.end method

.method private getCursor()Landroid/database/Cursor;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 182
    const/4 v1, 0x0

    .line 184
    .local v1, "cursor":Landroid/database/Cursor;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v4

    .line 185
    .local v4, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v2

    .line 186
    .local v2, "filePath":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getBucketIdbyPath(Ljava/lang/String;)I

    move-result v0

    .line 187
    .local v0, "bucketId":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getListType()I

    move-result v3

    .line 189
    .local v3, "listType":I
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 190
    const/16 v5, 0x9

    if-ne v3, v5, :cond_1

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v5, v0, v8, v9, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    .line 192
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v5, v1, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v1

    .line 202
    :cond_0
    :goto_0
    return-object v1

    .line 194
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const/4 v6, -0x1

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7, v9, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    .line 195
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v5, v1, v9}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 198
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v5, v0, v8, v9, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getVideoCursor(IZLjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v1

    .line 199
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v5, v1, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public cancelLoadInBackground()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->cancelLoadInBackground()V

    .line 99
    monitor-enter p0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    invoke-virtual {v0}, Landroid/os/CancellationSignal;->cancel()V

    .line 103
    :cond_0
    monitor-exit p0

    .line 104
    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public deliverResult(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->isReset()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 119
    if-eqz p1, :cond_0

    .line 120
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 125
    .local v0, "oldCursor":Landroid/database/Cursor;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 128
    invoke-super {p0, p1}, Landroid/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    .line 131
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    return-void
.end method

.method public loadInBackground()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 72
    monitor-enter p0

    .line 73
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->isLoadInBackgroundCanceled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Landroid/os/OperationCanceledException;

    invoke-direct {v1}, Landroid/os/OperationCanceledException;-><init>()V

    throw v1

    .line 77
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 76
    :cond_0
    :try_start_1
    new-instance v1, Landroid/os/CancellationSignal;

    invoke-direct {v1}, Landroid/os/CancellationSignal;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 77
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    :try_start_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 81
    .local v0, "cursor":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    .line 83
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 89
    :cond_1
    monitor-enter p0

    .line 90
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 91
    monitor-exit p0

    return-object v0

    :catchall_1
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1

    .line 89
    .end local v0    # "cursor":Landroid/database/Cursor;
    :catchall_2
    move-exception v1

    monitor-enter p0

    .line 90
    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCancellationSignal:Landroid/os/CancellationSignal;

    .line 91
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v1

    :catchall_3
    move-exception v1

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    throw v1
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public onCanceled(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 163
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 166
    :cond_0
    return-void
.end method

.method public bridge synthetic onCanceled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 35
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->onCanceled(Landroid/database/Cursor;)V

    return-void
.end method

.method protected onReset()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/content/AsyncTaskLoader;->onReset()V

    .line 173
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->onStopLoading()V

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 178
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    .line 179
    return-void
.end method

.method protected onStartLoading()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->deliverResult(Landroid/database/Cursor;)V

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->takeContentChanged()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mCursor:Landroid/database/Cursor;

    if-nez v0, :cond_2

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->forceLoad()V

    .line 150
    :cond_2
    return-void
.end method

.method protected onStopLoading()V
    .locals 0

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->cancelLoad()Z

    .line 159
    return-void
.end method

.method registerContentObserver(Landroid/database/Cursor;Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;->mObserver:Landroid/content/Loader$ForceLoadContentObserver;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 112
    return-void
.end method
