.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnInfoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnInfoListener:Landroid/media/MediaPlayer$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnInfoListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/media/MediaPlayer$OnInfoListener;->onInfo(Landroid/media/MediaPlayer;II)Z

    .line 531
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 565
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 534
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mIsAudioOnlyClip:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    goto :goto_0

    .line 538
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInfoListener. MEDIA_INFO_SUBTITLE_TIMED_OUT E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->processWebVTTOutband(I)V

    goto :goto_0

    .line 545
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInfoListener. MEDIA_INFO_UNSUPPORTED_SUBTITLE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->processWebVTTOutband(I)V

    goto :goto_0

    .line 553
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "mInfoListener. MEDIA_INFO_METADATA_UPDATE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getOutbandSubtilteTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->addOutbandTextTrackInfo([Landroid/media/MediaPlayer$TrackInfo;)V

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->selectSubtitle()Z

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$6;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->processWebVTTOutband(I)V

    goto :goto_0

    .line 531
    :sswitch_data_0
    .sparse-switch
        0x322 -> :sswitch_3
        0x385 -> :sswitch_2
        0x386 -> :sswitch_1
        0x3b7 -> :sswitch_0
        0x3cd -> :sswitch_0
    .end sparse-switch
.end method
