.class public Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
.super Ljava/lang/Object;
.source "VideoSettingInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;
    }
.end annotation


# static fields
.field public static OFF:I = 0x0

.field public static ON:I = 0x0

.field public static final PLAYER_SETTING_CHANGE_ALL:I = 0x3e8

.field public static final PLAYER_SETTING_CHANGE_BRIGHTNESS:I = 0x3ea

.field public static final PLAYER_SETTING_CHANGE_CAPTURE:I = 0x3eb

.field public static final PLAYER_SETTING_CHANGE_MINI_CONTROLLER:I = 0x3e9

.field public static final PLAYER_SETTING_CHANGE_MINI_CONTROLLER_POS:I = 0x3ef

.field public static final PLAYER_SETTING_CHANGE_PLAYSPEED:I = 0x3ec

.field public static final PLAYER_SETTING_CHANGE_SA_EFFECT:I = 0x3ed

.field public static final PLAYER_SETTING_CHANGE_SCREEN_MODE:I = 0x3f0

.field public static final PLAYER_SETTING_PREIMUM_MODE:I = 0x3f1

.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

.field public static final mSettingChangeAllList:[I


# instance fields
.field private mCaptureVisibility:I

.field private mContext:Landroid/content/Context;

.field private mMiniCtrlMode:I

.field private mMiniCtrlRightPos:I

.field private mOnSettingDataChangedListener:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

.field private mPlaySpeedVisibility:I

.field private mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private mPremiumMode:I

.field private mSAEffect:I

.field private mScreenMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "VideoPlayerSettingInfo"

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSettingChangeAllList:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3e9
        0x3eb
        0x3ec
        0x3ed
    .end array-data
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mContext:Landroid/content/Context;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mOnSettingDataChangedListener:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

    .line 43
    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    .line 45
    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    .line 48
    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    .line 52
    sget v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    .line 54
    sget v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    .line 56
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    .line 59
    if-nez p1, :cond_1

    .line 60
    sput-object v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mContext:Landroid/content/Context;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 67
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->initSettingValue()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public static get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 77
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mInstance:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    return-object v0
.end method

.method private initSettingValue()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 81
    sget-object v2, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    const-string v3, "initSettingValue E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-nez v2, :cond_0

    .line 84
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    const-string v2, "initSettingValue. prefMgr is null. return"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :goto_0
    return v0

    .line 89
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "mini_controller_on"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    .line 90
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "mini_controller_right"

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "videoCapture"

    sget v4, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "playspeed"

    sget v4, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    .line 100
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "sound_effect"

    const/16 v4, 0xa

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "screen_mode"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    .line 105
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "screen_mode_settings"

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    .line 107
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initSettingValue. mCaptureVisibility : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mPlaySpeedVisibility : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSAEffect : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mMiniCtrlRightPos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mScreenMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mPremiumMode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 109
    goto/16 :goto_0
.end method

.method private sendDataChangeNotify(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mOnSettingDataChangedListener:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

    if-eqz v0, :cond_0

    .line 159
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendDataChangeNotify. notify type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mOnSettingDataChangedListener:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;->onSettingDataChanged(I)V

    .line 162
    :cond_0
    return-void
.end method

.method private updatePreference(II)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "mode"    # I

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-nez v1, :cond_0

    .line 114
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    const-string v2, "updatePreference. mPrefMgr is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_0
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x0

    .line 120
    .local v0, "keyStr":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 122
    :pswitch_1
    const-string v0, "mini_controller_on"

    .line 153
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto :goto_0

    .line 126
    :pswitch_2
    const-string v0, "videoCapture"

    .line 127
    goto :goto_1

    .line 130
    :pswitch_3
    const-string v0, "playspeed"

    .line 131
    goto :goto_1

    .line 134
    :pswitch_4
    const-string v0, "sound_effect"

    .line 135
    goto :goto_1

    .line 138
    :pswitch_5
    const-string v0, "mini_controller_right"

    .line 139
    goto :goto_1

    .line 142
    :pswitch_6
    const-string v0, "screen_mode"

    .line 143
    goto :goto_1

    .line 146
    :pswitch_7
    const-string v0, "screen_mode_settings"

    .line 147
    goto :goto_1

    .line 120
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public getCaptureVisibility()I
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    return v0
.end method

.method public getMiniCtrlMode()I
    .locals 3

    .prologue
    .line 264
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiniCtrlMode. mMiniCtrlMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    return v0
.end method

.method public getMiniCtrlRightPos()I
    .locals 3

    .prologue
    .line 274
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiniCtrlRightPos. mMiniCtrlRightPos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    return v0
.end method

.method public getPlaySpeedVisibility()I
    .locals 3

    .prologue
    .line 284
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPlaySpeedVisibility. mPlaySpeedVisibility : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    return v0
.end method

.method public getPremiumMode()I
    .locals 3

    .prologue
    .line 269
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPremiumMode. mPremiumMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    return v0
.end method

.method public getSAEffectMode()I
    .locals 3

    .prologue
    .line 289
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSAEffectMode. mSAEffect : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    return v0
.end method

.method public getScreenMode()I
    .locals 3

    .prologue
    .line 294
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getScreenMode. mScreenMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    return v0
.end method

.method public setCaptureVisibility(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/16 v3, 0x3eb

    .line 199
    if-ltz p1, :cond_0

    sget v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    if-le p1, v0, :cond_1

    .line 200
    :cond_0
    sget p1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    .line 203
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCaptureVisibility. mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    if-eq v0, p1, :cond_2

    .line 206
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mCaptureVisibility:I

    .line 207
    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 208
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 210
    :cond_2
    return-void
.end method

.method public setMiniCtrlMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/16 v1, 0x3e9

    .line 183
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    if-eq v0, p1, :cond_0

    .line 184
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlMode:I

    .line 185
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 186
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public setMiniCtrlRightPos(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/16 v1, 0x3ef

    .line 175
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    .line 176
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mMiniCtrlRightPos:I

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 178
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 180
    return-void
.end method

.method public setPlaySpeedVisibility(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/16 v1, 0x3ec

    .line 213
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    if-eq v0, p1, :cond_0

    .line 214
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPlaySpeedVisibility:I

    .line 215
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 216
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 218
    :cond_0
    return-void
.end method

.method public setPremiumMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    const/16 v1, 0x3f1

    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    if-eq v0, p1, :cond_0

    .line 192
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mPremiumMode:I

    .line 193
    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 194
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 196
    :cond_0
    return-void
.end method

.method public setSAEffectMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/16 v2, 0x3ed

    .line 222
    const/16 v1, 0xa

    if-eq p1, v1, :cond_1

    .line 223
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 225
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    :cond_0
    const/16 p1, 0xa

    .line 234
    .end local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_1
    :goto_0
    iget v1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    if-eq v1, p1, :cond_2

    .line 235
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSAEffect:I

    .line 236
    invoke-direct {p0, v2, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 237
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 239
    :cond_2
    return-void

    .line 227
    .restart local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_3
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-nez v1, :cond_1

    .line 228
    const/16 v1, 0xb

    if-eq p1, v1, :cond_1

    .line 229
    const/16 p1, 0xa

    goto :goto_0
.end method

.method public setScreenMode()V
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setScreenMode(I)V

    .line 242
    return-void
.end method

.method public setScreenMode(I)V
    .locals 4
    .param p1, "newMode"    # I

    .prologue
    const/16 v3, 0x3f0

    .line 246
    const/4 v0, 0x3

    if-le p1, v0, :cond_0

    .line 247
    const/4 p1, 0x0

    .line 251
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isWebVTTFileType()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 253
    const/4 p1, 0x2

    .line 256
    :cond_2
    iput p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mScreenMode:I

    .line 257
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setScreenMode. newMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-direct {p0, v3, p1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->updatePreference(II)V

    .line 260
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->sendDataChangeNotify(I)V

    .line 261
    return-void
.end method

.method public setSettingDataChangedListener(Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mOnSettingDataChangedListener:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;

    .line 171
    return-void
.end method
