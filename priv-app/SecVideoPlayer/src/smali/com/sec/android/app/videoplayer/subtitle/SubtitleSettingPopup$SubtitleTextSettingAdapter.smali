.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SubtitleSettingPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SubtitleTextSettingAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final mMenuType:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/content/Context;ILjava/util/ArrayList;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resourceId"    # I
    .param p5, "menuType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1359
    .local p4, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 1360
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1361
    iput p5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->mMenuType:I

    .line 1362
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 1365
    if-nez p2, :cond_0

    .line 1366
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 1367
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030037

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 1370
    .end local v3    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 1371
    const-string v4, "VideoSubtitlePopup"

    const-string v5, "SubtitleFontAdapter : getView() - getCount() is 0"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1402
    :goto_0
    return-object p2

    .line 1375
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1376
    .local v0, "item":Ljava/lang/String;
    const v4, 0x7f0d01bc

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1378
    .local v1, "textView":Landroid/widget/TextView;
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->mMenuType:I

    packed-switch v4, :pswitch_data_0

    .line 1395
    const-string v4, "VideoSubtitlePopup"

    const-string v5, "SubtitleFontAdapter - getView - Why AM I Here?"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1396
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->stackTrace()V

    .line 1400
    :cond_2
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1380
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->applyFont(Ljava/lang/String;Landroid/widget/TextView;)V

    goto :goto_1

    .line 1384
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    const/4 v5, 0x1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setFontEdge(Landroid/widget/TextView;IZ)V
    invoke-static {v4, v1, p1, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$2200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Landroid/widget/TextView;IZ)V

    .line 1385
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    .line 1386
    .local v2, "tf":Landroid/graphics/Typeface;
    if-eqz v2, :cond_2

    .line 1387
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1

    .line 1391
    .end local v2    # "tf":Landroid/graphics/Typeface;
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$SubtitleTextSettingAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v4, p1, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->applyTextSize(ILandroid/widget/TextView;)V

    goto :goto_1

    .line 1378
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
