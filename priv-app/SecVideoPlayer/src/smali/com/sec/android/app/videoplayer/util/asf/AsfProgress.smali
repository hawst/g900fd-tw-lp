.class public Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;
.super Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;
.source "AsfProgress.java"


# static fields
.field private static sAsfProgress:Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;


# instance fields
.field private mProgress:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->sAsfProgress:Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method private create()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 62
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00fc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress$1;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress$2;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 87
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->sAsfProgress:Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 27
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 51
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    if-nez v1, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->create()V

    .line 55
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_1

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 59
    :cond_1
    return-void
.end method
