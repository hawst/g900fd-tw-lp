.class Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;
.super Ljava/lang/Object;
.source "VideoPlaySpeedPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 165
    const-string v0, "VideoPlaySpeedPopup"

    const-string v1, "setNegativeButton"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeed:I
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$200()I

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setPlaySpeed(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;I)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->mPlaySpeedPopup:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->access$402(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 172
    :cond_0
    return-void
.end method
