.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 6
    .param p1, "focusChange"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 866
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAudioFocusChange focusChange : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-nez v1, :cond_1

    .line 870
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "abandonAudioFocus listener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->abandonAudioFocus()V

    .line 914
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 887
    :pswitch_1
    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2002(Z)Z

    .line 889
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 890
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mAudioFocusListener. pause by alert sound"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iput-boolean v5, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 892
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto :goto_0

    .line 877
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 878
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iput-boolean v4, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 879
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 881
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "abandonAudioFocus for AUDIOFOCUS_LOSS"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2002(Z)Z

    goto :goto_0

    .line 897
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-boolean v1, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    if-eqz v1, :cond_0

    .line 898
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iput-boolean v4, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    .line 899
    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mHasAudioFocus:Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2002(Z)Z

    .line 900
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v1, :cond_3

    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v1, :cond_3

    .line 901
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 902
    .local v0, "config":Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "sub_lcd_auto_lock"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 904
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mAudioFocusListener. folder close && auto lock"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 908
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resume()V

    goto/16 :goto_0

    .line 875
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
