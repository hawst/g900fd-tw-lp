.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SubtitleSelectPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectSubtitleAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private mSubtitleActive:Z

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "subtitleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const/4 v0, 0x0

    .line 303
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 304
    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 301
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->mSubtitleActive:Z

    .line 305
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->mSubtitleActive:Z

    .line 306
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 309
    if-nez p2, :cond_0

    .line 310
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v8

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/LayoutInflater;

    .line 311
    .local v7, "vi":Landroid/view/LayoutInflater;
    const v8, 0x7f03002d

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 314
    .end local v7    # "vi":Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getCount()I

    move-result v8

    if-nez v8, :cond_2

    .line 379
    :cond_1
    :goto_0
    return-object p2

    .line 318
    :cond_2
    const v8, 0x7f0d018d

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 319
    .local v4, "subtitleName":Landroid/widget/TextView;
    const v8, 0x7f0d018e

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 320
    .local v5, "subtitlePath":Landroid/widget/TextView;
    const v8, 0x7f0d018f

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 322
    .local v0, "checkImage":Landroid/widget/CheckedTextView;
    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 324
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    .line 325
    .local v2, "path":Ljava/lang/String;
    const/16 v8, 0x2f

    invoke-virtual {v2, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    .line 327
    .local v3, "pos":I
    if-lez v3, :cond_3

    .line 328
    const/4 v8, 0x0

    add-int/lit8 v9, v3, 0x1

    invoke-virtual {v2, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 331
    :cond_3
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 332
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a0138

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a0035

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a001d

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 337
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->mSubtitleActive:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 342
    :cond_5
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v10, 0x7f0a001d

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 343
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 344
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 360
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a0138

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 361
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setVisibility(I)V

    .line 364
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getCount()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_c

    .line 365
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a001d

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 366
    const/4 v8, 0x0

    invoke-virtual {p3, v8}, Landroid/view/ViewGroup;->setEnabled(Z)V

    .line 367
    const/4 v8, 0x0

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 368
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setEnabled(Z)V

    .line 369
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 370
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a013e

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 346
    :cond_7
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 349
    :cond_8
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 351
    :cond_9
    if-nez p1, :cond_a

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    .line 352
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto :goto_1

    .line 353
    :cond_a
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v8

    if-eqz v8, :cond_b

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;

    move-result-object v9

    invoke-virtual {v9}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 355
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_1

    .line 357
    :cond_b
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    goto/16 :goto_1

    .line 373
    :cond_c
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    const v9, 0x7f0a001d

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 374
    const v8, 0x7f0d018c

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 375
    .local v6, "subtitlenamelayout":Landroid/widget/LinearLayout;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0800dd

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    goto/16 :goto_0
.end method
