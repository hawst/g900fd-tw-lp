.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;
.super Ljava/lang/Object;
.source "SubtitleDump.java"


# static fields
.field private static mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;


# instance fields
.field private languages:[Ljava/lang/String;

.field private mSubtitleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private noofLanguages:I

.field sub:[B

.field private subtitle:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->sub:[B

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleList:Ljava/util/ArrayList;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    .line 27
    return-void
.end method

.method private convertToString([B)V
    .locals 13
    .param p1, "sub2"    # [B

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 45
    if-eqz p1, :cond_7

    .line 46
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, p1}, Ljava/lang/String;-><init>([B)V

    .line 48
    .local v5, "s":Ljava/lang/String;
    const-string v9, "\n"

    invoke-virtual {v5, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    .line 49
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v11

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "a":[Ljava/lang/String;
    aget-object v9, v0, v12

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->noofLanguages:I

    .line 51
    iget v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->noofLanguages:I

    new-array v9, v9, [Ljava/lang/String;

    iput-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->languages:[Ljava/lang/String;

    .line 52
    const/4 v4, 0x0

    .line 55
    .local v4, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    array-length v9, v9

    if-ge v2, v9, :cond_1

    .line 56
    iget v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->noofLanguages:I

    if-ge v4, v9, :cond_0

    .line 57
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, "(.*)TRACK(.*)LANG(.*)"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 58
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, ":"

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 59
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->languages:[Ljava/lang/String;

    array-length v10, v0

    add-int/lit8 v10, v10, -0x1

    aget-object v10, v0, v10

    aput-object v10, v9, v4

    .line 60
    add-int/lit8 v4, v4, 0x1

    .line 55
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 67
    :cond_1
    const/4 v2, 0x0

    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    array-length v9, v9

    if-ge v2, v9, :cond_8

    .line 69
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, "(.*)TRACK(.*)LANG:(.*)"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 70
    add-int/lit8 v2, v2, 0x1

    .line 71
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, "(.*)TRACK(.*)LANG:"

    invoke-virtual {v9, v10}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    if-eq v2, v9, :cond_3

    .line 72
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 73
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v3, v9, v2

    .line 74
    .local v3, "index":Ljava/lang/String;
    const-string v9, "[0-9]+"

    invoke-virtual {v3, v9}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 67
    .end local v3    # "index":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 76
    .restart local v3    # "index":Ljava/lang/String;
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 77
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, " --> "

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 78
    .local v6, "ss":[Ljava/lang/String;
    aget-object v7, v6, v11

    .line 79
    .local v7, "starttime":Ljava/lang/String;
    aget-object v1, v6, v12

    .line 80
    .local v1, "endtime":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    .line 81
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v8, v9, v2

    .line 82
    .local v8, "text":Ljava/lang/String;
    add-int/lit8 v2, v2, 0x1

    .line 84
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    const-string v10, ""

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 85
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 86
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 87
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->subtitle:[Ljava/lang/String;

    aget-object v9, v9, v2

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 88
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 90
    :cond_5
    const-string v9, "\\<.*?\\>"

    const-string v10, " "

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 91
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    .line 92
    invoke-direct {p0, v3, v7, v1, v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->makeSubtitleList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 94
    .end local v1    # "endtime":Ljava/lang/String;
    .end local v3    # "index":Ljava/lang/String;
    .end local v6    # "ss":[Ljava/lang/String;
    .end local v7    # "starttime":Ljava/lang/String;
    .end local v8    # "text":Ljava/lang/String;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 101
    .end local v0    # "a":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v4    # "j":I
    .end local v5    # "s":Ljava/lang/String;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->resetSubtitleList()V

    .line 103
    :cond_8
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    .line 34
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleDump:Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;

    return-object v0
.end method

.method private makeSubtitleList(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "index"    # Ljava/lang/String;
    .param p2, "starttime"    # Ljava/lang/String;
    .param p3, "endtime"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;

    .prologue
    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    const-string v1, "SUBTITLE_INDEX"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    const-string v1, "SUBTITLE_START_TIME"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    const-string v1, "SUBTITLE_END_TIME"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    const-string v1, "SUBTITLE_TEXT"

    invoke-virtual {v0, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->map:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    return-void
.end method


# virtual methods
.method public NoOfLanguages()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public getSubtitleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public initdata([B)V
    .locals 0
    .param p1, "bs"    # [B

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->sub:[B

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->convertToString([B)V

    .line 41
    return-void
.end method

.method public resetSubtitleList()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleDump;->mSubtitleList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 107
    return-void
.end method
