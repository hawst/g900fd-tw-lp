.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 545
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v7, 0xbb8

    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 547
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 548
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 578
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 550
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_0

    .line 554
    :pswitch_1
    const-string v2, "VideoBtnController"

    const-string v3, "mPlayerListBtnTouchListener call"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 557
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 558
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 559
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hidePlayerList(Z)V

    .line 561
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 568
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changePlayerListBtn()V

    goto :goto_0

    .line 563
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->showPlayerList(Z)V

    .line 564
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const v3, 0x36ee80

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_1

    .line 548
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
