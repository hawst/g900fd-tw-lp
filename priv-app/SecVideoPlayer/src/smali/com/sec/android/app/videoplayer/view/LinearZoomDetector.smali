.class public Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;
.super Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
.source "LinearZoomDetector.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;


# static fields
.field private static final HIDE_RECTVIEW:I = 0x1

.field private static final RECTVIEW_HIDE_DELEY:I = 0x5dc

.field private static final TAG:Ljava/lang/String; = "LinearZoomDetector"


# instance fields
.field private mCurrDeltaX:I

.field private mCurrDeltaY:I

.field private mCurrZoomBottom:I

.field private mCurrZoomLeft:I

.field private mCurrZoomRight:I

.field private mCurrZoomTop:I

.field private mGestureDetectors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

.field private mZoomPercentWithMargin:F

.field protected mbInPanningMode:Z

.field public mbIsPanEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "app"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;-><init>(Landroid/content/Context;)V

    .line 18
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    .line 19
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mbInPanningMode:Z

    .line 21
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mbIsPanEnabled:Z

    .line 22
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaX:I

    .line 23
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaY:I

    .line 24
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    .line 25
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    .line 27
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    .line 28
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mZoomPercentWithMargin:F

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    .line 330
    new-instance v0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector$1;-><init>(Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mHandler:Landroid/os/Handler;

    .line 37
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/PanDetector;

    invoke-direct {v1, p1, p0}, Lcom/sec/android/app/videoplayer/view/PanDetector;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;

    invoke-direct {v1, p1, p0}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->hidePanRectView()V

    return-void
.end method

.method private applyCrop(IIII)Z
    .locals 10
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 141
    const/4 v0, 0x1

    .line 143
    .local v0, "bValuesAccepted":Z
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 145
    .local v5, "surface":Landroid/view/SurfaceView;
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getWidth()I

    move-result v3

    .line 146
    .local v3, "nWidth":I
    invoke-virtual {v5}, Landroid/view/SurfaceView;->getHeight()I

    move-result v1

    .line 147
    .local v1, "nHeight":I
    if-ltz p1, :cond_0

    if-ltz p2, :cond_0

    if-le p3, p1, :cond_0

    if-gt p3, v3, :cond_0

    if-le p4, p2, :cond_0

    if-gt p4, v1, :cond_0

    .line 149
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    .line 150
    iput p2, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    .line 151
    iput p3, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    .line 152
    iput p4, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    .line 153
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->frameWidth()I

    move-result v6

    int-to-float v6, v6

    int-to-float v7, v3

    div-float v4, v6, v7

    .line 154
    .local v4, "nWidthRatio":F
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->frameHeight()I

    move-result v6

    int-to-float v6, v6

    int-to-float v7, v1

    div-float v2, v6, v7

    .line 155
    .local v2, "nHeightRatio":F
    const-string v6, "LinearZoomDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "applyCrop nWidthRatio:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " nHeightRatio:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    iget v6, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    int-to-float v6, v6

    mul-float/2addr v6, v4

    float-to-int v6, v6

    iget v7, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    int-to-float v7, v7

    mul-float/2addr v7, v2

    float-to-int v7, v7

    iget v8, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    int-to-float v8, v8

    mul-float/2addr v8, v4

    float-to-int v8, v8

    iget v9, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    int-to-float v9, v9

    mul-float/2addr v9, v2

    float-to-int v9, v9

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->setVideoCrop(IIII)I

    .line 159
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->setZoomState(Z)V

    .line 160
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->invalidatePanRect()V

    .line 165
    .end local v2    # "nHeightRatio":F
    .end local v4    # "nWidthRatio":F
    :goto_0
    return v0

    .line 162
    :cond_0
    const-string v6, "LinearZoomDetector"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "applyCrop (IGNORING) l:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " t:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " r:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " b: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hidePanRectView()V
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/PanRectView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setVisibility(I)V

    .line 244
    :cond_0
    return-void
.end method

.method private invalidatePanRect()V
    .locals 3

    .prologue
    .line 269
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v1, :cond_0

    .line 270
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 272
    .local v0, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setVideoWidth(I)V

    .line 273
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setVideoHeight(I)V

    .line 275
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setZoomedLeft(I)V

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setZoomedTop(I)V

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setZoomedRight(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setZoomedBottom(I)V

    .line 279
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 282
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->hidePanRectView()V

    .line 287
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/PanRectView;->postInvalidate()V

    .line 289
    .end local v0    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_0
    return-void

    .line 284
    .restart local v0    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->showPanRectView()V

    goto :goto_0
.end method

.method private showPanRectView()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 247
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/PanRectView;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setVisibility(I)V

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/PanRectView;->requestLayout()V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 255
    :cond_1
    return-void
.end method


# virtual methods
.method public adjustBoundary(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    .line 297
    iget v3, p1, Landroid/graphics/Rect;->left:I

    if-gez v3, :cond_0

    .line 298
    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget v4, p1, Landroid/graphics/Rect;->left:I

    rsub-int/lit8 v4, v4, 0x0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->right:I

    .line 299
    iput v5, p1, Landroid/graphics/Rect;->left:I

    .line 301
    :cond_0
    iget v3, p1, Landroid/graphics/Rect;->top:I

    if-gez v3, :cond_1

    .line 302
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->top:I

    rsub-int/lit8 v4, v4, 0x0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->bottom:I

    .line 303
    iput v5, p1, Landroid/graphics/Rect;->top:I

    .line 306
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 308
    .local v2, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v1

    .line 309
    .local v1, "nWidth":I
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v0

    .line 311
    .local v0, "nHeiht":I
    iget v3, p1, Landroid/graphics/Rect;->right:I

    if-le v3, v1, :cond_2

    .line 312
    iget v3, p1, Landroid/graphics/Rect;->left:I

    iget v4, p1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->left:I

    .line 313
    iput v1, p1, Landroid/graphics/Rect;->right:I

    .line 315
    :cond_2
    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    if-le v3, v0, :cond_3

    .line 316
    iget v3, p1, Landroid/graphics/Rect;->top:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v0

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p1, Landroid/graphics/Rect;->top:I

    .line 317
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 319
    :cond_3
    return-void
.end method

.method public adjustZoomState(I)V
    .locals 4
    .param p1, "Area"    # I

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->isZoomEnabled()Z

    move-result v0

    .line 122
    .local v0, "prevZoomState":Z
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->getZoomPercent(I)F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 123
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->setZoomState(Z)V

    .line 128
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->isZoomEnabled()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 129
    const-string v1, "LinearZoomDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ZOOM State = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->isZoomEnabled()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    return-void

    .line 125
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->setZoomState(Z)V

    goto :goto_0
.end method

.method public applyCrop(Landroid/graphics/Rect;)Z
    .locals 4
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 185
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->applyCrop(IIII)Z

    move-result v0

    return v0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/PanRectView;->bringToFront()V

    .line 294
    :cond_0
    return-void
.end method

.method public getCropRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 170
    .local v0, "rect":Landroid/graphics/Rect;
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 171
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 172
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 173
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 174
    return-object v0
.end method

.method public getDelta()Landroid/graphics/Point;
    .locals 2

    .prologue
    .line 178
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 179
    .local v0, "deltaPoint":Landroid/graphics/Point;
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaX:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 180
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaY:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 181
    return-object v0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->frameHeight()I

    move-result v0

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->frameWidth()I

    move-result v0

    return v0
.end method

.method public getZoomPercentWithMargin()F
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mZoomPercentWithMargin:F

    return v0
.end method

.method public isPanEnabled()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mbIsPanEnabled:Z

    return v0
.end method

.method public isZoomEnabled()Z
    .locals 1

    .prologue
    .line 137
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v0

    return v0
.end method

.method public onLongPress()V
    .locals 2

    .prologue
    .line 322
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 323
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onLongPress()V

    .line 322
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 325
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->updateVideoFrameData()V

    .line 59
    new-instance v0, Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->getVideoWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->getVideoHeight()I

    move-result v5

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/view/PanRectView;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 61
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setVisibility(I)V

    .line 65
    :cond_0
    const/4 v9, 0x0

    .line 66
    .local v9, "isGestureTracked":Z
    const/4 v8, 0x0

    .local v8, "index":I
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_1

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->isGestureTracked()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 68
    const/4 v9, 0x1

    .line 72
    :cond_1
    if-nez v9, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->isZoomEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->resetCropRect()V

    .line 76
    :cond_2
    const-string v0, "EVENTMAP LinearZoom"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received event"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const/4 v6, 0x0

    .line 78
    .local v6, "bHandled":Z
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 79
    .local v7, "detectorCount":I
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v7, :cond_3

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    .line 93
    if-nez v8, :cond_5

    .line 94
    if-ne v6, v10, :cond_5

    move v2, v10

    .line 107
    :cond_3
    return v2

    .line 66
    .end local v6    # "bHandled":Z
    .end local v7    # "detectorCount":I
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 102
    .restart local v6    # "bHandled":Z
    .restart local v7    # "detectorCount":I
    :cond_5
    if-eqz v6, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->isGestureTracked()Z

    move-result v0

    if-nez v0, :cond_3

    .line 79
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_1
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 112
    const-string v1, "LinearZoomDetector"

    const-string v2, " reset "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->setZoomState(Z)V

    .line 115
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mGestureDetectors:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    return-void
.end method

.method public resetCropRect()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 201
    const-string v1, "LinearZoomDetector"

    const-string v2, "Crop Rect is being reset"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->updateVideoFrameData()V

    .line 205
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomLeft:I

    .line 206
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomTop:I

    .line 208
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 210
    .local v0, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomRight:I

    .line 211
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrZoomBottom:I

    .line 213
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->resetPanRect()V

    .line 214
    return-void
.end method

.method public resetPanRect()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 261
    .local v6, "mainView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 262
    new-instance v0, Lcom/sec/android/app/videoplayer/view/PanRectView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->getVideoWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->getVideoHeight()I

    move-result v5

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/view/PanRectView;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 264
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->invalidatePanRect()V

    .line 266
    .end local v6    # "mainView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    :cond_0
    return-void
.end method

.method public resetXYDelta()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaX:I

    .line 218
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaY:I

    .line 219
    return-void
.end method

.method public setDelta(Landroid/graphics/Point;)V
    .locals 2
    .param p1, "deltaPoint"    # Landroid/graphics/Point;

    .prologue
    .line 222
    const-string v0, "LinearZoomDetector"

    const-string v1, "Delta Set"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget v0, p1, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaX:I

    .line 224
    iget v0, p1, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mCurrDeltaY:I

    .line 225
    return-void
.end method

.method public setPanEnabled(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mbIsPanEnabled:Z

    .line 190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->mPanRectView:Lcom/sec/android/app/videoplayer/view/PanRectView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/PanRectView;->invalidate()V

    .line 194
    :cond_0
    return-void
.end method

.method public setZoomState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 133
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/view/PanRectView;->setZoomMode(Z)V

    .line 134
    return-void
.end method

.method public updateVideoFrameData()V
    .locals 0

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/LinearZoomDetector;->updateVideoFrameProperties()V

    .line 229
    return-void
.end method
