.class public final enum Lcom/sec/android/app/videoplayer/Video360/Sensor360;
.super Ljava/lang/Enum;
.source "Sensor360.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;,
        Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;,
        Lcom/sec/android/app/videoplayer/Video360/Sensor360$CompassListener;,
        Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/Video360/Sensor360;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/Video360/Sensor360;

.field public static final enum INSTANCE:Lcom/sec/android/app/videoplayer/Video360/Sensor360;

.field public static final SENSOR_DELAY_FASTEST:I = 0x0

.field public static final SENSOR_DELAY_GAME:I = 0x1

.field public static final SENSOR_DELAY_NORMAL:I = 0x3

.field public static final SENSOR_DELAY_UI:I = 0x2

.field private static final TAG:Ljava/lang/String; = "Sensor360"

.field private static final TIMER_INTERVAL:I = 0xf

.field private static mAccIsReady:Z

.field private static mHandler:Landroid/os/Handler;

.field private static mMagIsReady:Z

.field private static final mObject:Ljava/lang/Object;

.field private static mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

.field private static sAccMagMatrix:[F

.field private static sAccelTimeStamp:J

.field private static sGravity:[F

.field private static sGyroMatrix:[F

.field private static sGyroTimeStamp:J

.field private static sMagnet:[F

.field private static sRotationListeners:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

.field private static sSensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x9

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->INSTANCE:Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->INSTANCE:Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->$VALUES:[Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    .line 24
    sput-object v5, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;

    .line 29
    sput-boolean v2, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z

    .line 30
    sput-boolean v2, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z

    .line 32
    sput-object v5, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mObject:Ljava/lang/Object;

    .line 39
    new-array v0, v3, [F

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sMagnet:[F

    .line 42
    new-array v0, v3, [F

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F

    .line 45
    new-array v0, v4, [F

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F

    .line 47
    new-array v0, v4, [F

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccMagMatrix:[F

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 307
    return-void
.end method

.method static synthetic access$1000()Z
    .locals 1

    .prologue
    .line 12
    sget-boolean v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z

    return v0
.end method

.method static synthetic access$1002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 12
    sput-boolean p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z

    return p0
.end method

.method static synthetic access$1100()J
    .locals 2

    .prologue
    .line 12
    sget-wide v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$1102(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 12
    sput-wide p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J

    return-wide p0
.end method

.method static synthetic access$1200()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/util/Vector;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$1400()Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    return-object v0
.end method

.method static synthetic access$1500()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200()Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    return-object v0
.end method

.method static synthetic access$300()Landroid/hardware/SensorManager;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$400()J
    .locals 2

    .prologue
    .line 12
    sget-wide v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccelTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$402(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 12
    sput-wide p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccelTimeStamp:J

    return-wide p0
.end method

.method static synthetic access$500()[F
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F

    return-object v0
.end method

.method static synthetic access$600()[F
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sMagnet:[F

    return-object v0
.end method

.method static synthetic access$700()Z
    .locals 1

    .prologue
    .line 12
    sget-boolean v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z

    return v0
.end method

.method static synthetic access$702(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 12
    sput-boolean p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z

    return p0
.end method

.method static synthetic access$800()[F
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F

    return-object v0
.end method

.method static synthetic access$802([F)[F
    .locals 0
    .param p0, "x0"    # [F

    .prologue
    .line 12
    sput-object p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F

    return-object p0
.end method

.method static synthetic access$900()[F
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccMagMatrix:[F

    return-object v0
.end method

.method public static getLastMagnetData()[F
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sMagnet:[F

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 81
    :goto_0
    return-void

    .line 75
    :cond_0
    const-string v0, "sensor"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;

    .line 76
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;-><init>(Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    .line 77
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->initialize()V

    .line 78
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    .line 80
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method public static pauseSensor()V
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 101
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->stopSensor()V

    .line 102
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;

    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 104
    :cond_0
    return-void
.end method

.method public static registerRotationListener(Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;)V
    .locals 2
    .param p0, "aListener"    # Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    .prologue
    .line 107
    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mObject:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_0
    monitor-exit v1

    .line 111
    return-void

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static resumeSensor(III)V
    .locals 2
    .param p0, "accelRate"    # I
    .param p1, "gyroRate"    # I
    .param p2, "magRate"    # I

    .prologue
    .line 88
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 89
    invoke-static {p0, p1, p2}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->startSensor(III)V

    .line 91
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;-><init>(Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    .line 92
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->run()V

    .line 94
    :cond_0
    return-void
.end method

.method private static startSensor(III)V
    .locals 1
    .param p0, "accelRate"    # I
    .param p1, "gyroRate"    # I
    .param p2, "magRate"    # I

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;-><init>(III)V

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->start()V

    .line 140
    return-void
.end method

.method private static stopSensor()V
    .locals 2

    .prologue
    .line 143
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    if-eqz v0, :cond_0

    .line 144
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;

    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 146
    :cond_0
    return-void
.end method

.method public static unregisterRotationListener(Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;)V
    .locals 2
    .param p0, "aListener"    # Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    .prologue
    .line 114
    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mObject:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;

    invoke-virtual {v0, p0}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 116
    monitor-exit v1

    .line 117
    return-void

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/Video360/Sensor360;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/Video360/Sensor360;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->$VALUES:[Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/Video360/Sensor360;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/Video360/Sensor360;

    return-object v0
.end method
