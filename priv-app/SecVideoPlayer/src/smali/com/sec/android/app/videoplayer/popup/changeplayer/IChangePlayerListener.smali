.class public abstract Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
.super Ljava/lang/Object;
.source "IChangePlayerListener.java"


# static fields
.field public static STARTED:Z

.field public static UPDATE:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->UPDATE:Z

    .line 24
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->STARTED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 35
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;->setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerListenerNew;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract registerReceiver()V
.end method

.method public abstract unregisterReceiver()V
.end method
