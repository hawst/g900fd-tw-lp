.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;
.super Landroid/os/Handler;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 477
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0x14

    const/16 v4, 0x6f

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 479
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 603
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 481
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 482
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget v2, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_1

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v3, 0x0

    iput v3, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    .line 484
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 493
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget v3, v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setVolume(F)V

    goto :goto_0

    .line 486
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget v3, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    const v4, 0x3c23d70a    # 0.01f

    add-float/2addr v3, v4

    iput v3, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    .line 487
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget v2, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_2

    .line 488
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 490
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iput v5, v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    goto :goto_1

    .line 498
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x65

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 502
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    goto :goto_0

    .line 506
    :pswitch_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlayCheck()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    goto :goto_0

    .line 510
    :pswitch_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPauseCheck()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    goto :goto_0

    .line 514
    :pswitch_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isStopCheck()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    goto :goto_0

    .line 518
    :pswitch_7
    const-string v2, "MoviePlaybackService"

    const-string v3, "mVideoPlayerHandler. dla_Success_start_playback E"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 521
    const-string v2, "MoviePlaybackService"

    const-string v3, "mVideoPlayerHandler. mServiceInUse is false. return"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 527
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->drm_uri:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setDataSourcePrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 528
    :catch_0
    move-exception v0

    .line 529
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 530
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException occured  1 :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 535
    .end local v0    # "e":Ljava/io/IOException;
    :pswitch_8
    const-string v2, "MoviePlaybackService"

    const-string v3, "DrmLicenseExpired in handler"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x71

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto/16 :goto_0

    .line 540
    :pswitch_9
    const-string v2, "MoviePlaybackService"

    const-string v3, "DrmLicenseNotFound in handler"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 542
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x72

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto/16 :goto_0

    .line 546
    :pswitch_a
    const-string v2, "MoviePlaybackService"

    const-string v3, "ErrDrmRightsAcquisitionFailed in handler"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 548
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/16 v3, 0x6d

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto/16 :goto_0

    .line 552
    :pswitch_b
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RESUME_PLAYBACK_BY_CALLSTATE : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 554
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$900(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v2

    const/16 v3, 0xa

    if-ge v2, v3, :cond_4

    .line 555
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xc

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 556
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # operator++ for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$908(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    goto/16 :goto_0

    .line 558
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    .line 559
    .local v1, "playerState":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 560
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 561
    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 562
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    goto/16 :goto_0

    .line 563
    :cond_5
    if-nez v1, :cond_6

    .line 564
    const-string v2, "MoviePlaybackService"

    const-string v3, "Already prepared. Nothing to do. (remove duplicated play command and preventing flickering when it auto resumed.)"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 566
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startPlay()Z

    goto/16 :goto_0

    .line 573
    .end local v1    # "playerState":I
    :pswitch_c
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # ++operator for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1504(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_7

    .line 574
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 575
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pause()V

    goto/16 :goto_0

    .line 578
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    goto/16 :goto_0

    .line 587
    :pswitch_d
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.exit"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 591
    :pswitch_e
    const-string v2, "MoviePlaybackService"

    const-string v3, "DELAY_TREED_VIDE : "

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_8

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_8

    .line 594
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    .line 596
    :cond_8
    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.stateview.visibility"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "visibility"

    const-string v5, "hide"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_b
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_c
    .end packed-switch
.end method
