.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0

    .prologue
    .line 717
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v3, 0x0

    .line 719
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerListener:Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->unregisterReceiver()V

    .line 723
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->bConnectingScreenMirroring:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 725
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->setActivityState(Z)V

    .line 727
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.wfd.CHANGEPLAYER_CANCELED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 728
    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sendBroadcast. com.samsung.wfd.CHANGEPLAYER_CANCELED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 733
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$100()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v3, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 736
    return-void
.end method
