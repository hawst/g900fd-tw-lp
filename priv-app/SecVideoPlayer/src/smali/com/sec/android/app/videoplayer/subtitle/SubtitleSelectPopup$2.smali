.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 241
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    .line 243
    .local v1, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTrackType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 244
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0005

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 245
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.CAPTIONING_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 246
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.settings"

    const-string v3, "com.android.settings.Settings$CaptioningSettingsActivity"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 251
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 249
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->createSubtitleSettingPopup()V

    goto :goto_0
.end method
