.class Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;
.super Ljava/lang/Object;
.source "VideoVisualSeekView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 72
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>>>>>>>VideoVisualSeekView surfaceChanged<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 76
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>>>>>>>VideoVisualSeekView surfaceCreated<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$102(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # invokes: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->ShowVideo()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$200(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    .line 79
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 82
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>>>>>>>VideoVisualSeekView surfaceDestroyed<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$102(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 85
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "release() start"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$300(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # invokes: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setAIAContext(Z)V
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$400(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)V

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$300(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$302(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$502(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)Z

    .line 92
    return-void
.end method
