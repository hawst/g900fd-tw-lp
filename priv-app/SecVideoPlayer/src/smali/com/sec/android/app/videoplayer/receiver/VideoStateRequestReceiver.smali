.class public Lcom/sec/android/app/videoplayer/receiver/VideoStateRequestReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VideoStateRequestReceiver.java"


# static fields
.field public static final EXTRA_MEDIA_STATE_TYPE:Ljava/lang/String; = "com.samsung.sec.android.media.EXTRA_MEDIA_STATE_TYPE"

.field public static final EXTRA_MEDIA_TYPE:Ljava/lang/String; = "com.samsung.sec.android.media.EXTRA_MEDIA_TYPE"

.field public static final MEDIA_STATE_CHANGED_ACTION:Ljava/lang/String; = "com.samsung.sec.android.MEDIA_STATE_CHANGED"

.field public static final MEDIA_STATE_REQUEST_ACTION:Ljava/lang/String; = "com.samsung.sec.android.MEDIA_STATE_REQUEST"

.field private static final TAG:Ljava/lang/String; = "VideoStateRequestReceiver"

.field private static final TAG_VIDEO:Ljava/lang/String; = "video"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private isPlaying()Z
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 25
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 26
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.samsung.sec.android.media.EXTRA_MEDIA_TYPE"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 27
    .local v2, "mediaType":Ljava/lang/String;
    const-string v3, "VideoStateRequestReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReceive :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    const-string v3, "video"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.samsung.sec.android.MEDIA_STATE_REQUEST"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 29
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.sec.android.MEDIA_STATE_CHANGED"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 30
    .local v1, "broadcast":Landroid/content/Intent;
    const-string v3, "com.samsung.sec.android.media.EXTRA_MEDIA_TYPE"

    const-string v4, "video"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    const-string v3, "com.samsung.sec.android.media.EXTRA_MEDIA_STATE_TYPE"

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/receiver/VideoStateRequestReceiver;->isPlaying()Z

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 32
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 34
    .end local v1    # "broadcast":Landroid/content/Intent;
    :cond_0
    return-void
.end method
