.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;
.super Landroid/os/Handler;
.source "SubtitleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V
    .locals 0

    .prologue
    .line 840
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 842
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/os/Handler;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 843
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 858
    :goto_0
    return-void

    .line 845
    :pswitch_0
    const/4 v0, 0x0

    .line 847
    .local v0, "lineCount":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 848
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    .line 851
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 852
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout()V

    .line 855
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I

    goto :goto_0

    .line 843
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method
