.class final Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;
.super Ljava/lang/Thread;
.source "Sensor360.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/Video360/Sensor360;->startSensor(III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$accelRate:I

.field final synthetic val$gyroRate:I

.field final synthetic val$magRate:I


# direct methods
.method constructor <init>(III)V
    .locals 0

    .prologue
    .line 120
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$accelRate:I

    iput p2, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$gyroRate:I

    iput p3, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$magRate:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 122
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$200()Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$accelRate:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :cond_0
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$200()Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$gyroRate:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 134
    :cond_1
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensor360EventListener:Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$200()Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sSensorManager:Landroid/hardware/SensorManager;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$300()Landroid/hardware/SensorManager;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;->val$magRate:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 139
    :cond_2
    return-void
.end method
