.class public Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;
.super Landroid/app/Service;
.source "SCSCoreInitService.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "SLink.SCSCore"

.field private static isServiceRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static isServiceRunning()Z
    .locals 1

    .prologue
    .line 79
    sget-boolean v0, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    return v0
.end method

.method private setSCSCoreConfigValues(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    .line 47
    .local v1, "mp":Landroid/media/MediaPlayer;
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "ntcl://Null?NTSCoreStart"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->getScsCoreConfig(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "path":Ljava/lang/String;
    :try_start_0
    const-string v3, "SLink.SCSCore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setSCSCoreConfigValues : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 60
    :goto_0
    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 63
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    .line 64
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "SLink.SCSCore"

    const-string v4, "setSCSCoreConfigValues : IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 53
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 54
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v3, "SLink.SCSCore"

    const-string v4, "setSCSCoreConfigValues : SecurityException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 55
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v3, "SLink.SCSCore"

    const-string v4, "setSCSCoreConfigValues : IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 57
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 58
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "SLink.SCSCore"

    const-string v4, "setSCSCoreConfigValues : IOException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 23
    const-string v0, "SLink.SCSCore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    .line 25
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 74
    const-string v0, "SLink.SCSCore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy E "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    .line 76
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 29
    const-string v0, "SLink.SCSCore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand E : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    if-nez p1, :cond_0

    .line 32
    const/4 v0, 0x2

    .line 42
    :goto_0
    return v0

    .line 34
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    .line 36
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSAScsDataExist()Z

    move-result v0

    if-nez v0, :cond_1

    .line 37
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->loadSAScsConfigStringinPref(Landroid/content/Context;)V

    .line 39
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSAScsDataExist()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->setSCSCoreConfigValues(Landroid/content/Intent;)V

    .line 42
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    goto :goto_0
.end method

.method public unbindService(Landroid/content/ServiceConnection;)V
    .locals 3
    .param p1, "conn"    # Landroid/content/ServiceConnection;

    .prologue
    .line 68
    const-string v0, "SLink.SCSCore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unbindService E "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    invoke-super {p0, p1}, Landroid/app/Service;->unbindService(Landroid/content/ServiceConnection;)V

    .line 70
    return-void
.end method
