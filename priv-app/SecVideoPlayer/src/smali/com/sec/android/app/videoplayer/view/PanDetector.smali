.class public Lcom/sec/android/app/videoplayer/view/PanDetector;
.super Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
.source "PanDetector.java"


# static fields
.field private static final NUM_POINTER_ALLOWED_FOR_PAN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PanDetector"


# instance fields
.field private final PAN_SCALE_OFFSET:F

.field private mContext:Landroid/content/Context;

.field private mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

.field private mPanXStart:F

.field private mPanYStart:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "cropInterface"    # Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;-><init>(Landroid/content/Context;)V

    .line 26
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->PAN_SCALE_OFFSET:F

    .line 28
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    .line 29
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanYStart:F

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .line 43
    return-void
.end method

.method private calculatePanDelta(II)I
    .locals 3
    .param p1, "offset"    # I
    .param p2, "zoomDelta"    # I

    .prologue
    const v2, 0x3f4ccccd    # 0.8f

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "panDelta":I
    if-ltz p1, :cond_1

    .line 52
    if-ge p1, p2, :cond_0

    .line 53
    int-to-float v1, p1

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 54
    mul-int/lit8 v0, v0, -0x1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    mul-int/lit8 p1, p1, -0x1

    .line 61
    if-ge p1, p2, :cond_0

    .line 62
    int-to-float v1, p1

    mul-float/2addr v1, v2

    float-to-int v0, v1

    goto :goto_0
.end method

.method private initZoomParams()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->resetCropRect()V

    .line 158
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->resetXYDelta()V

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->setPanEnabled(Z)V

    .line 164
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    .line 165
    return-void
.end method


# virtual methods
.method public applyDelta(II)V
    .locals 7
    .param p1, "deltaX"    # I
    .param p2, "deltaY"    # I

    .prologue
    .line 144
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v5}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getCropRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 145
    .local v0, "cropRect":Landroid/graphics/Rect;
    iget v5, v0, Landroid/graphics/Rect;->left:I

    add-int v2, v5, p1

    .line 146
    .local v2, "newLeft":I
    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int v4, v5, p2

    .line 147
    .local v4, "newTop":I
    iget v5, v0, Landroid/graphics/Rect;->right:I

    add-int v3, v5, p1

    .line 148
    .local v3, "newRight":I
    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    add-int v1, v5, p2

    .line 152
    .local v1, "newBottom":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6, v2, v4, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface {v5, v6}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->applyCrop(Landroid/graphics/Rect;)Z

    .line 153
    return-void
.end method

.method public onLongPress()V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isZoomEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->setPanEnabled(Z)V

    .line 178
    :cond_0
    const-string v0, "PanDetector"

    const-string v1, "Long Press Event received : PAN is Enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v11, 0x5

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 73
    const/4 v2, 0x0

    .line 75
    .local v2, "bHandled":Z
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 77
    .local v6, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v8

    int-to-float v8, v8

    sub-float v0, v7, v8

    .line 78
    .local v0, "adustX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v8

    int-to-float v8, v8

    sub-float v1, v7, v8

    .line 81
    .local v1, "adustY":F
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v7}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isZoomEnabled()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 82
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 84
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v10, :cond_1

    .line 86
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v7}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isPanEnabled()Z

    move-result v7

    if-nez v7, :cond_0

    move v3, v2

    .line 139
    .end local v2    # "bHandled":Z
    .local v3, "bHandled":I
    :goto_0
    return v3

    .line 90
    .end local v3    # "bHandled":I
    .restart local v2    # "bHandled":Z
    :cond_0
    iget v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    cmpl-float v7, v7, v9

    if-nez v7, :cond_2

    .line 92
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    .line 93
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanYStart:F

    .line 111
    :goto_1
    const/4 v2, 0x1

    :cond_1
    :goto_2
    move v3, v2

    .line 139
    .restart local v3    # "bHandled":I
    goto :goto_0

    .line 101
    .end local v3    # "bHandled":I
    :cond_2
    iget v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    sub-float v7, v0, v7

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v8}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getDelta()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->x:I

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/videoplayer/view/PanDetector;->calculatePanDelta(II)I

    move-result v4

    .line 102
    .local v4, "panDeltaX":I
    iget v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanYStart:F

    sub-float v7, v1, v7

    float-to-int v7, v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v8}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getDelta()Landroid/graphics/Point;

    move-result-object v8

    iget v8, v8, Landroid/graphics/Point;->y:I

    invoke-direct {p0, v7, v8}, Lcom/sec/android/app/videoplayer/view/PanDetector;->calculatePanDelta(II)I

    move-result v5

    .line 105
    .local v5, "panDeltaY":I
    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/videoplayer/view/PanDetector;->applyDelta(II)V

    .line 108
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    .line 109
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanYStart:F

    goto :goto_1

    .line 114
    .end local v4    # "panDeltaX":I
    .end local v5    # "panDeltaY":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v10, :cond_5

    .line 115
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v7}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isPanEnabled()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 117
    const/4 v2, 0x1

    .line 122
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mCropInterface:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    const/4 v8, 0x0

    invoke-interface {v7, v8}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->setPanEnabled(Z)V

    .line 123
    const-string v7, "PanDetector"

    const-string v8, "PAN is Disabled"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_4
    iput v9, p0, Lcom/sec/android/app/videoplayer/view/PanDetector;->mPanXStart:F

    goto :goto_2

    .line 128
    :cond_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-eq v7, v11, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v11, :cond_1

    .line 129
    :cond_6
    const-string v7, "PanDetector"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Inside PAN Motion Event :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Pointer count :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v10, :cond_1

    .line 133
    const/4 v2, 0x1

    .line 135
    const-string v7, "PanDetector"

    const-string v8, "PAN is Enabled"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 169
    const-string v0, "PanDetector"

    const-string v1, " reset "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 171
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/PanDetector;->initZoomParams()V

    .line 172
    return-void
.end method
