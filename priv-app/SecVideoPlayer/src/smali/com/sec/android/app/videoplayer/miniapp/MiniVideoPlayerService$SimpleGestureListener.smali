.class final Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SimpleGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;

    .prologue
    .line 656
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 680
    const-string v1, "MiniVideoPlayerService"

    const-string v2, "onDoubleTap"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 683
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a0067

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 703
    :goto_0
    return v3

    .line 685
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 686
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a010d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0

    .line 690
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.finished.app-in-app"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 691
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 692
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 693
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 695
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v1, :cond_4

    .line 696
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setProgressBarVisibility(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)V

    .line 701
    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 702
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->callMoviePlayer(Landroid/content/Context;)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/content/Context;)V

    goto :goto_0

    .line 697
    :cond_4
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v1, :cond_3

    .line 698
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->forceHide()V

    goto :goto_1
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 658
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "onDown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 664
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "onSingleTapConfirmed()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsMoved:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    const-string v0, "MiniVideoPlayerService"

    const-string v1, "onSingleTapConfirmed() has been moved!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :goto_0
    return v2

    .line 670
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v0, :cond_1

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->toggleController()V

    goto :goto_0

    .line 673
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$SimpleGestureListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0
.end method
