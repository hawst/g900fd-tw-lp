.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 2028
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 2032
    const-string v0, "VideoServiceUtil"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    check-cast p2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$LocalBinder;

    .end local p2    # "service":Landroid/os/IBinder;
    invoke-virtual {p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$LocalBinder;->getService()Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$102(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .line 2036
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2037
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->changePlayer(I)V

    .line 2039
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 2043
    const-string v0, "VideoServiceUtil"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2044
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$102(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .line 2045
    return-void
.end method
