.class Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;
.super Ljava/lang/Object;
.source "SCoverPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private doNotCallOnCompleteEvent(I)Z
    .locals 1
    .param p1, "errorState"    # I

    .prologue
    .line 226
    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processError(I)I
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    const/16 v0, 0x2714

    .line 230
    const-string v1, "SCoverPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ">>>>>>>> MediaPlayer Error :: processError = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <<<<<<<<<<<<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    sparse-switch p1, :sswitch_data_0

    move p1, v0

    .line 252
    .end local p1    # "errorCode":I
    :goto_0
    return p1

    .line 233
    .restart local p1    # "errorCode":I
    :sswitch_0
    const-string v1, "SCoverPlayer"

    const-string v2, ">>>>>>>>MEDIA_ERROR_SERVER_DIED<<<<<<<<<<<<"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->invalidate()V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;I)V

    goto :goto_0

    .line 246
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;I)V

    goto :goto_0

    .line 231
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fffbffa -> :sswitch_1
        -0x7fffbff9 -> :sswitch_1
        -0x3fd -> :sswitch_1
        -0x3f2 -> :sswitch_1
        -0x3ef -> :sswitch_1
        -0x3ed -> :sswitch_1
        -0x63 -> :sswitch_1
        -0x62 -> :sswitch_1
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/16 v4, 0x2714

    .line 210
    const-string v1, "SCoverPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MediaPlayer :: mErrorListener = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " <<<<<<<<<<<<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->processError(I)I

    move-result v0

    .line 214
    .local v0, "errorState":I
    if-ne v0, v4, :cond_0

    .line 215
    invoke-direct {p0, p3}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->processError(I)I

    move-result v0

    .line 217
    :cond_0
    if-ne v0, v4, :cond_1

    .line 218
    const-string v1, "SCoverPlayer"

    const-string v2, ">>>>>>>> PLAYBACK_UNKNOWNERROR <<<<<<<<<<<<"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;I)V

    .line 222
    :cond_1
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;->doNotCallOnCompleteEvent(I)Z

    move-result v1

    return v1
.end method
