.class public Lcom/sec/android/app/videoplayer/service/NotificationView;
.super Ljava/lang/Object;
.source "NotificationView.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotifcationAction;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mRemoteViews:Landroid/widget/RemoteViews;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/android/app/videoplayer/service/NotificationView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/service/NotificationView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 32
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method

.method private createRemoteView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 173
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v1, :cond_0

    const v0, 0x7f030019

    .line 175
    .local v0, "layoutId":I
    :goto_0
    new-instance v1, Landroid/widget/RemoteViews;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v1

    .line 173
    .end local v0    # "layoutId":I
    :cond_0
    const v0, 0x7f030018

    goto :goto_0
.end method


# virtual methods
.method public build()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/NotificationView;->createRemoteView()Landroid/widget/RemoteViews;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public getLaunchPendingIntent()Landroid/app/PendingIntent;
    .locals 6

    .prologue
    .line 101
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v2, 0x7010007

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v4, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x8000000

    invoke-static {v1, v2, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 103
    .local v0, "launch":Landroid/app/PendingIntent;
    return-object v0
.end method

.method public setButtons(Landroid/os/IBinder;)Lcom/sec/android/app/videoplayer/service/NotificationView;
    .locals 15
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 53
    sget-object v10, Lcom/sec/android/app/videoplayer/service/NotificationView;->TAG:Ljava/lang/String;

    const-string v11, "setRemoteButtons E"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    iget-object v13, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const-class v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {v12, v13, v14}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v13, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v12

    const/high16 v13, 0x8000000

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 58
    .local v3, "launch":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d00fe

    invoke-virtual {v10, v11, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 60
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v10, :cond_0

    const v4, 0x7f0d00fa

    .line 61
    .local v4, "layoutId":I
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v10, v4, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 64
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.prev"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    .line 65
    .local v7, "prev":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d00fb

    invoke-virtual {v10, v11, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 68
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.next"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 69
    .local v5, "next":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d0102

    invoke-virtual {v10, v11, v5}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 72
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.playpause"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 73
    .local v6, "playPause":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d0101

    invoke-virtual {v10, v11, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 76
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.close"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 77
    .local v0, "close":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d0103

    invoke-virtual {v10, v11, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 80
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.ffdown"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 81
    .local v1, "ffDown":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.ffup"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 85
    .local v2, "ffUp":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.rewdown"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    .line 86
    .local v8, "rewDown":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    const v11, 0x7010007

    new-instance v12, Landroid/content/Intent;

    const-string v13, "videoplayer.notification.rewup"

    invoke-direct {v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    .line 90
    .local v9, "rewUp":Landroid/app/PendingIntent;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v11, 0x7f0d00f7

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 92
    return-object p0

    .line 60
    .end local v0    # "close":Landroid/app/PendingIntent;
    .end local v1    # "ffDown":Landroid/app/PendingIntent;
    .end local v2    # "ffUp":Landroid/app/PendingIntent;
    .end local v4    # "layoutId":I
    .end local v5    # "next":Landroid/app/PendingIntent;
    .end local v6    # "playPause":Landroid/app/PendingIntent;
    .end local v7    # "prev":Landroid/app/PendingIntent;
    .end local v8    # "rewDown":Landroid/app/PendingIntent;
    .end local v9    # "rewUp":Landroid/app/PendingIntent;
    :cond_0
    const v4, 0x7f0d00fd

    goto/16 :goto_0
.end method

.method public setPersonalPage()V
    .locals 3

    .prologue
    .line 130
    const/16 v0, 0x8

    .line 132
    .local v0, "visibility":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPlayingFromPersonalPage(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const/4 v0, 0x0

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    const v2, 0x7f0d00fc

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 136
    return-void
.end method

.method public setPlayStatus(Z)Lcom/sec/android/app/videoplayer/service/NotificationView;
    .locals 5
    .param p1, "isPlaying"    # Z

    .prologue
    const v4, 0x7f0d0101

    .line 158
    if-eqz p1, :cond_0

    const v0, 0x7f020230

    .line 159
    .local v0, "rsrcId":I
    :goto_0
    if-eqz p1, :cond_1

    const v1, 0x7f0a016b

    .line 161
    .local v1, "strId":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v4, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 164
    return-object p0

    .line 158
    .end local v0    # "rsrcId":I
    .end local v1    # "strId":I
    :cond_0
    const v0, 0x7f020231

    goto :goto_0

    .line 159
    .restart local v0    # "rsrcId":I
    :cond_1
    const v1, 0x7f0a016c

    goto :goto_1
.end method

.method public setThumbnail(Landroid/graphics/Bitmap;)Lcom/sec/android/app/videoplayer/service/NotificationView;
    .locals 6
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    const v5, 0x7f0d00fe

    const v1, 0x7f0d00fa

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 113
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v2, :cond_0

    move v0, v1

    .line 115
    .local v0, "layoutId":I
    :goto_0
    if-nez p1, :cond_1

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 117
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v0, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 123
    :goto_1
    return-object p0

    .line 113
    .end local v0    # "layoutId":I
    :cond_0
    const v0, 0x7f0d00f9

    goto :goto_0

    .line 119
    .restart local v0    # "layoutId":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v1, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v5, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 121
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v1, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_1
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/service/NotificationView;
    .locals 4
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    const v3, 0x7f0d0100

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mRemoteViews:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07004d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 148
    return-object p0
.end method
