.class Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;
.super Ljava/lang/Object;
.source "SideChapterViewDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V
    .locals 0

    .prologue
    .line 550
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v8, 0x7f020148

    const/4 v3, 0x1

    .line 552
    const/4 v0, 0x0

    .local v0, "increamentstep_hori":I
    const/4 v1, 0x0

    .local v1, "increamentstep_vert":I
    const/4 v2, 0x0

    .line 553
    .local v2, "limitchapter":I
    const-string v6, "videowall-SideChapterViewDialog"

    const-string v7, "OnKey Event!!!"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v6

    if-nez v6, :cond_0

    .line 560
    const/4 v0, 0x1

    .line 561
    const/4 v1, 0x3

    .line 562
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v2

    .line 566
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->ResetHideTimer()V
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V

    .line 568
    sparse-switch p2, :sswitch_data_0

    .line 634
    const/4 v3, 0x0

    .line 636
    :cond_1
    :goto_0
    return v3

    .line 570
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v6

    const/16 v7, 0x20

    if-eq v6, v7, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 571
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 573
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v8

    sub-long v4, v6, v8

    .line 575
    .local v4, "pressTime":J
    const-wide/16 v6, 0x1f4

    cmp-long v6, v4, v6

    if-gez v6, :cond_1

    .line 576
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$2500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v6

    new-instance v7, Landroid/content/Intent;

    const-string v8, "videoplayer.set.lock"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 583
    .end local v4    # "pressTime":J
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 584
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget v6, v6, v7

    if-nez v6, :cond_2

    .line 585
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v8

    mul-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v8

    div-int/2addr v7, v8

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->callMoviePlayerByTime(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)V

    goto :goto_0

    .line 587
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v8

    aget v7, v7, v8

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->callMoviePlayerByTime(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)V

    goto/16 :goto_0

    .line 592
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 593
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    sub-int/2addr v6, v0

    if-ltz v6, :cond_1

    .line 594
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    const-string v7, "#ff000000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 595
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    sub-int/2addr v7, v0

    # setter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$502(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)I

    .line 596
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 601
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 602
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    add-int/2addr v6, v0

    if-ge v6, v2, :cond_1

    .line 603
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    const-string v7, "#ff000000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 604
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    add-int/2addr v7, v0

    # setter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$502(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)I

    .line 605
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 610
    :sswitch_4
    const-string v6, "videowall-SideChapterViewDialog"

    const-string v7, "setView : MotionEvent.KEYCODE_DPAD_UP"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 612
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    sub-int/2addr v6, v1

    if-ltz v6, :cond_1

    .line 613
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    const-string v7, "#ff000000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 614
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    sub-int/2addr v7, v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$502(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)I

    .line 615
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 620
    :sswitch_5
    const-string v6, "videowall-SideChapterViewDialog"

    const-string v7, "setView : MotionEvent.KEYCODE_DPAD_DOWN"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v6

    if-ne v6, v3, :cond_1

    .line 622
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    add-int/2addr v6, v1

    if-ge v6, v2, :cond_1

    .line 623
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    const-string v7, "#ff000000"

    invoke-static {v7}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    add-int/2addr v7, v1

    # setter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$502(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)I

    .line 625
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v7

    aget-object v6, v6, v7

    invoke-virtual {v6, v8}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 631
    :sswitch_6
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$7;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    goto/16 :goto_0

    .line 568
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_6
        0x13 -> :sswitch_4
        0x14 -> :sswitch_5
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_1
        0x1a -> :sswitch_0
        0x42 -> :sswitch_1
        0x6f -> :sswitch_6
    .end sparse-switch
.end method
