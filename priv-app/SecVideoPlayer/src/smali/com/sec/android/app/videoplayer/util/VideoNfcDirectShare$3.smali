.class Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;
.super Ljava/lang/Object;
.source "VideoNfcDirectShare.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->continueDirectShare()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

.field final synthetic val$fileUris:[Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;[Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 352
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;->val$fileUris:[Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createBeamUris(Landroid/nfc/NfcEvent;)[Landroid/net/Uri;
    .locals 2
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 355
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "create beam uris"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->isSbeamOn()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$400(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sbeam uri create"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    const/4 v0, 0x0

    .line 362
    :goto_0
    return-object v0

    .line 361
    :cond_0
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "abeam uri create"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;->val$fileUris:[Landroid/net/Uri;

    goto :goto_0
.end method
