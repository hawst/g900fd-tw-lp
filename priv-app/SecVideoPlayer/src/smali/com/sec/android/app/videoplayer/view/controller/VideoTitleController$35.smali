.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2516
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 2518
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2519
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2520
    const/4 v0, 0x1

    .line 2547
    :cond_0
    :goto_0
    return v0

    .line 2522
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2524
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v2, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 2528
    :pswitch_1
    const-string v1, "VideoTitleController"

    const-string v2, "mSoundAliveBtnTouchListener call"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2529
    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 2530
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isVideoOnlyClip()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2531
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v1

    if-nez v1, :cond_2

    .line 2532
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1502(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 2533
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;)V

    .line 2538
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->showPopup()V

    goto :goto_0

    .line 2522
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
