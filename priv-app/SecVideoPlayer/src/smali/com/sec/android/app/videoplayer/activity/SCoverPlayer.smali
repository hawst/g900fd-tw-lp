.class public Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
.super Landroid/app/Activity;
.source "SCoverPlayer.java"


# static fields
.field private static final COVER_SCREEN_OFF_TIMEOUT:J = 0x1770L

.field private static final INVALID_URI_ERROR:I = 0x15f90

.field private static final MEDIA_ERROR_CONNECTION_LOST:I = -0x3ed

.field private static final MEDIA_ERROR_DIVX_NOTAUTHORIZED:I = -0x62

.field private static final MEDIA_ERROR_DRM_OPL_BLOCKED:I = -0x7fffbff9

.field private static final MEDIA_ERROR_DRM_TAMPER_DETECTED:I = -0x7fffbffa

.field private static final MEDIA_ERROR_EXPIRED_RENTALCOUNT:I = -0x63

.field private static final MEDIA_ERROR_MALFORMED:I = -0x3ef

.field private static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0xc8

.field private static final MEDIA_ERROR_RESOURCE_OVERSPEC:I = -0x3fd

.field private static final MEDIA_ERROR_UNKNOWN_ERROR:I = 0x2714

.field private static final MEDIA_ERROR_UNSUPPORTED:I = -0x3f2


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Landroid/media/AudioManager;

.field private final mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mHasAudioFocus:Z

.field private final mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsPaused:Z

.field private mRootLayout:Landroid/widget/RelativeLayout;

.field private mRootView:Landroid/view/View;

.field private mSettingTimeOut:J

.field private mUri:Landroid/net/Uri;

.field private mVideoCloseBtn:Landroid/widget/ImageButton;

.field private mVideoLayout:Landroid/widget/RelativeLayout;

.field private mVideoPlayBtn:Landroid/widget/ImageView;

.field private mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

.field private resumePosition:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    const-string v0, "SCoverPlayer"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->TAG:Ljava/lang/String;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mContext:Landroid/content/Context;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    .line 42
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoLayout:Landroid/widget/RelativeLayout;

    .line 44
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoCloseBtn:Landroid/widget/ImageButton;

    .line 46
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoPlayBtn:Landroid/widget/ImageView;

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mErrorPopup:Landroid/app/AlertDialog;

    .line 50
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 52
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    .line 55
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mIsPaused:Z

    .line 169
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 182
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$5;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 189
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$6;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 207
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$7;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 257
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$8;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method private ExitApp()V
    .locals 4

    .prologue
    .line 422
    const-string v1, "SCoverPlayer"

    const-string v2, "ExitApp()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    const/4 v0, 0x0

    .line 428
    .local v0, "finalResult":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->isCoverOpen()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mIsPaused:Z

    if-nez v1, :cond_0

    .line 429
    const/4 v0, -0x1

    .line 431
    :cond_0
    const-string v1, "SCoverPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "nianoh ExitApp() finalResult : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setResult(I)V

    .line 434
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->abandonAudioFocus()V

    .line 435
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->finish()V

    .line 436
    return-void
.end method

.method private abandonAudioFocus()V
    .locals 2

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    if-eqz v0, :cond_0

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    .line 286
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 290
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->play()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoCloseBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->pause()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resume()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->ExitApp()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->changeLayoutOpenState()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V

    return-void
.end method

.method private changeLayoutOpenState()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 377
    .local v0, "paramsRoot":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 378
    .local v1, "paramsVideo":Landroid/widget/RelativeLayout$LayoutParams;
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 379
    iput v5, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 380
    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 381
    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 382
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 383
    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 386
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 388
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getSettingTimeOut()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setScreenOffTime(JZ)V

    .line 389
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setRequestedOrientation(I)V

    .line 390
    return-void
.end method

.method private createErrorDialog(I)V
    .locals 5
    .param p1, "action"    # I

    .prologue
    .line 293
    const-string v2, "SCoverPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createErrorDialog(action, intent). action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getErrorStringResID(I)I

    move-result v1

    .line 296
    .local v1, "resId":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 297
    const-string v2, "SCoverPlayer"

    const-string v3, "createErrorDialog : Error type unknown don\'t make dialog!!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    :goto_0
    return-void

    .line 301
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 302
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 303
    const v2, 0x7f0a0027

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 305
    const v2, 0x104000a

    new-instance v3, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$9;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$9;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 313
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$10;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 332
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$11;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 340
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mErrorPopup:Landroid/app/AlertDialog;

    .line 341
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private getSettingTimeOut()J
    .locals 2

    .prologue
    .line 364
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mSettingTimeOut:J

    return-wide v0
.end method

.method private init(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 138
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    .line 139
    const-string v0, "SCoverPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init() - action : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 141
    const v0, 0x15f90

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V

    .line 144
    :cond_0
    return-void
.end method

.method private pause()V
    .locals 2

    .prologue
    .line 393
    const-string v0, "SCoverPlayer"

    const-string v1, "pause()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->abandonAudioFocus()V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoPlayBtn:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->pause()V

    .line 398
    return-void
.end method

.method private play()V
    .locals 4

    .prologue
    .line 401
    const-string v0, "SCoverPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "play() resumePosition :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->requestAudioFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    const-string v0, "SCoverPlayer"

    const-string v1, "play() : requestAudioFocus() is false"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoPlayBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 408
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    if-lez v0, :cond_1

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iget v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->seekTo(I)V

    .line 413
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->start()V

    .line 414
    return-void

    .line 411
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resumePosition:I

    goto :goto_0
.end method

.method private requestAudioFocus()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 271
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    if-nez v2, :cond_1

    .line 272
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    .line 273
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 274
    .local v0, "ret":I
    if-ne v0, v1, :cond_0

    .line 280
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 277
    .restart local v0    # "ret":I
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    .line 280
    .end local v0    # "ret":I
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mHasAudioFocus:Z

    goto :goto_0
.end method

.method private resume()V
    .locals 2

    .prologue
    .line 417
    const-string v0, "SCoverPlayer"

    const-string v1, "resume()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->resume()V

    .line 419
    return-void
.end method

.method private setScreenOffTime(JZ)V
    .locals 5
    .param p1, "timeOut"    # J
    .param p3, "isDimOff"    # Z

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 346
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput-wide p1, v0, Landroid/view/WindowManager$LayoutParams;->userActivityTimeout:J

    .line 347
    if-eqz p3, :cond_0

    .line 348
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    .line 352
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 353
    return-void

    .line 350
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, v0, Landroid/view/WindowManager$LayoutParams;->screenDimDuration:J

    goto :goto_0
.end method

.method private setSettingTimeOut()V
    .locals 4

    .prologue
    .line 357
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getLong(Landroid/content/ContentResolver;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mSettingTimeOut:J
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :goto_0
    return-void

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public isCoverOpen()Z
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    if-nez v0, :cond_1

    .line 369
    :cond_0
    const/4 v0, 0x1

    .line 372
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 72
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 73
    const-string v3, "SCoverPlayer"

    const-string v4, "onCreate E"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mContext:Landroid/content/Context;

    .line 77
    const-string v3, "layout_inflater"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 78
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03000d

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    const v4, 0x7f0d0089

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 80
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    const v4, 0x7f0d008a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoLayout:Landroid/widget/RelativeLayout;

    .line 81
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    const v4, 0x7f0d008b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    const v4, 0x7f0d008c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoCloseBtn:Landroid/widget/ImageButton;

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    const v4, 0x7f0d008d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoPlayBtn:Landroid/widget/ImageView;

    .line 84
    new-instance v3, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 85
    const-string v3, "audio"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 87
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 88
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 89
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    new-instance v4, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$1;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoLayout:Landroid/widget/RelativeLayout;

    new-instance v4, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$2;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoCloseBtn:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$3;-><init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/high16 v4, 0x80000

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    .line 116
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setSettingTimeOut()V

    .line 117
    const-wide/16 v4, 0x1770

    invoke-direct {p0, v4, v5, v6}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setScreenOffTime(JZ)V

    .line 119
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootView:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setContentView(Landroid/view/View;)V

    .line 121
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->setRequestedOrientation(I)V

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 124
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->init(Landroid/content/Intent;)V

    .line 127
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->isCoverOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->changeLayoutOpenState()V

    .line 135
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    .line 131
    .local v2, "pl":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800da

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800d9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 440
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 441
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 442
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mAudioManager:Landroid/media/AudioManager;

    .line 443
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 162
    const-string v0, "SCoverPlayer"

    const-string v1, "onPause E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 164
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mIsPaused:Z

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 166
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->ExitApp()V

    .line 167
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 148
    const-string v0, "SCoverPlayer"

    const-string v1, "onResume E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 158
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->setVideoURI(Landroid/net/Uri;)V

    .line 156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceView;->requestFocus()Z

    .line 157
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->resume()V

    goto :goto_0
.end method
