.class public Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;
.super Ljava/lang/Object;
.source "HandleVWLib.java"


# static fields
.field private static VWGlobal:Lcom/sec/android/videowall/Global;

.field private static VWMscEngine:Lcom/sec/android/videowall/MscEngine;

.field public static VWPLAY:Z

.field private static result:I

.field private static result_bn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 11
    sput-object v1, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    .line 13
    sput-object v1, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    .line 15
    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 17
    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result_bn:Z

    .line 19
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWPLAY:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Global_create()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Lcom/sec/android/videowall/Global;

    invoke-direct {v0}, Lcom/sec/android/videowall/Global;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    .line 103
    :cond_0
    return-void
.end method

.method public static Global_getChapterTranscodeThread()I
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterTranscodeThread()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 140
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailDisplayHeight()I
    .locals 1

    .prologue
    .line 176
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailDisplayHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 177
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailDisplayWidth()I
    .locals 1

    .prologue
    .line 171
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailDisplayWidth()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 172
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailHeight()I
    .locals 1

    .prologue
    .line 166
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 167
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailHeightforPort()I
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailHeightforPort()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 187
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailWidth()I
    .locals 1

    .prologue
    .line 161
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailWidth()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 162
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getChapterViewThumbnailWidthforPort()I
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getChapterViewThumbnailWidthforPort()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 182
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getDBG()Z
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    sget-boolean v0, Lcom/sec/android/videowall/Global;->DBG:Z

    return v0
.end method

.method public static Global_getListViewThumbnailHeight()I
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getListViewThumbnailHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 197
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getListViewThumbnailHeightEverglades()I
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getListViewThumbnailHeightEverglades()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 207
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getListViewThumbnailWidth()I
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getListViewThumbnailWidth()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 192
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getListViewThumbnailWidthEverglades()I
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getListViewThumbnailWidthEverglades()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 202
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getLongTimeDuration()I
    .locals 1

    .prologue
    .line 226
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getLongTimeDuration()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 227
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getNumberOfChapter()I
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getNumberOfChapter()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 130
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getNumberOfList()I
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getNumberOfList()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 135
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getNumberOfMaxStream()I
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getNumberOfMaxStream()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 125
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getServiceTranscodeThread()I
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getServiceTranscodeThread()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 145
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getShortTimeDuration()I
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getShortTimeDuration()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 222
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getSupportList(I)I
    .locals 1
    .param p0, "device_type"    # I

    .prologue
    .line 156
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/Global;->getSupportList(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 157
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getSupportVW()Z
    .locals 1

    .prologue
    .line 149
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_WALL:Z

    if-eqz v0, :cond_0

    .line 150
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getSupportVW()Z

    move-result v0

    .line 152
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static Global_getThumbnailFps()I
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getThumbnailFps()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 120
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getTimetextHeight()I
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getTimetextHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 212
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_getTimetextHeightforPort()I
    .locals 1

    .prologue
    .line 216
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->getTimetextHeightforPort()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 217
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_readCoreNum()I
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0}, Lcom/sec/android/videowall/Global;->readCoreNum()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 115
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static Global_setDevice(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWGlobal:Lcom/sec/android/videowall/Global;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/Global;->setDevice(Landroid/content/Context;)V

    .line 111
    return-void
.end method

.method public static MscEngine_close()I
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0}, Lcom/sec/android/videowall/MscEngine;->close()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 35
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_create()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/android/videowall/MscEngine;

    invoke-direct {v0}, Lcom/sec/android/videowall/MscEngine;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    .line 25
    :cond_0
    return-void
.end method

.method public static MscEngine_getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I
    .locals 8
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "time"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "debug"    # Z
    .param p6, "time_interval"    # I

    .prologue
    .line 85
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/videowall/MscEngine;->getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 86
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_getDurationTime(Ljava/lang/String;Z)I
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "debug"    # Z

    .prologue
    .line 80
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/videowall/MscEngine;->getDurationTime(Ljava/lang/String;Z)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 81
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_getKeyframeTime(Ljava/lang/String;I)I
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "chapter"    # I

    .prologue
    .line 90
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/videowall/MscEngine;->getKeyframeTime(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 91
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_initView(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/MscEngine;->initView(Landroid/graphics/Bitmap;)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 55
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_nativeGetProgressPercent()I
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0}, Lcom/sec/android/videowall/MscEngine;->nativeGetProgressPercent()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 71
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_open(Z)I
    .locals 1
    .param p0, "debug"    # Z

    .prologue
    .line 28
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWPLAY:Z

    .line 29
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/MscEngine;->open(Z)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 30
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_render(Landroid/graphics/Bitmap;I)I
    .locals 1
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "texture"    # I

    .prologue
    .line 59
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/videowall/MscEngine;->render(Landroid/graphics/Bitmap;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 60
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_setKeyframeTime(Ljava/lang/String;I)I
    .locals 1
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "chapter"    # I

    .prologue
    .line 95
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/videowall/MscEngine;->setKeyframeTime(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 96
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_setThumbnail(ILjava/lang/String;)I
    .locals 1
    .param p0, "index"    # I
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/videowall/MscEngine;->setThumbnail(ILjava/lang/String;)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 45
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_stopTranscoding(I)I
    .locals 1
    .param p0, "flag"    # I

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/MscEngine;->stopTranscoding(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 76
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_texture(I)I
    .locals 1
    .param p0, "index"    # I

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/MscEngine;->texture(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 40
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I
    .locals 12
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "newname"    # Ljava/lang/String;
    .param p2, "seektime"    # I
    .param p3, "chapter"    # I
    .param p4, "nbthumb"    # I
    .param p5, "width"    # I
    .param p6, "height"    # I
    .param p7, "framerate"    # I
    .param p8, "duration"    # I
    .param p9, "debug"    # Z
    .param p10, "time_interval"    # I

    .prologue
    .line 65
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-virtual/range {v0 .. v11}, Lcom/sec/android/videowall/MscEngine;->transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 66
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method

.method public static MscEngine_useHWDecoder(I)I
    .locals 1
    .param p0, "flag"    # I

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->VWMscEngine:Lcom/sec/android/videowall/MscEngine;

    invoke-virtual {v0, p0}, Lcom/sec/android/videowall/MscEngine;->useHWDecoder(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    .line 50
    sget v0, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->result:I

    return v0
.end method
