.class Lcom/sec/android/app/videoplayer/view/MainVideoView$2;
.super Landroid/os/Handler;
.source "MainVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 1561
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v8, 0xc8

    const/4 v6, -0x1

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1563
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 1650
    :cond_0
    :goto_0
    return-void

    .line 1568
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1569
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    goto :goto_0

    .line 1574
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1575
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 1579
    :sswitch_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v4, :cond_2

    .line 1580
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v0

    if-gez v0, :cond_1

    .line 1581
    const-string v0, "MainVideoView"

    const-string v1, "mHandler - init and start timer"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1583
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v1, 0xa28

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 1584
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a0020

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1585
    invoke-virtual {p0, v5, v3, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v8, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1589
    :cond_1
    const-string v0, "MainVideoView"

    const-string v1, "mHandler - exit player by H/W back key"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1591
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 1592
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->removeMessages(I)V

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.exit"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 1597
    :cond_2
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHandler - update timer for expiring. current timer ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v0

    if-lez v0, :cond_3

    .line 1601
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v1

    add-int/lit16 v1, v1, -0xc8

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 1602
    invoke-virtual {p0, v5, v3, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0, v8, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1605
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I
    invoke-static {v0, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    goto/16 :goto_0

    .line 1611
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-boolean v4, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 1613
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v0, :cond_0

    .line 1614
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    goto/16 :goto_0

    .line 1619
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1502(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z

    goto/16 :goto_0

    .line 1622
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1623
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    goto/16 :goto_0

    .line 1625
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 1630
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->restartProgressbarPreview()V

    goto/16 :goto_0

    .line 1634
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAudioOnlyClip()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1635
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    move-result-object v0

    if-nez v0, :cond_6

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachAudioOnlyView()V

    .line 1638
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1639
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showAudioOnlyView()V

    goto/16 :goto_0

    .line 1644
    :sswitch_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a0192

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto/16 :goto_0

    .line 1564
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_3
        0x1e -> :sswitch_8
    .end sparse-switch
.end method
