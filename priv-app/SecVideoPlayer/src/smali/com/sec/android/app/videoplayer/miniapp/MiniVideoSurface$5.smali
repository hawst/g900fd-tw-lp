.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "framework_err"    # I
    .param p3, "impl_err"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$602(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1002(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    :cond_0
    return v4
.end method
