.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleAlignmentPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 1127
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1129
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1131
    packed-switch p2, :pswitch_data_0

    .line 1148
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refreshSubtitleMenu()V

    .line 1149
    return-void

    .line 1133
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setTextAlignment(I)V

    goto :goto_0

    .line 1137
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setTextAlignment(I)V

    goto :goto_0

    .line 1141
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$14;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setTextAlignment(I)V

    goto :goto_0

    .line 1131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
