.class public Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;
.super Ljava/lang/Object;
.source "ViewUnbindUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static unbindReferences(Landroid/app/Activity;I)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "viewID"    # I

    .prologue
    .line 51
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 52
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 53
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewReferences(Landroid/view/View;)V

    .line 54
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 55
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "view":Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewGroupReferences(Landroid/view/ViewGroup;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static unbindReferences(Landroid/view/View;)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 24
    if-eqz p0, :cond_0

    .line 25
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewReferences(Landroid/view/View;)V

    .line 26
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 27
    check-cast p0, Landroid/view/ViewGroup;

    .end local p0    # "view":Landroid/view/View;
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewGroupReferences(Landroid/view/ViewGroup;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :cond_0
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private static unbindViewGroupReferences(Landroid/view/ViewGroup;)V
    .locals 4
    .param p0, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    .line 67
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 68
    .local v1, "nrOfChildren":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 69
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 70
    .local v2, "view":Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewReferences(Landroid/view/View;)V

    .line 71
    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 72
    check-cast v2, Landroid/view/ViewGroup;

    .end local v2    # "view":Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindViewGroupReferences(Landroid/view/ViewGroup;)V

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/view/ViewGroup;->removeAllViews()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 81
    :goto_1
    return-void

    .line 77
    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private static unbindViewReferences(Landroid/view/View;)V
    .locals 4
    .param p0, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 86
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    :goto_1
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 98
    :goto_2
    const/4 v2, 0x0

    :try_start_3
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 102
    :goto_3
    const/4 v2, 0x0

    :try_start_4
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 106
    :goto_4
    const/4 v2, 0x0

    :try_start_5
    invoke-virtual {p0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5

    .line 110
    :goto_5
    const/4 v2, 0x0

    :try_start_6
    invoke-virtual {p0, v2}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_6

    .line 113
    :goto_6
    invoke-virtual {p0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 114
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 116
    const/4 v2, 0x0

    :try_start_7
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    .line 120
    :cond_0
    :goto_7
    instance-of v2, p0, Landroid/widget/ImageView;

    if-eqz v2, :cond_3

    move-object v1, p0

    .line 121
    check-cast v1, Landroid/widget/ImageView;

    .line 122
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_1

    .line 124
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 130
    :cond_1
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_2
    :goto_8
    const/4 v2, 0x0

    :try_start_8
    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_8

    .line 140
    :goto_9
    const/4 v2, 0x0

    :try_start_9
    invoke-virtual {p0, v2}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_9

    .line 144
    :goto_a
    const/4 v2, 0x0

    :try_start_a
    invoke-virtual {p0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_a

    .line 148
    :goto_b
    const/4 v2, 0x0

    :try_start_b
    invoke-virtual {p0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_b

    .line 151
    :goto_c
    return-void

    .line 131
    :cond_3
    instance-of v2, p0, Landroid/webkit/WebView;

    if-eqz v2, :cond_2

    move-object v2, p0

    .line 132
    check-cast v2, Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->destroyDrawingCache()V

    move-object v2, p0

    .line 133
    check-cast v2, Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->destroy()V

    goto :goto_8

    .line 87
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v2

    goto :goto_0

    .line 91
    :catch_1
    move-exception v2

    goto :goto_1

    .line 95
    :catch_2
    move-exception v2

    goto :goto_2

    .line 99
    :catch_3
    move-exception v2

    goto :goto_3

    .line 103
    :catch_4
    move-exception v2

    goto :goto_4

    .line 107
    :catch_5
    move-exception v2

    goto :goto_5

    .line 111
    :catch_6
    move-exception v2

    goto :goto_6

    .line 117
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_7
    move-exception v2

    goto :goto_7

    .line 137
    :catch_8
    move-exception v2

    goto :goto_9

    .line 141
    :catch_9
    move-exception v2

    goto :goto_a

    .line 145
    :catch_a
    move-exception v2

    goto :goto_b

    .line 149
    :catch_b
    move-exception v2

    goto :goto_c
.end method
