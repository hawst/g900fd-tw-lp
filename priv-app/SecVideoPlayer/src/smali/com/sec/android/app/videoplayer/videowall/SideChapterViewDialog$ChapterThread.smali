.class public Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;
.super Ljava/lang/Thread;
.source "SideChapterViewDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ChapterThread"
.end annotation


# instance fields
.field private chapter:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

.field private thumbtime:I

.field private time_interval:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 513
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .line 514
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 515
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 525
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$900(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->sName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1000(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mVideoid:J
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$1100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)J

    move-result-wide v4

    iget v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->chapter:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->thumbtime:I

    iget v8, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->time_interval:I

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIII)V
    invoke-static/range {v1 .. v8}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$2400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;Ljava/lang/String;Ljava/lang/String;JIII)V

    .line 526
    return-void
.end method

.method public setArg(III)V
    .locals 0
    .param p1, "pChapter"    # I
    .param p2, "pThumbtime"    # I
    .param p3, "pTime_interval"    # I

    .prologue
    .line 518
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->chapter:I

    .line 519
    iput p2, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->thumbtime:I

    .line 520
    iput p3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$ChapterThread;->time_interval:I

    .line 521
    return-void
.end method
