.class public Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;
.super Ljava/lang/Object;
.source "Video360XMPUtil.java"


# static fields
.field private static final MAX_CHANGE_MODE:I = 0x3

.field private static TAG:Ljava/lang/String; = null

.field private static mInstance:Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil; = null

.field private static final schemaNS_EYESEE360:Ljava/lang/String; = "ns:eyesee360.com/ns/xmp_projection1/"

.field private static final schemaNS_SAMSUNG360:Ljava/lang/String; = "samsung:ns:roundviewvideo/"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEquirectangularDuration:[I

.field private mEquirectangularStartTime:[I

.field private mIsPlayChanging:Z

.field private mPlanarDurationTime:[I

.field private mPlanarStartTime:[I

.field private mXMPMeta:Lcom/adobe/xmp/XMPMeta;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "Video360XMPUtil"

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x3

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mContext:Landroid/content/Context;

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    .line 26
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    .line 27
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    .line 28
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    .line 29
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mContext:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mInstance:Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mInstance:Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mInstance:Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    return-object v0
.end method

.method private getPropertyXMPMeta()V
    .locals 5

    .prologue
    .line 315
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v3, "ns:eyesee360.com/ns/xmp_projection1/"

    const-string v4, "proj:Projection/proj:type"

    invoke-interface {v2, v3, v4}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 316
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v3, "ns:eyesee360.com/ns/xmp_projection1/"

    const-string v4, "proj:Projection/proj:type"

    invoke-interface {v2, v3, v4}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 317
    .local v1, "type":Ljava/lang/String;
    const-string v2, "cylindrical"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 318
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 333
    .end local v1    # "type":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 327
    .restart local v1    # "type":Ljava/lang/String;
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 330
    .end local v1    # "type":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 331
    .local v0, "ex":Lcom/adobe/xmp/XMPException;
    sget-object v2, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    const-string v3, "getPropertyXMPMeta : XMPException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getPropertyXMPMetaEx()Z
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 369
    const/4 v2, 0x0

    .line 370
    .local v2, "indexEquirectangular":I
    const/4 v3, 0x0

    .line 371
    .local v3, "indexPlanar":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->initTimeValue()V

    .line 374
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v8, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 375
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z

    .line 376
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[1]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 377
    .local v5, "type":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 378
    .local v4, "sTime":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 379
    .local v0, "dTime":I
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 380
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 381
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 382
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 383
    add-int/lit8 v2, v2, 0x1

    .line 394
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[2]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 395
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[2]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 396
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[2]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 397
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[2]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 398
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 399
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 400
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 401
    add-int/lit8 v2, v2, 0x1

    .line 408
    :cond_0
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[3]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 409
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[3]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 410
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[3]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 411
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[3]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 412
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 413
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 414
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 415
    add-int/lit8 v2, v2, 0x1

    .line 422
    :cond_1
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[4]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 423
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[4]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 424
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[4]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 425
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[4]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 426
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 427
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 428
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 429
    add-int/lit8 v2, v2, 0x1

    .line 436
    :cond_2
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[5]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 437
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[5]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 438
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[5]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 439
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[5]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 440
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 441
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 442
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 443
    add-int/lit8 v2, v2, 0x1

    .line 450
    :cond_3
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[6]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 451
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[6]/proj:Projection/proj:type"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 452
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[6]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 453
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v8, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v9, "xmpDM:Tracks[6]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v6, v8, v9}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 454
    const-string v6, "equirectangular"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 455
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v6, v2

    .line 456
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v6, v2

    .line 457
    add-int/lit8 v2, v2, 0x1

    .end local v0    # "dTime":I
    .end local v4    # "sTime":I
    .end local v5    # "type":Ljava/lang/String;
    :cond_4
    :goto_5
    move v6, v7

    .line 472
    :goto_6
    return v6

    .line 385
    .restart local v0    # "dTime":I
    .restart local v4    # "sTime":I
    .restart local v5    # "type":Ljava/lang/String;
    :cond_5
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v8, 0x2

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 386
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 387
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3

    .line 388
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 391
    .end local v0    # "dTime":I
    .end local v4    # "sTime":I
    .end local v5    # "type":Ljava/lang/String;
    :cond_6
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    .line 468
    :catch_0
    move-exception v1

    .line 469
    .local v1, "e":Lcom/adobe/xmp/XMPException;
    sget-object v6, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    const-string v8, "getPropertyXMPMetaEx : XMPException"

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 403
    .end local v1    # "e":Lcom/adobe/xmp/XMPException;
    .restart local v0    # "dTime":I
    .restart local v4    # "sTime":I
    .restart local v5    # "type":Ljava/lang/String;
    :cond_7
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 404
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3

    .line 405
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 417
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 418
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3

    .line 419
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 431
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 432
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3

    .line 433
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 445
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 446
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3

    .line 447
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 459
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v6, v3

    .line 460
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v6, v3
    :try_end_1
    .catch Lcom/adobe/xmp/XMPException; {:try_start_1 .. :try_end_1} :catch_0

    .line 461
    add-int/lit8 v3, v3, 0x1

    goto :goto_5
.end method

.method private getPropertyXMPMetaExForSamsung()Z
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 194
    const/4 v2, 0x0

    .line 195
    .local v2, "indexEquirectangular":I
    const/4 v3, 0x0

    .line 196
    .local v3, "indexPlanar":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->initTimeValue()V

    .line 199
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v10, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v11, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v9, v10, v11}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 200
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z

    .line 201
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v10, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v11, "xmpDM:Tracks[1]/xmpDM:markers[1]/srvv:type"

    invoke-interface {v9, v10, v11}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 202
    .local v6, "type":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v10, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v11, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:startTime"

    invoke-interface {v9, v10, v11}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 203
    .local v4, "sTime":I
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v10, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v11, "xmpDM:Tracks[1]/xmpDM:markers[1]/xmpDM:duration"

    invoke-interface {v9, v10, v11}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 205
    .local v0, "dTime":I
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 207
    .local v5, "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    if-nez v5, :cond_0

    .line 299
    .end local v0    # "dTime":I
    .end local v4    # "sTime":I
    .end local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .end local v6    # "type":Ljava/lang/String;
    :goto_0
    return v7

    .line 210
    .restart local v0    # "dTime":I
    .restart local v4    # "sTime":I
    .restart local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .restart local v6    # "type":Ljava/lang/String;
    :cond_0
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 211
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 212
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 213
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 214
    add-int/lit8 v2, v2, 0x1

    .line 225
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[2]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 226
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[2]/srvv:type"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 227
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[2]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 228
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[2]/xmpDM:duration"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 229
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 230
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 231
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 232
    add-int/lit8 v2, v2, 0x1

    .line 239
    :cond_1
    :goto_2
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[3]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 240
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[3]/srvv:type"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 241
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[3]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 242
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[3]/xmpDM:duration"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 243
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 244
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 245
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 246
    add-int/lit8 v2, v2, 0x1

    .line 253
    :cond_2
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[4]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 254
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[4]/srvv:type"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 255
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[4]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 256
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[4]/xmpDM:duration"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 257
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 258
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 259
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 260
    add-int/lit8 v2, v2, 0x1

    .line 267
    :cond_3
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[5]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 268
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[5]/srvv:type"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 269
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[5]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 270
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[5]/xmpDM:duration"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 271
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 272
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 273
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 274
    add-int/lit8 v2, v2, 0x1

    .line 281
    :cond_4
    :goto_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[6]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 282
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[6]/srvv:type"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 283
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[6]/xmpDM:startTime"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 284
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v9, "http://ns.adobe.com/xmp/1.0/DynamicMedia/"

    const-string v10, "xmpDM:Tracks[1]/xmpDM:markers[6]/xmpDM:duration"

    invoke-interface {v7, v9, v10}, Lcom/adobe/xmp/XMPMeta;->getPropertyInteger(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 285
    const-string v7, "equirectangular"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 286
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v4, v7, v2

    .line 287
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v0, v7, v2

    .line 288
    add-int/lit8 v2, v2, 0x1

    .end local v0    # "dTime":I
    .end local v4    # "sTime":I
    .end local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .end local v6    # "type":Ljava/lang/String;
    :cond_5
    :goto_6
    move v7, v8

    .line 299
    goto/16 :goto_0

    .line 216
    .restart local v0    # "dTime":I
    .restart local v4    # "sTime":I
    .restart local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .restart local v6    # "type":Ljava/lang/String;
    :cond_6
    const/4 v7, 0x2

    invoke-virtual {v5, v7}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 217
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 218
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3

    .line 219
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 222
    .end local v0    # "dTime":I
    .end local v4    # "sTime":I
    .end local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .end local v6    # "type":Ljava/lang/String;
    :cond_7
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 295
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Lcom/adobe/xmp/XMPException;
    sget-object v7, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    const-string v9, "getPropertyXMPMetaEx : XMPException"

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 234
    .end local v1    # "e":Lcom/adobe/xmp/XMPException;
    .restart local v0    # "dTime":I
    .restart local v4    # "sTime":I
    .restart local v5    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .restart local v6    # "type":Ljava/lang/String;
    :cond_8
    :try_start_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 235
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3

    .line 236
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 248
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 249
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3

    .line 250
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 262
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 263
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3

    .line 264
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_4

    .line 276
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 277
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3

    .line 278
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_5

    .line 290
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v4, v7, v3

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v0, v7, v3
    :try_end_1
    .catch Lcom/adobe/xmp/XMPException; {:try_start_1 .. :try_end_1} :catch_0

    .line 292
    add-int/lit8 v3, v3, 0x1

    goto :goto_6
.end method

.method private getPropertyXMPMetaForSamsung()V
    .locals 6

    .prologue
    .line 132
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v4, "samsung:ns:roundviewvideo/"

    const-string v5, "srvv:type"

    invoke-interface {v3, v4, v5}, Lcom/adobe/xmp/XMPMeta;->doesPropertyExist(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    const-string v4, "samsung:ns:roundviewvideo/"

    const-string v5, "srvv:type"

    invoke-interface {v3, v4, v5}, Lcom/adobe/xmp/XMPMeta;->getPropertyString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "type":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 137
    .local v1, "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    if-nez v1, :cond_1

    .line 158
    .end local v1    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .end local v2    # "type":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 140
    .restart local v1    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .restart local v2    # "type":Ljava/lang/String;
    :cond_1
    const-string v3, "cylindrical"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    .end local v1    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .end local v2    # "type":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 156
    .local v0, "ex":Lcom/adobe/xmp/XMPException;
    sget-object v3, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    const-string v4, "getPropertyXMPMeta : XMPException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 152
    .end local v0    # "ex":Lcom/adobe/xmp/XMPException;
    .restart local v1    # "surfaceGL":Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .restart local v2    # "type":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V
    :try_end_1
    .catch Lcom/adobe/xmp/XMPException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private initTimeValue()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v1, v0, v2

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v1, v0, v3

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aput v1, v0, v4

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v1, v0, v2

    .line 88
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v1, v0, v3

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aput v1, v0, v4

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v1, v0, v2

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v1, v0, v3

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aput v1, v0, v4

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v1, v0, v2

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v1, v0, v3

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aput v1, v0, v4

    .line 97
    return-void
.end method

.method private isEquirectangular(I)Z
    .locals 5
    .param p1, "time"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 44
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v1

    if-le p1, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aget v3, v3, v1

    add-int/2addr v2, v3

    if-ge p1, v2, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v0

    if-le p1, v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    if-lt p1, v2, :cond_0

    .line 50
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v4

    if-le p1, v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularStartTime:[I

    aget v2, v2, v4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mEquirectangularDuration:[I

    aget v3, v3, v4

    add-int/2addr v2, v3

    if-lt p1, v2, :cond_0

    :cond_3
    move v0, v1

    .line 53
    goto :goto_0
.end method

.method private isPlanar(I)Z
    .locals 5
    .param p1, "time"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 57
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v1

    if-le p1, v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aget v3, v3, v1

    add-int/2addr v2, v3

    if-ge p1, v2, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v0

    if-le p1, v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aget v3, v3, v0

    add-int/2addr v2, v3

    if-lt p1, v2, :cond_0

    .line 63
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v4

    if-le p1, v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarStartTime:[I

    aget v2, v2, v4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mPlanarDurationTime:[I

    aget v3, v3, v4

    add-int/2addr v2, v3

    if-lt p1, v2, :cond_0

    :cond_3
    move v0, v1

    .line 66
    goto :goto_0
.end method

.method private readXMPFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 477
    const/4 v0, 0x0

    .line 478
    .local v0, "br":Ljava/io/BufferedReader;
    const-string v4, ""

    .line 482
    .local v4, "outputString":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    invoke-direct {v6, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, "sCurrentLine":Ljava/lang/String;
    if-eqz v5, :cond_0

    .line 485
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    goto :goto_0

    .line 492
    :cond_0
    if-eqz v1, :cond_1

    .line 493
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    move-object v0, v1

    .line 500
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v5    # "sCurrentLine":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_2
    :goto_1
    return-object v4

    .line 495
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "sCurrentLine":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 496
    .local v3, "ex":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 498
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 488
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v5    # "sCurrentLine":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 489
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 492
    if-eqz v0, :cond_2

    .line 493
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 495
    :catch_2
    move-exception v3

    .line 496
    .restart local v3    # "ex":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 491
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ex":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 492
    :goto_3
    if-eqz v0, :cond_3

    .line 493
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 497
    :cond_3
    :goto_4
    throw v6

    .line 495
    :catch_3
    move-exception v3

    .line 496
    .restart local v3    # "ex":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 491
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v3    # "ex":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3

    .line 488
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2
.end method


# virtual methods
.method public getIsPlayChanging()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mIsPlayChanging:Z

    return v0
.end method

.method public getPlaingMode(I)I
    .locals 2
    .param p1, "time"    # I

    .prologue
    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->isEquirectangular(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->isPlanar(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public parseXMP(Ljava/lang/String;)V
    .locals 4
    .param p1, "stringXMP"    # Ljava/lang/String;

    .prologue
    .line 101
    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseXMP = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-static {p1}, Lcom/adobe/xmp/XMPMetaFactory;->parseFromString(Ljava/lang/String;)Lcom/adobe/xmp/XMPMeta;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;
    :try_end_0
    .catch Lcom/adobe/xmp/XMPException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    if-eqz v1, :cond_1

    .line 111
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPropertyXMPMetaExForSamsung()Z

    move-result v1

    if-nez v1, :cond_1

    .line 112
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPropertyXMPMetaForSamsung()V

    .line 115
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->mXMPMeta:Lcom/adobe/xmp/XMPMeta;

    .line 116
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Lcom/adobe/xmp/XMPException;
    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->TAG:Ljava/lang/String;

    const-string v2, "parseXMP : XMPException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
