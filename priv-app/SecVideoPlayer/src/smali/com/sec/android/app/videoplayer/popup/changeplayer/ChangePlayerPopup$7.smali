.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;
.super Ljava/lang/Object;
.source "ChangePlayerPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 779
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1400(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)I

    move-result v0

    if-gtz v0, :cond_3

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableDLNAdisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->updatePlayerList()V

    .line 784
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->availableWifidisplay()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->stopScanWifiDisplays()V

    .line 788
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->makeChangePlayerList()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1600(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)V

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mChangePlayerAdapter:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerAdapter;->notifyDataSetChanged()V

    .line 794
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 795
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 796
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    const/16 v1, 0x2710

    # setter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1402(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)I

    .line 802
    :goto_0
    return-void

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    const/16 v1, 0x3e8

    # -= operator for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshingCheckTime:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1420(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;I)I

    .line 801
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->mRefreshDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->access$1500(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
