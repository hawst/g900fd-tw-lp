.class public Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;
.super Landroid/app/Dialog;
.source "VideoTagBuddySetting.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;,
        Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoTagBuddySetting"


# instance fields
.field private adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

.field private mContext:Landroid/content/Context;

.field private mDateLayout:Landroid/widget/RelativeLayout;

.field private mDivider:Landroid/widget/RelativeLayout;

.field mFirstVisibleItem:I

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field mLastVisibleItem:I

.field private mListView:Landroid/widget/ListView;

.field private mLocationLayout:Landroid/widget/RelativeLayout;

.field private mLocationtv:Landroid/widget/TextView;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSwitch:Landroid/widget/Switch;

.field private titleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    const/16 v1, 0x400

    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 47
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDivider:Landroid/widget/RelativeLayout;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationtv:Landroid/widget/TextView;

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->titleText:Landroid/widget/TextView;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    .line 64
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    .line 350
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$4;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 69
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    .line 70
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 72
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->requestWindowFeature(I)Z

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x50

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->initDatas()V

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/Switch;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->exitDialog()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->saveContextualTagSettings()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method private exitDialog()V
    .locals 2

    .prologue
    .line 340
    const-string v0, "VideoTagBuddySetting"

    const-string v1, "exitDialog"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->dismiss()V

    .line 342
    return-void
.end method

.method private initView()V
    .locals 14

    .prologue
    const v13, 0x7f0d005e

    const v12, 0x7f0d005d

    const/16 v10, 0x8

    const/4 v11, -0x1

    const/4 v9, 0x0

    .line 94
    const v8, 0x7f030005

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setContentView(I)V

    .line 96
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v8, :cond_0

    .line 97
    invoke-virtual {p0, v12}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 98
    .local v3, "LP_preview":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p0, v13}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 99
    .local v0, "LP":Landroid/widget/RelativeLayout$LayoutParams;
    iput v11, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 100
    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 102
    invoke-virtual {p0, v12}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 103
    invoke-virtual {p0, v13}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    .end local v0    # "LP":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "LP_preview":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    const v8, 0x7f0d0066

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    .line 107
    .local v5, "mListView":Landroid/widget/ListView;
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setClickable(Z)V

    .line 109
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v4, "contextualData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;>;"
    new-instance v8, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    const-string v11, "Date"

    invoke-direct {v8, p0, v11}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    new-instance v8, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;

    const-string v11, "Location"

    invoke-direct {v8, p0, v11}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListClass;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    new-instance v8, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-direct {v8, p0, v11, v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Landroid/content/Context;Ljava/util/List;)V

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    .line 116
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getLocationTagShowStatus()Z

    move-result v7

    .line 117
    .local v7, "showLocation":Z
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getDateTagShowStatus()Z

    move-result v6

    .line 118
    .local v6, "showDate":Z
    const-string v8, "VideoTagBuddySetting"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "initView. showLocation : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", showDate : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v8, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    const v8, 0x7f0d0064

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationtv:Landroid/widget/TextView;

    .line 121
    const v8, 0x7f0d0062

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    .line 122
    const v8, 0x7f0d005f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    .line 124
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v8, :cond_1

    .line 125
    const v8, 0x7f0d0006

    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->titleText:Landroid/widget/TextView;

    .line 126
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->titleText:Landroid/widget/TextView;

    const-string v11, "sec-roboto-light"

    invoke-static {v11, v9}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 129
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 130
    .local v2, "LP_Location":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 132
    .local v1, "LP_Date":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 133
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationtv:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    const v12, 0x7f0a009a

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    if-eqz v7, :cond_2

    move v8, v9

    :goto_0
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    .line 137
    if-eqz v6, :cond_3

    :goto_1
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    .line 139
    new-instance v8, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;

    invoke-direct {v8, p0, v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$1;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;Ljava/util/List;)V

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 201
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 202
    invoke-virtual {v5}, Landroid/widget/ListView;->requestFocus()Z

    .line 203
    return-void

    :cond_2
    move v8, v10

    .line 136
    goto :goto_0

    :cond_3
    move v9, v10

    .line 137
    goto :goto_1
.end method

.method private saveContextualTagSettings()V
    .locals 8

    .prologue
    .line 307
    const/4 v3, 0x0

    .line 308
    .local v3, "locationChecked":Z
    const/4 v1, 0x0

    .line 309
    .local v1, "dateChecked":Z
    const v5, 0x7f0d0066

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    .line 311
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .line 312
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    .line 316
    iget v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .local v4, "position":I
    :goto_0
    iget v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    if-gt v4, v5, :cond_2

    .line 317
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->individual_item_view:Landroid/util/SparseArray;

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 318
    .local v2, "itemView":Landroid/view/View;
    const v5, 0x7f0d0068

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 320
    .local v0, "cb":Landroid/widget/CheckBox;
    if-nez v4, :cond_0

    .line 321
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    .line 322
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setDateTagShowStatus(Z)V

    .line 325
    :cond_0
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 326
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    .line 327
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setLocationTagShowStatus(Z)V

    .line 316
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 331
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v2    # "itemView":Landroid/view/View;
    :cond_2
    const-string v5, "VideoTagBuddySetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "saveContextualTagSettings. dateChecked : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", locationChecked : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    if-nez v1, :cond_3

    if-nez v3, :cond_3

    .line 334
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setTagBuddyShowStatus(Z)V

    .line 335
    const-string v5, "VideoTagBuddySetting"

    const-string v6, "saveContextualTagSettings : SW off"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_3
    return-void
.end method

.method private setDisappearListener()V
    .locals 1

    .prologue
    .line 392
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 405
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$6;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 417
    return-void
.end method

.method private updateTitlebar()V
    .locals 11

    .prologue
    const v10, 0x7f0a0142

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 206
    const-string v5, "VideoTagBuddySetting"

    const-string v6, "updateTitlebar"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v3

    .line 209
    .local v3, "showTagState":Z
    const v5, 0x7f0d0002

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Switch;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    .line 210
    const v5, 0x7f0d0004

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 211
    .local v0, "BackLayout":Landroid/widget/RelativeLayout;
    const v5, 0x7f0d0006

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 213
    .local v4, "title":Landroid/widget/TextView;
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    .line 215
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "name":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0164

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 217
    .local v2, "navUp":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v5, v3}, Landroid/widget/Switch;->setChecked(Z)V

    .line 221
    if-nez v3, :cond_0

    .line 222
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    .line 223
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    .line 226
    :cond_0
    const v5, 0x7f0d0066

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    .line 228
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    invoke-virtual {v5}, Landroid/widget/Switch;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 229
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 230
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v8}, Landroid/widget/ListView;->setClickable(Z)V

    .line 236
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;

    new-instance v6, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    invoke-virtual {v5, v6}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 293
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v5, :cond_1

    .line 294
    const v5, 0x7f0d0003

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDivider:Landroid/widget/RelativeLayout;

    .line 295
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDivider:Landroid/widget/RelativeLayout;

    invoke-virtual {v5, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 298
    :cond_1
    new-instance v5, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$3;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    return-void

    .line 232
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setClickable(Z)V

    goto :goto_0
.end method


# virtual methods
.method public changeDateTagVisibility(I)V
    .locals 1
    .param p1, "canVisible"    # I

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mDateLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 523
    return-void
.end method

.method public changeLocationTagVisibility(I)V
    .locals 1
    .param p1, "canVisible"    # I

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLocationLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 520
    return-void
.end method

.method public initDatas()V
    .locals 2

    .prologue
    .line 80
    const-string v0, "VideoTagBuddySetting"

    const-string v1, "initDatas"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoTagBuddySetting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 87
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setDisappearListener()V

    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->initView()V

    .line 90
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->updateTitlebar()V

    .line 91
    return-void
.end method

.method public onOrientationChanged()V
    .locals 2

    .prologue
    .line 345
    const-string v0, "VideoTagBuddySetting"

    const-string v1, "onOrientationChanged()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->initView()V

    .line 347
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->updateTitlebar()V

    .line 348
    return-void
.end method
