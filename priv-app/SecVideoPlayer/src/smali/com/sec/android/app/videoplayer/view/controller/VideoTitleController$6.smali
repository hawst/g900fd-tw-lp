.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/widget/PopupMenu$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 785
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/widget/PopupMenu;)V
    .locals 2
    .param p1, "menu"    # Landroid/widget/PopupMenu;

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/widget/ListPopupWindow;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowingSubPopupMenu()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    .line 792
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->dismissPopupMenu()V

    .line 793
    return-void
.end method
