.class public Lcom/sec/android/app/videoplayer/videowall/ListView;
.super Landroid/widget/ImageView;
.source "ListView.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "videowall-ListView"


# instance fields
.field public bitmap:Landroid/graphics/Bitmap;

.field private count:J

.field private delayTime:J

.field private fps:I

.field private ht:I

.field private movieindex:I

.field private nextFrame:Z

.field private nowTime:J

.field public prebitmap:Landroid/graphics/Bitmap;

.field private pretime:J

.field private ptX:I

.field private ptY:I

.field private res:I

.field private setVideo:Z

.field private vcount:J

.field private wd:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    .line 18
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nextFrame:Z

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->setVideo:Z

    .line 22
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->count:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->vcount:J

    .line 23
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->pretime:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nowTime:J

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->delayTime:J

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->movieindex:I

    .line 33
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/16 v10, 0x1

    const-wide/16 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x3e8

    .line 65
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 67
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->count:J

    cmp-long v2, v2, v8

    if-nez v2, :cond_0

    .line 68
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->pretime:J

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ListView;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->wd:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptX:I

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ListView;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ht:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptY:I

    .line 73
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->setVideo:Z

    if-eqz v2, :cond_5

    .line 75
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nextFrame:Z

    if-eqz v2, :cond_3

    .line 76
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->movieindex:I

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_render(Landroid/graphics/Bitmap;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->res:I

    .line 78
    iget v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->res:I

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->movieindex:I

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_render(Landroid/graphics/Bitmap;I)I

    .line 79
    :cond_2
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->vcount:J

    add-long/2addr v2, v10

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->vcount:J

    .line 82
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nowTime:J

    .line 83
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nowTime:J

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->pretime:J

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->delayTime:J

    .line 85
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->vcount:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_8

    .line 87
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->delayTime:J

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->fps:I

    div-int v4, v6, v4

    int-to-long v4, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    .line 88
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->prebitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 90
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nextFrame:Z

    .line 103
    :cond_5
    :goto_0
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->count:J

    add-long/2addr v2, v10

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->count:J

    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/ListView;->invalidate()V

    .line 105
    return-void

    .line 92
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_7

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ptY:I

    int-to-float v4, v4

    invoke-virtual {p1, v2, v3, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 94
    :cond_7
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nextFrame:Z

    .line 95
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->delayTime:J

    iget v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->fps:I

    div-int v4, v6, v4

    int-to-long v4, v4

    sub-long v0, v2, v4

    .line 96
    .local v0, "tmp":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->pretime:J

    goto :goto_0

    .line 99
    .end local v0    # "tmp":J
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->pretime:J

    goto :goto_0
.end method

.method public pauseView()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->setVideo:Z

    .line 61
    return-void
.end method

.method public setBitmapView(ILandroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "idx"    # I
    .param p2, "bit"    # Landroid/graphics/Bitmap;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 47
    const-string v0, "videowall-ListView"

    const-string v1, "=> setBitmapView()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->movieindex:I

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->bitmap:Landroid/graphics/Bitmap;

    .line 51
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->setVideo:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->nextFrame:Z

    .line 54
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->vcount:J

    .line 55
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->count:J

    .line 56
    return-void
.end method

.method public setView(III)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "framerate"    # I

    .prologue
    .line 37
    const-string v0, "videowall-ListView"

    const-string v1, "=> setView()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    iput p3, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->fps:I

    .line 39
    iput p1, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->wd:I

    .line 40
    iput p2, p0, Lcom/sec/android/app/videoplayer/videowall/ListView;->ht:I

    .line 43
    return-void
.end method
