.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v1, 0x5

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mCurrentState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$602(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 496
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1002(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;I)I

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setWakeMode(Z)V

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resetVideoRenderingStarted()V

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$4;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    .line 504
    :cond_0
    return-void
.end method
