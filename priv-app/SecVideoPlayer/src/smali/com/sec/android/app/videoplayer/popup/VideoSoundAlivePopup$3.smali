.class Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;
.super Ljava/lang/Object;
.source "VideoSoundAlivePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v3, 0xa

    .line 164
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/content/res/TypedArray;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, p2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    .line 165
    .local v0, "mResId":I
    sparse-switch v0, :sswitch_data_0

    .line 187
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    .line 191
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;->onSelected(I)V

    .line 195
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_PATEK:Z

    if-eqz v1, :cond_1

    .line 196
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 198
    :cond_1
    return-void

    .line 167
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    goto :goto_0

    .line 171
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    const/16 v2, 0xe

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    goto :goto_0

    .line 175
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    const/16 v2, 0xb

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    goto :goto_0

    .line 179
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    const/16 v2, 0xc

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    goto :goto_0

    .line 183
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    const/16 v2, 0xd

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mSoundAlive:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;I)I

    goto :goto_0

    .line 165
    :sswitch_data_0
    .sparse-switch
        0x7f0a001a -> :sswitch_1
        0x7f0a00cd -> :sswitch_3
        0x7f0a00d8 -> :sswitch_0
        0x7f0a0140 -> :sswitch_4
        0x7f0a01c6 -> :sswitch_2
    .end sparse-switch
.end method
