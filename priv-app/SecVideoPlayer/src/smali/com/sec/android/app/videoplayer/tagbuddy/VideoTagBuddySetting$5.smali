.class Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;
.super Ljava/lang/Object;
.source "VideoTagBuddySetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->setDisappearListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V
    .locals 0

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # invokes: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->saveContextualTagSettings()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$700(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$800(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$800(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoTagBuddySetting"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$5;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->sendTagBuddyInfoUpdateNotify()V

    .line 402
    return-void
.end method
