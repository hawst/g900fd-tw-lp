.class public Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;
.super Ljava/lang/Object;
.source "VideoNfcDirectShare.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private final STATUS_IS_CLOUD_FILE:I

.field private final STATUS_IS_DRM:I

.field private final STATUS_NONE:I

.field private final STATUS_NO_FILE_SELECTED:I

.field private final STATUS_PUSH:I

.field private mContext:Landroid/content/Context;

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mNdefStatus:I

.field private mNfcAdapter:Landroid/nfc/NfcAdapter;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field private sbeamSettingContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;

    .line 39
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 41
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    .line 43
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 49
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I

    .line 51
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->STATUS_NONE:I

    .line 53
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->STATUS_PUSH:I

    .line 55
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->STATUS_NO_FILE_SELECTED:I

    .line 57
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->STATUS_IS_CLOUD_FILE:I

    .line 59
    const/4 v1, 0x4

    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->STATUS_IS_DRM:I

    .line 62
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    const-string v2, "VideoNfcDirectShare E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    .line 65
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 66
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    .line 67
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    .line 69
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v1, :cond_0

    .line 72
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    new-instance v3, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;-><init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)V

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-array v4, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v1, v4}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    new-instance v3, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$2;-><init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)V

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-array v4, v5, [Landroid/app/Activity;

    invoke-virtual {v2, v3, v1, v4}, Landroid/nfc/NfcAdapter;->setOnNdefPushCompleteCallback(Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 228
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    const-string v2, "com.android.settings"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 234
    :goto_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 231
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;
    .param p1, "x1"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->isSbeamOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/nfc/NfcAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # [Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->getJsonStr(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    return-object v0
.end method

.method private blockAndroidBeamSetting()Z
    .locals 1

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->isSbeamOn()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDRM()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getJsonStr(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "filePath"    # [Ljava/lang/String;

    .prologue
    .line 266
    const-string v11, ""

    .line 267
    .local v11, "ret":Ljava/lang/String;
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 270
    .local v9, "obj":Lorg/json/JSONObject;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    const-string v16, "wifi"

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/net/wifi/WifiManager;

    .line 271
    .local v14, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v14}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v13

    .line 272
    .local v13, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    const/16 v17, 0x2

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 273
    .local v6, "firstMac":Ljava/lang/String;
    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x2

    invoke-virtual {v13}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 275
    .local v8, "lastMac":Ljava/lang/String;
    const/4 v15, 0x0

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    const-string v16, "0"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 276
    const-string v15, "mac"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "0"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    or-int/lit8 v17, v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 281
    :goto_0
    const-string v15, "mimeType"

    move-object/from16 v0, p1

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 282
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 284
    .local v3, "_list":Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p2

    array-length v15, v0

    if-le v15, v7, :cond_1

    .line 285
    new-instance v2, Ljava/io/File;

    aget-object v15, p2, v7

    invoke-direct {v2, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 286
    .local v2, "_f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    .line 287
    .local v12, "temp":Ljava/lang/String;
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    .line 288
    .local v5, "exPath":Ljava/io/File;
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 289
    .local v10, "obj2":Lorg/json/JSONObject;
    const-string v15, "fileName"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 290
    const-string v15, "subPath"

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    const-string v17, ""

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v12, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 291
    const-string v15, "fileLen"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v10, v15, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 292
    const-string v15, "filepath"

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 293
    invoke-virtual {v3, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 284
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 278
    .end local v2    # "_f":Ljava/io/File;
    .end local v3    # "_list":Lorg/json/JSONArray;
    .end local v5    # "exPath":Ljava/io/File;
    .end local v7    # "i":I
    .end local v10    # "obj2":Lorg/json/JSONObject;
    .end local v12    # "temp":Ljava/lang/String;
    :cond_0
    const-string v15, "mac"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v17, 0x10

    move/from16 v0, v17

    invoke-static {v6, v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v17

    or-int/lit8 v17, v17, 0x2

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 298
    .end local v6    # "firstMac":Ljava/lang/String;
    .end local v8    # "lastMac":Ljava/lang/String;
    .end local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v4

    .line 299
    .local v4, "e":Ljava/lang/NumberFormatException;
    sget-object v15, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "unable to get MAC address : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v4}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    .end local v4    # "e":Ljava/lang/NumberFormatException;
    .end local v11    # "ret":Ljava/lang/String;
    :goto_2
    return-object v11

    .line 296
    .restart local v3    # "_list":Lorg/json/JSONArray;
    .restart local v6    # "firstMac":Ljava/lang/String;
    .restart local v7    # "i":I
    .restart local v8    # "lastMac":Ljava/lang/String;
    .restart local v11    # "ret":Ljava/lang/String;
    .restart local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .restart local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_1
    :try_start_1
    const-string v15, "list"

    invoke-virtual {v9, v15, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 297
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v11

    goto :goto_2

    .line 301
    .end local v3    # "_list":Lorg/json/JSONArray;
    .end local v6    # "firstMac":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v8    # "lastMac":Ljava/lang/String;
    .end local v13    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v14    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_1
    move-exception v4

    .line 302
    .local v4, "e":Lorg/json/JSONException;
    sget-object v15, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "unable to get MAC address : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v4}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 304
    .end local v4    # "e":Lorg/json/JSONException;
    :catch_2
    move-exception v4

    .line 305
    .local v4, "e":Ljava/lang/NullPointerException;
    sget-object v15, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "unable to get MAC address : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v4}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private isSbeamOn()Z
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v3, 0x0

    .line 237
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSbeamAvailable(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    .line 262
    :goto_0
    return v2

    .line 240
    :cond_0
    const/4 v2, 0x0

    .line 241
    .local v2, "sbeamEnabled":Z
    const/4 v1, 0x0

    .line 243
    .local v1, "mSharedPreferences":Landroid/content/SharedPreferences;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;

    if-nez v4, :cond_1

    .line 245
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    const-string v4, "com.android.settings"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;

    const-string v4, "pref_sbeam"

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 248
    const-string v3, "SBeam_on_off"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    goto :goto_0

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 251
    const/4 v2, 0x0

    .line 255
    goto :goto_0

    .line 252
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 254
    const/4 v2, 0x0

    .line 255
    goto :goto_0

    .line 257
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->sbeamSettingContext:Landroid/content/Context;

    const-string v5, "pref_sbeam"

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 259
    const-string v4, "SBeam_on_off"

    invoke-interface {v1, v4, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public continueDirectShare()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 318
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    const-string v3, "continueDirectShare E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSbeamAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 330
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->blockAndroidBeamSetting()Z

    move-result v2

    if-nez v2, :cond_1

    .line 331
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    const-string v3, ":: Android Beam !!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 335
    const/4 v2, 0x2

    new-array v1, v2, [Landroid/net/Uri;

    .line 336
    .local v1, "fileUris":[Landroid/net/Uri;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v1, v5

    .line 337
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v1, v6

    .line 339
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 340
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBeamPushUris video : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v5

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBeamPushUris subtitle : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v6

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;

    new-instance v4, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;

    invoke-direct {v4, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$3;-><init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;[Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v3, v4, v2}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 373
    .end local v1    # "fileUris":[Landroid/net/Uri;
    :cond_1
    :goto_1
    return-void

    .line 344
    :cond_2
    new-array v1, v6, [Landroid/net/Uri;

    .line 345
    .restart local v1    # "fileUris":[Landroid/net/Uri;
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v1, v5

    .line 347
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setBeamPushUris video : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v5

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;

    const-string v3, "setBeamPushUris. Android Beam subtitle path is NULL."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_1

    .line 368
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_1
.end method
