.class Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;
.super Ljava/lang/Object;
.source "PlayerListView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$502(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;Z)Z

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 511
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 517
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 505
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 8
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v7, 0xb

    const/16 v6, 0xa

    const/4 v5, 0x0

    .line 480
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$300(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 481
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$300(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget v3, v3, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    invoke-virtual {v2, v3, v5, v5, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 482
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$400(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/GridView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget v3, v3, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListRightPadding:I

    invoke-virtual {v2, v5, v5, v3, v5}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 483
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitleLayout:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget v3, v3, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget v4, v4, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget v4, v4, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 484
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setNumberOfColumns()V

    .line 485
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$300(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 487
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 488
    .local v0, "p":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 489
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 491
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 493
    .local v1, "q":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 495
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$000(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 496
    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 500
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 501
    return-void

    .line 498
    :cond_0
    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0
.end method
