.class public Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.super Landroid/widget/RelativeLayout;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field private static final CENTER_M_PROGRESS_VIEW:I = 0x0

.field private static final DELAYED_CMD_PLAYSHORT:I = 0x20

.field private static final DELAY_TREED_VIDE:I = 0x1f

.field private static final FFLONGSEEK:I = 0x8

.field protected static final FFW_RWD_NOT_SUPPORT:I = 0x1e

.field private static final LEFT_M_PROGRESS_VIEW:I = 0x1

.field private static final LONG_PRESS_TIME:J = 0x1f4L

.field private static final PROGRESS_POPUP_FLOATING_POINT:I = 0x19

.field private static final PROGRESS_RESOLUTION:J = 0x186a0L

.field private static final REWLONGSEEK:I = 0x9

.field private static final RIGHT_M_PROGRESS_VIEW:I = 0x2

.field private static final SHOW_PROGRESS:I = 0x1

.field private static final START_VIDEO_CAPTURE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoBtnController"

.field private static final UNINITIALIZED_MEDIAPLAYERSERVICE:I = -0x1


# instance fields
.field private HOVER_IMAGE_GAP:I

.field private PROGRESS_MARGIN:I

.field private mAniPlayPauseButton:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

.field private mAniPlayStopButton:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

.field private mArrowHeight:I

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

.field private mBottomPadding:I

.field private mButtonEnable:Z

.field private mCaptureBtn:Landroid/widget/ImageButton;

.field private mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mCaptureLayout:Landroid/widget/RelativeLayout;

.field private mContext:Landroid/content/Context;

.field private mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

.field private mCtrlLayoutShow:Z

.field private mCtrlLayoutState:Z

.field public mCurrentTime:Landroid/widget/TextView;

.field protected mDetailSeekViewStub:Landroid/view/View;

.field protected mDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mDownKeyPressTime:J

.field private mDuration:J

.field public mEndTime:Landroid/widget/TextView;

.field private mFfButton:Landroid/widget/ImageButton;

.field private mFfKeyListener:Landroid/view/View$OnKeyListener;

.field private mFfTouchListener:Landroid/view/View$OnTouchListener;

.field private mFitToSrcBtn:Landroid/widget/ImageButton;

.field private mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerProgressPreview:Landroid/os/Handler;

.field private mHoldLongSeekSpeed:Z

.field private mHoverDuration:J

.field private mHoverEventStart:Z

.field private mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

.field private mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

.field private mIsAlreadyPauseState:Z

.field private mIsSearch:Z

.field private mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

.field private mLScreenArrowBtnRight:Landroid/widget/ImageButton;

.field public mLScreenCtrlPos:I

.field private mLeftPadding:I

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mMiniArrowBtn:Landroid/widget/ImageButton;

.field private mMiniArrowBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mMiniArrowBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOneFrameBackwardBtn:Landroid/widget/ImageButton;

.field private mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOneFrameForwardBtn:Landroid/widget/ImageButton;

.field private mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPauseClickListener:Landroid/view/View$OnClickListener;

.field private mPauseKeyListener:Landroid/view/View$OnKeyListener;

.field private mPauseTouchListener:Landroid/view/View$OnTouchListener;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field private mPlaySpeedButton:Landroid/widget/RelativeLayout;

.field private mPlaySpeedTextView:Landroid/widget/TextView;

.field private mPlayerListBtn:Landroid/widget/ImageButton;

.field private mPlayerListBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mPlayerListBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressBar:Landroid/widget/SeekBar;

.field private mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressContent:Landroid/view/View;

.field private mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

.field private mProgressPreviewImage:Landroid/widget/ImageView;

.field private mProgressPreviewShowTime:Landroid/widget/TextView;

.field private mProgressThumbViewState:I

.field private mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mQuarterDetailedSeekText:Landroid/widget/TextView;

.field private mRewButton:Landroid/widget/ImageButton;

.field private mRewKeyListener:Landroid/view/View$OnKeyListener;

.field private mRewTouchListener:Landroid/view/View$OnTouchListener;

.field private mRightPadding:I

.field protected mRoot:Landroid/view/View;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

.field private mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

.field private mScrubbingSpeed:I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSpeedTextView:Landroid/widget/TextView;

.field private mTimetextHeight:I

.field private mTimetextWidth:I

.field private mTopPadding:I

.field private mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

.field private mVideoBtnSeekBarPosionX:F

.field private mVideoBtnSeekBarPosionY:F

.field private mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

.field private mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

.field private mVideoSeekBarProgressX:F

.field private mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

.field private mViewModeRes:[I

.field private mbProgressDragStatus:Z

.field private mbResume:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 260
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 92
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    .line 104
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->PROGRESS_MARGIN:I

    .line 106
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->HOVER_IMAGE_GAP:I

    .line 108
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    .line 110
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoHeight:I

    .line 112
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    .line 114
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRightPadding:I

    .line 116
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTopPadding:I

    .line 118
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBottomPadding:I

    .line 120
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextHeight:I

    .line 122
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextWidth:I

    .line 124
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mArrowHeight:I

    .line 126
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    .line 132
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J

    .line 134
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDownKeyPressTime:J

    .line 136
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverDuration:J

    .line 140
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I

    .line 142
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 144
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    .line 146
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 148
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 150
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 152
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 154
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .line 156
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .line 158
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    .line 160
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressContent:Landroid/view/View;

    .line 162
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    .line 164
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    .line 166
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    .line 168
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 170
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    .line 172
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    .line 174
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    .line 192
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 194
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    .line 196
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtn:Landroid/widget/ImageButton;

    .line 198
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    .line 200
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    .line 202
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 204
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 206
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    .line 208
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureLayout:Landroid/widget/RelativeLayout;

    .line 210
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 212
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 214
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 216
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    .line 218
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 220
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    .line 222
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mIsSearch:Z

    .line 224
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 226
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z

    .line 228
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverEventStart:Z

    .line 232
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mButtonEnable:Z

    .line 236
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    .line 238
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mIsAlreadyPauseState:Z

    .line 240
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    .line 242
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    .line 244
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F

    .line 246
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 252
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewModeRes:[I

    .line 545
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$5;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 582
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 638
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$7;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 675
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$8;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1050
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    .line 1106
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$12;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    .line 1148
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$13;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    .line 1604
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$16;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    .line 1954
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$17;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPauseClickListener:Landroid/view/View$OnClickListener;

    .line 1978
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$18;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    .line 2017
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$19;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    .line 2071
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$20;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    .line 2186
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$21;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    .line 2283
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    .line 2393
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$23;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    .line 2571
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$24;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    .line 2804
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 2829
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$26;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 3024
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$27;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 3104
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$28;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 3216
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$29;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$29;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    .line 3697
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$31;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$31;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 3744
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$32;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$32;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 3772
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$33;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$33;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 3820
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$34;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$34;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 3849
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$35;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$35;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 3897
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$36;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$36;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 261
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    .line 262
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 263
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object v0, p1

    .line 264
    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 265
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 266
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 267
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->PROGRESS_MARGIN:I

    .line 270
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080043

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->HOVER_IMAGE_GAP:I

    .line 272
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v0, :cond_1

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    const-string v1, "l_screen_controller_pos"

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    .line 276
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V

    .line 278
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initFloatingWindow()V

    .line 279
    return-void

    .line 252
    nop

    :array_0
    .array-data 4
        0x7f020256
        0x7f020259
        0x7f020253
        0x7f02025c
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->openPlaySpeed()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbResume:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mIsAlreadyPauseState:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->blockSpeedSeek()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDownKeyPressTime:J

    return-wide v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDownKeyPressTime:J

    return-wide p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverDuration:J

    return-wide v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverDuration:J

    return-wide p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverEventStart:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverEventStart:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    return p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoHeight:I

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoHeight:I

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRightPadding:I

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTopPadding:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBottomPadding:I

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextHeight:I

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mArrowHeight:I

    return v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Landroid/widget/HoverPopupWindow;)Landroid/widget/HoverPopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressContent:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 74
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;IZ)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I

    return v0
.end method

.method static synthetic access$4102(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScrubbingSpeed:I

    return p1
.end method

.method static synthetic access$4202(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    return p1
.end method

.method static synthetic access$4302(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    return p1
.end method

.method static synthetic access$4400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initDetailSeekViewStub()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMiniControllerMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F

    return v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoSeekBarProgressX:F

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;IZ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateProgressbarPreviewView(IZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->refreshFitToScrBtn()V

    return-void
.end method

.method private blockPlaySpeed()Z
    .locals 1

    .prologue
    .line 3670
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->blockPlaySpeed()Z

    move-result v0

    return v0
.end method

.method private blockSpeedSeek()Z
    .locals 1

    .prologue
    .line 3968
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLinkStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromDms()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private changeControllerLayout(Z)V
    .locals 5
    .param p1, "isMultiwindowMode"    # Z

    .prologue
    const v4, 0x7f0d0132

    .line 3600
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v2, :cond_0

    .line 3608
    :goto_0
    return-void

    .line 3601
    :cond_0
    const/4 v1, 0x0

    .line 3602
    .local v1, "margin":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3603
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 3604
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    .line 3606
    :cond_1
    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 3607
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getAniDeltaX()I
    .locals 4

    .prologue
    .line 3975
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 3976
    .local v1, "mDm":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v2, :cond_0

    .line 3977
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 3979
    :cond_0
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    sub-int v0, v2, v3

    .line 3981
    .local v0, "delta":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_1

    .line 3982
    div-int/lit8 v0, v0, 0x2

    .line 3983
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3984
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080211

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 3988
    :cond_1
    return v0
.end method

.method private getFitToScnMode()I
    .locals 2

    .prologue
    .line 1567
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v0

    .line 1569
    .local v0, "retVal":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isWebVTTFileType()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_1

    .line 1571
    const/4 v0, 0x1

    .line 1574
    :cond_1
    return v0
.end method

.method private getMiniControllerMode()I
    .locals 1

    .prologue
    .line 3611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlMode()I

    move-result v0

    return v0
.end method

.method private getTransitionAni(I)Landroid/view/animation/Animation;
    .locals 4
    .param p1, "deltaX"    # I

    .prologue
    const/4 v2, 0x0

    .line 3991
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    int-to-float v1, p1

    invoke-direct {v0, v2, v1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 3992
    .local v0, "mAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3993
    new-instance v1, Landroid/view/animation/interpolator/SineInOut90;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineInOut90;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 3994
    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$37;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$37;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4014
    return-object v0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v8, 0x7f0d012c

    const v7, 0x7f0d012a

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 380
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initProgress(Landroid/view/View;)V

    .line 381
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initCtrlButton(Landroid/view/View;)V

    .line 383
    const v0, 0x7f0d0135

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 384
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 398
    :cond_1
    const v0, 0x7f0d010b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    .line 399
    const v0, 0x7f0d010a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00f8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setHoverPopupType(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    .line 413
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_d

    .line 414
    const v0, 0x7f0d0129

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureLayout:Landroid/widget/RelativeLayout;

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_c

    .line 418
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a014e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 428
    :cond_2
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0165

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 437
    :cond_3
    const v0, 0x7f0d012e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 442
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a016f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 446
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateCaptureBtn()V

    .line 456
    :goto_0
    const v0, 0x7f0d0110

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 459
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 462
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080142

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080145

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRightPadding:I

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080150

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTopPadding:I

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBottomPadding:I

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextWidth:I

    .line 467
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mTimetextHeight:I

    .line 468
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-nez v0, :cond_6

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08013f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mArrowHeight:I

    .line 472
    :cond_6
    const v0, 0x7f0d0142

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtn:Landroid/widget/ImageButton;

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_7

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMiniArrowBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 479
    :cond_7
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v0, :cond_b

    .line 480
    :cond_8
    const v0, 0x7f0d0143

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    if-eqz v0, :cond_9

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 509
    :cond_9
    const v0, 0x7f0d0144

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    if-eqz v0, :cond_a

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 538
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setAndUpdateTts()V

    .line 541
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    .line 543
    return-void

    .line 448
    :cond_c
    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 449
    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 450
    const v0, 0x7f0d012e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 453
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setRotateBtn(Landroid/content/Context;Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private initCtrlButton(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x3

    const/16 v9, 0x3031

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 814
    const v4, 0x7f0d0136

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 816
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080140

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    .line 818
    .local v0, "hoverCustomPopupOffset":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_0

    .line 819
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewModeRes:[I

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToScnMode()I

    move-result v6

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 820
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 821
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 822
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 823
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 825
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    .line 826
    .local v1, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v1, :cond_0

    .line 827
    invoke-virtual {v1, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 831
    .end local v1    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    const v4, 0x7f0d013c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 832
    const v4, 0x7f0d013d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mAniPlayPauseButton:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    .line 833
    const v4, 0x7f0d013e

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mAniPlayStopButton:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    .line 869
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_2

    .line 870
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 871
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 872
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 874
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    .line 875
    .restart local v1    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v1, :cond_1

    .line 876
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080143

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v2, v4

    .line 877
    .local v2, "offset":I
    invoke-virtual {v1, v2, v8}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 878
    invoke-virtual {v1, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 881
    .end local v2    # "offset":I
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePausePlayBtn()V

    .line 885
    .end local v1    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_2
    const v4, 0x7f0d0137

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    .line 887
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_4

    .line 888
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 889
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 890
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 891
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0170

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 894
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 896
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 897
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 898
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    if-eqz v4, :cond_3

    .line 899
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v7}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 900
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v8}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 901
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 902
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    new-instance v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$9;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v4, v5}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 938
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 941
    :cond_4
    const v4, 0x7f0d0139

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    .line 943
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v4, :cond_6

    .line 944
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 945
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 946
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 947
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a015c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 950
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 951
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v10}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 952
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 953
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    if-eqz v4, :cond_5

    .line 954
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v7}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 955
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v8}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 956
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v4, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    new-instance v5, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$10;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$10;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v4, v5}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 993
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 996
    :cond_6
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_7

    .line 997
    const v4, 0x7f0d013a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    .line 998
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_7

    .line 999
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1000
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0087

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1001
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1002
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1003
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    const v5, 0x7f020031

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1004
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1008
    :cond_7
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v4, :cond_9

    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v4, :cond_9

    .line 1009
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v4, :cond_8

    .line 1010
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setPopupPlayerBtn(Landroid/content/Context;Landroid/view/View;)V

    .line 1011
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->getPopupPlayerBtn()Landroid/widget/ImageButton;

    move-result-object v3

    .line 1012
    .local v3, "popupPlayerBtn":Landroid/widget/ImageButton;
    invoke-virtual {v3}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    .line 1013
    .restart local v1    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v1, :cond_8

    .line 1014
    invoke-virtual {v1, v9}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1020
    .end local v1    # "hpw":Landroid/widget/HoverPopupWindow;
    .end local v3    # "popupPlayerBtn":Landroid/widget/ImageButton;
    :cond_8
    :goto_0
    return-void

    .line 1018
    :cond_9
    const v4, 0x7f0d0138

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private initDetailSeekViewStub()V
    .locals 7

    .prologue
    const v6, 0x7f0a011c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2992
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2993
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0111

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2994
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0112

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    .line 2996
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 2997
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d014c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 2998
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d014d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    .line 2999
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1/2"

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3001
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0147

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 3002
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0148

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    .line 3003
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1/4"

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3005
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 3006
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setDetailedSeekHeight()V

    .line 3008
    :cond_1
    return-void
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 302
    const-string v2, "VideoBtnController"

    const-string v3, "VideoBtnController - initFloatingWindow"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 305
    .local v0, "mDecor":Landroid/view/View;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 306
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 308
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_0

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 311
    :cond_0
    return-void
.end method

.method private initProgress(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 767
    const v0, 0x7f0d011e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 769
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 772
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 773
    new-instance v0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    .line 775
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 776
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 777
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 778
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 779
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 780
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 782
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 783
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 785
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 786
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 806
    :cond_0
    :goto_0
    const v0, 0x7f0d0134

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    .line 807
    const v0, 0x7f0d0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 808
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 809
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 810
    return-void

    .line 791
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 793
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 794
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    .line 795
    :cond_2
    new-instance v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    .line 796
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->setDaemon(Z)V

    .line 797
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->start()V

    .line 799
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    goto :goto_0
.end method

.method private isMiniControllerMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3288
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getMiniControllerMode()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMultiwindowLandscapeMode()Z
    .locals 4

    .prologue
    .line 3294
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 3295
    const-string v2, "VideoBtnController"

    const-string v3, "isMultiwindowLandscapeMode. mContext is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3296
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v2

    .line 3302
    :goto_0
    return v2

    .line 3299
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;

    invoke-direct {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;-><init>()V

    .line 3300
    .local v0, "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 3302
    .local v1, "multiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->isFeatureEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->getVersionCode()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isProgressZoomPossible()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 735
    const-string v2, "VideoBtnController"

    const-string v3, "isProgressZoomPossible"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 763
    :cond_0
    :goto_0
    return v0

    .line 746
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 750
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 751
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 752
    goto :goto_0

    .line 759
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_0

    .line 762
    const-string v0, "VideoBtnController"

    const-string v2, "isProgressZoomPossible : true"

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 763
    goto :goto_0
.end method

.method private keepShowingController(I)V
    .locals 1
    .param p1, "timeout"    # I

    .prologue
    .line 4052
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 4053
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keepShowingController(I)V

    .line 4055
    :cond_0
    return-void
.end method

.method private openPlaySpeed()V
    .locals 2

    .prologue
    .line 3615
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    if-nez v0, :cond_0

    .line 3616
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    .line 3619
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$30;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$30;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->setOnUpdatedListener(Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup$OnUpdatedListener;)V

    .line 3625
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3626
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->showPopup()V

    .line 3629
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 3630
    :cond_2
    return-void
.end method

.method private refreshFitToScrBtn()V
    .locals 3

    .prologue
    .line 1536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1537
    :cond_0
    const-string v0, "VideoBtnController"

    const-string v1, "refreshFitToScrBtn() return"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    :goto_0
    return-void

    .line 1541
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mViewModeRes:[I

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToScnMode()I

    move-result v2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1544
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToScnMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1546
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a015d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1550
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1554
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a016a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1558
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0169

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1544
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private setDetailedSeekHeight()V
    .locals 3

    .prologue
    .line 3011
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3012
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3013
    .local v0, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 3014
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3022
    :goto_0
    return-void

    .line 3016
    .end local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 3017
    .restart local v0    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 3018
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08005b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 3020
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setEnableProgressbar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2517
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 2518
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 2520
    :cond_0
    return-void
.end method

.method private setPlayPauseButton(I)V
    .locals 5
    .param p1, "state"    # I

    .prologue
    .line 1823
    sget-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$ButtonState;->RELEASED:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$ButtonState;

    .line 1824
    .local v0, "buttonState":Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$ButtonState;
    const v2, 0x7f0a016c

    .line 1825
    .local v2, "descID":I
    const v3, 0x7f02001c

    .line 1826
    .local v3, "resID":I
    const-string v1, ""

    .line 1857
    .local v1, "desc":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 1858
    const v3, 0x7f02001c

    .line 1859
    const v2, 0x7f0a016c

    .line 1867
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1869
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 1870
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1872
    return-void

    .line 1860
    :cond_1
    const/4 v4, 0x1

    if-ne p1, v4, :cond_2

    .line 1861
    const v3, 0x7f02001b

    .line 1862
    const v2, 0x7f0a016b

    goto :goto_0

    .line 1863
    :cond_2
    const/4 v4, 0x2

    if-ne p1, v4, :cond_0

    .line 1864
    const v3, 0x7f02001f

    .line 1865
    const v2, 0x7f0a0172

    goto :goto_0
.end method

.method private setVisibleFitToScrnPopupPlayerBtn()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1492
    :goto_0
    return-void

    .line 1484
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiwindowMinimumControllerMode()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1486
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1487
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1489
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateFitToSrcBtn()V

    .line 1490
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePopupPlayerBtn()V

    goto :goto_0
.end method

.method private setVisibleLScreenArrowBtn()V
    .locals 6

    .prologue
    const v5, 0x7f0d0144

    const v3, 0x7f0d0143

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1519
    :goto_0
    return-void

    .line 1510
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1511
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1514
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v4, 0x1

    if-eq v0, v4, :cond_4

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    if-nez v0, :cond_7

    :cond_4
    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1516
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_5

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    if-nez v3, :cond_6

    :cond_5
    move v1, v2

    :cond_6
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1514
    goto :goto_1
.end method

.method private setVisibleLScreenArrowBtn4vasta()V
    .locals 6

    .prologue
    const v5, 0x7f0d0144

    const v4, 0x7f0d0143

    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1533
    :goto_0
    return-void

    .line 1523
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    if-eq v0, v3, :cond_3

    .line 1524
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1525
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1532
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0142

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1527
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v4, 0x1

    if-eq v0, v4, :cond_4

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    if-nez v0, :cond_6

    :cond_4
    move v0, v2

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_5

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    if-nez v3, :cond_7

    :cond_5
    :goto_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_6
    move v0, v1

    .line 1527
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1529
    goto :goto_3
.end method

.method private setVisiblePlayerListBtn()V
    .locals 4

    .prologue
    const v3, 0x7f0d013a

    const/4 v2, 0x0

    .line 1495
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v1, :cond_0

    .line 1506
    :goto_0
    return-void

    .line 1497
    :cond_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSupportPlaylist()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1498
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 1499
    .local v0, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1500
    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hidePlayerList(Z)V

    .line 1502
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1504
    .end local v0    # "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private stringForTime(IZ)Ljava/lang/String;
    .locals 4
    .param p1, "timeMs"    # I
    .param p2, "durationTime"    # Z

    .prologue
    .line 1662
    if-eqz p2, :cond_4

    .line 1663
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSavedDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1664
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 1676
    :goto_0
    return-object v0

    .line 1667
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1668
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1670
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1671
    :cond_2
    const-string v0, "--:--:--"

    goto :goto_0

    .line 1673
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1676
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateFitToSrcBtn()V
    .locals 4

    .prologue
    .line 1259
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-nez v2, :cond_0

    .line 1273
    :goto_0
    return-void

    .line 1262
    :cond_0
    const/4 v0, 0x1

    .line 1264
    .local v0, "enabled":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    .line 1265
    .local v1, "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1266
    :cond_1
    const/4 v0, 0x0

    .line 1269
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1270
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1271
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->invalidate()V

    .line 1272
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1270
    :cond_3
    const v2, 0x3ecccccd    # 0.4f

    goto :goto_1
.end method

.method private updatePlaySpeedText()Ljava/lang/String;
    .locals 7

    .prologue
    .line 3639
    const/4 v1, 0x0

    .line 3640
    .local v1, "playspeed":F
    const/4 v2, 0x0

    .line 3641
    .local v2, "text":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    .line 3643
    .local v0, "currentSpeed":I
    add-int/lit8 v3, v0, 0x5

    int-to-float v3, v3

    const v4, 0x3dcccccd    # 0.1f

    mul-float v1, v3, v4

    .line 3644
    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 3646
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private updateProgressbarPreviewView(IZ)I
    .locals 6
    .param p1, "progress"    # I
    .param p2, "isPortrait"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2540
    const-string v2, "VideoBtnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateProgressbarPreviewView progress : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " orientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2541
    const/4 v1, 0x0

    .line 2542
    .local v1, "x":I
    const/4 p2, 0x1

    .line 2544
    const/4 v0, 0x0

    .line 2545
    .local v0, "progressBarWidth":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v2, :cond_0

    .line 2546
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getWidth()I

    move-result v0

    .line 2549
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressContent:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 2550
    if-eqz p2, :cond_4

    .line 2551
    int-to-float v2, p1

    const v3, 0x47c35000    # 100000.0f

    div-float/2addr v2, v3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 2553
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->PROGRESS_MARGIN:I

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 2554
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->HOVER_IMAGE_GAP:I

    sub-int v1, v2, v3

    .line 2555
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    .line 2568
    :cond_1
    :goto_0
    return v1

    .line 2556
    :cond_2
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLeftPadding:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->PROGRESS_MARGIN:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_3

    .line 2557
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRightPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->HOVER_IMAGE_GAP:I

    sub-int/2addr v2, v3

    neg-int v1, v2

    .line 2558
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 2560
    :cond_3
    const/4 v1, 0x0

    .line 2561
    iput v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 2564
    :cond_4
    iput v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressThumbViewState:I

    goto :goto_0
.end method


# virtual methods
.method public canCaptureVideoFrame()Z
    .locals 1

    .prologue
    .line 3694
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->canCaptureVideoFrame()Z

    move-result v0

    return v0
.end method

.method public changeControllerLayout(I)V
    .locals 40
    .param p1, "windowWidth"    # I

    .prologue
    .line 3337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    if-eqz v37, :cond_12

    .line 3338
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0128

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/FrameLayout$LayoutParams;

    .line 3339
    .local v8, "LP_Top_Layout":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d012f

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 3341
    .local v5, "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d010a

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3342
    .local v6, "LP_PlaySpeed_Layout":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d010d

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3344
    .local v7, "LP_Rotate_Btn":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0133

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3345
    .local v17, "LP_time_current":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0134

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3346
    .local v18, "LP_time_total":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0136

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3347
    .local v11, "LP_fit_to_scr_btn":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0138

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3348
    .local v16, "LP_popup_player_btn":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d013a

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3349
    .local v12, "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0137

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3350
    .local v10, "LP_btn_rew":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0139

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    .line 3351
    .local v9, "LP_btn_ff":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/widget/FrameLayout$LayoutParams;

    .line 3353
    .local v15, "LP_mini_arrow_btn":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 3355
    .local v14, "LP_lscreen_arrow_btn_right":Landroid/widget/FrameLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/widget/FrameLayout$LayoutParams;

    .line 3358
    .local v13, "LP_lscreen_arrow_btn_left":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v20, -0x1

    .line 3360
    .local v20, "controller_layout_width":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080043

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v27, v0

    .line 3361
    .local v27, "margin_ctrl_progress":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080191

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v26, v0

    .line 3362
    .local v26, "margin_SIDE":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080190

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v25, v0

    .line 3363
    .local v25, "margin_REW":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080188

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v24, v0

    .line 3364
    .local v24, "margin_FF":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08017d

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v28, v0

    .line 3365
    .local v28, "margin_ctrl_top_layout_BOTTOM":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08015b

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v23, v0

    .line 3366
    .local v23, "margin_BOTTOM":I
    const/16 v22, 0x3

    .line 3368
    .local v22, "gravity":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08015d

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v21, v0

    .line 3370
    .local v21, "dimen_controller_width":I
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v37, :cond_0

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v37, :cond_3

    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v37

    if-eqz v37, :cond_1

    move/from16 v0, p1

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    .line 3371
    :cond_1
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-nez v37, :cond_2

    .line 3372
    move/from16 v20, v21

    .line 3374
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v37

    if-eqz v37, :cond_15

    .line 3375
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    move/from16 v37, v0

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_13

    .line 3376
    const/16 v22, 0x5

    .line 3386
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v37

    if-nez v37, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    if-eqz v37, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    check-cast v37, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v37

    if-eqz v37, :cond_3

    .line 3387
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08015c

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v23, v0

    .line 3388
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08015e

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v37, v0

    sub-int v37, v23, v37

    add-int v28, v28, v37

    .line 3393
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v37

    if-eqz v37, :cond_4

    .line 3394
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080192

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v28, v0

    .line 3397
    :cond_4
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_5

    .line 3398
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 3399
    const/16 v37, 0x9

    move/from16 v0, v37

    invoke-virtual {v12, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3400
    const/16 v37, 0x9

    move/from16 v0, v37

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 3401
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v11, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3404
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080043

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v31, v0

    .line 3405
    .local v31, "progress_Padding_left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d011e

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getPaddingTop()I

    move-result v33

    .line 3406
    .local v33, "progress_Padding_top":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080043

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v32, v0

    .line 3407
    .local v32, "progress_Padding_right":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d011e

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getPaddingBottom()I

    move-result v34

    .line 3409
    .local v34, "progress_padding_bottom":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    sget-object v38, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->NORMAL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/VUtils;->setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V

    .line 3412
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMultiwindowLandscapeMode()Z

    move-result v37

    if-eqz v37, :cond_17

    .line 3413
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f08018c

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f08018f

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v38

    add-float v37, v37, v38

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f08018f

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v38

    add-float v37, v37, v38

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v35, v0

    .line 3417
    .local v35, "resMinWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080161

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f080180

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v38

    add-float v37, v37, v38

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f080048

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v38

    add-float v37, v37, v38

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v37, v0

    add-int v36, v35, v37

    .line 3423
    .local v36, "resWidth":I
    move/from16 v0, v36

    move/from16 v1, p1

    if-le v0, v1, :cond_16

    .line 3424
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    sget-object v38, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_MINIMUM:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/VUtils;->setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V

    .line 3425
    sub-int v37, p1, v35

    div-int/lit8 v37, v37, 0x2

    move/from16 v0, v37

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    .line 3426
    const/16 v24, 0x0

    move/from16 v25, v24

    .line 3433
    :cond_6
    :goto_1
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_7

    move/from16 v0, p1

    move/from16 v1, v21

    if-gt v0, v1, :cond_7

    .line 3434
    const/16 v23, 0x0

    .line 3437
    :cond_7
    const-string v37, "VideoBtnController"

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "changeControllerLayout Layout "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, " / "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    const-string v39, " : "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, v38

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3438
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_8

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_9

    move/from16 v0, p1

    move/from16 v1, v21

    if-gt v0, v1, :cond_9

    .line 3439
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080045

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v32, v27

    move/from16 v31, v27

    .line 3441
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080048

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v26, v0

    .line 3443
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0130

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const v38, 0x7f0201c2

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3444
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3445
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleFitToScrnPopupPlayerBtn()V

    .line 3470
    .end local v35    # "resMinWidth":I
    .end local v36    # "resWidth":I
    :goto_2
    or-int/lit8 v37, v22, 0x50

    move/from16 v0, v37

    iput v0, v8, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3471
    or-int/lit8 v37, v22, 0x50

    move/from16 v0, v37

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3472
    or-int/lit8 v37, v22, 0x50

    move/from16 v0, v37

    iput v0, v15, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3473
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v37, :cond_a

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v37, :cond_b

    .line 3474
    :cond_a
    or-int/lit8 v37, v22, 0x50

    move/from16 v0, v37

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3475
    or-int/lit8 v37, v22, 0x50

    move/from16 v0, v37

    iput v0, v13, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 3479
    :cond_b
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 3480
    const/16 v37, 0x9

    move/from16 v0, v37

    invoke-virtual {v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3481
    const/16 v37, 0x9

    move/from16 v0, v37

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 3482
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 3485
    move/from16 v0, v20

    iput v0, v8, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 3486
    move/from16 v0, v20

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 3489
    move/from16 v0, v27

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v0, v27

    move-object/from16 v1, v17

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v0, v27

    move-object/from16 v1, v18

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3490
    move/from16 v0, v26

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3491
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v0, v37

    iput v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3493
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_1b

    .line 3494
    move/from16 v0, v26

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3495
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3496
    move/from16 v0, v26

    iput v0, v12, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3503
    :goto_3
    move/from16 v0, v25

    iput v0, v10, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3504
    move/from16 v0, v24

    iput v0, v9, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3505
    move/from16 v0, v28

    iput v0, v8, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 3506
    move/from16 v0, v23

    iput v0, v5, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 3509
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VUtils;->getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-result-object v37

    sget-object v38, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    if-ne v0, v1, :cond_c

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-nez v37, :cond_c

    .line 3510
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080183

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v37, v0

    add-int v29, v20, v37

    .line 3512
    .local v29, "miniArrowMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlRightPos()I

    move-result v37

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_1c

    .line 3514
    move/from16 v0, v29

    iput v0, v15, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3516
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v15, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 3519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const v38, 0x7f020019

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0a012f

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3533
    .end local v29    # "miniArrowMargin":I
    :cond_c
    :goto_4
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v37, :cond_d

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v37, :cond_f

    :cond_d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMultiwindowLandscapeMode()Z

    move-result v37

    if-nez v37, :cond_f

    .line 3534
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080183

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v19, v0

    .line 3535
    .local v19, "arrowMargin":I
    add-int v29, v20, v19

    .line 3536
    .restart local v29    # "miniArrowMargin":I
    div-int/lit8 v37, v20, 0x2

    add-int v37, v37, v19

    div-int/lit8 v38, v19, 0x2

    add-int v30, v37, v38

    .line 3538
    .local v30, "miniArrowMarginForCenter":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    move/from16 v37, v0

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_1d

    .line 3539
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v37

    if-eqz v37, :cond_e

    .line 3540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3542
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3543
    move/from16 v0, v29

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3544
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 3563
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v37

    if-nez v37, :cond_f

    .line 3564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3570
    .end local v19    # "arrowMargin":I
    .end local v29    # "miniArrowMargin":I
    .end local v30    # "miniArrowMarginForCenter":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d011e

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    move/from16 v1, v31

    move/from16 v2, v33

    move/from16 v3, v32

    move/from16 v4, v34

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 3572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0128

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v8}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3573
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d010a

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d012f

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0133

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0134

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0136

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v11}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d013a

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v12}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3579
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0137

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0139

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v9}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v15}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3583
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v37, :cond_10

    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v37, :cond_21

    .line 3584
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v14}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v13}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3586
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v37, :cond_11

    .line 3587
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changePlayerListBtn()V

    .line 3594
    :cond_11
    :goto_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMultiwindowLandscapeMode()Z

    move-result v37

    move-object/from16 v0, p0

    move/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(Z)V

    .line 3595
    const-string v37, "VideoBtnController"

    const-string v38, "changeControllerLayout Layout Changed"

    invoke-static/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3597
    .end local v5    # "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .end local v6    # "LP_PlaySpeed_Layout":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "LP_Rotate_Btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "LP_Top_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .end local v9    # "LP_btn_ff":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v10    # "LP_btn_rew":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v11    # "LP_fit_to_scr_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v12    # "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v13    # "LP_lscreen_arrow_btn_left":Landroid/widget/FrameLayout$LayoutParams;
    .end local v14    # "LP_lscreen_arrow_btn_right":Landroid/widget/FrameLayout$LayoutParams;
    .end local v15    # "LP_mini_arrow_btn":Landroid/widget/FrameLayout$LayoutParams;
    .end local v16    # "LP_popup_player_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v17    # "LP_time_current":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v18    # "LP_time_total":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v20    # "controller_layout_width":I
    .end local v21    # "dimen_controller_width":I
    .end local v22    # "gravity":I
    .end local v23    # "margin_BOTTOM":I
    .end local v24    # "margin_FF":I
    .end local v25    # "margin_REW":I
    .end local v26    # "margin_SIDE":I
    .end local v27    # "margin_ctrl_progress":I
    .end local v28    # "margin_ctrl_top_layout_BOTTOM":I
    .end local v31    # "progress_Padding_left":I
    .end local v32    # "progress_Padding_right":I
    .end local v33    # "progress_Padding_top":I
    .end local v34    # "progress_padding_bottom":I
    :cond_12
    return-void

    .line 3377
    .restart local v5    # "LP_Controller_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v6    # "LP_PlaySpeed_Layout":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v7    # "LP_Rotate_Btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v8    # "LP_Top_Layout":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v9    # "LP_btn_ff":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v10    # "LP_btn_rew":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v11    # "LP_fit_to_scr_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v12    # "LP_list_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v13    # "LP_lscreen_arrow_btn_left":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v14    # "LP_lscreen_arrow_btn_right":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v15    # "LP_mini_arrow_btn":Landroid/widget/FrameLayout$LayoutParams;
    .restart local v16    # "LP_popup_player_btn":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v17    # "LP_time_current":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v18    # "LP_time_total":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v20    # "controller_layout_width":I
    .restart local v21    # "dimen_controller_width":I
    .restart local v22    # "gravity":I
    .restart local v23    # "margin_BOTTOM":I
    .restart local v24    # "margin_FF":I
    .restart local v25    # "margin_REW":I
    .restart local v26    # "margin_SIDE":I
    .restart local v27    # "margin_ctrl_progress":I
    .restart local v28    # "margin_ctrl_top_layout_BOTTOM":I
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    move/from16 v37, v0

    const/16 v38, 0x2

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_14

    .line 3378
    const/16 v22, 0x3

    goto/16 :goto_0

    .line 3380
    :cond_14
    const/16 v22, 0x11

    goto/16 :goto_0

    .line 3383
    :cond_15
    const/16 v22, 0x11

    goto/16 :goto_0

    .line 3428
    .restart local v31    # "progress_Padding_left":I
    .restart local v32    # "progress_Padding_right":I
    .restart local v33    # "progress_Padding_top":I
    .restart local v34    # "progress_padding_bottom":I
    .restart local v35    # "resMinWidth":I
    .restart local v36    # "resWidth":I
    :cond_16
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    sget-object v38, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_FULL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/VUtils;->setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V

    .line 3429
    sub-int v37, p1, v36

    div-int/lit8 v37, v37, 0x2

    move/from16 v0, v37

    move/from16 v1, v25

    if-ge v0, v1, :cond_6

    .line 3430
    const/16 v24, 0x0

    move/from16 v25, v24

    goto/16 :goto_1

    .line 3446
    .end local v35    # "resMinWidth":I
    .end local v36    # "resWidth":I
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isMiniControllerMode()Z

    move-result v37

    if-eqz v37, :cond_1a

    .line 3447
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v37

    sget-object v38, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual/range {v37 .. v38}, Lcom/sec/android/app/videoplayer/common/VUtils;->setControllerMode(Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;)V

    .line 3448
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080042

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v25, v24

    .line 3449
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080044

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v32, v27

    move/from16 v31, v27

    .line 3451
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080047

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v26, v0

    .line 3453
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f080049

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v37

    move/from16 v0, v37

    float-to-int v0, v0

    move/from16 v20, v0

    .line 3455
    sget-boolean v37, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-nez v37, :cond_18

    .line 3456
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-static/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getMiniCtrlRightPos()I

    move-result v37

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_19

    .line 3457
    const/16 v22, 0x5

    .line 3462
    :cond_18
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0130

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const v38, 0x7f0201e8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3463
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 3459
    :cond_19
    const/16 v22, 0x3

    goto :goto_7

    .line 3465
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0130

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const v38, 0x7f0201c2

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_2

    .line 3498
    :cond_1b
    const/16 v37, 0x0

    move/from16 v0, v37

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move/from16 v0, v37

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 3499
    move/from16 v0, v26

    iput v0, v11, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 3500
    move/from16 v0, v26

    move-object/from16 v1, v16

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_3

    .line 3526
    .restart local v29    # "miniArrowMargin":I
    :cond_1c
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v15, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3527
    move/from16 v0, v29

    iput v0, v15, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 3528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const v38, 0x7f020018

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0142

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0a0130

    invoke-virtual/range {v38 .. v39}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 3545
    .restart local v19    # "arrowMargin":I
    .restart local v30    # "miniArrowMarginForCenter":I
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    move/from16 v37, v0

    if-nez v37, :cond_1f

    .line 3546
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v37

    if-eqz v37, :cond_1e

    .line 3547
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3550
    :cond_1e
    move/from16 v0, v30

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3551
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 3552
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v13, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3553
    move/from16 v0, v30

    iput v0, v13, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto/16 :goto_5

    .line 3555
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0143

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x8

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3556
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v37

    if-eqz v37, :cond_20

    .line 3557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0144

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    const/16 v38, 0x0

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->setVisibility(I)V

    .line 3559
    :cond_20
    const/16 v37, 0x0

    move/from16 v0, v37

    iput v0, v13, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 3560
    move/from16 v0, v29

    iput v0, v13, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    goto/16 :goto_5

    .line 3590
    .end local v19    # "arrowMargin":I
    .end local v29    # "miniArrowMargin":I
    .end local v30    # "miniArrowMarginForCenter":I
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d0138

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    move-object/from16 v37, v0

    const v38, 0x7f0d010d

    invoke-virtual/range {v37 .. v38}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    move-object/from16 v0, v37

    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_6
.end method

.method public changeFitToSrcBtn(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1252
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    .line 1256
    :cond_0
    return-void
.end method

.method public changePlayerListBtn()V
    .locals 4

    .prologue
    .line 3310
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 3326
    :goto_0
    return-void

    .line 3312
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 3314
    .local v0, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3315
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020030

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 3320
    :goto_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getItemCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 3322
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0

    .line 3317
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020031

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 3324
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayerListBtn:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public dismissAllHoveringPopup()V
    .locals 1

    .prologue
    .line 2793
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2794
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/HoverPopupWindow;->dismiss()V

    .line 2797
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    if-eqz v0, :cond_1

    .line 2798
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 2800
    :cond_1
    return-void
.end method

.method public dismissPlaySpeed()V
    .locals 1

    .prologue
    .line 3633
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3634
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoPlaySpeedpopup:Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->dismiss()V

    .line 3636
    :cond_0
    return-void
.end method

.method public forceHide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1348
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    if-nez v0, :cond_1

    .line 1349
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibility(I)V

    .line 1350
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setInvisibleAllViews()V

    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 1352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 1354
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 1355
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z

    .line 1357
    :cond_1
    return-void
.end method

.method public forceUpdateProgressBarSize()V
    .locals 2

    .prologue
    .line 3331
    const-string v0, "VideoBtnController"

    const-string v1, "force update progress bar size for multiwindow mode changing."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3335
    return-void
.end method

.method public getFitToSrcBtn()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 1103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public hide()V
    .locals 4

    .prologue
    const v3, 0x7f0d012f

    const/4 v2, 0x0

    .line 1309
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    if-nez v1, :cond_0

    .line 1310
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1311
    const-string v1, "VideoBtnController"

    const-string v2, "hide : findViewById() is null view not inflate yet!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1345
    :cond_0
    :goto_0
    return-void

    .line 1315
    :cond_1
    const/4 v0, 0x0

    .line 1316
    .local v0, "translateOff":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1317
    .restart local v0    # "translateOff":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1318
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 1319
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1320
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibility(I)V

    .line 1321
    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$15;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1339
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 1341
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    if-eqz v1, :cond_0

    .line 1342
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    goto :goto_0
.end method

.method public hide(Z)V
    .locals 2
    .param p1, "hide"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1360
    if-eqz p1, :cond_1

    .line 1361
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    if-eqz v0, :cond_0

    .line 1362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 1364
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibility(I)V

    .line 1365
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 1366
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutState:Z

    .line 1368
    :cond_1
    return-void
.end method

.method public isBtnShow()Z
    .locals 2

    .prologue
    .line 3674
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    .line 3675
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnLongSeekMode()Z
    .locals 1

    .prologue
    .line 2014
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method public isProgressDraging()Z
    .locals 1

    .prologue
    .line 1371
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method protected isSKTCloudContentNow()Z
    .locals 2

    .prologue
    .line 1375
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_0

    .line 1376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v0

    .line 1378
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 1196
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 361
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 362
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03001f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    .line 363
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPadding()V

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initControllerView(Landroid/view/View;)V

    .line 365
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->showAllshareBtnControl(Z)V

    .line 367
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1578
    const-string v0, "VideoBtnController"

    const-string v1, "VideoBtnController - onDetachedFromWindow"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stopUpdateProgress()V

    .line 1581
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1582
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 1583
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 1584
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initControllerView(Landroid/view/View;)V

    .line 285
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v10, 0x0

    const/4 v11, 0x1

    .line 4019
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    if-nez v8, :cond_1

    .line 4039
    :cond_0
    :goto_0
    return v11

    .line 4020
    :cond_1
    const/4 v8, 0x2

    new-array v4, v8, [I

    .line 4021
    .local v4, "screenPos":[I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 4022
    .local v2, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 4023
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 4025
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 4026
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 4027
    .local v7, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 4028
    .local v3, "height":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v5, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 4030
    .local v5, "screenWidth":I
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 4031
    .local v6, "viewStr":Ljava/lang/String;
    invoke-static {v1, v6, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 4033
    .local v0, "cheatSheet":Landroid/widget/Toast;
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v8, :cond_2

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a00f8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 4034
    mul-int/lit8 v3, v3, 0x2

    .line 4037
    :cond_2
    const/16 v8, 0x35

    aget v9, v4, v10

    sub-int v9, v5, v9

    div-int/lit8 v10, v7, 0x2

    sub-int/2addr v9, v10

    aget v10, v4, v11

    sub-int/2addr v10, v3

    invoke-virtual {v0, v8, v9, v10}, Landroid/widget/Toast;->setGravity(III)V

    .line 4038
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1292
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbResume:Z

    .line 1294
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v1, :cond_1

    .line 1305
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stopUpdateProgress()V

    .line 1306
    return-void

    .line 1297
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1298
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 1299
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 1300
    sget-object v1, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eq v1, v3, :cond_0

    .line 1301
    sget-object v1, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1276
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbResume:Z

    .line 1278
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->startUpdateProgress()V

    .line 1279
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 1289
    :cond_0
    :goto_0
    return-void

    .line 1282
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1283
    new-instance v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    .line 1284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->setDaemon(Z)V

    .line 1285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->start()V

    goto :goto_0
.end method

.method public playerStop()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2523
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2524
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2525
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2528
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2529
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2530
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2533
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 2534
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 2537
    :cond_2
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3276
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 3277
    return-void
.end method

.method public removeHandler()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-wide/16 v0, 0x0

    .line 288
    const-string v2, "VideoBtnController"

    const-string v3, "removeHandler"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mIsSearch:Z

    if-eqz v2, :cond_1

    .line 291
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeMessage(I)V

    .line 292
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeMessage(I)V

    move-wide v2, v0

    move v6, v5

    .line 293
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 294
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    if-eqz v2, :cond_0

    .line 295
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    .line 296
    iget v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionX:F

    iget v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVideoBtnSeekBarPosionY:F

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public removeHoverPopup()V
    .locals 1

    .prologue
    .line 2785
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_0

    .line 2786
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    if-eqz v0, :cond_0

    .line 2787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 2790
    :cond_0
    return-void
.end method

.method public removeMessage(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1596
    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    if-ne p1, v2, :cond_1

    .line 1597
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mIsSearch:Z

    .line 1598
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1602
    :cond_1
    return-void
.end method

.method public resetHoldLongSeek()V
    .locals 2

    .prologue
    .line 2007
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    if-eqz v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2009
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z

    .line 2011
    :cond_0
    return-void
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1587
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1589
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1590
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1591
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 323
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeAllViews()V

    .line 325
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 328
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 329
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 332
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 334
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 337
    :cond_0
    const v3, 0x7f0d012f

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 358
    return-void
.end method

.method public setAndUpdateTts()V
    .locals 5

    .prologue
    const v4, 0x7f0a0131

    .line 3927
    const/4 v0, 0x0

    .local v0, "description_left":Ljava/lang/String;
    const/4 v1, 0x0

    .line 3928
    .local v1, "description_right":Ljava/lang/String;
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 3929
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3937
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    if-eqz v2, :cond_0

    .line 3938
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnRight:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3939
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenArrowBtnLeft:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3941
    :cond_0
    return-void

    .line 3930
    :cond_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 3931
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 3933
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a012f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 3934
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0130

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 3174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 3175
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 3176
    return-void
.end method

.method public setButtonArrange()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1941
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDirectDMC()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileExternal()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isContentExternal()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getTutorialWidget()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getUriArray()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1946
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    const v1, 0x7f020197

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    const v1, 0x7f02018d

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1952
    :goto_0
    return-void

    .line 1949
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    const v1, 0x7f02019a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    const v1, 0x7f020189

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public setDisableRewFfBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    const v2, 0x7f0d0139

    const v1, 0x7f0d0137

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1025
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 1027
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1029
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1034
    :cond_0
    return-void
.end method

.method public setEnableRewFfBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x7f0d0139

    const v2, 0x7f0d0137

    .line 1037
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1039
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1040
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 1041
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1043
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 1046
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1048
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2490
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 2514
    :goto_0
    return-void

    .line 2494
    :cond_0
    const-string v0, "VideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2496
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2499
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 2500
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2502
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 2503
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2505
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 2506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2508
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    .line 2509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 2512
    :cond_5
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mButtonEnable:Z

    .line 2513
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 3280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 3281
    return-void
.end method

.method public setInvisibleAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1382
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1427
    :goto_0
    return-void

    .line 1384
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0135

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1386
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0137

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d013c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1388
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0139

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1389
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1391
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0140

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1392
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0132

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0133

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0134

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0128

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1397
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1400
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1409
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0141

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1410
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d011e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0142

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d013a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1416
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0143

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0144

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1419
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0129

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1421
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1422
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1426
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    goto/16 :goto_0
.end method

.method public setPadding()V
    .locals 3

    .prologue
    const/16 v2, 0x53

    const/4 v1, 0x0

    .line 371
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v2, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1, v1, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setProgress()I
    .locals 18

    .prologue
    .line 1685
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbProgressDragStatus:Z

    if-eqz v14, :cond_2

    .line 1686
    :cond_0
    const/4 v10, 0x0

    .line 1781
    :cond_1
    :goto_0
    return v10

    .line 1689
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v10

    .line 1690
    .local v10, "position":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v4

    .line 1692
    .local v4, "duration":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v14, :cond_f

    .line 1693
    const/16 v14, 0x3e8

    if-le v4, v14, :cond_e

    .line 1694
    const-wide/32 v14, 0x186a0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    int-to-long v0, v4

    move-wide/from16 v16, v0

    div-long v8, v14, v16

    .line 1696
    .local v8, "pos":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v14

    if-nez v14, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1697
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1702
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getBufferPercentage()I

    move-result v7

    .line 1703
    .local v7, "percent":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v14

    if-nez v14, :cond_4

    .line 1704
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    mul-int/lit16 v15, v7, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 1708
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1709
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 1710
    const-string v14, "VideoBtnController"

    const-string v15, "setProgress: MediaHub file download incomplete"

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1712
    const-wide/16 v12, 0x0

    .line 1714
    .local v12, "progress_prctg":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getMediahubDownloadingPercent()J

    move-result-wide v12

    .line 1716
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_5

    .line 1717
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setIncompletedMediaHub(Z)V

    .line 1720
    :cond_5
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_c

    .line 1721
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 1726
    :goto_2
    const-string v14, "VideoBtnController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MediaHub file downloading percent: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1742
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    .end local v12    # "progress_prctg":J
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1743
    if-gtz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1744
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1748
    :cond_7
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v5

    .line 1749
    .local v5, "endnewTime":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1750
    .local v6, "endnowTime":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1752
    .local v3, "currentnowTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    .line 1753
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1754
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 1755
    if-lez v4, :cond_8

    .line 1756
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1757
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1762
    :cond_8
    move v11, v10

    .line 1763
    .local v11, "time":I
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v2

    .line 1765
    .local v2, "currentnewTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v14, :cond_9

    .line 1766
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1768
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 1769
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1770
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1774
    :cond_9
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v14, :cond_1

    .line 1775
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    if-nez v14, :cond_a

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_a

    .line 1776
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 1778
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    if-nez v14, :cond_1

    .line 1779
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    goto/16 :goto_0

    .line 1699
    .end local v2    # "currentnewTime":Ljava/lang/String;
    .end local v3    # "currentnowTime":Ljava/lang/String;
    .end local v5    # "endnewTime":Ljava/lang/String;
    .end local v6    # "endnowTime":Ljava/lang/String;
    .end local v11    # "time":I
    .restart local v8    # "pos":J
    :cond_b
    const-string v14, "VideoBtnController"

    const-string v15, "setProgress: mServiceUtil.isPauseEnable() is true."

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1723
    .restart local v7    # "percent":I
    .restart local v12    # "progress_prctg":J
    :cond_c
    long-to-float v14, v12

    const v15, 0x3f7ae148    # 0.98f

    mul-float/2addr v14, v15

    float-to-long v12, v14

    .line 1724
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_2

    .line 1728
    .end local v12    # "progress_prctg":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_3

    .line 1733
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    :cond_e
    const-string v14, "VideoBtnController"

    const-string v15, "setProgress: duration is less than zero"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_3

    .line 1739
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1682
    return-void
.end method

.method public setProgressMax()V
    .locals 8

    .prologue
    .line 1785
    const-string v5, "VideoBtnController"

    const-string v6, "setProgressMax()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1786
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v5, :cond_0

    .line 1787
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v6, 0x186a0

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1792
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v2

    .line 1794
    .local v2, "duration":I
    const/4 v5, 0x1

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    .line 1795
    .local v3, "endnewTime":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1796
    .local v4, "endnowTime":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1798
    .local v1, "currentnowTime":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 1799
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1800
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1801
    if-lez v2, :cond_1

    .line 1802
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1803
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1808
    :cond_1
    const/4 v5, 0x0

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v0

    .line 1810
    .local v0, "currentnewTime":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 1811
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1813
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1814
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1815
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1819
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->stopUpdateProgress()V

    .line 1820
    return-void
.end method

.method public setSpeedTextViewVisibility(ZF)V
    .locals 7
    .param p1, "visibility"    # Z
    .param p2, "speed"    # F

    .prologue
    const/4 v6, 0x0

    .line 3193
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 3194
    if-eqz p1, :cond_3

    .line 3195
    new-instance v0, Ljava/lang/String;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 3196
    .local v0, "speedString":Ljava/lang/String;
    float-to-double v2, p2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v1, v2, v4

    if-nez v1, :cond_1

    .line 3197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1/2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3204
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0123

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3205
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3206
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3207
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 3208
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 3214
    .end local v0    # "speedString":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 3198
    .restart local v0    # "speedString":Ljava/lang/String;
    :cond_1
    float-to-double v2, p2

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    .line 3199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1/4"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3201
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    float-to-int v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 3211
    .end local v0    # "speedString":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSpeedTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setUpdate()V
    .locals 1

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePausePlayBtn()V

    .line 315
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v0

    if-nez v0, :cond_0

    .line 318
    const/16 v0, 0xbb8

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V

    .line 320
    :cond_0
    return-void
.end method

.method public setVisibleAllViews()V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x4

    const/4 v1, 0x0

    .line 1430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1478
    :cond_0
    :goto_0
    return-void

    .line 1432
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d012f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1433
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0135

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0137

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1435
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0139

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d013c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0140

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0141

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d011e

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1442
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v0, :cond_4

    .line 1443
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleLScreenArrowBtn4vasta()V

    .line 1448
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0132

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0133

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0134

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1451
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0128

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d010f

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1455
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setDisableRewFfBtn()V

    .line 1460
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateBtnControllerBtns()V

    .line 1462
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    .line 1463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0129

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1464
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_2

    .line 1465
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateCaptureBtn()V

    .line 1467
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisiblePlayerListBtn()V

    .line 1468
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleLScreenArrowBtn()V

    .line 1470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d010d

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1473
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1474
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 1445
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0142

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getControllerMode()Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    move-result-object v0

    sget-object v4, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    if-ne v0, v4, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_3

    .line 1457
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableRewFfBtn()V

    goto :goto_2
.end method

.method public show()V
    .locals 4

    .prologue
    const v3, 0x7f0d012f

    const/4 v2, 0x0

    .line 1200
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbResume:Z

    if-eqz v1, :cond_2

    .line 1201
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1202
    const-string v1, "VideoBtnController"

    const-string v2, "show : findViewById() is null view not inflate yet!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    :goto_0
    return-void

    .line 1206
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleAllViews()V

    .line 1207
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPadding()V

    .line 1208
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->refreshFitToScrBtn()V

    .line 1210
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1211
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateAutoRotationBtn()V

    .line 1214
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setProgress()I

    .line 1215
    const/4 v0, 0x0

    .line 1217
    .local v0, "translateOn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1218
    .restart local v0    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1219
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 1221
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1222
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibility(I)V

    .line 1224
    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$14;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$14;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1238
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCtrlLayoutShow:Z

    .line 1241
    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    :cond_2
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_4

    .line 1242
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mbResume:Z

    if-eqz v1, :cond_3

    .line 1243
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateCaptureBtn()V

    .line 1245
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changePlayerListBtn()V

    .line 1248
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->startUpdateProgress()V

    goto :goto_0
.end method

.method public showAllshareBtnControl(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 3179
    const-string v0, "VideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showAllshareBtnControl visible:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3181
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateFitToSrcBtn()V

    .line 3183
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v0, :cond_0

    .line 3184
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableProgressbar(Z)V

    .line 3187
    :cond_0
    if-eqz p1, :cond_1

    .line 3188
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleAllViews()V

    .line 3190
    :cond_1
    return-void
.end method

.method public startUpdateProgress()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 4047
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 4049
    return-void
.end method

.method public stopUpdateProgress()V
    .locals 2

    .prologue
    .line 4043
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4044
    return-void
.end method

.method public updateBtnControllerBtns()V
    .locals 2

    .prologue
    .line 3956
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 3965
    :goto_0
    return-void

    .line 3958
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_1

    .line 3959
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateAutoRotationBtn()V

    .line 3963
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleFitToScrnPopupPlayerBtn()V

    .line 3964
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    goto :goto_0

    .line 3961
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateCaptureBtn()V
    .locals 2

    .prologue
    .line 3681
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    .line 3682
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->isCaptureModeOn()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->canCaptureVideoFrame()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3683
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 3689
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_1

    .line 3690
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateOneFrameForwardBackwardBtn(Landroid/view/View;)V

    .line 3691
    :cond_1
    return-void

    .line 3685
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mCaptureBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public updatePausePlayBtn()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1876
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1935
    :cond_0
    :goto_0
    return-void

    .line 1879
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1880
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableRewFfBtn()V

    .line 1881
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableProgressbar(Z)V

    .line 1883
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1884
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1885
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    .line 1886
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableProgressbar(Z)V

    goto :goto_0

    .line 1888
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    .line 1889
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableProgressbar(Z)V

    goto :goto_0

    .line 1892
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1893
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 1921
    :cond_4
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 1926
    :cond_5
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnableProgressbar(Z)V

    .line 1927
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setDisableRewFfBtn()V

    .line 1929
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1930
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 1932
    :cond_6
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0
.end method

.method public updatePlayListBtn()V
    .locals 1

    .prologue
    .line 3944
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 3945
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisiblePlayerListBtn()V

    .line 3947
    :cond_0
    return-void
.end method

.method public updatePlaySpeedBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 3650
    const/4 v0, 0x0

    .line 3652
    .local v0, "playSeepdBtnVisible":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPlaySpeedVisibility()I

    move-result v2

    sget v3, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->ON:I

    if-ne v2, v3, :cond_0

    .line 3653
    const/4 v0, 0x1

    .line 3656
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 3657
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->blockPlaySpeed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 3658
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedText()Ljava/lang/String;

    move-result-object v1

    .line 3659
    .local v1, "speed":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3660
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3661
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3667
    .end local v1    # "speed":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 3663
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 3664
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mPlaySpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public updatePopupPlayerBtn()V
    .locals 2

    .prologue
    .line 3950
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    .line 3953
    :cond_0
    :goto_0
    return-void

    .line 3952
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updatePopupPlayerBtn(Landroid/view/View;)V

    goto :goto_0
.end method
