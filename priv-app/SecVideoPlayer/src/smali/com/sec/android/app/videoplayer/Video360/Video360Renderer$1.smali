.class Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;
.super Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;
.source "Video360Renderer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onRotationChanged(JFFFLcom/sec/android/app/videoplayer/Video360/Quaternion;)V
    .locals 5
    .param p1, "time"    # J
    .param p3, "azimuth"    # F
    .param p4, "pitch"    # F
    .param p5, "roll"    # F
    .param p6, "q"    # Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mLocker:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->access$000(Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 82
    :try_start_0
    invoke-virtual {p6}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->asMatrix3x3()[F

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mXAxis:[F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mYAxis:[F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mZAxis:[F

    invoke-static {v0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->extractBasis([F[F[F[F)V

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer$1;->this$0:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->mXAxis:[F

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/MathUtil;->negate([F)V

    .line 84
    monitor-exit v1

    .line 85
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
