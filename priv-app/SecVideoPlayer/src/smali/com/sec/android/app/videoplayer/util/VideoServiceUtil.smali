.class public Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;,
        Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;,
        Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;,
        Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;
    }
.end annotation


# static fields
.field public static final BUFFERING_CHECK_DELAY:I = 0x12c

.field public static final CMD_FFLONGSEEK:I = 0x6

.field public static final CMD_FFLONGSEEK_FOR_EXT_SCR:I = 0x10

.field public static final CMD_FFSHORTSEEK:I = 0x4

.field public static final CMD_HOLD_LONG_SEEK_SPEED:I = 0xf

.field public static final CMD_NEXT:I = 0x8

.field public static final CMD_NEXT_BY_KEYPAD:I = 0xc

.field public static final CMD_PLAYSHORT:I = 0x3

.field public static final CMD_PREV:I = 0x9

.field public static final CMD_PREV_BY_KEYPAD:I = 0xd

.field public static final CMD_RESET:I = 0xb

.field public static final CMD_RESETSEEK:I = 0xa

.field public static final CMD_REWLONGSEEK:I = 0x7

.field public static final CMD_REWLONGSEEK_FOR_EXT_SCR:I = 0x11

.field public static final CMD_REWSHORTSEEK:I = 0x5

.field private static final LONGSEEKMSG:I = 0x3

.field public static final LONG_PRESS_TIME:J = 0x1f4L

.field public static final LONG_PRESS_TIME_SLOWSPEED:J = 0x3e8L

.field public static final LOW_BATTERY_THRESHOLD:I = 0x1

.field public static final MAXPLAYINGCHECKTIME:I = 0xc350

.field public static MoviePlayerOnResume:Z = false

.field public static final NOTI_FINISH:I = 0x0

.field public static final NOTI_HIDE_AUDIO_ONLY_VIEW:I = 0x7

.field public static final NOTI_HIDE_TAG_BUDDY:I = 0x9

.field public static final NOTI_PLAY_CHANGE:I = 0x2

.field public static final NOTI_PLAY_STOP:I = 0x1

.field public static final NOTI_PROGRESS_BAR:I = 0x5

.field public static final NOTI_SHOW_AUDIO_ONLY_VIEW:I = 0x6

.field public static final NOTI_SHOW_CONTROL:I = 0x4

.field public static final NOTI_SHOW_SPEED:I = 0x3

.field public static final NOTI_SHOW_TVOUT_VIEW:I = 0xb

.field public static final NOTI_STATE_VIEW_STATUS:I = 0xd

.field public static final NOTI_UPDATE_ASF_BTN:I = 0xc

.field public static final NOTI_UPDATE_SMART_PAUSE_STATUS:I = 0xa

.field public static final NOTI_UPDATE_TITEL:I = 0x8

.field public static final NO_TIMEOUT:I = 0x36ee80

.field public static final PLAYER_PAUSE:I = 0x1

.field public static final PLAYER_PLAY:I = 0x0

.field public static final PLAYER_PREPARED:I = 0x4

.field public static final PLAYER_PREPARING:I = 0x3

.field public static final PLAYER_STOP:I = 0x2

.field public static final PLAYING_CHECK_DELAY:I = 0x12c

.field public static final SAVE_BEFORE:I = 0x1388

.field public static final SEEK_MODE_FF:I = 0x1

.field public static final SEEK_MODE_NORMAL:I = 0x0

.field public static final SEEK_MODE_REW:I = 0x2

.field private static final STARTDIRECTDMC:I = 0x7

.field private static final STARTPLAYBACK:I = 0x1

.field private static final STARTPLAYDMR:I = 0x6

.field private static final TAG:Ljava/lang/String; = "VideoServiceUtil"

.field private static sPresentationConnectionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/content/Context;",
            "Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;",
            ">;"
        }
    .end annotation
.end field

.field private static sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private static sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;


# instance fields
.field CurPosition:J

.field Duration:J

.field public bAsfError:Z

.field public mAlertDialog:Landroid/app/Dialog;

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mBTMediaBtnStatus:Ljava/lang/String;

.field private mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

.field public mBufferingChecker:Ljava/lang/Runnable;

.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mCurPlayingUriTemp:Landroid/net/Uri;

.field private mDisableCaptureState:Z

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mHandler:Landroid/os/Handler;

.field private mHoldLongSeekSpeed:Z

.field private mIsBound:Z

.field private mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

.field private mLastPos:J

.field private mLongKeyCnt:I

.field private mLongSeekMode:I

.field private mMoveSameFile:Z

.field private mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

.field public mOnSvcNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

.field private mPauseSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mPausedByUser:Z

.field private mPausedOnPlaying:Z

.field private mPlayListUtil:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

.field private mPlayerState:I

.field private mPlayingBeforePalm:Z

.field private mPlayingCheckTime:I

.field public mPlayingChecker:Ljava/lang/Runnable;

.field private mResumePosition:J

.field private mResumePositionForVideoExtensionState:J

.field public mSDPDownloader:Ljava/lang/Runnable;

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSeekPos:I

.field private mSelectedAudioTrack:I

.field private mSubtitleTrackChangedOnPresentationMode:Z

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mVideoListType:I

.field private sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 144
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationConnectionMap:Ljava/util/HashMap;

    .line 156
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayListUtil:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    .line 145
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 150
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    .line 152
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 153
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 155
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedByUser:Z

    .line 157
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    .line 158
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    .line 159
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 160
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSubtitleTrackChangedOnPresentationMode:Z

    .line 161
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDisableCaptureState:Z

    .line 162
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 164
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 165
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoListType:I

    .line 166
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayerState:I

    .line 167
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSelectedAudioTrack:I

    .line 168
    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 169
    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 170
    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 171
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBTMediaBtnStatus:Ljava/lang/String;

    .line 173
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 174
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 178
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 181
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCurPlayingUriTemp:Landroid/net/Uri;

    .line 184
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 185
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 187
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    .line 191
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mMoveSameFile:Z

    .line 525
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->CurPosition:J

    .line 526
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->Duration:J

    .line 1430
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$1;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    .line 1493
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$2;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    .line 2024
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mIsBound:Z

    .line 2026
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .line 2028
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$3;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    .line 2167
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$4;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    .line 2407
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSDPDownloader:Ljava/lang/Runnable;

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    return p1
.end method

.method static synthetic access$020(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ffSeek()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->rewSeek()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Lcom/sec/android/app/videoplayer/service/IPresentationService;)Lcom/sec/android/app/videoplayer/service/IPresentationService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    return-object p1
.end method

.method private ffSeek()V
    .locals 13

    .prologue
    const/high16 v12, -0x40800000    # -1.0f

    const/16 v11, 0xa

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 766
    const-string v4, "VideoServiceUtil"

    const-string v5, "ffSeek E"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 769
    const/4 v2, 0x0

    .line 771
    .local v2, "scale":I
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    const/16 v5, 0xc

    if-le v4, v5, :cond_0

    .line 772
    const/16 v4, 0xc

    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 776
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v4

    if-nez v4, :cond_2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 777
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    if-eqz v4, :cond_2

    .line 778
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 858
    :cond_1
    :goto_0
    return-void

    .line 783
    :cond_2
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-nez v4, :cond_3

    .line 784
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 787
    :cond_3
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    if-lt v4, v10, :cond_5

    .line 788
    const/4 v2, 0x1

    .line 796
    :goto_1
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    .line 797
    .local v1, "powVal":F
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPowValForAllShareList(F)F

    move-result v1

    .line 799
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    int-to-float v4, v4

    mul-int/lit16 v5, v2, 0x3e8

    int-to-float v5, v5

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 800
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 802
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v5

    if-le v4, v5, :cond_4

    .line 803
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 804
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 807
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 808
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v0

    .line 811
    .local v0, "duration":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getMediahubDownloadingPercent()J

    move-result-wide v4

    long-to-int v4, v4

    mul-int/2addr v4, v0

    div-int/lit8 v3, v4, 0x64

    .line 812
    .local v3, "secProgress":I
    const-string v4, "VideoServiceUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ffSeek. duration : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " secProgress : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-ge v4, v3, :cond_7

    .line 815
    const-string v4, "VideoServiceUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ffSeek. MediaHub file seek. seekTo : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 829
    .end local v0    # "duration":I
    .end local v3    # "secProgress":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v5, 0x5

    invoke-interface {v4, v5, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 831
    const/4 v4, -0x1

    if-ne v2, v4, :cond_9

    .line 832
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-gtz v4, :cond_9

    .line 833
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v5, 0x4

    invoke-interface {v4, v5, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 834
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v4, v8, v12}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 835
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 836
    invoke-virtual {p0, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 789
    .end local v1    # "powVal":F
    :cond_5
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    const/4 v5, -0x1

    if-lt v4, v5, :cond_6

    .line 790
    const/4 v2, 0x1

    .line 791
    iput v10, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    goto/16 :goto_1

    .line 793
    :cond_6
    const/4 v2, -0x1

    goto/16 :goto_1

    .line 818
    .restart local v0    # "duration":I
    .restart local v1    # "powVal":F
    .restart local v3    # "secProgress":I
    :cond_7
    const-string v4, "VideoServiceUtil"

    const-string v5, "ffSeek. MediaHub file seek over total size."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v5, 0x4

    invoke-interface {v4, v5, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 820
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v4, v8, v12}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 821
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 822
    invoke-virtual {p0, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 826
    .end local v0    # "duration":I
    .end local v3    # "secProgress":I
    :cond_8
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_2

    .line 841
    :cond_9
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v5

    if-ge v4, v5, :cond_c

    .line 842
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    if-nez v4, :cond_a

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    if-ne v4, v10, :cond_a

    .line 843
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updatePopupPlayerBtn()V

    .line 845
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v4, v8, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 846
    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, v1, v4

    if-gez v4, :cond_b

    cmpl-float v4, v1, v9

    if-lez v4, :cond_b

    .line 847
    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v8, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    .line 851
    :goto_3
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    if-nez v4, :cond_1

    .line 852
    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    goto/16 :goto_0

    .line 849
    :cond_b
    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v8, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto :goto_3

    .line 855
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 856
    invoke-virtual {p0, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0
.end method

.method private getDisableCaptureState()Z
    .locals 1

    .prologue
    .line 2747
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDisableCaptureState:Z

    return v0
.end method

.method private getFileName(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;
    .locals 18
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 2261
    const-string v3, " "

    .line 2262
    .local v3, "displayName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v14

    .line 2263
    .local v14, "uri":Landroid/net/Uri;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v15

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getCurIdx()I

    move-result v2

    .line 2264
    .local v2, "currentIdx":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v12

    .line 2266
    .local v12, "playListUtil":Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFullBrowsableMode()Z

    move-result v15

    if-nez v15, :cond_0

    .line 2267
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 2370
    :goto_0
    return-object v15

    .line 2270
    :cond_0
    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getPlayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 2272
    .local v11, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v11, :cond_1

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    .line 2273
    :cond_1
    if-eqz v14, :cond_2

    .line 2274
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    goto :goto_0

    .line 2277
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v15, :cond_3

    .line 2278
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 2281
    :cond_3
    sget-boolean v15, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v15, :cond_4

    .line 2282
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2286
    :cond_4
    sget-object v15, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 2287
    add-int/lit8 v2, v2, -0x1

    .line 2289
    if-gez v2, :cond_5

    .line 2290
    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v15

    add-int/lit8 v2, v15, -0x1

    .line 2298
    :cond_5
    :goto_1
    if-eqz v11, :cond_6

    .line 2299
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "uri":Landroid/net/Uri;
    check-cast v14, Landroid/net/Uri;

    .line 2302
    .restart local v14    # "uri":Landroid/net/Uri;
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_4

    :cond_7
    sget-boolean v15, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v15, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v15, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2304
    :cond_8
    if-eqz v14, :cond_17

    .line 2305
    invoke-virtual {v14}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isBrowser(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v15

    if-eqz v15, :cond_d

    .line 2306
    :cond_9
    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2307
    .local v5, "filePath":Ljava/lang/String;
    const/16 v15, 0x2f

    invoke-virtual {v5, v15}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 2308
    .local v6, "lastIndex1":I
    const/16 v15, 0x2e

    invoke-virtual {v5, v15}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v7

    .line 2310
    .local v7, "lastIndex2":I
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v6, v15, :cond_c

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-ge v7, v15, :cond_c

    .line 2312
    add-int/lit8 v15, v6, 0x1

    :try_start_0
    invoke-virtual {v5, v15, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 2363
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v6    # "lastIndex1":I
    .end local v7    # "lastIndex2":I
    :cond_a
    :goto_2
    const-string v15, " "

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_17

    .line 2364
    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "content://mms"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_17

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "content://gmail-ls"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_17

    .line 2366
    invoke-virtual {v14}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_0

    .line 2291
    :cond_b
    sget-object v15, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2292
    add-int/lit8 v2, v2, 0x1

    .line 2294
    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v15

    add-int/lit8 v15, v15, -0x1

    if-le v2, v15, :cond_5

    .line 2295
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2313
    .restart local v5    # "filePath":Ljava/lang/String;
    .restart local v6    # "lastIndex1":I
    .restart local v7    # "lastIndex2":I
    :catch_0
    move-exception v4

    .line 2314
    .local v4, "e":Ljava/lang/StringIndexOutOfBoundsException;
    const-string v3, " "

    .line 2315
    goto :goto_2

    .line 2317
    .end local v4    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_c
    const-string v3, " "

    goto :goto_2

    .line 2318
    .end local v5    # "filePath":Ljava/lang/String;
    .end local v6    # "lastIndex1":I
    .end local v7    # "lastIndex2":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v15

    if-eqz v15, :cond_e

    .line 2319
    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "file://"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2320
    new-instance v13, Ljava/io/File;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2322
    .local v13, "temp":Ljava/io/File;
    if-eqz v13, :cond_a

    .line 2323
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2324
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 2328
    .end local v13    # "temp":Ljava/io/File;
    :cond_e
    const/4 v10, 0x0

    .line 2330
    .local v10, "newUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v15

    if-nez v15, :cond_f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v15

    if-eqz v15, :cond_12

    .line 2331
    :cond_f
    move-object v10, v14

    .line 2343
    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v15, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 2345
    if-nez v3, :cond_a

    .line 2346
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v15, :cond_10

    .line 2347
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 2350
    :cond_10
    sget-boolean v15, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v15, :cond_11

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v15, :cond_11

    .line 2351
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v15}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 2354
    :cond_11
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v15, :cond_15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v15, v10}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_15

    .line 2355
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2332
    :cond_12
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v15

    if-eqz v15, :cond_14

    .line 2333
    const-wide/16 v8, -0x1

    .line 2334
    .local v8, "mVideoDbId":J
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v15, v14}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v8

    .line 2336
    const-wide/16 v16, -0x1

    cmp-long v15, v8, v16

    if-nez v15, :cond_13

    .line 2337
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v14}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v8

    .line 2339
    :cond_13
    sget-object v15, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    .line 2340
    goto/16 :goto_3

    .line 2341
    .end local v8    # "mVideoDbId":J
    :cond_14
    move-object v10, v14

    goto/16 :goto_3

    .line 2356
    :cond_15
    sget-boolean v15, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v15, :cond_16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v15, :cond_16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v15, v10}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_16

    .line 2357
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2359
    :cond_16
    const-string v3, " "

    goto/16 :goto_2

    .end local v10    # "newUri":Landroid/net/Uri;
    :cond_17
    move-object v15, v3

    .line 2370
    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1

    .prologue
    .line 196
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v0, :cond_0

    .line 197
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 199
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method private getPowValForAllShareList(F)F
    .locals 1
    .param p1, "val"    # F

    .prologue
    .line 936
    move v0, p1

    .line 944
    .local v0, "powVal":F
    return v0
.end method

.method public static getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;
    .locals 1

    .prologue
    .line 2522
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    return-object v0
.end method

.method private isBrowser(Ljava/lang/String;)Z
    .locals 1
    .param p1, "scheme"    # Ljava/lang/String;

    .prologue
    .line 2228
    const-string v0, "http"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "rtsp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "sshttp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2229
    :cond_0
    const/4 v0, 0x1

    .line 2231
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHubContent(Landroid/net/Uri;)Z
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v2, 0x0

    .line 559
    if-nez p1, :cond_1

    .line 562
    :cond_0
    :goto_0
    return v2

    .line 560
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    .line 561
    .local v0, "db":Lcom/sec/android/app/videoplayer/db/VideoDB;
    const-string v3, "pricing_type_code"

    invoke-virtual {v0, p1, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 562
    .local v1, "pricingCode":Ljava/lang/String;
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isShortSeekDisalbedCase()Z
    .locals 4

    .prologue
    .line 1365
    const/4 v0, 0x0

    .line 1367
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getTutorialWidget()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSStream()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getUriArray()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1370
    :cond_0
    const/4 v0, 0x1

    .line 1373
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1374
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_2

    .line 1375
    const/4 v0, 0x1

    .line 1381
    :cond_2
    :goto_0
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isShortSeekDisalbedCase() - getPlayerState():"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", ret : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    return v0

    .line 1377
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    .line 1378
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private rewSeek()V
    .locals 12

    .prologue
    const/16 v5, -0xc

    const/4 v11, 0x0

    const/4 v10, -0x2

    const/4 v9, 0x0

    const/4 v8, 0x3

    .line 862
    const-string v3, "VideoServiceUtil"

    const-string v4, "rewSeek E."

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 865
    const/4 v2, 0x0

    .line 867
    .local v2, "scale":I
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    if-ge v3, v5, :cond_0

    .line 868
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 871
    :cond_0
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-nez v3, :cond_1

    .line 872
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 875
    :cond_1
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    if-gt v3, v10, :cond_6

    .line 876
    const/4 v2, -0x1

    .line 884
    :goto_0
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-float v1, v4

    .line 885
    .local v1, "powVal":F
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPowValForAllShareList(F)F

    move-result v1

    .line 887
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    if-ne v3, v10, :cond_2

    .line 888
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updatePopupPlayerBtn()V

    .line 890
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v3, v8, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 891
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    int-to-float v3, v3

    mul-int/lit16 v4, v2, 0x3e8

    int-to-float v4, v4

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 892
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    int-to-long v4, v3

    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 894
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-gtz v3, :cond_3

    .line 895
    iput v11, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 898
    :cond_3
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0, v3, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 900
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v4, 0x5

    invoke-interface {v3, v4, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 902
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 904
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 905
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v4

    if-lt v3, v4, :cond_4

    .line 906
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 907
    const/16 v3, 0xa

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 911
    :cond_4
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    if-lez v3, :cond_9

    .line 913
    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_8

    cmpl-float v3, v1, v9

    if-lez v3, :cond_8

    .line 914
    const-wide/16 v4, 0x3e8

    invoke-virtual {p0, v8, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    .line 918
    :goto_1
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    if-nez v3, :cond_5

    .line 919
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 933
    :cond_5
    :goto_2
    return-void

    .line 877
    .end local v0    # "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .end local v1    # "powVal":F
    :cond_6
    iget v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    const/4 v4, 0x1

    if-gt v3, v4, :cond_7

    .line 878
    const/4 v2, -0x1

    .line 879
    iput v10, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    goto/16 :goto_0

    .line 881
    :cond_7
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 916
    .restart local v0    # "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .restart local v1    # "powVal":F
    :cond_8
    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v8, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto :goto_1

    .line 921
    :cond_9
    if-eqz v0, :cond_5

    .line 922
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isOnLongSeekMode()Z

    move-result v3

    if-nez v3, :cond_a

    .line 923
    const-string v3, "rewSeek"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 926
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v4, 0x4

    invoke-interface {v3, v4, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 927
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/high16 v4, -0x40800000    # -1.0f

    invoke-interface {v3, v8, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 928
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetLongSeekHold()V

    .line 929
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removeMessage(I)V

    .line 930
    iput v11, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    goto :goto_2
.end method

.method private setDisableCapture(Z)V
    .locals 2
    .param p1, "isDisable"    # Z

    .prologue
    .line 2735
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 2737
    .local v0, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    if-eqz v0, :cond_0

    .line 2738
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setVisibility(I)V

    .line 2739
    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setSecure(Z)V

    .line 2740
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setVisibility(I)V

    .line 2741
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setDisableCaptureState(Z)V

    .line 2743
    :cond_0
    return-void
.end method

.method public static setSideSyncInfo(Lcom/sec/android/app/videoplayer/data/SideSyncInfo;)V
    .locals 0
    .param p0, "sideSyncInfo"    # Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .prologue
    .line 2526
    sput-object p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 2527
    return-void
.end method

.method private shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z
    .locals 12
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 582
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "shiftContent() - direction:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    sget-boolean v7, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCompletedListenrCall:Z

    if-eqz v7, :cond_0

    .line 585
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent() - mCompletedListenrCall is true"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    const/4 v7, 0x1

    .line 742
    :goto_0
    return v7

    .line 590
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFullBrowsableMode()Z

    move-result v7

    if-nez v7, :cond_1

    .line 591
    const/4 v7, 0x0

    goto :goto_0

    .line 595
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->resetPlaySpeed()V

    .line 598
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_2

    .line 599
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 600
    .local v1, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v1, :cond_2

    .line 601
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 604
    .end local v1    # "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    :cond_2
    const/4 v7, -0x1

    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSelectedAudioTrack:I

    .line 606
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    .line 607
    .local v3, "oldUri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 609
    .local v2, "newUri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 610
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent. Download tab and play store hub file."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 614
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    .line 616
    .local v0, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->createLocalCursorOnDevice()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_3

    .line 617
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent() : DownloadList is empty"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    const/4 v7, 0x0

    goto :goto_0

    .line 621
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getMediaHubPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, p1, v7}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getFilePathOnDevice(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 623
    .local v4, "path":Ljava/lang/String;
    if-nez v4, :cond_4

    .line 624
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent() : getFilePathOnDevice() is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    const/4 v7, 0x0

    goto :goto_0

    .line 627
    :cond_4
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "path = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 631
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 633
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 635
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 636
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sendBroadcastPlayedContentInfo()V

    .line 726
    .end local v0    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    .end local v4    # "path":Ljava/lang/String;
    :goto_1
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_360:Z

    if-eqz v7, :cond_5

    .line 727
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    .line 730
    :cond_5
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "oldUri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "newUri : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v7

    if-eqz v7, :cond_16

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 734
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mMoveSameFile:Z

    .line 739
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetTrackInfo()V

    .line 740
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startNextPrevPlayback(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 741
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitle()V

    .line 742
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 637
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 638
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent() - SCHEME_SLINK_LAUNCH_TYPE"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 641
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->updatePlayingdata(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 642
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 644
    .restart local v4    # "path":Ljava/lang/String;
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_8

    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 646
    :cond_8
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 648
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 650
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/slink/SLink;->getResumePosition()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 651
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startNextPrevPlayback(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 653
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    new-instance v8, Landroid/content/Intent;

    const-string v9, "videoplayer.set.controller.play.stop"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 655
    const/4 v7, 0x1

    goto/16 :goto_0

    .line 657
    .end local v4    # "path":Ljava/lang/String;
    :cond_9
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getPlayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 659
    .local v5, "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v5, :cond_a

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ge v7, v8, :cond_b

    .line 660
    :cond_a
    const-string v7, "VideoServiceUtil"

    const-string v8, "shiftContent() : mPlayList is empty"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 664
    :cond_b
    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 666
    const/4 v6, 0x0

    .line 668
    .local v6, "uri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v7, :cond_c

    .line 669
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 672
    :cond_c
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v7, :cond_d

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v7, :cond_d

    .line 673
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 677
    :cond_d
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateContentIndex(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V

    .line 678
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v7

    new-instance v8, Landroid/content/Intent;

    const-string v9, "videoplayer.set.controller.play.stop"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 680
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getUriCurrentIdx()Landroid/net/Uri;

    move-result-object v6

    .line 682
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v7, :cond_e

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_e

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_d

    :cond_e
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_f

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isContentReady(Landroid/net/Uri;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 685
    :cond_f
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v7

    if-eqz v7, :cond_11

    .line 686
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-gez v7, :cond_11

    .line 687
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "shiftContent() invalid uri: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getCurIdx()I

    move-result v7

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v7, v8, :cond_10

    .line 689
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->removeUri()V

    .line 690
    sget-object v7, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v7, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 691
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setPrevItemIndex()V

    .line 694
    :cond_10
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z

    move-result v7

    goto/16 :goto_0

    .line 698
    :cond_11
    move-object v2, v6

    .line 700
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 701
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v7

    if-eqz v7, :cond_12

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    if-eqz v7, :cond_12

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v7

    if-eqz v7, :cond_12

    .line 702
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    if-eqz v7, :cond_12

    .line 703
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->updatePlayerListView()V

    .line 707
    :cond_12
    if-eqz v6, :cond_14

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content://cloud/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_13

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "content://com.skp.tcloud/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 709
    :cond_13
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v8, 0x23

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 713
    :cond_14
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v7

    if-eqz v7, :cond_15

    .line 714
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 719
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v8, 0xc

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 720
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 722
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "shiftContent() mCurIdx = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getCurIdx()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " mIdArrayListCnt = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    const-string v7, "VideoServiceUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "shiftContent() uri = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mResumePosition = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 716
    :cond_15
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    goto :goto_3

    .line 736
    .end local v5    # "playList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v6    # "uri":Landroid/net/Uri;
    :cond_16
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mMoveSameFile:Z

    goto/16 :goto_2
.end method

.method private updateContentIndex(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V
    .locals 1
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 746
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setPrevItemIndex()V

    .line 755
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 749
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setNextItemIndex()V

    goto :goto_0

    .line 750
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->BOTTOM:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 751
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setLastItemIndex()V

    goto :goto_0

    .line 752
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->TOP:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 753
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setFirstItemIndex()V

    goto :goto_0
.end method


# virtual methods
.method public AddViewPresentationChangeSurface()V
    .locals 2

    .prologue
    .line 2668
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2670
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_ChangeSurfaceAddView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2675
    :cond_0
    :goto_0
    return-void

    .line 2671
    :catch_0
    move-exception v0

    .line 2672
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public DisableTvOutPresentation()V
    .locals 2

    .prologue
    .line 2648
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2650
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_DisableTvOut()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2655
    :cond_0
    :goto_0
    return-void

    .line 2651
    :catch_0
    move-exception v0

    .line 2652
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public DismissPresentation()V
    .locals 2

    .prologue
    .line 2626
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2628
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_Dismiss()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2633
    :cond_0
    :goto_0
    return-void

    .line 2629
    :catch_0
    move-exception v0

    .line 2630
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public RemoveDisableTvOutPresentation()V
    .locals 2

    .prologue
    .line 2658
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2660
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_RemoveDisableTvOut()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2665
    :cond_0
    :goto_0
    return-void

    .line 2661
    :catch_0
    move-exception v0

    .line 2662
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public RemoveViewPresentationChangeSurface()V
    .locals 2

    .prologue
    .line 2678
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2680
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_ChangeSurfaceRemoveView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2685
    :cond_0
    :goto_0
    return-void

    .line 2681
    :catch_0
    move-exception v0

    .line 2682
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public ShowPresentation()V
    .locals 2

    .prologue
    .line 2616
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v1, :cond_0

    .line 2618
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v1}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->Presentation_Show()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2623
    :cond_0
    :goto_0
    return-void

    .line 2619
    :catch_0
    move-exception v0

    .line 2620
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1922
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1923
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 1927
    :goto_0
    return-void

    .line 1925
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "addOutbandSubTitle() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bindService()Z
    .locals 4

    .prologue
    .line 2049
    const-string v0, "VideoServiceUtil"

    const-string v1, "bindService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2054
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mIsBound:Z

    .line 2055
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setServiceContext()V

    .line 2056
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mIsBound:Z

    return v0
.end method

.method public bindToPresentationService(Landroid/content/ServiceConnection;Ljava/lang/Boolean;)Z
    .locals 8
    .param p1, "callback"    # Landroid/content/ServiceConnection;
    .param p2, "showPresentation"    # Ljava/lang/Boolean;

    .prologue
    .line 2075
    const-string v4, "VideoServiceUtil"

    const-string v5, "bindToPresentationService E."

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2076
    new-instance v0, Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v5, Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2077
    .local v0, "cn":Landroid/content/ComponentName;
    const/4 v2, 0x0

    .line 2078
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2079
    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.action.SHOW_PRESENTATION"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 2083
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2084
    new-instance v3, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;-><init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Landroid/content/ServiceConnection;)V

    .line 2085
    .local v3, "sb":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationConnectionMap:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2088
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2089
    .local v1, "i":Landroid/content/Intent;
    const-string v4, "app_name"

    const-string v5, "video"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2090
    const-string v4, "user_id"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2091
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 2093
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v7, Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v4

    return v4

    .line 2081
    .end local v1    # "i":Landroid/content/Intent;
    .end local v3    # "sb":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;
    :cond_0
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public bottom()Z
    .locals 1

    .prologue
    .line 574
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->BOTTOM:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public bufferstate()V
    .locals 6

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 2152
    const-string v1, "VideoServiceUtil"

    const-string v2, "bufferstate"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2155
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 2157
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v0, :cond_0

    .line 2158
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 2159
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    .line 2165
    .end local v0    # "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    :cond_0
    :goto_0
    return-void

    .line 2162
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2163
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public changePlayer(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 2727
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changePlayer() mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2729
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2730
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->changePlayer(I)V

    .line 2732
    :cond_0
    return-void
.end method

.method public controlRequest(I)V
    .locals 10
    .param p1, "command"    # I

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 948
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() command = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    sparse-switch p1, :sswitch_data_0

    .line 965
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1361
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 955
    :sswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 956
    :cond_2
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - Player not initialized or doesn\'t support seek"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 967
    :pswitch_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSmartPauseOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 969
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 970
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v3, 0xa

    invoke-interface {v2, v3, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 976
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 978
    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 979
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-eq v2, v5, :cond_1

    .line 980
    invoke-virtual {p0, v7, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 981
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 982
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 983
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 984
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    goto :goto_0

    .line 972
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v3, 0xa

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_1

    .line 987
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v5, :cond_7

    .line 988
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    .line 989
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 990
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 991
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto/16 :goto_0

    .line 992
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-eq v2, v7, :cond_8

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v9, :cond_1

    .line 993
    :cond_8
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 994
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    goto/16 :goto_0

    .line 998
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 999
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1000
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 1001
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    goto/16 :goto_0

    .line 1003
    :cond_a
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_b

    .line 1004
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    .line 1005
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1006
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1007
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto/16 :goto_0

    .line 1009
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 1010
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    goto/16 :goto_0

    .line 1014
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1015
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 1016
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    goto/16 :goto_0

    .line 1018
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_e

    .line 1019
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    .line 1020
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1021
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1022
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto/16 :goto_0

    .line 1024
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_1

    .line 1025
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v2

    if-eqz v2, :cond_10

    .line 1026
    :cond_f
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1027
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto/16 :goto_0

    .line 1037
    :pswitch_2
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_PREV/NEXT = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v2, :cond_11

    .line 1040
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1043
    :cond_11
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_12

    .line 1044
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_PREV/NEXT = ShowPresentation"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1045
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1048
    :cond_12
    const/16 v2, 0x8

    if-ne p1, v2, :cond_13

    .line 1049
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    move-result v1

    .line 1053
    .local v1, "result":Z
    :goto_2
    if-eqz v1, :cond_14

    .line 1054
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v5, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1051
    .end local v1    # "result":Z
    :cond_13
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->prev()Z

    move-result v1

    .restart local v1    # "result":Z
    goto :goto_2

    .line 1056
    :cond_14
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1060
    .end local v1    # "result":Z
    :pswitch_3
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_NEXT_BY_KEYPAD"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1062
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v2, :cond_15

    .line 1063
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1066
    :cond_15
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v9, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1068
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    .line 1069
    .local v0, "cpos":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v2

    add-int/lit16 v2, v2, -0x3a98

    if-lt v0, v2, :cond_16

    .line 1070
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    goto/16 :goto_0

    .line 1072
    :cond_16
    add-int/lit16 v2, v0, 0x3a98

    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_0

    .line 1077
    .end local v0    # "cpos":I
    :pswitch_4
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_PREV_BY_KEYPAD"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v2, :cond_17

    .line 1080
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1083
    :cond_17
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v9, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1085
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    .line 1086
    .restart local v0    # "cpos":I
    const/16 v2, 0xbb8

    if-gt v0, v2, :cond_18

    .line 1087
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->prev()Z

    goto/16 :goto_0

    .line 1088
    :cond_18
    const/16 v2, 0x3a98

    if-gt v0, v2, :cond_19

    .line 1089
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_0

    .line 1091
    :cond_19
    add-int/lit16 v2, v0, -0x3a98

    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_0

    .line 1096
    .end local v0    # "cpos":I
    :pswitch_5
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_FFSHORTSEEK"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1098
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShortSeekDisalbedCase()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1099
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v2, :cond_1a

    .line 1100
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1103
    :cond_1a
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_1b

    .line 1104
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1115
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->next()Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 1116
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v5, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1118
    :cond_1c
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1124
    :pswitch_6
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_REWSHORTSEEK"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1126
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShortSeekDisalbedCase()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1127
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-lez v2, :cond_1d

    .line 1128
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1131
    :cond_1d
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v0

    .line 1132
    .restart local v0    # "cpos":I
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest CMD_REWSHORTSEEK. cpos = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 1135
    const/16 v2, 0xbb8

    if-le v0, v2, :cond_1e

    .line 1136
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v2, :cond_1

    .line 1137
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->seek(J)V

    goto/16 :goto_0

    .line 1139
    :cond_1e
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->prev()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1140
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v5, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1142
    :cond_1f
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1145
    :cond_20
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_21

    .line 1146
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1162
    :cond_21
    const/16 v2, 0xbb8

    if-le v0, v2, :cond_22

    .line 1163
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_0

    .line 1164
    :cond_22
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->prev()Z

    move-result v2

    if-eqz v2, :cond_23

    .line 1165
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v5, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1167
    :cond_23
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    invoke-interface {v2, v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1174
    .end local v0    # "cpos":I
    :pswitch_7
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    goto/16 :goto_0

    .line 1178
    :pswitch_8
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_FFLONGSEEK state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1180
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1184
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v2, :cond_24

    .line 1190
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 1191
    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1192
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1193
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1194
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 1196
    :cond_24
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v7, :cond_25

    .line 1197
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1198
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1200
    :cond_25
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v5, :cond_1

    .line 1201
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1203
    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1204
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1210
    :pswitch_9
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_FFLONGSEEK_FOR_EXT_SCR state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1216
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v2, :cond_26

    .line 1222
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 1223
    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1224
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1225
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 1227
    :cond_26
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v7, :cond_27

    .line 1228
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    goto/16 :goto_0

    .line 1230
    :cond_27
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v5, :cond_1

    .line 1231
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1232
    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    goto/16 :goto_0

    .line 1238
    :pswitch_a
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_REWLONGSEEK state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1240
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1244
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v2, :cond_28

    .line 1251
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 1252
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1253
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1254
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1255
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 1257
    :cond_28
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v5, :cond_29

    .line 1258
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1259
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1261
    :cond_29
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v7, :cond_1

    .line 1262
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1264
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1265
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const v3, 0x4a5bba00    # 3600000.0f

    invoke-interface {v2, v9, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto/16 :goto_0

    .line 1271
    :pswitch_b
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_REWLONGSEEK_FOR_EXT_SCR state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1273
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1277
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-nez v2, :cond_2a

    .line 1284
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSeekPos:I

    .line 1285
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1286
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1287
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sendMessage(IJ)V

    goto/16 :goto_0

    .line 1289
    :cond_2a
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v5, :cond_2b

    .line 1290
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    goto/16 :goto_0

    .line 1292
    :cond_2b
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-ne v2, v7, :cond_1

    .line 1293
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    .line 1294
    iput v5, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    goto/16 :goto_0

    .line 1300
    :pswitch_c
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_RESETSEEK state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1302
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v2, :cond_1

    .line 1305
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removeMessage(I)V

    .line 1308
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 1310
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_2c

    .line 1311
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 1313
    :cond_2c
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 1314
    :cond_2d
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2e

    .line 1315
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    long-to-int v2, v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 1332
    :cond_2e
    :goto_3
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    .line 1335
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v3, 0x3

    const/high16 v4, -0x40800000    # -1.0f

    invoke-interface {v2, v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1336
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1337
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    .line 1338
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHoldLongSeekSpeed:Z

    goto/16 :goto_0

    .line 1313
    :cond_2f
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v2

    if-nez v2, :cond_2d

    .line 1317
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v2

    if-nez v2, :cond_30

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v2

    if-eqz v2, :cond_32

    .line 1318
    :cond_30
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_31

    .line 1319
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_3

    .line 1321
    :cond_31
    invoke-virtual {p0, v7, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_3

    .line 1324
    :cond_32
    const-string v2, "VideoServiceUtil"

    const-string v3, "Real seek called!!!!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLastPos:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v2, v2

    invoke-virtual {p0, v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto :goto_3

    .line 1342
    :pswitch_d
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "controlRequest() - CMD_RESET state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_33

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v2

    if-nez v2, :cond_33

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_33

    .line 1345
    const-string v2, "VideoServiceUtil"

    const-string v3, "controlRequest() - CMD_RESET = ShowPresentation"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1346
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1349
    :cond_33
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removeMessage(I)V

    .line 1351
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v2, :cond_34

    .line 1352
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1354
    :cond_34
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 1355
    iput v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongKeyCnt:I

    goto/16 :goto_0

    .line 950
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x7 -> :sswitch_0
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch

    .line 965
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_a
        :pswitch_2
        :pswitch_2
        :pswitch_c
        :pswitch_d
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method public getBTMediaBtnStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2759
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBTMediaBtnStatus:Ljava/lang/String;

    return-object v0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 1870
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1871
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getBufferPercentage()I

    move-result v0

    .line 1873
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurPos()J
    .locals 2

    .prologue
    .line 533
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->CurPosition:J

    return-wide v0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2556
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2557
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2559
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 1792
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1793
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v0

    long-to-int v0, v0

    .line 1795
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentResumePosition()J
    .locals 2

    .prologue
    .line 2706
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    return-wide v0
.end method

.method public getDur()J
    .locals 2

    .prologue
    .line 537
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->Duration:J

    return-wide v0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 1784
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1785
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->duration()J

    move-result-wide v0

    long-to-int v0, v0

    .line 1787
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFPS()I
    .locals 1

    .prologue
    .line 2003
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2004
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getFPS()I

    move-result v0

    .line 2006
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFullDumpSubtitle(Ljava/lang/String;Z)[B
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1946
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getFullDumpSubtitle(Ljava/lang/String;Z)[B

    move-result-object v0

    .line 1949
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getKDrmDisableCapture()Z
    .locals 1

    .prologue
    .line 2472
    const/4 v0, 0x0

    return v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 1471
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoListType:I

    return v0
.end method

.method public getMediahubDownloadingPercent()J
    .locals 10

    .prologue
    .line 2211
    const-wide/16 v4, 0x0

    .line 2212
    .local v4, "percent":J
    const-wide/16 v2, 0x0

    .line 2215
    .local v2, "file_length":J
    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getMediaHubPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2216
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 2217
    const-wide/16 v6, 0x64

    mul-long/2addr v6, v2

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getMediaHubFileTotalLength()J

    move-result-wide v8

    div-long v4, v6, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2223
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    const-string v6, "VideoServiceUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMediahubDownloadingPercent. percent : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", file_length : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2224
    return-wide v4

    .line 2218
    :catch_0
    move-exception v0

    .line 2219
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "VideoServiceUtil"

    const-string v7, "setProgress: FileNotFoundException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2220
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getNextFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2253
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFileName(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerState()I
    .locals 1

    .prologue
    .line 2458
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2459
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getPlayerState()I

    move-result v0

    .line 2461
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayerState:I

    goto :goto_0
.end method

.method public getPrevFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2257
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFileName(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperPathToOpen()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1475
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 1476
    .local v1, "path":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1477
    :cond_0
    const-string v3, "getProperPathToOpen"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getProperPathToOpen : mCurPlayingPath = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    .end local v1    # "path":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v1

    .line 1481
    .restart local v1    # "path":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    .line 1483
    .local v0, "currentUri":Landroid/net/Uri;
    if-eqz v0, :cond_1

    .line 1484
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1485
    .local v2, "uri":Ljava/lang/String;
    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v1, v2

    .line 1486
    goto :goto_0
.end method

.method public getResumePosition()J
    .locals 2

    .prologue
    .line 2514
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    return-wide v0
.end method

.method public getSelectedAudioTrack()I
    .locals 1

    .prologue
    .line 2395
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSelectedAudioTrack:I

    return v0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 1994
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1995
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getVideoHeight()I

    move-result v0

    .line 1997
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 1986
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1987
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getVideoWidth()I

    move-result v0

    .line 1989
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public handleZoomOnPause()V
    .locals 4

    .prologue
    .line 2535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->seek(J)J

    .line 2538
    :cond_0
    return-void
.end method

.method public hideNotification(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 2604
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2605
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->hideNotification(Z)V

    .line 2607
    :cond_0
    return-void
.end method

.method public initSelectSubtitleTrack()V
    .locals 2

    .prologue
    .line 1954
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1955
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->initSelectSubtitleTrack()V

    .line 1959
    :goto_0
    return-void

    .line 1957
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "initSelectSubtitleTrack() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1931
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->initSubtitle(Ljava/lang/String;Z)V

    .line 1935
    :goto_0
    return-void

    .line 1933
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "initSubtitle() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public is1080PEquivalent()Z
    .locals 3

    .prologue
    .line 2580
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 2581
    .local v0, "videoRes":I
    const v1, 0x11cc40

    if-ge v0, v1, :cond_1

    const v1, 0x7e900

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x3c

    if-ge v1, v2, :cond_1

    :cond_0
    const v1, 0x38400

    if-lt v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x78

    if-lt v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public is1088pEquivalent()Z
    .locals 4

    .prologue
    const/16 v3, 0x780

    const/16 v2, 0x440

    .line 2572
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v0

    .line 2573
    .local v0, "videoHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v1

    .line 2575
    .local v1, "videoWidth":I
    if-lt v1, v0, :cond_0

    if-gt v1, v3, :cond_1

    if-gt v0, v2, :cond_1

    :cond_0
    if-ge v1, v0, :cond_2

    if-gt v1, v2, :cond_1

    if-le v0, v3, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public is720PEquivalent()Z
    .locals 3

    .prologue
    .line 2587
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 2588
    .local v0, "videoRes":I
    const v1, 0x7e900

    if-ge v0, v1, :cond_0

    const v1, 0x38400

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFPS()I

    move-result v1

    const/16 v2, 0x3c

    if-lt v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isHEVCPDType()Z
    .locals 2

    .prologue
    .line 2688
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isHEVCextension()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2691
    const-string v0, "VideoServiceUtil"

    const-string v1, "HEVC PD Type!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2692
    const/4 v0, 0x1

    .line 2694
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHEVCStreamingType()Z
    .locals 2

    .prologue
    .line 2698
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699
    const-string v0, "VideoServiceUtil"

    const-string v1, "HEVC Streaming Type!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2700
    const/4 v0, 0x1

    .line 2702
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHttpBrowser()Z
    .locals 2

    .prologue
    .line 2236
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v0

    .line 2238
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 2239
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isBrowser(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2240
    const/4 v1, 0x1

    .line 2243
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1862
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1863
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isInitialized()Z

    move-result v0

    .line 1865
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isKDrmPlayingNow()Z
    .locals 1

    .prologue
    .line 2479
    const/4 v0, 0x0

    return v0
.end method

.method public isLongSeekMode()Z
    .locals 1

    .prologue
    .line 2487
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    if-eqz v0, :cond_0

    .line 2488
    const/4 v0, 0x1

    .line 2490
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPauseBy(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1707
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPauseBy. ctx = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    const/4 v0, 0x0

    .line 1709
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1710
    const/4 v0, 0x1

    .line 1713
    :cond_0
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isPauseBy. retVal = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1714
    return v0
.end method

.method public isPauseSetEmpty()Z
    .locals 1

    .prologue
    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 1764
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    .line 1766
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isPausedByTransientLossOfFocus()Z
    .locals 1

    .prologue
    .line 2564
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2565
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPausedByTransientLossOfFocus()Z

    move-result v0

    .line 2567
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPausedByUser()Z
    .locals 1

    .prologue
    .line 2502
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedByUser:Z

    return v0
.end method

.method public isPlayPathValid()Z
    .locals 5

    .prologue
    .line 223
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    .line 224
    .local v2, "mCurPlayingUri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 227
    .local v1, "mCurPlayingPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v3

    if-nez v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v3

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 231
    :cond_2
    if-eqz v1, :cond_3

    .line 232
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 234
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_3

    .line 235
    const-string v3, "VideoServiceUtil"

    const-string v4, "isPlayPathValid - file path to play is not valid."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    const/4 v3, 0x0

    .line 241
    .end local v0    # "file":Ljava/io/File;
    :goto_0
    return v3

    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1847
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1848
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    .line 1850
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlayingBeforePalm()Z
    .locals 1

    .prologue
    .line 2494
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    return v0
.end method

.method public isQcifOrLowerResClip()Z
    .locals 3

    .prologue
    .line 2593
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v2

    mul-int v0, v1, v2

    .line 2594
    .local v0, "videoRes":I
    const/16 v1, 0x7900

    if-gt v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 1

    .prologue
    .line 2011
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2012
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isSecVideo()Z

    move-result v0

    .line 2014
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowingPresentation()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2636
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-eqz v2, :cond_0

    .line 2638
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    invoke-interface {v2}, Lcom/sec/android/app/videoplayer/service/IPresentationService;->isShowing_Presentation()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2644
    :cond_0
    :goto_0
    return v1

    .line 2639
    :catch_0
    move-exception v0

    .line 2640
    .local v0, "ex":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public next()Z
    .locals 1

    .prologue
    .line 566
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public notifyPlayer(I)V
    .locals 2
    .param p1, "notication"    # I

    .prologue
    .line 2247
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    if-eqz v0, :cond_0

    .line 2248
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 2250
    :cond_0
    return-void
.end method

.method public oneFrameBackward()V
    .locals 1

    .prologue
    .line 1841
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1842
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->oneFrameBackward()V

    .line 1844
    :cond_0
    return-void
.end method

.method public oneFrameForward()V
    .locals 1

    .prologue
    .line 1835
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1836
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->oneFrameForward()V

    .line 1838
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 1641
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1643
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-nez v0, :cond_0

    .line 1649
    :goto_0
    return-void

    .line 1645
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pause()V

    .line 1648
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_0
.end method

.method public pauseBy(Ljava/lang/Object;)V
    .locals 3
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1681
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseBy. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1682
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1684
    const-string v0, "VideoServiceUtil"

    const-string v1, "pauseBy do nothing in RTSP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1704
    :goto_0
    return-void

    .line 1689
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "rewSeek"

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1691
    :cond_1
    const-string v0, "VideoServiceUtil"

    const-string v1, "pauseBy do nothing in video extension mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1695
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1696
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1697
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 1698
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    .line 1703
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1700
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    goto :goto_1
.end method

.method public pauseOrStopPlaying()V
    .locals 5

    .prologue
    .line 1667
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    .line 1668
    .local v0, "isPlaying":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v1

    .line 1669
    .local v1, "pauseEnable":Z
    const-string v2, "VideoServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pauseOrStopPlaying. isPlaying = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pauseEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1671
    if-eqz v0, :cond_0

    .line 1672
    if-eqz v1, :cond_1

    .line 1673
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 1678
    :cond_0
    :goto_0
    return-void

    .line 1675
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    goto :goto_0
.end method

.method public play(Z)V
    .locals 3
    .param p1, "start"    # Z

    .prologue
    .line 1629
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "play() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1631
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1632
    if-eqz p1, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startPlay()Z

    .line 1638
    :cond_0
    :goto_0
    return-void

    .line 1635
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    goto :goto_0
.end method

.method public prev()Z
    .locals 1

    .prologue
    .line 570
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public removeHandlerMessage()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 220
    return-void
.end method

.method public removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1394
    return-void
.end method

.method public removePauseSet(Ljava/lang/Object;)V
    .locals 3
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    .line 1746
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removePauseSet. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1748
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1749
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 1751
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 1771
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-nez v0, :cond_0

    .line 1781
    :goto_0
    return-void

    .line 1774
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1779
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->reset()V

    .line 1780
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    goto :goto_0

    .line 1777
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    goto :goto_1
.end method

.method public resetBTMediaBtnStatus()V
    .locals 1

    .prologue
    .line 2763
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBTMediaBtnStatus:Ljava/lang/String;

    .line 2764
    return-void
.end method

.method public resetCurrentResumePosition()V
    .locals 2

    .prologue
    .line 2710
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    .line 2711
    return-void
.end method

.method public resetPauseSet()V
    .locals 2

    .prologue
    .line 1754
    const-string v0, "VideoServiceUtil"

    const-string v1, "resetPauseSet."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1756
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    .line 1757
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    .line 1758
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 1760
    :cond_0
    return-void
.end method

.method public resetPausedByUserFlag()V
    .locals 1

    .prologue
    .line 2510
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedByUser:Z

    .line 2511
    return-void
.end method

.method public resetSubtitle()V
    .locals 2

    .prologue
    .line 1962
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1963
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetSubtitle()V

    .line 1967
    :goto_0
    return-void

    .line 1965
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "resetSubtitle() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resetTrackInfo()V
    .locals 2

    .prologue
    .line 2547
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2548
    const-string v0, "VideoServiceUtil"

    const-string v1, "resetTrackInfo"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2549
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    .line 2553
    :goto_0
    return-void

    .line 2551
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "resetTrackInfo() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resumeBy(Ljava/lang/Object;)V
    .locals 4
    .param p1, "ctx"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 1718
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resumeBy. ctx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1720
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1721
    const-string v0, "VideoServiceUtil"

    const-string v1, "resumeBy do nothing in RTSP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1743
    :cond_0
    :goto_0
    return-void

    .line 1725
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1726
    const-string v0, "VideoServiceUtil"

    const-string v1, "resumeBy do nothing in video extension mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1727
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSubtitleTrackChangedOnPresentationMode:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1728
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSubtitleTrackChangedOnPresentationMode:Z

    .line 1729
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startSubtitle()V

    goto :goto_0

    .line 1734
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPauseSet:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    if-eqz v0, :cond_3

    .line 1737
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 1738
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1740
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedOnPlaying:Z

    goto :goto_0
.end method

.method public saveCurrentResumePosition()V
    .locals 2

    .prologue
    .line 2714
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    .line 2715
    return-void
.end method

.method public saveRecentlyPlayedTime()V
    .locals 3

    .prologue
    .line 541
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    .line 542
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 543
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->saveRecentlyPlayedTime(Landroid/net/Uri;)V

    .line 556
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 545
    const/4 v0, 0x0

    .line 547
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 548
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 550
    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHubContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 551
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->saveRecentlyPlayedTime(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public saveResumePosition(ZZ)V
    .locals 12
    .param p1, "pauseActivity"    # Z
    .param p2, "rewind"    # Z

    .prologue
    .line 403
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveResumePosition E. mPlayerState = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const/4 v7, 0x0

    .line 406
    .local v7, "isDrm":Z
    const/4 v6, 0x0

    .line 409
    .local v6, "isDivxRentalDrm":Z
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 410
    const-string v0, "VideoServiceUtil"

    const-string v1, "saveResumePosition() is DRM file"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    const/4 v7, 0x1

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getRightStatus()I

    move-result v0

    if-nez v0, :cond_0

    .line 414
    sget v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    const/16 v1, 0x27

    if-ne v0, v1, :cond_0

    .line 415
    const/4 v6, 0x1

    .line 419
    :cond_0
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "updateResumePos() - isDrm = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ", isDivxRentalDrm "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_1
    const-wide/16 v2, 0x0

    .line 423
    .local v2, "curPos":J
    const-wide/16 v4, 0x0

    .line 425
    .local v4, "duration":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 426
    const-string v0, "VideoServiceUtil"

    const-string v1, "saveResumePosition doesn\'t skip during play DMS contents via DMR"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_3

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v2

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->duration()J

    move-result-wide v4

    .line 438
    :cond_3
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumeAndDuration(JJ)V

    .line 439
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveResumePosition. curPos = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ", duration = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    const-wide/16 v0, 0x2710

    cmp-long v0, v4, v0

    if-lez v0, :cond_13

    .line 442
    if-eqz p2, :cond_11

    .line 443
    const-wide/16 v0, 0x1388

    cmp-long v0, v2, v0

    if-gez v0, :cond_f

    .line 444
    const-wide/16 v2, 0x0

    .line 466
    :cond_4
    :goto_0
    if-eqz v7, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 467
    const-string v0, "VideoServiceUtil"

    const-string v1, "saveResumePosition() - drm is true. set curPosition to 0."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    const-wide/16 v2, 0x0

    .line 471
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 472
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 475
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_6

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 479
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v0

    if-nez v0, :cond_7

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 481
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setContentDuration(Landroid/net/Uri;J)I

    .line 487
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 488
    const/4 v8, 0x0

    .line 490
    .local v8, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 491
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    .line 493
    if-eqz v8, :cond_9

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, v8, v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->setResumePosition(Landroid/net/Uri;J)I

    .line 499
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v0

    if-nez v0, :cond_a

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_a

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendVHLastPlayedItemBroadcast(Z)V

    .line 506
    .end local v8    # "uri":Landroid/net/Uri;
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSideSync()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 507
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isNeedSaveResumePos()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 508
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveResumePosition. curPos = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_b

    .line 510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendSideSyncLastPlayedItemBroadcast(J)V

    .line 515
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 516
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->saveResumePosition(Landroid/content/Context;JJ)V

    .line 519
    :cond_c
    if-eqz p1, :cond_d

    .line 520
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 523
    :cond_d
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveResumePosition X. mPausedByUser :  "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedByUser:Z

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ", mResumePosition : "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v10, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    :goto_1
    return-void

    .line 428
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 429
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "saveResumePosition skip saving position due to PlayerStatus = "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v9

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 445
    :cond_f
    sub-long v0, v4, v2

    const-wide/16 v10, 0x1388

    cmp-long v0, v0, v10

    if-gtz v0, :cond_10

    .line 446
    const-wide/16 v2, 0x0

    goto/16 :goto_0

    .line 448
    :cond_10
    const-wide/16 v0, 0x1388

    sub-long/2addr v2, v0

    goto/16 :goto_0

    .line 451
    :cond_11
    const-wide/16 v0, 0x3e8

    cmp-long v0, v2, v0

    if-gez v0, :cond_12

    .line 452
    const-wide/16 v2, 0x0

    goto/16 :goto_0

    .line 453
    :cond_12
    sub-long v0, v4, v2

    const-wide/16 v10, 0x3e8

    cmp-long v0, v0, v10

    if-gtz v0, :cond_4

    .line 454
    const-wide/16 v2, 0x0

    goto/16 :goto_0

    .line 461
    :cond_13
    sub-long v0, v4, v2

    const-wide/16 v10, 0x1f4

    cmp-long v0, v0, v10

    if-gez v0, :cond_4

    .line 462
    const-wide/16 v2, 0x0

    goto/16 :goto_0
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "msec"    # I

    .prologue
    .line 1800
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    .line 1801
    return-void
.end method

.method public seekTo(II)V
    .locals 4
    .param p1, "msec"    # I
    .param p2, "seekMode"    # I

    .prologue
    const/4 v2, 0x2

    .line 1804
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-nez v0, :cond_0

    .line 1832
    :goto_0
    return-void

    .line 1806
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1807
    const-string v0, "VideoServiceUtil"

    const-string v1, "seekTo() - visual Seek - live streaming. don\'t seek."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1811
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1812
    :cond_2
    const-string v0, "VideoServiceUtil"

    const-string v1, "seekTo() - visual Seek - streaming/download tab. call seek()."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1813
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->seek(J)J

    .line 1816
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    goto :goto_0

    .line 1811
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1820
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_5

    .line 1821
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v0

    if-ne v0, v2, :cond_4

    .line 1822
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 1824
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v0

    if-ne v0, v2, :cond_5

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v0

    if-nez v0, :cond_5

    .line 1825
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 1828
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->realSeek(II)J

    .line 1831
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    goto :goto_0
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1388
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1389
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1390
    return-void
.end method

.method public sendSeekBroadcast(Ljava/lang/String;)V
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 758
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendSeekBroadcast() - command : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.videoplayer.info"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 760
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "command"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 761
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 762
    return-void
.end method

.method public setAdaptSound(Z)V
    .locals 2
    .param p1, "mode"    # Z

    .prologue
    .line 1886
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1887
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setAdaptSound(ZZ)V

    .line 1891
    :goto_0
    return-void

    .line 1889
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setAdaptSound() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAudioTrack(I)V
    .locals 3
    .param p1, "trackNumber"    # I

    .prologue
    .line 2374
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSelectedAudioTrack:I

    .line 2376
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2377
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trackNumber: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2378
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setAudioTrack(I)V

    .line 2381
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isDefaultSpeed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2382
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->isQuarterSpeedMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2383
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    if-nez v0, :cond_2

    .line 2384
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setQuarterPlaySpeed()V

    .line 2392
    :cond_1
    :goto_0
    return-void

    .line 2386
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    goto :goto_0

    .line 2389
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    goto :goto_0
.end method

.method public setBTMediaBtnStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 2755
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBTMediaBtnStatus:Ljava/lang/String;

    .line 2756
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 211
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 212
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 213
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 214
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayListUtil:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    .line 215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayListUtil:Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setInfo(Landroid/content/Context;)V

    .line 216
    return-void
.end method

.method public setCurrentResumePosition()V
    .locals 2

    .prologue
    .line 2718
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 2719
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePositionForVideoExtensionState:J

    .line 2720
    return-void
.end method

.method public setDisableCaptureState(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 2751
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mDisableCaptureState:Z

    .line 2752
    return-void
.end method

.method public setHasAudioFocus()V
    .locals 1

    .prologue
    .line 1856
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1857
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setHasAudioFocus()V

    .line 1859
    :cond_0
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 2

    .prologue
    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setInbandSubtitle()V

    .line 1943
    :goto_0
    return-void

    .line 1941
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setInbandSubtitle() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setListType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 1467
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoListType:I

    .line 1468
    return-void
.end method

.method public setLongSeekMode(I)V
    .locals 0
    .param p1, "LongSeekMode"    # I

    .prologue
    .line 2483
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mLongSeekMode:I

    .line 2484
    return-void
.end method

.method public setMovieVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 1894
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1895
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setVolume(F)V

    .line 1897
    :cond_0
    return-void
.end method

.method public setOnNotificationListener(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    .prologue
    .line 2139
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    .line 2140
    return-void
.end method

.method public setOnSvcNotificationListener(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

    .prologue
    .line 2147
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

    .line 2148
    return-void
.end method

.method public setPausedByUser()V
    .locals 1

    .prologue
    .line 2506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPausedByUser:Z

    .line 2507
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 3
    .param p1, "speed"    # I

    .prologue
    const v2, 0x3dcccccd    # 0.1f

    .line 1900
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1901
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setPlaySpeed() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910
    :cond_1
    :goto_0
    return-void

    .line 1905
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    add-int/lit8 v1, p1, 0x5

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setPlaySpeed(F)V

    .line 1907
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFPS()I

    move-result v0

    const/16 v1, 0x18

    if-le v0, v1, :cond_1

    .line 1908
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    add-int/lit8 v0, p1, 0x5

    int-to-float v0, v0

    mul-float/2addr v0, v2

    const v2, 0x3f8ccccd    # 1.1f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->dropLCDfps(Z)Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setPlayerState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 2465
    iput p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayerState:I

    .line 2466
    return-void
.end method

.method public setPlayingBeforePalm(Z)V
    .locals 0
    .param p1, "playingBeforePalm"    # Z

    .prologue
    .line 2498
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingBeforePalm:Z

    .line 2499
    return-void
.end method

.method public setPresentatnionService(Lcom/sec/android/app/videoplayer/service/IPresentationService;)V
    .locals 1
    .param p1, "service"    # Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .prologue
    .line 2069
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    if-nez v0, :cond_0

    .line 2070
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 2072
    :cond_0
    return-void
.end method

.method public setQuarterPlaySpeed()V
    .locals 2

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1914
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setQuarterPlaySpeed() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1919
    :goto_0
    return-void

    .line 1918
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/high16 v1, 0x3e800000    # 0.25f

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setPlaySpeed(F)V

    goto :goto_0
.end method

.method public setResumeAndDuration(JJ)V
    .locals 1
    .param p1, "curPos"    # J
    .param p3, "duration2"    # J

    .prologue
    .line 528
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->CurPosition:J

    .line 529
    iput-wide p3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->Duration:J

    .line 530
    return-void
.end method

.method public setResumePosition(J)V
    .locals 1
    .param p1, "mResumePosition"    # J

    .prologue
    .line 2518
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 2519
    return-void
.end method

.method public setServiceContext()V
    .locals 1

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    sput-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    .line 2020
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sput-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 2021
    sput-object p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 2022
    return-void
.end method

.method public setSoundAliveMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1879
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setSoundAliveMode(I)V

    .line 1883
    :goto_0
    return-void

    .line 1881
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setSoundAliveMode() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSubtitleSyncTime(I)V
    .locals 2
    .param p1, "synctime"    # I

    .prologue
    .line 1970
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1971
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setSubtitleSyncTime(I)V

    .line 1975
    :goto_0
    return-void

    .line 1973
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "setSubtitleSyncTime() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSubtitleTrackChangedOnPresentationMode(Z)V
    .locals 0
    .param p1, "mSubtitleTrackChangedOnPresentationMode"    # Z

    .prologue
    .line 2723
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSubtitleTrackChangedOnPresentationMode:Z

    .line 2724
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2541
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2542
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setVideoCrop(IIII)V

    .line 2544
    :cond_0
    return-void
.end method

.method public showNotification()V
    .locals 1

    .prologue
    .line 2598
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2599
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->showNotification()V

    .line 2601
    :cond_0
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 1592
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-nez v0, :cond_1

    .line 1626
    :cond_0
    :goto_0
    return-void

    .line 1596
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1598
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1599
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 1600
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start E. playing mPLAYERSTATUS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayerState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1605
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 1610
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 1612
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    goto :goto_0

    .line 1614
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startPlay()Z

    goto :goto_0

    .line 1617
    :cond_4
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playerstate ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1618
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startPlay()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1620
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayingChecker()V

    goto/16 :goto_0

    .line 1623
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    goto/16 :goto_0
.end method

.method public startNextPrevPlayback(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V
    .locals 5
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 346
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setContentChanged(Z)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setPlayerState(I)V

    .line 355
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayDmr(Z)V

    .line 400
    :cond_1
    :goto_0
    return-void

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_3

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setKeepAudioFocus(Z)V

    .line 390
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 393
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mMoveSameFile:Z

    if-nez v0, :cond_4

    .line 394
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setTitleShowed(Z)V

    .line 397
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_1

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setKeepAudioFocus(Z)V

    goto :goto_0
.end method

.method public startPlayDmr(Z)V
    .locals 8
    .param p1, "directDmcMode"    # Z

    .prologue
    const-wide/16 v6, 0x64

    const/4 v4, 0x6

    const/4 v3, 0x0

    .line 331
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startPlayDmr(). directDmcMode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v1, 0xb

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 335
    if-eqz p1, :cond_1

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isContentChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_0
.end method

.method public startPlayback()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 245
    const-string v3, "VideoServiceUtil"

    const-string v4, "startPlayback() start"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 248
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsTransportType()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v3

    if-nez v3, :cond_1

    .line 249
    const-string v3, "SLink"

    const-string v4, "startPlayback() Scs Data Not exist We need to get it..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->waitingForScsDataUpdate(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 251
    const-string v3, "SLink"

    const-string v4, "startPlayback() Scs Data Still Not exist We need to wait..."

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    const-string v3, "SLink"

    const-string v4, "startPlayback() Scs Data exist now; we don\'t need to wait"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 270
    .local v1, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v3, :cond_3

    .line 271
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    .line 280
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSdp()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 281
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 283
    const-string v3, "VideoServiceUtil"

    const-string v4, "HTTP SDP Start"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    new-instance v0, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSDPDownloader:Ljava/lang/Runnable;

    invoke-direct {v0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 286
    .local v0, "downloadThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 288
    .end local v0    # "downloadThread":Ljava/lang/Thread;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 290
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    if-nez v3, :cond_0

    .line 305
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

    const/16 v4, 0x8f

    const-string v5, "com.sec.videoplayer.SECURE_MODE_UPDATE"

    invoke-interface {v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;->onSvcNotification(ILjava/lang/String;)V

    .line 307
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayingChecker()V

    .line 308
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 310
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_360:Z

    if-eqz v3, :cond_7

    .line 311
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->get360Video(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 312
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-nez v3, :cond_7

    .line 313
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSurfaceview()V

    .line 322
    :cond_7
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 323
    .local v2, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 324
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 316
    .end local v2    # "msg":Landroid/os/Message;
    :cond_8
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v3, :cond_7

    .line 317
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSurfaceview()V

    goto :goto_1
.end method

.method public startPlayingChecker()V
    .locals 6

    .prologue
    .line 1397
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1398
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1418
    :goto_0
    return-void

    .line 1402
    :cond_0
    const-string v1, "VideoServiceUtil"

    const-string v2, "startPlayingChecker() start!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1404
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1405
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 1406
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v0, :cond_1

    .line 1407
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    .line 1409
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1410
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v2, 0xd

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1416
    .end local v0    # "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1417
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public startSubtitle()V
    .locals 2

    .prologue
    .line 1978
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startSubtitle()V

    .line 1983
    :goto_0
    return-void

    .line 1981
    :cond_0
    const-string v0, "VideoServiceUtil"

    const-string v1, "startSubtitle() - service is not ready."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 1652
    const-string v0, "VideoServiceUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() sService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-nez v0, :cond_1

    .line 1664
    :cond_0
    :goto_0
    return-void

    .line 1656
    :cond_1
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    .line 1659
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mResumePosition:J

    .line 1660
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1661
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1662
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    goto :goto_0
.end method

.method public stopBufferingChecker()V
    .locals 2

    .prologue
    .line 2202
    const-string v0, "VideoServiceUtil"

    const-string v1, "stopBufferingChecker() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2205
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBufferingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 2207
    :cond_0
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 2208
    return-void
.end method

.method public stopPlayingChecker()V
    .locals 3

    .prologue
    .line 1421
    const-string v0, "VideoServiceUtil"

    const-string v1, "stopPlayingChecker() start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;->onNotification(IF)V

    .line 1425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingChecker:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1426
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1427
    const v0, 0xc350

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mPlayingCheckTime:I

    .line 1428
    return-void
.end method

.method public top()Z
    .locals 1

    .prologue
    .line 578
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->TOP:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->shiftContent(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Z

    move-result v0

    return v0
.end method

.method public unbindFromPresentationService()V
    .locals 5

    .prologue
    .line 2098
    const-string v1, "VideoServiceUtil"

    const-string v2, "unbindFromPresentationService E."

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationConnectionMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;

    .line 2102
    .local v0, "sb":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;
    if-nez v0, :cond_0

    .line 2110
    :goto_0
    return-void

    .line 2106
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 2107
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 2108
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    const-class v4, Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 2109
    const-string v1, "VideoServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unbindFromPresentationService sConnectionMap.isEmpty = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationConnectionMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 2060
    const-string v0, "VideoServiceUtil"

    const-string v1, "unbindService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2061
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mIsBound:Z

    if-eqz v0, :cond_0

    .line 2062
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 2063
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mIsBound:Z

    .line 2064
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .line 2066
    :cond_0
    return-void
.end method

.method public updateNotification(Z)V
    .locals 1
    .param p1, "contentChanged"    # Z

    .prologue
    .line 2610
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    if-eqz v0, :cond_0

    .line 2611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mBoundService:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->updateNotification(Z)V

    .line 2613
    :cond_0
    return-void
.end method

.method public writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2400
    const/4 v0, 0x0

    .line 2401
    .local v0, "c":I
    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2402
    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    .line 2403
    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 2404
    return-void
.end method
