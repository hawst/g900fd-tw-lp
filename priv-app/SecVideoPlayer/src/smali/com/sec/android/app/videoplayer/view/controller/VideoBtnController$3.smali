.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->initControllerView(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 487
    const-string v0, "VideoBtnController"

    const-string v1, "mmLScreenArrowBtnRightTouchListener call"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_VASTA:Z

    if-eqz v0, :cond_2

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/4 v1, 0x0

    iput v1, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    .line 494
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    const-string v1, "l_screen_controller_pos"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setAndUpdateTts()V

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 501
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 505
    :cond_2
    return-void

    .line 491
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    if-nez v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/4 v1, 0x2

    iput v1, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mLScreenCtrlPos:I

    goto :goto_0
.end method
