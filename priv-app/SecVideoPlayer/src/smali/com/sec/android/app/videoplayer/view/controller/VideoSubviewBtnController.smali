.class public Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.super Landroid/widget/RelativeLayout;
.source "VideoSubviewBtnController.java"


# static fields
.field private static final CENTER_M_PROGRESS_VIEW:I = 0x0

.field private static final DELAY_TREED_VIDE:I = 0x1f

.field private static final FFLONGSEEK:I = 0x8

.field protected static final FFW_RWD_NOT_SUPPORT:I = 0x1e

.field private static final LEFT_M_PROGRESS_VIEW:I = 0x1

.field private static final LONG_PRESS_TIME:J = 0x1f4L

.field private static final PROGRESS_POPUP_FLOATING_POINT:I = 0x19

.field private static final PROGRESS_RESOLUTION:J = 0x186a0L

.field private static final REWLONGSEEK:I = 0x9

.field private static final RIGHT_M_PROGRESS_VIEW:I = 0x2

.field private static final SHOW_PROGRESS:I = 0x1

.field private static final START_CHANGE_PLAYER:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VideoSubViewBtnController"

.field private static final UNINITIALIZED_MEDIAPLAYERSERVICE:I = -0x1


# instance fields
.field private HOVER_IMAGE_GAP:I

.field private PROGRESS_MARGIN:I

.field private mAllshareListener:Landroid/view/View$OnTouchListener;

.field private mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mArrowHeight:I

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

.field private mBottomPadding:I

.field private mButtonEnable:Z

.field private mChangePlayerBtn:Landroid/widget/ImageButton;

.field private mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

.field private mConnectionCheckPopup:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

.field private mCtrlLayoutShow:Z

.field private mCtrlLayoutState:Z

.field public mCurrentTime:Landroid/widget/TextView;

.field protected mDetailSeekViewStub:Landroid/view/View;

.field protected mDetailSeekViewStubLandscape:Landroid/view/View;

.field protected mDetailSeekViewStubPortrait:Landroid/view/View;

.field protected mDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mDownKeyPressTime:J

.field private mDuration:J

.field public mEndTime:Landroid/widget/TextView;

.field private mFfButton:Landroid/widget/ImageButton;

.field private mFfKeyListener:Landroid/view/View$OnKeyListener;

.field private mFfTouchListener:Landroid/view/View$OnTouchListener;

.field private mFitToSrcBtn:Landroid/widget/ImageButton;

.field private mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

.field private final mHandler:Landroid/os/Handler;

.field private mHandlerProgressPreview:Landroid/os/Handler;

.field private mHoldLongSeekSpeed:Z

.field private mHoverDuration:J

.field private mHoverEventStart:Z

.field private mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

.field private mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

.field private mIsAlreadyPauseState:Z

.field private mIsSearch:Z

.field private mLeftPadding:I

.field protected mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mMoreBtn:Landroid/widget/ImageButton;

.field private mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mPauseKeyListener:Landroid/view/View$OnKeyListener;

.field private mPauseTouchListener:Landroid/view/View$OnTouchListener;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field mPopupMenu:Landroid/widget/PopupMenu;

.field private mProgressBar:Landroid/widget/SeekBar;

.field private mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressContent:Landroid/view/View;

.field private mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

.field private mProgressPreviewImage:Landroid/widget/ImageView;

.field private mProgressPreviewShowTime:Landroid/widget/TextView;

.field private mProgressThumbViewState:I

.field private mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

.field private mQuarterDetailedSeekText:Landroid/widget/TextView;

.field private mRewButton:Landroid/widget/ImageButton;

.field private mRewKeyListener:Landroid/view/View$OnKeyListener;

.field private mRewTouchListener:Landroid/view/View$OnTouchListener;

.field private mRightPadding:I

.field protected mRoot:Landroid/view/View;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

.field private mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

.field private mScrubbingSpeed:I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mTimetextHeight:I

.field private mTimetextWidth:I

.field private mTopPadding:I

.field private mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

.field private mVideoBtnSeekBarPosionX:F

.field private mVideoBtnSeekBarPosionY:F

.field private mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

.field private mVideoSeekBarProgressX:F

.field private mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

.field private mViewModeRes:[I

.field private mbProgressDragStatus:Z

.field private mbResume:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 247
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 102
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    .line 108
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->PROGRESS_MARGIN:I

    .line 110
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->HOVER_IMAGE_GAP:I

    .line 112
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    .line 114
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I

    .line 116
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I

    .line 118
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRightPadding:I

    .line 120
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTopPadding:I

    .line 122
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBottomPadding:I

    .line 124
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextHeight:I

    .line 126
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextWidth:I

    .line 128
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mArrowHeight:I

    .line 130
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDuration:J

    .line 132
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J

    .line 134
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J

    .line 138
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I

    .line 140
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 142
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    .line 144
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 146
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 148
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 150
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 152
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    .line 154
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    .line 156
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 164
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 166
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 168
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 170
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    .line 172
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 174
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    .line 176
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsSearch:Z

    .line 178
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    .line 180
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    .line 184
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mButtonEnable:Z

    .line 188
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    .line 190
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsAlreadyPauseState:Z

    .line 192
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionX:F

    .line 194
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionY:F

    .line 196
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F

    .line 198
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 202
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    .line 204
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 208
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 210
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    .line 212
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 214
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 216
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 218
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    .line 226
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .line 228
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;

    .line 230
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    .line 232
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    .line 236
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z

    .line 239
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewModeRes:[I

    .line 671
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    .line 799
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$7;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 843
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$8;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1159
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    .line 1431
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$12;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAllshareListener:Landroid/view/View$OnTouchListener;

    .line 1482
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1725
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$14;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    .line 1762
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$15;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    .line 1808
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$16;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    .line 1903
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$17;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    .line 2000
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$18;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    .line 2094
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$19;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    .line 2272
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    .line 2496
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$21;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 2519
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 2759
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$23;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 2853
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$24;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 2927
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$25;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    .line 3051
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$26;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    .line 3094
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$27;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    .line 248
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    .line 249
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 250
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    .line 251
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 252
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 253
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 254
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 256
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initFloatingWindow()V

    .line 257
    return-void

    .line 239
    :array_0
    .array-data 4
        0x7f020104
        0x7f020103
        0x7f020106
        0x7f020105
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsAlreadyPauseState:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->blockSpeedSeek()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDownKeyPressTime:J

    return-wide p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J

    return-wide v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J

    return-wide p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRightPadding:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTopPadding:I

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBottomPadding:I

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mArrowHeight:I

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/HoverPopupWindow;)Landroid/widget/HoverPopupWindow;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$3500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setContentsDescription4TWProgressBar()V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I

    return p1
.end method

.method static synthetic access$4102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionX:F

    return p1
.end method

.method static synthetic access$4202(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionY:F

    return p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initDetailSeekViewStub()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->changeLayoutForDetailSeekView(F)V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F

    return v0
.end method

.method static synthetic access$4502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # F

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoSeekBarProgressX:F

    return p1
.end method

.method static synthetic access$4600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextWidth:I

    return v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateProgressbarPreviewView(IZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowingSubPopupMenu()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    return v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->refreshFitToScrBtn()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    return p1
.end method

.method private blockSpeedSeek()Z
    .locals 1

    .prologue
    .line 3015
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLinkStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromDms()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private changeLayoutForDetailSeekView(F)V
    .locals 8
    .param p1, "getY"    # F

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    const v7, 0x7f080058

    const/high16 v6, 0x43340000    # 180.0f

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2704
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-nez v2, :cond_1

    .line 2757
    :cond_0
    :goto_0
    return-void

    .line 2708
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v1, v2, Landroid/util/DisplayMetrics;->density:F

    .line 2710
    .local v1, "scale":F
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2711
    cmpg-float v2, p1, v5

    if-gez v2, :cond_2

    .line 2713
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2714
    .local v0, "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 2715
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2716
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2717
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2718
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2719
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setRotation(F)V

    .line 2720
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setRotation(F)V

    .line 2721
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setRotation(F)V

    goto :goto_0

    .line 2722
    .end local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    const/high16 v2, 0x42200000    # 40.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 2724
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2725
    .restart local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 2726
    const/16 v2, 0xa

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2727
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2728
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2729
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2730
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setRotation(F)V

    .line 2731
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 2732
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setRotation(F)V

    goto/16 :goto_0

    .line 2735
    .end local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    cmpg-float v2, p1, v5

    if-gez v2, :cond_4

    .line 2737
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2738
    .restart local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 2739
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2740
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2741
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2742
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setRotation(F)V

    .line 2743
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 2744
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setRotation(F)V

    goto/16 :goto_0

    .line 2745
    .end local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    const/high16 v2, 0x42200000    # 40.0f

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 2747
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2748
    .restart local v0    # "LP_DetailedSeekLayout":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 2749
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2750
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v4, v4, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2751
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2752
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setRotation(F)V

    .line 2753
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setRotation(F)V

    .line 2754
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setRotation(F)V

    goto/16 :goto_0
.end method

.method private checkSupportChangePlayer()Z
    .locals 4

    .prologue
    .line 1396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isWfdSupport()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->checkDmcDisabled()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 3000
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFitToScnMode()I
    .locals 2

    .prologue
    .line 3191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v0

    .line 3193
    .local v0, "retVal":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->isWebVTTFileType()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    if-nez v0, :cond_1

    .line 3195
    const/4 v0, 0x1

    .line 3198
    :cond_1
    return v0
.end method

.method private initCtrlButton(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x5153

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 511
    const v1, 0x7f0d01cc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 513
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 514
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 515
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 516
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 517
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updatePausePlayBtn()V

    .line 520
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080140

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 522
    .local v0, "hoverCustomPopupOffset":I
    const v1, 0x7f0d01cb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    .line 524
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 525
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 526
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 527
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0170

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 532
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 534
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_1

    .line 536
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 537
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v6}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 538
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 575
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 578
    :cond_2
    const v1, 0x7f0d01cd

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    .line 580
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_4

    .line 581
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 582
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 583
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 584
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a015c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 587
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverListenerFF_REW_BTN:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 588
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 589
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 590
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_3

    .line 591
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 592
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v6}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 593
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 629
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 632
    :cond_4
    const v1, 0x7f0d0136

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v1, :cond_5

    .line 635
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewModeRes:[I

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToScnMode()I

    move-result v3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 636
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScnCtrlTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 638
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScnCtrlKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 641
    :cond_5
    return-void
.end method

.method private initDetailSeekViewStub()V
    .locals 7

    .prologue
    const v2, 0x7f0d0113

    const v1, 0x7f0d0111

    const v6, 0x7f0a011c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2665
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-nez v0, :cond_1

    .line 2701
    :cond_0
    :goto_0
    return-void

    .line 2669
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubLandscape:Landroid/view/View;

    if-nez v0, :cond_3

    .line 2670
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_2

    .line 2671
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2672
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v1, 0x7f0d0112

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubLandscape:Landroid/view/View;

    .line 2676
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubPortrait:Landroid/view/View;

    if-nez v0, :cond_5

    .line 2677
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_4

    .line 2678
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2679
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v1, 0x7f0d0114

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubPortrait:Landroid/view/View;

    .line 2682
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2683
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubLandscape:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    .line 2688
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2689
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2691
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 2693
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d014c

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 2694
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d014d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    .line 2695
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHalfDetailedSeekLayoutText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1/2"

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2697
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0147

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekLayout:Landroid/widget/LinearLayout;

    .line 2698
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailedSeekLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0148

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    .line 2699
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mQuarterDetailedSeekText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "1/4"

    aput-object v3, v2, v4

    invoke-virtual {v1, v6, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2685
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStubPortrait:Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDetailSeekViewStub:Landroid/view/View;

    goto :goto_1
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 295
    const-string v1, "VideoSubViewBtnController"

    const-string v2, "VideoSubviewBtnController - initFloatingWindow"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 298
    .local v0, "mDecor":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->setSubContentView(Landroid/app/Activity;Landroid/view/View;)V

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 303
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 329
    :cond_0
    return-void
.end method

.method private initProgress(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 466
    const v0, 0x7f0d01c8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 471
    new-instance v0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 478
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 480
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 481
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 483
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 504
    :cond_0
    :goto_0
    const v0, 0x7f0d01c5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    .line 505
    const v0, 0x7f0d01c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    .line 506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 507
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 508
    return-void

    .line 489
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressOnSeekHoverListener:Landroid/widget/SeekBar$OnSeekBarHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    .line 493
    :cond_2
    new-instance v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->setDaemon(Z)V

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->start()V

    .line 497
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    goto :goto_0
.end method

.method private isProgressZoomPossible()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 430
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "isProgressZoomPossible"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 462
    :cond_0
    :goto_0
    return v0

    .line 441
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->is1088pEquivalent()Z

    move-result v2

    if-nez v2, :cond_0

    .line 445
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 446
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 447
    goto :goto_0

    .line 453
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAudioOnlyClip()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 454
    const-string v1, "VideoSubViewBtnController"

    const-string v2, "progress preview disabled for audio only files"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 462
    goto :goto_0
.end method

.method private isShowingSubPopupMenu()Z
    .locals 1

    .prologue
    .line 796
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->isShowingSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowingPopupMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private refreshFitToScrBtn()V
    .locals 3

    .prologue
    .line 3160
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 3161
    :cond_0
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "refreshFitToScrBtn() return"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3188
    :goto_0
    return-void

    .line 3165
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewModeRes:[I

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToScnMode()I

    move-result v2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 3168
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToScnMode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 3170
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a015d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3174
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3178
    :pswitch_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a016a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3182
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0169

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 3168
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private setContentsDescription4TWProgressBar()V
    .locals 6

    .prologue
    .line 2512
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2513
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0175

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2514
    const-string v1, ". "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2515
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0174

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2516
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2517
    return-void
.end method

.method private setEnableProgressbar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 2218
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 2219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 2221
    :cond_0
    return-void
.end method

.method private stringForTime(IZ)Ljava/lang/String;
    .locals 4
    .param p1, "timeMs"    # I
    .param p2, "durationTime"    # Z

    .prologue
    .line 1532
    if-eqz p2, :cond_3

    .line 1533
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSavedDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1534
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    .line 1543
    :goto_0
    return-object v0

    .line 1537
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1538
    :cond_1
    const-string v0, "--:--:--"

    goto :goto_0

    .line 1540
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1543
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateFitToSrcBtn()V
    .locals 2

    .prologue
    .line 3149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 3150
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3151
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3155
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    .line 3157
    :cond_1
    return-void

    .line 3153
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateProgressbarPreviewView(IZ)I
    .locals 6
    .param p1, "progress"    # I
    .param p2, "isPortrait"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2241
    const-string v2, "VideoSubViewBtnController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateProgressbarPreviewView progress : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " orientation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2242
    const/4 v1, 0x0

    .line 2243
    .local v1, "x":I
    const/4 p2, 0x1

    .line 2245
    const/4 v0, 0x0

    .line 2246
    .local v0, "progressBarWidth":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v2, :cond_0

    .line 2247
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getWidth()I

    move-result v0

    .line 2250
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;

    if-eqz v2, :cond_1

    .line 2251
    if-eqz p2, :cond_4

    .line 2252
    int-to-float v2, p1

    const v3, 0x47c35000    # 100000.0f

    div-float/2addr v2, v3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 2254
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->PROGRESS_MARGIN:I

    sub-int/2addr v2, v3

    if-ge v1, v2, :cond_2

    .line 2255
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->HOVER_IMAGE_GAP:I

    sub-int v1, v2, v3

    .line 2256
    const/4 v2, 0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    .line 2269
    :cond_1
    :goto_0
    return v1

    .line 2257
    :cond_2
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->PROGRESS_MARGIN:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_3

    .line 2258
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I

    div-int/lit8 v2, v2, 0x2

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRightPadding:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->HOVER_IMAGE_GAP:I

    sub-int/2addr v2, v3

    neg-int v1, v2

    .line 2259
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 2261
    :cond_3
    const/4 v1, 0x0

    .line 2262
    iput v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    goto :goto_0

    .line 2265
    :cond_4
    iput v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressThumbViewState:I

    goto :goto_0
.end method


# virtual methods
.method public changeFitToSrcBtn(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 3142
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 3143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 3144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->invalidate()V

    .line 3146
    :cond_0
    return-void
.end method

.method public dismissChangePlayerPopup()V
    .locals 1

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->dismiss()V

    .line 1393
    :cond_0
    return-void
.end method

.method public dismissPopupMenu()V
    .locals 2

    .prologue
    .line 772
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_0

    .line 773
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    .line 774
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 780
    :cond_0
    :goto_0
    return-void

    .line 776
    :catch_0
    move-exception v0

    .line 778
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public forceHide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1042
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    if-nez v0, :cond_1

    .line 1043
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibility(I)V

    .line 1044
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 1045
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 1046
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setInvisibleAllViews()V

    .line 1048
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    .line 1049
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    .line 1051
    :cond_1
    return-void
.end method

.method public getFitToSrcBtn()Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 3048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method public hide()V
    .locals 5

    .prologue
    const v4, 0x7f0d01be

    const/4 v3, 0x0

    .line 990
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    if-nez v2, :cond_0

    .line 991
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    .line 992
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "hide : findViewById() is null view not inflate yet!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    :cond_0
    :goto_0
    return-void

    .line 996
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1001
    const/4 v1, 0x0

    .line 1003
    .local v1, "translateOff":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1006
    .local v0, "rotation":I
    if-nez v0, :cond_2

    .line 1007
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v2, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1016
    .restart local v1    # "translateOff":Landroid/view/animation/Animation;
    :goto_1
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1017
    invoke-virtual {v1}, Landroid/view/animation/Animation;->startNow()V

    .line 1018
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1019
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibility(I)V

    .line 1020
    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$10;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1037
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    goto :goto_0

    .line 1008
    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 1009
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v3, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOff":Landroid/view/animation/Animation;
    goto :goto_1

    .line 1010
    :cond_3
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 1011
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v2, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOff":Landroid/view/animation/Animation;
    goto :goto_1

    .line 1013
    :cond_4
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v3, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOff":Landroid/view/animation/Animation;
    goto :goto_1
.end method

.method public hide(Z)V
    .locals 2
    .param p1, "hide"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1054
    if-eqz p1, :cond_0

    .line 1055
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibility(I)V

    .line 1056
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    .line 1057
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    .line 1059
    :cond_0
    return-void
.end method

.method protected initControllerView(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 389
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initProgress(Landroid/view/View;)V

    .line 390
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initCtrlButton(Landroid/view/View;)V

    .line 392
    const v0, 0x7f0d01ca

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mViewCtrlBtnBar:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 397
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 405
    :cond_1
    const v0, 0x7f0d01c9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 408
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 409
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 411
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 415
    :cond_2
    const v0, 0x7f0d01c1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a014f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAllshareListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 421
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 423
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 427
    :cond_3
    return-void
.end method

.method public isOnLongSeekMode()Z
    .locals 1

    .prologue
    .line 1759
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method public isProgressDraging()Z
    .locals 1

    .prologue
    .line 1062
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 890
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method public isShowingPopupMenu()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 784
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    .line 785
    const/4 v1, 0x1

    .line 791
    :cond_0
    :goto_0
    return v1

    .line 788
    :catch_0
    move-exception v0

    .line 790
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 380
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 381
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030039

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initControllerView(Landroid/view/View;)V

    .line 383
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->showAllshareBtnControl(Z)V

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1130
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "VideoSubviewBtnController - onDetachedFromWindow"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1132
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1133
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1134
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    .line 1135
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 1136
    return-void
.end method

.method public onDmrListChanged()V
    .locals 1

    .prologue
    .line 1411
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 1412
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->updateChangePlayerList()V

    .line 1414
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 260
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->initControllerView(Landroid/view/View;)V

    .line 263
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v2, 0xbb8

    const/4 v0, 0x1

    .line 280
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->isShowingSubMenu()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 281
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->dismissSubMenus()V

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    .line 291
    :cond_0
    :goto_0
    return v0

    .line 285
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowingPopupMenu()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 286
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->dismissPopupMenu()V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    goto :goto_0

    .line 291
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 973
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z

    .line 975
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v1, :cond_1

    .line 986
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 987
    return-void

    .line 978
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 979
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 980
    .local v0, "msg":Landroid/os/Message;
    iput v3, v0, Landroid/os/Message;->what:I

    .line 981
    sget-object v1, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    iget v2, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eq v1, v3, :cond_0

    .line 982
    sget-object v1, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 955
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 960
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_1

    .line 970
    :cond_0
    :goto_0
    return-void

    .line 963
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 964
    new-instance v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    .line 965
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->setDaemon(Z)V

    .line 966
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBitmapThread:Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->start()V

    goto :goto_0
.end method

.method public playerStop()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2224
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2225
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2229
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2230
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2234
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 2235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2236
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 2238
    :cond_2
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2987
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 2988
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->releaseView()V

    .line 2990
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2991
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandlerProgressPreview:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2992
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 2993
    return-void
.end method

.method public removeHandler()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    const-wide/16 v0, 0x0

    .line 266
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "removeHandler"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsSearch:Z

    if-eqz v2, :cond_1

    .line 269
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeMessage(I)V

    .line 270
    const/16 v2, 0x9

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeMessage(I)V

    move-wide v2, v0

    move v6, v5

    .line 271
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    if-eqz v2, :cond_0

    .line 273
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    .line 274
    iget v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionX:F

    iget v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoBtnSeekBarPosionY:F

    move-wide v2, v0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public removeMessage(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getSpeedTextView()Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1157
    :cond_0
    :goto_0
    return-void

    .line 1151
    :cond_1
    const/16 v0, 0x9

    if-eq p1, v0, :cond_2

    if-ne p1, v2, :cond_0

    .line 1152
    :cond_2
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsSearch:Z

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getSpeedTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setPressed(Z)V

    goto :goto_0
.end method

.method public resetHoldLongSeek()V
    .locals 2

    .prologue
    .line 1752
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    if-eqz v0, :cond_0

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1754
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoldLongSeekSpeed:Z

    .line 1756
    :cond_0
    return-void
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 1139
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1141
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 1142
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1143
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeAllViews()V

    .line 344
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 347
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 348
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 351
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 353
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 356
    :cond_0
    const v3, 0x7f0d01be

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 377
    return-void
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 2923
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 2924
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 2925
    return-void
.end method

.method public setDisableRewFfBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    const v2, 0x7f0d01cd

    const v1, 0x7f0d01cb

    .line 644
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 646
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 647
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 650
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 655
    :cond_0
    return-void
.end method

.method public setEnableRewFfBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x7f0d01cd

    const v2, 0x7f0d01cb

    .line 658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 659
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setClickable(Z)V

    .line 667
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 669
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 2215
    :goto_0
    return-void

    .line 2195
    :cond_0
    const-string v0, "VideoSubViewBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2200
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 2201
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2203
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 2204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFfButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2206
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 2207
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRewButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 2209
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    .line 2210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 2213
    :cond_5
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mButtonEnable:Z

    .line 2214
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 2996
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 2997
    return-void
.end method

.method public setInvisibleAllViews()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1095
    :goto_0
    return-void

    .line 1069
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1072
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1073
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1074
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1077
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1078
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1079
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1080
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1081
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1082
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1083
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1084
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1088
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1094
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    goto/16 :goto_0

    .line 1091
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibleAllViews()V

    goto :goto_1
.end method

.method public setProgress()I
    .locals 18

    .prologue
    .line 1552
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z

    if-eqz v14, :cond_2

    .line 1553
    :cond_0
    const/4 v10, 0x0

    .line 1645
    :cond_1
    :goto_0
    return v10

    .line 1556
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v10

    .line 1557
    .local v10, "position":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v4

    .line 1559
    .local v4, "duration":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v14, :cond_f

    .line 1560
    const/16 v14, 0x3e8

    if-le v4, v14, :cond_e

    .line 1561
    const-wide/32 v14, 0x186a0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    int-to-long v0, v4

    move-wide/from16 v16, v0

    div-long v8, v14, v16

    .line 1562
    .local v8, "pos":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v14

    if-nez v14, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1563
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1568
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getBufferPercentage()I

    move-result v7

    .line 1569
    .local v7, "percent":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v14

    if-nez v14, :cond_4

    .line 1570
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    mul-int/lit16 v15, v7, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 1574
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1575
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 1576
    const-string v14, "VideoSubViewBtnController"

    const-string v15, "setProgress: MediaHub file download incomplete"

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    const-wide/16 v12, 0x0

    .line 1580
    .local v12, "progress_prctg":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getMediahubDownloadingPercent()J

    move-result-wide v12

    .line 1582
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_5

    .line 1583
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setIncompletedMediaHub(Z)V

    .line 1586
    :cond_5
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_c

    .line 1587
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 1592
    :goto_2
    const-string v14, "VideoSubViewBtnController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MediaHub file downloading percent: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1606
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    .end local v12    # "progress_prctg":J
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1607
    if-gtz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1608
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1612
    :cond_7
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v5

    .line 1613
    .local v5, "endnewTime":Ljava/lang/String;
    const/4 v6, 0x0

    .line 1614
    .local v6, "endnowTime":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1616
    .local v3, "currentnowTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    .line 1617
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1618
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 1619
    if-lez v4, :cond_8

    .line 1620
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1621
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1626
    :cond_8
    move v11, v10

    .line 1627
    .local v11, "time":I
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v2

    .line 1629
    .local v2, "currentnewTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v14, :cond_9

    .line 1630
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1632
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 1633
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1634
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1638
    :cond_9
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v14, :cond_1

    .line 1639
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    if-nez v14, :cond_a

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_a

    .line 1640
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 1642
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    if-nez v14, :cond_1

    .line 1643
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    goto/16 :goto_0

    .line 1565
    .end local v2    # "currentnewTime":Ljava/lang/String;
    .end local v3    # "currentnowTime":Ljava/lang/String;
    .end local v5    # "endnewTime":Ljava/lang/String;
    .end local v6    # "endnowTime":Ljava/lang/String;
    .end local v11    # "time":I
    .restart local v8    # "pos":J
    :cond_b
    const-string v14, "VideoSubViewBtnController"

    const-string v15, "setProgress: mServiceUtil.isPauseEnable() is true."

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1589
    .restart local v7    # "percent":I
    .restart local v12    # "progress_prctg":J
    :cond_c
    long-to-float v14, v12

    const v15, 0x3f7ae148    # 0.98f

    mul-float/2addr v14, v15

    float-to-long v12, v14

    .line 1590
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_2

    .line 1594
    .end local v12    # "progress_prctg":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_3

    .line 1599
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    :cond_e
    const-string v14, "VideoSubViewBtnController"

    const-string v15, "setProgress: duration is less than zero"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1600
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_3

    .line 1603
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1549
    return-void
.end method

.method public setProgressMax()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 1649
    const-string v5, "VideoSubViewBtnController"

    const-string v6, "setProgressMax()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;

    const v6, 0x186a0

    invoke-virtual {v5, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1654
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v2

    .line 1656
    .local v2, "duration":I
    invoke-direct {p0, v2, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    .line 1657
    .local v3, "endnewTime":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1658
    .local v4, "endnowTime":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1660
    .local v1, "currentnowTime":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    .line 1661
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1662
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1663
    if-lez v2, :cond_0

    .line 1664
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1665
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1670
    :cond_0
    const/4 v5, 0x0

    invoke-direct {p0, v2, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v0

    .line 1672
    .local v0, "currentnewTime":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 1673
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1675
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 1676
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1677
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1681
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1682
    return-void
.end method

.method public setUpdate()V
    .locals 2

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updatePausePlayBtn()V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedBtn()V

    .line 336
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayingBeforePalm()Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 339
    :cond_1
    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 895
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    const/4 p1, 0x0

    .line 898
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 899
    return-void
.end method

.method public setVisibleAllViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1098
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1127
    :goto_0
    return-void

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {v0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailSubWindow;->getSubWindow(Landroid/app/Activity;)Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1104
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01ca

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01cd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1114
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1116
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0136

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1120
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1121
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setDisableRewFfBtn()V

    .line 1126
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateBtnControllerBtns()V

    goto/16 :goto_0

    .line 1123
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableRewFfBtn()V

    goto :goto_1
.end method

.method public setVolumeBtn()V
    .locals 0

    .prologue
    .line 3201
    return-void
.end method

.method public show()V
    .locals 6

    .prologue
    const v5, 0x7f0d01be

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 902
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z

    if-eqz v2, :cond_1

    .line 903
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_0

    .line 904
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "show : findViewById() is null view not inflate yet!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    :goto_0
    return-void

    .line 907
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibleAllViews()V

    .line 908
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->refreshFitToScrBtn()V

    .line 910
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setProgress()I

    .line 911
    const/4 v1, 0x0

    .line 913
    .local v1, "translateOn":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 916
    .local v0, "rotation":I
    if-nez v0, :cond_2

    .line 917
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 926
    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    :goto_1
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 927
    invoke-virtual {v1}, Landroid/view/animation/Animation;->startNow()V

    .line 928
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v2

    if-nez v2, :cond_5

    .line 929
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 933
    :goto_2
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibility(I)V

    .line 934
    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$9;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 948
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z

    .line 950
    .end local v0    # "rotation":I
    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 951
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 918
    .restart local v0    # "rotation":I
    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    :cond_2
    if-ne v0, v4, :cond_3

    .line 919
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    goto :goto_1

    .line 920
    :cond_3
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 921
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-direct {v1, v2, v3, v3, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    goto :goto_1

    .line 923
    :cond_4
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v2, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    goto :goto_1

    .line 931
    :cond_5
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z

    goto :goto_2
.end method

.method public showAllshareBtn(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1354
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 1355
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "showAllshareBtn : mChangePlayerBtn is NOT init"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1387
    :goto_0
    return-void

    .line 1359
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1360
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1365
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v0

    if-gtz v0, :cond_4

    .line 1366
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1368
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "showAllshareBtn : Browser is NOT init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1380
    :cond_4
    if-eqz p1, :cond_5

    .line 1381
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1382
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1384
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public showAllshareBtnControl(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 1333
    const-string v0, "VideoSubViewBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showAllshareBtnControl visible:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mFitToSrcBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1336
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateFitToSrcBtn()V

    .line 1339
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v0, :cond_1

    .line 1340
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableProgressbar(Z)V

    .line 1343
    :cond_1
    if-eqz p1, :cond_2

    .line 1344
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibleAllViews()V

    .line 1346
    :cond_2
    return-void
.end method

.method public showPopupMenu()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 706
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v2

    if-nez v2, :cond_0

    .line 712
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "showPopupMenu() E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 716
    .local v0, "activity":Landroid/app/Activity;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_3

    .line 717
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_2

    .line 718
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->dismiss()V

    .line 721
    :cond_2
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 724
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 727
    .local v1, "rotation":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-nez v2, :cond_5

    .line 728
    if-eqz v1, :cond_4

    const/4 v2, 0x3

    if-eq v1, v2, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 729
    :cond_4
    new-instance v2, Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v5, 0x7f0d0106

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 736
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_5

    .line 737
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$5;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 745
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$6;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 759
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v2, :cond_0

    .line 760
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/Menu;->clear()V

    .line 761
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v6, v2}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 762
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v0, v6, v7, v2}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    .line 764
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 765
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v2}, Landroid/widget/PopupMenu;->show()V

    goto/16 :goto_0

    .line 730
    :cond_6
    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    .line 731
    new-instance v2, Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v5, 0x7f0d0107

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    goto :goto_1

    .line 733
    :cond_7
    new-instance v2, Landroid/widget/PopupMenu;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v5, 0x7f0d0108

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPopupMenu:Landroid/widget/PopupMenu;

    goto :goto_1
.end method

.method public startChangePlayer()V
    .locals 3

    .prologue
    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1429
    return-void
.end method

.method public updateAllShareBtn()V
    .locals 1

    .prologue
    .line 1349
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->checkSupportChangePlayer()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->showAllshareBtn(Z)V

    .line 1350
    return-void

    .line 1349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateBtnControllerBtns()V
    .locals 1

    .prologue
    .line 3005
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-nez v0, :cond_1

    .line 3012
    :cond_0
    :goto_0
    return-void

    .line 3007
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedBtn()V

    .line 3009
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setPaddingForSpeedText()V

    .line 3010
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updateRotationBtn()V

    goto :goto_0
.end method

.method public updateChangePlayerButton()V
    .locals 2

    .prologue
    .line 1417
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1424
    :goto_0
    return-void

    .line 1419
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const v1, 0x7f0200fb

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0

    .line 1422
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const v1, 0x7f0200fa

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public updatePausePlayBtn()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const v5, 0x7f0a016c

    const v4, 0x7f0a016b

    const v3, 0x7f0200fe

    const v2, 0x7f0200fd

    .line 1686
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1687
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 1688
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1689
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableRewFfBtn()V

    .line 1690
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1691
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1692
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1693
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1694
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableProgressbar(Z)V

    .line 1723
    :cond_0
    :goto_0
    return-void

    .line 1696
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1697
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1698
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableProgressbar(Z)V

    goto :goto_0

    .line 1701
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1702
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1703
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1708
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableProgressbar(Z)V

    goto :goto_0

    .line 1705
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1706
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 1711
    :cond_4
    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnableProgressbar(Z)V

    .line 1712
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setDisableRewFfBtn()V

    .line 1713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 1717
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1718
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_0
.end method
