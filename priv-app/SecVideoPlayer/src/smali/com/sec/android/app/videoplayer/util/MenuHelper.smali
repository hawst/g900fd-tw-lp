.class public Lcom/sec/android/app/videoplayer/util/MenuHelper;
.super Ljava/lang/Object;
.source "MenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;
    }
.end annotation


# static fields
.field private static sMenuHelper:Lcom/sec/android/app/videoplayer/util/MenuHelper;


# instance fields
.field private mConfig:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

.field private mContext:Landroid/content/Context;

.field private mSubMenu:Landroid/view/SubMenu;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mConfig:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    .line 44
    return-void
.end method

.method private canSendtoAnotherSLinkDevice()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 465
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v8

    .line 468
    .local v8, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469
    const-class v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "canSendtoAnotherSLinkDevice. return true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :goto_0
    return v7

    .line 471
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->isSlinkSignedIn()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 472
    :cond_1
    const-class v1, Lcom/sec/android/app/videoplayer/util/MenuHelper;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "canSendtoAnotherSLinkDevice. return false"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v0

    .line 473
    goto :goto_0

    .line 476
    :cond_2
    const-string v3, "transport_type == ? AND network_mode != ?"

    .line 477
    .local v3, "where":Ljava/lang/String;
    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->name()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    .line 481
    .local v4, "whereArgs":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 482
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 483
    .local v7, "isOnlineDevice":Z
    if-eqz v6, :cond_4

    .line 484
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 485
    const/4 v7, 0x1

    .line 487
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 488
    const/4 v6, 0x0

    .line 491
    :cond_4
    const-class v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "canSendtoAnotherSLinkDevice. isOnlineDevice : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->sMenuHelper:Lcom/sec/android/app/videoplayer/util/MenuHelper;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->sMenuHelper:Lcom/sec/android/app/videoplayer/util/MenuHelper;

    .line 56
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->sMenuHelper:Lcom/sec/android/app/videoplayer/util/MenuHelper;

    return-object v0
.end method

.method private isDownloadableContents(Landroid/net/Uri;)Z
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 496
    const/4 v2, 0x1

    .line 497
    .local v2, "isDownloadable":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 499
    .local v0, "Mimetype":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const-string v4, "application/x-dtcp1"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v3, v4, :cond_0

    .line 500
    const/4 v3, 0x0

    const-string v4, "application/x-dtcp1"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "checkMimetype":Ljava/lang/String;
    const-string v3, "application/x-dtcp1"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 503
    const/4 v2, 0x0

    .line 506
    .end local v1    # "checkMimetype":Ljava/lang/String;
    :cond_0
    const-string v3, "MenuHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDownloadableContents return  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    return v2
.end method

.method private isSlinkSignedIn()Z
    .locals 2

    .prologue
    .line 458
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 460
    :goto_0
    return v1

    .line 459
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private makeMenus(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 109
    if-nez p1, :cond_1

    .line 131
    :cond_0
    return-void

    .line 111
    :cond_1
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    .line 113
    .local v2, "size":I
    const/4 v1, 0x0

    .line 115
    .local v1, "item":Landroid/view/MenuItem;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 116
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 118
    if-eqz v1, :cond_3

    .line 119
    const/4 v3, 0x0

    .line 121
    .local v3, "visible":Z
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mConfig:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    sget-object v5, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    if-ne v4, v5, :cond_2

    .line 122
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v4

    const v5, 0x7f0d0212

    if-ne v4, v5, :cond_4

    .line 123
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkEditMenuVisible(I)Z

    move-result v3

    .line 128
    :cond_2
    :goto_1
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 115
    .end local v3    # "visible":Z
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 125
    .restart local v3    # "visible":Z
    :cond_4
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v3

    goto :goto_1
.end method

.method private supportEditMenu()Z
    .locals 8

    .prologue
    .line 433
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    .line 434
    .local v2, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    .line 435
    .local v1, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    .line 437
    .local v0, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    const/4 v3, 0x1

    .line 439
    .local v3, "support":Z
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v4, v4, v6

    if-ltz v4, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v4

    if-nez v4, :cond_1

    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 450
    :cond_1
    const/4 v3, 0x0

    .line 453
    :cond_2
    return v3
.end method


# virtual methods
.method public checkEditMenuVisible(I)Z
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 369
    const/4 v4, 0x1

    .line 371
    .local v4, "enable":Z
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v0

    .line 372
    .local v0, "bNxpTrim":Z
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v2

    .line 373
    .local v2, "bVideoEditor":Z
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR_ON_S_APPS:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v3

    .line 374
    .local v3, "bVideoEditorOnSApps":Z
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_STUDIO:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v1

    .line 376
    .local v1, "bSStudio":Z
    packed-switch p1, :pswitch_data_0

    .line 424
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 378
    :pswitch_1
    const/4 v4, 0x0

    .line 380
    if-nez v2, :cond_1

    if-eqz v3, :cond_0

    :cond_1
    if-nez v1, :cond_2

    if-eqz v0, :cond_0

    .line 381
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->supportEditMenu()Z

    move-result v4

    goto :goto_0

    .line 386
    :pswitch_2
    const/4 v4, 0x0

    .line 388
    if-eqz v0, :cond_0

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    if-nez v1, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->supportEditMenu()Z

    move-result v4

    goto :goto_0

    .line 394
    :pswitch_3
    const/4 v4, 0x0

    .line 396
    if-nez v2, :cond_3

    if-eqz v3, :cond_0

    :cond_3
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    .line 397
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->supportEditMenu()Z

    move-result v4

    goto :goto_0

    .line 402
    :pswitch_4
    const/4 v4, 0x0

    .line 404
    if-eqz v1, :cond_0

    if-nez v3, :cond_0

    if-nez v2, :cond_0

    .line 405
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->supportEditMenu()Z

    move-result v4

    goto :goto_0

    .line 410
    :pswitch_5
    if-nez v1, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    .line 414
    :pswitch_6
    if-eqz v0, :cond_4

    if-eqz v1, :cond_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 418
    :pswitch_7
    if-nez v3, :cond_0

    if-nez v2, :cond_0

    const/4 v4, 0x0

    goto :goto_0

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0213
        :pswitch_1
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public checkMenuVisible(I)Z
    .locals 10
    .param p1, "id"    # I

    .prologue
    const-wide/16 v8, 0x1

    .line 234
    const/4 v1, 0x1

    .line 236
    .local v1, "enable":Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    .line 237
    .local v4, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    .line 238
    .local v3, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    .line 239
    .local v2, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 241
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    packed-switch p1, :pswitch_data_0

    .line 358
    :cond_0
    :goto_0
    :pswitch_0
    return v1

    .line 243
    :pswitch_1
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->isDownloadableContents(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 244
    const/4 v1, 0x1

    goto :goto_0

    .line 246
    :cond_1
    const/4 v1, 0x0

    .line 248
    goto :goto_0

    .line 251
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->canSendtoAnotherSLinkDevice()Z

    move-result v1

    .line 252
    goto :goto_0

    .line 255
    :pswitch_3
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-ltz v5, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v5

    if-nez v5, :cond_3

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkIsShare()Z

    move-result v5

    if-nez v5, :cond_4

    .line 264
    :cond_3
    const/4 v1, 0x0

    .line 267
    :cond_4
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 271
    :pswitch_4
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-ltz v5, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v5

    if-nez v5, :cond_6

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_6

    :cond_5
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 279
    :cond_6
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 280
    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getMediahubDownloadingPercent()J

    move-result-wide v6

    const-wide/16 v8, 0x64

    cmp-long v5, v6, v8

    if-gez v5, :cond_0

    .line 281
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 284
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 291
    :pswitch_5
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-ltz v5, :cond_9

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v5

    if-nez v5, :cond_9

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v5

    if-nez v5, :cond_9

    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "sec_container_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 304
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 309
    :pswitch_6
    const/4 v1, 0x0

    .line 311
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBTModeSCO()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 312
    :cond_a
    const/4 v1, 0x1

    .line 315
    :cond_b
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v5

    if-nez v5, :cond_c

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_VZW:Z

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 316
    :cond_c
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 321
    :pswitch_7
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v5

    if-nez v5, :cond_d

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v5

    if-nez v5, :cond_d

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBTModeSCO()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 324
    :cond_d
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 329
    :pswitch_8
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    cmp-long v5, v6, v8

    if-ltz v5, :cond_e

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v5, :cond_f

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v5

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 336
    :cond_e
    const/4 v1, 0x0

    .line 339
    :cond_f
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v1, 0x1

    goto/16 :goto_0

    .line 343
    :pswitch_9
    const/4 v1, 0x0

    .line 345
    sget v5, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->SHARE_VIDEO:I

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isLocalType()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-nez v5, :cond_0

    .line 348
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 241
    :pswitch_data_0
    .packed-switch 0x7f0d0210
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public dismissSubMenus()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mSubMenu:Landroid/view/SubMenu;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mSubMenu:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->close()V

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mSubMenu:Landroid/view/SubMenu;

    .line 225
    :cond_0
    return-void
.end method

.method public getMenuItemVisibility(Landroid/view/Menu;I)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "id"    # I

    .prologue
    const/4 v1, 0x0

    .line 95
    if-nez p1, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v1

    .line 97
    :cond_1
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 98
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    goto :goto_0
.end method

.method public isShowingSubMenu()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mSubMenu:Landroid/view/SubMenu;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setInfo(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 67
    return-void
.end method

.method public setMenuItemVisibility(Landroid/view/Menu;IZ)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "id"    # I
    .param p3, "visible"    # Z

    .prologue
    .line 81
    if-nez p1, :cond_1

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 84
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public setMode(Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;)V
    .locals 0
    .param p1, "config"    # Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mConfig:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    .line 71
    return-void
.end method

.method public updateMenus(Landroid/view/Menu;)V
    .locals 9
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v8, 0x7f0d021c

    const v7, 0x7f0d021b

    const v6, 0x7f0d0224

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 139
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->makeMenus(Landroid/view/Menu;)V

    .line 141
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v2, :cond_0

    .line 142
    const v2, 0x7f0d0215

    invoke-virtual {p0, p1, v2, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    const v2, 0x7f0d0216

    invoke-virtual {p0, p1, v2, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 144
    const v2, 0x7f0d0217

    invoke-virtual {p0, p1, v2, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mConfig:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    sget-object v5, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->EXCEPTIONAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    if-ne v2, v5, :cond_1

    .line 148
    invoke-virtual {p0, p1, v6, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 150
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    .line 151
    .local v0, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    .line 153
    .local v1, "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 154
    const v2, 0x7f0d0222

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 155
    invoke-virtual {p0, p1, v6, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 173
    .end local v0    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    .end local v1    # "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    :cond_1
    :goto_0
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_E:Z

    if-eqz v2, :cond_2

    .line 174
    const v2, 0x7f0d021e

    invoke-virtual {p0, p1, v2, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 177
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->disableAppInAppBtn()Z

    move-result v2

    if-nez v2, :cond_7

    .line 178
    const v2, 0x7f0d021d

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    :goto_1
    return-void

    .line 156
    .restart local v0    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    .restart local v1    # "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 157
    invoke-virtual {p0, p1, v7, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 158
    invoke-virtual {p0, p1, v8, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 159
    const v2, 0x7f0d0222

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 160
    invoke-virtual {p0, p1, v6, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 161
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 162
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v2

    invoke-virtual {p0, p1, v7, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 163
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v2

    invoke-virtual {p0, p1, v8, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 164
    const v5, 0x7f0d021e

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f0d021e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v3

    :goto_2
    invoke-virtual {p0, p1, v5, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 165
    const v2, 0x7f0d0223

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 166
    const v2, 0x7f0d0222

    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v2

    invoke-virtual {p0, p1, v6, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    :cond_5
    move v2, v4

    .line 164
    goto :goto_2

    .line 169
    :cond_6
    const v2, 0x7f0d0210

    const v5, 0x7f0d0210

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v5

    invoke-virtual {p0, p1, v2, v5}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 180
    .end local v0    # "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    .end local v1    # "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    :cond_7
    const v2, 0x7f0d021d

    invoke-virtual {p0, p1, v2, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1
.end method

.method public updateSubMenus(Landroid/view/SubMenu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/SubMenu;

    .prologue
    .line 190
    if-nez p1, :cond_1

    .line 211
    :cond_0
    return-void

    .line 192
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/MenuHelper;->mSubMenu:Landroid/view/SubMenu;

    .line 194
    invoke-interface {p1}, Landroid/view/SubMenu;->size()I

    move-result v2

    .line 196
    .local v2, "size":I
    const/4 v1, 0x0

    .line 198
    .local v1, "item":Landroid/view/MenuItem;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 199
    invoke-interface {p1, v0}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 201
    if-eqz v1, :cond_2

    .line 202
    const/4 v3, 0x0

    .line 203
    .local v3, "visible":Z
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v4

    const v5, 0x7f0d0214

    if-ne v4, v5, :cond_3

    .line 204
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkEditMenuVisible(I)Z

    move-result v3

    .line 208
    :goto_1
    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 198
    .end local v3    # "visible":Z
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    .restart local v3    # "visible":Z
    :cond_3
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v3

    goto :goto_1
.end method
