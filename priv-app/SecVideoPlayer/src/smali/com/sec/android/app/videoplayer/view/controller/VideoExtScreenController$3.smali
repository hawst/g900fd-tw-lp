.class Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;
.super Ljava/lang/Object;
.source "VideoExtScreenController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isOutside:Z

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 1

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->isOutside:Z

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x1

    const-wide/16 v10, 0x1f4

    const/16 v8, 0xb

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 189
    const-string v2, "VideoExtScreenController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mREWBtnTouchListener E."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 191
    .local v0, "pressTime":J
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 261
    :cond_0
    :goto_0
    return v7

    .line 193
    :pswitch_0
    const-string v2, "VideoExtScreenController"

    const-string v3, "mREWBtnTouchListener ACTION_DOWN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->isOutside:Z

    .line 195
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->blockSpeedSeek()Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 196
    const-string v2, "VideoExtScreenController"

    const-string v3, "mRewTouchListener. skip longseek"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x7

    invoke-virtual {v3, v4, v5, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 198
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0

    .line 200
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 205
    :pswitch_1
    const-string v2, "VideoExtScreenController"

    const-string v3, "mREWBtnTouchListener ACTION_UP"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->blockSpeedSeek()Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 207
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 208
    cmp-long v2, v0, v10

    if-gez v2, :cond_0

    .line 209
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 210
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 211
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 216
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v6, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 218
    cmp-long v2, v0, v10

    if-gez v2, :cond_4

    .line 219
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 225
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->resetHoldLongSeek()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    goto/16 :goto_0

    .line 227
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 228
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 229
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 232
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "rewSeek"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPauseBy(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 233
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 234
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "rewSeek"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 236
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z
    invoke-static {v2, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$502(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z

    .line 237
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    .line 238
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 246
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->isOutside:Z

    if-nez v2, :cond_0

    .line 247
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v6, v2

    if-gtz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v6, v2

    if-gtz v2, :cond_6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 249
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 250
    iput-boolean v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->isOutside:Z

    .line 252
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "rewSeek"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPauseBy(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const-string v3, "rewSeek"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
