.class public Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;,
        Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;,
        Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverMsg;
    }
.end annotation


# static fields
.field private static final MSG_SEEK:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoProgressPreviewPopup"

.field private static mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;


# instance fields
.field private FrameImage:Landroid/widget/ImageView;

.field private FrameImageArrow:Landroid/widget/ImageView;

.field private mArrowWidth:I

.field private mContext:Landroid/content/Context;

.field private mCurrentPosition:I

.field private mCurrentTime:Landroid/widget/TextView;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;

.field private mHoverEventStart:Z

.field private mIsInitialized:Z

.field private mMeasuredVideoHeight:I

.field private mMeasuredVideoWidth:I

.field private mPopupHeight:I

.field private mPopupWidth:I

.field private mProgressX:I

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSeekBarPadding:I

.field private mSeekHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;

.field private mSeekbarRect:Landroid/graphics/Rect;

.field private mStartPosition:I

.field private mSurfaceView:Landroid/view/SurfaceView;

.field private mUri:Landroid/net/Uri;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private relative_layout:Landroid/widget/RelativeLayout;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private videoHeight:I

.field private videoPreviewDialog:Landroid/app/Dialog;

.field private videoWidth:I


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    .line 55
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 57
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHoverEventStart:Z

    .line 87
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    .line 88
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    .line 89
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    .line 90
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    .line 92
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    .line 94
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 95
    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mStartPosition:I

    .line 103
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    .line 826
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$7;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 119
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->initViews()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mStartPosition:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHoverEventStart:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->reSetSurfaceViewImage()V

    return-void
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->playVideo()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->hideBoarders()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->show()V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->dismiss()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    return p1
.end method

.method private currentTime(I)Ljava/lang/String;
    .locals 9
    .param p1, "timeMs"    # I

    .prologue
    const/4 v8, 0x0

    .line 951
    div-int/lit16 v3, p1, 0x3e8

    .line 952
    .local v3, "totalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 953
    .local v2, "seconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 954
    .local v1, "minutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 956
    .local v0, "hours":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 957
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private dismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 679
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "dismiss() "

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->isShow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 682
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 683
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 687
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :goto_0
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    .line 694
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 696
    const/4 v1, 0x0

    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 697
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 698
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 704
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_3

    .line 705
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 707
    :cond_3
    return-void

    .line 688
    :catch_0
    move-exception v0

    .line 689
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 699
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 700
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private extractData(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "dataType"    # I

    .prologue
    const/4 v1, 0x0

    .line 710
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "extractData() type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 712
    if-nez p1, :cond_1

    .line 735
    :cond_0
    :goto_0
    return-object v1

    .line 716
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v3

    if-nez v3, :cond_0

    .line 721
    const/4 v1, 0x0

    .line 723
    .local v1, "extracted":Ljava/lang/String;
    :try_start_0
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 724
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    invoke-virtual {v2, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v2, p2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v1

    .line 726
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 727
    .end local v2    # "retriever":Landroid/media/MediaMetadataRetriever;
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 729
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 730
    .local v0, "e":Ljava/lang/RuntimeException;
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0

    .line 731
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_2
    move-exception v0

    .line 732
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getCheckRoation()I
    .locals 4

    .prologue
    .line 351
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x18

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->extractData(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 353
    .local v1, "rotation":Ljava/lang/String;
    if-eqz v1, :cond_0

    .end local v1    # "rotation":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 354
    .local v0, "m_VideoRotation":I
    sparse-switch v0, :sswitch_data_0

    .line 356
    const/4 v0, 0x0

    .line 358
    :goto_1
    return v0

    .line 353
    .end local v0    # "m_VideoRotation":I
    .restart local v1    # "rotation":Ljava/lang/String;
    :cond_0
    const-string v1, "0"

    goto :goto_0

    .line 355
    .end local v1    # "rotation":Ljava/lang/String;
    .restart local v0    # "m_VideoRotation":I
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_1

    .line 354
    :sswitch_data_0
    .sparse-switch
        0x5a -> :sswitch_0
        0x10e -> :sswitch_0
    .end sparse-switch
.end method

.method private getHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 229
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "getHandler()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$HoverPopupHandler;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    .locals 1

    .prologue
    .line 739
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    if-nez v0, :cond_0

    .line 740
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .line 743
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    return-object v0
.end method

.method private getRawY(I)I
    .locals 3
    .param p1, "mY"    # I

    .prologue
    .line 380
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isScaleWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 381
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080253

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 383
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    sub-int v0, p1, v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080252

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private getSeekHandler()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 239
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "getSeekHandler()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;

    if-nez v0, :cond_0

    .line 242
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekHandler:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$SeekHandler;

    return-object v0
.end method

.method private getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "durationTime"    # J

    .prologue
    .line 981
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThumbnail - uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", durationTime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    const/4 v2, 0x0

    .line 983
    .local v2, "thumb":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 986
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 987
    iget v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 988
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p2

    invoke-virtual {v1, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 989
    const-string v3, "VideoProgressPreviewPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getThumbnail() tumb :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 994
    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 999
    :goto_0
    return-object v2

    .line 995
    :catch_0
    move-exception v0

    .line 996
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 990
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 991
    .local v0, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 994
    :try_start_3
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 995
    :catch_2
    move-exception v0

    .line 996
    .local v0, "ex":Ljava/lang/RuntimeException;
    const-string v3, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 993
    .end local v0    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    .line 994
    :try_start_4
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 997
    :goto_1
    throw v3

    .line 995
    :catch_3
    move-exception v0

    .line 996
    .restart local v0    # "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoProgressPreviewPopup"

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private hideBoarders()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 937
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 938
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 941
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 942
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 945
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 946
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 948
    :cond_2
    return-void
.end method

.method private initViews()V
    .locals 13

    .prologue
    const v12, 0x7f08026b

    const v11, 0x7f080267

    .line 277
    const-string v8, "VideoProgressPreviewPopup"

    const-string v9, "initView"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v8, :cond_0

    .line 279
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->dismiss()V

    .line 281
    :cond_0
    new-instance v8, Landroid/app/Dialog;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    .line 283
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v8}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 284
    .local v0, "ImageViewParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080260

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080262

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 287
    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080264

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080251

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 293
    .local v1, "addtional_margin":I
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    mul-int/lit8 v9, v1, 0x2

    add-int/2addr v8, v9

    iput v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 294
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    mul-int/lit8 v9, v1, 0x4

    add-int/2addr v8, v9

    iput v8, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 297
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 300
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d01d2

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 301
    .local v5, "surface_timetext_layout":Landroid/widget/RelativeLayout;
    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 302
    .local v6, "surface_timetext_params":Landroid/widget/FrameLayout$LayoutParams;
    iget v8, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    add-int/2addr v8, v1

    iput v8, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 303
    iget v8, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    add-int/2addr v8, v1

    iput v8, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 304
    iget v8, v6, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    add-int/2addr v8, v1

    iput v8, v6, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 305
    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 308
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v9, 0x7f0d01d3

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/SurfaceView;

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    .line 309
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v8}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 310
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v8, v9}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 311
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v8}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 312
    .local v3, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 313
    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 314
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v8, v3}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 318
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogProperties()V

    .line 321
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 322
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, v9}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 323
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 324
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 325
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 329
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 343
    :cond_1
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v8}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    .line 344
    .local v7, "window":Landroid/view/Window;
    invoke-virtual {v7, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 346
    iget v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    .line 347
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080258

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    .line 348
    return-void

    .line 331
    .end local v7    # "window":Landroid/view/Window;
    :cond_2
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 332
    iget v8, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iget v9, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    sub-int/2addr v8, v9

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 336
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 337
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getRotation()I

    move-result v4

    .line 338
    .local v4, "rotation":I
    const/4 v8, 0x1

    if-ne v4, v8, :cond_1

    .line 339
    iget v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f08026c

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0
.end method

.method private isShow()Z
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 674
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playVideo()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 539
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v1, :cond_1

    .line 540
    :cond_0
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "openVideo() uri null or surfaceholder null"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    :goto_0
    return-void

    .line 544
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_2

    .line 545
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 546
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 549
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-nez v1, :cond_3

    .line 550
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 554
    :cond_3
    :try_start_0
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 556
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 564
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 568
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 573
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 584
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 621
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 623
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    .line 624
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    .line 625
    const-string v1, "android.resource"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 626
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 638
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/16 v2, 0x79e

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 639
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 640
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 642
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setAIAContext(Z)V

    .line 643
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V

    .line 644
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->showBoarders()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    .local v0, "ex":Ljava/io/IOException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 628
    .end local v0    # "ex":Ljava/io/IOException;
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 647
    :catch_1
    move-exception v0

    .line 648
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 631
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :cond_5
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "content://"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 649
    :catch_2
    move-exception v0

    .line 650
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "VideoProgressPreviewPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException - Unable to open content: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 634
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_6
    :try_start_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1
.end method

.method private reSetSurfaceViewImage()V
    .locals 2

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1014
    return-void
.end method

.method private setAIAContext(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 972
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 978
    :goto_0
    return-void

    .line 973
    :catch_0
    move-exception v0

    .line 974
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "setAIAContext - NullPointerException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 975
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 976
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "setAIAContext - IllegalStateException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setDialogProperties()V
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/16 v3, 0x8

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 436
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 439
    return-void
.end method

.method private setLayout()V
    .locals 4

    .prologue
    const v2, 0x7f03003a

    const/4 v3, 0x0

    .line 260
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 261
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 262
    .local v0, "rotation":I
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    .line 274
    .end local v0    # "rotation":I
    :cond_1
    :goto_0
    return-void

    .line 264
    .restart local v0    # "rotation":I
    :cond_2
    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v1

    if-nez v1, :cond_4

    .line 265
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f03003d

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 266
    :cond_4
    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    .line 267
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f03003b

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 268
    :cond_5
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f03003c

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    goto :goto_0

    .line 272
    .end local v0    # "rotation":I
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    goto :goto_0
.end method

.method private setSurfaceViewImage()V
    .locals 6

    .prologue
    .line 1003
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSurfaceViewImage mCurrentPosition: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    iget v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    iput v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mStartPosition:I

    .line 1006
    const/4 v1, 0x0

    .line 1007
    .local v1, "imageDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v0, 0x0

    .line 1008
    .local v0, "imageBitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    iget v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    int-to-long v4, v3

    invoke-direct {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getThumbnailBitmap(Landroid/net/Uri;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1009
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    .end local v1    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1010
    .restart local v1    # "imageDrawable":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v2, v1}, Landroid/view/SurfaceView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1011
    return-void
.end method

.method private show()V
    .locals 5

    .prologue
    .line 655
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    if-eqz v1, :cond_1

    .line 658
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const-string v2, "com.sec.android.app.videoplayer"

    const-string v3, "VARV"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x308

    const-string v4, "SAMSUNG_FLAG_NO_RESIZE_ANIMATION_INCLUDE_CHILD"

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    .line 660
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 661
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 668
    :cond_1
    :goto_0
    return-void

    .line 662
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 664
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 665
    .local v0, "e":Landroid/view/WindowManager$BadTokenException;
    invoke-virtual {v0}, Landroid/view/WindowManager$BadTokenException;->printStackTrace()V

    goto :goto_0
.end method

.method private showBoarders()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 918
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 922
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 926
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 927
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 930
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSurfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_3

    .line 932
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setSurfaceViewImage()V

    .line 934
    :cond_3
    return-void
.end method


# virtual methods
.method public dpToPx(J)I
    .locals 3
    .param p1, "dp"    # J

    .prologue
    .line 961
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 962
    .local v0, "density":F
    long-to-float v1, p1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public getVideoViewSize()Z
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 388
    const/4 v0, 0x0

    .line 389
    .local v0, "dimensionsString":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-nez v7, :cond_0

    .line 390
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 393
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v7, :cond_1

    .line 394
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResolution(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 397
    :cond_1
    if-eqz v0, :cond_3

    .line 398
    const-string v7, "x"

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 399
    .local v1, "index":I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    .line 401
    .local v4, "totalLenght":I
    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 402
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {v0, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 408
    .end local v1    # "index":I
    .end local v4    # "totalLenght":I
    :goto_0
    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    if-eqz v7, :cond_5

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-eqz v7, :cond_5

    .line 409
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getCheckRoation()I

    move-result v2

    .line 410
    .local v2, "m_VideoRotation":I
    if-ne v2, v5, :cond_2

    .line 411
    iget v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 412
    .local v3, "temp":I
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 413
    iput v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    .line 415
    .end local v3    # "temp":I
    :cond_2
    const-string v6, "VideoProgressPreviewPopup"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getVideoViewSize() Width "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " Height "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    if-lt v6, v7, :cond_4

    .line 418
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08025f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    .line 419
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    div-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    .line 425
    :goto_1
    const-string v6, "VideoProgressPreviewPopup"

    const-string v7, "getvideoViewSize return true"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    .end local v2    # "m_VideoRotation":I
    :goto_2
    return v5

    .line 404
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    .line 405
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v7

    iput v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    goto :goto_0

    .line 421
    .restart local v2    # "m_VideoRotation":I
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080266

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    .line 422
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoWidth:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoHeight:I

    mul-int/2addr v6, v7

    iget v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoWidth:I

    div-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mMeasuredVideoHeight:I

    goto :goto_1

    .line 428
    .end local v2    # "m_VideoRotation":I
    :cond_5
    const-string v5, "VideoProgressPreviewPopup"

    const-string v7, "Video width or height zero"

    invoke-static {v5, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v5, v6

    .line 429
    goto :goto_2
.end method

.method public init()V
    .locals 3

    .prologue
    .line 249
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "init"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 252
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mFormatter:Ljava/util/Formatter;

    .line 253
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setLayout()V

    .line 254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d01d1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d01d5

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->relative_layout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d01d4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    .line 257
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 878
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 880
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 881
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 882
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "video progress popup pause() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 883
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 886
    :catch_0
    move-exception v0

    .line 887
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public progressSeekto()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 214
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    .line 215
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "progressSeekto() :: remove SEEK msg"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 221
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getSeekHandler()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 226
    :cond_1
    :goto_0
    return-void

    .line 224
    :cond_2
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "progressSeekto mediaplayer is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pxToDp(J)I
    .locals 3
    .param p1, "px"    # J

    .prologue
    .line 966
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 967
    .local v0, "density":F
    long-to-float v1, p1

    div-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public removeDelayedMessage()V
    .locals 7

    .prologue
    const/4 v6, 0x7

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 786
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 789
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 793
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_DISMISS_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 795
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 798
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 799
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_SEEK)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 800
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 803
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 804
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_PLAY)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 808
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 809
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "removeDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_PLAY)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 813
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 814
    return-void
.end method

.method public seek()V
    .locals 5

    .prologue
    .line 893
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    .line 894
    iget v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 895
    .local v1, "position":I
    if-gtz v1, :cond_0

    .line 896
    const/4 v1, 0x0

    .line 898
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v2, :cond_1

    .line 900
    :try_start_0
    const-string v2, "VideoProgressPreviewPopup"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "seek() position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 907
    .end local v1    # "position":I
    :cond_1
    :goto_0
    return-void

    .line 902
    .restart local v1    # "position":I
    :catch_0
    move-exception v0

    .line 903
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public sendDelayedMoveMessage()V
    .locals 4

    .prologue
    const/4 v3, 0x7

    const/4 v2, 0x5

    .line 757
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 758
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_PLAY"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 759
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 761
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 762
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_SEEK"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 766
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 767
    return-void
.end method

.method public sendDelayedPauseMessage()V
    .locals 2

    .prologue
    .line 774
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 775
    return-void
.end method

.method public sendDelayedSeekMessage(I)V
    .locals 5
    .param p1, "ms"    # I

    .prologue
    const/4 v4, 0x7

    .line 778
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "send move msg remove MSG_SHOW_SEEK"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 782
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 783
    return-void
.end method

.method public sendDelayedShowMessage()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 747
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "sendDelayedMessage"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 749
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "sendDelayedMessage: removeMessages(HoverMsg.MSG_SHOW_POPUP)"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 752
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 753
    return-void
.end method

.method public sendDelayedStartMessage(I)V
    .locals 4
    .param p1, "ms"    # I

    .prologue
    .line 770
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x5

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 771
    return-void
.end method

.method public setCurrentPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 910
    iput p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentPosition:I

    .line 912
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 913
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->currentTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 915
    :cond_0
    return-void
.end method

.method public setDialogPosition()V
    .locals 12

    .prologue
    .line 443
    const-string v9, "VideoProgressPreviewPopup"

    const-string v10, "setPosition"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    if-eqz v9, :cond_5

    .line 447
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 448
    .local v6, "params":Landroid/view/WindowManager$LayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 449
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 450
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    check-cast v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v9

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getRotation()I

    move-result v7

    .line 451
    .local v7, "rotation":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 452
    const/4 v9, 0x3

    if-eq v7, v9, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 453
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x53

    invoke-virtual {v9, v10}, Landroid/view/Window;->setGravity(I)V

    .line 457
    :cond_1
    :goto_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 473
    .end local v7    # "rotation":I
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    div-int/lit8 v10, v10, 0x2

    iget v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    if-gt v9, v10, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v9

    if-eqz v9, :cond_d

    .line 475
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v9

    if-nez v9, :cond_3

    .line 476
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-ge v9, v10, :cond_c

    .line 477
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 503
    :cond_3
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    if-eqz v9, :cond_5

    .line 504
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 505
    .local v0, "ArrowParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    div-int/lit8 v2, v9, 0x2

    .line 506
    .local v2, "halfArrowWidth":I
    const/4 v3, 0x0

    .line 507
    .local v3, "leftMargin":I
    const/4 v8, 0x0

    .line 508
    .local v8, "topMargin":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v9

    if-eqz v9, :cond_11

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_11

    .line 509
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v9, v10

    iget v10, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v9, v10

    sub-int v8, v9, v2

    .line 514
    :goto_3
    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mArrowWidth:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v10, v10, 0x2

    sub-int v4, v9, v10

    .line 515
    .local v4, "leftMarginRightLimit":I
    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v9, v9, 0x2

    sub-int v5, v2, v9

    .line 517
    .local v5, "leftMaringLeftLimit":I
    if-ge v3, v5, :cond_12

    .line 518
    move v3, v5

    .line 522
    :cond_4
    :goto_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v9

    if-eqz v9, :cond_13

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_13

    .line 523
    iput v8, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 528
    :goto_5
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 536
    .end local v0    # "ArrowParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "halfArrowWidth":I
    .end local v3    # "leftMargin":I
    .end local v4    # "leftMarginRightLimit":I
    .end local v5    # "leftMaringLeftLimit":I
    .end local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .end local v8    # "topMargin":I
    :cond_5
    :goto_6
    return-void

    .line 454
    .restart local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .restart local v7    # "rotation":I
    :cond_6
    const/4 v9, 0x1

    if-ne v7, v9, :cond_1

    .line 455
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x33

    invoke-virtual {v9, v10}, Landroid/view/Window;->setGravity(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 530
    .end local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .end local v7    # "rotation":I
    :catch_0
    move-exception v1

    .line 531
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_6

    .line 459
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    .restart local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .restart local v7    # "rotation":I
    :cond_7
    if-eqz v7, :cond_8

    :try_start_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 460
    :cond_8
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x35

    invoke-virtual {v9, v10}, Landroid/view/Window;->setGravity(I)V

    .line 464
    :cond_9
    :goto_7
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupHeight:I

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->y:I
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 532
    .end local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .end local v7    # "rotation":I
    :catch_1
    move-exception v1

    .line 533
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_6

    .line 461
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v6    # "params":Landroid/view/WindowManager$LayoutParams;
    .restart local v7    # "rotation":I
    :cond_a
    const/4 v9, 0x2

    if-ne v7, v9, :cond_9

    .line 462
    :try_start_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x33

    invoke-virtual {v9, v10}, Landroid/view/Window;->setGravity(I)V

    goto :goto_7

    .line 467
    .end local v7    # "rotation":I
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->videoPreviewDialog:Landroid/app/Dialog;

    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v9

    const/16 v10, 0x33

    invoke-virtual {v9, v10}, Landroid/view/Window;->setGravity(I)V

    .line 468
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 469
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08025e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    sub-int/2addr v9, v10

    invoke-direct {p0, v9}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getRawY(I)I

    move-result v9

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/16 :goto_1

    .line 478
    :cond_c
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    if-le v9, v10, :cond_3

    .line 479
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    sub-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_2

    .line 485
    :cond_d
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v10, v10, 0x2

    if-le v9, v10, :cond_f

    .line 486
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v10, v10, 0x2

    if-ge v9, v10, :cond_e

    .line 487
    iget v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v9, v9, 0x2

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_2

    .line 488
    :cond_e
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    add-int/2addr v10, v11

    iget v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    if-le v9, v10, :cond_3

    .line 489
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_2

    .line 493
    :cond_f
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    div-int/lit8 v10, v10, 0x2

    if-ge v9, v10, :cond_3

    .line 494
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-ge v9, v10, :cond_10

    .line 495
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_2

    .line 496
    :cond_10
    iget v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    add-int/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    add-int/2addr v10, v11

    if-le v9, v10, :cond_3

    .line 497
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mPopupWidth:I

    sub-int/2addr v9, v10

    iput v9, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_2

    .line 511
    .restart local v0    # "ArrowParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v2    # "halfArrowWidth":I
    .restart local v3    # "leftMargin":I
    .restart local v8    # "topMargin":I
    :cond_11
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    add-int/2addr v9, v10

    iget v10, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    sub-int/2addr v9, v10

    sub-int v3, v9, v2

    goto/16 :goto_3

    .line 519
    .restart local v4    # "leftMarginRightLimit":I
    .restart local v5    # "leftMaringLeftLimit":I
    :cond_12
    if-le v3, v4, :cond_4

    .line 520
    move v3, v4

    goto/16 :goto_4

    .line 525
    :cond_13
    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_5
.end method

.method public setHolder()V
    .locals 3

    .prologue
    .line 852
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 854
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 861
    :cond_0
    :goto_0
    return-void

    .line 855
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 857
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 858
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public setHoverEventStart(Z)V
    .locals 0
    .param p1, "hoverStart"    # Z

    .prologue
    .line 1017
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mHoverEventStart:Z

    .line 1018
    return-void
.end method

.method public setParam(Landroid/content/Context;ILandroid/graphics/Rect;Landroid/net/Uri;Lcom/sec/android/app/videoplayer/type/SchemeType;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listtype"    # I
    .param p3, "mRect"    # Landroid/graphics/Rect;
    .param p4, "uri"    # Landroid/net/Uri;
    .param p5, "videoSchemeType"    # Lcom/sec/android/app/videoplayer/type/SchemeType;

    .prologue
    .line 818
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    .line 819
    iput-object p4, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mUri:Landroid/net/Uri;

    .line 820
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekbarRect:Landroid/graphics/Rect;

    .line 821
    iput-object p5, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 824
    return-void
.end method

.method public setRawX(IIII)V
    .locals 1
    .param p1, "mX"    # I
    .param p2, "seekBarLeftPadding"    # I
    .param p3, "mPositionStatus"    # I
    .param p4, "progress"    # I

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->FrameImage:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 371
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mProgressX:I

    .line 372
    iput p2, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mSeekBarPadding:I

    .line 373
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 864
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 866
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 867
    const-string v1, "VideoProgressPreviewPopup"

    const-string v2, "video progress popup start() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 869
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->sendDelayedSeekMessage(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 875
    :cond_0
    :goto_0
    return-void

    .line 871
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
