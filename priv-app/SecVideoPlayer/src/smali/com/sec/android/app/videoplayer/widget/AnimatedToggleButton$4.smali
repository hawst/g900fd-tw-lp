.class Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initButtonAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V
    .locals 0

    .prologue
    .line 559
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 579
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 566
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # getter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mAnimationState:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1000(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, v1, :cond_1

    .line 567
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->B:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    # setter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mAnimationState:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1002(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;)Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    .line 568
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initMask()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    .line 574
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->invalidate()V

    .line 575
    return-void

    .line 569
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # getter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mAnimationState:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1000(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->BA:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, v1, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->A:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    # setter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mAnimationState:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1002(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;)Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initMask()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$4;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # getter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mMaxRadius:F
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1200(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)F

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->updateMask(F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1300(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;F)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 583
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 562
    return-void
.end method
