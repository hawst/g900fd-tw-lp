.class public final Lcom/sec/android/app/videoplayer/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final actionbar_black_selector:I = 0x7f020000

.field public static final actionbar_menu_text:I = 0x7f020001

.field public static final actionbar_selector:I = 0x7f020002

.field public static final actionbar_tab_text_selector:I = 0x7f020003

.field public static final aia_btn_control_ff:I = 0x7f020004

.field public static final aia_btn_control_next:I = 0x7f020005

.field public static final aia_btn_control_pause:I = 0x7f020006

.field public static final aia_btn_control_play:I = 0x7f020007

.field public static final aia_btn_control_prev:I = 0x7f020008

.field public static final aia_btn_control_rew:I = 0x7f020009

.field public static final aia_btn_control_stop:I = 0x7f02000a

.field public static final allshare_my_device:I = 0x7f02000b

.field public static final baidu_icon:I = 0x7f02000c

.field public static final bookmark_selector_item:I = 0x7f02000d

.field public static final bookmark_thumb_selector:I = 0x7f02000e

.field public static final btn_2d_selector:I = 0x7f02000f

.field public static final btn_3d_selector:I = 0x7f020010

.field public static final btn_allshare_volume_down:I = 0x7f020011

.field public static final btn_allshare_volume_mute:I = 0x7f020012

.field public static final btn_allshare_volume_up:I = 0x7f020013

.field public static final btn_app_in_app:I = 0x7f020014

.field public static final btn_check_to_off_mtrl:I = 0x7f020015

.field public static final btn_check_to_on_mtrl:I = 0x7f020016

.field public static final btn_control_ff:I = 0x7f020017

.field public static final btn_control_mini_arrow_left:I = 0x7f020018

.field public static final btn_control_mini_arrow_right:I = 0x7f020019

.field public static final btn_control_next:I = 0x7f02001a

.field public static final btn_control_pause:I = 0x7f02001b

.field public static final btn_control_play:I = 0x7f02001c

.field public static final btn_control_prev:I = 0x7f02001d

.field public static final btn_control_rew:I = 0x7f02001e

.field public static final btn_control_stop:I = 0x7f02001f

.field public static final btn_delete:I = 0x7f020020

.field public static final btn_delete_selector:I = 0x7f020021

.field public static final btn_display_change_selector:I = 0x7f020022

.field public static final btn_display_change_selector_connected:I = 0x7f020023

.field public static final btn_lock:I = 0x7f020024

.field public static final btn_more:I = 0x7f020025

.field public static final btn_more_option:I = 0x7f020026

.field public static final btn_next:I = 0x7f020027

.field public static final btn_next_focus:I = 0x7f020028

.field public static final btn_next_press:I = 0x7f020029

.field public static final btn_pause:I = 0x7f02002a

.field public static final btn_pause_focus:I = 0x7f02002b

.field public static final btn_pause_press:I = 0x7f02002c

.field public static final btn_play:I = 0x7f02002d

.field public static final btn_play_focus:I = 0x7f02002e

.field public static final btn_play_press:I = 0x7f02002f

.field public static final btn_playerlist_on_selector:I = 0x7f020030

.field public static final btn_playerlist_selector:I = 0x7f020031

.field public static final btn_playspeed_minus:I = 0x7f020032

.field public static final btn_playspeed_mode:I = 0x7f020033

.field public static final btn_playspeed_mode_textcolor:I = 0x7f020034

.field public static final btn_playspeed_plus:I = 0x7f020035

.field public static final btn_prev:I = 0x7f020036

.field public static final btn_prev_focus:I = 0x7f020037

.field public static final btn_prev_press:I = 0x7f020038

.field public static final btn_rotate_selector:I = 0x7f020039

.field public static final btn_sound_alive_selector:I = 0x7f02003a

.field public static final btn_tv_connect_selector:I = 0x7f02003b

.field public static final btn_video_scrubber_control:I = 0x7f02003c

.field public static final btn_view_mode_fill:I = 0x7f02003d

.field public static final btn_view_mode_fit_height:I = 0x7f02003e

.field public static final btn_view_mode_keep_aspect:I = 0x7f02003f

.field public static final btn_view_mode_orig:I = 0x7f020040

.field public static final btn_volume:I = 0x7f020041

.field public static final btn_volume_down:I = 0x7f020042

.field public static final btn_volume_mute:I = 0x7f020043

.field public static final btn_volume_up:I = 0x7f020044

.field public static final btn_widget_play:I = 0x7f020045

.field public static final btn_widget_play_large:I = 0x7f020046

.field public static final change_display_dummy:I = 0x7f020047

.field public static final chapter_preview_press:I = 0x7f020048

.field public static final check_icon_selector:I = 0x7f020049

.field public static final common_title_btn_arrow_normal:I = 0x7f02004a

.field public static final common_title_btn_arrow_press:I = 0x7f02004b

.field public static final common_title_btn_close_normal:I = 0x7f02004c

.field public static final common_title_btn_close_press:I = 0x7f02004d

.field public static final common_title_btn_list_normal:I = 0x7f02004e

.field public static final common_title_btn_list_press:I = 0x7f02004f

.field public static final complete:I = 0x7f020050

.field public static final contextual_option_bg:I = 0x7f020051

.field public static final custom_progress_bar_thumbnail:I = 0x7f020052

.field public static final dropbox_png:I = 0x7f020053

.field public static final duration_text_color_default:I = 0x7f020054

.field public static final easy_btn_control_pause:I = 0x7f020055

.field public static final easy_btn_control_play:I = 0x7f020056

.field public static final easy_video_control_pause_f:I = 0x7f020057

.field public static final easy_video_control_pause_n:I = 0x7f020058

.field public static final easy_video_control_pause_p:I = 0x7f020059

.field public static final easy_video_control_play_f:I = 0x7f02005a

.field public static final easy_video_control_play_n:I = 0x7f02005b

.field public static final easy_video_control_play_p:I = 0x7f02005c

.field public static final ext_scr_btn_exit_n:I = 0x7f02005d

.field public static final folder_list_divider:I = 0x7f02005e

.field public static final folder_list_divider_press:I = 0x7f02005f

.field public static final help_main_new_features_01:I = 0x7f020060

.field public static final help_main_new_features_02:I = 0x7f020061

.field public static final help_main_new_features_03:I = 0x7f020062

.field public static final help_main_new_features_page_01:I = 0x7f020063

.field public static final help_main_new_features_page_01_focus:I = 0x7f020064

.field public static final help_main_new_features_page_01_press:I = 0x7f020065

.field public static final help_main_new_features_page_02:I = 0x7f020066

.field public static final help_main_new_features_page_02_focus:I = 0x7f020067

.field public static final help_main_new_features_page_02_press:I = 0x7f020068

.field public static final help_main_new_features_page_03:I = 0x7f020069

.field public static final help_main_new_features_page_03_focus:I = 0x7f02006a

.field public static final help_main_new_features_page_03_press:I = 0x7f02006b

.field public static final help_popup_picker_bg_w_01:I = 0x7f02006c

.field public static final help_popup_picker_t_c:I = 0x7f02006d

.field public static final hover_video_bg_c:I = 0x7f02006e

.field public static final hover_video_bg_l:I = 0x7f02006f

.field public static final hover_video_bg_r:I = 0x7f020070

.field public static final hover_video_player_thumb:I = 0x7f020071

.field public static final hover_video_player_thumb01:I = 0x7f020072

.field public static final ic_logo_accuweather:I = 0x7f020073

.field public static final ic_logo_accuweather_video:I = 0x7f020074

.field public static final ic_logo_weathernews:I = 0x7f020075

.field public static final ic_menu_download:I = 0x7f020076

.field public static final ic_search_api_holo:I = 0x7f020077

.field public static final ic_search_api_holo_light:I = 0x7f020078

.field public static final ic_widi_audio:I = 0x7f020079

.field public static final ic_widi_camera:I = 0x7f02007a

.field public static final ic_widi_computer:I = 0x7f02007b

.field public static final ic_widi_displays:I = 0x7f02007c

.field public static final ic_widi_game_deivces:I = 0x7f02007d

.field public static final ic_widi_input_device:I = 0x7f02007e

.field public static final ic_widi_multimedia:I = 0x7f02007f

.field public static final ic_widi_network_infra:I = 0x7f020080

.field public static final ic_widi_printer:I = 0x7f020081

.field public static final ic_widi_storage:I = 0x7f020082

.field public static final ic_widi_tab:I = 0x7f020083

.field public static final ic_widi_telephone:I = 0x7f020084

.field public static final ic_widi_unknown_dmr:I = 0x7f020085

.field public static final ico_settings_more_close:I = 0x7f020086

.field public static final ico_settings_more_close_h:I = 0x7f020087

.field public static final ico_settings_more_open:I = 0x7f020088

.field public static final ico_settings_more_open_h:I = 0x7f020089

.field public static final library_thumbnail_default_video_wl:I = 0x7f02008a

.field public static final list_divider_holo_dark:I = 0x7f02008b

.field public static final list_focus:I = 0x7f02008c

.field public static final list_press:I = 0x7f02008d

.field public static final list_select:I = 0x7f02008e

.field public static final list_selector:I = 0x7f02008f

.field public static final list_selector_item:I = 0x7f020090

.field public static final magazine_video_noitem_ic:I = 0x7f020091

.field public static final magazine_video_preview:I = 0x7f020092

.field public static final magazine_widget_video_noitem_bg:I = 0x7f020093

.field public static final min_icon_focus:I = 0x7f020094

.field public static final min_icon_normal:I = 0x7f020095

.field public static final min_icon_press:I = 0x7f020096

.field public static final mini_arrow_selector:I = 0x7f020097

.field public static final mini_close_selector:I = 0x7f020098

.field public static final mini_list_selector:I = 0x7f020099

.field public static final mini_video:I = 0x7f02009a

.field public static final music_control_progressbar_buffering:I = 0x7f02009b

.field public static final music_play_thumb_loading:I = 0x7f02009c

.field public static final music_volume_dim:I = 0x7f02009d

.field public static final mute_icon_focus:I = 0x7f02009e

.field public static final mute_icon_normal:I = 0x7f02009f

.field public static final mute_icon_press:I = 0x7f0200a0

.field public static final mux_ic_video:I = 0x7f0200a1

.field public static final mux_settings_personal_08:I = 0x7f0200a2

.field public static final mux_settings_personal_08_dim:I = 0x7f0200a3

.field public static final no_video_widget_bg_l:I = 0x7f0200a4

.field public static final no_video_widget_bg_p:I = 0x7f0200a5

.field public static final overlay_tutorial:I = 0x7f0200a6

.field public static final playerlist_second_row_text_color:I = 0x7f0200a7

.field public static final plus_icon_focus:I = 0x7f0200a8

.field public static final plus_icon_normal:I = 0x7f0200a9

.field public static final plus_icon_press:I = 0x7f0200aa

.field public static final popup_value_color:I = 0x7f0200ab

.field public static final preview_selector:I = 0x7f0200ac

.field public static final quick_panel_icon_call_thumbnail_focus:I = 0x7f0200ad

.field public static final quick_panel_icon_call_thumbnail_icon:I = 0x7f0200ae

.field public static final quick_panel_icon_call_thumbnail_press:I = 0x7f0200af

.field public static final quick_panel_icon_mini_video:I = 0x7f0200b0

.field public static final quick_panel_icon_mini_video_h:I = 0x7f0200b1

.field public static final quick_panel_music_ff:I = 0x7f0200b2

.field public static final quick_panel_music_ff_disable:I = 0x7f0200b3

.field public static final quick_panel_music_ff_disable_focus:I = 0x7f0200b4

.field public static final quick_panel_music_ff_focus:I = 0x7f0200b5

.field public static final quick_panel_music_ff_press:I = 0x7f0200b6

.field public static final quick_panel_music_next:I = 0x7f0200b7

.field public static final quick_panel_music_next_disable:I = 0x7f0200b8

.field public static final quick_panel_music_next_disable_focus:I = 0x7f0200b9

.field public static final quick_panel_music_next_focus:I = 0x7f0200ba

.field public static final quick_panel_music_next_press:I = 0x7f0200bb

.field public static final quick_panel_music_pause:I = 0x7f0200bc

.field public static final quick_panel_music_pause_disable:I = 0x7f0200bd

.field public static final quick_panel_music_pause_disable_focus:I = 0x7f0200be

.field public static final quick_panel_music_pause_focus:I = 0x7f0200bf

.field public static final quick_panel_music_pause_press:I = 0x7f0200c0

.field public static final quick_panel_music_play:I = 0x7f0200c1

.field public static final quick_panel_music_play_dim:I = 0x7f0200c2

.field public static final quick_panel_music_play_disable:I = 0x7f0200c3

.field public static final quick_panel_music_play_disable_focus:I = 0x7f0200c4

.field public static final quick_panel_music_play_focus:I = 0x7f0200c5

.field public static final quick_panel_music_play_press:I = 0x7f0200c6

.field public static final quick_panel_music_rew:I = 0x7f0200c7

.field public static final quick_panel_music_rew_disable:I = 0x7f0200c8

.field public static final quick_panel_music_rew_disable_focus:I = 0x7f0200c9

.field public static final quick_panel_music_rew_focus:I = 0x7f0200ca

.field public static final quick_panel_music_rew_press:I = 0x7f0200cb

.field public static final quick_panel_music_stop:I = 0x7f0200cc

.field public static final quick_panel_music_stop_disable:I = 0x7f0200cd

.field public static final quick_panel_music_stop_focus:I = 0x7f0200ce

.field public static final quick_panel_music_stop_press:I = 0x7f0200cf

.field public static final quick_panel_player_cancel:I = 0x7f0200d0

.field public static final quick_panel_player_cancel_disable:I = 0x7f0200d1

.field public static final quick_panel_player_cancel_disable_focus:I = 0x7f0200d2

.field public static final quick_panel_player_cancel_focus:I = 0x7f0200d3

.field public static final quick_panel_player_cancel_press:I = 0x7f0200d4

.field public static final quick_panel_player_close:I = 0x7f0200d5

.field public static final quick_panel_player_close_focus:I = 0x7f0200d6

.field public static final quick_panel_player_close_press:I = 0x7f0200d7

.field public static final quick_panel_player_exit:I = 0x7f0200d8

.field public static final quick_panel_player_exit_disable:I = 0x7f0200d9

.field public static final quick_panel_player_exit_disable_focus:I = 0x7f0200da

.field public static final quick_panel_player_exit_focus:I = 0x7f0200db

.field public static final quick_panel_player_exit_press:I = 0x7f0200dc

.field public static final quick_panel_player_stop:I = 0x7f0200dd

.field public static final quick_panel_player_stop_disable:I = 0x7f0200de

.field public static final quick_panel_player_stop_disable_focus:I = 0x7f0200df

.field public static final quick_panel_player_stop_focus:I = 0x7f0200e0

.field public static final quick_panel_player_stop_press:I = 0x7f0200e1

.field public static final quick_panel_selector:I = 0x7f0200e2

.field public static final quickpanel_btn_focus:I = 0x7f0200e3

.field public static final quickpanel_btn_normal:I = 0x7f0200e4

.field public static final quickpanel_btn_press:I = 0x7f0200e5

.field public static final ripple_bg_common:I = 0x7f0200e6

.field public static final ripple_common:I = 0x7f0200e7

.field public static final scover_play:I = 0x7f0200e8

.field public static final scover_player_ic_close_normal:I = 0x7f0200e9

.field public static final scover_player_ic_close_pressed:I = 0x7f0200ea

.field public static final search_view_selector:I = 0x7f0200eb

.field public static final seek_thumb:I = 0x7f0200ec

.field public static final select_item:I = 0x7f0200ed

.field public static final select_text:I = 0x7f0200ee

.field public static final select_text2:I = 0x7f0200ef

.field public static final selector_list_view_effect:I = 0x7f0200f0

.field public static final selector_thumbnail:I = 0x7f0200f1

.field public static final settings_selector_item:I = 0x7f0200f2

.field public static final settings_text_selector:I = 0x7f0200f3

.field public static final settings_value_text_selector:I = 0x7f0200f4

.field public static final split_sub_text:I = 0x7f0200f5

.field public static final splittextcolor:I = 0x7f0200f6

.field public static final stat_notify_video:I = 0x7f0200f7

.field public static final sub_list_text_selector:I = 0x7f0200f8

.field public static final subtitle_preview_bg:I = 0x7f0200f9

.field public static final subview_btn_allshare:I = 0x7f0200fa

.field public static final subview_btn_allshare_connected:I = 0x7f0200fb

.field public static final subview_btn_control_next:I = 0x7f0200fc

.field public static final subview_btn_control_pause:I = 0x7f0200fd

.field public static final subview_btn_control_play:I = 0x7f0200fe

.field public static final subview_btn_control_prev:I = 0x7f0200ff

.field public static final subview_btn_easy_mute:I = 0x7f020100

.field public static final subview_btn_easy_volume:I = 0x7f020101

.field public static final subview_btn_more:I = 0x7f020102

.field public static final subview_btn_view_mode_fill:I = 0x7f020103

.field public static final subview_btn_view_mode_fit_height:I = 0x7f020104

.field public static final subview_btn_view_mode_keep_aspect:I = 0x7f020105

.field public static final subview_btn_view_mode_orig:I = 0x7f020106

.field public static final sw_btn_thumb_delete:I = 0x7f020107

.field public static final sw_btn_thumb_delete_focused:I = 0x7f020108

.field public static final sw_btn_thumb_delete_pressed:I = 0x7f020109

.field public static final tag_buddy_icon_location:I = 0x7f02010a

.field public static final tag_buddy_icon_time:I = 0x7f02010b

.field public static final thumbnail_border:I = 0x7f02010c

.field public static final thumbnail_border_playing:I = 0x7f02010d

.field public static final thumbnail_progress:I = 0x7f02010e

.field public static final thumbnail_progress_bg:I = 0x7f02010f

.field public static final tutorial_1:I = 0x7f020110

.field public static final tutorial_2:I = 0x7f020111

.field public static final tutorial_3:I = 0x7f020112

.field public static final tutorial_4:I = 0x7f020113

.field public static final tutorial_5:I = 0x7f020114

.field public static final tw_ab_transparent_holo:I = 0x7f020115

.field public static final tw_ab_transparent_light_holo:I = 0x7f020116

.field public static final tw_action_bar_icon_bestvideoshot_holo_dark:I = 0x7f020117

.field public static final tw_action_bar_icon_bt_holo_dark:I = 0x7f020118

.field public static final tw_action_bar_icon_cancel_02_disabled_holo_light:I = 0x7f020119

.field public static final tw_action_bar_icon_cancel_02_holo_light:I = 0x7f02011a

.field public static final tw_action_bar_icon_chapter_preview_holo_dark:I = 0x7f02011b

.field public static final tw_action_bar_icon_check_disabled_holo_light:I = 0x7f02011c

.field public static final tw_action_bar_icon_check_holo_light:I = 0x7f02011d

.field public static final tw_action_bar_icon_delete_holo_dark:I = 0x7f02011e

.field public static final tw_action_bar_icon_details_holo_dark:I = 0x7f02011f

.field public static final tw_action_bar_icon_divider:I = 0x7f020120

.field public static final tw_action_bar_icon_duration_holo_dark:I = 0x7f020121

.field public static final tw_action_bar_icon_music_via_device_holo_dark:I = 0x7f020122

.field public static final tw_action_bar_icon_nearby_devices_holo_dark:I = 0x7f020123

.field public static final tw_action_bar_icon_search_holo_dark:I = 0x7f020124

.field public static final tw_action_bar_icon_setting_holo_dark:I = 0x7f020125

.field public static final tw_action_bar_icon_share_disabled_holo_dark:I = 0x7f020126

.field public static final tw_action_bar_icon_share_holo_dark:I = 0x7f020127

.field public static final tw_action_bar_icon_sub_title_holo_light:I = 0x7f020128

.field public static final tw_action_bar_icon_trim_holo_dark:I = 0x7f020129

.field public static final tw_action_bar_icon_video_editor_holo_dark:I = 0x7f02012a

.field public static final tw_action_item_background:I = 0x7f02012b

.field public static final tw_action_item_background_focused_holo:I = 0x7f02012c

.field public static final tw_action_item_background_pressed_holo:I = 0x7f02012d

.field public static final tw_action_item_background_selected_holo:I = 0x7f02012e

.field public static final tw_background_light:I = 0x7f02012f

.field public static final tw_btn_check_off_holo_dark:I = 0x7f020130

.field public static final tw_btn_check_off_pressed_holo_dark:I = 0x7f020131

.field public static final tw_btn_check_on_holo_dark:I = 0x7f020132

.field public static final tw_btn_check_on_pressed_holo_dark:I = 0x7f020133

.field public static final tw_dialog_list_section_divider_holo_dark:I = 0x7f020134

.field public static final tw_divider_ab_holo_dark:I = 0x7f020135

.field public static final tw_divider_vertical_holo_light:I = 0x7f020136

.field public static final tw_expander_list_line_holo:I = 0x7f020137

.field public static final tw_ic_ab_back_holo_light:I = 0x7f020138

.field public static final tw_ic_ab_back_mtrl:I = 0x7f020139

.field public static final tw_list_focused_holo:I = 0x7f02013a

.field public static final tw_list_pressed_holo:I = 0x7f02013b

.field public static final tw_list_section_divider:I = 0x7f02013c

.field public static final tw_list_selected_holo_light:I = 0x7f02013d

.field public static final tw_preference_contents_list_left_split_default_holo_light:I = 0x7f02013e

.field public static final tw_progress_bg_holo_light:I = 0x7f02013f

.field public static final tw_progress_primary_holo_light:I = 0x7f020140

.field public static final tw_scrubber_control_focused_holo_dark:I = 0x7f020141

.field public static final tw_scrubber_control_holo_dark:I = 0x7f020142

.field public static final tw_scrubber_control_pressed_holo_dark:I = 0x7f020143

.field public static final tw_tab_selected:I = 0x7f020144

.field public static final tw_tab_selected_pressed:I = 0x7f020145

.field public static final tw_tab_selector:I = 0x7f020146

.field public static final tw_tab_unselected:I = 0x7f020147

.field public static final tw_thumbnail_focused:I = 0x7f020148

.field public static final tw_title_search_box:I = 0x7f020149

.field public static final tw_title_search_box_pressed:I = 0x7f02014a

.field public static final tw_topmenu_pressed_bg:I = 0x7f02014b

.field public static final video:I = 0x7f02014c

.field public static final video_2d_icon_focused:I = 0x7f02014d

.field public static final video_2d_icon_normal:I = 0x7f02014e

.field public static final video_2d_icon_pressed:I = 0x7f02014f

.field public static final video_3d_icon_focused:I = 0x7f020150

.field public static final video_3d_icon_normal:I = 0x7f020151

.field public static final video_3d_icon_pressed:I = 0x7f020152

.field public static final video_add_bg:I = 0x7f020153

.field public static final video_airview_popup_picker_b_a:I = 0x7f020154

.field public static final video_airview_popup_picker_b_c:I = 0x7f020155

.field public static final video_airview_popup_picker_b_l:I = 0x7f020156

.field public static final video_airview_popup_picker_b_r:I = 0x7f020157

.field public static final video_airview_popup_picker_bg:I = 0x7f020158

.field public static final video_allshare_bg:I = 0x7f020159

.field public static final video_allshare_displaychange:I = 0x7f02015a

.field public static final video_allshare_icon_f:I = 0x7f02015b

.field public static final video_allshare_icon_n:I = 0x7f02015c

.field public static final video_allshare_icon_n_dim:I = 0x7f02015d

.field public static final video_allshare_icon_p:I = 0x7f02015e

.field public static final video_appinapp_d:I = 0x7f02015f

.field public static final video_appinapp_f:I = 0x7f020160

.field public static final video_appinapp_n:I = 0x7f020161

.field public static final video_appinapp_p:I = 0x7f020162

.field public static final video_bg_shadow:I = 0x7f020163

.field public static final video_bookmark_thumbnail_press:I = 0x7f020164

.field public static final video_bottom_player_bg_common:I = 0x7f020165

.field public static final video_btn_list_dim:I = 0x7f020166

.field public static final video_btn_list_focus:I = 0x7f020167

.field public static final video_btn_list_normal:I = 0x7f020168

.field public static final video_btn_list_on:I = 0x7f020169

.field public static final video_btn_list_press:I = 0x7f02016a

.field public static final video_btn_press:I = 0x7f02016b

.field public static final video_btn_tv_normal:I = 0x7f02016c

.field public static final video_btn_tv_normal_press:I = 0x7f02016d

.field public static final video_camera_usp_snapshot_icon:I = 0x7f02016e

.field public static final video_camera_usp_thumbnail:I = 0x7f02016f

.field public static final video_capture_center_f:I = 0x7f020170

.field public static final video_capture_center_n:I = 0x7f020171

.field public static final video_capture_center_p:I = 0x7f020172

.field public static final video_capture_left_f:I = 0x7f020173

.field public static final video_capture_left_n:I = 0x7f020174

.field public static final video_capture_left_p:I = 0x7f020175

.field public static final video_capture_right_f:I = 0x7f020176

.field public static final video_capture_right_n:I = 0x7f020177

.field public static final video_capture_right_p:I = 0x7f020178

.field public static final video_check_bar_memomark:I = 0x7f020179

.field public static final video_color_gradation:I = 0x7f02017a

.field public static final video_color_select:I = 0x7f02017b

.field public static final video_color_square_picker:I = 0x7f02017c

.field public static final video_colorchip_none:I = 0x7f02017d

.field public static final video_colors_01:I = 0x7f02017e

.field public static final video_colors_02:I = 0x7f02017f

.field public static final video_colors_03:I = 0x7f020180

.field public static final video_colors_04:I = 0x7f020181

.field public static final video_colors_05:I = 0x7f020182

.field public static final video_colors_06:I = 0x7f020183

.field public static final video_colors_07:I = 0x7f020184

.field public static final video_colors_08:I = 0x7f020185

.field public static final video_colors_09:I = 0x7f020186

.field public static final video_colors_outline:I = 0x7f020187

.field public static final video_control_ff_f:I = 0x7f020188

.field public static final video_control_ff_n:I = 0x7f020189

.field public static final video_control_ff_p:I = 0x7f02018a

.field public static final video_control_next_f:I = 0x7f02018b

.field public static final video_control_next_h:I = 0x7f02018c

.field public static final video_control_next_n:I = 0x7f02018d

.field public static final video_control_next_p:I = 0x7f02018e

.field public static final video_control_pause_f:I = 0x7f02018f

.field public static final video_control_pause_n:I = 0x7f020190

.field public static final video_control_pause_p:I = 0x7f020191

.field public static final video_control_play_f:I = 0x7f020192

.field public static final video_control_play_n:I = 0x7f020193

.field public static final video_control_play_p:I = 0x7f020194

.field public static final video_control_prev_f:I = 0x7f020195

.field public static final video_control_prev_h:I = 0x7f020196

.field public static final video_control_prev_n:I = 0x7f020197

.field public static final video_control_prev_p:I = 0x7f020198

.field public static final video_control_rew_f:I = 0x7f020199

.field public static final video_control_rew_n:I = 0x7f02019a

.field public static final video_control_rew_p:I = 0x7f02019b

.field public static final video_control_stop_f:I = 0x7f02019c

.field public static final video_control_stop_n:I = 0x7f02019d

.field public static final video_control_stop_p:I = 0x7f02019e

.field public static final video_control_vol_icon_f:I = 0x7f02019f

.field public static final video_control_vol_icon_n:I = 0x7f0201a0

.field public static final video_control_vol_icon_p:I = 0x7f0201a1

.field public static final video_control_vol_mute_icon_f:I = 0x7f0201a2

.field public static final video_control_vol_mute_icon_n:I = 0x7f0201a3

.field public static final video_control_vol_mute_icon_p:I = 0x7f0201a4

.field public static final video_default_thumbnail_image_press:I = 0x7f0201a5

.field public static final video_delete_icon_f:I = 0x7f0201a6

.field public static final video_delete_icon_n:I = 0x7f0201a7

.field public static final video_delete_icon_p:I = 0x7f0201a8

.field public static final video_fullplayer_multi_penmemo_bg:I = 0x7f0201a9

.field public static final video_fullplayer_multi_penmemo_press_bg:I = 0x7f0201aa

.field public static final video_fullplayer_multi_penmemo_selector:I = 0x7f0201ab

.field public static final video_fullplayer_single_penmemo_bg:I = 0x7f0201ac

.field public static final video_fullplayer_single_penmemo_press_bg:I = 0x7f0201ad

.field public static final video_fullplayer_single_penmemo_selector:I = 0x7f0201ae

.field public static final video_icon_rotate_f:I = 0x7f0201af

.field public static final video_icon_rotate_n:I = 0x7f0201b0

.field public static final video_icon_rotate_p:I = 0x7f0201b1

.field public static final video_list_ic_fingerprint_new:I = 0x7f0201b2

.field public static final video_list_on_bg:I = 0x7f0201b3

.field public static final video_list_progress_bar:I = 0x7f0201b4

.field public static final video_loading_bg:I = 0x7f0201b5

.field public static final video_play_btn_common:I = 0x7f0201b6

.field public static final video_play_btn_common_press:I = 0x7f0201b7

.field public static final video_play_btn_large:I = 0x7f0201b8

.field public static final video_play_btn_large_press:I = 0x7f0201b9

.field public static final video_player_allshare_cast:I = 0x7f0201ba

.field public static final video_player_arrow_left_01:I = 0x7f0201bb

.field public static final video_player_arrow_left_02:I = 0x7f0201bc

.field public static final video_player_arrow_left_03:I = 0x7f0201bd

.field public static final video_player_arrow_right_01:I = 0x7f0201be

.field public static final video_player_arrow_right_02:I = 0x7f0201bf

.field public static final video_player_arrow_right_03:I = 0x7f0201c0

.field public static final video_player_audio_only:I = 0x7f0201c1

.field public static final video_player_background:I = 0x7f0201c2

.field public static final video_player_btn_capture:I = 0x7f0201c3

.field public static final video_player_btn_capture_left:I = 0x7f0201c4

.field public static final video_player_btn_capture_right:I = 0x7f0201c5

.field public static final video_player_capture_frame:I = 0x7f0201c6

.field public static final video_player_chapter_01:I = 0x7f0201c7

.field public static final video_player_chapter_02:I = 0x7f0201c8

.field public static final video_player_chapter_03:I = 0x7f0201c9

.field public static final video_player_chapter_04:I = 0x7f0201ca

.field public static final video_player_chapter_05:I = 0x7f0201cb

.field public static final video_player_chapter_06:I = 0x7f0201cc

.field public static final video_player_chapter_07:I = 0x7f0201cd

.field public static final video_player_chapter_08:I = 0x7f0201ce

.field public static final video_player_chapter_09:I = 0x7f0201cf

.field public static final video_player_chapter_10:I = 0x7f0201d0

.field public static final video_player_chapter_11:I = 0x7f0201d1

.field public static final video_player_chapter_12:I = 0x7f0201d2

.field public static final video_player_control_vol_dn_icon:I = 0x7f0201d3

.field public static final video_player_control_vol_dn_icon_d:I = 0x7f0201d4

.field public static final video_player_control_vol_dn_icon_f:I = 0x7f0201d5

.field public static final video_player_control_vol_dn_icon_p:I = 0x7f0201d6

.field public static final video_player_control_vol_up_icon:I = 0x7f0201d7

.field public static final video_player_control_vol_up_icon_d:I = 0x7f0201d8

.field public static final video_player_control_vol_up_icon_f:I = 0x7f0201d9

.field public static final video_player_control_vol_up_icon_p:I = 0x7f0201da

.field public static final video_player_frame_arrow_01:I = 0x7f0201db

.field public static final video_player_frame_arrow_02:I = 0x7f0201dc

.field public static final video_player_frame_arrow_03:I = 0x7f0201dd

.field public static final video_player_frame_arrow_v_01:I = 0x7f0201de

.field public static final video_player_frame_arrow_v_02:I = 0x7f0201df

.field public static final video_player_frame_arrow_v_03:I = 0x7f0201e0

.field public static final video_player_frame_bg:I = 0x7f0201e1

.field public static final video_player_mini_arrow_left:I = 0x7f0201e2

.field public static final video_player_mini_arrow_left_f:I = 0x7f0201e3

.field public static final video_player_mini_arrow_left_p:I = 0x7f0201e4

.field public static final video_player_mini_arrow_right:I = 0x7f0201e5

.field public static final video_player_mini_arrow_right_f:I = 0x7f0201e6

.field public static final video_player_mini_arrow_right_p:I = 0x7f0201e7

.field public static final video_player_mini_player_bg:I = 0x7f0201e8

.field public static final video_player_moreoverflow:I = 0x7f0201e9

.field public static final video_player_moreoverflow_p:I = 0x7f0201ea

.field public static final video_player_no_chapter:I = 0x7f0201eb

.field public static final video_player_preview_frame:I = 0x7f0201ec

.field public static final video_player_preview_frame_focus:I = 0x7f0201ed

.field public static final video_player_preview_frame_press:I = 0x7f0201ee

.field public static final video_player_preview_frame_select:I = 0x7f0201ef

.field public static final video_player_soundalive_nomal:I = 0x7f0201f0

.field public static final video_player_soundalive_press:I = 0x7f0201f1

.field public static final video_player_speed_bg:I = 0x7f0201f2

.field public static final video_player_speed_btn:I = 0x7f0201f3

.field public static final video_player_speed_btn_f:I = 0x7f0201f4

.field public static final video_player_speed_btn_p:I = 0x7f0201f5

.field public static final video_popup_icon_mute_n:I = 0x7f0201f6

.field public static final video_popup_icon_no_lightness_00_n:I = 0x7f0201f7

.field public static final video_popup_icon_no_lightness_01_n:I = 0x7f0201f8

.field public static final video_popup_icon_no_lightness_02_n:I = 0x7f0201f9

.field public static final video_popup_icon_no_lightness_03_n:I = 0x7f0201fa

.field public static final video_popup_icon_no_lightness_04_n:I = 0x7f0201fb

.field public static final video_popup_icon_no_lightness_05_n:I = 0x7f0201fc

.field public static final video_popup_icon_no_lightness_06_n:I = 0x7f0201fd

.field public static final video_popup_icon_no_lightness_07_n:I = 0x7f0201fe

.field public static final video_popup_icon_no_lightness_08_n:I = 0x7f0201ff

.field public static final video_popup_icon_no_lightness_09_n:I = 0x7f020200

.field public static final video_popup_icon_no_lightness_10_n:I = 0x7f020201

.field public static final video_popup_icon_volume_n:I = 0x7f020202

.field public static final video_popup_play_progress:I = 0x7f020203

.field public static final video_popup_play_progress_bg:I = 0x7f020204

.field public static final video_popup_player_close_btn:I = 0x7f020205

.field public static final video_popup_player_close_f:I = 0x7f020206

.field public static final video_popup_player_close_n:I = 0x7f020207

.field public static final video_popup_player_close_p:I = 0x7f020208

.field public static final video_popup_player_ff_f:I = 0x7f020209

.field public static final video_popup_player_ff_n:I = 0x7f02020a

.field public static final video_popup_player_ff_p:I = 0x7f02020b

.field public static final video_popup_player_next_f:I = 0x7f02020c

.field public static final video_popup_player_next_n:I = 0x7f02020d

.field public static final video_popup_player_next_p:I = 0x7f02020e

.field public static final video_popup_player_pause_f:I = 0x7f02020f

.field public static final video_popup_player_pause_n:I = 0x7f020210

.field public static final video_popup_player_pause_p:I = 0x7f020211

.field public static final video_popup_player_play_f:I = 0x7f020212

.field public static final video_popup_player_play_n:I = 0x7f020213

.field public static final video_popup_player_play_p:I = 0x7f020214

.field public static final video_popup_player_prev_f:I = 0x7f020215

.field public static final video_popup_player_prev_n:I = 0x7f020216

.field public static final video_popup_player_prev_p:I = 0x7f020217

.field public static final video_popup_player_progress_bg:I = 0x7f020218

.field public static final video_popup_player_rew_f:I = 0x7f020219

.field public static final video_popup_player_rew_n:I = 0x7f02021a

.field public static final video_popup_player_rew_p:I = 0x7f02021b

.field public static final video_popup_player_stop_f:I = 0x7f02021c

.field public static final video_popup_player_stop_n:I = 0x7f02021d

.field public static final video_popup_player_stop_p:I = 0x7f02021e

.field public static final video_popup_progress_player_primary:I = 0x7f02021f

.field public static final video_popup_sync_minus_f:I = 0x7f020220

.field public static final video_popup_sync_minus_n:I = 0x7f020221

.field public static final video_popup_sync_minus_p:I = 0x7f020222

.field public static final video_popup_sync_plus_f:I = 0x7f020223

.field public static final video_popup_sync_plus_n:I = 0x7f020224

.field public static final video_popup_sync_plus_p:I = 0x7f020225

.field public static final video_popupplay_bg:I = 0x7f020226

.field public static final video_progress_bar:I = 0x7f020227

.field public static final video_progress_bg:I = 0x7f020228

.field public static final video_progress_point:I = 0x7f020229

.field public static final video_progress_point_press:I = 0x7f02022a

.field public static final video_progressive_focus:I = 0x7f02022b

.field public static final video_progressive_ing:I = 0x7f02022c

.field public static final video_progressive_press:I = 0x7f02022d

.field public static final video_quickpanel_close_selector:I = 0x7f02022e

.field public static final video_quickpanel_ff_selector:I = 0x7f02022f

.field public static final video_quickpanel_pause_selector:I = 0x7f020230

.field public static final video_quickpanel_play_selector:I = 0x7f020231

.field public static final video_quickpanel_rew_selector:I = 0x7f020232

.field public static final video_scover_player_close_btn:I = 0x7f020233

.field public static final video_tb_allshare_icon_d:I = 0x7f020234

.field public static final video_tb_allshare_icon_displaychange:I = 0x7f020235

.field public static final video_tb_allshare_icon_f:I = 0x7f020236

.field public static final video_tb_allshare_icon_n:I = 0x7f020237

.field public static final video_tb_allshare_icon_p:I = 0x7f020238

.field public static final video_tb_more_icon_f:I = 0x7f020239

.field public static final video_tb_more_icon_n:I = 0x7f02023a

.field public static final video_tb_more_icon_p:I = 0x7f02023b

.field public static final video_tb_mute_icon_f:I = 0x7f02023c

.field public static final video_tb_mute_icon_n:I = 0x7f02023d

.field public static final video_tb_mute_icon_p:I = 0x7f02023e

.field public static final video_tb_progress_bar:I = 0x7f02023f

.field public static final video_tb_view_mode_icon_1_f:I = 0x7f020240

.field public static final video_tb_view_mode_icon_1_n:I = 0x7f020241

.field public static final video_tb_view_mode_icon_1_p:I = 0x7f020242

.field public static final video_tb_view_mode_icon_2_f:I = 0x7f020243

.field public static final video_tb_view_mode_icon_2_n:I = 0x7f020244

.field public static final video_tb_view_mode_icon_2_p:I = 0x7f020245

.field public static final video_tb_view_mode_icon_3_f:I = 0x7f020246

.field public static final video_tb_view_mode_icon_3_n:I = 0x7f020247

.field public static final video_tb_view_mode_icon_3_p:I = 0x7f020248

.field public static final video_tb_view_mode_icon_4_f:I = 0x7f020249

.field public static final video_tb_view_mode_icon_4_n:I = 0x7f02024a

.field public static final video_tb_view_mode_icon_4_p:I = 0x7f02024b

.field public static final video_tb_volume_icon_f:I = 0x7f02024c

.field public static final video_tb_volume_icon_n:I = 0x7f02024d

.field public static final video_tb_volume_icon_p:I = 0x7f02024e

.field public static final video_view_lock_f:I = 0x7f02024f

.field public static final video_view_lock_n:I = 0x7f020250

.field public static final video_view_lock_p:I = 0x7f020251

.field public static final video_view_mode_01_f:I = 0x7f020252

.field public static final video_view_mode_01_n:I = 0x7f020253

.field public static final video_view_mode_01_p:I = 0x7f020254

.field public static final video_view_mode_02_f:I = 0x7f020255

.field public static final video_view_mode_02_n:I = 0x7f020256

.field public static final video_view_mode_02_p:I = 0x7f020257

.field public static final video_view_mode_03_f:I = 0x7f020258

.field public static final video_view_mode_03_n:I = 0x7f020259

.field public static final video_view_mode_03_p:I = 0x7f02025a

.field public static final video_view_mode_04_f:I = 0x7f02025b

.field public static final video_view_mode_04_n:I = 0x7f02025c

.field public static final video_view_mode_04_p:I = 0x7f02025d

.field public static final video_view_more_f:I = 0x7f02025e

.field public static final video_view_more_n:I = 0x7f02025f

.field public static final video_view_more_p:I = 0x7f020260

.field public static final video_volume_bar:I = 0x7f020261

.field public static final video_volume_bg:I = 0x7f020262

.field public static final video_volume_brightness_progress_bg3:I = 0x7f020263

.field public static final video_volume_popup_bg:I = 0x7f020264

.field public static final video_volume_popup_progress_bar:I = 0x7f020265

.field public static final video_volume_popup_progress_bg:I = 0x7f020266

.field public static final video_volume_sa_dim:I = 0x7f020267

.field public static final video_volume_sa_focus:I = 0x7f020268

.field public static final video_volume_sa_normal:I = 0x7f020269

.field public static final video_volume_sa_press:I = 0x7f02026a

.field public static final video_widget_bg_l:I = 0x7f02026b

.field public static final video_widget_bg_l_v:I = 0x7f02026c

.field public static final video_widget_bg_p_v:I = 0x7f02026d

.field public static final video_widget_bg_v:I = 0x7f02026e

.field public static final videoplayer_hdmi_default_img_01:I = 0x7f02026f

.field public static final videoplayer_point_seekbar:I = 0x7f020270

.field public static final videoplayer_point_seekbar_dim:I = 0x7f020271

.field public static final videoplayer_progressbar:I = 0x7f020272

.field public static final videoplayer_seek_thumb:I = 0x7f020273

.field public static final videoplayer_subview_progressbar:I = 0x7f020274

.field public static final videoplayer_subview_seek_thumb:I = 0x7f020275

.field public static final videoplayer_volume_point:I = 0x7f020276

.field public static final videowidget_magazine_progressbar:I = 0x7f020277

.field public static final videowidget_popup_progressbar:I = 0x7f020278

.field public static final videowidget_progressbar:I = 0x7f020279

.field public static final widget_add_icon:I = 0x7f02027a

.field public static final widget_innerline:I = 0x7f02027b

.field public static final widget_shadow:I = 0x7f02027c

.field public static final widget_video_2x1_progress_bar:I = 0x7f02027d

.field public static final widget_video_2x1_progress_bg:I = 0x7f02027e

.field public static final widget_video_btn_play:I = 0x7f02027f

.field public static final widget_video_default_bg:I = 0x7f020280

.field public static final widget_video_preview:I = 0x7f020281

.field public static final widget_video_shadow:I = 0x7f020282

.field public static final widget_video_time_bar:I = 0x7f020283


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1030
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
