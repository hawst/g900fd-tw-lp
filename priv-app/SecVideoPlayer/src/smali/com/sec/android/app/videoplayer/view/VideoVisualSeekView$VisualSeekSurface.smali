.class public Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;
.super Landroid/view/SurfaceView;
.source "VideoVisualSeekView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "VisualSeekSurface"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 323
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .line 324
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 325
    return-void
.end method


# virtual methods
.method public getDefaultsize(II)I
    .locals 3
    .param p1, "size"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 409
    move v0, p1

    .line 410
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 411
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 413
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 422
    :goto_0
    return v0

    .line 415
    :sswitch_0
    move v0, p1

    .line 416
    goto :goto_0

    .line 419
    :sswitch_1
    move v0, v2

    goto :goto_0

    .line 413
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 328
    const/4 v4, 0x0

    .line 329
    .local v4, "mVideoWidth":I
    const/4 v3, 0x0

    .line 331
    .local v3, "mVideoHeight":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    .line 333
    .local v5, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v4

    .line 334
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v3

    .line 336
    invoke-static {v4, p1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;->getDefaultSize(II)I

    move-result v6

    .line 337
    .local v6, "width":I
    invoke-static {v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;->getDefaultSize(II)I

    move-result v0

    .line 339
    .local v0, "height":I
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onMeasure() - param size: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " x "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onMeasure() - real size: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " x "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onMeasure() - surfaceWidth : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", surfaceHeight : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    if-lez v4, :cond_0

    if-lez v3, :cond_0

    .line 344
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    .line 345
    .local v7, "widthSpecMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 346
    .local v8, "widthSpecSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 347
    .local v1, "heightSpecMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 349
    .local v2, "heightSpecSize":I
    const/high16 v9, 0x40000000    # 2.0f

    if-ne v7, v9, :cond_2

    const/high16 v9, 0x40000000    # 2.0f

    if-ne v1, v9, :cond_2

    .line 351
    move v6, v8

    .line 352
    move v0, v2

    .line 355
    mul-int v9, v4, v0

    mul-int v10, v6, v3

    if-ge v9, v10, :cond_1

    .line 357
    mul-int v9, v0, v4

    div-int v6, v9, v3

    .line 400
    .end local v1    # "heightSpecMode":I
    .end local v2    # "heightSpecSize":I
    .end local v7    # "widthSpecMode":I
    .end local v8    # "widthSpecSize":I
    :cond_0
    :goto_0
    invoke-virtual {p0, v6, v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;->setMeasuredDimension(II)V

    .line 402
    return-void

    .line 358
    .restart local v1    # "heightSpecMode":I
    .restart local v2    # "heightSpecSize":I
    .restart local v7    # "widthSpecMode":I
    .restart local v8    # "widthSpecSize":I
    :cond_1
    mul-int v9, v4, v0

    mul-int v10, v6, v3

    if-le v9, v10, :cond_0

    .line 360
    mul-int v9, v6, v3

    div-int v0, v9, v4

    goto :goto_0

    .line 362
    :cond_2
    const/high16 v9, 0x40000000    # 2.0f

    if-ne v7, v9, :cond_3

    .line 365
    move v6, v8

    .line 366
    mul-int v9, v6, v3

    div-int v0, v9, v4

    .line 367
    const/high16 v9, -0x80000000

    if-ne v1, v9, :cond_0

    if-le v0, v2, :cond_0

    .line 369
    move v0, v2

    goto :goto_0

    .line 371
    :cond_3
    const/high16 v9, 0x40000000    # 2.0f

    if-ne v1, v9, :cond_4

    .line 374
    move v0, v2

    .line 375
    mul-int v9, v0, v4

    div-int v6, v9, v3

    .line 376
    const/high16 v9, -0x80000000

    if-ne v7, v9, :cond_0

    if-le v6, v8, :cond_0

    .line 378
    move v6, v8

    goto :goto_0

    .line 383
    :cond_4
    move v6, v4

    .line 384
    move v0, v3

    .line 385
    const/high16 v9, -0x80000000

    if-ne v1, v9, :cond_5

    if-le v0, v2, :cond_5

    .line 387
    move v0, v2

    .line 388
    mul-int v9, v0, v4

    div-int v6, v9, v3

    .line 390
    :cond_5
    const/high16 v9, -0x80000000

    if-ne v7, v9, :cond_0

    if-le v6, v8, :cond_0

    .line 392
    move v6, v8

    .line 393
    mul-int v9, v6, v3

    div-int v0, v9, v4

    goto :goto_0
.end method

.method public resolveAdjustedSize(II)I
    .locals 1
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 405
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;->getDefaultsize(II)I

    move-result v0

    return v0
.end method
