.class public Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;
.super Ljava/lang/Object;
.source "WfdObserver.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static sWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->sWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-class v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->TAG:Ljava/lang/String;

    .line 17
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mContext:Landroid/content/Context;

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->sWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    return-object v0
.end method


# virtual methods
.method public remove()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->deleteObserver(Ljava/util/Observer;)V

    .line 37
    return-void
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mContext:Landroid/content/Context;

    .line 30
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->addObserver(Ljava/util/Observer;)V

    .line 32
    sget-object v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->sWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    return-object v0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 41
    check-cast p2, [I

    .end local p2    # "data":Ljava/lang/Object;
    move-object v0, p2

    check-cast v0, [I

    .line 43
    .local v0, "msg":[I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update() - msg[0]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", msg[1]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget v3, v0, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    aget v1, v0, v4

    packed-switch v1, :pswitch_data_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 47
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->setContext(Landroid/content/Context;)V

    .line 49
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->show()V

    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v1, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    goto :goto_0

    .line 45
    :pswitch_data_0
    .packed-switch 0x320
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
