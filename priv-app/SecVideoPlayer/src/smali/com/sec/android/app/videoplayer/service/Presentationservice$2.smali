.class Lcom/sec/android/app/videoplayer/service/Presentationservice$2;
.super Landroid/app/Presentation;
.source "Presentationservice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/service/Presentationservice;->show(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;Landroid/content/Context;Landroid/view/Display;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/view/Display;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-direct {p0, p2, p3}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    return-void
.end method


# virtual methods
.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 269
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 270
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 277
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 279
    .local v0, "action":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$200(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$200(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mGestureDetector.onTouchEvent :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    :cond_0
    packed-switch v0, :pswitch_data_0

    .line 355
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Presentation;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1

    .line 285
    :pswitch_0
    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent ACTION_UP "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 294
    :pswitch_1
    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent ACTION_DOWN "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    # setter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mXTouchPos:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$302(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I

    .line 296
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    # setter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDownYPos:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$502(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I

    move-result v2

    # setter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mYTouchPos:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$402(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I

    .line 298
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$600(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 299
    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onTouchEvent : mVideoExtScreenController is Not Null "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$600(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$600(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->hideController()V

    goto :goto_0

    .line 303
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$600(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    move-result-object v1

    const/16 v2, 0x1770

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0

    .line 311
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDownYPos:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$500(Lcom/sec/android/app/videoplayer/service/Presentationservice;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080089

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_1

    .line 343
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    # setter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mXTouchPos:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$302(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I

    .line 344
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    # setter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->mYTouchPos:I
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$402(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I

    goto/16 :goto_0

    .line 283
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
