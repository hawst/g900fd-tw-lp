.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;
.super Ljava/lang/Object;
.source "SubtitleSyncPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 311
    sparse-switch p2, :sswitch_data_0

    .line 347
    :cond_0
    :goto_0
    return v2

    .line 314
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 315
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->hide()V

    .line 317
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 319
    .local v0, "pressTime":J
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 320
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string v5, "videoplayer.set.lock"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 327
    .end local v0    # "pressTime":J
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 328
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->hide()V

    .line 329
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->onOrientationChanged()V

    .line 330
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->show()V

    .line 331
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->pause()V

    goto :goto_0

    .line 344
    :sswitch_2
    const/4 v2, 0x0

    goto :goto_0

    .line 311
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
