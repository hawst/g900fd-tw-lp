.class Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;
.super Ljava/lang/Object;
.source "SideChapterViewDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->setView(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 275
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 276
    .local v1, "id":I
    const/4 v2, 0x0

    .line 278
    .local v2, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$200()[I

    move-result-object v3

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 279
    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->viewIdList:[I
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$200()[I

    move-result-object v3

    aget v3, v3, v0

    if-ne v3, v1, :cond_0

    .line 280
    move v2, v0

    .line 278
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_1
    const-string v3, "videowall-SideChapterViewDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClick() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", duration : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v3, "videowall-SideChapterViewDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start time : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v5

    mul-int/2addr v5, v2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v6

    div-int/2addr v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->position:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$500(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v4

    aget-object v3, v3, v4

    const-string v4, "#ff000000"

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->mll:[Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$100(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[Landroid/widget/LinearLayout;

    move-result-object v3

    aget-object v3, v3, v2

    const v4, 0x7f020048

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 287
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v3

    aget v3, v3, v2

    if-nez v3, :cond_2

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->duration:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$300(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v4

    mul-int/2addr v4, v2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->num:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$400(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)I

    move-result v5

    div-int/2addr v4, v5

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->callMoviePlayerByTime(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)V

    .line 292
    :goto_1
    return-void

    .line 290
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog$2;->this$0:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->keyframe_time:[I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$600(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;)[I

    move-result-object v4

    aget v4, v4, v2

    # invokes: Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->callMoviePlayerByTime(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->access$700(Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;I)V

    goto :goto_1
.end method
