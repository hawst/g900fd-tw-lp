.class public Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;
.super Ljava/lang/Object;
.source "DrmKDRMInfo.java"


# static fields
.field public static final DRM_TPE_INVALID_TERUTEN:I = 0x4

.field public static final DRM_TYPE_MAX:I = 0x5

.field public static final DRM_TYPE_NED:I = 0x2

.field public static final DRM_TYPE_NOMEDIA:I = 0x3

.field public static final DRM_TYPE_NORMAL:I = 0x0

.field public static final DRM_TYPE_TERUTEN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DrmKDRMInfo"

.field private static bLmsStart:Z


# instance fields
.field public mAvailCount:I

.field private mDisableCapture:Z

.field private mDisableTVOut:Z

.field private mDrmPlaybackStatus:I

.field private mDrmType:I

.field private mTotalCount:I

.field private mWatermark:[B

.field private mWatermarkFlag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    .line 125
    const-string v0, "kordrm-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->nativeInit()V

    .line 127
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDisableTVOut:Z

    .line 8
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDisableCapture:Z

    .line 9
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mWatermarkFlag:Z

    .line 11
    const/16 v0, 0x80

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mWatermark:[B

    .line 12
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mAvailCount:I

    .line 13
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mTotalCount:I

    .line 26
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDrmPlaybackStatus:I

    .line 28
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDrmType:I

    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->openFile(Ljava/lang/String;)V

    .line 32
    return-void
.end method

.method public static native DiscountCall(Ljava/lang/String;)V
.end method

.method public static native LMSClose(I)V
.end method

.method public static native LMSOpen()V
.end method

.method public static native LMSPlay(I)V
.end method

.method public static native LMSStop(I)V
.end method

.method public static LogLmsClose(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 81
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LogLmsClose=====================]] ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 84
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LMSClose(I)V

    .line 85
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    .line 87
    :cond_0
    return-void
.end method

.method public static LogLmsOpen()V
    .locals 2

    .prologue
    .line 73
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    .line 75
    const-string v0, "DrmKDRMInfo"

    const-string v1, "LogLmsOpen[[====================="

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LMSOpen()V

    .line 78
    :cond_0
    return-void
.end method

.method public static LogLmsPlay(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 90
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LMSPlay bLmsStart ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 92
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LMSPlay(I)V

    .line 94
    :cond_0
    return-void
.end method

.method public static LogLmsStop(I)V
    .locals 3
    .param p0, "pos"    # I

    .prologue
    .line 97
    const-string v0, "DrmKDRMInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LogLmsStop bLmsStart ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->bLmsStart:Z

    if-eqz v0, :cond_0

    .line 99
    div-int/lit16 v0, p0, 0x3e8

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LMSStop(I)V

    .line 101
    :cond_0
    return-void
.end method

.method private static native nativeInit()V
.end method

.method private native openFile(Ljava/lang/String;)V
.end method


# virtual methods
.method public native checkCID()I
.end method

.method public getAvailableCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mAvailCount:I

    return v0
.end method

.method public getDisableCapture()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDisableCapture:Z

    return v0
.end method

.method public getDisableTVOut()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDisableTVOut:Z

    return v0
.end method

.method public getDrmPlaybackStatus()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDrmPlaybackStatus:I

    return v0
.end method

.method public getDrmType()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mDrmType:I

    return v0
.end method

.method public native getTestCount(Ljava/lang/String;)I
.end method

.method public getTotalCount()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mTotalCount:I

    return v0
.end method

.method public getWatermark()[B
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mWatermark:[B

    return-object v0
.end method

.method public getWatermarkFlag()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->mWatermarkFlag:Z

    return v0
.end method
