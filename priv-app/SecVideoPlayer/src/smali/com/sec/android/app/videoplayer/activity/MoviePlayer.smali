.class public Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.super Landroid/app/Activity;
.source "MoviePlayer.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;
.implements Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;
.implements Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;,
        Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    }
.end annotation


# static fields
.field private static final CHANGE_CONTROLLER_MARGIN_FOR_MULTIWINDOW:I = 0xcf

.field private static final CHECK_EDITOR_PACKAGE:I = 0xd7

.field private static final CHECK_KNOX_MODE:I = 0xd4

.field private static final DRM_AVAILABLE_DISCOUNT:I = 0xcc

.field private static final EXIT_MOVIE_PLAYER:I = 0xca

.field private static final HANDLER_MSG:I = 0xc8

.field private static final HANDLE_SET_SUBTITLE_SYNC_TIME:I = 0xc9

.field private static final HIDE_CONTROLLER:I = 0xd8

.field private static final HIDE_PRESENTATION_VIEW:I = 0xd1

.field private static final MOTION_PANNING_THRESHOLD:J = 0x3L

.field private static final MOTION_PEEK_CHAPTER_PREVIEW_THRESHOLD:J = 0x46L

.field private static final MOVIEPLAYER_EXIT_ON_ERRORPOPUP:I = 0xce

.field private static final RESET_SUBTITLE_SYNC_TIME:I = 0x0

.field private static final SAVE_SUBTITLE_SYNC_TIME:I = 0x1

.field private static final SET_DNIE_SETTINGS:I = 0xd5

.field private static final SHOW_CONTROLLER:I = 0xcb

.field private static final SHOW_HELP_PROGRESSBAR_PREVIEW:I = 0xd0

.field private static final SHOW_PRESENTATION_VIEW:I = 0xd2

.field private static final SIDE_MIRRORING_UPDATE:I = 0xd6

.field private static final TAG:Ljava/lang/String;

.field private static final VOLUME_UPDATE:I = 0xcd

.field public static isCheckedSKTCloudServiceFeature:Z

.field private static mInitialHeadsetAction:Z

.field private static mIsActivityPauseState:Z

.field private static mIsActivityStopState:Z

.field public static mIsConnectedPresentation:Z

.field public static mIsLeaveByUser:Z

.field public static mPresentationResumePosition:J

.field public static mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

.field private static mWatchOnResumeTime:J

.field public static sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

.field private static sSurfaceHolder:Landroid/view/SurfaceHolder;

.field public static sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

.field public static sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;


# instance fields
.field private bLocalReceiverRegistered:Z

.field private bNotAllowedScreenMirroringPopup:Z

.field private bPackageReceiverRegistered:Z

.field private bPlaylistSetCompleted:Z

.field private bScoverclosedWhenOnResume:Z

.field private isTransferToDeviceSelected:Z

.field private mASPUri:Landroid/net/Uri;

.field mAction:I

.field private mAlertmessage:Z

.field private final mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mAutoPlay:Z

.field private final mB2BSoultionReceiver:Landroid/content/BroadcastReceiver;

.field private mBattLevel:I

.field private mBattScale:I

.field private mBattStatus:I

.field private mBezelMgr:Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

.field private mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mBroadcastReceiverExRegistered:Z

.field private final mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

.field public mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

.field private final mChangePlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

.field private mCurrentMode:I

.field private mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

.field private mDirectShare:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

.field private final mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

.field private mDockReceiver:Landroid/content/BroadcastReceiver;

.field private mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

.field private mDrmPopupType:I

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mEditPopup:Landroid/app/AlertDialog;

.field private mErrorPopup:Landroid/app/AlertDialog;

.field private mErrorType:I

.field private mErrorUri:Ljava/lang/String;

.field private mExit:Z

.field private mExitByAppinApp:Z

.field public mExitPlayer:Ljava/lang/Runnable;

.field private mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

.field public mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

.field private mGolbalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mHEVCPopup:Landroid/app/AlertDialog;

.field private final mHandler:Landroid/os/Handler;

.field private mHelpPrevType:Ljava/lang/String;

.field private mIsCoverOpen:Z

.field private mIsPrecessingSideMirroring:Z

.field private mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

.field private mKdrmPopup:Landroid/app/AlertDialog;

.field private mKeycnt:I

.field private mLicErrorRelatedPopupShow:Z

.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

.field private mLocalReceiver:Landroid/content/BroadcastReceiver;

.field public mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

.field private mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

.field public mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private final mMediaReceiver:Landroid/content/BroadcastReceiver;

.field private mMenu:Landroid/view/Menu;

.field private mMenuVisibilityListener:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;

.field private mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

.field private mMotionPanningXSum:I

.field private mMotionPanningYSum:I

.field private final mMultiWindowBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mObserverAutoBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightness:Landroid/database/ContentObserver;

.field private mObserverBrightnessMode:Landroid/database/ContentObserver;

.field private mObserverMWTray:Landroid/database/ContentObserver;

.field private mObserverSmartPause:Landroid/database/ContentObserver;

.field private mOldFolderState:I

.field private mOrientation:I

.field public mOrientationEventListener:Landroid/view/OrientationEventListener;

.field private mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

.field private mPlaySpeedFromAIA:I

.field public mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

.field private mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private final mPresentationHandler:Landroid/os/Handler;

.field private mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

.field private final mPresentationServiceConnection:Landroid/content/ServiceConnection;

.field private mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

.field private mPreviousAppHeight:I

.field private mPreviousAppOrientation:I

.field private mPreviousAppWidth:I

.field private mProgress:Landroid/app/ProgressDialog;

.field private mRegisteredWatchONReceiver:Z

.field private mRootLayout:Landroid/widget/RelativeLayout;

.field private mRotationObserver:Landroid/database/ContentObserver;

.field private mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

.field private final mSPenReceiver:Landroid/content/BroadcastReceiver;

.field public mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mScreenModeObserver:Landroid/database/ContentObserver;

.field private mSecureMode:Z

.field public mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

.field private mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

.field private mShouldBackToList:Z

.field public mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

.field private mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

.field private final mSlinkBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

.field public mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

.field private final mStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mStopped:Z

.field private mSubEdit:Landroid/view/SubMenu;

.field private mSubShare:Landroid/view/SubMenu;

.field private mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

.field private mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field private mSubtitleViaAIA:Z

.field public mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

.field public mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

.field private final mTrimReceiver:Landroid/content/BroadcastReceiver;

.field private mTutorialCheck:Z

.field private mUserRotationObserver:Landroid/database/ContentObserver;

.field private mVideoActivityCreated:Z

.field private mVideoActivityOnResume:Z

.field public mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

.field private mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

.field public mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

.field private mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mVto:Landroid/view/ViewTreeObserver;

.field private mWatchOnReceiver:Landroid/content/BroadcastReceiver;

.field private mWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

.field private mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 193
    const-class v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    .line 242
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 243
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 244
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 245
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 254
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    .line 332
    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    .line 360
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z

    .line 361
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    .line 362
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z

    .line 363
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    .line 364
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    .line 369
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationResumePosition:J

    .line 6538
    sput-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isCheckedSKTCloudServiceFeature:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x64

    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 191
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 195
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;

    .line 196
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHelpPrevType:Ljava/lang/String;

    .line 197
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mASPUri:Landroid/net/Uri;

    .line 199
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    .line 203
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    .line 204
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .line 205
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .line 206
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    .line 207
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 208
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 209
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    .line 210
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .line 211
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .line 213
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    .line 215
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .line 216
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 217
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .line 218
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 219
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    .line 222
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    .line 223
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    .line 224
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubEdit:Landroid/view/SubMenu;

    .line 226
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    .line 227
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    .line 228
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 229
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 230
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 231
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverMWTray:Landroid/database/ContentObserver;

    .line 232
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    .line 233
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    .line 235
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 236
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 237
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 238
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 239
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 240
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 247
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 248
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 249
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 250
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 251
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 252
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .line 253
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .line 255
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 257
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 258
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    .line 260
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 261
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 263
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 264
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    .line 266
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 267
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    .line 269
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 270
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 278
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 279
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 313
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z

    .line 318
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopupType:I

    .line 319
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    .line 320
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I

    .line 321
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I

    .line 322
    iput v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I

    .line 323
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    .line 334
    iput v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I

    .line 336
    iput v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I

    .line 338
    iput v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I

    .line 339
    iput v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I

    .line 340
    iput v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I

    .line 343
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityCreated:Z

    .line 345
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExit:Z

    .line 346
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    .line 347
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z

    .line 348
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBroadcastReceiverExRegistered:Z

    .line 349
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRegisteredWatchONReceiver:Z

    .line 350
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    .line 351
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTutorialCheck:Z

    .line 352
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    .line 353
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    .line 354
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z

    .line 355
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    .line 356
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    .line 357
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsCoverOpen:Z

    .line 371
    iput v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I

    .line 374
    iput v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCurrentMode:I

    .line 377
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBezelMgr:Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    .line 381
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bLocalReceiverRegistered:Z

    .line 384
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPackageReceiverRegistered:Z

    .line 386
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 387
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I

    .line 389
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPlaylistSetCompleted:Z

    .line 390
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bNotAllowedScreenMirroringPopup:Z

    .line 393
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bScoverclosedWhenOnResume:Z

    .line 1554
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$5;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mGolbalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 3277
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$15;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDockReceiver:Landroid/content/BroadcastReceiver;

    .line 3310
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$16;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnReceiver:Landroid/content/BroadcastReceiver;

    .line 3621
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$17;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$17;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mB2BSoultionReceiver:Landroid/content/BroadcastReceiver;

    .line 3645
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$18;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    .line 3727
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    .line 3756
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$20;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMultiWindowBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 3797
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$21;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mChangePlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 3826
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$22;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTrimReceiver:Landroid/content/BroadcastReceiver;

    .line 3845
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$23;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSlinkBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 3860
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;

    .line 3901
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$25;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    .line 4170
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAction:I

    .line 4577
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 4898
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 4913
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$30;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$30;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 5153
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$36;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$36;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    .line 5197
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$37;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$37;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitPlayer:Ljava/lang/Runnable;

    .line 5352
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$41;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$41;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 5668
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$42;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$42;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 6579
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$51;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    .line 6751
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationServiceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method private PrecessingSideMirroring()V
    .locals 5

    .prologue
    const/16 v4, 0xd6

    .line 5860
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z

    .line 5861
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 5862
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "sendEmptyMessageDelayed() SIDE_MIRRORING_UPDATE."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5863
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 5864
    return-void
.end method

.method private StartMoviPlay4Presentation()V
    .locals 2

    .prologue
    .line 6702
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setServiceContext()V

    .line 6704
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveActivityPreferences()V

    .line 6706
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 6707
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 6709
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 6710
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 6713
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    .line 6715
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSelectedbyChangePlayer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6716
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 6717
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSelectedbyChangePlayer(Z)V

    .line 6728
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 6731
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateChangePlayerButton()V

    .line 6732
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getActivityPreferences()V

    .line 6733
    return-void
.end method

.method private StartSecondaryDisplay(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "showPresentation"    # Ljava/lang/Boolean;

    .prologue
    .line 6736
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "StartSecondaryDisplay"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6738
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bindToPresentationService(Landroid/content/ServiceConnection;Ljava/lang/Boolean;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6739
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "PresentationService Connection fail!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6741
    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLayoutForMultiWindow()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppOrientation:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/SubMenu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubEdit:Landroid/view/SubMenu;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createPlayContinueDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeDialogView()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startMoreService()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I

    return p1
.end method

.method static synthetic access$2412(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningXSum:I

    return v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I

    return v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I

    return p1
.end method

.method static synthetic access$2512(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionPanningYSum:I

    return v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isBlockMotionPeek()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->PrecessingSideMirroring()V

    return-void
.end method

.method static synthetic access$2902(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 191
    sput-wide p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    return-wide p0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsCoverOpen:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsCoverOpen:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->resumeOrStartPlaying()V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I

    return p1
.end method

.method static synthetic access$3308(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKeycnt:I

    return v0
.end method

.method static synthetic access$3400()Z
    .locals 1

    .prologue
    .line 191
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object p1
.end method

.method static synthetic access$3600()Z
    .locals 1

    .prologue
    .line 191
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bNotAllowedScreenMirroringPopup:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->dismissShowingPopup()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartMoviPlay4Presentation()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setStaticView()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    return v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I

    return v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I

    return v0
.end method

.method static synthetic access$4702(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I

    return p1
.end method

.method static synthetic access$4800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I

    return v0
.end method

.method static synthetic access$4802(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    return-object v0
.end method

.method static synthetic access$4902(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    return-object p1
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    return-object v0
.end method

.method static synthetic access$5200()Z
    .locals 1

    .prologue
    .line 191
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z

    return v0
.end method

.method static synthetic access$5202(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 191
    sput-boolean p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z

    return p0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z

    return v0
.end method

.method static synthetic access$5302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z

    return p1
.end method

.method static synthetic access$5400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendSideSyncDisableVideoBroadcast()V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSymmetricMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSourceRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # I

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateSortByInfo(I)V

    return-void
.end method

.method static synthetic access$5900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bScoverclosedWhenOnResume:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/popup/IVideoPopup;)Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    return-object p1
.end method

.method static synthetic access$6102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z

    return p1
.end method

.method static synthetic access$6200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->handleErrorPopup()V

    return-void
.end method

.method static synthetic access$6402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    return p1
.end method

.method static synthetic access$6500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->doOnCompleteAction()V

    return-void
.end method

.method static synthetic access$6602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothA2dp;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-object p1
.end method

.method static synthetic access$6700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    return v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isExternalDisplay()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->checkExceptionalCaseDuringDlna()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartSecondaryDisplay(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$7100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/service/IPresentationService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    return-object v0
.end method

.method static synthetic access$7102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/service/IPresentationService;)Lcom/sec/android/app/videoplayer/service/IPresentationService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/SharedPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-object v0
.end method

.method private broadcastToSideSync(Ljava/lang/String;)V
    .locals 9
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    .line 6298
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v4, :cond_0

    .line 6299
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, " broadcastToSideSync() - mServiceUtil is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6350
    :goto_0
    return-void

    .line 6303
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.sidesync.common.MEDIA_SHARE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6304
    .local v2, "intentForSideSync":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 6305
    .local v1, "curPlayingPath":Ljava/lang/String;
    const/4 v3, 0x0

    .line 6306
    .local v3, "subtitlePath":Ljava/lang/String;
    const/4 v0, 0x1

    .line 6308
    .local v0, "canPlay":Z
    const-string v4, "CONTENT"

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 6309
    const-string v4, "SET"

    invoke-virtual {v2, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6310
    const-string v4, "resumePos"

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6311
    const-string v4, "MIMETYPE"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6313
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v4, :cond_1

    .line 6314
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistWatchNowSubtitle()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 6315
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getWatchNowSubtitleFilePath()Ljava/lang/String;

    move-result-object v3

    .line 6321
    :cond_1
    :goto_1
    if-eqz v3, :cond_2

    .line 6322
    const-string v4, "SUBTITLE"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6325
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 6326
    const-string v4, "needSaveResumePos"

    invoke-virtual {v2, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6332
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 6334
    :cond_4
    const/4 v0, 0x0

    .line 6340
    :cond_5
    :goto_3
    const-string v4, "canPlay"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6342
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " broadcastToSideSync() - Uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6343
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " broadcastToSideSync() - mServiceUtil.getResumePosition() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6344
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " broadcastToSideSync() - mimeType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6345
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " broadcastToSideSync() - subtitlePath = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6346
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " broadcastToSideSync() - canPlay = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6348
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 6349
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateVideoWidget()V

    goto/16 :goto_0

    .line 6316
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getTitleFileName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistOutbandSubtitle(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 6317
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 6328
    :cond_7
    const-string v4, "needSaveResumePos"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_2

    .line 6335
    :cond_8
    if-eqz v1, :cond_5

    .line 6336
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 6337
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method private canPlayUsingSideSync()Z
    .locals 5

    .prologue
    .line 6267
    const/4 v0, 0x0

    .line 6269
    .local v0, "canPlay":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_0

    .line 6270
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v1

    .line 6271
    .local v1, "keyType":I
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " canPlayUsingSideSync() - keyType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6273
    packed-switch v1, :pswitch_data_0

    .line 6289
    .end local v1    # "keyType":I
    :cond_0
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 6290
    const/4 v0, 0x0

    .line 6293
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " canPlayUsingSideSync() - canPlay = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6294
    return v0

    .line 6284
    .restart local v1    # "keyType":I
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 6273
    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private changeDialogView()V
    .locals 1

    .prologue
    .line 3469
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v0, :cond_0

    .line 3470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3471
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->chapterviewPause()V

    .line 3472
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->chapterviewResume()V

    .line 3476
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_1

    .line 3477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v0, :cond_1

    .line 3478
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->onOrientationChanged()V

    .line 3482
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3483
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->onOrientationChanged()V

    .line 3486
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v0, :cond_3

    .line 3487
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v0, :cond_3

    .line 3488
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->onSensorChanged()V

    .line 3492
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->changeView()V

    .line 3493
    return-void
.end method

.method private changeLockStatus(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 6867
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 6868
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->changeLockStatus(Z)V

    .line 6870
    :cond_0
    return-void
.end method

.method private checkExceptionalCaseDuringDlna()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 6562
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    .line 6564
    .local v0, "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 6565
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v2, :cond_0

    .line 6566
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v1, v4, v5, v1}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->changePlayer(IJZ)V

    .line 6568
    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6569
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    const/16 v2, 0x2eb

    const/16 v3, 0x2ee

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->notify(II)V

    .line 6573
    :cond_0
    const/4 v1, 0x1

    .line 6575
    :cond_1
    return v1
.end method

.method private createErrorDialog(ILjava/lang/String;)V
    .locals 7
    .param p1, "action"    # I
    .param p2, "uriStr"    # Ljava/lang/String;

    .prologue
    const v6, 0x104000a

    const/4 v5, 0x1

    .line 4965
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createErrorDialog(action, intent). action = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4967
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$31;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$31;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 4973
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I

    .line 4974
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;

    .line 4976
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getErrorStringResID(I)I

    move-result v1

    .line 4977
    .local v1, "resId":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 4978
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "createErrorDialog : Error type unknown don\'t make dialog!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5136
    :goto_0
    return-void

    .line 4982
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 4991
    :goto_1
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 4993
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 4994
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4996
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v2, :cond_1

    .line 4997
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v3, "autoPlay"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    .line 5000
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5007
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v2, :cond_3

    .line 5008
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dismissAllHoveringPopup()V

    .line 5010
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 4987
    :pswitch_1
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z

    goto :goto_1

    .line 5012
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v2, :cond_5

    .line 5013
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 5015
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto :goto_0

    .line 5020
    :cond_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5021
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a0027

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 5023
    const/16 v2, 0x2714

    if-eq p1, v2, :cond_7

    const/16 v2, 0xc8

    if-eq p1, v2, :cond_7

    const/16 v2, -0x3ed

    if-eq p1, v2, :cond_7

    const/16 v2, -0x17d4

    if-eq p1, v2, :cond_7

    const/16 v2, -0x177b

    if-eq p1, v2, :cond_7

    const/16 v2, -0x177c

    if-eq p1, v2, :cond_7

    const/16 v2, -0x177d

    if-eq p1, v2, :cond_7

    const v2, -0x7fffbff9

    if-eq p1, v2, :cond_7

    const v2, -0x7fffbffa

    if-eq p1, v2, :cond_7

    const/16 v2, -0x3fd

    if-ne p1, v2, :cond_8

    .line 5033
    :cond_7
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$32;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$32;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5086
    :goto_2
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 5124
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$35;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$35;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 5132
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    .line 5133
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAltermessageDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 5134
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 5135
    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    goto/16 :goto_0

    .line 5042
    :cond_8
    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v6, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 4982
    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private createPlayContinueDialog()V
    .locals 3

    .prologue
    .line 2000
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 2001
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 2003
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2004
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2005
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2006
    const v1, 0x7f0a00dd

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$8;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$8;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2014
    const v1, 0x7f0a0026

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$9;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$9;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2021
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    .line 2022
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2023
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2024
    return-void
.end method

.method private dismissDialogPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5359
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "=== dismissDialogPopup , mDrmPopup:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 5363
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iput-object v3, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 5366
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->dismiss()V

    .line 5370
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 5371
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 5374
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 5375
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->dismiss()V

    .line 5378
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v0, :cond_4

    .line 5379
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismiss()V

    .line 5382
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_5

    .line 5383
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 5386
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v0, :cond_6

    .line 5387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 5390
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    if-eqz v0, :cond_7

    .line 5391
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->dismiss()V

    .line 5394
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    if-eqz v0, :cond_8

    .line 5395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->dismiss()V

    .line 5398
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_9

    .line 5399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dismissPlaySpeed()V

    .line 5402
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    if-eqz v0, :cond_a

    .line 5403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->dismiss()V

    .line 5404
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 5407
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    if-eqz v0, :cond_b

    .line 5408
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->dismiss()V

    .line 5409
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .line 5412
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 5413
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismissSyncPopup()V

    .line 5416
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 5417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 5418
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 5421
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_e

    .line 5422
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 5423
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 5426
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_f

    .line 5427
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->dismissPlaySpeed()V

    .line 5430
    :cond_f
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->dismiss()V

    .line 5432
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    .line 5433
    return-void
.end method

.method private dismissShowingPopup()V
    .locals 3

    .prologue
    .line 5562
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "dismissShowingPopup()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5564
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->isPopupShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5565
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismiss()V

    .line 5568
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5569
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSettingsSubPopup() getID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v2}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->getID()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5570
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 5572
    :cond_1
    return-void
.end method

.method private doOnCompleteAction()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 4506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 4507
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setProgressMax()V

    .line 4510
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->resetPlaySpeed()V

    .line 4511
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 4512
    sput-boolean v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 4514
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v0, :cond_1

    .line 4515
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "autoPlay"

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    .line 4518
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    .line 4520
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSvcNotification(). PLAYBACK_COMPLETE - mAutoPlay : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ,mAlertmessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSchemeType - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4524
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    if-nez v0, :cond_7

    .line 4532
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 4536
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_5

    .line 4537
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 4540
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_6

    .line 4541
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dismissAllHoveringPopup()V

    .line 4544
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 4545
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 4546
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 4553
    :cond_7
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_8

    .line 4554
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    .line 4556
    :cond_8
    return-void

    .line 4549
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto :goto_0
.end method

.method private exitApp()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3527
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "exitApp E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3530
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-eq v0, v3, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-ne v0, v3, :cond_8

    .line 3532
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3533
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 3536
    :cond_2
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v0, :cond_3

    .line 3537
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    .line 3540
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopMoviPlay4Presentation()V

    .line 3541
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopSecondaryDisplay()V

    .line 3543
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_4

    .line 3544
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 3553
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3554
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3555
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 3558
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_7

    .line 3559
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dismissAllHoveringPopup()V

    .line 3562
    :cond_7
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    .line 3564
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityCreated:Z

    if-nez v0, :cond_9

    .line 3565
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "exitApp. Video Activity not created."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3595
    :goto_1
    return-void

    .line 3546
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isASFConnected()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3547
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v0, :cond_4

    .line 3548
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    goto :goto_0

    .line 3569
    :cond_9
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    .line 3571
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_a

    .line 3572
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->deleteRemoteSubTitleFile()V

    .line 3575
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_c

    .line 3576
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 3577
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 3578
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 3580
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v0

    if-nez v0, :cond_c

    .line 3581
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 3582
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->release()V

    .line 3586
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 3587
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_d

    .line 3588
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChangeViewDone(Z)V

    .line 3591
    :cond_d
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V

    .line 3594
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->finish()V

    goto :goto_1
.end method

.method private getActivityPreferences()V
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateVersion()V

    .line 727
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_1

    .line 728
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitleSetting()V

    .line 730
    :cond_1
    return-void
.end method

.method private getMenuArray(Landroid/view/SubMenu;)[Ljava/lang/CharSequence;
    .locals 8
    .param p1, "menu"    # Landroid/view/SubMenu;

    .prologue
    .line 1935
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1936
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mList:Ljava/util/ArrayList;

    .line 1938
    invoke-interface {p1}, Landroid/view/SubMenu;->size()I

    move-result v3

    .line 1939
    .local v3, "size":I
    const/4 v1, 0x0

    .line 1941
    .local v1, "item":Landroid/view/MenuItem;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_3

    .line 1942
    invoke-interface {p1, v0}, Landroid/view/SubMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1944
    if-eqz v1, :cond_1

    .line 1945
    const/4 v4, 0x0

    .line 1946
    .local v4, "visible":Z
    invoke-interface {v1}, Landroid/view/MenuItem;->getGroupId()I

    move-result v5

    const v6, 0x7f0d0214

    if-ne v5, v6, :cond_2

    .line 1947
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v5

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkEditMenuVisible(I)Z

    move-result v4

    .line 1952
    :goto_1
    if-eqz v4, :cond_0

    .line 1953
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mList:Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1954
    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1955
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getMenuArray - list.add : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1957
    :cond_0
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1941
    .end local v4    # "visible":Z
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1949
    .restart local v4    # "visible":Z
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v5

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->checkMenuVisible(I)Z

    move-result v4

    goto :goto_1

    .line 1960
    .end local v4    # "visible":Z
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/CharSequence;

    return-object v5
.end method

.method private getUriFromAllShareIntent()Landroid/net/Uri;
    .locals 7

    .prologue
    .line 6461
    const/4 v3, 0x0

    .line 6462
    .local v3, "uri":Landroid/net/Uri;
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v4, :cond_0

    .line 6463
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.sec.android.allshare.intent.extra.ITEM"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    .line 6464
    .local v1, "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.sec.android.allshare.intent.extra.ALLSHARE_ENABLED"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 6466
    .local v0, "enabled":Z
    if-eqz v1, :cond_0

    .line 6467
    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    .line 6468
    invoke-virtual {v1}, Lcom/samsung/android/allshare/Item;->getSubtitle()Landroid/net/Uri;

    move-result-object v2

    .line 6469
    .local v2, "tempUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v4, :cond_0

    .line 6470
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4, v1, v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setInfoFromAllShareIntent(Lcom/samsung/android/allshare/Item;Z)V

    .line 6471
    if-eqz v2, :cond_0

    .line 6472
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setAllShareIntentSubtitle(Landroid/net/Uri;)V

    .line 6477
    .end local v0    # "enabled":Z
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    .end local v2    # "tempUri":Landroid/net/Uri;
    :cond_0
    return-object v3
.end method

.method private handleErrorPopup()V
    .locals 3

    .prologue
    const/16 v2, 0xca

    .line 5143
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5144
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I

    const/16 v1, -0x3f2

    if-ne v0, v1, :cond_0

    .line 5145
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleErrorPopup. Nearby device contents mErrorType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5151
    :goto_0
    return-void

    .line 5149
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 5150
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private init(Landroid/content/Intent;)V
    .locals 44
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 740
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 741
    .local v4, "action":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v32

    .line 742
    .local v32, "type":Ljava/lang/String;
    const/16 v34, 0x0

    .line 743
    .local v34, "uri":Landroid/net/Uri;
    const-wide/16 v36, -0x1

    .line 744
    .local v36, "videoID":J
    const/16 v18, -0x1

    .line 745
    .local v18, "keyType":I
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v10

    .line 746
    .local v10, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->initInfo()V

    .line 749
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v39, v0

    if-eqz v39, :cond_0

    .line 750
    const/16 v39, 0x0

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 753
    :cond_0
    new-instance v39, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    invoke-direct/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    .line 754
    const/16 v39, 0x0

    invoke-static/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setFromSideSync(Z)V

    .line 755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v39, v0

    invoke-static/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setSideSyncInfo(Lcom/sec/android/app/videoplayer/data/SideSyncInfo;)V

    .line 758
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->resetPlaySpeed()V

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_1

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, v39

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bAsfError:Z

    .line 766
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isAllShareIntent()Z

    move-result v39

    if-eqz v39, :cond_6

    .line 767
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getUriFromAllShareIntent()Landroid/net/Uri;

    move-result-object v34

    .line 768
    if-nez v34, :cond_3

    .line 769
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - AllShare Intent URI is NULL."

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    .line 1222
    :cond_2
    :goto_0
    return-void

    .line 773
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setStateAllShareIntent(Z)V

    .line 779
    :goto_1
    const-string v39, "android.intent.action.SECURE_LOCK"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_4

    .line 780
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "ACTION_SECURE_LOCK : true"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 781
    const-string v39, "android.intent.extra.STREAM"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 782
    .local v5, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v5, :cond_4

    .line 783
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setUriArray(Ljava/util/ArrayList;)V

    .line 788
    .end local v5    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_4
    move-object/from16 v0, v34

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mASPUri:Landroid/net/Uri;

    .line 789
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - action : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ", uri : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v39, :cond_5

    .line 792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_5

    .line 793
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setContentChanged(Z)V

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setDirectDmcMode(Z)V

    .line 796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const-string v40, "CurrentProviderName"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setProvider(Ljava/lang/String;)V

    .line 797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const-string v40, "NIC"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setNIC(Ljava/lang/String;)V

    .line 801
    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v14

    .line 802
    .local v14, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->setData(Landroid/content/Intent;)Z

    move-result v13

    .line 804
    .local v13, "hubContent":Z
    const-string v39, "com.samsung.android.sconnect.action.VIDEO_DMR"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_15

    .line 805
    invoke-static {}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->getInstance()Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;

    move-result-object v27

    .line 806
    .local v27, "sconnect":Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->setInfo(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v39

    if-nez v39, :cond_7

    .line 807
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 775
    .end local v13    # "hubContent":Z
    .end local v14    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .end local v27    # "sconnect":Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v34

    goto/16 :goto_1

    .line 810
    .restart local v13    # "hubContent":Z
    .restart local v14    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .restart local v27    # "sconnect":Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
    :cond_7
    invoke-virtual/range {v27 .. v27}, Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;->getStartUri()Landroid/net/Uri;

    move-result-object v34

    .line 811
    if-nez v34, :cond_8

    .line 812
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 815
    :cond_8
    move-object/from16 v0, v34

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 816
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setListType(I)V

    .line 817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1071
    .end local v27    # "sconnect":Lcom/sec/android/app/videoplayer/sconnect/SConnectMgr;
    :cond_9
    :goto_2
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, " init() - mSideSyncInfo.isSourceRunning() = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1072
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, " init() - mSideSyncInfo.isSinkRunning() = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v39

    if-eqz v39, :cond_41

    .line 1075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->setupSideSyncStream(Landroid/content/Intent;)Z

    move-result v39

    if-eqz v39, :cond_c

    .line 1076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    if-eqz v39, :cond_b

    .line 1077
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSdp()Z

    move-result v39

    if-nez v39, :cond_a

    .line 1078
    const/16 v18, 0x28

    .line 1080
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1083
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitle_for_sidesync"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSideSyncSubtitleFilePath(Ljava/lang/String;)V

    .line 1084
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init()- subtitle_for_sidesync :"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "subtitle_for_sidesync"

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    if-eqz v39, :cond_d

    .line 1098
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setFrom(Landroid/content/Intent;)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setFrom(I)V

    .line 1102
    :cond_d
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v39, :cond_43

    const-string v39, "ActionVideoMinimodeService"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-object/from16 v39, v0

    if-eqz v39, :cond_43

    .line 1103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-object/from16 v39, v0

    const-string v40, "autoPlay"

    const/16 v41, 0x0

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    .line 1107
    :goto_3
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - ACTION_AUTO_PLAY_NEXT: "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    move/from16 v41, v0

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v39

    const-string v40, "autoPlay"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    move/from16 v41, v0

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 1116
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v39, :cond_46

    const-string v39, "ActionVideoMinimodeService"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_46

    .line 1117
    const-string v39, "resumePos"

    const/16 v40, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    .line 1118
    .local v24, "resumePos":I
    const-string v39, "audioTrack"

    const/16 v40, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 1119
    .local v6, "audioTrack":I
    const-string v39, "playSpeed"

    const/16 v40, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    .line 1120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    const-string v40, "ListKey"

    const/16 v41, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1121
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v39

    const-string v40, "bucketid"

    const/16 v41, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setBucketID(I)V

    .line 1122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const-string v40, "ListType"

    const/16 v41, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setListType(I)V

    .line 1124
    const/16 v21, 0x0

    .line 1125
    .local v21, "minimode_uri":Ljava/lang/String;
    const-string v39, "uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 1127
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - minimode_uri = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1128
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - resumePos = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1141
    if-eqz v21, :cond_44

    const-string v39, ""

    move-object/from16 v0, v21

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_44

    .line 1142
    invoke-static/range {v21 .. v21}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 1147
    :goto_4
    const/16 v39, -0x1

    move/from16 v0, v24

    move/from16 v1, v39

    if-eq v0, v1, :cond_e

    .line 1148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v40, v0

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1151
    :cond_e
    const/16 v39, -0x1

    move/from16 v0, v39

    if-eq v6, v0, :cond_f

    .line 1152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setAudioTrack(I)V

    .line 1156
    :cond_f
    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    .line 1158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleSettings()V

    .line 1159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitleActivation"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSamsungSubtitleActivation(Z)V

    .line 1160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitleIsMulti"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setIsMultiSubtitle(Z)V

    .line 1161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitleMultiCount"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setMultiSelectSubtitle(I)V

    .line 1162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitleMultiIndex"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setMultiSelectedSubtitleIndex([I)V

    .line 1163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subttileSyncTime"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 1165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v39

    if-eqz v39, :cond_10

    .line 1166
    const-string v39, "subtitleFile"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1168
    .local v31, "subtitleFile":Ljava/lang/String;
    if-eqz v31, :cond_45

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->isEmpty()Z

    move-result v39

    if-nez v39, :cond_45

    .line 1169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkExistOutbandSubtitle(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_10

    .line 1170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 1171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "subtitleLanguageIdx"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleLanguageIndex(I)V

    .line 1172
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    .line 1196
    .end local v6    # "audioTrack":I
    .end local v21    # "minimode_uri":Ljava/lang/String;
    .end local v24    # "resumePos":I
    .end local v31    # "subtitleFile":Ljava/lang/String;
    :cond_10
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromDms()Z

    move-result v39

    if-eqz v39, :cond_11

    .line 1197
    const-string v39, "title_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setDmsContentTitle(Ljava/lang/String;)V

    .line 1200
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    if-eqz v39, :cond_49

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getFrom()I

    move-result v39

    const/16 v40, 0xc8

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_49

    const/16 v17, 0x1

    .line 1202
    .local v17, "isFromWidget":Z
    :goto_6
    if-eqz v17, :cond_12

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z

    move-result v39

    if-eqz v39, :cond_12

    .line 1203
    new-instance v16, Landroid/content/Intent;

    invoke-direct/range {v16 .. v16}, Landroid/content/Intent;-><init>()V

    .line 1204
    .local v16, "i":Landroid/content/Intent;
    const-string v39, "AppInAppResumePositionRequest"

    move-object/from16 v0, v16

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1205
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 1208
    .end local v16    # "i":Landroid/content/Intent;
    :cond_12
    if-nez v13, :cond_4a

    if-nez v17, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    if-eqz v39, :cond_4a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v39

    if-nez v39, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v39

    if-nez v39, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v39

    if-eqz v39, :cond_4a

    .line 1209
    :cond_13
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    .line 1213
    :goto_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v39

    if-eqz v39, :cond_14

    .line 1214
    const/16 v39, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V

    .line 1217
    :cond_14
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v39, :cond_2

    .line 1218
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - getID : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v42

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1219
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - mResumePosition = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v42

    move-object/from16 v0, v40

    move-wide/from16 v1, v42

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - mKeyType : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v41, v0

    invoke-virtual/range {v41 .. v41}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getKeyType()I

    move-result v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 818
    .end local v17    # "isFromWidget":Z
    :cond_15
    if-eqz v34, :cond_38

    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/String;->length()I

    move-result v39

    if-lez v39, :cond_38

    .line 819
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v35

    .line 820
    .local v35, "uri2string":Ljava/lang/String;
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v26

    .line 821
    .local v26, "scheme":Ljava/lang/String;
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v28

    .line 822
    .local v28, "sdpPath":Ljava/lang/String;
    const/16 v30, 0x0

    .line 824
    .local v30, "storyItemId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_16

    .line 825
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const-wide/16 v40, -0x1

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 826
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x2

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 829
    :cond_16
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v39 .. v39}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 830
    .local v9, "extension":Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v39

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 832
    .local v20, "mimeType":Ljava/lang/String;
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - scheme : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, "  uri2string = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 833
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - mimeType : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    const-string v39, "android.intent.action.START_SLINK_PLAYBACK"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_1b

    .line 836
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->init(Landroid/app/Activity;)V

    .line 837
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v39

    move-object/from16 v0, v39

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v35

    .line 838
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSchemeType()I

    move-result v18

    .line 840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_17

    .line 841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/app/videoplayer/slink/SLink;->getResumePosition()J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 843
    :cond_17
    const-string v39, "DMC_MODE"

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v39

    if-eqz v39, :cond_18

    .line 844
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x1

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setDirectDmcMode(Z)V

    .line 988
    :cond_18
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_19

    .line 991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const/16 v40, -0x1

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setListType(I)V

    .line 994
    :cond_19
    invoke-static/range {v35 .. v35}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 996
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v39

    if-eqz v39, :cond_9

    .line 998
    const-string v39, "content://mms/part"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_1a

    const-string v39, "content://security_mms/part"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_9

    .line 1000
    :cond_1a
    const/16 v39, 0x1

    move/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setMmsContents(Z)V

    .line 1001
    const-string v39, "title_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setMmsTitleName(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 846
    :cond_1b
    const-string v39, "http"

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_1c

    const-string v39, "https"

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_1c

    const-string v39, "sshttp"

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_21

    .line 847
    :cond_1c
    if-eqz v28, :cond_1e

    const-string v39, "sdp"

    move-object/from16 v0, v39

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_1d

    const-string v39, "SDP"

    move-object/from16 v0, v39

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_1d

    const-string v39, "application/sdp"

    move-object/from16 v0, v39

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_1e

    .line 848
    :cond_1d
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init()- HTTP SDP file playback"

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    const/16 v18, 0x20

    goto/16 :goto_8

    .line 850
    :cond_1e
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isM3U8Content(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_1f

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isHLSMimeType(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_20

    .line 851
    :cond_1f
    const/16 v18, 0x22

    goto/16 :goto_8

    .line 853
    :cond_20
    const/16 v18, 0x1b

    .line 855
    if-eqz v13, :cond_18

    .line 856
    const/16 v18, 0x1d

    .line 857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const-string v40, "caption_file_path"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setWatchNowSubtitleFilePath(Ljava/lang/String;)V

    .line 858
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    move-result v39

    const/high16 v40, 0x100000

    and-int v39, v39, v40

    if-nez v39, :cond_18

    .line 859
    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v24

    .line 860
    .local v24, "resumePos":J
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init. MediaHub/VideoHub. paused_position : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_8

    .line 865
    .end local v24    # "resumePos":J
    :cond_21
    const-string v39, "rtsp"

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_22

    .line 866
    const/16 v18, 0x1c

    goto/16 :goto_8

    .line 867
    :cond_22
    const-string v39, "sstream"

    move-object/from16 v0, v39

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_27

    .line 868
    invoke-static/range {p1 .. p1}, Lsstream/lib/SStreamContentManager;->getStoryItemIdFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v30

    .line 869
    if-eqz v30, :cond_26

    .line 870
    const-string v39, "_"

    move-object/from16 v0, v30

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v33

    .line 871
    .local v33, "undPosition":I
    add-int/lit8 v39, v33, 0x1

    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->length()I

    move-result v40

    move-object/from16 v0, v30

    move/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v30

    .line 873
    const/16 v18, 0x16

    .line 874
    invoke-static/range {v30 .. v30}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v36

    .line 876
    const-wide/16 v40, 0x0

    cmp-long v39, v36, v40

    if-lez v39, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    if-nez v39, :cond_24

    .line 877
    :cond_23
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - SCHEME_CONTENT_EXTERNAL_TYPE(SSTREAM). video id <= 0"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 879
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    if-eqz v39, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriById(J)Landroid/net/Uri;

    move-result-object v39

    if-nez v39, :cond_25

    .line 880
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - SCHEME_CONTENT_EXTERNAL_TYPE(SSTREAM). getUriById is NULL"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 881
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 883
    :cond_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriById(J)Landroid/net/Uri;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_8

    .line 887
    .end local v33    # "undPosition":I
    :cond_26
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - storyItemId is null"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_8

    .line 890
    :cond_27
    sget-object v39, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v39 .. v39}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_2a

    .line 891
    const/16 v22, 0x0

    .line 892
    .local v22, "moviewidget":Ljava/lang/String;
    const-string v39, "intent.tutorial.type"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 895
    if-eqz v22, :cond_29

    const-string v39, ""

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-nez v39, :cond_29

    .line 896
    const-string v39, "intent.tutorial.type"

    move-object/from16 v0, v22

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_28

    .line 897
    const/16 v39, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setTutorialWidget(Z)V

    .line 905
    :goto_9
    const/16 v18, 0x16

    .line 906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    .line 907
    goto/16 :goto_8

    .line 899
    :cond_28
    const/16 v39, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setTutorialWidget(Z)V

    goto :goto_9

    .line 902
    :cond_29
    const/16 v39, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setTutorialWidget(Z)V

    goto :goto_9

    .line 907
    .end local v22    # "moviewidget":Ljava/lang/String;
    :cond_2a
    sget-object v39, Landroid/provider/MediaStore$Video$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v39 .. v39}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_2b

    .line 908
    const/16 v18, 0x15

    .line 909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    goto/16 :goto_8

    .line 910
    :cond_2b
    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB_FILE:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->MEDIAHUB_DOWNLOAD:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB_FILE:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->VIDEOHUB_DOWNLOAD:Ljava/lang/String;

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    const-string v39, "/storage/extSdCard/.samsungmediahub"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    const-string v39, "file:///storage/extSdCard/.samsungmediahub"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    const-string v39, "/storage/extSdCard/.samsungvideohub"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_2c

    const-string v39, "file:///storage/extSdCard/.samsungvideohub"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_30

    .line 920
    :cond_2c
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v35

    .line 922
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isWatchONInstalled()Z

    move-result v39

    sput-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    .line 923
    const/4 v12, 0x0

    .line 925
    .local v12, "fromWatchON":Z
    const-string v39, "Y"

    const-string v40, "PauseAndResume"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v39

    if-eqz v39, :cond_2d

    .line 926
    const/4 v12, 0x1

    .line 929
    :cond_2d
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init. MediaHub/VideoHub. SUPPORT_YOSEMITE_FUCNTION : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    sget-boolean v41, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v40

    const-string v41, ", fromWatchON : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    const/16 v18, 0x1d

    .line 933
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->set(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v15

    .line 935
    .local v15, "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    if-nez v13, :cond_2e

    .line 936
    const/16 v39, 0x1

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getData(Ljava/lang/String;Z)V

    .line 939
    :cond_2e
    invoke-virtual {v15, v12}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->setWatchONEnable(Z)V

    .line 941
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getFlags()I

    move-result v39

    const/high16 v40, 0x100000

    and-int v39, v39, v40

    if-nez v39, :cond_2f

    .line 942
    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v24

    .line 943
    .restart local v24    # "resumePos":J
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init. MediaHub/VideoHub. paused_position : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_8

    .line 946
    .end local v24    # "resumePos":J
    :cond_2f
    const/16 v39, 0x1

    move-object/from16 v0, v35

    move/from16 v1, v39

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getData(Ljava/lang/String;Z)V

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_8

    .line 949
    .end local v12    # "fromWatchON":Z
    .end local v15    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :cond_30
    const-string v39, "file://"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_36

    .line 950
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v11

    .line 952
    .local v11, "filePath":Ljava/lang/String;
    const-string v39, "/storage"

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_33

    .line 953
    const/16 v18, 0x18

    .line 954
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v11}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v36

    .line 963
    :goto_a
    const-wide/16 v40, 0x0

    cmp-long v39, v36, v40

    if-gtz v39, :cond_32

    .line 964
    const/16 v18, 0x1a

    .line 966
    const-string v39, "/storage/UsbDrive"

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_31

    .line 967
    const/16 v18, 0x24

    .line 970
    :cond_31
    sget-object v39, Lcom/sec/android/app/videoplayer/common/feature/Path;->HIDE_PATH:Ljava/lang/String;

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_32

    .line 971
    const/16 v18, 0x29

    .line 975
    :cond_32
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    if-eqz v39, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    if-eqz v39, :cond_35

    .line 976
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_8

    .line 955
    :cond_33
    const-string v39, "/system/"

    move-object/from16 v0, v39

    invoke-virtual {v11, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_34

    .line 956
    const/16 v18, 0x17

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    invoke-virtual {v0, v11}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;)J

    move-result-wide v36

    goto :goto_a

    .line 959
    :cond_34
    const/16 v18, 0x1a

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    goto :goto_a

    .line 978
    :cond_35
    invoke-virtual/range {v34 .. v34}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v35

    goto/16 :goto_8

    .line 980
    .end local v11    # "filePath":Ljava/lang/String;
    :cond_36
    const-string v39, "android.resource://"

    move-object/from16 v0, v35

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_37

    .line 981
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - Path starts with android.resource://"

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 982
    const/16 v18, 0x1a

    goto/16 :goto_8

    .line 985
    :cond_37
    const/16 v18, 0x1a

    goto/16 :goto_8

    .line 1007
    .end local v9    # "extension":Ljava/lang/String;
    .end local v20    # "mimeType":Ljava/lang/String;
    .end local v26    # "scheme":Ljava/lang/String;
    .end local v28    # "sdpPath":Ljava/lang/String;
    .end local v30    # "storyItemId":Ljava/lang/String;
    .end local v35    # "uri2string":Ljava/lang/String;
    :cond_38
    const/16 v38, 0x0

    .line 1008
    .local v38, "videoUri":Landroid/net/Uri;
    const-string v39, "filePath"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1009
    .restart local v11    # "filePath":Ljava/lang/String;
    const-string v39, "uri"

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1010
    .local v23, "newUri":Ljava/lang/String;
    const-string v39, "bucket"

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 1011
    .local v7, "bucketId":I
    const-string v39, "ListType"

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 1012
    .local v19, "listType":I
    const-string v39, "cloud_option"

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 1014
    .local v8, "cloud_show_option":I
    if-eqz v7, :cond_39

    .line 1015
    const/16 v19, 0x9

    .line 1018
    :cond_39
    if-eqz v23, :cond_3a

    .line 1019
    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v38

    .line 1022
    :cond_3a
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v39

    const-string v40, "search"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->setPlayListInfo(Ljava/lang/String;I)V

    .line 1023
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-object/from16 v39, v0

    const-string v40, "list_view_by_cloud"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    invoke-virtual {v0, v1, v8}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 1025
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    if-eqz v39, :cond_3b

    .line 1026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setListType(I)V

    .line 1029
    :cond_3b
    if-nez v38, :cond_3c

    .line 1030
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v40, "init() - uri is invalid."

    invoke-static/range {v39 .. v40}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    const v39, 0x15f90

    const/16 v40, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1035
    :cond_3c
    invoke-virtual/range {v38 .. v38}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    const-string v40, "content://cloud/"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-nez v39, :cond_3d

    invoke-virtual/range {v38 .. v38}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    const-string v40, "content://com.skp.tcloud/"

    invoke-virtual/range {v39 .. v40}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v39

    if-eqz v39, :cond_3e

    .line 1036
    :cond_3d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    const/16 v40, 0x23

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    .line 1038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1060
    :goto_b
    move-object/from16 v0, v38

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 1062
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v39, :cond_9

    .line 1063
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - filePath : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - fileUri = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - cloud_show_option : "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1039
    :cond_3e
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v39, :cond_3f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getListType()I

    move-result v39

    const/16 v40, 0xb

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_3f

    .line 1040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    const/16 v40, 0x21

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    const-wide/16 v40, 0x0

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_b

    .line 1042
    :cond_3f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getListType()I

    move-result v39

    const/16 v40, 0xe

    move/from16 v0, v39

    move/from16 v1, v40

    if-ne v0, v1, :cond_40

    .line 1046
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    const/16 v40, 0x1e

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1047
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    .line 1050
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    move-object/from16 v40, v0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->set(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v15

    .line 1051
    .restart local v15    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    invoke-virtual/range {v38 .. v38}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v39

    const/16 v40, 0x1

    move-object/from16 v0, v39

    move/from16 v1, v40

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getData(Ljava/lang/String;Z)V

    .line 1052
    invoke-virtual {v15}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->sendBroadcastPlayedContentInfo()V

    .line 1053
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getResumePos()J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_b

    .line 1055
    .end local v15    # "hubUtil":Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;
    :cond_40
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    const/16 v40, 0x14

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1056
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v39, v0

    move-object/from16 v0, v39

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v36

    .line 1057
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v39, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v40

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_b

    .line 1086
    .end local v7    # "bucketId":I
    .end local v8    # "cloud_show_option":I
    .end local v11    # "filePath":Ljava/lang/String;
    .end local v19    # "listType":I
    .end local v23    # "newUri":Ljava/lang/String;
    .end local v38    # "videoUri":Landroid/net/Uri;
    :cond_41
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v39

    if-eqz v39, :cond_c

    .line 1087
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->canPlayUsingSideSync()Z

    move-result v39

    if-eqz v39, :cond_42

    .line 1088
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastToSideSync(Ljava/lang/String;)V

    .line 1092
    :goto_c
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 1090
    :cond_42
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v39

    const v40, 0x7f0a00f5

    const/16 v41, 0x1

    invoke-virtual/range {v39 .. v41}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto :goto_c

    .line 1105
    :cond_43
    const-string v39, "autoplay"

    const/16 v40, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v39

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAutoPlay:Z

    goto/16 :goto_3

    .line 1144
    .restart local v6    # "audioTrack":I
    .restart local v21    # "minimode_uri":Ljava/lang/String;
    .local v24, "resumePos":I
    :cond_44
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v39

    move-object/from16 v0, v39

    invoke-virtual {v10, v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    goto/16 :goto_4

    .line 1174
    .restart local v31    # "subtitleFile":Ljava/lang/String;
    :cond_45
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleLanguageIndex()I

    move-result v39

    if-lez v39, :cond_10

    .line 1175
    const/16 v39, 0x1

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    goto/16 :goto_5

    .line 1179
    .end local v6    # "audioTrack":I
    .end local v21    # "minimode_uri":Ljava/lang/String;
    .end local v24    # "resumePos":I
    .end local v31    # "subtitleFile":Ljava/lang/String;
    :cond_46
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-object/from16 v39, v0

    if-eqz v39, :cond_47

    .line 1180
    const-string v39, "sortby"

    const/16 v40, 0x2

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    .line 1181
    .local v29, "sortOrder":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-object/from16 v39, v0

    const-string v40, "sortorder"

    move-object/from16 v0, v39

    move-object/from16 v1, v40

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 1183
    sget-object v39, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v40, Ljava/lang/StringBuilder;

    invoke-direct/range {v40 .. v40}, Ljava/lang/StringBuilder;-><init>()V

    const-string v41, "init() - sortby = "

    invoke-virtual/range {v40 .. v41}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v40

    move-object/from16 v0, v40

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    .end local v29    # "sortOrder":I
    :cond_47
    const/16 v39, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v39

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    .line 1187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleSettings()V

    .line 1188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 1189
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-object/from16 v39, v0

    const/16 v40, 0x0

    invoke-virtual/range {v39 .. v40}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 1191
    sget-boolean v39, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v39, :cond_48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v39

    if-nez v39, :cond_10

    .line 1192
    :cond_48
    const/16 v39, -0x1

    invoke-static/range {v39 .. v39}, Lcom/sec/android/app/videoplayer/common/VUtils;->setUserOrientation(I)V

    goto/16 :goto_5

    .line 1200
    :cond_49
    const/16 v17, 0x0

    goto/16 :goto_6

    .line 1211
    .restart local v17    # "isFromWidget":Z
    :cond_4a
    const/16 v39, 0x0

    move/from16 v0, v39

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    goto/16 :goto_7
.end method

.method private initMoviePlayer()V
    .locals 2

    .prologue
    .line 1539
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "initMoviePlayer"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initMainView()V

    .line 1542
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 1543
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initPlayerListView()V

    .line 1550
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->resetColor()V

    .line 1551
    return-void
.end method

.method private initPlayerListView()V
    .locals 2

    .prologue
    .line 1312
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "initPlayerListView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    if-eqz v0, :cond_0

    .line 1315
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->removeAllViews()V

    .line 1318
    :cond_0
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .line 1319
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->addViewTo(Landroid/view/View;)V

    .line 1320
    return-void
.end method

.method private initSettingsPopup()V
    .locals 2

    .prologue
    .line 632
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-nez v0, :cond_0

    .line 633
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    new-instance v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$3;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->setOnPopupCreatedListener(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;)V

    .line 640
    return-void
.end method

.method private initSplitSubtitleView()V
    .locals 2

    .prologue
    .line 1323
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "initSplitSubtitleView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1325
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    if-eqz v0, :cond_0

    .line 1326
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->removeAllViews()V

    .line 1329
    :cond_0
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    .line 1330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->addViewTo(Landroid/view/View;)V

    .line 1331
    return-void
.end method

.method public static isActivityPauseState()Z
    .locals 1

    .prologue
    .line 6914
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    return v0
.end method

.method private isAllShareIntent()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6450
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v2, :cond_0

    .line 6451
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 6452
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 6453
    const-string v2, "com.sec.android.allshare.intent.action.VIDEOPLAYER"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 6456
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    return v1
.end method

.method private isBlockMotionPeek()Z
    .locals 4

    .prologue
    .line 6898
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v0

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sec_container_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isExternalDisplay()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 6765
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSymmetricMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 6781
    :goto_0
    return v3

    .line 6769
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string v5, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v4, v5}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    .line 6770
    .local v0, "displays":[Landroid/view/Display;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 6771
    .local v2, "mDisplays":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/Display;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 6772
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isExternalDisplay ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] #"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6773
    aget-object v4, v0, v1

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6771
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6776
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 6777
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v4, "isExternalDisplay : true"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6778
    const/4 v3, 0x1

    goto :goto_0

    .line 6780
    :cond_2
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "isExternalDisplay : false"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isHLSMimeType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1304
    if-eqz p1, :cond_1

    const-string v0, "application/vnd.apple.mpegurl"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/x-mpegurl"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isIntentAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 5658
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 5659
    .local v1, "pckManager":Landroid/content/pm/PackageManager;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5660
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 5662
    .local v2, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 5663
    const/4 v3, 0x1

    .line 5665
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private isM3U8Content(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sdpPath"    # Ljava/lang/String;

    .prologue
    .line 1308
    if-eqz p1, :cond_0

    const-string v0, "m3u8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPresentationServiceRunning()Z
    .locals 2

    .prologue
    .line 6859
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private isSourceRunning()Z
    .locals 2

    .prologue
    .line 6845
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6846
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "SideSync Source is Running"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6847
    const/4 v0, 0x1

    .line 6849
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSymmetricMode()Z
    .locals 2

    .prologue
    .line 6836
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSymmetricModeRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6837
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "SideSync SymmetricMode is Running"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6838
    const/4 v0, 0x1

    .line 6840
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWatchONInstalled()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6443
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.msc.android.yosemite.intent.action.YosemiteService"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6444
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 6445
    .local v0, "handlers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private pauseOrStopPlaying()V
    .locals 1

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 2091
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseOrStopPlaying()V

    .line 2093
    :cond_0
    return-void
.end method

.method private preProcessForPlaySpeed()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 4559
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    if-eq v0, v2, :cond_2

    .line 4560
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 4561
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    .line 4563
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->updatePlaySpeed(I)V

    .line 4568
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_3

    .line 4569
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedBtn()V

    .line 4574
    :cond_1
    :goto_1
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlaySpeedFromAIA:I

    .line 4575
    return-void

    .line 4565
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    goto :goto_0

    .line 4570
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 4571
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    goto :goto_1
.end method

.method private registerBroadcastReciever()V
    .locals 9

    .prologue
    .line 3226
    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    .line 3227
    .local v4, "intentFilter1":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3228
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3229
    const-string v7, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3230
    const-string v7, "android.intent.action.USB_ANLG_HEADSET_PLUG"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3231
    const-string v7, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3232
    const-string v7, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3233
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_WIFI_DISPLAY:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3234
    const-string v7, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3235
    const-string v7, "android.intent.action.PALM_DOWN"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3236
    const-string v7, "android.intent.action.PALM_UP"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3237
    const-string v7, "AppInAppResumePositionReply"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3238
    const-string v7, "android.intent.action.USER_PRESENT"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3239
    const-string v7, "intent.stop.app-in-app"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3240
    const-string v7, "android.media.IMediaPlayer.videoexist"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3241
    const-string v7, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3242
    const-string v7, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3243
    const-string v7, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3244
    const-string v7, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3245
    const-string v7, "com.sec.android.app.videoplayer.SORT_BY"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3246
    const-string v7, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v4, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3247
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8, v4}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3249
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 3250
    .local v5, "intentFilter3":Landroid/content/IntentFilter;
    sget-object v7, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_CMD:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3251
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3254
    new-instance v2, Landroid/content/IntentFilter;

    const-string v7, "android.intent.action.POSITION_INFO_FROM_MSC_APP"

    invoke-direct {v2, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 3255
    .local v2, "intent5":Landroid/content/IntentFilter;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mB2BSoultionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3257
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 3258
    .local v6, "intentFilterDock":Landroid/content/IntentFilter;
    sget-object v7, Landroid/app/UiModeManager;->ACTION_ENTER_CAR_MODE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3259
    sget-object v7, Landroid/app/UiModeManager;->ACTION_ENTER_DESK_MODE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3260
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3262
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 3263
    .local v3, "intent9":Landroid/content/IntentFilter;
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3264
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v3, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3265
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3267
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 3268
    .local v0, "intent10":Landroid/content/IntentFilter;
    const-string v7, "com.sec.android.action.NOTIFY_SPLIT_WINDOWS"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3269
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMultiWindowBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3271
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 3272
    .local v1, "intent11":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.NOT_ALLOWED_SCREEN_MIRRORING"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3273
    const-string v7, "android.intent.action.VIDEO_PLAYBACK_STOP"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3274
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mChangePlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3275
    return-void
.end method

.method private registerContentObserver()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 5985
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver E"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5988
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    if-nez v4, :cond_4

    .line 5989
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$43;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    .line 6019
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6020
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v4, "accelerometer_rotation"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6021
    .local v2, "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6023
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v4, :cond_0

    .line 6024
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6025
    .local v1, "cr2":Landroid/content/ContentResolver;
    const-string v4, "accelerometer_rotation_second"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 6026
    .local v3, "tmp2":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6028
    .end local v1    # "cr2":Landroid/content/ContentResolver;
    .end local v3    # "tmp2":Landroid/net/Uri;
    :cond_0
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mRotationObserver is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6034
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    if-nez v4, :cond_5

    .line 6035
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    .line 6057
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6058
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "user_rotation"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6059
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6061
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v4, :cond_1

    .line 6062
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 6063
    .restart local v1    # "cr2":Landroid/content/ContentResolver;
    const-string v4, "accelerometer_rotation_second"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 6064
    .restart local v3    # "tmp2":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6066
    .end local v1    # "cr2":Landroid/content/ContentResolver;
    .end local v3    # "tmp2":Landroid/net/Uri;
    :cond_1
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mUserRotationObserver is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6073
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-nez v4, :cond_6

    .line 6074
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$45;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$45;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 6083
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6084
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "screen_brightness_mode"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6085
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6086
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverBrightnessMode is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6091
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    if-nez v4, :cond_7

    .line 6092
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$46;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$46;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 6106
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6107
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "screen_brightness"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6108
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6109
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverBrightnessMode is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6114
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-nez v4, :cond_8

    .line 6115
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$47;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$47;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 6123
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6124
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "auto_brightness_detail"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6125
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6126
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverAutoBrightness is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6159
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :goto_4
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v4, :cond_2

    .line 6160
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    if-nez v4, :cond_9

    .line 6161
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$49;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$49;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    .line 6171
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6172
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "smart_pause"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6173
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6174
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverSmartPause is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6180
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :cond_2
    :goto_5
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v4, :cond_3

    .line 6181
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    if-nez v4, :cond_a

    .line 6182
    new-instance v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$50;

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$50;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    .line 6195
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6196
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    const-string v4, "screen_mode_automatic_setting"

    invoke-static {v4}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 6197
    .restart local v2    # "tmp":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 6198
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mScreenModeObserver is now registered"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6203
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "tmp":Landroid/net/Uri;
    :cond_3
    :goto_6
    return-void

    .line 6030
    :cond_4
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mRotationObserver already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 6068
    :cond_5
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mUserRotationObserver already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 6088
    :cond_6
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverBrightnessMode already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 6111
    :cond_7
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverBrightnessMode already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 6128
    :cond_8
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverAutoBrightness already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 6176
    :cond_9
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mObserverSmartPause already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 6200
    :cond_a
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "registerContentObserver - mScreenModeObserver already exists"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method private registerLocalReceiver()V
    .locals 3

    .prologue
    .line 517
    invoke-static {p0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    .line 519
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bLocalReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 520
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bLocalReceiverRegistered:Z

    .line 522
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 523
    .local v0, "Localfilter":Landroid/content/IntentFilter;
    const-string v1, "videoplayer.exit"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 524
    const-string v1, "videoplayer.set.lock"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 525
    const-string v1, "videoplayer.pauseby"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 526
    const-string v1, "videoplayer.resumeby"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 527
    const-string v1, "videoplayer.update.title"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 528
    const-string v1, "videoplayer.show.error.popup"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 529
    const-string v1, "videoplayer.swipe"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 530
    const-string v1, "videoplayer.dismiss.presentation"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 531
    const-string v1, "videoplayer.show.controller.smartpause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 532
    const-string v1, "videoplayer.stateview.visibility"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 534
    new-instance v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 598
    .end local v0    # "Localfilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerPackageReceiver()V
    .locals 2

    .prologue
    .line 609
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPackageReceiverRegistered:Z

    if-nez v1, :cond_0

    .line 610
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPackageReceiverRegistered:Z

    .line 611
    new-instance v1, Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    invoke-direct {v1}, Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    .line 613
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 614
    .local v0, "intentfilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 615
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 616
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 617
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 619
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 621
    .end local v0    # "intentfilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private registerWatchONReceiver()V
    .locals 3

    .prologue
    .line 3287
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "registerWatchONReceiver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3290
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3291
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRegisteredWatchONReceiver:Z

    .line 3292
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 3293
    .local v0, "intentFilterVolume":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.intent.RESPONSE_FROM_YOSEMITE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3294
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3296
    .end local v0    # "intentFilterVolume":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private release()V
    .locals 2

    .prologue
    .line 3598
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "release."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3599
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->unbindService()V

    .line 3600
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterLocalReceiver()V

    .line 3601
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterPackageReceiver()V

    .line 3602
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->releaseExtensionFwk()V

    .line 3603
    return-void
.end method

.method private releaseExtensionFwk()V
    .locals 1

    .prologue
    .line 3606
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v0, :cond_2

    .line 3607
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_1

    .line 3608
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->release()V

    .line 3609
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->disconnect()V

    .line 3611
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->remove()V

    .line 3618
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->remove()V

    .line 3619
    :cond_3
    return-void
.end method

.method private releaseMainView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 3177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->releaseView()V

    .line 3178
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 3181
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_1

    .line 3182
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->releaseView()V

    .line 3183
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 3186
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 3187
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->releaseView()V

    .line 3188
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 3191
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_3

    .line 3192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->releaseView()V

    .line 3193
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 3196
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_4

    .line 3197
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->releaseView()V

    .line 3198
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 3201
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_5

    .line 3202
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 3203
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 3206
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3208
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_6

    .line 3209
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 3212
    :cond_6
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_7

    .line 3213
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 3216
    :cond_7
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_8

    .line 3217
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->releaseView()V

    .line 3218
    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 3221
    :cond_8
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 3222
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "releaseMainView() finished"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3223
    return-void
.end method

.method private removeAllPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5436
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "removeAllPopup E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 5439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 5440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 5443
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    if-eqz v0, :cond_1

    .line 5444
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->deletePopup()V

    .line 5445
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .line 5455
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 5456
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 5457
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    .line 5460
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    if-eqz v0, :cond_3

    .line 5461
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->deletePopup()V

    .line 5462
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .line 5465
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_4

    .line 5466
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 5467
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    .line 5470
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v0, :cond_5

    .line 5471
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismiss()V

    .line 5472
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->removeAllPopup()V

    .line 5473
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .line 5476
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v0, :cond_6

    .line 5477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->dismiss()V

    .line 5478
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .line 5481
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 5482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismissSyncPopup()V

    .line 5485
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 5486
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 5487
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 5490
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    if-eqz v0, :cond_9

    .line 5491
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->dismiss()V

    .line 5492
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    .line 5495
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    if-eqz v0, :cond_a

    .line 5496
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->dismiss()V

    .line 5497
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 5500
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_b

    .line 5501
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->dismissPlaySpeed()V

    .line 5504
    :cond_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->dismiss()V

    .line 5506
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    if-eqz v0, :cond_c

    .line 5507
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->dismissChangePlayerPopup()V

    .line 5510
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->dismiss()V

    .line 5512
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    .line 5513
    return-void
.end method

.method private removeAnimationEffects()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 6525
    invoke-virtual {p0, v0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->overridePendingTransition(II)V

    .line 6526
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x308

    const-string v3, "SAMSUNG_FLAG_NO_RESIZE_ANIMATION_INCLUDE_CHILD"

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    .line 6527
    return-void
.end method

.method private resumeOrStartPlaying()V
    .locals 5

    .prologue
    .line 2097
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v1

    .line 2099
    .local v1, "pauaseEnable":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_0

    .line 2100
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    .line 2102
    .local v0, "isPlaying":Z
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resumeOrStartPlaying() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pauseEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2104
    if-nez v0, :cond_0

    .line 2105
    if-eqz v1, :cond_1

    .line 2106
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 2112
    .end local v0    # "isPlaying":Z
    :cond_0
    :goto_0
    return-void

    .line 2108
    .restart local v0    # "isPlaying":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto :goto_0
.end method

.method private saveSubtitleSyncTime(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 6873
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveSubtitleSyncTime() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6875
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    if-eqz v0, :cond_0

    .line 6876
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->dismiss()V

    .line 6879
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSyncPopup()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6880
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismissSyncPopup()V

    .line 6883
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6884
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 6885
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 6888
    :cond_2
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_3

    .line 6889
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 6891
    :cond_3
    return-void
.end method

.method private sendSideSyncDisableVideoBroadcast()V
    .locals 3

    .prologue
    .line 6853
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "sendSideSyncDisableVideoBroadcast"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6854
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sidesync.common.DISABLE_PLAY_VIDEO"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6855
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 6856
    return-void
.end method

.method private setStaticView()V
    .locals 3

    .prologue
    .line 7077
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hideTVOutView()V

    .line 7079
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_2

    .line 7080
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTVOutView()V

    .line 7081
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v1, v2, :cond_0

    .line 7082
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_3

    .line 7083
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v1, v2, :cond_0

    .line 7084
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 7090
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v1, v2, :cond_1

    .line 7091
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_4

    .line 7092
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v1, v2, :cond_1

    .line 7093
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 7100
    :cond_1
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v1, v2, :cond_2

    .line 7101
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 7107
    :cond_2
    :goto_2
    return-void

    .line 7087
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 7104
    :catch_0
    move-exception v0

    .line 7105
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    .line 7096
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static sleep(I)V
    .locals 4
    .param p0, "ms"    # I

    .prologue
    .line 4892
    int-to-long v2, p0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4896
    :goto_0
    return-void

    .line 4893
    :catch_0
    move-exception v0

    .line 4894
    .local v0, "ie":Ljava/lang/InterruptedException;
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "sleep interrupted"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private startAudioOnlyView()V
    .locals 1

    .prologue
    .line 5825
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5826
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addAudioOnlyView()V

    .line 5828
    :cond_0
    return-void
.end method

.method private startMoreService()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 2052
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2053
    .local v0, "intent":Landroid/content/Intent;
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    if-eqz v1, :cond_0

    .line 2054
    const-string v1, "samsungapps://ProductDetail/com.sec.android.app.vefull"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2058
    :goto_0
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2059
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startActivity(Landroid/content/Intent;)V

    .line 2060
    return-void

    .line 2056
    :cond_0
    const-string v1, "samsungapps://ProductDetail/com.sec.android.app.ve"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private stopAppInApp()V
    .locals 3

    .prologue
    .line 6398
    new-instance v0, Landroid/content/Intent;

    const-string v1, "intent.stop.app-in-app"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6399
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "stopFrom"

    const-string v2, "stopFromInternal"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6400
    const-string v1, "intent.stop.app-in-app.send_app"

    const-string v2, "VideoPlayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6401
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 6402
    return-void
.end method

.method private stopOtherPresentationServices()V
    .locals 3

    .prologue
    .line 2769
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2770
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "app_name"

    const-string v2, "video"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2771
    const-string v1, "user_id"

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2772
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 2773
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 6206
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6207
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 6209
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 6210
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6211
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRotationObserver:Landroid/database/ContentObserver;

    .line 6212
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mRotationObserver is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6215
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_1

    .line 6216
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6217
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mUserRotationObserver:Landroid/database/ContentObserver;

    .line 6218
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mUserRotationObserver is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6222
    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    if-eqz v1, :cond_2

    .line 6223
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6224
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightnessMode:Landroid/database/ContentObserver;

    .line 6225
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mObserverBrightnessMode is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6228
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_3

    .line 6229
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6230
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverBrightness:Landroid/database/ContentObserver;

    .line 6231
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mObserverBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6234
    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    if-eqz v1, :cond_4

    .line 6235
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6236
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverAutoBrightness:Landroid/database/ContentObserver;

    .line 6237
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mObserverAutoBrightness is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6249
    :cond_4
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SMART_PAUSE:Z

    if-eqz v1, :cond_5

    .line 6250
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    if-eqz v1, :cond_5

    .line 6251
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6252
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mObserverSmartPause:Landroid/database/ContentObserver;

    .line 6253
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mObserverSmartPause is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6257
    :cond_5
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v1, :cond_6

    .line 6258
    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_6

    .line 6259
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 6260
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mScreenModeObserver:Landroid/database/ContentObserver;

    .line 6261
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "unregisterContentObserver - mScreenModeObserver is now unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6264
    :cond_6
    return-void
.end method

.method private unregisterLocalReceiver()V
    .locals 2

    .prologue
    .line 601
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bLocalReceiverRegistered:Z

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalBroadcastManager:Landroid/support/v4/content/LocalBroadcastManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 603
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bLocalReceiverRegistered:Z

    .line 605
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 606
    return-void
.end method

.method private unregisterPackageReceiver()V
    .locals 1

    .prologue
    .line 624
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPackageReceiverRegistered:Z

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 626
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPackageReceiverRegistered:Z

    .line 628
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPackageReceiver:Lcom/sec/android/app/videoplayer/receiver/PackageReceiver;

    .line 629
    return-void
.end method

.method private unregisterWatchONReceiver()V
    .locals 2

    .prologue
    .line 3299
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "unregisterWatchONReceiver E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3301
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRegisteredWatchONReceiver:Z

    if-nez v0, :cond_1

    .line 3308
    :cond_0
    :goto_0
    return-void

    .line 3303
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 3304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnReceiver:Landroid/content/BroadcastReceiver;

    .line 3306
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRegisteredWatchONReceiver:Z

    goto :goto_0
.end method

.method private updateLastPlayedItem()V
    .locals 3

    .prologue
    .line 6405
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    .line 6406
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 6407
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "lastPlayedItem"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 6408
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastLastFileName(Ljava/lang/String;)V

    .line 6409
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->updateLastPlayedItem(Landroid/net/Uri;)V

    .line 6411
    .end local v0    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private updateLayoutForMultiWindow()V
    .locals 4

    .prologue
    const/16 v3, 0xd5

    .line 1631
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_2

    .line 1632
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_1

    .line 1633
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateScreen(II)V

    .line 1635
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isBlockShowControllerMWmode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1637
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 1638
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->change_Margin(I)V

    .line 1642
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1643
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_1

    .line 1644
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 1649
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1650
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1652
    :cond_2
    return-void
.end method

.method private updateSortByInfo(I)V
    .locals 2
    .param p1, "order"    # I

    .prologue
    .line 4883
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-nez v0, :cond_0

    .line 4884
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 4887
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "sortorder"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 4888
    return-void
.end method

.method private updateVideoWidget()V
    .locals 5

    .prologue
    .line 3009
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3010
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3011
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "videowidget.update"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3012
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 3014
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ENABLE_MAGAZINE_HOME:Z

    if-eqz v3, :cond_0

    .line 3015
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3016
    .local v1, "intent2":Landroid/content/Intent;
    const-string v3, "videowidget.update"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3017
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 3019
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 3020
    .local v2, "intent3":Landroid/content/Intent;
    const-string v3, "com.samsung.everglades.video"

    const-string v4, "com.samsung.everglades.video.myvideo.list.FavoriteIconWidget"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3021
    const-string v3, "videowidget.update"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 3022
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V

    .line 3025
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "intent2":Landroid/content/Intent;
    .end local v2    # "intent3":Landroid/content/Intent;
    :cond_0
    return-void
.end method


# virtual methods
.method public DisablePresentationMode()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6806
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "DisablePresentationMode()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 6808
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_4

    .line 6809
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->changeFitToSrcBtn(Z)V

    .line 6814
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_3

    .line 6815
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_5

    .line 6816
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v0, v1, :cond_1

    .line 6817
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 6827
    :cond_1
    :goto_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v0, v1, :cond_2

    .line 6828
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 6830
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 6831
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hideTVOutView()V

    .line 6833
    :cond_3
    return-void

    .line 6810
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 6811
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeFitToSrcBtn(Z)V

    goto :goto_0

    .line 6820
    :cond_5
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v0, v1, :cond_6

    .line 6821
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 6823
    :cond_6
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v0, v1, :cond_1

    .line 6824
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public EnablePresentationMode()V
    .locals 2

    .prologue
    .line 6786
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "EnablePresentationMode()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 6787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-nez v0, :cond_0

    .line 6803
    :goto_0
    return-void

    .line 6791
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_2

    .line 6792
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 6801
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 6802
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTVOutView()V

    goto :goto_0

    .line 6794
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_3

    .line 6795
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 6797
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 6798
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public StopMoviPlay4Presentation()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6685
    sput-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    .line 6687
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_0

    .line 6688
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->stopSubtitle()V

    .line 6691
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceExists()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_3

    .line 6693
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 6694
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    .line 6696
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 6697
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 6699
    :cond_3
    return-void
.end method

.method public StopSecondaryDisplay()V
    .locals 2

    .prologue
    .line 6744
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "StopSecondaryDisplay"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6746
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 6747
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->unbindFromPresentationService()V

    .line 6749
    :cond_0
    return-void
.end method

.method public blockSmartPause()V
    .locals 1

    .prologue
    .line 6894
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->blockSmartPause()V

    .line 6895
    return-void
.end method

.method public broadcastLastFileName(Ljava/lang/String;)V
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 6388
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.everglades.video.myvideo.LAST_FILE_NAME"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6389
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "filepath"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 6391
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6395
    :goto_0
    return-void

    .line 6392
    :catch_0
    move-exception v0

    .line 6393
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ActivityNotFoundException occured :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public broadcastPausedPosition(Z)V
    .locals 12
    .param p1, "exitPlayerByAppinApp"    # Z

    .prologue
    const-wide/16 v10, 0x1388

    .line 6353
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v6, :cond_1

    .line 6385
    :cond_0
    :goto_0
    return-void

    .line 6358
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    if-nez v6, :cond_0

    .line 6360
    :cond_2
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v7, "broadcastPausedPosition() launch from videoHub"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6361
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v6

    int-to-long v0, v6

    .line 6362
    .local v0, "curPos":J
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v6

    int-to-long v2, v6

    .line 6363
    .local v2, "duration":J
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastPausedPosition() - curPos : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", duration : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6365
    cmp-long v6, v0, v10

    if-gez v6, :cond_3

    .line 6366
    const-wide/16 v0, 0x0

    .line 6373
    :goto_1
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.sdgtl.stamhubb.PAUSED_POSITION"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 6374
    .local v5, "intent":Landroid/content/Intent;
    const-string v6, "paused_position"

    invoke-virtual {v5, v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 6375
    const-string v6, "filepath"

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 6376
    const-string v6, "exitPlayerByAppinApp"

    invoke-virtual {v5, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 6377
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "broadcastPausedPosition() - postion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " exitPlayerByAppinApp : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6380
    :try_start_0
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 6381
    :catch_0
    move-exception v4

    .line 6382
    .local v4, "e":Landroid/content/ActivityNotFoundException;
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ActivityNotFoundException occured :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 6367
    .end local v4    # "e":Landroid/content/ActivityNotFoundException;
    .end local v5    # "intent":Landroid/content/Intent;
    :cond_3
    sub-long v6, v2, v0

    cmp-long v6, v6, v10

    if-gtz v6, :cond_4

    .line 6368
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 6370
    :cond_4
    sub-long/2addr v0, v10

    goto :goto_1
.end method

.method public callBTDevicePickerActivity()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5618
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.bluetooth.devicepicker.action.LAUNCH"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5619
    .local v0, "btIntent":Landroid/content/Intent;
    const-string v2, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5620
    const-string v2, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 5621
    const-string v2, "android.bluetooth.FromHeadsetActivity"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5624
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 5628
    :goto_0
    return-void

    .line 5625
    :catch_0
    move-exception v1

    .line 5626
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onOptionsItemSelected. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public callPreviewDialog()V
    .locals 8

    .prologue
    .line 5850
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v5, "callPreviewDialog"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5852
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v4

    int-to-long v2, v4

    .line 5853
    .local v2, "resumePos":J
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    sget-object v5, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->TAG:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 5855
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    .line 5856
    .local v0, "fileId":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->setFileId(J)Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v5

    const-wide/16 v6, 0x0

    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    long-to-int v4, v2

    div-int/lit16 v4, v4, 0x3e8

    :goto_0
    invoke-virtual {v5, v4}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->show(I)V

    .line 5857
    return-void

    .line 5856
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public callSlideChapterPreview()V
    .locals 4

    .prologue
    .line 5831
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "callSlideChapterPreview() E."

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5832
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    .line 5833
    .local v0, "fileId":J
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 5835
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-nez v2, :cond_0

    .line 5836
    new-instance v2, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    const v3, 0x7f0b0025

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    .line 5839
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v2

    if-nez v2, :cond_1

    .line 5840
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "Show SideChapterViewDialog"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5841
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5842
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->initDatasForHelpMotionPeek(Ljava/lang/String;)V

    .line 5845
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->show()V

    .line 5847
    :cond_1
    return-void

    .line 5844
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->initDatas(J)V

    goto :goto_0
.end method

.method public callWatchOnClient()V
    .locals 6

    .prologue
    .line 5632
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    if-nez v2, :cond_1

    .line 5655
    :cond_0
    :goto_0
    return-void

    .line 5636
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v2

    int-to-long v2, v2

    sput-wide v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    .line 5637
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "callWatchOnClient. resumeTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5638
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 5640
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.msc.android.yosemite.intent.action.CPManager"

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isIntentAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5641
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 5642
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.msc.android.yosemite.intent.action.CPManager"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 5643
    const/high16 v2, 0x18000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5644
    const-string v2, "device_type"

    const-string v3, "tv"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5645
    const-string v2, "cp_name"

    const-string v3, "MediaHub"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5646
    const-string v2, "command"

    const-string v3, "PAUSE_AND_RESUME"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5647
    const-string v2, "resume_time"

    sget-wide v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5650
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5651
    :catch_0
    move-exception v0

    .line 5652
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "callWatchOnClient error. activity not found!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public changeBtnControllerView()V
    .locals 3

    .prologue
    .line 2710
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 2711
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide(Z)V

    .line 2712
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeAllViewsInLayout()V

    .line 2715
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2716
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 2721
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setRootViewReference(Landroid/widget/RelativeLayout;)V

    .line 2722
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    .line 2723
    return-void

    .line 2718
    :cond_1
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    goto :goto_0
.end method

.method public changeSubviewBtnControllerForTvOutView(Z)V
    .locals 5
    .param p1, "isConnected"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2727
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeSubviewBtnControllerForTvOutView() - isConnected : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2729
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 2730
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 2731
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide(Z)V

    .line 2732
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeAllViewsInLayout()V

    .line 2735
    :cond_0
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 2736
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 2738
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    .line 2740
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2741
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->addViewTo(Landroid/view/View;)V

    .line 2742
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setAdditionalViewToChildView(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V

    .line 2745
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setRootViewReference(Landroid/widget/RelativeLayout;)V

    .line 2765
    :goto_0
    return-void

    .line 2747
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_3

    .line 2748
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->hide(Z)V

    .line 2749
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeAllViewsInLayout()V

    .line 2752
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_4

    .line 2753
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->hide()V

    .line 2759
    :cond_4
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 2760
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 2762
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    .line 2763
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setRootViewReference(Landroid/widget/RelativeLayout;)V

    goto :goto_0
.end method

.method public changeSurfaceview()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 7031
    const/4 v7, 0x0

    .line 7033
    .local v7, "presentationRunning":Z
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 7034
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->RemoveViewPresentationChangeSurface()V

    .line 7035
    const/4 v7, 0x1

    .line 7038
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_6

    .line 7039
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 7040
    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 7041
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 7042
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 7044
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7045
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setSecure(Z)V

    .line 7054
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_2

    .line 7055
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 7056
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChildViewReferences(Lcom/sec/android/app/videoplayer/view/VideoSurface;Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;Lcom/sec/android/app/videoplayer/view/VideoStateView;Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V

    .line 7059
    :cond_2
    if-eqz v7, :cond_5

    .line 7060
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_3

    .line 7061
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 7064
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_4

    .line 7065
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 7068
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_5

    .line 7069
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->AddViewPresentationChangeSurface()V

    .line 7073
    :cond_5
    return-void

    .line 7047
    :cond_6
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 7048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 7049
    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 7050
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 7051
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public checkSKTCloudServiceFeature()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 6540
    sget-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isCheckedSKTCloudServiceFeature:Z

    if-eqz v2, :cond_0

    .line 6559
    :goto_0
    return-void

    .line 6542
    :cond_0
    const/4 v1, 0x0

    .line 6545
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.skp.tcloud.agent"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 6552
    :goto_1
    if-nez v1, :cond_1

    .line 6553
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    .line 6558
    :goto_2
    sput-boolean v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isCheckedSKTCloudServiceFeature:Z

    goto :goto_0

    .line 6548
    :catch_0
    move-exception v0

    .line 6549
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "com.skp.tcloud.agent NotFound"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 6555
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    sput-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    goto :goto_2
.end method

.method public createEditDialog()V
    .locals 3

    .prologue
    .line 2027
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2028
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0057

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2029
    const v1, 0x7f0a01b3

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2030
    const v1, 0x7f0a00ae

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$10;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2040
    const v1, 0x7f0a0026

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$11;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$11;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2046
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 2047
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2048
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2049
    return-void
.end method

.method public createKDrmPopup(I)V
    .locals 13
    .param p1, "type"    # I

    .prologue
    .line 5205
    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createKdrmPopup() type = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 5207
    const/4 v3, 0x0

    .line 5208
    .local v3, "leftBtn":I
    const/4 v7, 0x0

    .line 5209
    .local v7, "rightBtn":I
    const/4 v1, 0x1

    .line 5210
    .local v1, "btnEnable":Z
    const/4 v5, 0x0

    .line 5211
    .local v5, "popupStr":Ljava/lang/String;
    const/4 v6, 0x0

    .line 5212
    .local v6, "popupTitleStr":Ljava/lang/String;
    move v0, p1

    .line 5214
    .local v0, "PopUptype":I
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getDrmPlaybackStatus()I

    move-result v8

    .line 5215
    .local v8, "status":I
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getAvailableCount()I

    move-result v2

    .line 5217
    .local v2, "cnt":I
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->checkMTPConnected()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 5218
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    .line 5350
    :goto_0
    return-void

    .line 5222
    :cond_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    if-eqz v9, :cond_1

    .line 5223
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->dismiss()V

    .line 5224
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 5227
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 5276
    :goto_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v4, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 5278
    .local v4, "popup":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v4, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 5279
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 5281
    new-instance v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$38;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$38;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)V

    invoke-virtual {v4, v3, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5317
    if-eqz v1, :cond_2

    .line 5318
    new-instance v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$39;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$39;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)V

    invoke-virtual {v4, v7, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 5341
    :cond_2
    new-instance v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$40;

    invoke-direct {v9, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$40;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v4, v9}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 5347
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    .line 5348
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mKdrmPopup:Landroid/app/AlertDialog;

    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 5349
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    goto :goto_0

    .line 5229
    .end local v4    # "popup":Landroid/app/AlertDialog$Builder;
    :pswitch_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto :goto_0

    .line 5233
    :pswitch_1
    const v3, 0x104000a

    .line 5234
    const/4 v1, 0x0

    .line 5235
    const-string v6, "DRM Expired"

    .line 5237
    const v9, -0xfffdffa

    if-ne v8, v9, :cond_3

    .line 5238
    const v9, 0x7f0a005a

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 5239
    :cond_3
    const v9, -0xfffdffc

    if-ne v8, v9, :cond_4

    .line 5240
    const v9, 0x7f0a005b

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 5242
    :cond_4
    const v9, 0x7f0a00f7

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 5244
    goto :goto_1

    .line 5248
    :pswitch_2
    const-string v6, "DRM Check"

    .line 5249
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const v10, 0x7f0a0061

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0a0062

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const v10, 0x7f0a0063

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 5251
    const v3, 0x1040013

    .line 5252
    const v7, 0x1040009

    .line 5253
    const/4 v1, 0x1

    .line 5254
    goto/16 :goto_1

    .line 5259
    :pswitch_3
    const v9, 0x7f0a005c

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 5260
    const v3, 0x7f0a0172

    .line 5261
    const v7, 0x1040009

    .line 5262
    const/4 v1, 0x1

    .line 5263
    goto/16 :goto_1

    .line 5266
    :pswitch_4
    const v9, 0x7f0a005e

    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 5267
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v9

    const-string v10, "gpsVerifYes"

    const-string v11, "string"

    const-string v12, "android"

    invoke-virtual {v9, v10, v11, v12}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 5268
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v9

    const-string v10, "gpsVerifNo"

    const-string v11, "string"

    const-string v12, "android"

    invoke-virtual {v9, v10, v11, v12}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 5269
    const/4 v1, 0x1

    .line 5270
    goto/16 :goto_1

    .line 5227
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public createMenuEditDialog(Landroid/view/SubMenu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/SubMenu;

    .prologue
    .line 1922
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getMenuArray(Landroid/view/SubMenu;)[Ljava/lang/CharSequence;

    move-result-object v1

    .line 1924
    .local v1, "menuArray":[Ljava/lang/CharSequence;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$7;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v2, v1, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0a00b0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1930
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1931
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1932
    return-void
.end method

.method public createSamsungAppsDialog()V
    .locals 3

    .prologue
    .line 2063
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2064
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2065
    const v1, 0x7f0a0107

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2066
    const v1, 0x7f0a00dd

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$12;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$12;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2078
    const v1, 0x7f0a0026

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$13;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$13;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2084
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 2085
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2086
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2087
    return-void
.end method

.method public exitMovieplayer4AppInApp()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 3496
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v1, :cond_1

    .line 3497
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v1, :cond_0

    .line 3498
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 3499
    .local v0, "path":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v2, "lastPlayedItemOfAIA"

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 3502
    .end local v0    # "path":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    .line 3504
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityCreated:Z

    if-nez v1, :cond_2

    .line 3524
    :cond_1
    :goto_0
    return-void

    .line 3506
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    .line 3508
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_4

    .line 3509
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 3510
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 3512
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 3513
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->release()V

    .line 3517
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3518
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V

    .line 3521
    :cond_5
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    .line 3522
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->finish()V

    goto :goto_0
.end method

.method public finish()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4136
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "finish E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4138
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 4139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 4142
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 4144
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4145
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "finish. isInLockTaskMode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4146
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 4168
    :cond_1
    :goto_0
    return-void

    .line 4150
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExit:Z

    if-nez v0, :cond_1

    .line 4153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExit:Z

    .line 4154
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    .line 4156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_3

    .line 4157
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    .line 4160
    :cond_3
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    if-eqz v0, :cond_4

    .line 4161
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    .line 4162
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    if-nez v0, :cond_4

    .line 4163
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->launchVideoList(Landroid/content/Context;)V

    .line 4167
    :cond_4
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method

.method public getASPUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 6419
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mASPUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getExitByAppinApp()Z
    .locals 1

    .prologue
    .line 6481
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    return v0
.end method

.method public getOrientation()I
    .locals 1

    .prologue
    .line 7114
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I

    return v0
.end method

.method public getPreviousAppWidth()I
    .locals 1

    .prologue
    .line 6863
    iget v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    return v0
.end method

.method public getTutorialWidget()Z
    .locals 1

    .prologue
    .line 6534
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTutorialCheck:Z

    return v0
.end method

.method public getUserRotationModefromAssistantMenu()I
    .locals 5

    .prologue
    .line 5910
    const/4 v0, -0x1

    .line 5911
    .local v0, "mode":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "user_rotation"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 5913
    .local v1, "userRotation":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->LANDSCAPE_ASSISTANTMENU:Z

    if-eqz v2, :cond_0

    .line 5914
    packed-switch v1, :pswitch_data_0

    .line 5928
    const/4 v0, -0x1

    .line 5950
    :goto_0
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getUserRotationModefromAssistantMenu : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5951
    return v0

    .line 5916
    :pswitch_0
    const/4 v0, 0x0

    .line 5917
    goto :goto_0

    .line 5919
    :pswitch_1
    const/4 v0, 0x1

    .line 5920
    goto :goto_0

    .line 5922
    :pswitch_2
    const/16 v0, 0x8

    .line 5923
    goto :goto_0

    .line 5925
    :pswitch_3
    const/16 v0, 0x9

    .line 5926
    goto :goto_0

    .line 5932
    :cond_0
    packed-switch v1, :pswitch_data_1

    .line 5946
    const/4 v0, -0x1

    goto :goto_0

    .line 5934
    :pswitch_4
    const/4 v0, 0x1

    .line 5935
    goto :goto_0

    .line 5937
    :pswitch_5
    const/4 v0, 0x0

    .line 5938
    goto :goto_0

    .line 5940
    :pswitch_6
    const/16 v0, 0x9

    .line 5941
    goto :goto_0

    .line 5943
    :pswitch_7
    const/16 v0, 0x8

    .line 5944
    goto :goto_0

    .line 5914
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 5932
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getsplitSubtitleViewState()Z
    .locals 1

    .prologue
    .line 6989
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSplitSubtitleActive()Z

    move-result v0

    return v0
.end method

.method public hidePlayerList(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 6957
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->hideList(ZZ)V

    .line 6958
    return-void
.end method

.method public hideSplitSubtitle()V
    .locals 1

    .prologue
    .line 6973
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->hideView()V

    .line 6974
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6975
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->showPlayerList(Z)V

    .line 6977
    :cond_0
    return-void
.end method

.method public hideTVOutView()V
    .locals 1

    .prologue
    .line 6414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 6415
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTVOutView()V

    .line 6416
    :cond_0
    return-void
.end method

.method public initMainView()V
    .locals 10

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1334
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "initMainView"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVto:Landroid/view/ViewTreeObserver;

    .line 1338
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_0

    .line 1339
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVto:Landroid/view/ViewTreeObserver;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mGolbalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1341
    new-instance v9, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v9, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 1342
    .local v9, "multiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    new-instance v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v9, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 1426
    .end local v9    # "multiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_1

    .line 1427
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeAllInController()V

    .line 1428
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeAllViews()V

    .line 1429
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->releaseView()V

    .line 1430
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 1433
    :cond_1
    const/4 v7, 0x0

    .line 1435
    .local v7, "attr":Landroid/util/AttributeSet;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_2

    .line 1436
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    monitor-enter v1

    .line 1437
    const/4 v2, 0x0

    :try_start_0
    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 1438
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1441
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_3

    .line 1442
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    monitor-enter v1

    .line 1443
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 1444
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1447
    :cond_3
    new-instance v1, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 1449
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_360:Z

    if-eqz v1, :cond_c

    .line 1450
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->get360Video(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1451
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 1452
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1453
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setSecure(Z)V

    .line 1462
    :cond_4
    :goto_0
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoStateView;

    const v2, 0x101007a

    invoke-direct {v1, p0, v7, v2}, Lcom/sec/android/app/videoplayer/view/VideoStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 1463
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 1465
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 1466
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setVisibility(I)V

    .line 1468
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleView(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    .line 1469
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 1470
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 1472
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_d

    .line 1473
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    .line 1474
    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v8, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1475
    .local v8, "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1476
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_5

    .line 1477
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1479
    :cond_5
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_6

    .line 1480
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1482
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 1492
    .end local v8    # "containerParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 1493
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestFocus()Z

    .line 1495
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->addViewTo(Landroid/view/View;)V

    .line 1496
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 1497
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->addViewTo(Landroid/view/View;)V

    .line 1499
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1500
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 1501
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->addViewTo(Landroid/view/View;)V

    .line 1505
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v0, 0x0

    :cond_9
    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCurrentMode:I

    .line 1507
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1509
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasySubviewBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 1520
    :goto_2
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 1522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setRootViewReference(Landroid/widget/RelativeLayout;)V

    .line 1523
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1524
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    .line 1529
    :goto_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1530
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setAdditionalViewToChildView(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V

    .line 1533
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLockCtrlView:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChildViewReferences(Lcom/sec/android/app/videoplayer/view/VideoSurface;Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;Lcom/sec/android/app/videoplayer/view/VideoStateView;Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V

    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setImportantForAccessibility(I)V

    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1536
    return-void

    .line 1438
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1444
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1456
    :cond_b
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    goto/16 :goto_0

    .line 1459
    :cond_c
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    goto/16 :goto_0

    .line 1484
    :cond_d
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_e

    .line 1485
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 1487
    :cond_e
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_7

    .line 1488
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1511
    :cond_f
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    goto/16 :goto_2

    .line 1514
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1515
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    goto/16 :goto_2

    .line 1517
    :cond_11
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    goto/16 :goto_2

    .line 1526
    :cond_12
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    goto/16 :goto_3
.end method

.method public isBluetoothDeviceConnected()Z
    .locals 2

    .prologue
    .line 6423
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 6424
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 6425
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 6426
    const/4 v1, 0x1

    .line 6430
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBluetoothOn()Z
    .locals 1

    .prologue
    .line 6434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 6435
    const/4 v0, 0x0

    .line 6438
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isDialogPopupShowing()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 5516
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "isDialogPopupShowing()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5518
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_1

    .line 5519
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "isDialogPopupShowing - mServiceUtil.mAlertDialog return true"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5558
    :cond_0
    :goto_0
    return v0

    .line 5523
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    if-eqz v1, :cond_2

    .line 5524
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isDialogPopupShowing - mAlertmessage:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5528
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5532
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5536
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5540
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5544
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 5545
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "isDialogPopupShowing - mSubtitlePopup true"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5549
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 5553
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->isPopupShowing()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 5554
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "isDialogPopupShowing - mSettingsPopup true"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 5558
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public isHelpHoverType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 6937
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHelpPrevType:Ljava/lang/String;

    return-object v0
.end method

.method public isPlayerListShowing()Z
    .locals 2

    .prologue
    .line 6980
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    if-eqz v0, :cond_0

    .line 6981
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->isShowing()Z

    move-result v0

    .line 6984
    :goto_0
    return v0

    .line 6983
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "isPlayerListShowing. mPlayerListView is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6984
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStateViewVisible()Z
    .locals 1

    .prologue
    .line 7110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoExtensionState()Z
    .locals 1

    .prologue
    .line 6927
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/16 v9, 0xcf

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3387
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3389
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onConfigurationChanged. orientation : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3391
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;

    invoke-direct {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;-><init>()V

    .line 3392
    .local v0, "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 3394
    .local v1, "multiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->isFeatureEnabled(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->getVersionCode()I

    move-result v2

    if-lez v2, :cond_0

    .line 3395
    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v2

    if-nez v2, :cond_0

    .line 3403
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 3404
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 3405
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v2, v9, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3410
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 3466
    :cond_1
    :goto_0
    return-void

    .line 3412
    :cond_2
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_7

    .line 3413
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 3414
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, v3

    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    .line 3425
    :goto_2
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v2, :cond_3

    .line 3426
    iget v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOldFolderState:I

    iget v4, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-eq v2, v4, :cond_3

    .line 3427
    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOldFolderState:I

    .line 3428
    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v8, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "sub_lcd_auto_lock"

    invoke-static {v2, v4, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_3

    .line 3430
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 3431
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_3

    .line 3432
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 3438
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-nez v2, :cond_9

    .line 3439
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v4, "onConfigurationChanged. mMainVideoView is null"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3440
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    .line 3455
    :goto_3
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 3456
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 3457
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->showPlayerList(Z)V

    goto :goto_0

    :cond_4
    move v2, v4

    .line 3414
    goto :goto_1

    .line 3417
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v4, v3

    :cond_6
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen(I)V

    goto :goto_2

    .line 3421
    :cond_7
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v8, :cond_8

    move v4, v3

    :cond_8
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->setScreenOrientation(I)V

    goto :goto_2

    .line 3442
    :cond_9
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_b

    .line 3443
    iget v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    if-ne v2, v8, :cond_a

    .line 3444
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->prepareChangeView(I)V

    .line 3452
    :goto_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeDialogView()V

    goto :goto_3

    .line 3446
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenOrientation()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->prepareChangeView(I)V

    goto :goto_4

    .line 3449
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->prepareChangeView(I)V

    goto :goto_4

    .line 3459
    :cond_c
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hidePlayerList(Z)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 397
    const-string v0, "VerificationLog"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 401
    sput-boolean v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    .line 402
    sput-boolean v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z

    .line 403
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    .line 405
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->stopOtherPresentationServices()V

    .line 406
    invoke-static {}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->init()V

    .line 408
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setVolumeControlStream(I)V

    .line 409
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_create()V

    .line 411
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 412
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 413
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setContext(Landroid/content/Context;)V

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPauseSet()V

    .line 416
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->bindService()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate. bindService fail"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->finish()V

    .line 421
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 422
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setContext(Landroid/content/Context;)V

    .line 424
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setContext(Landroid/content/Context;)V

    .line 426
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setVUtilsData(Landroid/content/Context;)V

    .line 427
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setExitListener(Lcom/sec/android/app/videoplayer/common/VideoFileInfo$onFinishListener;)V

    .line 429
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDirectShare:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    .line 430
    new-instance v0, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    .line 432
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->checkSKTCloudServiceFeature()V

    .line 434
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_1

    .line 435
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_1

    .line 436
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSKTCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->initTcloud()V

    .line 442
    :cond_1
    const-string v0, "display"

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 444
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 445
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 446
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 452
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v0, :cond_2

    .line 453
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 454
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    .line 458
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    .line 459
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;->setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdObserver:Lcom/sec/android/app/videoplayer/util/wfd/WfdObserver;

    .line 462
    new-instance v0, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBezelMgr:Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    .line 464
    const v0, 0x7f030028

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setContentView(I)V

    .line 465
    const v0, 0x7f0d0177

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    .line 467
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 468
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 471
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityCreated:Z

    .line 472
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    .line 473
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    .line 474
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExit:Z

    .line 475
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOldFolderState:I

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenuVisibilityListener:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;

    if-nez v0, :cond_4

    .line 478
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenuVisibilityListener:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$MenuVisibilityListener;

    .line 481
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setOnNotificationListener(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnNotificationListener;)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setOnSvcNotificationListener(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;)V

    .line 484
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->init(Landroid/content/Intent;)V

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 488
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 491
    :cond_5
    new-instance v0, Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 493
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 494
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 508
    :cond_6
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initMoviePlayer()V

    .line 509
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerBroadcastRecieverExt(Z)V

    .line 510
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerWatchONReceiver()V

    .line 511
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerLocalReceiver()V

    .line 512
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerPackageReceiver()V

    .line 513
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onCreate() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/16 v2, 0xd7

    .line 1655
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onCreateOptionsMenu"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1657
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1660
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1661
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setInfo(Landroid/content/Context;)V

    .line 1663
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3029
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy() - start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3031
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3032
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->unBindSAService()V

    .line 3035
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3036
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V

    .line 3039
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v1

    if-eqz v1, :cond_c

    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    if-eqz v1, :cond_c

    .line 3040
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy() :: stop presentation"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3042
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_2

    .line 3043
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removeHandlerMessage()V

    .line 3045
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v1, :cond_3

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_17

    .line 3046
    :cond_3
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_4

    .line 3047
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->segSurfaceExists(Z)V

    .line 3048
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 3051
    :cond_4
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_5

    .line 3052
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->segSurfaceExists(Z)V

    .line 3053
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 3056
    :cond_5
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_6

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_9

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceExists()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_9

    .line 3058
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1, v4, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 3060
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    if-nez v1, :cond_8

    .line 3061
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastPausedPosition(Z)V

    .line 3063
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    .line 3071
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 3072
    :cond_a
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 3075
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopMoviPlay4Presentation()V

    .line 3076
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopSecondaryDisplay()V

    .line 3079
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_d

    .line 3080
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 3082
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3085
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 3086
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 3090
    :cond_e
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_f

    .line 3091
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3096
    :cond_f
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->reset()V

    .line 3098
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    .line 3099
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    .line 3100
    sput-boolean v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    .line 3107
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-eqz v1, :cond_10

    .line 3108
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->removeDrmMgrClient()V

    .line 3111
    :cond_10
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_11

    .line 3112
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->statusBarSetting(Z)V

    .line 3115
    :cond_11
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->removeAllPopup()V

    .line 3117
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v1, :cond_12

    .line 3118
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVto:Landroid/view/ViewTreeObserver;

    if-eqz v1, :cond_12

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVto:Landroid/view/ViewTreeObserver;

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 3122
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVto:Landroid/view/ViewTreeObserver;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mGolbalLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 3130
    :cond_12
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterLocalReceiver()V

    .line 3131
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterPackageReceiver()V

    .line 3133
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 3134
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    .line 3138
    :cond_13
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v1, v2}, Landroid/hardware/display/DisplayManager;->unregisterDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;)V

    .line 3140
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerBroadcastRecieverExt(Z)V

    .line 3141
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterWatchONReceiver()V

    .line 3143
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityCreated:Z

    .line 3144
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 3146
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_14

    .line 3147
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    .line 3150
    :cond_14
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->unRegisterObserver()V

    .line 3152
    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    if-eqz v1, :cond_15

    .line 3153
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->releaseMainView()V

    .line 3156
    :cond_15
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_16

    .line 3157
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    if-eqz v1, :cond_16

    .line 3158
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->releaseListView()V

    .line 3159
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .line 3171
    :cond_16
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 3172
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy() - end"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3173
    :goto_1
    return-void

    .line 3066
    :cond_17
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onPause() - does not load VideoSurface"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3067
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto :goto_1

    .line 3092
    :catch_0
    move-exception v0

    .line 3093
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onDestroy : mStatusReceiver already unregistered"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onFinish()V
    .locals 3

    .prologue
    const/16 v2, 0xca

    .line 6946
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onFinish E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6948
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 6949
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 6950
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 643
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 644
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onNewIntent()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setIntent(Landroid/content/Intent;)V

    .line 647
    sget-object v0, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 720
    :goto_0
    return-void

    .line 651
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 652
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_d

    .line 653
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_2

    .line 654
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->segSurfaceExists(Z)V

    .line 655
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 657
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_3

    .line 658
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->segSurfaceExists(Z)V

    .line 659
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setSurfaceHolder(Landroid/view/SurfaceHolder;)V

    .line 661
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceExists()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_7

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 664
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v2, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    if-nez v0, :cond_6

    .line 667
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastPausedPosition(Z)V

    .line 669
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    .line 677
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 678
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 681
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopMoviPlay4Presentation()V

    .line 682
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StopSecondaryDisplay()V

    .line 684
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_a

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 689
    :cond_a
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 694
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 695
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 698
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 699
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onNewIntent() - call connect. Do not play video."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a0067

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 701
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 672
    :cond_d
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onPause() - does not load VideoSurface"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 705
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_f

    .line 706
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mRootLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 709
    :cond_f
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 710
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xcf

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 713
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_11

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPauseSet()V

    .line 717
    :cond_11
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->init(Landroid/content/Intent;)V

    .line 718
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->invalidateOptionsMenu()V

    .line 719
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initMoviePlayer()V

    goto/16 :goto_0
.end method

.method public onNotification(IF)V
    .locals 5
    .param p1, "notification"    # I
    .param p2, "param"    # F

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 5688
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5689
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 5822
    :cond_0
    :goto_0
    return-void

    .line 5691
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-nez v0, :cond_2

    if-nez p1, :cond_0

    .line 5694
    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 5696
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    .line 5697
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto :goto_0

    .line 5701
    :pswitch_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_7

    .line 5702
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->playerStop()V

    .line 5707
    :cond_3
    :goto_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_4

    .line 5708
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 5710
    :cond_4
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_5

    .line 5711
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 5714
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->getID()I

    move-result v0

    if-ne v0, v1, :cond_6

    .line 5715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 5718
    :cond_6
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    goto :goto_0

    .line 5703
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_3

    .line 5704
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->playerStop()V

    goto :goto_1

    .line 5722
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v0

    if-nez v0, :cond_8

    .line 5723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_8

    .line 5724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    .line 5728
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_9

    .line 5729
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V

    .line 5730
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->resetSubtitleSettings()V

    .line 5734
    :cond_9
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_a

    .line 5735
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 5737
    :cond_a
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_b

    .line 5738
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 5740
    :cond_b
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    goto/16 :goto_0

    .line 5744
    :pswitch_4
    cmpl-float v0, p2, v2

    if-ltz v0, :cond_d

    .line 5745
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 5746
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setSpeedTextViewVisibility(ZF)V

    goto/16 :goto_0

    .line 5747
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5748
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setSpeedTextViewVisibility(ZF)V

    goto/16 :goto_0

    .line 5751
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_e

    .line 5752
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->setSpeedTextViewVisibility(ZF)V

    goto/16 :goto_0

    .line 5753
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 5754
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v3, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setSpeedTextViewVisibility(ZF)V

    goto/16 :goto_0

    .line 5760
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5761
    cmpl-float v0, p2, v2

    if-nez v0, :cond_f

    .line 5762
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 5764
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    float-to-int v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 5770
    :pswitch_6
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startAudioOnlyView()V

    goto/16 :goto_0

    .line 5774
    :pswitch_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isAudioOnlyViewShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5775
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideAudioOnlyView()V

    goto/16 :goto_0

    .line 5780
    :pswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateTitle()V

    goto/16 :goto_0

    .line 5784
    :pswitch_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5785
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    goto/16 :goto_0

    .line 5790
    :pswitch_a
    cmpl-float v0, p2, v4

    if-nez v0, :cond_10

    .line 5791
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->resumeSmartPause()V

    goto/16 :goto_0

    .line 5793
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->blockSmartPause()V

    goto/16 :goto_0

    .line 5798
    :pswitch_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5799
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTVOutView()V

    goto/16 :goto_0

    .line 5804
    :pswitch_c
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5805
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateAllShareBtn()V

    goto/16 :goto_0

    .line 5810
    :pswitch_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 5811
    cmpl-float v0, p2, v4

    if-nez v0, :cond_11

    .line 5812
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showStateView()V

    goto/16 :goto_0

    .line 5814
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideStateView()V

    goto/16 :goto_0

    .line 5694
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 10
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const-wide/16 v8, 0x64

    const/16 v6, 0xcd

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1720
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 1722
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isDestroyed()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1918
    :goto_0
    return v3

    .line 1725
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    .line 1726
    .local v2, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->getInstance()Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    move-result-object v1

    .line 1728
    .local v1, "menuExecutor":Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 1914
    :pswitch_0
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onOptionsItemSelected(). getItemId(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    move v3, v4

    .line 1918
    goto :goto_0

    .line 1730
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->download(Landroid/net/Uri;)V

    goto :goto_1

    .line 1734
    :pswitch_2
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1735
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->sendToOtherDevices(Landroid/content/Context;)V

    goto :goto_1

    .line 1737
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    invoke-virtual {v3, p0, v6, v7}, Lcom/sec/android/app/videoplayer/slink/SLink;->sendToOtherDevicesLocalContents(Landroid/content/Context;J)V

    goto :goto_1

    .line 1743
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1744
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getSubMenu()Landroid/view/SubMenu;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createMenuEditDialog(Landroid/view/SubMenu;)V

    goto :goto_1

    .line 1752
    :pswitch_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1754
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v5}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto :goto_1

    .line 1759
    :pswitch_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1761
    :cond_5
    sget v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1762
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4, v3}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto :goto_1

    .line 1764
    :cond_6
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 1765
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 1771
    :pswitch_6
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1773
    :cond_7
    const/4 v3, 0x2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v5}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto/16 :goto_1

    .line 1777
    :pswitch_7
    const/4 v3, 0x4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v3, v5}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto/16 :goto_1

    .line 1781
    :pswitch_8
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1782
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    const v5, 0x7f0a00a3

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a00fb

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, p0, v5, v6}, Lcom/sec/android/app/videoplayer/slink/SLink;->downloadCurrentFile(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1784
    :cond_8
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v3

    if-nez v3, :cond_9

    .line 1785
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->getShareViaIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showShareViaMenu(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 1787
    :cond_9
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->openShareVia(Landroid/content/Context;)V

    goto/16 :goto_1

    .line 1793
    :pswitch_9
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1795
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    if-nez v3, :cond_b

    .line 1796
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .line 1798
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->showPopup()V

    goto/16 :goto_1

    .line 1802
    :pswitch_a
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const-string v5, "com.sec.android.app.videoplayer"

    const-string v6, "CPRV"

    invoke-virtual {v3, v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_c

    .line 1805
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1806
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v5, 0x36ee80

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1809
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->callPreviewDialog()V

    goto/16 :goto_1

    .line 1814
    :pswitch_b
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v3

    if-nez v3, :cond_d

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1815
    :cond_d
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 1818
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setAudioPathDevice()V

    .line 1819
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1820
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z

    goto/16 :goto_1

    .line 1824
    :pswitch_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1826
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1827
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z

    .line 1828
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v3

    const/16 v5, 0xa

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 1830
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isBluetoothDeviceConnected()Z

    move-result v3

    if-eqz v3, :cond_10

    .line 1831
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setAudioPathBT()V

    .line 1832
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v3, v5, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1835
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v3

    if-nez v3, :cond_f

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1836
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    goto/16 :goto_1

    .line 1858
    :cond_10
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->callBTDevicePickerActivity()V

    goto/16 :goto_1

    .line 1863
    .end local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :pswitch_d
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1865
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1866
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-nez v3, :cond_12

    .line 1867
    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    const v6, 0x7f0b0005

    invoke-direct {v3, v5, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 1872
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->show()V

    goto/16 :goto_1

    .line 1869
    :cond_12
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->updateDialog()V

    goto :goto_2

    .line 1873
    :cond_13
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getHasSubtitleFile()Z

    move-result v3

    if-eqz v3, :cond_15

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleCount()I

    move-result v3

    if-nez v3, :cond_15

    .line 1874
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    if-nez v3, :cond_14

    .line 1875
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .line 1878
    :cond_14
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    new-instance v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$6;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->setOnDismissListener(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;)V

    .line 1886
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->show()V

    goto/16 :goto_1

    .line 1889
    :cond_15
    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 1890
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->createSelectSubtitlePopup()V

    goto/16 :goto_1

    .line 1895
    :pswitch_e
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->showPopup()V

    .line 1896
    :cond_16
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeHoverPopup()V

    goto/16 :goto_1

    .line 1900
    :pswitch_f
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1902
    :cond_17
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    if-eqz v3, :cond_18

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->checkContentChanged(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 1903
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    .line 1906
    :cond_18
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    if-nez v3, :cond_19

    .line 1907
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    goto/16 :goto_1

    .line 1909
    :cond_19
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDetailsPopup:Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoDetailsPopup;->show()V

    goto/16 :goto_1

    .line 1728
    :pswitch_data_0
    .packed-switch 0x7f0d0210
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_7
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method protected onPause()V
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v9, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 2777
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "onPause() - start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2778
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 2780
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v2, :cond_0

    .line 2781
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 2784
    :cond_0
    sput-boolean v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    .line 2785
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsPrecessingSideMirroring:Z

    .line 2786
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bScoverclosedWhenOnResume:Z

    .line 2787
    sput-boolean v7, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 2788
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->dropLCDfps(Z)Z

    .line 2790
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2791
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->dismissDeletePopup()V

    .line 2794
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isLongSeekMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2795
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2798
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 2800
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2801
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v3, "onPause() autoRotation is off - disable mOrientationEventListener"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2802
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v2}, Landroid/view/OrientationEventListener;->disable()V

    .line 2805
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->stopSmartPause()V

    .line 2807
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2808
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v2}, Landroid/view/Menu;->close()V

    .line 2811
    :cond_4
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_5

    .line 2812
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hidePlayerList(Z)V

    .line 2813
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->releasePlayerList()V

    .line 2816
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2817
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    .line 2819
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2820
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 2824
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v2, :cond_7

    .line 2825
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 2828
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2830
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->releaseCpuBooster()V

    .line 2831
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->releaseBusBooster()V

    .line 2833
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    const/16 v6, 0x1a

    invoke-virtual {v2, v3, v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 2834
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    const/16 v6, 0xbb

    invoke-virtual {v2, v3, v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 2836
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_8

    .line 2837
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->statusBarSetting(Z)V

    .line 2839
    :cond_8
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2840
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2841
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 2844
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 2845
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopBufferingChecker()V

    .line 2846
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removeHandlerMessage()V

    .line 2849
    :cond_b
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v2

    if-nez v2, :cond_1d

    .line 2850
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateVideoWidget()V

    .line 2858
    :cond_c
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 2859
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLastPlayedItem()V

    .line 2874
    :cond_d
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    .line 2875
    sput-boolean v7, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    .line 2883
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v2, :cond_e

    .line 2884
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 2885
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 2889
    :cond_e
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v2, :cond_f

    .line 2890
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2891
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v2, v7}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setPowerWakeLock(Z)V

    .line 2895
    :cond_f
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v2

    if-eqz v2, :cond_20

    .line 2896
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bNotAllowedScreenMirroringPopup:Z

    if-nez v2, :cond_1e

    .line 2897
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->showNotification()V

    .line 2902
    :goto_1
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_1f

    .line 2903
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 2908
    :cond_10
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v4, :cond_11

    sget-boolean v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v2, :cond_11

    .line 2909
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v4, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 2910
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 2911
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 2912
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    .line 2946
    :cond_11
    :goto_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_12

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v2, :cond_12

    .line 2947
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeView(Landroid/view/View;)V

    .line 2950
    :cond_12
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_13

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2951
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 2954
    :cond_13
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    if-nez v2, :cond_14

    .line 2955
    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveSubtitleSyncTime(I)V

    .line 2958
    :cond_14
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->unregisterRemoteControlReceiver()V

    move-wide v2, v0

    move v6, v5

    .line 2960
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2961
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->dismissDialogPopup()V

    .line 2962
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->saveActivityPreferences()V

    .line 2964
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 2967
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterContentObserver()V

    .line 2969
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v0, :cond_15

    .line 2970
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWfdUtil:Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    const/16 v1, 0x321

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->notifyChanged(II)V

    .line 2974
    :cond_15
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2975
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_16

    .line 2976
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 2979
    :cond_16
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->release()V

    .line 2980
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 2982
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_17

    .line 2983
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetCurrentResumePosition()V

    .line 2987
    :cond_17
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    .line 2988
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2989
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->removeAnimationEffects()V

    .line 2991
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_18

    .line 2992
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    .line 2995
    :cond_18
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v0, :cond_19

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 2996
    iput v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppWidth:I

    .line 2997
    iput v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPreviousAppHeight:I

    .line 3000
    :cond_19
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 3001
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    if-nez v1, :cond_1a

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1b

    :cond_1a
    move v7, v4

    :cond_1b
    invoke-virtual {v0, v7}, Lcom/sec/android/app/videoplayer/slink/SLink;->onPause(Z)V

    .line 3004
    :cond_1c
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onPause() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3005
    :goto_4
    return-void

    .line 2852
    :cond_1d
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bNotAllowedScreenMirroringPopup:Z

    if-nez v2, :cond_c

    .line 2853
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateVideoWidget()V

    goto/16 :goto_0

    .line 2899
    :cond_1e
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bNotAllowedScreenMirroringPopup:Z

    goto/16 :goto_1

    .line 2904
    :cond_1f
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v2, :cond_10

    .line 2905
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSurfaceHolder:Landroid/view/SurfaceHolder;

    goto/16 :goto_2

    .line 2914
    :cond_20
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_21

    .line 2915
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->showNotification()V

    goto/16 :goto_3

    .line 2917
    :cond_21
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v2, :cond_22

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v2, :cond_28

    .line 2918
    :cond_22
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v2

    if-nez v2, :cond_23

    .line 2919
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_23

    .line 2920
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onPause()V

    .line 2924
    :cond_23
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_24

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v2

    if-nez v2, :cond_25

    :cond_24
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v2, :cond_11

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceExists()Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_25
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_11

    .line 2926
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    .line 2927
    .local v8, "pm":Landroid/os/PowerManager;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v4, :cond_27

    invoke-virtual {v8}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v2

    if-eqz v2, :cond_27

    .line 2928
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v4, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 2933
    :goto_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v2

    if-eqz v2, :cond_26

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    if-nez v2, :cond_26

    .line 2934
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->broadcastPausedPosition(Z)V

    .line 2936
    :cond_26
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    goto/16 :goto_3

    .line 2930
    :cond_27
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2, v4, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    goto :goto_5

    .line 2940
    .end local v8    # "pm":Landroid/os/PowerManager;
    :cond_28
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onPause() - does not load VideoSurface"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2941
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_4
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0d0223

    const/4 v4, 0x0

    .line 1667
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onPrepareOptionsMenu E"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    if-eqz p1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelp()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1671
    :cond_1
    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onPrepareOptionsMenu. menu is null or Lock State or from help screen"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1716
    :goto_0
    return v4

    .line 1675
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v5, :cond_3

    .line 1676
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1677
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowingPopupMenu()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1678
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v6, 0x36ee80

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1683
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v5, :cond_4

    .line 1684
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeHandler()V

    .line 1685
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->resetHoldLongSeek()V

    .line 1688
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v5, :cond_5

    .line 1689
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeHandler()V

    .line 1690
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->resetHoldLongSeek()V

    .line 1693
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    if-nez v5, :cond_6

    .line 1694
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initSettingsPopup()V

    .line 1695
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->setAllVisible()V

    .line 1698
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v0

    .line 1700
    .local v0, "helper":Lcom/sec/android/app/videoplayer/util/MenuHelper;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    .line 1701
    .local v2, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    .line 1703
    .local v3, "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    sget-object v1, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    .line 1705
    .local v1, "menuMode":Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1706
    :cond_7
    sget-object v1, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->EXCEPTIONAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    .line 1709
    :cond_8
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMode(Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;)V

    .line 1710
    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->updateMenus(Landroid/view/Menu;)V

    .line 1712
    sget-object v5, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    if-eq v1, v5, :cond_9

    invoke-virtual {v0, p1, v7}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getMenuItemVisibility(Landroid/view/Menu;I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1713
    :cond_9
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->updateSettingPopup()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {v0, p1, v7, v4}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1716
    :cond_a
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v4

    goto/16 :goto_0
.end method

.method protected onResume()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/16 v11, 0xa

    const/4 v12, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2316
    const-string v6, "VerificationLog"

    const-string v9, "onResume"

    invoke-static {v6, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2317
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() - start"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2318
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 2320
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_0

    .line 2321
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->invalidate()V

    .line 2323
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2324
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v6, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isBluetoothOn()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2325
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothHeadsetServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-virtual {v6, p0, v9, v12}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2326
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume. Getting Headset Proxy failed"

    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2330
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 2332
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    .line 2334
    .local v5, "vUtils":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2335
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() autoRotation is off - enable mOrientationEventListener"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2336
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v6}, Landroid/view/OrientationEventListener;->enable()V

    .line 2339
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSourceRunning()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2340
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " onResume() mSideSyncInfo.isSinkRunning() = "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v9

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    const v6, 0x7f0a00f5

    invoke-virtual {v5, v6, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    .line 2342
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    .line 2706
    :goto_0
    return-void

    .line 2346
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2347
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/slink/SLink;->onResume()V

    .line 2350
    :cond_4
    sget-boolean v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    if-eqz v6, :cond_5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPlusCallActive()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2351
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() - Resume from pause state in Wuacall mode"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2352
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 2355
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 2356
    sput-boolean v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z

    .line 2357
    sput-boolean v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    .line 2358
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bScoverclosedWhenOnResume:Z

    .line 2360
    invoke-virtual {v5, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->dropLCDfps(Z)Z

    .line 2362
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->removeAnimationEffects()V

    .line 2364
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v6

    const/16 v9, 0x308

    const-string v10, "SAMSUNG_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v5, v6, v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    .line 2365
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowBackgroundColor(Landroid/view/Window;)V

    .line 2367
    sput-boolean v8, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z

    .line 2369
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 2371
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v6

    if-nez v6, :cond_6

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2372
    :cond_6
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() - reset Sound effect"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "sound_effect"

    invoke-virtual {v6, v9, v11}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 2385
    :cond_7
    :goto_1
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v6, :cond_8

    .line 2386
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v6, :cond_8

    .line 2387
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->registerMotionListener()V

    .line 2390
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    sget-boolean v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    if-nez v6, :cond_a

    .line 2391
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "onResume() - call connect. Do not play video."

    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2392
    const v6, 0x7f0a0067

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 2393
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z

    .line 2394
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 2374
    :cond_9
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-nez v6, :cond_7

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v6

    if-nez v6, :cond_7

    .line 2375
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() - reset Sound effect"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2376
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v6, :cond_7

    .line 2377
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "sound_effect"

    invoke-virtual {v6, v9, v11}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v4

    .line 2379
    .local v4, "sa":I
    const/16 v6, 0xb

    if-le v4, v6, :cond_7

    .line 2380
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "sound_effect"

    invoke-virtual {v6, v9, v11}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto :goto_1

    .line 2398
    .end local v4    # "sa":I
    :cond_a
    sput-boolean v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z

    .line 2399
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setServiceContext()V

    .line 2401
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-nez v6, :cond_b

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-nez v6, :cond_b

    .line 2402
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v7, "onResume() - does not load VideoSurface"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 2407
    :cond_b
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v6, :cond_c

    .line 2408
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 2410
    :cond_c
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v6, :cond_d

    .line 2411
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 2415
    :cond_d
    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_e

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v6

    if-eqz v6, :cond_22

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v6

    if-nez v6, :cond_22

    .line 2417
    :cond_e
    const/16 v6, 0x1a

    invoke-virtual {v5, p0, v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 2424
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->resetMode()V

    .line 2425
    const-string v6, "video"

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setBatteryADC(Ljava/lang/String;Z)V

    .line 2426
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkPrevScreenmode()V

    .line 2428
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v6, :cond_f

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v6

    if-nez v6, :cond_f

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v6, :cond_f

    .line 2429
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " onResume() - screenmode: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v11, "screen_mode_settings"

    invoke-virtual {v10, v11, v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2431
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "screen_mode_settings"

    invoke-virtual {v6, v9, v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v12, :cond_23

    .line 2432
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v12, v6}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    .line 2433
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "screen_mode_settings"

    invoke-virtual {v6, v9, v12}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 2440
    :cond_f
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    if-eqz v6, :cond_10

    .line 2441
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v9, 0xd5

    const-wide/16 v10, 0x1f4

    invoke-virtual {v6, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2442
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v9, 0xd4

    const-wide/16 v10, 0x1f4

    invoke-virtual {v6, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2445
    :cond_10
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    .line 2446
    sput-boolean v8, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->MoviePlayerOnResume:Z

    .line 2447
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExitByAppinApp:Z

    .line 2449
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v6

    if-nez v6, :cond_11

    .line 2450
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_11

    .line 2451
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChangeViewDone(Z)V

    .line 2454
    :cond_11
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_12

    .line 2455
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->applyAllSettings()V

    .line 2458
    :cond_12
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v6, :cond_13

    .line 2459
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v6

    if-eqz v6, :cond_13

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setPowerWakeLock(Z)V

    .line 2462
    :cond_13
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v6, :cond_14

    .line 2463
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_14

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->isShowing()Z

    move-result v6

    if-eqz v6, :cond_14

    .line 2464
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->dismiss()V

    .line 2465
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iput-object v13, v6, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 2469
    :cond_14
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v6, :cond_27

    .line 2470
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_25

    .line 2471
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_24

    move v6, v7

    :goto_4
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    .line 2482
    :goto_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerContentObserver()V

    .line 2485
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mExternalDisplayManager:Landroid/hardware/display/DisplayManager;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDisplayListener:Landroid/hardware/display/DisplayManager$DisplayListener;

    invoke-virtual {v6, v9, v13}, Landroid/hardware/display/DisplayManager;->registerDisplayListener(Landroid/hardware/display/DisplayManager$DisplayListener;Landroid/os/Handler;)V

    .line 2488
    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsCoverOpen:Z

    if-eqz v6, :cond_15

    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_15

    .line 2489
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 2492
    :cond_15
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->dismissVolumePanel()V

    .line 2494
    iget v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I

    if-gt v6, v8, :cond_29

    iget v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I

    if-eq v6, v12, :cond_29

    .line 2496
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    if-nez v6, :cond_16

    .line 2497
    new-instance v6, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .line 2500
    :cond_16
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 2501
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->show()V

    .line 2626
    :cond_17
    :goto_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_18

    .line 2627
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateMWFontSize()V

    .line 2631
    :cond_18
    sget-wide v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    const-wide/16 v12, 0x0

    cmp-long v6, v10, v12

    if-ltz v6, :cond_19

    .line 2632
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onResume() - resumeTime : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-wide v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    sget-wide v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    invoke-virtual {v6, v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 2634
    const-wide/16 v10, -0x1

    sput-wide v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mWatchOnResumeTime:J

    .line 2638
    :cond_19
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_40

    move v2, v7

    .line 2639
    .local v2, "mNewMode":I
    :goto_7
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onResume() - mCurrentMode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCurrentMode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mNewMode : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2641
    iget v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCurrentMode:I

    if-eq v6, v2, :cond_1a

    .line 2642
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeBtnControllerView()V

    .line 2643
    iput v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCurrentMode:I

    .line 2646
    :cond_1a
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_1b

    .line 2647
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onResume()V

    .line 2648
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setControllerUpdate()V

    .line 2649
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    .line 2650
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 2653
    :cond_1b
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v6, :cond_1c

    .line 2654
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->startSubtitle()V

    .line 2657
    :cond_1c
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->createPlayArrayList()V

    .line 2659
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v6, :cond_1d

    .line 2660
    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPlaylistSetCompleted:Z

    if-nez v6, :cond_1d

    .line 2661
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setPlayerList()V

    .line 2662
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bPlaylistSetCompleted:Z

    .line 2666
    :cond_1d
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDirectShare:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->continueDirectShare()V

    .line 2668
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_1e

    .line 2669
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTagBuddy(Z)V

    .line 2673
    :cond_1e
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_41

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v6

    if-eqz v6, :cond_41

    .line 2674
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v8, 0xd0

    const-wide/16 v10, 0x1f4

    invoke-virtual {v6, v8, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2675
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_1f

    .line 2676
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 2686
    :cond_1f
    :goto_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v6

    if-eqz v6, :cond_20

    .line 2687
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_20

    .line 2688
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 2692
    :cond_20
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->startSmartPause()V

    .line 2694
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v6, :cond_21

    .line 2695
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v6

    const-string v8, "newlyLaunched"

    invoke-virtual {v6, v8, v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 2703
    :cond_21
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getActivityPreferences()V

    .line 2704
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v7, "onResume() - end"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2705
    const-string v6, "VerificationLog"

    const-string v7, "Executed"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2419
    .end local v2    # "mNewMode":I
    :cond_22
    const/16 v6, 0x1a

    invoke-virtual {v5, p0, v6, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 2420
    const/4 v6, 0x3

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v9

    invoke-virtual {v5, p0, v6, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 2421
    const/16 v6, 0xbb

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v9

    invoke-virtual {v5, p0, v6, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    goto/16 :goto_2

    .line 2435
    :cond_23
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v7, v6}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    .line 2436
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v9, "screen_mode_settings"

    invoke-virtual {v6, v9, v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto/16 :goto_3

    :cond_24
    move v6, v8

    .line 2471
    goto/16 :goto_4

    .line 2474
    :cond_25
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_26

    move v6, v7

    :goto_9
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen(I)V

    goto/16 :goto_5

    :cond_26
    move v6, v8

    goto :goto_9

    .line 2478
    :cond_27
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_28

    move v6, v7

    :goto_a
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto/16 :goto_5

    :cond_28
    move v6, v8

    goto :goto_a

    .line 2503
    :cond_29
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() - startPlayback()"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2506
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 2507
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->stopAppInApp()V

    .line 2510
    :cond_2a
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v6

    if-nez v6, :cond_2d

    .line 2512
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hideTVOutView()V

    .line 2514
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v6, :cond_2b

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_2b

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v6, v9, :cond_2b

    .line 2516
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v6, :cond_2e

    .line 2517
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v6, :cond_2b

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v6, v9, :cond_2b

    .line 2518
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 2524
    :cond_2b
    :goto_b
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v6, :cond_2c

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_2c

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v6, v9, :cond_2c

    .line 2526
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v6, :cond_2f

    .line 2527
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v6, :cond_2c

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v6, v9, :cond_2c

    .line 2528
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 2535
    :cond_2c
    :goto_c
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v6, :cond_2d

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_2d

    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eq v6, v9, :cond_2d

    .line 2537
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2544
    :cond_2d
    :goto_d
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlayPathValid()Z

    move-result v6

    if-nez v6, :cond_30

    .line 2545
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v7, "onResume() - file path to play is not valid."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2546
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6, v13}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 2547
    const v6, 0x7f0a0076

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 2548
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    goto/16 :goto_0

    .line 2521
    :cond_2e
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_b

    .line 2539
    :catch_0
    move-exception v1

    .line 2540
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_d

    .line 2531
    .end local v1    # "e":Ljava/lang/IllegalStateException;
    :cond_2f
    :try_start_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    sget-object v9, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v6, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_c

    .line 2552
    :cond_30
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v6, :cond_32

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isDirectDmcMode()Z

    move-result v6

    if-eqz v6, :cond_32

    .line 2553
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setDirectDmcMode(Z)V

    .line 2555
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-eqz v6, :cond_31

    .line 2556
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume(). 3Box-DirectDmc mode"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2557
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->startChangePlayer()V

    goto/16 :goto_6

    .line 2559
    :cond_31
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume(). DirectDmc mode"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2560
    invoke-virtual {v5, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->setPlayerMode(I)V

    .line 2561
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayDmr(Z)V

    goto/16 :goto_6

    .line 2563
    :cond_32
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v6

    if-eqz v6, :cond_33

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isExternalDisplay()Z

    move-result v6

    if-nez v6, :cond_33

    .line 2564
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayDmr(Z)V

    goto/16 :goto_6

    .line 2566
    :cond_33
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->checkExceptionalCaseDuringDlna()Z

    .line 2567
    const/4 v3, 0x0

    .line 2569
    .local v3, "overlayMagnificationEnabled":Z
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v6, :cond_34

    .line 2570
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "accessibility_magnifier"

    invoke-static {v6, v9, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v6

    if-ne v6, v8, :cond_37

    move v3, v8

    .line 2574
    :cond_34
    :goto_e
    sget-boolean v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-nez v6, :cond_3b

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isExternalDisplay()Z

    move-result v6

    if-eqz v6, :cond_3b

    if-nez v3, :cond_3b

    .line 2575
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v6

    if-eqz v6, :cond_3a

    .line 2576
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    if-eq v6, v8, :cond_35

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    if-nez v6, :cond_38

    .line 2578
    :cond_35
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() PresentationServiceRunning!! Do not start SecondaryDisplay"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2579
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->hideNotification(Z)V

    .line 2580
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_36

    .line 2581
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChangeViewDone(Z)V

    .line 2596
    :cond_36
    :goto_f
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->stopOtherPresentationServices()V

    goto/16 :goto_6

    :cond_37
    move v3, v7

    .line 2570
    goto :goto_e

    .line 2583
    :cond_38
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v6

    if-ne v6, v12, :cond_36

    .line 2584
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() PresentationServiceRunning!! PLAYER_STOP"

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_39

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v6

    if-eqz v6, :cond_39

    .line 2587
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 2591
    :goto_10
    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v6

    if-nez v6, :cond_36

    .line 2592
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 2593
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto :goto_f

    .line 2589
    :cond_39
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    goto :goto_10

    .line 2598
    :cond_3a
    sget-object v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v9, "onResume() startSecondaryDisplay E."

    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2599
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->EnablePresentationMode()V

    .line 2600
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartSecondaryDisplay(Ljava/lang/Boolean;)V

    .line 2601
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;

    const/16 v9, 0xd2

    invoke-virtual {v6, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_6

    .line 2603
    :cond_3b
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v6

    if-nez v6, :cond_3c

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSdp()Z

    move-result v6

    if-eqz v6, :cond_3d

    :cond_3c
    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_17

    :cond_3d
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v6

    if-nez v6, :cond_17

    .line 2604
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v6, v10, v12

    if-nez v6, :cond_3e

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentResumePosition()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v6, v10, v12

    if-eqz v6, :cond_3e

    .line 2605
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    .line 2615
    :cond_3e
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v6

    if-eqz v6, :cond_3f

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v6

    if-nez v6, :cond_3f

    .line 2616
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->bScoverclosedWhenOnResume:Z

    .line 2617
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    goto/16 :goto_6

    .line 2619
    :cond_3f
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto/16 :goto_6

    .end local v3    # "overlayMagnificationEnabled":Z
    :cond_40
    move v2, v8

    .line 2638
    goto/16 :goto_7

    .line 2677
    .restart local v2    # "mNewMode":I
    :cond_41
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_42

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_42

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v6

    if-eqz v6, :cond_42

    .line 2678
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachHelpClip()V

    goto/16 :goto_8

    .line 2679
    :cond_42
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_43

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_43

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpSmartPause()Z

    move-result v6

    if-eqz v6, :cond_43

    .line 2680
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachSmartPause()V

    goto/16 :goto_8

    .line 2681
    :cond_43
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_1f

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_1f

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 2682
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachMotionPeekHelp()V

    goto/16 :goto_8
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/16 v2, 0xd7

    .line 2116
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 2117
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onStart() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2119
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerBroadcastReciever()V

    .line 2121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBezelMgr:Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->registerListener()V

    .line 2144
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v0, :cond_1

    .line 2145
    new-instance v0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$14;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    .line 2237
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v0, :cond_2

    .line 2238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->connect()V

    .line 2241
    :cond_2
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onStart() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2242
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2246
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onStop() - start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2248
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 2251
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V

    .line 2255
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    .line 2258
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->close()V

    .line 2261
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2262
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2264
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mB2BSoultionReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_4

    .line 2265
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mB2BSoultionReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2267
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_5

    .line 2268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtMediaBtnReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2270
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDockReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_6

    .line 2271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDockReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2273
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_7

    .line 2274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSPenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2276
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMultiWindowBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_8

    .line 2277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMultiWindowBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2279
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mChangePlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_9

    .line 2280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mChangePlayerBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2282
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_b

    .line 2283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_a

    .line 2284
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 2285
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 2287
    :cond_a
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    .line 2290
    :cond_b
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v0, :cond_c

    .line 2291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v0, :cond_c

    .line 2292
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->unregisterMotionListener()V

    .line 2302
    :cond_c
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v0

    if-nez v0, :cond_d

    .line 2303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2304
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->unBindSAService()V

    .line 2308
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBezelMgr:Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/sconnect/BezelMgr;->unregisterListener()V

    .line 2309
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z

    .line 2310
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 2311
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "onStop() - end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2312
    return-void
.end method

.method public onSvcNotification(ILjava/lang/String;)V
    .locals 11
    .param p1, "action"    # I
    .param p2, "uriStr"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v10, 0x0

    .line 4175
    iget v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAction:I

    if-eq v3, p1, :cond_0

    .line 4176
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onSvcNotification - action : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4178
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAction:I

    .line 4180
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v3

    if-nez v3, :cond_2

    .line 4503
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 4182
    :cond_2
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 4395
    :sswitch_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 4396
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4399
    :cond_3
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    if-nez v3, :cond_1

    .line 4400
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V

    .line 4401
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z

    goto :goto_0

    .line 4184
    :sswitch_2
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    if-nez v3, :cond_1

    .line 4186
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 4187
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3, v10}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    .line 4196
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 4197
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V

    .line 4200
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4201
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 4202
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 4205
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 4206
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 4210
    :cond_7
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_8

    .line 4211
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setControllerUpdate()V

    .line 4214
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 4215
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    .line 4218
    :cond_9
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_1

    .line 4219
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->runOrStopHelpClip()V

    goto/16 :goto_0

    .line 4224
    :sswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->preProcessForPlaySpeed()V

    .line 4226
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_a

    .line 4227
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateAllShareBtn()V

    .line 4230
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_b

    .line 4231
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->startUpdateProgress()V

    .line 4234
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setChangeViewDone(Z)V

    .line 4236
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_c

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v3, :cond_c

    .line 4237
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 4238
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateCaptureBtn()V

    .line 4242
    :cond_c
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v3, :cond_e

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v3

    if-nez v3, :cond_e

    .line 4243
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    .line 4244
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v4

    .line 4247
    .local v4, "videoId":J
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isServerOnlyContent(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_e

    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v3, :cond_d

    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isServerOnlyContent(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_e

    .line 4249
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_e

    .line 4250
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTagBuddy(J)V

    .line 4255
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v4    # "videoId":J
    :cond_e
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_f

    .line 4256
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getFPS()I

    move-result v3

    const/16 v6, 0x28

    if-le v3, v6, :cond_15

    .line 4257
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->dropLCDfps(Z)Z

    .line 4263
    :cond_f
    :goto_1
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v3, :cond_10

    .line 4264
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    new-instance v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$26;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$26;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-static {v3, v6}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->setDataExtractionListener(Landroid/view/View;Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;)Z

    .line 4278
    :cond_10
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v3, :cond_11

    .line 4279
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    new-instance v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$27;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$27;-><init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    invoke-static {v3, v6}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->setDataExtractionListener(Landroid/view/View;Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;)Z

    .line 4293
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isAIAMWNotSupportedOnHEVC1080P()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 4294
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 4295
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    if-eqz v3, :cond_12

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHEVCPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-nez v3, :cond_13

    .line 4296
    :cond_12
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createPlayContinueDialog()V

    .line 4301
    :cond_13
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_14

    .line 4302
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveRecentlyPlayedTime()V

    .line 4305
    :cond_14
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isActivityPauseState()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4306
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_1

    .line 4307
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->showNotification()V

    goto/16 :goto_0

    .line 4259
    :cond_15
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->dropLCDfps(Z)Z

    goto :goto_1

    .line 4312
    :sswitch_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_1

    .line 4313
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    goto/16 :goto_0

    .line 4318
    :sswitch_5
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->doOnCompleteAction()V

    goto/16 :goto_0

    .line 4322
    :sswitch_6
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onSvcNotification. PLAYBACK_DRM_SHOW_ACQUIRING_POPUP"

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4324
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_16

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_16

    .line 4325
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 4327
    :cond_16
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z

    if-eqz v3, :cond_17

    .line 4328
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onSvcNotification. mLicErrorRelatedPopupShow is true"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4330
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4331
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 4333
    :cond_17
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onSvcNotification - receive PLAYBACK_DRM_SHOW_ACQUIRING_POPUP 1"

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4335
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    .line 4336
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a019f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 4337
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v8}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 4338
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3, v10}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 4340
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    .line 4341
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    goto/16 :goto_0

    .line 4346
    :sswitch_7
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "onSvcNotification - PLAYBACK_DRM_DISMISS_ACQUIRING_POPUP."

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4348
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4349
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    goto/16 :goto_0

    .line 4362
    :sswitch_8
    sget v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    iput v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopupType:I

    .line 4363
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onSvcNotification - receive DRM popup. mDrmPopupType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopupType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4365
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    if-nez v3, :cond_18

    .line 4366
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    iget v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopupType:I

    invoke-direct {v3, p0, v6}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    .line 4372
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->show()V

    goto/16 :goto_0

    .line 4368
    :cond_18
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    iget v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopupType:I

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->setType(I)V

    .line 4369
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mDrmPopup:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->createDialog()V

    goto :goto_2

    .line 4406
    :sswitch_9
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    if-nez v3, :cond_19

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1a

    .line 4407
    :cond_19
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v6, 0xc9

    const-wide/16 v8, 0x320

    invoke-virtual {v3, v6, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 4408
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->restartSubtitle()V

    .line 4413
    :goto_3
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleViaAIA:Z

    goto/16 :goto_0

    .line 4410
    :cond_1a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->initSubtitle()V

    goto :goto_3

    .line 4430
    :sswitch_a
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const v6, 0x7f0a017c

    invoke-virtual {v3, v6, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    .line 4431
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startAudioOnlyView()V

    .line 4433
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_1

    .line 4434
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    goto/16 :goto_0

    .line 4440
    :sswitch_b
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startAudioOnlyView()V

    .line 4441
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_1

    .line 4442
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    goto/16 :goto_0

    .line 4447
    :sswitch_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const v6, 0x7f0a017a

    invoke-virtual {v3, v6, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto/16 :goto_0

    .line 4455
    :sswitch_d
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_1b

    .line 4456
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePausePlayBtn()V

    .line 4459
    :cond_1b
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_1

    .line 4460
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateOneFrameForwardBackwardBtn()V

    goto/16 :goto_0

    .line 4465
    :sswitch_e
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MoviePlaybackService.UPDATE_SURFACE_SECURE_MODE. uriStr : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " path : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Path;->INTERNAL_ROOT_PATH:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4466
    const/4 v0, 0x0

    .line 4467
    .local v0, "isSecure":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 4470
    .local v1, "mfilePath":Ljava/lang/String;
    if-eqz p2, :cond_1c

    const-string v3, "com.sec.videoplayer.SECURE_MODE_UPDATE"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 4471
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MoviePlaybackService.UPDATE_SURFACE_SECURE_MODE. mfilePath : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4473
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 4474
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "MoviePlaybackService.UPDATE_SURFACE_SECURE_MODE. drm file!"

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4475
    const/4 v0, 0x1

    .line 4479
    :cond_1c
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MoviePlaybackService.UPDATE_SURFACE_SECURE_MODE. mSecureMode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", isSecure :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4481
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    if-eq v3, v0, :cond_1

    .line 4482
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    .line 4484
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v6, "MoviePlaybackService.UPDATE_SURFACE_SECURE_MODE. mode change so re-set"

    invoke-static {v3, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4486
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v3, :cond_1d

    .line 4487
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setVisibility(I)V

    .line 4488
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setSecure(Z)V

    .line 4489
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v3, v10}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setVisibility(I)V

    .line 4492
    :cond_1d
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v3, :cond_1

    .line 4493
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setVisibility(I)V

    .line 4494
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSecureMode:Z

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setSecure(Z)V

    .line 4495
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v3, v10}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setVisibility(I)V

    goto/16 :goto_0

    .line 4182
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7fffbffa -> :sswitch_1
        -0x7fffbff9 -> :sswitch_1
        -0x17d4 -> :sswitch_1
        -0x177d -> :sswitch_1
        -0x177c -> :sswitch_1
        -0x177b -> :sswitch_1
        -0x3fd -> :sswitch_1
        -0x3f2 -> :sswitch_1
        -0x3ef -> :sswitch_1
        -0x3ed -> :sswitch_1
        -0x63 -> :sswitch_1
        -0x62 -> :sswitch_1
        0x65 -> :sswitch_5
        0x66 -> :sswitch_2
        0x6d -> :sswitch_1
        0x6e -> :sswitch_6
        0x6f -> :sswitch_7
        0x70 -> :sswitch_1
        0x71 -> :sswitch_1
        0x72 -> :sswitch_1
        0x74 -> :sswitch_8
        0x75 -> :sswitch_8
        0x76 -> :sswitch_8
        0x77 -> :sswitch_8
        0x78 -> :sswitch_8
        0x79 -> :sswitch_8
        0x7a -> :sswitch_8
        0x7b -> :sswitch_8
        0x7c -> :sswitch_8
        0x82 -> :sswitch_9
        0x83 -> :sswitch_0
        0x85 -> :sswitch_3
        0x8c -> :sswitch_4
        0x8e -> :sswitch_d
        0x8f -> :sswitch_e
        0xc8 -> :sswitch_1
        0x3b6 -> :sswitch_c
        0x3b7 -> :sswitch_a
        0x3cd -> :sswitch_b
        0x2714 -> :sswitch_1
    .end sparse-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 3381
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isChangeViewDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 3383
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    const/4 v3, 0x0

    .line 6995
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 6996
    const/16 v0, 0x28

    if-lt p1, v0, :cond_6

    .line 6997
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6998
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;

    invoke-interface {v0}, Landroid/view/Menu;->close()V

    .line 7001
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 7002
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubShare:Landroid/view/SubMenu;

    invoke-interface {v0}, Landroid/view/SubMenu;->close()V

    .line 7005
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_3

    .line 7006
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_2

    .line 7007
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 7009
    :cond_2
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    .line 7012
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    if-eqz v0, :cond_4

    .line 7013
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->dismiss()V

    .line 7014
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSelectSubtitlePopup:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .line 7017
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    if-eqz v0, :cond_5

    .line 7018
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->dismiss()V

    .line 7019
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleInvalidPopup:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    .line 7022
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    if-eqz v0, :cond_6

    .line 7023
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 7024
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 7027
    :cond_6
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 2

    .prologue
    .line 6918
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v1, "LeaveByUser"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6919
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mStopped:Z

    if-eqz v0, :cond_0

    .line 6920
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    .line 6924
    :goto_0
    return-void

    .line 6922
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasFocus"    # Z

    .prologue
    const/16 v4, 0xcb

    .line 6485
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onWindowFocusChanged() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6487
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setMoviePlayerWindowFocus(Z)V

    .line 6489
    if-eqz p1, :cond_4

    .line 6490
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 6491
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 6516
    :cond_0
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_1

    .line 6517
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 6519
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_2

    .line 6520
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 6522
    :cond_2
    return-void

    .line 6492
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6493
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 6496
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowingPopupMenu()Z

    move-result v1

    if-nez v1, :cond_5

    .line 6498
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;

    const/16 v2, 0xd8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 6502
    :cond_5
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_6

    .line 6503
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 6506
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v1, :cond_0

    .line 6507
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6508
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 6511
    :catch_0
    move-exception v0

    .line 6512
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "onWindowFocusChanged - IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public performAction(I)V
    .locals 4
    .param p1, "which"    # I

    .prologue
    .line 1964
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1966
    .local v1, "selectedId":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->getInstance()Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;

    move-result-object v0

    .line 1968
    .local v0, "menuExecutor":Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;
    packed-switch v1, :pswitch_data_0

    .line 1997
    :goto_0
    return-void

    .line 1971
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1973
    :cond_0
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto :goto_0

    .line 1978
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1980
    :cond_1
    sget v2, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1981
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto :goto_0

    .line 1983
    :cond_2
    new-instance v2, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/popup/EditGuidePopup;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    .line 1984
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mEditPopup:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 1990
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 1992
    :cond_3
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/cmd/MenuExecutor;->execute(ILandroid/content/Context;)V

    goto :goto_0

    .line 1968
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0215
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public registerBroadcastRecieverExt(Z)V
    .locals 5
    .param p1, "register"    # Z

    .prologue
    .line 3338
    if-eqz p1, :cond_2

    .line 3339
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBroadcastReceiverExRegistered:Z

    if-eqz v4, :cond_1

    .line 3378
    :cond_0
    :goto_0
    return-void

    .line 3342
    :cond_1
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 3343
    .local v3, "intentFilterVolume":Landroid/content/IntentFilter;
    const-string v4, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v3, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3344
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3347
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 3348
    .local v2, "intentFilterTrim":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.START_VIDEO_FROM_TRIM"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3349
    const-string v4, "file"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 3350
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTrimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3353
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 3354
    .local v1, "intentFilterSlink":Landroid/content/IntentFilter;
    const-string v4, "com.samsung.android.sdk.samsunglink.DOWNLOAD_RESULT_BROADCAST_ACTION"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3355
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSlinkBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3357
    new-instance v0, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 3358
    .local v0, "intentFilterMedia":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3359
    const-string v4, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3360
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3361
    const-string v4, "android.intent.action.MEDIA_CHECKING"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 3362
    const-string v4, "file"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 3363
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 3365
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBroadcastReceiverExRegistered:Z

    goto :goto_0

    .line 3367
    .end local v0    # "intentFilterMedia":Landroid/content/IntentFilter;
    .end local v1    # "intentFilterSlink":Landroid/content/IntentFilter;
    .end local v2    # "intentFilterTrim":Landroid/content/IntentFilter;
    .end local v3    # "intentFilterVolume":Landroid/content/IntentFilter;
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBroadcastReceiverExRegistered:Z

    if-eqz v4, :cond_0

    .line 3369
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVolumeChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3371
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTrimReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3373
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSlinkBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3375
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMediaReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 3376
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBroadcastReceiverExRegistered:Z

    goto :goto_0
.end method

.method public rotateScreen()V
    .locals 1

    .prologue
    .line 5867
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    .line 5868
    return-void
.end method

.method public rotateScreen(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    .line 5871
    move v0, p1

    .line 5873
    .local v0, "mode":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 5874
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getUserOrientation()I

    move-result v0

    .line 5875
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rotateScreen : From GUIDE TOUR! use user orentation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5887
    :cond_0
    :goto_0
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rotateScreen : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 5889
    if-eq v0, v5, :cond_1

    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    const/16 v2, 0x9

    if-eq v0, v2, :cond_1

    const/16 v2, 0x8

    if-ne v0, v2, :cond_3

    .line 5895
    :cond_1
    if-ne v0, v5, :cond_9

    .line 5896
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setScreenOrientation(I)V

    .line 5900
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setRequestedOrientation(I)V

    .line 5904
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_4

    .line 5905
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateControllerBtns()V

    .line 5907
    :cond_4
    return-void

    .line 5876
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 5877
    const/4 v0, 0x1

    goto :goto_0

    .line 5878
    :cond_6
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->isMotionPeekEnabled()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-boolean v2, v2, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    if-ne v2, v1, :cond_7

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v2

    if-nez v2, :cond_7

    .line 5880
    const/4 v0, 0x1

    goto :goto_0

    .line 5881
    :cond_7
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_360:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->get360Video(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 5882
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 5883
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isAutoRotation(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5884
    const/4 v0, 0x4

    goto/16 :goto_0

    .line 5898
    :cond_9
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setScreenOrientation(I)V

    goto :goto_1
.end method

.method public rotateSecondScreen()V
    .locals 1

    .prologue
    .line 5955
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenOrientation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen(I)V

    .line 5956
    return-void
.end method

.method public rotateSecondScreen(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    .line 5959
    move v0, p1

    .line 5961
    .local v0, "mode":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5962
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSecondScreenUserOrientation()I

    move-result v0

    .line 5963
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "rotateScreen : From GUIDE TOUR! use user orentation : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5968
    :cond_0
    :goto_0
    if-eq v0, v1, :cond_1

    if-eqz v0, :cond_1

    if-ne v0, v5, :cond_6

    .line 5969
    :cond_1
    if-ne v0, v5, :cond_5

    .line 5970
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenLandscape(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v1, 0x0

    :cond_2
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSecondScreenOrientation(I)V

    .line 5975
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setRequestedOrientation(I)V

    .line 5979
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_3

    .line 5980
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    .line 5982
    :cond_3
    return-void

    .line 5964
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSecondScreenAutoRotation(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 5965
    const/4 v0, 0x4

    goto :goto_0

    .line 5972
    :cond_5
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->setSecondScreenOrientation(I)V

    goto :goto_1

    .line 5977
    :cond_6
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v2, "rotateSecondScreen : invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected saveActivityPreferences()V
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->saveSubtitleSetting()V

    .line 736
    :cond_0
    return-void
.end method

.method public setFrom(Landroid/content/Intent;)I
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1226
    const/4 v6, -0x1

    .line 1228
    .local v6, "whereFrom":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v7, :cond_0

    .line 1229
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_GALLERY:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isContentExternal()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1230
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_SETUP_WIZARD:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1231
    const/16 v6, 0x6b

    .line 1299
    :cond_0
    :goto_0
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "init() - setFrom : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    return v6

    .line 1232
    :cond_1
    const-string v7, "android.intent.action.SECURE_LOCK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v8, "WhereFrom"

    const/4 v9, -0x1

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    const/16 v8, 0x74

    if-ne v7, v8, :cond_3

    .line 1233
    :cond_2
    const/16 v6, 0x74

    goto :goto_0

    .line 1235
    :cond_3
    const/16 v6, 0x64

    goto :goto_0

    .line 1237
    :cond_4
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_WIDGET:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1238
    const/16 v6, 0xc8

    goto :goto_0

    .line 1239
    :cond_5
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_PHOTORING:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1240
    const/16 v6, 0x75

    goto :goto_0

    .line 1241
    :cond_6
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v7, :cond_8

    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_VZW_GUIDE_TOUR:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1242
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "init() - START_VIDEO_FROM_GUIDED_TOUR"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1243
    const/16 v6, 0x65

    .line 1244
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v8, -0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1245
    const-string v7, "isLandscape"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v7, 0x0

    :goto_1
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->setUserOrientation(I)V

    goto :goto_0

    :cond_7
    const/4 v7, 0x1

    goto :goto_1

    .line 1247
    :cond_8
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_PROGRESS_BAR_PREVIEW:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1248
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "init() - FROM_HELP_VIDEO_PROGRESS_BAR_PREVIEW"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1249
    const/16 v6, 0x68

    .line 1250
    const-string v7, "type"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1251
    .local v5, "type":Ljava/lang/String;
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setHelpHoverType(Ljava/lang/String;)V

    .line 1252
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v8, 0x1a

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1253
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v8, -0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1254
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.resource://"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1255
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    const-string v8, "file:///system/media/video/video_help.mp4"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    goto/16 :goto_0

    .line 1257
    .end local v5    # "type":Ljava/lang/String;
    :cond_9
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_SMART_PAUSE:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1258
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "init() - FROM_HELP_VIDEO_SMART_PAUSE"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    const/16 v6, 0x69

    .line 1260
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v8, 0x1a

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1261
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v8, -0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1262
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.resource://"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1263
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    const-string v8, "file:///system/media/video/video_help.mp4"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    goto/16 :goto_0

    .line 1265
    :cond_a
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_VIDEO_MOTION_PEEK:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1266
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "init() - FROM_HELP_VIDEO_MOTION_PEEK"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1267
    const/16 v6, 0x6c

    .line 1268
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v8, 0x1a

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1269
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-wide/16 v8, -0x1

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1270
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.resource://"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1271
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    const-string v8, "file:///system/media/video/video_help.mp4"

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    goto/16 :goto_0

    .line 1273
    :cond_b
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 1274
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    const-string v8, "init() - FROM_HELP_CLIP"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1275
    const/16 v6, 0x6a

    .line 1277
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_TIME_STEMP:Ljava/lang/String;

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v3

    .line 1278
    .local v3, "timeStemp":[J
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_DESCRIPTION:Ljava/lang/String;

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1279
    .local v0, "description":[Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_CLIP_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1280
    .local v4, "title":[Ljava/lang/String;
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_HELP_START_CLIP:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1282
    .local v2, "startClip":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getInstance()Lcom/sec/android/app/videoplayer/view/help/HelpData;

    move-result-object v7

    invoke-virtual {v7, v3, v0, v4, v2}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->setHelpData([J[Ljava/lang/String;[Ljava/lang/String;I)V

    .line 1284
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    const/16 v8, 0x1a

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 1285
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    aget-wide v8, v3, v2

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_0

    .line 1286
    .end local v0    # "description":[Ljava/lang/String;
    .end local v2    # "startClip":I
    .end local v3    # "timeStemp":[J
    .end local v4    # "title":[Ljava/lang/String;
    :cond_c
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_ASP_NEAR_BY_DEVICE:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 1287
    const/16 v6, 0x71

    goto/16 :goto_0

    .line 1288
    :cond_d
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v7, :cond_e

    const-string v7, "ActionVideoMinimodeService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1289
    const-string v7, "WhereFrom"

    const/4 v8, -0x1

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    goto/16 :goto_0

    .line 1290
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isContentExternal()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 1291
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1292
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "/storage"

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1293
    const/16 v6, 0x66

    goto/16 :goto_0

    .line 1294
    .end local v1    # "path":Ljava/lang/String;
    :cond_f
    sget-object v7, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->FROM_MYFILES:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1295
    const/16 v6, 0x73

    goto/16 :goto_0
.end method

.method public setHelpHoverType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 6931
    if-eqz p1, :cond_0

    .line 6932
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHelpPrevType:Ljava/lang/String;

    .line 6934
    :cond_0
    return-void
.end method

.method public setSubtitleSetting(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0
    .param p1, "dialog"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    .line 6941
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 6942
    return-void
.end method

.method public setTutorialWidget(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 6530
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTutorialCheck:Z

    .line 6531
    return-void
.end method

.method public showPlayerList(Z)V
    .locals 2
    .param p1, "animate"    # Z

    .prologue
    .line 6953
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPlayerListView:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->showList(ZZ)V

    .line 6954
    return-void
.end method

.method public showSplitSubtitle()V
    .locals 2

    .prologue
    .line 6961
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSplitSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSplitView;->showView(Z)V

    .line 6962
    return-void
.end method

.method public updateMWFontSize()V
    .locals 3

    .prologue
    .line 5575
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    if-eqz v0, :cond_0

    .line 5576
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setMWFontSize(II)V

    .line 5578
    :cond_0
    return-void
.end method

.method public updateSplitSubtitleView()V
    .locals 0

    .prologue
    .line 6970
    return-void
.end method

.method public updateTitle()V
    .locals 5

    .prologue
    .line 5581
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 5583
    .local v0, "fileTitle":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v1

    .line 5585
    .local v1, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isFromStore()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isTrailer()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 5586
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 5592
    :cond_1
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 5593
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v0

    .line 5596
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTitle(). title : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5598
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v2, :cond_3

    .line 5599
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitle(Ljava/lang/String;)V

    .line 5601
    :cond_3
    return-void

    .line 5587
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideSyncInfo:Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isTitleExist()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 5588
    invoke-static {}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->getTitleofSideSync()Ljava/lang/String;

    move-result-object v0

    .line 5589
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTitle(mSideSyncInfo.getTitleofSideSync())"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
