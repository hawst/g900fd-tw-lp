.class public Lcom/sec/android/app/videoplayer/service/NotificationMgr;
.super Ljava/lang/Object;
.source "NotificationMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotifcationAction;,
        Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;,
        Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

.field private mContext:Landroid/content/Context;

.field private mImageUpdater:Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

.field private mNotificationReceiver:Landroid/content/BroadcastReceiver;

.field private mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

.field protected mShowing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/service/NotificationMgr;Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/NotificationMgr;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->createVideoThumbnail(Ljava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    return-object v0
.end method

.method private createVideoThumbnail(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "duration"    # J

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f080088

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 310
    .local v2, "size":I
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 312
    .local v1, "retriever":Landroid/media/MediaMetadataRetriever;
    const-wide/16 v4, -0x1

    .line 314
    .local v4, "thumbnailTime":J
    const-wide/16 v6, 0x3a98

    cmp-long v3, p2, v6

    if-ltz v3, :cond_1

    .line 315
    const-wide/32 v4, 0xe4e1c0

    .line 319
    :cond_0
    :goto_0
    const/4 v3, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v1, v2, v2, v3, v6}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 320
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v1, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 328
    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_4

    .line 334
    :goto_1
    return-object v0

    .line 316
    :cond_1
    const-wide/16 v6, 0x3e8

    cmp-long v3, p2, v6

    if-ltz v3, :cond_0

    .line 317
    const-wide/32 v4, 0xf4240

    goto :goto_0

    .line 322
    :catch_0
    move-exception v3

    .line 328
    :try_start_2
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 329
    :catch_1
    move-exception v3

    goto :goto_1

    .line 324
    :catch_2
    move-exception v3

    .line 328
    :try_start_3
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 329
    :catch_3
    move-exception v3

    goto :goto_1

    .line 327
    :catchall_0
    move-exception v3

    .line 328
    :try_start_4
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_5

    .line 331
    :goto_2
    throw v3

    .line 329
    :catch_4
    move-exception v3

    goto :goto_1

    :catch_5
    move-exception v6

    goto :goto_2
.end method

.method private registerNotificationReceiver()V
    .locals 3

    .prologue
    .line 129
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 130
    sget-object v1, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    const-string v2, "registerNotificationReceiver E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v1, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;-><init>(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    .line 197
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 198
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "videoplayer.notification.prev"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 199
    const-string v1, "videoplayer.notification.next"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 200
    const-string v1, "videoplayer.notification.playpause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 201
    const-string v1, "videoplayer.notification.ffdown"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 202
    const-string v1, "videoplayer.notification.ffup"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 203
    const-string v1, "videoplayer.notification.rewdown"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    const-string v1, "videoplayer.notification.rewup"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 205
    const-string v1, "videoplayer.notification.close"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 212
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 214
    .end local v0    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private unregisterNotificationReceiver()V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationReceiver:Landroid/content/BroadcastReceiver;

    .line 224
    :cond_0
    return-void
.end method


# virtual methods
.method public addCallback(Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    .line 346
    return-void
.end method

.method public getLaunchPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/NotificationView;->getLaunchPendingIntent()Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public getRemoteView()Landroid/widget/RemoteViews;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/sec/android/app/videoplayer/service/NotificationView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/service/NotificationView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/NotificationView;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    return-object v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 118
    sget-object v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    const-string v1, "hide"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mImageUpdater:Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mImageUpdater:Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->cancel(Z)Z

    .line 121
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    .line 122
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->unregisterNotificationReceiver()V

    .line 123
    return-void
.end method

.method public removeCallback()V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    .line 353
    return-void
.end method

.method public setAllView(Lcom/sec/android/app/videoplayer/common/VideoFileInfo;)V
    .locals 2
    .param p1, "fileInfo"    # Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    if-nez v0, :cond_0

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    const-string v1, "setAllView"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setThumbnail(Landroid/graphics/Bitmap;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setTitle(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    .line 99
    new-instance v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;-><init>(Lcom/sec/android/app/videoplayer/service/NotificationMgr;Lcom/sec/android/app/videoplayer/common/VideoFileInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mImageUpdater:Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mImageUpdater:Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public setButtons(Landroid/os/IBinder;)V
    .locals 1
    .param p1, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setButtons(Landroid/os/IBinder;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    .line 75
    return-void
.end method

.method public setPlayStatus(Z)V
    .locals 1
    .param p1, "isPlaying"    # Z

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setPlayStatus(Z)Lcom/sec/android/app/videoplayer/service/NotificationView;

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;

    const-string v1, "show"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->registerNotificationReceiver()V

    .line 112
    :cond_0
    return-void
.end method
