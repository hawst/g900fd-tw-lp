.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 3397
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    const/16 v5, 0x780

    const/16 v3, 0x438

    const/4 v4, 0x0

    .line 3399
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TouchPlayer :: mInfoListener. info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3401
    sparse-switch p2, :sswitch_data_0

    .line 3517
    :cond_0
    :goto_0
    return v4

    .line 3403
    :sswitch_0
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setOneFrameSeekEnable(Z)V

    goto :goto_0

    .line 3408
    :sswitch_1
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setFileMediaType(I)V

    .line 3409
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    goto :goto_0

    .line 3414
    :sswitch_2
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setFileMediaType(I)V

    .line 3415
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # invokes: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V

    .line 3417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3418
    const-string v0, "MoviePlaybackService"

    const-string v1, "onInfo - MEDIA_INFO_NO_VIDEO / MEDIA_INFO_UNSUPPORTED_VIDEO : hide state view for local contents"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3419
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    goto :goto_0

    .line 3424
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3425
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 3426
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->notifyPlayer(I)V

    goto :goto_0

    .line 3432
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3433
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 3434
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->notifyPlayer(I)V

    goto/16 :goto_0

    .line 3440
    :sswitch_5
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onInfo - MEDIA_INFO_BUFFERING_TOAST : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3441
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateStatePercentView(I)V

    .line 3442
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.stateview.visibility"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "visibility"

    const-string v3, "show"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 3447
    :sswitch_6
    const-string v0, "MoviePlaybackService"

    const-string v1, "onInfo - MEDIA_INFO_BUFFERING_START"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3448
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.stateview.visibility"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "visibility"

    const-string v3, "show"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 3453
    :sswitch_7
    const-string v0, "MoviePlaybackService"

    const-string v1, "onInfo - MEDIA_INFO_BUFFERING_END : hide state view"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3454
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.stateview.visibility"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "visibility"

    const-string v3, "hide"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 3457
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    goto/16 :goto_0

    .line 3461
    :sswitch_8
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SLSI_JA_CHIP:Z

    if-nez v0, :cond_4

    .line 3466
    :goto_1
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 3467
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3302(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 3468
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I

    .line 3470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v1

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    if-ge v0, v5, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    if-ge v0, v3, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    if-ge v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$3400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I

    move-result v0

    if-lt v0, v5, :cond_0

    .line 3472
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setAIAMWNotSupportedOnHEVC1080P(Z)V

    goto/16 :goto_0

    .line 3462
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->acquireCpuBooster()V

    .line 3463
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->acquireBusBooster()V

    goto :goto_1

    .line 3478
    :sswitch_9
    const-string v0, "MoviePlaybackService"

    const-string v1, "MEDIA_INFO_VIDEO_RENDERING_START. E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3479
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    goto/16 :goto_0

    .line 3483
    :sswitch_a
    const-string v0, "MoviePlaybackService"

    const-string v1, "MEDIA_INFO_SUBTITLE_TIMED_OUT E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3485
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    if-eqz v0, :cond_0

    .line 3486
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;->oSubtitleInfo(I)V

    goto/16 :goto_0

    .line 3491
    :sswitch_b
    const-string v0, "MoviePlaybackService"

    const-string v1, "MEDIA_INFO_UNSUPPORTED_SUBTITLE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3494
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    if-eqz v0, :cond_0

    .line 3495
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;->oSubtitleInfo(I)V

    goto/16 :goto_0

    .line 3500
    :sswitch_c
    const-string v0, "MoviePlaybackService"

    const-string v1, "MEDIA_INFO_METADATA_UPDATE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3502
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getOutbandSubtilteTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->addOutbandTextTrackInfo([Landroid/media/MediaPlayer$TrackInfo;)V

    .line 3503
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$5;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->startSubtitle()V

    .line 3505
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    if-eqz v0, :cond_0

    .line 3506
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->mOnSubtitleInfoListener:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;

    invoke-interface {v0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil$OnSubtitleInfoListener;->oSubtitleInfo(I)V

    goto/16 :goto_0

    .line 3401
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_9
        0x2bd -> :sswitch_6
        0x2be -> :sswitch_7
        0x309 -> :sswitch_5
        0x322 -> :sswitch_c
        0x385 -> :sswitch_b
        0x386 -> :sswitch_a
        0x38e -> :sswitch_3
        0x38f -> :sswitch_4
        0x3b6 -> :sswitch_1
        0x3b7 -> :sswitch_2
        0x3b9 -> :sswitch_0
        0x3ca -> :sswitch_8
        0x3cc -> :sswitch_1
        0x3cd -> :sswitch_2
    .end sparse-switch
.end method
