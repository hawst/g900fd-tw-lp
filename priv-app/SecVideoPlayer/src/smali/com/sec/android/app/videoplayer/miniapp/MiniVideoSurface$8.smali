.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 638
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 6
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 640
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ">>>>>>> mini surfaceChanged <<<<<<<"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mTargetState:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    move v1, v2

    .line 643
    .local v1, "isValidState":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoWidth:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v4

    if-ne v4, p3, :cond_3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mVideoHeight:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v4

    if-ne v4, p4, :cond_3

    move v0, v2

    .line 644
    .local v0, "hasValidSize":Z
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 645
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 646
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSeekWhenPrepared:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    .line 648
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->start()V

    .line 650
    :cond_1
    return-void

    .end local v0    # "hasValidSize":Z
    .end local v1    # "isValidState":Z
    :cond_2
    move v1, v3

    .line 642
    goto :goto_0

    .restart local v1    # "isValidState":Z
    :cond_3
    move v0, v3

    .line 643
    goto :goto_1
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>>>>>> mini surfaceCreated <<<<<<<"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 655
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->openVideo()V

    .line 656
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ">>>>>>> mini surfaceDestroyed <<<<<<<"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1802(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resetSubtitleController()V

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$8;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->release(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)V

    .line 663
    return-void
.end method
