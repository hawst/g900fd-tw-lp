.class public Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
.super Landroid/widget/RelativeLayout;
.source "PlayerListView.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/RelativeLayout;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoPlayerListView"


# instance fields
.field final HANDLE_NOTIFY_DATA_CHANGED:I

.field public mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

.field private mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mContext:Landroid/content/Context;

.field mControllerWidth:I

.field mDm:Landroid/util/DisplayMetrics;

.field mHandler:Landroid/os/Handler;

.field mHashMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mHideAniListener:Landroid/view/animation/Animation$AnimationListener;

.field mListHeight:I

.field mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field mListLeftPadding:I

.field mListRightPadding:I

.field private mListView:Landroid/widget/GridView;

.field mListWidth:I

.field mMainHeight:I

.field mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field mMainWidth:I

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPlayerListShowState:Z

.field private mPlayerListView:Landroid/widget/RelativeLayout;

.field mProgressLayout:Landroid/widget/LinearLayout;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mShowAniListener:Landroid/view/animation/Animation$AnimationListener;

.field mSurfaceHeight:I

.field mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field mTitleLayout:Landroid/widget/LinearLayout;

.field mTitlePadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 70
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    .line 53
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 54
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 60
    iput v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->HANDLE_NOTIFY_DATA_CHANGED:I

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 64
    iput v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListRightPadding:I

    iput v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    .line 67
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    .line 162
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$1;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 450
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$4;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 467
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$5;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 476
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$6;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mShowAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 520
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHideAniListener:Landroid/view/animation/Animation$AnimationListener;

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    .line 72
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 73
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->resetPlayerLayout()V

    return-void
.end method

.method private destroyHandler()V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    .line 237
    :cond_0
    return-void
.end method

.method private getColumnsCount()I
    .locals 3

    .prologue
    .line 155
    const/4 v0, 0x1

    .line 156
    .local v0, "n":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 157
    const/4 v0, 0x3

    .line 159
    :cond_0
    return v0
.end method

.method private getPixel(I)I
    .locals 1
    .param p1, "dimen"    # I

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method private initListView()V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d017c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const v1, 0x7f02008f

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(I)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setDrawingCacheQuality(I)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setTextFilterEnabled(Z)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->onWindowFocusChanged(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setNumberOfColumns()V

    .line 132
    return-void
.end method

.method private loadLayoutValues(Z)V
    .locals 1
    .param p1, "isLandscape"    # Z

    .prologue
    .line 322
    if-eqz p1, :cond_0

    .line 323
    const v0, 0x7f080211

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListWidth:I

    .line 324
    const v0, 0x7f0801ff

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    .line 325
    const v0, 0x7f080202

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListRightPadding:I

    .line 326
    const v0, 0x7f08020c

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    .line 327
    const v0, 0x7f080206

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    .line 335
    :goto_0
    const v0, 0x7f08015d

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mControllerWidth:I

    .line 336
    return-void

    .line 329
    :cond_0
    const v0, 0x7f0801fe

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListHeight:I

    .line 330
    const v0, 0x7f080200

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    .line 331
    const v0, 0x7f080203

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListRightPadding:I

    .line 332
    const v0, 0x7f08020d

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    .line 333
    const v0, 0x7f080207

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    goto :goto_0
.end method

.method private resetPlayerLayout()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 312
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 313
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 315
    .local v0, "resetMainLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 316
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 318
    .local v1, "resetSurfaceLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 319
    return-void
.end method

.method private setHandler()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$3;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    .line 231
    return-void
.end method

.method private setHorizontalSpacing()V
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const v1, 0x7f080138

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    .line 152
    return-void
.end method

.method private setImageCacheListener()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$2;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->setOnUpdatedListener(Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;)V

    .line 214
    return-void
.end method

.method private setLayoutParams(Z)V
    .locals 5
    .param p1, "isLandscape"    # Z

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0xa

    const/4 v2, -0x1

    .line 339
    if-eqz p1, :cond_0

    .line 340
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainWidth:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 341
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListWidth:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 342
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainWidth:I

    iget v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 345
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 349
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 360
    :goto_0
    return-void

    .line 352
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainHeight:I

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 353
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListHeight:I

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 354
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 357
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_0
.end method

.method private setListView()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mProgressLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d017b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mProgressLayout:Landroid/widget/LinearLayout;

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    if-nez v0, :cond_1

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->initListView()V

    .line 117
    :cond_1
    return-void
.end method

.method private setupAdapter()V
    .locals 4

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 107
    return-void
.end method

.method private updatePlayerLayout(ZZ)V
    .locals 12
    .param p1, "isLandscape"    # Z
    .param p2, "animate"    # Z

    .prologue
    const-wide/16 v10, 0x1f4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 364
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    const v5, 0x7f0d0178

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitleLayout:Landroid/widget/LinearLayout;

    .line 365
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 372
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->loadLayoutValues(Z)V

    .line 374
    if-eqz p1, :cond_1

    .line 375
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListWidth:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainWidth:I

    .line 376
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setLayoutParams(Z)V

    .line 378
    new-array v4, v9, [I

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    aput v5, v4, v7

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainWidth:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 379
    .local v1, "changeWidth":Landroid/animation/ValueAnimator;
    new-array v4, v9, [I

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v7

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 381
    .local v0, "changeHeight":Landroid/animation/ValueAnimator;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 382
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 384
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-direct {v2, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 385
    .local v2, "mAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mShowAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 400
    :goto_0
    if-eqz p2, :cond_2

    .line 401
    invoke-virtual {v2, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 402
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 403
    .local v3, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 404
    const-wide/16 v4, 0x19

    invoke-virtual {v3, v4, v5}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 405
    new-array v4, v9, [Landroid/animation/Animator;

    aput-object v1, v4, v7

    aput-object v0, v4, v8

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 407
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 427
    .end local v3    # "set":Landroid/animation/AnimatorSet;
    :cond_0
    :goto_1
    return-void

    .line 387
    .end local v0    # "changeHeight":Landroid/animation/ValueAnimator;
    .end local v1    # "changeWidth":Landroid/animation/ValueAnimator;
    .end local v2    # "mAnimation":Landroid/view/animation/Animation;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListHeight:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainHeight:I

    .line 388
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setLayoutParams(Z)V

    .line 390
    new-array v4, v9, [I

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v7

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainHeight:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 391
    .restart local v1    # "changeWidth":Landroid/animation/ValueAnimator;
    new-array v4, v9, [I

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v7

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 393
    .restart local v0    # "changeHeight":Landroid/animation/ValueAnimator;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 394
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 396
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainHeight:I

    int-to-float v4, v4

    invoke-direct {v2, v6, v6, v4, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 397
    .restart local v2    # "mAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mShowAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0

    .line 410
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v4, :cond_0

    .line 411
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 412
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 415
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v5, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 416
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 419
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    invoke-virtual {v4, v5, v7, v7, v7}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 420
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListRightPadding:I

    invoke-virtual {v4, v7, v7, v5, v7}, Landroid/widget/GridView;->setPadding(IIII)V

    .line 421
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitleLayout:Landroid/widget/LinearLayout;

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    iget v6, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListLeftPadding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mTitlePadding:I

    invoke-virtual {v4, v5, v7, v6, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 422
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setNumberOfColumns()V

    .line 423
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 424
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    goto/16 :goto_1
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 77
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 80
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f030029

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 437
    const/4 v0, -0x1

    .line 438
    .local v0, "count":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    if-nez v1, :cond_0

    .line 439
    const/4 v0, -0x1

    .line 443
    :goto_0
    return v0

    .line 441
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getPlayerListView()Landroid/widget/RelativeLayout;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method public hideList(ZZ)V
    .locals 12
    .param p1, "isLandscape"    # Z
    .param p2, "animate"    # Z

    .prologue
    const-wide/16 v10, 0x1f4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 263
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 266
    const/4 v2, 0x0

    .line 267
    .local v2, "mAnimation":Landroid/view/animation/Animation;
    const/4 v1, 0x0

    .local v1, "changeWidth":Landroid/animation/ValueAnimator;
    const/4 v0, 0x0

    .line 269
    .local v0, "changeHeight":Landroid/animation/ValueAnimator;
    if-eqz p2, :cond_1

    .line 270
    if-eqz p1, :cond_0

    .line 271
    const v4, 0x7f080206

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    .line 273
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "mAnimation":Landroid/view/animation/Animation;
    iget v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListWidth:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-direct {v2, v6, v4, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 274
    .restart local v2    # "mAnimation":Landroid/view/animation/Animation;
    new-array v4, v9, [I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainWidth:I

    aput v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 275
    new-array v4, v9, [I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    aput v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 276
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 277
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 288
    :goto_0
    invoke-virtual {v2, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 289
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListView:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 291
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 292
    .local v3, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v3, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-result-object v4

    new-array v5, v9, [Landroid/animation/Animator;

    aput-object v1, v5, v7

    aput-object v0, v5, v8

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 295
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHideAniListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 304
    .end local v3    # "set":Landroid/animation/AnimatorSet;
    :goto_1
    return-void

    .line 279
    :cond_0
    const v4, 0x7f080207

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getPixel(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    .line 281
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "mAnimation":Landroid/view/animation/Animation;
    iget v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListHeight:I

    int-to-float v4, v4

    invoke-direct {v2, v6, v6, v6, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 282
    .restart local v2    # "mAnimation":Landroid/view/animation/Animation;
    new-array v4, v9, [I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainHeight:I

    aput v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 283
    new-array v4, v9, [I

    iget v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mSurfaceHeight:I

    aput v5, v4, v7

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mDm:Landroid/util/DisplayMetrics;

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    aput v5, v4, v8

    invoke-static {v4}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 284
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeWidthAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 285
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mChangeHeightAniListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v4}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    goto :goto_0

    .line 297
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->resetPlayerLayout()V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 299
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    goto :goto_1
.end method

.method public isShowing()Z
    .locals 3

    .prologue
    .line 120
    const-string v0, "VideoPlayerListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isShowing. mPlayerListShowState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z

    return v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/CustomCursorLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/16 v1, 0x8

    .line 246
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 248
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mProgressLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    .line 255
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mProgressLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 48
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "arg0":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mAdapter:Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 260
    return-void
.end method

.method public releaseListView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    .line 140
    :cond_0
    return-void
.end method

.method public releasePlayerList()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->setOnUpdatedListener(Lcom/sec/android/app/videoplayer/playerlist/ImageCache$OnUpdatedListener;)V

    .line 99
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->destroyHandler()V

    .line 100
    return-void
.end method

.method public setNumberOfColumns()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mListView:Landroid/widget/GridView;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->getColumnsCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    .line 146
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setHorizontalSpacing()V

    .line 148
    :cond_0
    return-void
.end method

.method public setPlayerList()V
    .locals 3

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setListView()V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->setContext(Landroid/content/Context;)V

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setImageCacheListener()V

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setupAdapter()V

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 94
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->setHandler()V

    .line 95
    return-void
.end method

.method public showList(ZZ)V
    .locals 3
    .param p1, "isLandscape"    # Z
    .param p2, "animate"    # Z

    .prologue
    .line 307
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->updatePlayerLayout(ZZ)V

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 309
    return-void
.end method

.method public updatePlayerListView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 434
    :cond_0
    return-void
.end method
