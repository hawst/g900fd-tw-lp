.class Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;
.super Landroid/os/AsyncTask;
.source "NotificationMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/NotificationMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotificationImageUpdateTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/service/NotificationMgr;Lcom/sec/android/app/videoplayer/common/VideoFileInfo;)V
    .locals 0
    .param p2, "fileInfo"    # Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 248
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    .line 249
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->isCancelled()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x0

    .line 276
    :cond_0
    :goto_0
    return-object v3

    .line 258
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v1

    .line 259
    .local v1, "curFileUri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v0

    .line 260
    .local v0, "curFilePath":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "doInBackground. curFileUri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", curFilePath : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const/4 v3, 0x0

    .line 264
    .local v3, "imageNotification":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 267
    .local v2, "fileUri":Ljava/lang/String;
    sget-object v4, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 268
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-static {}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getInstance()Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDurationTime(Landroid/net/Uri;)J

    move-result-wide v6

    # invokes: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->createVideoThumbnail(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    invoke-static {v4, v0, v6, v7}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$300(Lcom/sec/android/app/videoplayer/service/NotificationMgr;Ljava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 269
    :cond_2
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 270
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v5

    int-to-long v6, v5

    # invokes: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->createVideoThumbnail(Ljava/lang/String;J)Landroid/graphics/Bitmap;
    invoke-static {v4, v0, v6, v7}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$300(Lcom/sec/android/app/videoplayer/service/NotificationMgr;Ljava/lang/String;J)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0

    .line 271
    :cond_3
    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$100(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v3

    goto/16 :goto_0

    .line 273
    :cond_4
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v4, :cond_0

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 274
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$100(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->fileInfo:Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v3

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 238
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 284
    if-eqz p1, :cond_0

    .line 285
    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onPostExecute. update"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$400(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$400(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setThumbnail(Landroid/graphics/Bitmap;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    .line 291
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v0, :cond_2

    .line 292
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mNotificationView:Lcom/sec/android/app/videoplayer/service/NotificationView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$400(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/NotificationView;->setPersonalPage()V

    .line 294
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$200(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$200(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;->update()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 238
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$NotificationImageUpdateTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
