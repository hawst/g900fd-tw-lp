.class Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 586
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "onError"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    if-eqz p1, :cond_0

    .line 588
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$702(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Z)Z

    .line 592
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getHandler()Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 593
    return v2
.end method
