.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;
.super Ljava/lang/Object;
.source "MiniVideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 824
    const/4 v2, 0x0

    .line 825
    .local v2, "retVal":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 826
    const-wide/16 v0, 0x0

    .line 827
    .local v0, "pressTime":J
    sparse-switch p2, :sswitch_data_0

    .line 884
    .end local v0    # "pressTime":J
    :cond_0
    :goto_0
    return v2

    .line 830
    .restart local v0    # "pressTime":J
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 832
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1

    .line 833
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;J)J

    .line 834
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 837
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->blockSpeedSeek()Z
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 838
    const-string v3, "MiniVideoBtnController"

    const-string v4, "mPrevKeyListener. skip longseek"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x1e

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 840
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v4, 0x0

    const v5, 0x36ee80

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_0

    .line 842
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto :goto_0

    .line 847
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 848
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const-wide/16 v4, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J
    invoke-static {v3, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;J)J

    .line 850
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->blockSpeedSeek()Z
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 851
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_3

    .line 852
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 853
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setLongSeekMode(I)V

    .line 854
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 855
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v3

    const/16 v4, 0xd

    invoke-interface {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    .line 872
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v4, 0x0

    const/16 v5, 0xbb8

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto/16 :goto_0

    .line 858
    :cond_4
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_6

    .line 859
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 860
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    goto :goto_1

    .line 862
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setLongSeekMode(I)V

    .line 863
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 864
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v3

    const/16 v4, 0xd

    invoke-interface {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    goto :goto_1

    .line 867
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setHoldLongSeek()V

    .line 868
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto :goto_1

    .line 827
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 830
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
