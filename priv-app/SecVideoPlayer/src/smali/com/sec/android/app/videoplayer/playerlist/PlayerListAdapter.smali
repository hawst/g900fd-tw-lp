.class public Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;
.super Landroid/widget/CursorAdapter;
.source "PlayerListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final NULLPATH:Ljava/lang/String; = "null_filepath"


# instance fields
.field private mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

.field private mContext:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 33
    const/4 v0, 0x2

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 34
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    .line 35
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 36
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 37
    return-void
.end method

.method private getFilePath(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)Ljava/lang/String;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 163
    .local v0, "filePath":Ljava/lang/String;
    iget v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->pathIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 165
    :cond_0
    const-string v0, "null_filepath"

    .line 168
    :cond_1
    return-object v0
.end method

.method private getPercentage(JJ)I
    .locals 5
    .param p1, "currentTime"    # J
    .param p3, "durationTime"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 265
    const/4 v0, 0x0

    .line 266
    .local v0, "currentPercent":I
    cmp-long v1, p3, v2

    if-lez v1, :cond_0

    .line 267
    cmp-long v1, p1, v2

    if-gtz v1, :cond_1

    .line 268
    const/4 v0, 0x0

    .line 276
    :cond_0
    :goto_0
    return v0

    .line 270
    :cond_1
    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    div-long/2addr v2, p3

    long-to-int v0, v2

    .line 271
    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getTitle(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)Ljava/lang/String;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "videoTitle":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    iget v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->displayNameIndex:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getCloudTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 187
    :goto_0
    return-object v0

    .line 185
    :cond_0
    iget v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private stringForTime(J)Ljava/lang/String;
    .locals 15
    .param p1, "timeMs"    # J

    .prologue
    .line 252
    const-wide/16 v10, 0x0

    cmp-long v1, p1, v10

    if-gez v1, :cond_0

    .line 253
    const-string v0, "-:--:--"

    .line 261
    :goto_0
    return-object v0

    .line 256
    :cond_0
    const-wide/16 v10, 0x3e8

    div-long v8, p1, v10

    .line 257
    .local v8, "totalSeconds":J
    const-wide/16 v10, 0x3c

    rem-long v6, v8, v10

    .line 258
    .local v6, "seconds":J
    const-wide/16 v10, 0x3c

    div-long v10, v8, v10

    const-wide/16 v12, 0x3c

    rem-long v4, v10, v12

    .line 259
    .local v4, "minutes":J
    const-wide/16 v10, 0xe10

    div-long v2, v8, v10

    .line 261
    .local v2, "hours":J
    const-string v1, "%02d:%02d:%02d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateCloudIcon(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 236
    iget-object v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 249
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 240
    iget-object v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 241
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_BAIDU_CLOUD:Z

    if-eqz v0, :cond_1

    .line 242
    iget-object v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 244
    :cond_1
    iget-object v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateFirstRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 172
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getTitle(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v0

    .line 176
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private updateIndices(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 111
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowIndex:I

    .line 112
    const-string v0, "duration"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowIndex:I

    .line 113
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->idIndex:I

    .line 114
    const-string v0, "resumePos"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->resumePosIndex:I

    .line 115
    const-string v0, "_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->pathIndex:I

    .line 116
    const-string v0, "_display_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->displayNameIndex:I

    .line 117
    return-void
.end method

.method private updateListSecondRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 195
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 224
    :goto_0
    return-void

    .line 199
    :cond_0
    const-wide/16 v2, 0x0

    .line 201
    .local v2, "currentTime":J
    iget v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "duration":Ljava/lang/String;
    iget v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->resumePosIndex:I

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 204
    if-nez v0, :cond_1

    .line 205
    const-string v0, "0"

    .line 207
    :cond_1
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 208
    .local v4, "durationTime":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v7

    .line 209
    .local v7, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 210
    const-wide/16 v2, 0x0

    .line 212
    :cond_2
    const-string v1, "%s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v12

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 213
    .local v9, "strElapsed":Ljava/lang/String;
    const/4 v8, 0x0

    .line 214
    .local v8, "strDuration":Ljava/lang/String;
    const-string v1, " | %s"

    new-array v6, v11, [Ljava/lang/Object;

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->stringForTime(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v12

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 216
    const-wide/16 v10, 0x0

    cmp-long v1, v4, v10

    if-nez v1, :cond_3

    .line 217
    const-string v8, " | --:--:--"

    .line 220
    :cond_3
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    iget-object v1, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v1, p0

    move-object v6, p2

    .line 223
    invoke-direct/range {v1 .. v6}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateProgessBar(JJLcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    goto :goto_0
.end method

.method private updatePlayingEffectFrame(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 125
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    .line 127
    .local v0, "currentFileId":J
    iget v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->idIndex:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 128
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02010d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 129
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07003b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 140
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02010c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 133
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07003a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    iget v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->percentage:I

    if-eqz v2, :cond_1

    iget v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->percentage:I

    const/16 v3, 0x64

    if-ne v2, v3, :cond_2

    .line 136
    :cond_1
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 138
    :cond_2
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateProgessBar(JJLcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 3
    .param p1, "currentTime"    # J
    .param p3, "durationTime"    # J
    .param p5, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 227
    iget-object v1, p5, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    if-nez v1, :cond_0

    .line 233
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getPercentage(JJ)I

    move-result v0

    .line 231
    .local v0, "currentPercent":I
    iget-object v1, p5, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 232
    iput v0, p5, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->percentage:I

    goto :goto_0
.end method

.method private updateSecondRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 191
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateListSecondRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 192
    return-void
.end method

.method private updateThumb(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 143
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    if-nez v2, :cond_0

    .line 158
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getFilePath(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "filePath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 150
    .local v1, "thumbnail":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_1

    .line 151
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->defaultThumbView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 153
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 155
    :cond_1
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->defaultThumbView:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    iget-object v2, p2, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateThumbs(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "vh"    # Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateThumb(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 121
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 98
    if-eqz p3, :cond_0

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    .line 101
    .local v0, "vh":Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateIndices(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 102
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateThumbs(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 103
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateFirstRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 104
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateSecondRowText(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 105
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updateCloudIcon(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 106
    invoke-direct {p0, p3, v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->updatePlayingEffectFrame(Landroid/database/Cursor;Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;)V

    .line 108
    .end local v0    # "vh":Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;
    :cond_0
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 64
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "couldn\'t move cursor to position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 68
    :cond_0
    if-nez p2, :cond_1

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0, v1, v2, p3}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 73
    .local v0, "v":Landroid/view/View;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 74
    return-object v0

    .line 71
    .end local v0    # "v":Landroid/view/View;
    :cond_1
    move-object v0, p2

    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 79
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03002a

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 81
    .local v1, "view":Landroid/view/View;
    new-instance v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;-><init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter;)V

    .line 82
    .local v0, "vh":Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;
    const v2, 0x7f0d017d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->topRootLayout:Landroid/widget/LinearLayout;

    .line 83
    const v2, 0x7f0d0184

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->firstRowText:Landroid/widget/TextView;

    .line 84
    const v2, 0x7f0d0185

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->secondRowText:Landroid/widget/TextView;

    .line 86
    const v2, 0x7f0d017f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailBorder:Landroid/widget/RelativeLayout;

    .line 87
    const v2, 0x7f0d0181

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 88
    const v2, 0x7f0d0180

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->defaultThumbView:Landroid/widget/ImageView;

    .line 89
    const v2, 0x7f0d0182

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->cloudIcon:Landroid/widget/ImageView;

    .line 90
    const v2, 0x7f0d0183

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, v0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListAdapter$ViewHolder;->progressView:Landroid/widget/ProgressBar;

    .line 92
    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 93
    return-object v1
.end method
