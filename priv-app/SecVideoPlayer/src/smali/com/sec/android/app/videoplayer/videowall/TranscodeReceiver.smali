.class public Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TranscodeReceiver.java"


# static fields
.field private static final ACTION_CHECK_SIOP_LEVEL:Ljava/lang/String; = "android.intent.action.CHECK_SIOP_LEVEL"

.field private static final ACTION_POWER_CONNECTED:Ljava/lang/String; = "android.intent.action.ACTION_POWER_CONNECTED"

.field private static final ACTION_POWER_DISCONNECTED:Ljava/lang/String; = "android.intent.action.ACTION_POWER_DISCONNECTED"

.field private static final ACTION_SCREEN_OFF:Ljava/lang/String; = "android.intent.action.SCREEN_OFF"

.field private static final ACTION_SCREEN_ON:Ljava/lang/String; = "android.intent.action.SCREEN_ON"

.field private static final SIOP_LEVEL_BROADCAST:Ljava/lang/String; = "siop_level_broadcast"

.field private static final TAG:Ljava/lang/String;

.field public static isPowerConnected:Z

.field public static isScreenOff:Z

.field public static serviceStop:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 11
    const-class v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->TAG:Ljava/lang/String;

    .line 13
    sput-boolean v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    .line 15
    sput-boolean v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    .line 17
    sput-boolean v1, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->serviceStop:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x20000000

    .line 37
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "action":Ljava/lang/String;
    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onReceive : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_create()V

    .line 42
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 44
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getSupportVW()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 45
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    if-nez v3, :cond_0

    .line 47
    sput-boolean v8, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    .line 48
    sput-boolean v7, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    .line 49
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 50
    .local v2, "start_intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 95
    .end local v2    # "start_intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 53
    :cond_1
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 54
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    if-nez v3, :cond_0

    .line 55
    sput-boolean v8, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    .line 56
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    if-eqz v3, :cond_0

    .line 57
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 58
    .restart local v2    # "start_intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 59
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 62
    .end local v2    # "start_intent":Landroid/content/Intent;
    :cond_2
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 63
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    if-eqz v3, :cond_0

    .line 64
    sput-boolean v7, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    .line 65
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    if-eqz v3, :cond_0

    .line 66
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .restart local v2    # "start_intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 68
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 71
    .end local v2    # "start_intent":Landroid/content/Intent;
    :cond_3
    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 72
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    if-eqz v3, :cond_0

    .line 73
    sput-boolean v7, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isPowerConnected:Z

    .line 74
    sput-boolean v7, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->isScreenOff:Z

    .line 76
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->serviceStop:Z

    if-nez v3, :cond_0

    .line 77
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    .restart local v2    # "start_intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 79
    invoke-virtual {p1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto :goto_0

    .line 82
    .end local v2    # "start_intent":Landroid/content/Intent;
    :cond_4
    const-string v3, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    const-string v3, "siop_level_broadcast"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 84
    .local v0, "SIOPLevel":I
    sget-object v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " SIOP_LEVEL: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v3, 0x2

    if-lt v0, v3, :cond_0

    .line 87
    sget-boolean v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeReceiver;->serviceStop:Z

    if-nez v3, :cond_0

    .line 88
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/sec/android/app/videoplayer/videowall/TranscodeService;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    .restart local v2    # "start_intent":Landroid/content/Intent;
    invoke-virtual {v2, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 90
    invoke-virtual {p1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    goto/16 :goto_0
.end method
