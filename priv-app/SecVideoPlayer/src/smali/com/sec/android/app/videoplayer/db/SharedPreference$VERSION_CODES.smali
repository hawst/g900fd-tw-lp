.class public interface abstract Lcom/sec/android/app/videoplayer/db/SharedPreference$VERSION_CODES;
.super Ljava/lang/Object;
.source "SharedPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/db/SharedPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VERSION_CODES"
.end annotation


# static fields
.field public static final ICE_CREAM_SANDWICH:I = 0xe

.field public static final ICE_CREAM_SANDWICH_MR1:I = 0xf

.field public static final JELLY_BEAN:I = 0x10

.field public static final JELLY_BEAN_MR1:I = 0x11

.field public static final JELLY_BEAN_MR2:I = 0x12

.field public static final KITKAT:I = 0x13

.field public static final LATEST_VERSION:I = 0x13
