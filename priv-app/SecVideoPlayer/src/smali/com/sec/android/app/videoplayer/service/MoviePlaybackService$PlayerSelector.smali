.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/service/IPlayer;
.implements Lcom/sec/android/app/videoplayer/service/ITouchPlayer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayerSelector"
.end annotation


# instance fields
.field private mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 1880
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$1;

    .prologue
    .line 1880
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    return-void
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;)V

    .line 2010
    :cond_0
    return-void
.end method

.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1971
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1972
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 1974
    :cond_0
    return-void
.end method

.method public changePlayer(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1888
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/service/IPlayer;->changePlayer(I)V

    .line 1889
    return-void
.end method

.method public deselectSubtitleTrack()V
    .locals 1

    .prologue
    .line 2034
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2035
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->deselectSubtitleTrack()V

    .line 2037
    :cond_0
    return-void
.end method

.method public duration()J
    .locals 2

    .prologue
    .line 1923
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 2127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2130
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFPS()I
    .locals 1

    .prologue
    .line 2111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->getFPS()I

    move-result v0

    .line 2114
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFullDumpSubtitle(Ljava/lang/String;Z)[B
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1985
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1986
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->getFullDumpSubtitle(Ljava/lang/String;Z)[B

    move-result-object v0

    .line 1988
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initSelectSubtitleTrack()V
    .locals 1

    .prologue
    .line 2020
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2021
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->initSelectSubtitleTrack()V

    .line 2023
    :cond_0
    return-void
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1978
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1979
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->initSubtitle(Ljava/lang/String;Z)V

    .line 1981
    :cond_0
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1898
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isSecVideo()Z
    .locals 1

    .prologue
    .line 2119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2120
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->isSecVideo()Z

    move-result v0

    .line 2122
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public oneFrameSeek(I)V
    .locals 1
    .param p1, "plusOrMinus"    # I

    .prologue
    .line 2083
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2084
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->oneFrameSeek(I)V

    .line 2086
    :cond_0
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->pause()V

    .line 1914
    return-void
.end method

.method public position()J
    .locals 2

    .prologue
    .line 1928
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public realSeek(II)J
    .locals 2
    .param p1, "whereto"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/IPlayer;->realSeek(II)J

    move-result-wide v0

    return-wide v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 2062
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2063
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->reset()V

    .line 2065
    :cond_0
    return-void
.end method

.method public resetPlayer()V
    .locals 1

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2077
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetPlayer()V

    .line 2079
    :cond_0
    return-void
.end method

.method public resetSubtitle()V
    .locals 1

    .prologue
    .line 1993
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1994
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->resetSubtitle()V

    .line 1996
    :cond_0
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "whereto"    # J

    .prologue
    .line 1933
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/IPlayer;->seek(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public selectSubtitleTrack(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 2027
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2028
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->selectSubtitleTrack(I)V

    .line 2030
    :cond_0
    return-void
.end method

.method public setAdaptSound(ZZ)V
    .locals 1
    .param p1, "isOn"    # Z
    .param p2, "showToast"    # Z

    .prologue
    .line 1943
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1944
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAdaptSound(ZZ)V

    .line 1946
    :cond_0
    return-void
.end method

.method public setAudioTrack(I)V
    .locals 1
    .param p1, "tackNumber"    # I

    .prologue
    .line 2097
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2098
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setAudioTrack(I)V

    .line 2100
    :cond_0
    return-void
.end method

.method public setDataSource(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2055
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2056
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSource(Landroid/net/Uri;)V

    .line 2058
    :cond_0
    return-void
.end method

.method public setDataSourceAsync(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSourceAsync(Landroid/net/Uri;)V

    .line 2051
    :cond_0
    return-void
.end method

.method public setDataSourcePrepare(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1893
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/service/IPlayer;->setDataSourcePrepare(Landroid/net/Uri;)V

    .line 1894
    return-void
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 2069
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2070
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setHandler(Landroid/os/Handler;)V

    .line 2072
    :cond_0
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 1

    .prologue
    .line 2000
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2001
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setInbandSubtitle()V

    .line 2003
    :cond_0
    return-void
.end method

.method public setPlaySpeed(F)V
    .locals 1
    .param p1, "speed"    # F

    .prologue
    .line 1964
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1965
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setPlaySpeed(F)V

    .line 1967
    :cond_0
    return-void
.end method

.method public setPlayer(Lcom/sec/android/app/videoplayer/service/IPlayer;)V
    .locals 0
    .param p1, "player"    # Lcom/sec/android/app/videoplayer/service/IPlayer;

    .prologue
    .line 1884
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    .line 1885
    return-void
.end method

.method public setSoundAliveMode()V
    .locals 1

    .prologue
    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSoundAliveMode()V

    .line 1953
    :cond_0
    return-void
.end method

.method public setSoundAliveMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1957
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSoundAliveMode(I)V

    .line 1960
    :cond_0
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 1
    .param p1, "synctime"    # I

    .prologue
    .line 2041
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2042
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setSubtitleSyncTime(I)V

    .line 2044
    :cond_0
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 2104
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2105
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setVideoCrop(IIII)V

    .line 2107
    :cond_0
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "vol"    # F

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2091
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setVolume(F)V

    .line 2093
    :cond_0
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->start()V

    .line 1904
    return-void
.end method

.method public startSubtitle()V
    .locals 1

    .prologue
    .line 2013
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    instance-of v0, v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 2014
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->startSubtitle()V

    .line 2016
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 1908
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->mCurrentPlayer:Lcom/sec/android/app/videoplayer/service/IPlayer;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/service/IPlayer;->stop()V

    .line 1909
    return-void
.end method
