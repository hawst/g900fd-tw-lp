.class Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;
.super Ljava/lang/Object;
.source "VideoAdaptSoundPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 110
    if-nez p2, :cond_3

    .line 111
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->isEnableAdaptSoundPath()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 112
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->isAlreadyDiagnosed(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.hearingadjust.launch"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;->onSelected(I)V

    .line 133
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 134
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "VideoAdaptSoundPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not found hearingadjust application :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 119
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->setAdaptSound(Z)V
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Z)V

    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 122
    check-cast v1, Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 123
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->setAdaptSound(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Z)V

    .line 124
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const v2, 0x7f0a012d

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_0

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->setAdaptSound(Z)V
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Z)V

    goto :goto_0
.end method
