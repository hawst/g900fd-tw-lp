.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchTask"
.end annotation


# instance fields
.field final mFile:Ljava/io/File;

.field final synthetic this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V
    .locals 0
    .param p2, "f"    # Ljava/io/File;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->mFile:Ljava/io/File;

    .line 486
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 490
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->mFile:Ljava/io/File;

    if-nez v5, :cond_1

    .line 505
    :cond_0
    return-void

    .line 492
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->mFile:Ljava/io/File;

    new-instance v6, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-direct {v6, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)V

    invoke-virtual {v5, v6}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 493
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 494
    array-length v5, v2

    if-eqz v5, :cond_0

    .line 497
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 498
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 499
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchFiles(Ljava/io/File;)V
    invoke-static {v5, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->access$1100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V

    .line 497
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 501
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
