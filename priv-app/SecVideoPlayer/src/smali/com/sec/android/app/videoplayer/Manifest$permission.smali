.class public final Lcom/sec/android/app/videoplayer/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final READ:Ljava/lang/String; = "com.sec.android.videowall.permission.READ"

.field public static final READ_EXTERNAL_STORAGE:Ljava/lang/String; = "com.sec.android.provider.video.READ_EXTERNAL_STORAGE"

.field public static final WRITE:Ljava/lang/String; = "com.sec.android.videowall.permission.WRITE"

.field public static final WRITE_EXTERNAL_STORAGE:Ljava/lang/String; = "com.sec.android.provider.video.WRITE_EXTERNAL_STORAGE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
