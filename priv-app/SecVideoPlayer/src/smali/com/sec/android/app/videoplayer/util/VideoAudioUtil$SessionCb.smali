.class Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;
.super Landroid/media/session/MediaSession$Callback;
.source "VideoAudioUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SessionCb"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$1;

    .prologue
    .line 457
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;-><init>(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V

    return-void
.end method

.method private onMediaKeyDown(Landroid/view/KeyEvent;)V
    .locals 14
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    .line 498
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "onMediaKeyDown - KeyEvent :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iget-object v11, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v11

    invoke-virtual {v11}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v11

    invoke-virtual {v11}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v5

    .line 500
    .local v5, "state":Landroid/media/session/PlaybackState;
    if-nez v5, :cond_2

    move-wide v6, v8

    .line 501
    .local v6, "validActions":J
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    .line 502
    .local v4, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    const/4 v2, 0x0

    .line 503
    .local v2, "command":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    .line 575
    :cond_0
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 576
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    invoke-virtual {v8, v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendMediaButtonCommand(Ljava/lang/String;)V

    .line 579
    :cond_1
    return-void

    .line 500
    .end local v2    # "command":Ljava/lang/String;
    .end local v4    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .end local v6    # "validActions":J
    :cond_2
    invoke-virtual {v5}, Landroid/media/session/PlaybackState;->getActions()J

    move-result-wide v6

    goto :goto_0

    .line 505
    .restart local v2    # "command":Ljava/lang/String;
    .restart local v4    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .restart local v6    # "validActions":J
    :sswitch_0
    const-wide/16 v10, 0x4

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 506
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onPlay()V

    .line 507
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PLAY_CMD:Ljava/lang/String;

    goto :goto_1

    .line 511
    :sswitch_1
    const-wide/16 v10, 0x2

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onPause()V

    .line 513
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_PAUSE_CMD:Ljava/lang/String;

    goto :goto_1

    .line 517
    :sswitch_2
    const-wide/16 v10, 0x20

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 518
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onSkipToNext()V

    .line 519
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_NEXT:Ljava/lang/String;

    goto :goto_1

    .line 523
    :sswitch_3
    const-wide/16 v10, 0x10

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 524
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onSkipToPrevious()V

    .line 525
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PREV:Ljava/lang/String;

    goto :goto_1

    .line 529
    :sswitch_4
    const-wide/16 v10, 0x1

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 530
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onStop()V

    goto :goto_1

    .line 536
    :sswitch_5
    const-wide/16 v10, 0x40

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 537
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 538
    const/16 v8, 0xc

    invoke-virtual {v4, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 541
    :goto_2
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_DOWN:Ljava/lang/String;

    goto :goto_1

    .line 540
    :cond_3
    const/4 v8, 0x6

    invoke-virtual {v4, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 547
    :sswitch_6
    const-wide/16 v10, 0x8

    and-long/2addr v10, v6

    cmp-long v8, v10, v8

    if-eqz v8, :cond_0

    .line 548
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 549
    const/16 v8, 0xd

    invoke-virtual {v4, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 553
    :goto_3
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_DOWN:Ljava/lang/String;

    goto/16 :goto_1

    .line 551
    :cond_4
    const/4 v8, 0x7

    invoke-virtual {v4, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_3

    .line 559
    :sswitch_7
    if-nez v5, :cond_6

    move v3, v10

    .line 561
    .local v3, "isPlaying":Z
    :goto_4
    const-wide/16 v12, 0x204

    and-long/2addr v12, v6

    cmp-long v11, v12, v8

    if-eqz v11, :cond_8

    move v1, v0

    .line 562
    .local v1, "canPlay":Z
    :goto_5
    const-wide/16 v12, 0x202

    and-long/2addr v12, v6

    cmp-long v8, v12, v8

    if-eqz v8, :cond_9

    .line 563
    .local v0, "canPause":Z
    :goto_6
    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    .line 564
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onPause()V

    .line 568
    :cond_5
    :goto_7
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v8

    if-nez v8, :cond_0

    .line 569
    sget-object v2, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_PLAYPAUSE:Ljava/lang/String;

    goto/16 :goto_1

    .line 559
    .end local v0    # "canPause":Z
    .end local v1    # "canPlay":Z
    .end local v3    # "isPlaying":Z
    :cond_6
    invoke-virtual {v5}, Landroid/media/session/PlaybackState;->getState()I

    move-result v11

    const/4 v12, 0x3

    if-ne v11, v12, :cond_7

    move v3, v0

    goto :goto_4

    :cond_7
    move v3, v10

    goto :goto_4

    .restart local v3    # "isPlaying":Z
    :cond_8
    move v1, v10

    .line 561
    goto :goto_5

    .restart local v1    # "canPlay":Z
    :cond_9
    move v0, v10

    .line 562
    goto :goto_6

    .line 565
    .restart local v0    # "canPause":Z
    :cond_a
    if-nez v3, :cond_5

    if-eqz v1, :cond_5

    .line 566
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onPlay()V

    goto :goto_7

    .line 503
    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_7
        0x55 -> :sswitch_7
        0x56 -> :sswitch_4
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_6
        0x5a -> :sswitch_5
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method

.method private onMediaKeyUp(Landroid/view/KeyEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0xa

    .line 582
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMediaKeyUp - KeyEvent :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    .line 584
    .local v1, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    const/4 v0, 0x0

    .line 585
    .local v0, "command":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 604
    :goto_0
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMiniModeServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 605
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendMediaButtonCommand(Ljava/lang/String;)V

    .line 607
    :cond_0
    return-void

    .line 587
    :pswitch_1
    sget-object v0, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_STOP:Ljava/lang/String;

    .line 588
    goto :goto_0

    .line 591
    :pswitch_2
    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 592
    sget-object v0, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_REW_UP:Ljava/lang/String;

    .line 593
    goto :goto_0

    .line 596
    :pswitch_3
    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 597
    sget-object v0, Lcom/sec/android/app/videoplayer/receiver/VideoBtReceiver;->VIDEO_MEDIA_BTN_FF_UP:Ljava/lang/String;

    .line 598
    goto :goto_0

    .line 585
    nop

    :pswitch_data_0
    .packed-switch 0x56
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V
    .locals 2
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "cb"    # Landroid/os/ResultReceiver;

    .prologue
    .line 462
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    invoke-super {p0, p1, p2, p3}, Landroid/media/session/MediaSession$Callback;->onCommand(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ResultReceiver;)V

    .line 464
    return-void
.end method

.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 469
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onCustomAction()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    invoke-super {p0, p1, p2}, Landroid/media/session/MediaSession$Callback;->onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 471
    return-void
.end method

.method public onFastForward()V
    .locals 2

    .prologue
    .line 686
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onFastForward()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onFastForward()V

    .line 688
    return-void
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "mediaButtonIntent"    # Landroid/content/Intent;

    .prologue
    .line 476
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MediaSession.Callback - onMediaButtonEvent()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 478
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 479
    .local v0, "event":Landroid/view/KeyEvent;
    if-eqz v0, :cond_0

    .line 480
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 494
    .end local v0    # "event":Landroid/view/KeyEvent;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v1

    :goto_1
    return v1

    .line 482
    .restart local v0    # "event":Landroid/view/KeyEvent;
    :pswitch_0
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KeyEvent.ACTION_DOWN"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onMediaKeyDown(Landroid/view/KeyEvent;)V

    .line 484
    const/4 v1, 0x1

    goto :goto_1

    .line 486
    :pswitch_1
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KeyEvent.ACTION_UP"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->onMediaKeyUp(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 480
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 662
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 663
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaSession.Callback - onPause(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/PlaybackState;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseOrStopPlaying()V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V

    .line 667
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onPause()V

    .line 668
    return-void
.end method

.method public onPlay()V
    .locals 6

    .prologue
    .line 639
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 640
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MediaSession.Callback - onPlay(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v5

    invoke-virtual {v5}, Landroid/media/session/PlaybackState;->getState()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    .line 644
    .local v2, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    .line 645
    .local v0, "isPlaying":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$300(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v1

    .line 647
    .local v1, "pauaseEnable":Z
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resumeOrStartPlaying() = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pauseEnable = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    if-nez v0, :cond_1

    .line 650
    if-eqz v1, :cond_2

    .line 651
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 656
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState()V
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V

    .line 657
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onPlay()V

    .line 658
    return-void

    .line 653
    :cond_2
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    goto :goto_0
.end method

.method public onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "mediaId"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 612
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onPlayFromMediaId()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    invoke-super {p0, p1, p2}, Landroid/media/session/MediaSession$Callback;->onPlayFromMediaId(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 614
    return-void
.end method

.method public onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 619
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onPlayFromSearch()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 620
    invoke-super {p0, p1, p2}, Landroid/media/session/MediaSession$Callback;->onPlayFromSearch(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 621
    return-void
.end method

.method public onRewind()V
    .locals 2

    .prologue
    .line 692
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onRewind()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onRewind()V

    .line 694
    return-void
.end method

.method public onSetRating(Landroid/media/Rating;)V
    .locals 2
    .param p1, "rating"    # Landroid/media/Rating;

    .prologue
    .line 626
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onSetRating()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onSetRating(Landroid/media/Rating;)V

    .line 628
    return-void
.end method

.method public onSkipToNext()V
    .locals 2

    .prologue
    .line 672
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onSkipToNext()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 674
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onSkipToNext()V

    .line 675
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 2

    .prologue
    .line 679
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onSkipToPrevious()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 681
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onSkipToPrevious()V

    .line 682
    return-void
.end method

.method public onSkipToQueueItem(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 632
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onSkipToQueueItem()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    invoke-super {p0, p1, p2}, Landroid/media/session/MediaSession$Callback;->onSkipToQueueItem(J)V

    .line 635
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    .line 698
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MediaSession.Callback - onStop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 700
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaSession.Callback - onStop(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)Landroid/media/session/MediaSession;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/session/PlaybackState;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil$SessionCb;->this$0:Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setPlaybackState()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;)V

    .line 704
    invoke-super {p0}, Landroid/media/session/MediaSession$Callback;->onStop()V

    .line 705
    return-void
.end method
