.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;,
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;
    }
.end annotation


# static fields
.field private static final SKIP_FOLDER_FOR_FILSSHARE:Ljava/lang/String;

.field private static final SKIP_FOLDER_FOR_SMPTE:Ljava/lang/String;

.field private static final SKIP_FOLDER_FOR_SMPTE_SDCARD:Ljava/lang/String; = "/storage/extSdCard/Android/data"

.field private static final TAG:Ljava/lang/String; = "VideoSubtitlePopup"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

.field private final mHandler:Landroid/os/Handler;

.field private mInvaildSubtitle:Z

.field private mSelectedFile:Ljava/io/File;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSubFilesList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mSubtitleFile:Ljava/lang/String;

.field private mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

.field private mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Android/data"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->SKIP_FOLDER_FOR_SMPTE:Ljava/lang/String;

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/data/data/com.sec.android.app.FileShareServer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->SKIP_FOLDER_FOR_FILSSHARE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;

    .line 87
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 655
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$5;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mHandler:Landroid/os/Handler;

    .line 663
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$6;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 103
    const-string v0, "VideoSubtitlePopup"

    const-string v1, "VideoSubtitlePopup - VideoSelectSubtitlePopup E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    .line 106
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 107
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v0

    if-nez v0, :cond_0

    .line 111
    const v0, 0x7f0a0035

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/io/File;)Ljava/io/File;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # Ljava/io/File;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSelectedFile:Ljava/io/File;

    return-object p1
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->SKIP_FOLDER_FOR_SMPTE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->SKIP_FOLDER_FOR_FILSSHARE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/view/VideoStateView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->clearKeepScreenOn()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleFile:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$600(Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->existFile(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->callSelectSubtitlePopup()V

    return-void
.end method

.method private callSelectSubtitlePopup()V
    .locals 7

    .prologue
    const/4 v6, -0x2

    .line 139
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "callSelectSubtitlePopup()"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 142
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0a0125

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 145
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 146
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    new-instance v4, Ljava/io/File;

    const v5, 0x7f0a0138

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->getStr(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;

    invoke-direct {v3, p0, v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$SelectSubtitleAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Landroid/content/Context;Ljava/util/ArrayList;)V

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    :cond_1
    const v3, 0x7f0a0126

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 254
    const v3, 0x7f0a0026

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 262
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubPopupKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 264
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    .line 265
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 281
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 282
    .local v2, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 283
    iput v6, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    check-cast v2, Landroid/view/WindowManager$LayoutParams;

    .end local v2    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 287
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 293
    :goto_0
    return-void

    .line 288
    :catch_0
    move-exception v1

    .line 289
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "Subtitle Dialog :: NullPointerException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 290
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 291
    .local v1, "e":Landroid/view/WindowManager$BadTokenException;
    const-string v3, "VideoSubtitlePopup"

    const-string v4, "Subtitle Dialog :: BadTokenException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private declared-synchronized clearKeepScreenOn()V
    .locals 2

    .prologue
    .line 419
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :cond_0
    monitor-exit p0

    return-void

    .line 419
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static existFile(Ljava/lang/String;)Z
    .locals 2
    .param p0, "subTitleFileName"    # Ljava/lang/String;

    .prologue
    .line 296
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 297
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private getStr(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 708
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 711
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private resetFileSearcher()V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->finish()V

    .line 406
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    .line 409
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->clearKeepScreenOn()V

    .line 410
    return-void
.end method

.method private declared-synchronized setKeepScreenOn()V
    .locals 2

    .prologue
    .line 413
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 416
    :cond_0
    monitor-exit p0

    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public createSelectSubtitlePopup()V
    .locals 4

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 386
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-nez v0, :cond_1

    .line 390
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoStateView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const v3, 0x101007a

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/view/VideoStateView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 391
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->addViewTo(Landroid/view/View;)V

    .line 394
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 396
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->resetFileSearcher()V

    .line 397
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    .line 398
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mFS:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->searchSubtitles()V

    .line 400
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->setKeepScreenOn()V

    .line 401
    return-void
.end method

.method public createSubtitleSettingPopup()V
    .locals 4

    .prologue
    .line 716
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleSettingPopup"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 719
    const-string v1, "VideoSubtitlePopup"

    const-string v2, "createSubtitleSettingPopup. mContext is NULL"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    :goto_0
    return-void

    .line 723
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    .line 724
    .local v0, "sufp":Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCtx(Landroid/content/Context;)V

    .line 726
    new-instance v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f0b0005

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 727
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->show()V

    .line 729
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_1

    .line 730
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iput-object v2, v1, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 731
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v2, "VideoSubtitlePopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 734
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setSubtitleSetting(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    goto :goto_0
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v1, "VideoSubtitlePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 127
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->resetFileSearcher()V

    .line 128
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
