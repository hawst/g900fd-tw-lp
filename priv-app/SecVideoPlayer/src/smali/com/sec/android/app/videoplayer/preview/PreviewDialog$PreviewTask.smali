.class Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;
.super Landroid/os/AsyncTask;
.source "PreviewDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PreviewTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field keyframeTime:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 455
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v6, 0x0

    .line 461
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v0

    .line 463
    .local v0, "chapterMgr":Lcom/sec/android/app/videoplayer/preview/ChapterManager;
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    if-ge v1, v2, :cond_3

    sget-boolean v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bStart:Z

    if-eqz v2, :cond_3

    .line 464
    iput v6, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    .line 466
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->existThumbnailFile(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 467
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->setThumbnail(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 468
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6, v1, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 469
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4, v1, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 471
    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getKeyframeTime(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    .line 480
    :goto_1
    if-eqz v1, :cond_0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    if-nez v2, :cond_0

    .line 481
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$200(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resumeChapterView(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " keyframeTime is 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    mul-int/2addr v2, v1

    iput v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    .line 485
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mChapterFrameTime:Ljava/util/WeakHashMap;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$700(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    iget v5, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    invoke-virtual {v3, v4, v1, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 463
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 473
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->deleteThumbnailFile(I)V

    .line 474
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->showThumbnailAtTime(I)V

    goto :goto_1

    .line 477
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->showThumbnailAtTime(I)V

    goto :goto_1

    .line 488
    :cond_3
    const/4 v2, 0x0

    return-object v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 455
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 493
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->execute()V

    .line 494
    return-void
.end method

.method showThumbnailAtTime(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    const/4 v5, 0x0

    .line 502
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 503
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$500(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/WeakHashMap;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    move-result-object v1

    .line 507
    .local v1, "chapterMgr":Lcom/sec/android/app/videoplayer/preview/ChapterManager;
    invoke-virtual {v1, p1, v0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getBitmapAtTime(ILandroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 508
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5, p1, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 509
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4, p1, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 511
    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->setKeyframeTime(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->keyframeTime:I

    .line 515
    :goto_0
    return-void

    .line 513
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$PreviewTask;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$600(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v5, p1, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
