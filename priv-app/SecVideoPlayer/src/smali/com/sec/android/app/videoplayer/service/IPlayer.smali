.class interface abstract Lcom/sec/android/app/videoplayer/service/IPlayer;
.super Ljava/lang/Object;
.source "IPlayer.java"


# virtual methods
.method public abstract changePlayer(I)V
.end method

.method public abstract duration()J
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract pause()V
.end method

.method public abstract position()J
.end method

.method public abstract realSeek(II)J
.end method

.method public abstract seek(J)J
.end method

.method public abstract setDataSourcePrepare(Landroid/net/Uri;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract start()V
.end method

.method public abstract stop()V
.end method
