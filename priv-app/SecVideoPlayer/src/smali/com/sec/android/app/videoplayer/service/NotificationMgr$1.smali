.class Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;
.super Landroid/content/BroadcastReceiver;
.source "NotificationMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/service/NotificationMgr;->registerNotificationReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method consumeEvent()Z
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->controlable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getCommand(Ljava/lang/String;)I
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 180
    const-string v0, "videoplayer.notification.prev"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const/16 v0, 0x9

    .line 193
    :goto_0
    return v0

    .line 182
    :cond_0
    const-string v0, "videoplayer.notification.next"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const/16 v0, 0x8

    goto :goto_0

    .line 184
    :cond_1
    const-string v0, "videoplayer.notification.playpause"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 185
    const/4 v0, 0x3

    goto :goto_0

    .line 186
    :cond_2
    const-string v0, "videoplayer.notification.ffdown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187
    const/4 v0, 0x6

    goto :goto_0

    .line 188
    :cond_3
    const-string v0, "videoplayer.notification.rewdown"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 189
    const/4 v0, 0x7

    goto :goto_0

    .line 190
    :cond_4
    const-string v0, "videoplayer.notification.ffup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "videoplayer.notification.rewup"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 191
    :cond_5
    const/16 v0, 0xa

    goto :goto_0

    .line 193
    :cond_6
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "action : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    .line 139
    .local v3, "vUtil":Lcom/sec/android/app/videoplayer/common/VUtils;
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    .line 142
    .local v2, "isDlnaMode":Z
    if-nez v2, :cond_1

    const-string v4, "videoplayer.notification.close"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$100(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mNotificationReceiver. return"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    const-string v4, "videoplayer.notification.close"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 148
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$200(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->this$0:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    # getter for: Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->access$200(Lcom/sec/android/app/videoplayer/service/NotificationMgr;)Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    move-result-object v4

    invoke-interface {v4}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;->exit()V

    .line 151
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->getCommand(Ljava/lang/String;)I

    move-result v1

    .line 153
    .local v1, "command":I
    packed-switch v1, :pswitch_data_0

    .line 171
    :cond_3
    :pswitch_0
    if-lez v1, :cond_0

    .line 172
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 157
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr$1;->consumeEvent()Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 162
    :pswitch_2
    if-eqz v2, :cond_3

    .line 163
    const v4, 0x7f0a0192

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(II)V

    goto :goto_0

    .line 153
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
