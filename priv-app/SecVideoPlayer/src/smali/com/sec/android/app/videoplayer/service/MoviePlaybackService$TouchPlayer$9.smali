.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$9;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnTimedTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 3683
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$9;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "text"    # Landroid/media/TimedText;

    .prologue
    .line 3685
    const-string v0, "MoviePlaybackService"

    const-string v1, "TouchPlayer :: mOnTimedTextListener()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3687
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$9;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mIsOutbandSubtitle:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->access$4100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v0

    if-eqz v0, :cond_0

    .line 3689
    const-string v0, "MoviePlaybackService"

    const-string v1, "mOnTimedTextListener() :: mIsOutbandSubtitle is false and not inband type"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3694
    :goto_0
    return-void

    .line 3693
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->onTimedTextListener(Landroid/media/TimedText;)V

    goto :goto_0
.end method
