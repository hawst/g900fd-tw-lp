.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;
.super Ljava/lang/Object;
.source "MiniVideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 0

    .prologue
    .line 716
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 718
    const/4 v0, 0x0

    .line 719
    .local v0, "retVal":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 720
    sparse-switch p2, :sswitch_data_0

    .line 743
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 724
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 728
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    .line 730
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v2, 0x0

    const/16 v3, 0xbb8

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_0

    .line 720
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x3e -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 724
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
