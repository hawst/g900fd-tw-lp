.class public Lcom/sec/android/app/videoplayer/widget/Mover;
.super Ljava/lang/Object;
.source "Mover.java"


# instance fields
.field public isStarted:Z

.field private mAnimationDurationMillis:I

.field private mCurrentZoom:F

.field private mEndZoom:F

.field private mFinished:Z

.field private mInterpolator:Landroid/view/animation/Interpolator;

.field private mStartRTC:J


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "duration"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    .line 68
    new-instance v0, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v0}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 69
    iput p2, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mAnimationDurationMillis:I

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->isStarted:Z

    .line 71
    return-void
.end method


# virtual methods
.method public abortAnimation()V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    .line 91
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mEndZoom:F

    iput v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mCurrentZoom:F

    .line 92
    return-void
.end method

.method public computeZoom()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 116
    iget-boolean v5, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    if-eqz v5, :cond_0

    .line 129
    :goto_0
    return v1

    .line 120
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mStartRTC:J

    sub-long v2, v6, v8

    .line 121
    .local v2, "tRTC":J
    iget v5, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mAnimationDurationMillis:I

    int-to-long v6, v5

    cmp-long v5, v2, v6

    if-ltz v5, :cond_1

    .line 122
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    .line 123
    iget v4, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mEndZoom:F

    iput v4, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mCurrentZoom:F

    goto :goto_0

    .line 127
    :cond_1
    long-to-float v1, v2

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v1, v5

    iget v5, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mAnimationDurationMillis:I

    int-to-float v5, v5

    div-float v0, v1, v5

    .line 128
    .local v0, "t":F
    iget v1, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mEndZoom:F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v5, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v5

    mul-float/2addr v1, v5

    iput v1, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mCurrentZoom:F

    move v1, v4

    .line 129
    goto :goto_0
.end method

.method public forceFinished(Z)V
    .locals 0
    .param p1, "finished"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    .line 82
    return-void
.end method

.method public getCurrZoom()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mCurrentZoom:F

    return v0
.end method

.method public startZoom(F)V
    .locals 2
    .param p1, "endZoom"    # F

    .prologue
    .line 101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mStartRTC:J

    .line 102
    iput p1, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mEndZoom:F

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mFinished:Z

    .line 105
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->mCurrentZoom:F

    .line 106
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/widget/Mover;->isStarted:Z

    .line 107
    return-void
.end method
