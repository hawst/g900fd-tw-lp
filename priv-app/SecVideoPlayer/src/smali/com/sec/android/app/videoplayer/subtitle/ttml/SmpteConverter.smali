.class public Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;
.super Ljava/lang/Object;
.source "SmpteConverter.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final UNDEFIEND:I = -0x989676

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->TAG:Ljava/lang/String;

    .line 10
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    .line 16
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    return-object v0
.end method


# virtual methods
.method public isContainString(Ljava/lang/String;)Z
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 20
    const/4 v1, 0x0

    .line 21
    .local v1, "result":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x20

    if-eq v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0xd

    if-eq v2, v3, :cond_0

    .line 23
    const/4 v1, 0x1

    .line 21
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 26
    :cond_1
    return v1
.end method

.method public isNumber(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stringToColor(Ljava/lang/String;)I
    .locals 5
    .param p1, "Str"    # Ljava/lang/String;

    .prologue
    .line 82
    const v0, -0xf423f

    .line 83
    .local v0, "color":I
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "tempStr":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 86
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stringToColor : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const-string v2, "BLACK"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 88
    const/high16 v0, -0x1000000

    .line 112
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    const-string v2, "BLUE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 90
    const v0, -0xffff01

    goto :goto_0

    .line 91
    :cond_2
    const-string v2, "CYAN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 92
    const v0, -0xff0001

    goto :goto_0

    .line 93
    :cond_3
    const-string v2, "DKGRAY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 94
    const v0, -0xbbbbbc

    goto :goto_0

    .line 95
    :cond_4
    const-string v2, "GREEN"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 96
    const v0, -0xff0100

    goto :goto_0

    .line 97
    :cond_5
    const-string v2, "GRAY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 98
    const v0, -0x777778

    goto :goto_0

    .line 99
    :cond_6
    const-string v2, "LTGRAY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 100
    const v0, -0x333334

    goto :goto_0

    .line 101
    :cond_7
    const-string v2, "MAGENTA"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 102
    const v0, -0xff01

    goto :goto_0

    .line 103
    :cond_8
    const-string v2, "RED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 104
    const/high16 v0, -0x10000

    goto :goto_0

    .line 105
    :cond_9
    const-string v2, "TRANSPARENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 106
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_a
    const-string v2, "WHITE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 108
    const/4 v0, -0x1

    goto :goto_0

    .line 109
    :cond_b
    const-string v2, "YELLOW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    const/16 v0, -0x100

    goto :goto_0
.end method

.method public stringToFloat(Ljava/lang/String;)F
    .locals 6
    .param p1, "Str"    # Ljava/lang/String;

    .prologue
    .line 69
    const/4 v2, 0x0

    .line 70
    .local v2, "retVal":F
    move-object v3, p1

    .line 71
    .local v3, "tempStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 72
    .local v1, "numStr":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 74
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x30

    if-le v4, v5, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x39

    if-lt v4, v5, :cond_1

    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x2e

    if-ne v4, v5, :cond_2

    .line 75
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_3
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 78
    return v2
.end method

.method public stringToInt(Ljava/lang/String;)I
    .locals 7
    .param p1, "Str"    # Ljava/lang/String;

    .prologue
    const v4, -0x989676

    .line 50
    const-string v5, "%"

    invoke-virtual {p1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    .line 65
    :goto_0
    return v2

    .line 53
    :cond_0
    const v2, -0x989676

    .line 54
    .local v2, "retVal":I
    move-object v3, p1

    .line 55
    .local v3, "tempStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 56
    .local v1, "numStr":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 58
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x30

    if-lt v5, v6, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x39

    if-gt v5, v6, :cond_1

    .line 59
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 57
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 61
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v4

    .line 62
    goto :goto_0

    .line 64
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 65
    goto :goto_0
.end method

.method public stringToOutline(Ljava/lang/String;)[I
    .locals 10
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, -0x989676

    .line 30
    const/4 v5, 0x3

    new-array v4, v5, [I

    .line 31
    .local v4, "retval":[I
    aput v6, v4, v9

    aput v6, v4, v8

    aput v6, v4, v7

    .line 32
    const-string v5, " "

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_3

    aget-object v1, v0, v2

    .line 33
    .local v1, "attr":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->isNumber(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    aget v5, v4, v7

    if-ne v5, v6, :cond_1

    .line 34
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v7

    .line 32
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 35
    :cond_1
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->isNumber(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 36
    aget v5, v4, v8

    if-ne v5, v6, :cond_2

    .line 37
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v8

    goto :goto_1

    .line 38
    :cond_2
    aget v5, v4, v9

    if-ne v5, v6, :cond_0

    .line 39
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v4, v9

    goto :goto_1

    .line 42
    .end local v1    # "attr":Ljava/lang/String;
    :cond_3
    return-object v4
.end method
