.class public Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;
.super Ljava/lang/Object;
.source "RetainImageCache.java"


# static fields
.field private static sRetainCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;


# instance fields
.field public mRetainedCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/ImageCache;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getOrCreateRetainableCache()Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->sRetainCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;

    if-nez v0, :cond_0

    .line 10
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->sRetainCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;

    .line 12
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;->sRetainCache:Lcom/sec/android/app/videoplayer/popup/changeplayer/RetainImageCache;

    return-object v0
.end method
