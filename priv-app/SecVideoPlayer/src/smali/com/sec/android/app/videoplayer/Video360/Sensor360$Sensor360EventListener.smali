.class Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;
.super Ljava/lang/Object;
.source "Sensor360.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/Video360/Sensor360;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Sensor360EventListener"
.end annotation


# static fields
.field public static final EPSILON:F = 1.0E-9f

.field private static final NS2S:F = 1.0E-9f


# instance fields
.field private mAccel:[F

.field private mGyro:[F


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mAccel:[F

    .line 154
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mGyro:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;-><init>()V

    return-void
.end method

.method private calculateAccMagOrientation()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 218
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccMagMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$900()[F

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mAccel:[F

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sMagnet:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$600()[F

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->getRotationMatrix([F[F[F[F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1000()Z

    move-result v0

    if-nez v0, :cond_0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$700()Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 220
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccMagMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$900()[F

    move-result-object v0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v2

    array-length v2, v2

    invoke-static {v0, v4, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 221
    # setter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1002(Z)Z

    .line 224
    :cond_0
    return-void
.end method

.method private getRotationVectorFromGyro([F[FF)V
    .locals 12
    .param p1, "gyroValues"    # [F
    .param p2, "deltaRotationVector"    # [F
    .param p3, "timeFactor"    # F

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 278
    new-array v1, v11, [F

    .line 281
    .local v1, "normValues":[F
    aget v5, p1, v8

    aget v6, p1, v8

    mul-float/2addr v5, v6

    aget v6, p1, v9

    aget v7, p1, v9

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    aget v6, p1, v10

    aget v7, p1, v10

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v2, v6

    .line 287
    .local v2, "omegaMagnitude":F
    const v5, 0x3089705f    # 1.0E-9f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_0

    .line 288
    aget v5, p1, v8

    div-float/2addr v5, v2

    aput v5, v1, v8

    .line 289
    aget v5, p1, v9

    div-float/2addr v5, v2

    aput v5, v1, v9

    .line 290
    aget v5, p1, v10

    div-float/2addr v5, v2

    aput v5, v1, v10

    .line 297
    :cond_0
    mul-float v4, v2, p3

    .line 298
    .local v4, "thetaOverTwo":F
    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v3, v6

    .line 299
    .local v3, "sinThetaOverTwo":F
    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 300
    .local v0, "cosThetaOverTwo":F
    aget v5, v1, v8

    mul-float/2addr v5, v3

    aput v5, p2, v8

    .line 301
    aget v5, v1, v9

    mul-float/2addr v5, v3

    aput v5, p2, v9

    .line 302
    aget v5, v1, v10

    mul-float/2addr v5, v3

    aput v5, p2, v10

    .line 303
    aput v0, p2, v11

    .line 304
    return-void
.end method

.method private gyroFunction(Landroid/hardware/SensorEvent;)V
    .locals 10
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v4, 0x0

    .line 230
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mAccIsReady:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1000()Z

    move-result v3

    if-eqz v3, :cond_0

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$700()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_1

    .line 251
    :goto_1
    return-void

    :cond_0
    move v3, v4

    .line 230
    goto :goto_0

    .line 235
    :cond_1
    const/4 v3, 0x4

    new-array v2, v3, [F

    .line 236
    .local v2, "deltaVector":[F
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1100()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-eqz v3, :cond_2

    .line 237
    iget-wide v6, p1, Landroid/hardware/SensorEvent;->timestamp:J

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1100()J

    move-result-wide v8

    sub-long/2addr v6, v8

    long-to-float v3, v6

    const v5, 0x3089705f    # 1.0E-9f

    mul-float v0, v3, v5

    .line 238
    .local v0, "dT":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mGyro:[F

    const/4 v6, 0x3

    invoke-static {v3, v4, v5, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mGyro:[F

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v0, v4

    invoke-direct {p0, v3, v2, v4}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->getRotationVectorFromGyro([F[FF)V

    .line 243
    .end local v0    # "dT":F
    :cond_2
    iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J

    # setter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1102(J)J

    .line 246
    const/16 v3, 0x9

    new-array v1, v3, [F

    .line 247
    .local v1, "deltaMatrix":[F
    invoke-static {v1, v2}, Landroid/hardware/SensorManager;->getRotationMatrixFromVector([F[F)V

    .line 250
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v3

    invoke-static {v3, v1}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->matrixMultiplication([F[F)[F

    move-result-object v3

    # setter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$802([F)[F

    goto :goto_1
.end method

.method public static matrixMultiplication([F[F)[F
    .locals 10
    .param p0, "A"    # [F
    .param p1, "B"    # [F

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 254
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 256
    .local v0, "result":[F
    aget v1, p0, v5

    aget v2, p1, v5

    mul-float/2addr v1, v2

    aget v2, p0, v6

    aget v3, p1, v8

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aget v2, p0, v7

    const/4 v3, 0x6

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v0, v5

    .line 257
    aget v1, p0, v5

    aget v2, p1, v6

    mul-float/2addr v1, v2

    aget v2, p0, v6

    aget v3, p1, v9

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aget v2, p0, v7

    const/4 v3, 0x7

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v0, v6

    .line 258
    aget v1, p0, v5

    aget v2, p1, v7

    mul-float/2addr v1, v2

    aget v2, p0, v6

    const/4 v3, 0x5

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aget v2, p0, v7

    const/16 v3, 0x8

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v0, v7

    .line 260
    aget v1, p0, v8

    aget v2, p1, v5

    mul-float/2addr v1, v2

    aget v2, p0, v9

    aget v3, p1, v8

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/4 v2, 0x5

    aget v2, p0, v2

    const/4 v3, 0x6

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v0, v8

    .line 261
    aget v1, p0, v8

    aget v2, p1, v6

    mul-float/2addr v1, v2

    aget v2, p0, v9

    aget v3, p1, v9

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    const/4 v2, 0x5

    aget v2, p0, v2

    const/4 v3, 0x7

    aget v3, p1, v3

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    aput v1, v0, v9

    .line 262
    const/4 v1, 0x5

    aget v2, p0, v8

    aget v3, p1, v7

    mul-float/2addr v2, v3

    aget v3, p0, v9

    const/4 v4, 0x5

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x5

    aget v3, p0, v3

    const/16 v4, 0x8

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 264
    const/4 v1, 0x6

    const/4 v2, 0x6

    aget v2, p0, v2

    aget v3, p1, v5

    mul-float/2addr v2, v3

    const/4 v3, 0x7

    aget v3, p0, v3

    aget v4, p1, v8

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/16 v3, 0x8

    aget v3, p0, v3

    const/4 v4, 0x6

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 265
    const/4 v1, 0x7

    const/4 v2, 0x6

    aget v2, p0, v2

    aget v3, p1, v6

    mul-float/2addr v2, v3

    const/4 v3, 0x7

    aget v3, p0, v3

    aget v4, p1, v9

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/16 v3, 0x8

    aget v3, p0, v3

    const/4 v4, 0x7

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 266
    const/16 v1, 0x8

    const/4 v2, 0x6

    aget v2, p0, v2

    aget v3, p1, v7

    mul-float/2addr v2, v3

    const/4 v3, 0x7

    aget v3, p0, v3

    const/4 v4, 0x5

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    const/16 v3, 0x8

    aget v3, p0, v3

    const/16 v4, 0x8

    aget v4, p1, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 268
    return-object v0
.end method


# virtual methods
.method public initialize()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 211
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x0

    aput v3, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x1

    aput v2, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 212
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x3

    aput v2, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x4

    aput v3, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x5

    aput v2, v0, v1

    .line 213
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x6

    aput v2, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/4 v1, 0x7

    aput v2, v0, v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v0

    const/16 v1, 0x8

    aput v3, v0, v1

    .line 214
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 163
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 13
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 168
    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 207
    :goto_0
    :pswitch_0
    return-void

    .line 171
    :pswitch_1
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccelTimeStamp:J
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$400()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 172
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v4

    invoke-static {v3, v8, v4, v8, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 188
    :goto_1
    iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J

    # setter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccelTimeStamp:J
    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$402(J)J

    .line 192
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->mAccel:[F

    invoke-static {v3, v8, v4, v8, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->calculateAccMagOrientation()V

    goto :goto_0

    .line 174
    :cond_0
    const v2, 0x3c75c290    # 0.015000001f

    .line 175
    .local v2, "timeStep":F
    iget-wide v4, p1, Landroid/hardware/SensorEvent;->timestamp:J

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccelTimeStamp:J
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$400()J

    move-result-wide v6

    sub-long/2addr v4, v6

    long-to-float v3, v4

    const v4, 0x3089705f    # 1.0E-9f

    mul-float v1, v3, v4

    .line 176
    .local v1, "dt":F
    const v3, 0x3c75c290    # 0.015000001f

    const v4, 0x3c75c290    # 0.015000001f

    add-float/2addr v4, v1

    div-float v0, v3, v4

    .line 178
    .local v0, "delta":F
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v3

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v4

    aget v4, v4, v8

    mul-float/2addr v4, v0

    sub-float v5, v10, v0

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v8

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v3, v8

    .line 179
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v3

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v4

    aget v4, v4, v9

    mul-float/2addr v4, v0

    sub-float v5, v10, v0

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v9

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v3, v9

    .line 180
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v3

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGravity:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$500()[F

    move-result-object v4

    aget v4, v4, v11

    mul-float/2addr v4, v0

    sub-float v5, v10, v0

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v11

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    aput v4, v3, v11

    goto :goto_1

    .line 199
    .end local v0    # "delta":F
    .end local v1    # "dt":F
    .end local v2    # "timeStep":F
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$Sensor360EventListener;->gyroFunction(Landroid/hardware/SensorEvent;)V

    goto :goto_0

    .line 203
    :pswitch_3
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sMagnet:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$600()[F

    move-result-object v4

    invoke-static {v3, v8, v4, v8, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 204
    # setter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mMagIsReady:Z
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$702(Z)Z

    goto/16 :goto_0

    .line 168
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
