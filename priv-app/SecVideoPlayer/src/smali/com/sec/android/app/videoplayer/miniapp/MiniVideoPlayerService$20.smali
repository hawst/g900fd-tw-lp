.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 3783
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 2
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 3786
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setIsCoverClosed(Z)V

    .line 3788
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3789
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3790
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$20;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 3794
    :cond_0
    return-void

    .line 3787
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
