.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v5, 0xbb8

    const/4 v4, 0x1

    .line 584
    const/4 v2, 0x0

    .line 585
    .local v2, "retVal":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 586
    sparse-switch p2, :sswitch_data_0

    .line 615
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v3, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 616
    .local v0, "isReturn":Z
    if-eqz v0, :cond_2

    .line 634
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v2

    .line 589
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 591
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v3, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_0

    .line 595
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 597
    .local v1, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 598
    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hidePlayerList(Z)V

    .line 599
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v3, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 606
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changePlayerListBtn()V

    goto :goto_0

    .line 601
    :cond_1
    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->showPlayerList(Z)V

    .line 602
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const v4, 0x36ee80

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_1

    .line 619
    .end local v1    # "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    .restart local v0    # "isReturn":Z
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 621
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 622
    goto :goto_0

    .line 625
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    .line 626
    goto :goto_0

    .line 586
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 589
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 619
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
