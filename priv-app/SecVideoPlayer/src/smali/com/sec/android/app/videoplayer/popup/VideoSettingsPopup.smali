.class public Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;,
        Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;
    }
.end annotation


# static fields
.field public static final MENU_ADAPT_SOUND:I = 0x8

.field public static final MENU_AUDIO_TRACK:I = 0x3

.field public static final MENU_BRIGHTNESS:I = 0x0

.field public static final MENU_CONTEXTUAL:I = 0x4

.field public static final MENU_MAX:I = 0xa

.field public static final MENU_MINI_CONTROLLER:I = 0x6

.field public static final MENU_PLAY_SPEED_MODE:I = 0x1

.field public static final MENU_PREMIUM_MODE:I = 0x9

.field public static final MENU_RESOLUTION:I = 0x64

.field public static final MENU_SOUND_ALIVE:I = 0x2

.field public static final MENU_SUBTITLE:I = 0x7

.field public static final MENU_VIDEO_CAPTURE:I = 0x5

.field private static final TAG:Ljava/lang/String; = "VideoSettingsPopup"

.field private static final TITLE:Ljava/lang/String; = "MENU_TITLE"

.field private static final VALUE:Ljava/lang/String; = "MENU_VALUE"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mAvailableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mIsHeadsetUnplugged:Z

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOnPopupCreatedListener:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

.field private mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

.field private mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

.field private mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

.field public mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

.field private mUIInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

.field public mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

.field private mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

.field private mVisibleMap:Landroid/util/SparseBooleanArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    .line 77
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 831
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 861
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$6;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 110
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    .line 112
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 114
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initVisbleMap()V

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/db/SharedPreference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Lcom/sec/android/app/videoplayer/db/SharedPreference;)Lcom/sec/android/app/videoplayer/db/SharedPreference;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->showPopup(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    return-object p1
.end method

.method private checkSettingMenuVisibile(I)I
    .locals 10
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 137
    const/4 v0, 0x1

    .line 138
    .local v0, "enable":Z
    const/4 v2, 0x1

    .line 140
    .local v2, "returnVal":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    .line 141
    .local v3, "schemeType":Lcom/sec/android/app/videoplayer/type/SchemeType;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    .line 143
    .local v1, "fileInfo":Lcom/sec/android/app/videoplayer/common/VideoFileInfo;
    packed-switch p1, :pswitch_data_0

    .line 190
    const/4 v2, -0x1

    .line 194
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v6, -0x1

    if-eq v2, v6, :cond_1

    .line 195
    if-eqz v0, :cond_8

    move v2, v4

    .line 197
    :cond_1
    :goto_1
    return v2

    .line 145
    :pswitch_1
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiOrNoAudioChannel()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isVideoOnlyClip()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->blockPlaySpeed()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 147
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 152
    :pswitch_2
    const/4 v0, 0x1

    .line 153
    goto :goto_0

    .line 156
    :pswitch_3
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v6, v6, v8

    if-ltz v6, :cond_4

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isCloudFile()Z

    move-result v6

    if-nez v6, :cond_4

    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v6

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isSingleAudioTrack()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 161
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 166
    :pswitch_4
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 167
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 172
    :pswitch_5
    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v6, :cond_6

    .line 173
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->canCaptureVideoFrame()Z

    move-result v0

    goto/16 :goto_0

    .line 175
    :cond_6
    const/4 v0, 0x0

    .line 177
    goto/16 :goto_0

    .line 180
    :pswitch_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->isCriticalLowBatteryStatus()Z

    move-result v6

    if-nez v6, :cond_7

    move v0, v4

    .line 181
    :goto_2
    goto/16 :goto_0

    :cond_7
    move v0, v5

    .line 180
    goto :goto_2

    :cond_8
    move v2, v5

    .line 195
    goto/16 :goto_1

    .line 143
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private dismissSubPopup()V
    .locals 1

    .prologue
    .line 606
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_0

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 608
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    .line 611
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_1

    .line 612
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    if-eqz v0, :cond_1

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->dismiss()V

    .line 615
    :cond_1
    return-void
.end method

.method private getAudioTrackCount()I
    .locals 3

    .prologue
    .line 897
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    .line 898
    .local v1, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getAudioTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v0

    .line 899
    .local v0, "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-eqz v0, :cond_1

    .line 900
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    .line 903
    :goto_1
    return v2

    .line 898
    .end local v0    # "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 903
    .restart local v0    # "audioTrack":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_1
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private initAdaptSound()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 741
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v2, :cond_0

    .line 742
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 743
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0008

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 747
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 748
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 753
    .local v1, "text":Ljava/lang/String;
    :goto_0
    const-string v2, "MENU_VALUE"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 755
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 756
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 758
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    return-void

    .line 750
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private initBrightness()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 644
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 645
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 646
    .local v2, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "MENU_TITLE"

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0023

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 647
    const/4 v4, 0x0

    .line 649
    .local v4, "text":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v5, :cond_0

    .line 650
    new-instance v5, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 653
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->isAutoBrightness()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 654
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a001c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 662
    :goto_0
    const-string v5, "MENU_VALUE"

    invoke-virtual {v2, v5, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 664
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    .end local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "text":Ljava/lang/String;
    :cond_1
    return-void

    .line 656
    .restart local v2    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "text":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    .line 657
    .local v1, "level":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v3

    .line 658
    .local v3, "maxLevel":I
    int-to-float v5, v1

    int-to-float v6, v3

    div-float/2addr v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    float-to-int v0, v5

    .line 659
    .local v0, "currentLevel":I
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private initContextualTag()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    .line 793
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 814
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v0

    .line 796
    .local v0, "isTagBuddyVisible":Z
    const-string v3, "VideoSettingsPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initContextualTag. isTagBuddyVisible : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 799
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 800
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "MENU_TITLE"

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0142

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->getTagBuddyShowStatus()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 804
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 809
    .local v2, "text":Ljava/lang/String;
    :goto_1
    const-string v3, "MENU_VALUE"

    invoke-virtual {v1, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 812
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 806
    .end local v2    # "text":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a001d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "text":Ljava/lang/String;
    goto :goto_1
.end method

.method private initMenuData()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    .line 228
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    if-nez v0, :cond_6

    .line 229
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    .line 233
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PHONE_MINI_CTRL_ENABLE:Z

    if-eqz v0, :cond_0

    .line 235
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initMiniController()V

    .line 238
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->BRIGHTENESS_SETTINGS_ENABLE:Z

    if-eqz v0, :cond_1

    .line 239
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initBrightness()V

    .line 242
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_2

    .line 243
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initVideoCapture()V

    .line 246
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initPlaySpeedMode()V

    .line 248
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v0, :cond_3

    .line 249
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initAdaptSound()V

    .line 252
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initSoundAlive()V

    .line 254
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-nez v0, :cond_4

    .line 255
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initPremiumMode()V

    .line 258
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initMultiTrack()V

    .line 259
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initContextualTag()V

    .line 260
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initSubtitles()V

    .line 261
    return-void

    .line 226
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 231
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method private initMiniController()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 679
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 681
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "MENU_TITLE"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00b1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 682
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 683
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private initMultiTrack()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 781
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 782
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 783
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->getAudioTrackCount()I

    move-result v0

    .line 785
    .local v0, "getCounts":I
    const-string v2, "MENU_VALUE"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 788
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    .end local v0    # "getCounts":I
    .end local v1    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private initPlaySpeedMode()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 697
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 698
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 699
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "MENU_TITLE"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private initPremiumMode()V
    .locals 5

    .prologue
    const/16 v4, 0x9

    .line 688
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 689
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 690
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "MENU_TITLE"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a016e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 692
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 694
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private initSoundAlive()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 706
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 707
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 708
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "MENU_TITLE"

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a012c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getSAEffectMode()I

    move-result v1

    .line 710
    .local v1, "soundAlive":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 712
    .local v2, "text":Ljava/lang/String;
    packed-switch v1, :pswitch_data_0

    .line 733
    :goto_0
    const-string v3, "MENU_VALUE"

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 735
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 738
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "soundAlive":I
    .end local v2    # "text":Ljava/lang/String;
    :cond_0
    return-void

    .line 714
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v1    # "soundAlive":I
    .restart local v2    # "text":Ljava/lang/String;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 715
    goto :goto_0

    .line 718
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a01c6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 719
    goto :goto_0

    .line 722
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00cd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 723
    goto :goto_0

    .line 726
    :pswitch_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0140

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 727
    goto :goto_0

    .line 712
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private initSubtitles()V
    .locals 6

    .prologue
    const/4 v5, 0x7

    .line 761
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v5}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 762
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 763
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "MENU_TITLE"

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 767
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 768
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 773
    .local v1, "text":Ljava/lang/String;
    :goto_0
    const-string v2, "MENU_VALUE"

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 775
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 778
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    return-void

    .line 770
    .restart local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private initVideoCapture()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    .line 670
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, v4}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 672
    .local v0, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "MENU_TITLE"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a01c5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 673
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 676
    .end local v0    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method private initVisbleMap()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->setAllVisible()V

    .line 122
    :cond_0
    return-void
.end method

.method private openPopup(ILandroid/view/View;)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 490
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismissSubPopup()V

    .line 492
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 494
    .local v1, "mainActivity":Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
    packed-switch p1, :pswitch_data_0

    .line 594
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

    if-eqz v3, :cond_1

    .line 595
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;->onPopupCreated(Lcom/sec/android/app/videoplayer/popup/IVideoPopup;)V

    .line 596
    :cond_1
    return-void

    .line 496
    :pswitch_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    if-nez v3, :cond_2

    .line 497
    new-instance v3, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    .line 499
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    if-nez v3, :cond_3

    .line 500
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .line 503
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->show()V

    .line 504
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    goto :goto_0

    .line 508
    :pswitch_1
    const-string v3, "VideoSettingsPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "openPopup. MENU_SOUND_ALIVE. MULTI_SPEAKER : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const-string v4, "com.sec.android.app.videoplayer"

    const-string v5, "SALV"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    if-nez v3, :cond_4

    .line 515
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 517
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    new-instance v4, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$OnSelectedListener;)V

    .line 523
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->showPopup()V

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    goto :goto_0

    .line 531
    :pswitch_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    if-nez v3, :cond_5

    .line 532
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .line 534
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    new-instance v4, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;)V

    .line 540
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->showPopup()V

    .line 541
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    goto/16 :goto_0

    .line 545
    :pswitch_3
    if-eqz v1, :cond_6

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_6

    .line 546
    iget-object v3, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 549
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v2

    .line 550
    .local v2, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTrackType()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_7

    .line 552
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0005

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 553
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.settings.CAPTIONING_SETTINGS"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 554
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "com.android.settings"

    const-string v4, "com.android.settings.Settings$CaptioningSettingsActivity"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 555
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 557
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_7
    new-instance v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    const v5, 0x7f0b0005

    invoke-direct {v3, v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 558
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->show()V

    .line 559
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iput-object v4, v3, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 561
    if-eqz v1, :cond_0

    .line 562
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mSubtitleSetting:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setSubtitleSetting(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V

    goto/16 :goto_0

    .line 568
    .end local v2    # "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    :pswitch_4
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    goto/16 :goto_0

    .line 572
    :pswitch_5
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v3, :cond_0

    .line 573
    iget-object v3, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v3, :cond_0

    .line 574
    iget-object v3, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 575
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const-string v4, "com.sec.android.app.videoplayer"

    const-string v5, "TBDY"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->callContextualTitle()V

    goto/16 :goto_0

    .line 585
    :pswitch_6
    if-eqz p2, :cond_0

    .line 586
    const v3, 0x7f0d009a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->toggle()V

    goto/16 :goto_0

    .line 494
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_6
    .end packed-switch
.end method

.method private showPopup(ILandroid/view/View;)V
    .locals 2
    .param p1, "selected"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 472
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 474
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 482
    :pswitch_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 485
    :pswitch_1
    invoke-direct {p0, v0, p2}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->openPopup(ILandroid/view/View;)V

    .line 487
    .end local v0    # "id":I
    :cond_0
    return-void

    .line 474
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public MenuVisibilityCount()I
    .locals 3

    .prologue
    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v1, v2, :cond_1

    .line 216
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    .line 215
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 219
    :cond_1
    return v0
.end method

.method public callContextualTitle()V
    .locals 3

    .prologue
    .line 817
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_1

    .line 818
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    if-nez v0, :cond_0

    .line 819
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0b0005

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .line 822
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    if-eqz v0, :cond_1

    .line 823
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->initDatas()V

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->show()V

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iput-object v1, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mAlertDialog:Landroid/app/Dialog;

    .line 829
    :cond_1
    return-void
.end method

.method public checkNSetMenuVisibility(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x1

    .line 201
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    if-eqz v2, :cond_0

    .line 202
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->checkSettingMenuVisibile(I)I

    move-result v0

    .line 203
    .local v0, "isMenuVisible":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 204
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    if-ne v0, v1, :cond_1

    :goto_0
    invoke-virtual {v2, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 206
    .end local v0    # "isMenuVisible":I
    :cond_0
    return-void

    .line 204
    .restart local v0    # "isMenuVisible":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 600
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    .line 603
    :cond_0
    return-void
.end method

.method public isPopupShowing()Z
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 460
    :goto_0
    return v0

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->isShowing()Z

    move-result v0

    goto :goto_0

    .line 460
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSubPopupShowing()Z
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->isShowing()Z

    move-result v0

    .line 468
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOrientationChanged()V
    .locals 1

    .prologue
    .line 915
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_0

    .line 916
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 917
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->onOrientationChanged()V

    .line 920
    :cond_0
    return-void
.end method

.method public onSensorChanged()V
    .locals 1

    .prologue
    .line 927
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 929
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->onSensorChanged()V

    .line 932
    :cond_0
    return-void
.end method

.method public removeAllPopup()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 618
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismissSubPopup()V

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->dismiss()V

    .line 622
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    if-eqz v0, :cond_1

    .line 626
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->dismiss()V

    .line 627
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoAdaptSoundPopup:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .line 630
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_2

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    if-eqz v0, :cond_2

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->dismiss()V

    .line 633
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mTagBuddySettingDialog:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    .line 637
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    if-eqz v0, :cond_3

    .line 638
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->dismiss()V

    .line 639
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVideoBrightnessPopup:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    .line 641
    :cond_3
    return-void
.end method

.method public setAllVisible()V
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    return-void
.end method

.method public setDismiss4UnpluggedHeadset()V
    .locals 1

    .prologue
    .line 923
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z

    .line 924
    return-void
.end method

.method public setMenuVisibility(IZ)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mVisibleMap:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 211
    :cond_0
    return-void
.end method

.method public setOnPopupCreatedListener(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .prologue
    .line 911
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mOnPopupCreatedListener:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$OnPopupCreatedListener;

    .line 912
    return-void
.end method

.method public showPopup()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    .line 411
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v2, "VideoSettingsPopup"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 413
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->initMenuData()V

    .line 414
    const/4 v0, 0x0

    .line 415
    .local v0, "adapter":Landroid/widget/SimpleAdapter;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 416
    .local v9, "listPadding":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800eb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 417
    .local v8, "listBottomPadding":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800ea

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 418
    .local v10, "listTopPadding":I
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;

    .end local v0    # "adapter":Landroid/widget/SimpleAdapter;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;

    const v4, 0x7f030010

    new-array v5, v12, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "MENU_TITLE"

    aput-object v6, v5, v1

    const/4 v1, 0x1

    const-string v6, "MENU_VALUE"

    aput-object v6, v5, v1

    new-array v6, v12, [I

    fill-array-data v6, :array_0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$SettingMenuListAdapter;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Landroid/content/Context;Ljava/util/ArrayList;I[Ljava/lang/String;[I)V

    .line 420
    .restart local v0    # "adapter":Landroid/widget/SimpleAdapter;
    new-instance v11, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 421
    .local v11, "listView":Landroid/widget/ListView;
    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 422
    const/high16 v1, 0x2000000

    invoke-virtual {v11, v1}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    .line 423
    invoke-virtual {v11, v9, v10, v9, v8}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 424
    new-instance v1, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    invoke-virtual {v11, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 431
    new-instance v7, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    invoke-direct {v7, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 432
    .local v7, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0126

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 433
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 435
    const v1, 0x7f0a0034

    new-instance v2, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V

    invoke-virtual {v7, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 441
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 442
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    .line 443
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 444
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 445
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 448
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 449
    return-void

    .line 418
    nop

    :array_0
    .array-data 4
        0x7f0d0006
        0x7f0d0096
    .end array-data
.end method

.method public updateSettingPopup()Z
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    .line 131
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->checkNSetMenuVisibility(I)V

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->MenuVisibilityCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
