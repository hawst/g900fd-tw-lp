.class Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$2;
.super Ljava/lang/Object;
.source "VideoMultiTrackPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 100
    const-string v1, "VideoMultiTrackPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "createSelectAudioTrack() which: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getAudioTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v0, v1, p2

    .line 102
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setAudioTrack(I)V

    .line 104
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 105
    return-void
.end method
