.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)V
    .locals 0

    .prologue
    .line 2281
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdated(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2283
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$1300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2284
    const-string v1, "MoviePlaybackService"

    const-string v2, "mSKTcloudListener. player service is destroyed. so this lisentenr is ignored."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2298
    :cond_0
    :goto_0
    return-void

    .line 2288
    :cond_1
    if-eqz p1, :cond_2

    .line 2290
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setDataSourcePrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2291
    :catch_0
    move-exception v0

    .line 2292
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2295
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->access$2400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2296
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer$2;->this$1:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    # getter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->access$2400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2, v3, v3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_0
.end method
