.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0

    .prologue
    .line 551
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 553
    sparse-switch p2, :sswitch_data_0

    .line 589
    :cond_0
    :goto_0
    return v2

    .line 556
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 557
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->hide()V

    .line 559
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 560
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->dismiss()V

    .line 562
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 564
    .local v0, "pressTime":J
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 565
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string v5, "videoplayer.set.lock"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 572
    .end local v0    # "pressTime":J
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 573
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->hide()V

    goto :goto_0

    .line 586
    :sswitch_2
    const/4 v2, 0x0

    goto :goto_0

    .line 553
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x17 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
