.class public Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
.super Ljava/lang/Object;
.source "VideoProgressBarScrubbing.java"


# static fields
.field public static final HALF_SPEED_SCRUBBING:I = 0x2

.field public static final HI_SPEED_SCRUBBING:I = 0x1

.field public static final QUARTER_SPEED_SCRUBBING:I = 0x4

.field public static final START_SPEED_SCRUBBING_HEIGHT_FOR_2ND_SCREEN:F = 40.0f

.field private static mScrubbingSpeedInfoToast:Landroid/widget/Toast;


# instance fields
.field private final HALF_SPEED_SCRUBBING_HEIGHT:F

.field private detailed_seek_help_txt:Landroid/widget/TextView;

.field private detailed_seek_txt:Landroid/widget/TextView;

.field private mComparedProgress:I

.field private mContext:Landroid/content/Context;

.field private mSavedProgress:I

.field private mSavedScrubbingSpeed:I

.field private mScrubbingSpeed:I

.field private mStartedProgress:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const/4 v3, -0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput v5, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    .line 33
    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    .line 35
    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    .line 37
    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    .line 39
    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 47
    const/high16 v3, 0x42a00000    # 80.0f

    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->HALF_SPEED_SCRUBBING_HEIGHT:F

    .line 56
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08005b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 60
    .local v0, "height":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0085

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    sput-object v3, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    .line 62
    sget-object v3, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    const/16 v4, 0x51

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, Landroid/widget/Toast;->setGravity(III)V

    .line 64
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 65
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030008

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 66
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0d0071

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->detailed_seek_txt:Landroid/widget/TextView;

    .line 67
    const v3, 0x7f0d0072

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->detailed_seek_help_txt:Landroid/widget/TextView;

    .line 68
    const v3, 0x7f0d0070

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    sget-object v4, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 69
    sget-object v3, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v3, v2}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 70
    return-void
.end method


# virtual methods
.method public cancelScrubbingSpeedInfoToast()V
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 86
    return-void
.end method

.method public getScrubbingSpeed()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    return v0
.end method

.method public reprocessProgress(I)I
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    if-eq v0, v1, :cond_0

    .line 143
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    .line 145
    iput p1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    .line 149
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 151
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    packed-switch v0, :pswitch_data_0

    .line 171
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mSavedProgress:I

    .line 173
    return p1

    .line 155
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    add-int p1, v0, v1

    .line 157
    goto :goto_0

    .line 161
    :pswitch_2
    iget v0, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x4

    add-int p1, v0, v1

    .line 163
    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v9, 0x7f0a00fd

    const v8, 0x7f0a007d

    const/high16 v7, 0x42200000    # 40.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 92
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v4, v5, Landroid/util/DisplayMetrics;->density:F

    .line 94
    .local v4, "scale":F
    const/high16 v5, 0x42a00000    # 80.0f

    mul-float/2addr v5, v4

    add-float/2addr v5, v6

    float-to-int v0, v5

    .line 96
    .local v0, "heightOfHalf":I
    const v5, 0x7f0a0085

    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setText(I)V

    .line 98
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v1, v5

    .line 100
    .local v1, "nTouchPointY":I
    const/4 v3, 0x1

    .line 102
    .local v3, "returnValue":I
    if-gez v1, :cond_2

    .line 103
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-le v5, v0, :cond_1

    .line 104
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setText(I)V

    .line 105
    const/4 v3, 0x4

    .line 121
    :cond_0
    :goto_0
    iput v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeed:I

    .line 123
    return v3

    .line 107
    :cond_1
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setText(I)V

    .line 108
    const/4 v3, 0x2

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v5

    if-eqz v5, :cond_0

    mul-float v5, v7, v4

    add-float/2addr v5, v6

    float-to-int v5, v5

    if-le v1, v5, :cond_0

    .line 111
    mul-float v5, v7, v4

    float-to-int v5, v5

    sub-int v2, v1, v5

    .line 112
    .local v2, "nTouchPointYDelta":I
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v5

    if-le v5, v0, :cond_3

    .line 113
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setText(I)V

    .line 114
    const/4 v3, 0x4

    goto :goto_0

    .line 116
    :cond_3
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->setText(I)V

    .line 117
    const/4 v3, 0x2

    goto :goto_0
.end method

.method public setText(I)V
    .locals 5
    .param p1, "str_id"    # I

    .prologue
    .line 133
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "scrub_str":Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 135
    .local v2, "top_text":Ljava/lang/String;
    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "bottom_text":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->detailed_seek_txt:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->detailed_seek_help_txt:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public showScrubbingSpeedInfoToast()V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->mScrubbingSpeedInfoToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 78
    return-void
.end method
