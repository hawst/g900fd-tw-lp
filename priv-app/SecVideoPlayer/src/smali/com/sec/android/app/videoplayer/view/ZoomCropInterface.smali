.class public interface abstract Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;
.super Ljava/lang/Object;
.source "ZoomCropInterface.java"


# virtual methods
.method public abstract adjustBoundary(Landroid/graphics/Rect;)V
.end method

.method public abstract adjustZoomState(I)V
.end method

.method public abstract applyCrop(Landroid/graphics/Rect;)Z
.end method

.method public abstract getCropRect()Landroid/graphics/Rect;
.end method

.method public abstract getDelta()Landroid/graphics/Point;
.end method

.method public abstract getVideoHeight()I
.end method

.method public abstract getVideoWidth()I
.end method

.method public abstract getZoomPercentWithMargin()F
.end method

.method public abstract isPanEnabled()Z
.end method

.method public abstract isZoomEnabled()Z
.end method

.method public abstract reset()V
.end method

.method public abstract resetCropRect()V
.end method

.method public abstract resetXYDelta()V
.end method

.method public abstract setDelta(Landroid/graphics/Point;)V
.end method

.method public abstract setPanEnabled(Z)V
.end method

.method public abstract setZoomState(Z)V
.end method

.method public abstract updateVideoFrameData()V
.end method
