.class public Lcom/sec/android/app/videoplayer/slink/SLink;
.super Ljava/lang/Object;
.source "SLink.java"


# static fields
.field private static final DELEY_TIME:I = 0x1f4

.field public static final DEVICE_ID:Ljava/lang/String; = "DEVICE_ID"

.field public static final DEVICE_NAME:Ljava/lang/String; = "DEVICE_NAME"

.field public static final DMC_MODE:Ljava/lang/String; = "DMC_MODE"

.field public static final DOWNLOAD_RESULT_BROADCAST_ACTION:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink.DOWNLOAD_RESULT_BROADCAST_ACTION"

.field public static final ERROR_PATH:Ljava/lang/String; = "http://ERROR"

.field private static final HANDLE_WAIT_SCS_DATA:I = 0x1

.field private static final PLAYING_TYPE_LOCAL:I = 0x1

.field private static final PLAYING_TYPE_NTCL:I = 0x3

.field private static final PLAYING_TYPE_PROXY:I = 0x4

.field private static final PLAYING_TYPE_SAME_AP:I = 0x2

.field public static final SCSConstantValues:[Ljava/lang/String;

.field public static final SECURITY_KEY:Ljava/lang/String; = "8D6D7C96BCFEADC052AE9C7C273BCC3A"

.field public static final TAG:Ljava/lang/String; = "SLink"

.field private static final UNDEFINED:J = -0x1L

.field public static UNKNOWN:Ljava/lang/String; = null

.field public static final VIDEO_APPID:Ljava/lang/String; = "4v4198zs5c"

.field public static final VIDEO_ID:Ljava/lang/String; = "VIDEO_ID"

.field private static sInstance:Lcom/sec/android/app/videoplayer/slink/SLink;


# instance fields
.field private final ID_REQUEST_ACCESSTOKEN:I

.field private mActivity:Landroid/app/Activity;

.field private mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mDuration:J

.field private final mHandler:Landroid/os/Handler;

.field private mISaService:Lcom/msc/sa/aidl/ISAService;

.field private mIsFromSLinkApp:Z

.field private mPlayingType:I

.field private mPosition:J

.field private mRegistrationCode:Ljava/lang/String;

.field private mSACallback:Lcom/msc/sa/aidl/SACallback;

.field private mSAScsCoreConfigString:Ljava/lang/String;

.field mSAServiceConnection:Landroid/content/ServiceConnection;

.field private mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

.field private mSLinkPlaylist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSLinkScsCoreConfigString:Ljava/lang/String;

.field private mScs_data_tryTime:I

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mTempDuration:J

.field private mTempPosition:J

.field private mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

.field private mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 70
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "?AppId="

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "4v4198zs5c"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "?AuthType="

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?ServicePort="

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "1051"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "?ProxyPortInstance="

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "9050"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "?Plain_UDP="

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "5"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "?fwk_target="

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "0"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "?server_type="

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "0"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->SCSConstantValues:[Ljava/lang/String;

    .line 95
    const-string v0, "Unknown"

    sput-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    .line 98
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->sInstance:Lcom/sec/android/app/videoplayer/slink/SLink;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSACallback:Lcom/msc/sa/aidl/SACallback;

    .line 110
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    .line 111
    const/16 v0, 0x11b6

    iput v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->ID_REQUEST_ACCESSTOKEN:I

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 116
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    .line 118
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    .line 120
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    .line 121
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mDuration:J

    .line 122
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempPosition:J

    .line 123
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempDuration:J

    .line 125
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    .line 887
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLink$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/slink/SLink$3;-><init>(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;

    .line 1018
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLink$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/slink/SLink$4;-><init>(Lcom/sec/android/app/videoplayer/slink/SLink;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAServiceConnection:Landroid/content/ServiceConnection;

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/slink/SLink;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I

    return v0
.end method

.method static synthetic access$008(Lcom/sec/android/app/videoplayer/slink/SLink;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/slink/SLink;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->startPlayback()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/slink/SLink;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/slink/SLink;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/slink/SLink;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/slink/SLink;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->destroySAService()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/slink/SLink;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->requestAccessToken()V

    return-void
.end method

.method static synthetic access$802(Lcom/sec/android/app/videoplayer/slink/SLink;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$902(Lcom/sec/android/app/videoplayer/slink/SLink;Lcom/msc/sa/aidl/ISAService;)Lcom/msc/sa/aidl/ISAService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/slink/SLink;
    .param p1, "x1"    # Lcom/msc/sa/aidl/ISAService;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    return-object p1
.end method

.method private bindSAService(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 941
    if-nez p1, :cond_0

    .line 942
    const-string v2, "SLink"

    const-string v3, "bindSAService : context is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    :goto_0
    return-void

    .line 944
    :cond_0
    const-string v2, "SLink"

    const-string v3, "bindSAService : try to bind samsung account service"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 947
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    .line 951
    :cond_1
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 952
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 953
    const-string v2, "com.osp.app.signin"

    const-string v3, "com.msc.sa.service.RequestService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 954
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 955
    const-string v2, "SLink"

    const-string v3, "bindSAService : Bind Service requested! need to wait for call back..."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 956
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 957
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bindSAService : IllegalArgumentException "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private checkCurrentSLinkContentInfo()V
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    if-nez v0, :cond_0

    .line 430
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    .line 432
    :cond_0
    return-void
.end method

.method private destroySAService()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 977
    const-string v1, "SLink"

    const-string v2, "destroySAService E:"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    .line 980
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 986
    :cond_0
    :goto_0
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 987
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    .line 988
    const-string v1, "SLink"

    const-string v2, "destroySAService : done"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    return-void

    .line 981
    :catch_0
    move-exception v0

    .line 982
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "destroySAService :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private findDeviceNameFromCursor(Ljava/lang/String;Landroid/content/ContentResolver;J)[Ljava/lang/String;
    .locals 11
    .param p1, "cursorStr"    # Ljava/lang/String;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "deviceID"    # J

    .prologue
    .line 293
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v3, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v0

    const/4 v5, 0x0

    move-object v0, p2

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 294
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    new-array v10, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    aput-object v1, v10, v0

    const/4 v0, 0x1

    sget-object v1, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    aput-object v1, v10, v0

    .line 295
    .local v10, "retVal":[Ljava/lang/String;
    const-wide/16 v8, -0x1

    .line 297
    .local v8, "id":J
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 298
    :cond_0
    const-string v0, "SLink"

    const-string v1, "findDeviceNameFromCursor : Cursor is null or there is nothing"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 312
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 313
    const/4 v6, 0x0

    .line 316
    :cond_2
    return-object v10

    .line 300
    :cond_3
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v7, v0, :cond_1

    .line 301
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-long v8, v0

    .line 302
    cmp-long v0, v8, p3

    if-nez v0, :cond_4

    .line 303
    const/4 v0, 0x0

    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    .line 304
    const/4 v0, 0x1

    const-string v1, "transport_type"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v10, v0

    goto :goto_0

    .line 307
    :cond_4
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    .line 300
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method private getDLNAPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 471
    if-nez p1, :cond_0

    const-string v4, "http://ERROR"

    .line 487
    :goto_0
    return-object v4

    .line 472
    :cond_0
    const/4 v3, 0x0

    .line 473
    .local v3, "videoUri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoID()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v6, v8, v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getVideoUriInfoBatch(Landroid/content/ContentResolver;J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;

    move-result-object v0

    .line 474
    .local v0, "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;
    if-nez v0, :cond_1

    .line 475
    const-string v4, "SLink"

    const-string v5, "getDLNAPath : uris is null!!"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    const-string v4, "http://ERROR"

    goto :goto_0

    .line 478
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_4

    move v1, v4

    .line 479
    .local v1, "urisLocal":Z
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_5

    move v2, v4

    .line 481
    .local v2, "urisProxy":Z
    :goto_2
    if-eqz v1, :cond_2

    .line 482
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    .line 484
    :cond_2
    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    .line 485
    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    .line 487
    :cond_3
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .end local v1    # "urisLocal":Z
    .end local v2    # "urisProxy":Z
    :cond_4
    move v1, v5

    .line 478
    goto :goto_1

    .restart local v1    # "urisLocal":Z
    :cond_5
    move v2, v5

    .line 479
    goto :goto_2

    .line 487
    .restart local v2    # "urisProxy":Z
    :cond_6
    const-string v4, "http://ERROR"

    goto :goto_0
.end method

.method private getDeviceNameNTypefromDB(Landroid/content/ContentResolver;J)[Ljava/lang/String;
    .locals 6
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "deviceID"    # J

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 277
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/String;

    const-string v2, "transport_type != ?"

    aput-object v2, v0, v4

    const-string v2, "transport_type == ?"

    aput-object v2, v0, v5

    .line 278
    .local v0, "cursorStr":[Ljava/lang/String;
    const-wide/16 v2, 0x1

    cmp-long v2, p2, v2

    if-nez v2, :cond_0

    .line 279
    const-string v2, "transport_type == ?"

    aput-object v2, v0, v4

    .line 280
    const-string v2, "transport_type != ?"

    aput-object v2, v0, v5

    .line 283
    :cond_0
    aget-object v2, v0, v4

    invoke-direct {p0, v2, p1, p2, p3}, Lcom/sec/android/app/videoplayer/slink/SLink;->findDeviceNameFromCursor(Ljava/lang/String;Landroid/content/ContentResolver;J)[Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "retVal":[Ljava/lang/String;
    sget-object v2, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    aget-object v3, v1, v4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    const-string v2, "SLink"

    const-string v3, "getDeviceNamefromDB : try again!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    aget-object v2, v0, v5

    invoke-direct {p0, v2, p1, p2, p3}, Lcom/sec/android/app/videoplayer/slink/SLink;->findDeviceNameFromCursor(Ljava/lang/String;Landroid/content/ContentResolver;J)[Ljava/lang/String;

    move-result-object v1

    .line 289
    .end local v1    # "retVal":[Ljava/lang/String;
    :cond_1
    return-object v1
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->sInstance:Lcom/sec/android/app/videoplayer/slink/SLink;

    if-nez v0, :cond_0

    .line 129
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLink;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/slink/SLink;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->sInstance:Lcom/sec/android/app/videoplayer/slink/SLink;

    .line 131
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/slink/SLink;->sInstance:Lcom/sec/android/app/videoplayer/slink/SLink;

    return-object v0
.end method

.method private getPath(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "getPlayingPath"    # Z

    .prologue
    .line 496
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath E : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 498
    .local v1, "path":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 499
    const-string v9, "SLink"

    const-string v10, "getPath : context is null!!"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    const-string v9, "http://ERROR"

    .line 581
    :goto_0
    return-object v9

    .line 503
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoID()Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getVideoUriInfoBatch(Landroid/content/ContentResolver;J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;

    move-result-object v2

    .line 504
    .local v2, "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;
    if-nez v2, :cond_1

    .line 505
    const-string v9, "SLink"

    const-string v10, "getPath : uris is null!!"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    const-string v9, "http://ERROR"

    goto :goto_0

    .line 509
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_4

    const/4 v3, 0x1

    .line 510
    .local v3, "urisLocal":Z
    :goto_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getScsUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    if-eqz v9, :cond_5

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getScsUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_5

    const/4 v5, 0x1

    .line 511
    .local v5, "urisSCS":Z
    :goto_2
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getSameAccessPointUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getSameAccessPointUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_6

    const/4 v6, 0x1

    .line 512
    .local v6, "urisSameAP":Z
    :goto_3
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_7

    const/4 v4, 0x1

    .line 514
    .local v4, "urisProxy":Z
    :goto_4
    const/4 v8, 0x0

    .line 515
    .local v8, "videoUri":Landroid/net/Uri;
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath - urisLocal:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / urisSCS: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / urisSameAP: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / urisProxy: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    if-eqz p2, :cond_12

    .line 518
    const/4 v7, 0x0

    .line 521
    .local v7, "videoOptimization":Z
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "com.samsung.android.sdk.samsunglink.SlinkMediaStore.Settings.VIDEO_OPTIMIZATION"

    invoke-static {v9, v10}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Settings;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 526
    :goto_5
    if-eqz v3, :cond_8

    .line 527
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getLocalUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 528
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    .line 562
    :goto_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSchemeType()I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->setKeyType(I)V

    .line 564
    if-eqz v8, :cond_2

    .line 565
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 567
    :cond_2
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath : videoOptimization:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / type: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / playing path: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_11

    .line 570
    :cond_3
    const-string v9, "http://ERROR"

    goto/16 :goto_0

    .line 509
    .end local v3    # "urisLocal":Z
    .end local v4    # "urisProxy":Z
    .end local v5    # "urisSCS":Z
    .end local v6    # "urisSameAP":Z
    .end local v7    # "videoOptimization":Z
    .end local v8    # "videoUri":Landroid/net/Uri;
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 510
    .restart local v3    # "urisLocal":Z
    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 511
    .restart local v5    # "urisSCS":Z
    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 512
    .restart local v6    # "urisSameAP":Z
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 522
    .restart local v4    # "urisProxy":Z
    .restart local v7    # "videoOptimization":Z
    .restart local v8    # "videoUri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 523
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath - VIDEO_OPTIMIZATION - SettingNotFoundException :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 529
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_8
    if-eqz v6, :cond_c

    .line 530
    if-eqz v7, :cond_9

    .line 531
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getSameAccessPointUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getOptimizedVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 532
    if-nez v8, :cond_9

    .line 533
    const-string v9, "SLink"

    const-string v10, "getPath - urisSameAP : videoOptimization is true but path returns Null we can\'t play videoOptimization"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    :cond_9
    if-nez v8, :cond_a

    .line 537
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getSameAccessPointUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 539
    :cond_a
    if-eqz v8, :cond_b

    const-string v9, "http"

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 540
    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v10, "slhttp"

    invoke-virtual {v9, v10}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    .line 542
    :cond_b
    const/4 v9, 0x2

    iput v9, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    goto/16 :goto_6

    .line 543
    :cond_c
    if-eqz v5, :cond_f

    .line 544
    if-eqz v7, :cond_d

    .line 545
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getScsUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getOptimizedVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 546
    if-nez v8, :cond_d

    .line 547
    const-string v9, "SLink"

    const-string v10, "getPath - urisSCS : videoOptimization is true but path returns Null we can\'t play videoOptimization"

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    :cond_d
    if-nez v8, :cond_e

    .line 551
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getScsUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 553
    :cond_e
    const/4 v9, 0x3

    iput v9, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    goto/16 :goto_6

    .line 554
    :cond_f
    if-eqz v4, :cond_10

    .line 555
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    .line 556
    const/4 v9, 0x4

    iput v9, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    goto/16 :goto_6

    .line 558
    :cond_10
    const/4 v9, -0x1

    iput v9, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    .line 559
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath : playing type not set!!! please check with SLink Platform / mPlayingType :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    :cond_11
    move-object v9, v1

    .line 572
    goto/16 :goto_0

    .line 573
    .end local v7    # "videoOptimization":Z
    :cond_12
    if-eqz v4, :cond_13

    .line 574
    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfoBatch;->getHttpProxyUris()Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media$VideoUriInfo;->getCaptionUri()Landroid/net/Uri;

    move-result-object v8

    .line 575
    if-eqz v8, :cond_13

    .line 576
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 577
    const-string v9, "SLink"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getPath : subtitle path: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v1

    .line 578
    goto/16 :goto_0

    .line 581
    :cond_13
    const-string v9, "http://ERROR"

    goto/16 :goto_0
.end method

.method private getSortOrderString(I)Ljava/lang/String;
    .locals 4
    .param p1, "sortOrder"    # I

    .prologue
    .line 1093
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSortOrderString : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1095
    packed-switch p1, :pswitch_data_0

    .line 1116
    const-string v1, "title COLLATE LOCALIZED ASC"

    :goto_0
    return-object v1

    .line 1097
    :pswitch_0
    const-string v1, "isPlayed DESC"

    goto :goto_0

    .line 1100
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1101
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "(date_added * 1) DESC, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1102
    const-string v1, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1103
    const-string v1, "(datetaken * 1) DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1104
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1107
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v1, "title COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 1110
    :pswitch_3
    const-string v1, "(_size * 1) DESC"

    goto :goto_0

    .line 1113
    :pswitch_4
    const-string v1, "mime_type ASC"

    goto :goto_0

    .line 1095
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private isSlinkScsDataExist()Z
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requestAccessToken()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 992
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-nez v4, :cond_4

    .line 993
    :cond_0
    const-string v7, "SLink"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "requestAccessToken : mContext or mISaService is null "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-nez v4, :cond_2

    move v4, v5

    :goto_0
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " / "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-nez v8, :cond_3

    :goto_1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-nez v4, :cond_1

    .line 995
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->bindSAService(Landroid/content/Context;)V

    .line 1016
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v4, v6

    .line 993
    goto :goto_0

    :cond_3
    move v5, v6

    goto :goto_1

    .line 1000
    :cond_4
    new-instance v4, Lcom/msc/sa/aidl/SACallback;

    invoke-direct {v4}, Lcom/msc/sa/aidl/SACallback;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSACallback:Lcom/msc/sa/aidl/SACallback;

    .line 1003
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    const-string v5, "4v4198zs5c"

    const-string v6, "8D6D7C96BCFEADC052AE9C7C273BCC3A"

    const-string v7, "com.sec.android.app.videoplayer"

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSACallback:Lcom/msc/sa/aidl/SACallback;

    invoke-interface {v4, v5, v6, v7, v8}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    .line 1005
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 1006
    .local v3, "manager":Landroid/accounts/AccountManager;
    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1007
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v4, v0

    if-lez v4, :cond_1

    .line 1008
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1009
    .local v1, "data":Landroid/os/Bundle;
    const-string v4, "additional"

    sget-object v5, Lcom/msc/sa/aidl/SACallback;->ScsConfigString:[Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1010
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    const/16 v5, 0x11b6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v4, v5, v6, v1}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1013
    .end local v0    # "accountArr":[Landroid/accounts/Account;
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v3    # "manager":Landroid/accounts/AccountManager;
    :catch_0
    move-exception v2

    .line 1014
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_2
.end method

.method private runSCSCoreConfigService()V
    .locals 4

    .prologue
    .line 1034
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-nez v2, :cond_0

    .line 1035
    const-string v2, "SLink"

    const-string v3, "runSCSCoreConfigService : mContext is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    :goto_0
    return-void

    .line 1038
    :cond_0
    const-string v2, "SLink"

    const-string v3, "runSCSCoreConfigService :"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.sec.android.app.videoplayer"

    const-string v3, "com.sec.android.app.videoplayer.slink.SCSCoreInitService"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    .local v0, "cn":Landroid/content/ComponentName;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 1041
    .local v1, "i":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private saveScsConfigStringinPref(Ljava/lang/String;)V
    .locals 4
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 725
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveScsConfigStringinPref :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    .line 727
    .local v0, "PrefMgr":Lcom/sec/android/app/videoplayer/db/SharedPreference;
    const-string v1, "scs_config_string"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Ljava/lang/String;)V

    .line 728
    return-void
.end method

.method private sendToOtherDevices(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    .prologue
    .line 824
    new-instance v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 825
    .local v1, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->allowCloudStorageTargetDevice:Z

    .line 826
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v2

    invoke-virtual {v2, p2, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v0

    .line 827
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 828
    return-void
.end method

.method private setContentInfo(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/content/Intent;)V
    .locals 35
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cr"    # Landroid/content/ContentResolver;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 185
    const-string v4, "VIDEO_ID"

    const-wide/16 v6, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v32

    .line 186
    .local v32, "videoID":J
    const-string v4, "DEVICE_ID"

    const-wide/16 v6, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v24

    .line 188
    .local v24, "deviceID":J
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    .line 191
    :try_start_0
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    const-string v6, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :goto_0
    const-wide/16 v26, -0x1

    .line 198
    .local v26, "id":J
    const/4 v10, 0x0

    .line 199
    .local v10, "name":Ljava/lang/String;
    const-wide/16 v30, -0x1

    .line 200
    .local v30, "size":J
    const/4 v12, 0x0

    .line 201
    .local v12, "type":Ljava/lang/String;
    const/4 v13, 0x0

    .line 202
    .local v13, "captionType":Ljava/lang/String;
    const-wide/16 v22, -0x1

    .line 203
    .local v22, "date":J
    const-wide/16 v28, -0x1

    .line 204
    .local v28, "resumePosition":J
    const/16 v18, -0x1

    .line 205
    .local v18, "cursorPos":I
    const-wide/16 v16, -0x1

    .line 207
    .local v16, "duration":J
    const/16 v20, 0x0

    .line 209
    .local v20, "currentPositionForSlinkApp":I
    const-string v4, "SLink"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setContentInfo : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v32

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    if-eqz p1, :cond_0

    .line 212
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0a0145

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    .line 214
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    .line 216
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    .line 218
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->releaseSLinkCursor()V

    .line 220
    const/4 v4, 0x2

    new-array v0, v4, [Ljava/lang/String;

    move-object/from16 v19, v0

    const/4 v4, 0x0

    sget-object v6, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    aput-object v6, v19, v4

    const/4 v4, 0x1

    sget-object v6, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    aput-object v6, v19, v4

    .line 222
    .local v19, "DeviceNameAndType":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    if-eqz v4, :cond_3

    .line 223
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getCursorFromViewIntent(Landroid/content/Intent;)Landroid/database/Cursor;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    .line 224
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_1

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "device_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    .line 226
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v20

    .line 227
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-wide/from16 v2, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceNameNTypefromDB(Landroid/content/ContentResolver;J)[Ljava/lang/String;

    move-result-object v19

    .line 238
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-nez v4, :cond_5

    .line 239
    :cond_2
    const-string v4, "SLink"

    const-string v6, "Cursor is null or there is nothing"

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->releaseSLinkCursor()V

    .line 274
    :goto_2
    return-void

    .line 192
    .end local v10    # "name":Ljava/lang/String;
    .end local v12    # "type":Ljava/lang/String;
    .end local v13    # "captionType":Ljava/lang/String;
    .end local v16    # "duration":J
    .end local v18    # "cursorPos":I
    .end local v19    # "DeviceNameAndType":[Ljava/lang/String;
    .end local v20    # "currentPositionForSlinkApp":I
    .end local v22    # "date":J
    .end local v26    # "id":J
    .end local v28    # "resumePosition":J
    .end local v30    # "size":J
    :catch_0
    move-exception v21

    .line 193
    .local v21, "e":Ljava/lang/NullPointerException;
    const-string v4, "SLink"

    const-string v6, "setContentInfo : SlinkMediaStore is null / Not From SLink APP "

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    goto/16 :goto_0

    .line 230
    .end local v21    # "e":Ljava/lang/NullPointerException;
    .restart local v10    # "name":Ljava/lang/String;
    .restart local v12    # "type":Ljava/lang/String;
    .restart local v13    # "captionType":Ljava/lang/String;
    .restart local v16    # "duration":J
    .restart local v18    # "cursorPos":I
    .restart local v19    # "DeviceNameAndType":[Ljava/lang/String;
    .restart local v20    # "currentPositionForSlinkApp":I
    .restart local v22    # "date":J
    .restart local v26    # "id":J
    .restart local v28    # "resumePosition":J
    .restart local v30    # "size":J
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v4, v24, v6

    if-gez v4, :cond_4

    const-wide/16 v6, 0x0

    cmp-long v4, v32, v6

    if-gez v4, :cond_4

    .line 231
    const-string v4, "SLink"

    const-string v6, "setContentInfo - deviceID or videoID is invalid"

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 233
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-wide/from16 v2, v24

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceNameNTypefromDB(Landroid/content/ContentResolver;J)[Ljava/lang/String;

    move-result-object v19

    .line 234
    invoke-static/range {v24 .. v25}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Video$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v4, "sortby"

    const/4 v9, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSortOrderString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v4, p2

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    goto :goto_1

    .line 244
    :cond_5
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v5, v4, :cond_9

    .line 245
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "_id"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    .line 246
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "_display_name"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 247
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "_size"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    .line 248
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "mime_type"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 249
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "caption_type"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 250
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "date_modified"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v22

    .line 251
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "bookmark"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 252
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->getPosition()I

    move-result v18

    .line 253
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v7, "duration"

    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 255
    if-nez v10, :cond_6

    .line 256
    sget-object v10, Lcom/sec/android/app/videoplayer/slink/SLink;->UNKNOWN:Ljava/lang/String;

    .line 259
    :cond_6
    cmp-long v4, v26, v32

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    if-eqz v4, :cond_8

    move/from16 v0, v20

    if-ne v5, v0, :cond_8

    .line 260
    :cond_7
    const-string v4, "SLink"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setContentInfo : i:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " id:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    new-instance v4, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v19, v7

    const/4 v8, 0x1

    aget-object v8, v19, v8

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-direct/range {v4 .. v18}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;JI)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    .line 263
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    move-object/from16 v34, v0

    new-instance v4, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x0

    aget-object v7, v19, v7

    const/4 v8, 0x1

    aget-object v8, v19, v8

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-direct/range {v4 .. v18}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;-><init>(ILjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;JI)V

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    .line 244
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 267
    :cond_9
    const-string v4, "SLink"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setContentInfo: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceID()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoID()Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSize()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getCaptionType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDate()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getResumePosition()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDuration()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method private declared-synchronized startPlayback()V
    .locals 2

    .prologue
    .line 932
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    .line 933
    const-string v0, "SLink"

    const-string v1, "startPlayback E :"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 938
    :goto_0
    monitor-exit p0

    return-void

    .line 936
    :cond_0
    :try_start_1
    const-string v0, "SLink"

    const-string v1, "startPlayback : mServiceUtil is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 932
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public acquireWakeLock(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    if-nez v1, :cond_0

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "tag":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->createWakeLock(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 154
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->acquire()V

    .line 155
    const-string v1, "SLink"

    const-string v2, "acquireWakeLock :"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .end local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public acquireWakeLockPopupPlayer(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 168
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    if-nez v1, :cond_0

    .line 169
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "tag":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->createWakeLock(Ljava/lang/String;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 171
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->acquire()V

    .line 172
    const-string v1, "SLink"

    const-string v2, "acquireWakeLockPopupPlayer :"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .end local v0    # "tag":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public doDownloadActionFromtReceiver(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 790
    const-string v2, "SLink"

    const-string v3, "doDownloadActionFromReciver E:"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    .line 792
    .local v1, "mediaStoreUris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalPathsFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 793
    .local v0, "localPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSlinkDownloadResult - uris"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSlinkDownloadResult - path"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 797
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    invoke-virtual {v3, p1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->openShareVia(Landroid/content/Context;Landroid/net/Uri;)V

    .line 798
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    if-eqz v2, :cond_0

    .line 799
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/slink/SLink;->setContentInfo(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/content/Intent;)V

    .line 801
    :cond_0
    return-void
.end method

.method public downloadCurrentFile(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getLocalUris()Landroid/net/Uri;

    move-result-object v6

    .line 759
    .local v6, "mediaStoreUri":Landroid/net/Uri;
    if-eqz v6, :cond_1

    .line 760
    const-string v0, "SLink"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "downloadCurrentFile openShareVia mediaStoreUri: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->openShareVia(Landroid/content/Context;Landroid/net/Uri;)V

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 765
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    if-eqz v0, :cond_0

    .line 766
    const-wide/16 v2, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->downloadFiles(Landroid/app/Activity;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public downloadFiles(Landroid/app/Activity;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "videoID"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "text"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 771
    new-instance v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 772
    .local v5, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    iput-boolean v4, v5, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->skipIfDuplicate:Z

    .line 774
    const/4 v1, 0x0

    .line 776
    .local v1, "mediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    const-wide/16 v2, -0x1

    cmp-long v0, p2, v2

    if-eqz v0, :cond_1

    .line 777
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getMediaSet(J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    .line 781
    :goto_0
    if-eqz v1, :cond_0

    .line 782
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v0

    move-object v2, p4

    move-object v3, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createModalDownloadActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Ljava/lang/String;Ljava/lang/String;ZLcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v6

    .line 783
    .local v6, "i":Landroid/content/Intent;
    invoke-virtual {p1, v6}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 786
    .end local v6    # "i":Landroid/content/Intent;
    :cond_0
    const-string v0, "SLink"

    const-string v2, "downloadFiles E : "

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    return-void

    .line 779
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    goto :goto_0
.end method

.method public downloadSubtitleFile(Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;Landroid/content/Context;)V
    .locals 5
    .param p1, "subUtil"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 435
    const-string v2, "SLink"

    const-string v3, "downloadSubtitleFiles E"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    if-eqz p2, :cond_0

    if-nez p1, :cond_2

    .line 437
    :cond_0
    const-string v2, "SLink"

    const-string v3, "downloadSubtitleFiles : context is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    :cond_1
    :goto_0
    return-void

    .line 440
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->clearRemoteSubtitle()V

    .line 441
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getCaptionType()Ljava/lang/String;

    move-result-object v1

    .line 442
    .local v1, "subtitletype":Ljava/lang/String;
    invoke-virtual {p0, p2}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSubtitlePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 443
    .local v0, "subtitleUrl":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 444
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "downloadSubtitleFiles : subtitleUrl = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 445
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 446
    invoke-virtual {p1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 447
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->downloadRemoteSubTitleFile(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCaptionType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 424
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getCaptionType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContentTitle(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)Ljava/lang/String;
    .locals 4
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 347
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getIndexOfArray()I

    move-result v0

    .line 348
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 349
    :cond_0
    const-string v2, "SLink"

    const-string v3, "getContentTitle :  can\'t get item!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const/4 v2, 0x0

    .line 365
    :goto_0
    return-object v2

    .line 353
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 355
    .local v1, "max":I
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 356
    add-int/lit8 v0, v0, -0x1

    .line 357
    if-gez v0, :cond_2

    .line 358
    move v0, v1

    .line 365
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoName()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 359
    :cond_3
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 360
    add-int/lit8 v0, v0, 0x1

    .line 361
    if-le v0, v1, :cond_2

    .line 362
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getDate()J
    .locals 2

    .prologue
    .line 409
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDate()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDeviceID()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 370
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDeviceID()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 375
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 380
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 394
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getSize()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getLocalUris()Landroid/net/Uri;
    .locals 5

    .prologue
    .line 746
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getCursorPosition()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 747
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getCursorPosition()I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 748
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    const-string v4, "local_source_media_id"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 749
    .local v0, "mediaStoreId":J
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLocalUris mediaStoreId :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 751
    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 754
    .end local v0    # "mediaStoreId":J
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 2

    .prologue
    .line 804
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoID()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getMediaSet(J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    return-object v0
.end method

.method public getMediaSet(J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 5
    .param p1, "videoID"    # J

    .prologue
    .line 808
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 810
    :goto_0
    return-object v1

    .line 809
    :cond_0
    const/4 v1, 0x1

    new-array v0, v1, [J

    const/4 v1, 0x0

    aput-wide p1, v0, v1

    .line 810
    .local v0, "videoIDs":[J
    invoke-static {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v1

    goto :goto_0
.end method

.method public getPlayingPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 456
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    .line 457
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPath(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getScsCoreConfig(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getResumePosition()J
    .locals 2

    .prologue
    .line 414
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 415
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getResumePosition()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSavedDuration()J
    .locals 2

    .prologue
    .line 1085
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mDuration:J

    return-wide v0
.end method

.method public getSavedPosition()J
    .locals 2

    .prologue
    .line 1081
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    return-wide v0
.end method

.method public getSchemeType()I
    .locals 2

    .prologue
    .line 452
    iget v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x2b

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x2a

    goto :goto_0
.end method

.method public getScsCoreConfig(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 590
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 591
    :cond_0
    const-string v1, "SLink"

    const-string v2, "getScsCoreConfig. path is null or empty"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    .end local p2    # "path":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 595
    .restart local p2    # "path":Ljava/lang/String;
    :cond_1
    const-string v1, "NTCL://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "ntcl://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 596
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z

    move-result v1

    if-nez v1, :cond_3

    .line 597
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->updateSlinkScsCoreConfig(Landroid/content/Context;)V

    .line 600
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 602
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 603
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 607
    :cond_4
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScsCoreConfig : Path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 609
    :cond_5
    const-string v1, "SLink"

    const-string v2, "getScsCoreConfig : SLink String is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 614
    :cond_6
    const-string v1, "SLink"

    const-string v2, "getScsCoreConfig : SamsungAccount String is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    :goto_2
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScsCoreConfig ntcl : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto/16 :goto_0

    .line 611
    :cond_7
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScsCoreConfig : SLink String: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 616
    :cond_8
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getScsCoreConfig : SamsungAccount String: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 622
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_9
    const-string v1, "SLink"

    const-string v2, "getScsCoreConfig : path is not start with ntcl no need to send scs data"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getSize()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSubtitlePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 491
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPath(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 492
    .local v0, "retVal":Ljava/lang/String;
    const-string v1, "http://ERROR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    .end local v0    # "retVal":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUri(Landroid/content/Context;)Landroid/net/Uri;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 586
    .local v0, "path":Ljava/lang/String;
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method public getVideoID()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 384
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoID()Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getVideoName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public init(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 137
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    .line 139
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    .line 140
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mActivity:Landroid/app/Activity;

    .line 141
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    .line 142
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPlayingType:I

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->resetSavedPosition()V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->loadSAScsConfigStringinPref(Landroid/content/Context;)V

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->setContentInfo(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/content/Intent;)V

    .line 148
    return-void
.end method

.method public isFromSLinkApp()Z
    .locals 1

    .prologue
    .line 1089
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    return v0
.end method

.method public isSAScsDataExist()Z
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScsDataExist()Z
    .locals 1

    .prologue
    .line 654
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSAScsDataExist()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScsTransportType()Z
    .locals 4

    .prologue
    .line 667
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 668
    .local v0, "retVal":Z
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isScsTransportType :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    return v0
.end method

.method public loadSAScsConfigStringinPref(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 731
    if-nez p1, :cond_1

    .line 732
    const-string v1, "SLink"

    const-string v2, "loadSAScsConfigStringinPref : context is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 737
    :cond_1
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    .line 738
    .local v0, "PrefMgr":Lcom/sec/android/app/videoplayer/db/SharedPreference;
    const-string v1, "scs_config_string"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 739
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadSAScsConfigStringinPref :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSAScsDataExist()Z

    move-result v1

    if-nez v1, :cond_0

    .line 742
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->bindSAService(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onPause(Z)V
    .locals 2
    .param p1, "isFinish"    # Z

    .prologue
    .line 1057
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->releaseWakeLock()V

    .line 1058
    if-eqz p1, :cond_0

    .line 1059
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->releaseSLinkCursor()V

    .line 1060
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->resetSavedPosition()V

    .line 1061
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mIsFromSLinkApp:Z

    .line 1066
    :goto_0
    return-void

    .line 1063
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempPosition:J

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    .line 1064
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempDuration:J

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mDuration:J

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 1053
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->acquireWakeLock(Landroid/content/Context;)V

    .line 1054
    return-void
.end method

.method public refreshScsConfigString(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Landroid/content/Context;)V
    .locals 2
    .param p1, "serviceUtil"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 711
    const-string v0, "SLink"

    const-string v1, "refreshScsConfigString E:"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-nez v0, :cond_0

    .line 714
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/slink/SLink;->bindSAService(Landroid/content/Context;)V

    .line 719
    :goto_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 720
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->saveScsConfigStringinPref(Ljava/lang/String;)V

    .line 721
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->waitingForScsDataUpdate(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Z

    .line 722
    return-void

    .line 716
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->requestAccessToken()V

    goto :goto_0
.end method

.method public releaseSLinkCursor()V
    .locals 2

    .prologue
    .line 1045
    const-string v0, "SLink"

    const-string v1, "releaseSLinkCursor E :"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1048
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mCursor:Landroid/database/Cursor;

    .line 1050
    :cond_0
    return-void
.end method

.method public releaseWakeLock()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->release()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 163
    const-string v0, "SLink"

    const-string v1, "releaseWakeLock :"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_0
    return-void
.end method

.method public releaseWakeLockPopupPlayer()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->release()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mWakeLockPopupPlayer:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    .line 180
    const-string v0, "SLink"

    const-string v1, "releaseWakeLockPopupPlayer :"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    :cond_0
    return-void
.end method

.method public resetSavedPosition()V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 1074
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    .line 1075
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mDuration:J

    .line 1076
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempPosition:J

    .line 1077
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempDuration:J

    .line 1078
    return-void
.end method

.method public saveResumePosition(Landroid/content/Context;JJ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resumpos"    # J
    .param p4, "duration"    # J

    .prologue
    .line 831
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveResumePosition E:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 833
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v1, p2, p3}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->setResumePosition(J)V

    .line 835
    iput-wide p2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempPosition:J

    .line 836
    iput-wide p4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mTempDuration:J

    .line 838
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLink$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink$1;-><init>(Lcom/sec/android/app/videoplayer/slink/SLink;Landroid/content/Context;)V

    .line 855
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Long;Ljava/lang/Void;Ljava/lang/Boolean;>;"
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoID()Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 857
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->updateVideoPlayedTime(Landroid/content/Context;)V

    .line 858
    return-void
.end method

.method public sendToOtherDevices(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 814
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getMediaSet()Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->sendToOtherDevices(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V

    .line 815
    return-void
.end method

.method public sendToOtherDevicesLocalContents(Landroid/content/Context;J)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "videoID"    # J

    .prologue
    .line 818
    const/4 v2, 0x1

    new-array v1, v2, [J

    const/4 v2, 0x0

    aput-wide p2, v1, v2

    .line 819
    .local v1, "videoIDs":[J
    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    .line 820
    .local v0, "mediaSet":Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/videoplayer/slink/SLink;->sendToOtherDevices(Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;)V

    .line 821
    return-void
.end method

.method public setDLNAPath(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 461
    if-eqz p1, :cond_0

    .line 462
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getDLNAPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 463
    :cond_0
    return-void
.end method

.method public setNormalPath(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 466
    if-eqz p1, :cond_0

    .line 467
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 468
    :cond_0
    return-void
.end method

.method public setSavedPosition(J)V
    .locals 5
    .param p1, "pos"    # J

    .prologue
    .line 1069
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1070
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mPosition:J

    .line 1071
    :cond_0
    return-void
.end method

.method public unBindSAService()V
    .locals 4

    .prologue
    .line 963
    const-string v1, "SLink"

    const-string v2, "unBindservice E:"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    .line 966
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 967
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->destroySAService()V

    .line 968
    const-string v1, "SLink"

    const-string v2, "unBindservice : unbinded"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 974
    :cond_0
    :goto_0
    return-void

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SLink"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unBindservice :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updatePlayingdata(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;)V
    .locals 4
    .param p1, "direction"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->checkCurrentSLinkContentInfo()V

    .line 321
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getIndexOfArray()I

    move-result v0

    .line 322
    .local v0, "index":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 323
    :cond_0
    const-string v2, "SLink"

    const-string v3, "updatePlayingdata :  can\'t update item!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 329
    .local v1, "max":I
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->PREV:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 330
    add-int/lit8 v0, v0, -0x1

    .line 331
    if-gez v0, :cond_2

    .line 332
    move v0, v1

    .line 342
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkPlaylist:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    goto :goto_0

    .line 333
    :cond_3
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->NEXT:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 334
    add-int/lit8 v0, v0, 0x1

    .line 335
    if-le v0, v1, :cond_2

    .line 336
    const/4 v0, 0x0

    goto :goto_1

    .line 337
    :cond_4
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->BOTTOM:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 338
    move v0, v1

    goto :goto_1

    .line 339
    :cond_5
    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->TOP:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PlayDirection;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 340
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateSAScsConfigString(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 6
    .param p1, "token"    # Ljava/lang/String;
    .param p2, "scsConfigString"    # [Ljava/lang/String;
    .param p3, "scsResultString"    # [Ljava/lang/String;

    .prologue
    .line 674
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateSAScsConfigString E : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 677
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 678
    .local v1, "sb":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "?token="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_0

    .line 680
    const/16 v2, 0x3f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 681
    aget-object v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 682
    const/16 v2, 0x3d

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 683
    aget-object v2, p3, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 679
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 686
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    .line 687
    const-string v2, "SLink"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateSAScsConfigString : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->unBindSAService()V

    .line 689
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSAScsCoreConfigString:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->saveScsConfigStringinPref(Ljava/lang/String;)V

    .line 692
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SCSCoreInitService;->isServiceRunning()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 693
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->runSCSCoreConfigService()V

    .line 696
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->getVideoID()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 697
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getPlayingPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 699
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z

    move-result v2

    if-nez v2, :cond_2

    .line 700
    const-string v2, "SLink"

    const-string v3, "updateSAScsConfigString : SLink Data is not exist try to get again"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->updateSlinkScsCoreConfig(Landroid/content/Context;)V

    .line 704
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 705
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->startPlayback()V

    .line 707
    :cond_3
    return-void
.end method

.method public updateSlinkScsCoreConfig(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 628
    if-nez p1, :cond_0

    .line 629
    const-string v3, "SLink"

    const-string v4, "updateSlinkScsCoreConfig : Context is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    :goto_0
    return-void

    .line 633
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 634
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    .line 636
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getScsCoreConfig()Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    move-result-object v0

    .line 637
    .local v0, "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    if-eqz v0, :cond_1

    .line 638
    const/4 v3, 0x6

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "?GroupId="

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getGroupId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "?PeerId="

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getPeerId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "?InstanceID="

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 644
    .local v2, "strArray":[Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->StringArrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    new-instance v3, Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/videoplayer/slink/SLink;->SCSConstantValues:[Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->StringArrayToString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    .line 647
    const-string v3, "SLink"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateSlinkScsCoreConfig :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkScsCoreConfigString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 649
    .end local v2    # "strArray":[Ljava/lang/String;
    :cond_1
    const-string v3, "SLink"

    const-string v4, "updateSlinkScsCoreConfig : getScsCoreConfig returns Null from SLink!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public updateVideoPlayedTime(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 862
    const-string v1, "SLink"

    const-string v2, "updateVideoPlayedTime E:"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 863
    new-instance v0, Lcom/sec/android/app/videoplayer/slink/SLink$2;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/videoplayer/slink/SLink$2;-><init>(Lcom/sec/android/app/videoplayer/slink/SLink;Landroid/content/Context;)V

    .line 869
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Long;Ljava/lang/Void;Ljava/lang/Boolean;>;"
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mSLinkContentInfo:Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/slink/SLinkContentInfo;->getVideoID()Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 870
    return-void
.end method

.method public waitingForScsDataUpdate(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Z
    .locals 4
    .param p1, "serviceUtil"    # Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 873
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 874
    iput v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mScs_data_tryTime:I

    .line 875
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isSlinkScsDataExist()Z

    move-result v2

    if-nez v2, :cond_0

    .line 876
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/slink/SLink;->updateSlinkScsCoreConfig(Landroid/content/Context;)V

    .line 878
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/slink/SLink;->isScsDataExist()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 883
    :goto_0
    return v0

    .line 881
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 882
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/slink/SLink;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 883
    goto :goto_0
.end method
