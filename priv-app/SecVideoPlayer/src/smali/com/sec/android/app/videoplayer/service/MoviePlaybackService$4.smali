.class Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;
.super Ljava/lang/Object;
.source "MoviePlaybackService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0

    .prologue
    .line 726
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 4
    .param p1, "event"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 729
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "motion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 758
    :cond_0
    :goto_0
    return-void

    .line 732
    :pswitch_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 733
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-nez v1, :cond_0

    .line 734
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 735
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    const-string v2, "com.sec.android.app.videoplayer"

    const-string v3, "VTNO"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pause()V

    .line 742
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_2

    .line 743
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 746
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v1, :cond_3

    .line 747
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->blockSmartPause()V

    .line 751
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;->this$0:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z

    goto :goto_0

    .line 730
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method
