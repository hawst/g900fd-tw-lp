.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 1997
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1999
    const/4 v1, 0x0

    .line 2000
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2001
    sparse-switch p2, :sswitch_data_0

    .line 2023
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 2024
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 2042
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 2004
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 2006
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2007
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/16 v3, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V

    goto :goto_0

    .line 2011
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2012
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->oneFrameForward()V

    .line 2013
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a0165

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToastTitleController(I)V

    goto :goto_0

    .line 2027
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 2029
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2030
    goto :goto_0

    .line 2033
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2034
    goto :goto_0

    .line 2001
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 2004
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 2027
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
