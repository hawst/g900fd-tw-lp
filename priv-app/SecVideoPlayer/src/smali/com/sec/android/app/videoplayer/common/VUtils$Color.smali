.class public final enum Lcom/sec/android/app/videoplayer/common/VUtils$Color;
.super Ljava/lang/Enum;
.source "VUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/common/VUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Color"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/common/VUtils$Color;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$Color;

.field public static final enum BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

.field public static final enum NONE:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

.field public static final enum NULL:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

.field public static final enum TRANSPARENCY:Lcom/sec/android/app/videoplayer/common/VUtils$Color;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 860
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils$Color;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NONE:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 861
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/common/VUtils$Color;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NULL:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 862
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    const-string v1, "BLACK"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils$Color;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 863
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    const-string v1, "TRANSPARENCY"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/videoplayer/common/VUtils$Color;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->TRANSPARENCY:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    .line 859
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NONE:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->NULL:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->TRANSPARENCY:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->$VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 859
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/common/VUtils$Color;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 859
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/common/VUtils$Color;
    .locals 1

    .prologue
    .line 859
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->$VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/common/VUtils$Color;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    return-object v0
.end method
