.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 2864
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$1;

    .prologue
    .line 2864
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    return-void
.end method

.method private reSizeAIA(I)V
    .locals 25
    .param p1, "scaleValue"    # I

    .prologue
    .line 2894
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f080039

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    .line 2895
    .local v9, "minWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f080035

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v7, v0

    .line 2896
    .local v7, "margin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f080037

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v8, v0

    .line 2897
    .local v8, "maxWidth":I
    sget-boolean v19, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v19, :cond_0

    .line 2898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v19

    if-nez v19, :cond_0

    .line 2899
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f08003a

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    .line 2900
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f080038

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v8, v0

    .line 2901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f080036

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v7, v0

    .line 2905
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v6

    .line 2906
    .local v6, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v18, v0

    .line 2907
    .local v18, "width":I
    iget v4, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2908
    .local v4, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoWidth()I

    move-result v17

    .line 2909
    .local v17, "videoWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getVideoHeight()I

    move-result v16

    .line 2910
    .local v16, "videoHeight":I
    const/4 v3, 0x0

    .line 2911
    .local v3, "baseWidth":I
    const/4 v2, 0x0

    .line 2912
    .local v2, "baseHeight":I
    const/4 v13, 0x0

    .line 2913
    .local v13, "screenWidth":I
    const/4 v12, 0x0

    .line 2915
    .local v12, "screenHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v5

    .line 2917
    .local v5, "isWideClip":Z
    if-lez v17, :cond_1

    if-lez v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v19

    if-lez v19, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v19

    if-gtz v19, :cond_2

    .line 3001
    :cond_1
    :goto_0
    return-void

    .line 2921
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    .line 2922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v13

    .line 2923
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v12

    .line 2930
    :goto_1
    if-lez p1, :cond_b

    .line 2931
    move/from16 v0, v18

    if-ge v0, v13, :cond_1

    if-ge v4, v12, :cond_1

    .line 2937
    :cond_3
    add-int v3, v18, p1

    .line 2938
    add-int v2, v4, p1

    .line 2941
    if-le v3, v13, :cond_4

    move v3, v13

    .line 2942
    :cond_4
    if-le v2, v12, :cond_5

    move v2, v12

    .line 2946
    :cond_5
    const/4 v11, 0x0

    .local v11, "newWidth":I
    const/4 v10, 0x0

    .line 2947
    .local v10, "newHeight":I
    if-eqz v5, :cond_e

    .line 2948
    if-ge v3, v9, :cond_6

    move v3, v9

    .line 2949
    :cond_6
    sub-int v8, v13, v7

    .line 2950
    if-le v3, v8, :cond_7

    move v3, v8

    .line 2952
    :cond_7
    int-to-float v0, v3

    move/from16 v19, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    div-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v14, v0

    .line 2953
    .local v14, "tempHeight":I
    if-gt v14, v12, :cond_d

    .line 2954
    move v11, v3

    .line 2955
    move v10, v14

    .line 2981
    .end local v14    # "tempHeight":I
    :cond_8
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkAndUpdateCtrlLayout(I)V

    .line 2982
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilitySubtitleView(Z)V
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)V

    .line 2983
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSubtitleTextSize(II)V
    invoke-static {v0, v11, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;II)V

    .line 2984
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 2986
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isRequiredSmoothing()Z
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 2987
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v19

    if-eqz v19, :cond_9

    .line 2989
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    new-instance v20, Landroid/view/animation/TranslateAnimation;

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v21, v0

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->x:I

    move/from16 v22, v0

    add-int v22, v22, v11

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v23, v0

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    move/from16 v24, v0

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    iget v0, v6, Landroid/view/WindowManager$LayoutParams;->y:I

    move/from16 v24, v0

    add-int v24, v24, v10

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    invoke-direct/range {v20 .. v24}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;
    invoke-static/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    .line 2990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/view/animation/Animation;

    move-result-object v19

    const-wide/16 v20, 0xc8

    invoke-virtual/range {v19 .. v21}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mRootLayout:Landroid/widget/RelativeLayout;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/view/animation/Animation;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 2992
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mResizeAnim:Landroid/view/animation/Animation;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$5300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/view/animation/Animation;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/animation/Animation;->startNow()V

    .line 2996
    :cond_9
    iput v11, v6, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2997
    iput v10, v6, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3000
    const-string v19, "MiniVideoPlayerService"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "reSizeAIA :"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " x "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2925
    .end local v10    # "newHeight":I
    .end local v11    # "newWidth":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v13

    .line 2926
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v12

    goto/16 :goto_1

    .line 2933
    :cond_b
    if-eqz v5, :cond_c

    move/from16 v0, v18

    if-le v0, v9, :cond_1

    .line 2934
    :cond_c
    if-nez v5, :cond_3

    if-gt v4, v9, :cond_3

    goto/16 :goto_0

    .line 2957
    .restart local v10    # "newHeight":I
    .restart local v11    # "newWidth":I
    .restart local v14    # "tempHeight":I
    :cond_d
    int-to-float v0, v12

    move/from16 v19, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v11, v0

    .line 2958
    move v10, v12

    goto/16 :goto_2

    .line 2961
    .end local v14    # "tempHeight":I
    :cond_e
    if-ge v2, v9, :cond_f

    move v2, v9

    .line 2962
    :cond_f
    sub-int v8, v12, v7

    .line 2963
    if-le v2, v8, :cond_10

    move v2, v8

    .line 2965
    :cond_10
    int-to-float v0, v2

    move/from16 v19, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    div-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v15, v0

    .line 2966
    .local v15, "tempWidth":I
    if-gt v15, v13, :cond_11

    .line 2967
    move v11, v15

    .line 2968
    move v10, v2

    .line 2970
    if-ge v10, v9, :cond_8

    .line 2971
    move v10, v9

    .line 2972
    int-to-float v0, v10

    move/from16 v19, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    div-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v11, v0

    goto/16 :goto_2

    .line 2976
    :cond_11
    move v11, v13

    .line 2977
    int-to-float v0, v13

    move/from16 v19, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v21, v0

    div-float v20, v20, v21

    mul-float v19, v19, v20

    move/from16 v0, v19

    float-to-int v10, v0

    goto/16 :goto_2
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 6
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    .line 2868
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)F

    move-result v2

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    mul-float v0, v2, v3

    .line 2869
    .local v0, "ScaleFactor":F
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)F

    move-result v2

    sub-float v2, v0, v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget v3, v3, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleMultiplier:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 2871
    .local v1, "scaleValue":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 2872
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2874
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/ProgressBar;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 2875
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/ProgressBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2877
    :cond_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v2, :cond_2

    .line 2878
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBtnCtrlLayout:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->forceHide()V

    .line 2881
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->bScreenScaling:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4902(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 2883
    if-nez v1, :cond_3

    .line 2884
    const-string v2, "MiniVideoPlayerService"

    const-string v3, "onScale scaleValue is 0 No need for resizing!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2888
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScaleFactor:F
    invoke-static {v2, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;F)F

    .line 2890
    return v4

    .line 2886
    :cond_3
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$ScaleListener;->reSizeAIA(I)V

    goto :goto_0
.end method
