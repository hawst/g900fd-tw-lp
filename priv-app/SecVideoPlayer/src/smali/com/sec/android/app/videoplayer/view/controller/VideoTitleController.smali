.class public Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.super Landroid/widget/RelativeLayout;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;,
        Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;
    }
.end annotation


# static fields
.field private static final EAR_SHOCK_VOLUME_VALUE:I = 0xa

.field private static final FADE_OUT_VOLUME_BAR:I = 0xa

.field private static final FADE_OUT_VOLUME_BAR_DELAY:I = 0x7d0

.field private static final LAUNCH_WATCHON_APP:I = 0x20

.field private static final LAYOUT_ID:I = 0x12c

.field public static final POPUP_VOLUMEBAR_FOCUSABLE:Z = true

.field public static final POPUP_VOLUMEBAR_UNFOCUSABLE:Z = false

.field private static final START_CHANGE_PLAYER:I = 0x2

.field private static final START_VIDEO_CAPTURE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "VideoTitleController"

.field private static mShowing:Z


# instance fields
.field private mAllShareMoreBtn:Landroid/widget/ImageButton;

.field private mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

.field private mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

.field private mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

.field private mAllshareListener:Landroid/view/View$OnTouchListener;

.field private mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

.field private mAllshareTitleText:Landroid/widget/TextView;

.field private mAllshareVolPopupLayout:Landroid/view/View;

.field private mAllshareVolumeDown:Landroid/widget/ImageButton;

.field private mAllshareVolumeDownClickListener:Landroid/view/View$OnClickListener;

.field private mAllshareVolumeMute:Landroid/widget/ImageButton;

.field private mAllshareVolumeMuteClickListener:Landroid/view/View$OnClickListener;

.field private mAllshareVolumeUp:Landroid/widget/ImageButton;

.field private mAllshareVolumeUpClickListener:Landroid/view/View$OnClickListener;

.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mButtonEnable:Z

.field private mCaptureBtn:Landroid/widget/ImageButton;

.field private mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mChangePlayerBtn:Landroid/widget/ImageButton;

.field private mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

.field private mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

.field private mConnectionCheckPopup:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

.field private mDeleteBtn:Landroid/widget/ImageButton;

.field private mDeleteBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mDeleteBtnTouchListener:Landroid/view/View$OnTouchListener;

.field mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

.field private mHandler:Landroid/os/Handler;

.field private mHoverListenerTitle:Landroid/view/View$OnHoverListener;

.field private mHoverPopupWindowTitle:Landroid/widget/HoverPopupWindow;

.field public mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

.field private mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

.field private mMoreBtn:Landroid/widget/ImageButton;

.field private mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOneFrameBackwardBtn:Landroid/widget/ImageButton;

.field private mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mOneFrameForwardBtn:Landroid/widget/ImageButton;

.field private mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

.field public mPopupAllshareVolBar:Landroid/widget/PopupWindow;

.field mPopupMenu:Landroid/widget/PopupMenu;

.field public mPopupVolBar:Landroid/widget/PopupWindow;

.field private mRoot:Landroid/view/View;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSelectShareMenuAdapter:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

.field private mSharePopupMenu:Landroid/widget/ListPopupWindow;

.field private mSoundAliveBtn:Landroid/widget/ImageButton;

.field private mSoundAliveBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mSoundAliveBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mTitleKeyListener:Landroid/view/View$OnKeyListener;

.field private mTitleLayout:Landroid/widget/RelativeLayout;

.field private mTitleText:Landroid/widget/TextView;

.field private mTitleTextLayout:Landroid/widget/RelativeLayout;

.field private mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

.field private mVolPopupLayout:Landroid/view/View;

.field private mVolumeBtn:Landroid/widget/ImageButton;

.field private mVolumeBtnKeyListener:Landroid/view/View$OnKeyListener;

.field private mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

.field public mVolumeSeekBarPopup:Landroid/widget/SeekBar;

.field private mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mVolumeTextPopup:Landroid/widget/TextView;

.field private mWatchTVBtn:Landroid/widget/ImageButton;

.field private mWatermarkLayout:Landroid/widget/RelativeLayout;

.field private mWatermarkText:Landroid/widget/TextView;

.field private mWathTVBtnListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mButtonEnable:Z

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 131
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    .line 141
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    .line 143
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 145
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 147
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    .line 161
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    .line 163
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 165
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    .line 173
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    .line 175
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    .line 177
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    .line 179
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    .line 198
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    .line 200
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatermarkLayout:Landroid/widget/RelativeLayout;

    .line 202
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatermarkText:Landroid/widget/TextView;

    .line 208
    sget-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    .line 210
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHoverPopupWindowTitle:Landroid/widget/HoverPopupWindow;

    .line 212
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .line 214
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 216
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 218
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    .line 220
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 695
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHoverListenerTitle:Landroid/view/View$OnHoverListener;

    .line 835
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$7;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 877
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$8;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 922
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$9;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 961
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$10;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1092
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$13;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareListener:Landroid/view/View$OnTouchListener;

    .line 1141
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$14;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1191
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$15;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWathTVBtnListener:Landroid/view/View$OnTouchListener;

    .line 1753
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$19;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 1797
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$20;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$20;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1850
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$21;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$21;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDownClickListener:Landroid/view/View$OnClickListener;

    .line 1864
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$22;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$22;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUpClickListener:Landroid/view/View$OnClickListener;

    .line 1878
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$23;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMuteClickListener:Landroid/view/View$OnClickListener;

    .line 1892
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$24;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleKeyListener:Landroid/view/View$OnKeyListener;

    .line 1921
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$25;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$25;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 1969
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$26;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$26;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 1997
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$27;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 2046
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$28;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$28;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 2076
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$29;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 2125
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$30;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 2160
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$31;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    .line 2300
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 2331
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$33;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$33;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 2339
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$34;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$34;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 2516
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$35;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 2551
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$36;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtnKeyListener:Landroid/view/View$OnKeyListener;

    .line 226
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    .line 227
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    .line 228
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    .line 229
    check-cast p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local p1    # "context":Landroid/content/Context;
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 230
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 231
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 233
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    .line 236
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->initFloatingWindow()V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 239
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .param p1, "x1"    # I

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->keepShowingController(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mConnectionCheckPopup:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeBtnPopup()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVideoSoundAlivePopup:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHoverPopupWindowTitle:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowingSubPopupMenu()Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/KeyEvent;

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method private checkSupportChangePlayer()Z
    .locals 4

    .prologue
    .line 2400
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isWfdSupport()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoDbId()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->checkDmcDisabled()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isHdmiConnected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSideSyncInfo()Lcom/sec/android/app/videoplayer/data/SideSyncInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/data/SideSyncInfo;->isSinkRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private commonKeyListener(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v10, 0x7f0d012a

    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x1

    .line 313
    const v4, 0x7f0d01d7

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    .line 315
    const v4, 0x7f0d0007

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 316
    .local v3, "viewGroup":Landroid/widget/RelativeLayout;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 318
    .local v1, "layoutInflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030046

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    .line 320
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 334
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-nez v4, :cond_15

    .line 335
    new-instance v4, Landroid/widget/PopupWindow;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    .line 340
    :goto_0
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v4, :cond_0

    .line 341
    const v4, 0x7f030001

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    .line 343
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 357
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    if-nez v4, :cond_16

    .line 358
    new-instance v4, Landroid/widget/PopupWindow;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    .line 364
    :cond_0
    :goto_1
    const v4, 0x7f0d01e1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    if-eqz v4, :cond_1

    .line 367
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 368
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 373
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setLayoutLeftIDofTitleTextLayout()V

    .line 375
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_18

    .line 376
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v4, :cond_2

    .line 377
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setRotateBtn(Landroid/content/Context;Landroid/view/View;)V

    .line 379
    :cond_2
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v4, :cond_17

    .line 380
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v4, :cond_3

    .line 381
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5, p1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setPopupPlayerBtn(Landroid/content/Context;Landroid/view/View;)V

    .line 387
    :cond_3
    :goto_2
    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 388
    const v4, 0x7f0d012c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 389
    const v4, 0x7f0d012e

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 429
    :goto_3
    const v4, 0x7f0d01e2

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    .line 430
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    if-eqz v4, :cond_4

    .line 431
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->enableMultiSelection(Z)V

    .line 433
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHoverListenerTitle:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 435
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHoverPopupWindowTitle:Landroid/widget/HoverPopupWindow;

    .line 436
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 437
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 438
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    const-string v5, "/system/fonts/Roboto-Regular.ttf"

    invoke-static {v5}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 444
    :cond_4
    :goto_4
    const v4, 0x7f0d01db

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    .line 445
    const v4, 0x7f0d01e8

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareTitleText:Landroid/widget/TextView;

    .line 447
    const v4, 0x7f0d000a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    .line 448
    const v4, 0x7f0d0009

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    .line 449
    const v4, 0x7f0d000b

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    .line 451
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d0207

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeTextPopup:Landroid/widget/TextView;

    .line 452
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d0205

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    .line 453
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v4, :cond_5

    .line 454
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMode(I)V

    .line 455
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarChangeListenerpopup:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 456
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 457
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 458
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    const/4 v6, 0x3

    invoke-virtual {v5, v6}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAudioShockWarningEnabled()V

    .line 464
    :cond_5
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v4, :cond_8

    .line 465
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d000a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    .line 466
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d0009

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    .line 467
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d000b

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    .line 469
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    if-eqz v4, :cond_6

    .line 470
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 471
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 472
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDownClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeDown:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a008e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 476
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    if-eqz v4, :cond_7

    .line 477
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 478
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 479
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUpClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 480
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeUp:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a008f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 483
    :cond_7
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    if-eqz v4, :cond_8

    .line 484
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 485
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 486
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMuteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareCtrlVolumeMute:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0088

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 491
    :cond_8
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v4, :cond_9

    .line 492
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    const v5, 0x7f0d0208

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    .line 493
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_9

    .line 494
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 495
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a012c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 496
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 497
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSoundAliveBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 501
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    if-eqz v4, :cond_a

    .line 502
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 503
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 504
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDownClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 505
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeDown:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a008e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 508
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    if-eqz v4, :cond_b

    .line 509
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 510
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 511
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUpClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 512
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeUp:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a008f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 515
    :cond_b
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    if-eqz v4, :cond_c

    .line 516
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 517
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 518
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMuteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 519
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolumeMute:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a0088

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 522
    :cond_c
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_d

    .line 523
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 525
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v4

    if-nez v4, :cond_1e

    .line 526
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v5, 0x7f0201a3

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 531
    :goto_5
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 532
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 533
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a008d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 534
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 537
    :cond_d
    const v4, 0x7f0d01c1

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    .line 539
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_e

    .line 540
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a014f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 541
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 542
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 543
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 544
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 547
    :cond_e
    const v4, 0x7f0d01ea

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    .line 549
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    if-eqz v4, :cond_f

    .line 550
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v6, 0x7f0a014f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 551
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 552
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 553
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 554
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 557
    :cond_f
    const-string v4, "VideoTitleController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initctrilview. WATCH_ON function : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-boolean v6, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const v4, 0x7f0d01de

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    .line 561
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    if-eqz v4, :cond_20

    .line 562
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_10

    .line 563
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWathTVBtnListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 565
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v4

    if-eqz v4, :cond_1f

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->isWatchONEnable()Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 566
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 583
    :cond_10
    :goto_6
    const v4, 0x7f0d01d9

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    .line 585
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_11

    .line 586
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v4

    if-eqz v4, :cond_21

    .line 587
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 588
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 589
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 590
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 591
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00a2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 598
    :cond_11
    :goto_7
    const v4, 0x7f0d01c9

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    .line 600
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_13

    .line 601
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v4

    if-eqz v4, :cond_22

    .line 602
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 603
    const/4 v2, 0x0

    .line 604
    .local v2, "rightMargin":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 605
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080074

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v2, v4

    .line 608
    :cond_12
    const v4, 0x7f0d01da

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 609
    .local v0, "LP_VolumeBtn":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 610
    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 611
    const/16 v4, 0xb

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 612
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 613
    const v4, 0x7f0d01da

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 624
    .end local v0    # "LP_VolumeBtn":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v2    # "rightMargin":I
    :cond_13
    :goto_8
    const v4, 0x7f0d01e6

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    .line 626
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_14

    .line 627
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v4

    if-eqz v4, :cond_23

    .line 628
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 638
    :cond_14
    :goto_9
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    .line 639
    return-void

    .line 337
    :cond_15
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    goto/16 :goto_0

    .line 360
    :cond_16
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    goto/16 :goto_1

    .line 384
    :cond_17
    const v4, 0x7f0d0138

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 391
    :cond_18
    const v4, 0x7f0d0138

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 392
    const v4, 0x7f0d010d

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 394
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v4, :cond_1c

    .line 395
    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    .line 396
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_19

    .line 397
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 398
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a014e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 399
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 400
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 401
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 404
    :cond_19
    const v4, 0x7f0d012c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    .line 405
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_1a

    .line 406
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 407
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 408
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 409
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0165

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 410
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameForwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 413
    :cond_1a
    const v4, 0x7f0d012e

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    .line 414
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_1b

    .line 415
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 417
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a016f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 419
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mOneFrameBackwardBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 421
    :cond_1b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    goto/16 :goto_3

    .line 423
    :cond_1c
    invoke-virtual {p1, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 424
    const v4, 0x7f0d012c

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 425
    const v4, 0x7f0d012e

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 440
    :cond_1d
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    const-string v5, "/system/fonts/Roboto-Light.ttf"

    invoke-static {v5}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_4

    .line 528
    :cond_1e
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v5, 0x7f0201a0

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto/16 :goto_5

    .line 568
    :cond_1f
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_6

    .line 572
    :cond_20
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    if-eqz v4, :cond_10

    .line 573
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mWatchTVBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_6

    .line 594
    :cond_21
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeleteBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_7

    .line 616
    :cond_22
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 617
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 618
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 619
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0163

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 620
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_8

    .line 630
    :cond_23
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 631
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 632
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 633
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0163

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 634
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v4, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto/16 :goto_9
.end method

.method private initFloatingWindow()V
    .locals 4

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 251
    .local v0, "mDecor":Landroid/view/View;
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 252
    .local v1, "p":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 254
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_0

    .line 255
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    :cond_0
    return-void
.end method

.method private isPhoneView(Landroid/view/View;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 687
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a014e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0165

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a016f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    :cond_0
    const/4 v0, 0x1

    .line 692
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isShowing()Z
    .locals 1

    .prologue
    .line 1268
    sget-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    return v0
.end method

.method private isShowingSubPopupMenu()Z
    .locals 1

    .prologue
    .line 832
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->getInstance()Lcom/sec/android/app/videoplayer/util/MenuHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/MenuHelper;->isShowingSubMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowingPopupMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keepShowingController(I)V
    .locals 1
    .param p1, "timeout"    # I

    .prologue
    .line 2671
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    .line 2672
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keepShowingController(I)V

    .line 2674
    :cond_0
    return-void
.end method

.method private setEnableChangePlayerBtn(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1719
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 1737
    :goto_0
    return-void

    .line 1721
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1722
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1724
    if-eqz p1, :cond_1

    .line 1725
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setLayoutLeftIDofTitleTextLayout()V

    .line 1726
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1727
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1729
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1730
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1735
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setLayoutLeftIDofTitleTextLayout()V

    goto :goto_0

    .line 1732
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 1733
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method private setLayoutLeftIDofTitleTextLayout()V
    .locals 3

    .prologue
    .line 2469
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_0

    .line 2482
    :goto_0
    return-void

    .line 2472
    :cond_0
    const v1, 0x7f0d01dc

    .line 2473
    .local v1, "setTitleLeftOfID":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v2, :cond_2

    .line 2474
    const v1, 0x7f0d01e0

    .line 2479
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2480
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2481
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleTextLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 2475
    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 2476
    const v1, 0x7f0d01da

    goto :goto_1
.end method

.method private setVolumeBtnPopup()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1476
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1477
    .local v1, "mFormatBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v1, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 1479
    .local v2, "mFormatter":Ljava/util/Formatter;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1481
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v3

    .line 1483
    .local v3, "vol":I
    if-nez v3, :cond_1

    .line 1484
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v5, 0x7f0201a3

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1489
    :goto_0
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 1490
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeTextPopup:Landroid/widget/TextView;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v2, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1491
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    .line 1493
    if-nez v3, :cond_2

    .line 1494
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0088

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1499
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1500
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getSubviewBtnController()Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1501
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getSubviewBtnController()Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVolumeBtn()V

    .line 1504
    :cond_0
    return-void

    .line 1486
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    const v5, 0x7f0201a0

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 1496
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a008d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateEasyModeBtn()V
    .locals 5

    .prologue
    const v4, 0x7f0d01d8

    const/4 v3, 0x0

    const v2, 0x7f0d01da

    .line 2498
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2499
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2500
    .local v0, "LP_volume_btn":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2501
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 2502
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2504
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2506
    .end local v0    # "LP_volume_btn":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2507
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2508
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d01c9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2509
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d01d9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2514
    :cond_2
    :goto_0
    return-void

    .line 2511
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updatePersonalPageImage()V
    .locals 3

    .prologue
    const v2, 0x7f0d00fc

    .line 1223
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1224
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPlayingFromPersonalPage(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1225
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1230
    :cond_1
    :goto_0
    return-void

    .line 1227
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateSoundAliveBtn()V
    .locals 2

    .prologue
    const v1, 0x7f0d0208

    .line 2461
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isVideoOnlyClip()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2462
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2466
    :goto_0
    return-void

    .line 2464
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public canCaptureVideoFrame()Z
    .locals 1

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->canCaptureVideoFrame()Z

    move-result v0

    return v0
.end method

.method public change_Margin(I)V
    .locals 1
    .param p1, "windowWidth"    # I

    .prologue
    .line 2441
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2442
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    .line 2443
    :cond_0
    return-void
.end method

.method public checkShowingAndDismissPopupVolBar()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1447
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1449
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1466
    :goto_0
    return v1

    .line 1450
    :catch_0
    move-exception v0

    .line 1451
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1452
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 1453
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1456
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1458
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 1459
    :catch_2
    move-exception v0

    .line 1460
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 1461
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 1462
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 1466
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dismissChangePlayerPopup()V
    .locals 1

    .prologue
    .line 2155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 2156
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->dismiss()V

    .line 2158
    :cond_0
    return-void
.end method

.method public dismissDeletePopup()V
    .locals 1

    .prologue
    .line 1441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1442
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mDeletePopup:Lcom/sec/android/app/videoplayer/popup/DeletePopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/DeletePopup;->dismiss()V

    .line 1444
    :cond_0
    return-void
.end method

.method public dismissPopupMenu()V
    .locals 2

    .prologue
    .line 808
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_0

    .line 809
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    .line 810
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 816
    :cond_0
    :goto_0
    return-void

    .line 812
    :catch_0
    move-exception v0

    .line 814
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public forceHide()V
    .locals 1

    .prologue
    .line 1305
    sget-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    if-eqz v0, :cond_0

    .line 1306
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 1307
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleAllViews()V

    .line 1308
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    .line 1310
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 5

    .prologue
    const v3, 0x7f0d01d7

    const/4 v4, 0x0

    .line 1272
    sget-boolean v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    if-eqz v2, :cond_1

    .line 1273
    const/4 v0, 0x0

    .line 1274
    .local v0, "dy":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1275
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getHeight()I

    move-result v3

    add-int v0, v2, v3

    .line 1280
    :goto_0
    const/4 v1, 0x0

    .line 1281
    .local v1, "translateOff":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    neg-int v2, v0

    int-to-float v2, v2

    invoke-direct {v1, v4, v4, v4, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1282
    .restart local v1    # "translateOff":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1283
    invoke-virtual {v1}, Landroid/view/animation/Animation;->startNow()V

    .line 1284
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1285
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 1286
    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$16;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$16;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1300
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    .line 1302
    .end local v0    # "dy":I
    .end local v1    # "translateOff":Landroid/view/animation/Animation;
    :cond_1
    return-void

    .line 1277
    .restart local v0    # "dy":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public hide(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 1313
    if-eqz p1, :cond_0

    .line 1314
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 1315
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    .line 1317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 1318
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1321
    :cond_0
    return-void
.end method

.method public hideVolumeBarPopup()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 1470
    const-wide/16 v0, 0x7d0

    .line 1471
    .local v0, "delay":J
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1472
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1473
    return-void
.end method

.method public isShowingPopupMenu()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 820
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_0

    .line 821
    const/4 v1, 0x1

    .line 827
    :cond_0
    :goto_0
    return v1

    .line 824
    :catch_0
    move-exception v0

    .line 826
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isVolumeBarShowing()Z
    .locals 1

    .prologue
    .line 2391
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2392
    const/4 v0, 0x1

    .line 2394
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 298
    const-string v1, "VideoTitleController"

    const-string v2, "makeControllerView"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 300
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03003e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    .line 301
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setPadding()V

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->initControllerView(Landroid/view/View;)V

    .line 303
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showAllshareTitleControl(Z)V

    .line 304
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method public makeToggleMute()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2416
    invoke-static {}, Landroid/media/AudioManager;->isMediaSilentMode()Z

    move-result v0

    .line 2417
    .local v0, "isMute":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Landroid/media/AudioManager;->setMediaSilentMode(Z)V

    .line 2418
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2, v2}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 2419
    return-void

    :cond_0
    move v1, v2

    .line 2417
    goto :goto_0
.end method

.method public onDmrListChanged()V
    .locals 1

    .prologue
    .line 2410
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 2411
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->updateChangePlayerList()V

    .line 2413
    :cond_0
    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 242
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->initControllerView(Landroid/view/View;)V

    .line 246
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 14
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 643
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v11

    if-nez v11, :cond_1

    .line 644
    :cond_0
    const/4 v11, 0x1

    .line 682
    :goto_0
    return v11

    .line 645
    :cond_1
    const/4 v11, 0x2

    new-array v6, v11, [I

    .line 646
    .local v6, "screenPos":[I
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 647
    .local v2, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 648
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 650
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 651
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v10

    .line 652
    .local v10, "width":I
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 653
    .local v3, "height":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v7, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 654
    .local v7, "screenWidth":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v11

    iget v5, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 655
    .local v5, "screenHeight":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080157

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v8, v11

    .line 657
    .local v8, "toastMargin":I
    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-interface {v11}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 658
    .local v9, "viewStr":Ljava/lang/String;
    const/4 v11, 0x0

    invoke-static {v1, v9, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 660
    .local v0, "cheatSheet":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a014e

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a0165

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a016f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->getCurrentRotation()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v11

    if-eqz v11, :cond_4

    .line 661
    :cond_3
    mul-int/lit8 v3, v3, 0x2

    .line 664
    :cond_4
    const/4 v4, 0x0

    .line 666
    .local v4, "leftOffset":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 667
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v11

    if-nez v11, :cond_5

    .line 668
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f090005

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 672
    :cond_5
    const/4 v11, 0x1

    aget v11, v6, v11

    add-int/2addr v11, v3

    iget v12, v2, Landroid/graphics/Rect;->bottom:I

    if-ge v11, v12, :cond_6

    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isPhoneView(Landroid/view/View;)Z

    move-result v11

    if-nez v11, :cond_6

    .line 674
    const/16 v11, 0x35

    const/4 v12, 0x0

    aget v12, v6, v12

    sub-int v12, v7, v12

    div-int/lit8 v13, v10, 0x2

    sub-int/2addr v12, v13

    sub-int/2addr v12, v8

    sub-int/2addr v12, v4

    const/4 v13, 0x1

    aget v13, v6, v13

    add-int/2addr v13, v3

    invoke-virtual {v0, v11, v12, v13}, Landroid/widget/Toast;->setGravity(III)V

    .line 681
    :goto_1
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 682
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 675
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isPhoneView(Landroid/view/View;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 676
    const/16 v11, 0x35

    const/4 v12, 0x0

    aget v12, v6, v12

    sub-int v12, v7, v12

    div-int/lit8 v13, v10, 0x2

    sub-int/2addr v12, v13

    const/4 v13, 0x1

    aget v13, v6, v13

    sub-int/2addr v13, v3

    invoke-virtual {v0, v11, v12, v13}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1

    .line 679
    :cond_7
    const/16 v11, 0x51

    const/4 v12, 0x0

    iget v13, v2, Landroid/graphics/Rect;->bottom:I

    sub-int v13, v5, v13

    add-int/2addr v13, v3

    invoke-virtual {v0, v11, v12, v13}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_1
.end method

.method public releaseView()V
    .locals 2

    .prologue
    .line 2432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    if-eqz v0, :cond_0

    .line 2433
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->releaseView()V

    .line 2436
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2437
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 2438
    return-void
.end method

.method public setAnchorView()V
    .locals 5

    .prologue
    .line 260
    const-string v3, "VideoTitleController"

    const-string v4, "setAnchorView"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->removeAllViews()V

    .line 263
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 265
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 266
    .local v2, "v":Landroid/view/View;
    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    .line 267
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 270
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 271
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 274
    :cond_0
    const v3, 0x7f0d01d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 295
    return-void
.end method

.method public setAudioShockWarningEnabled()V
    .locals 2

    .prologue
    .line 736
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v1, :cond_1

    .line 737
    const/4 v0, -0x1

    .line 739
    .local v0, "value":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioShockWarningEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v1, :cond_0

    .line 740
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {}, Landroid/media/AudioManager;->getEarProtectLimitIndex()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 742
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOverlapPointForDualColor(I)V

    .line 743
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->invalidate()V

    .line 745
    .end local v0    # "value":I
    :cond_1
    return-void
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 1740
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 1741
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setPressed(Z)V

    .line 1743
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1744
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1745
    :cond_1
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1653
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 1664
    :goto_0
    return-void

    .line 1656
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 1659
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1662
    :cond_1
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mButtonEnable:Z

    .line 1663
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 2446
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2447
    return-void
.end method

.method protected setInvisibleAllShareLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1362
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1363
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1364
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1365
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01ea

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1366
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1367
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1368
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d000b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1369
    return-void
.end method

.method public setInvisibleAllViews()V
    .locals 3

    .prologue
    .line 1324
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleNormalLayout()V

    .line 1325
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleAllShareLayout()V

    .line 1326
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateKDrmLayout(I)V

    .line 1328
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    .line 1334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 1335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1338
    :cond_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    .line 1339
    return-void
.end method

.method protected setInvisibleNormalLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1342
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01c1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1345
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01de

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1347
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01db

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1348
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0138

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1349
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d010d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d01e3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1353
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1354
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1355
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d012e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d00fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1357
    return-void
.end method

.method public setPadding()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const/16 v1, 0x53

    invoke-virtual {v0, v2, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 310
    :cond_0
    return-void
.end method

.method public setVisibleAllViews()V
    .locals 1

    .prologue
    .line 1372
    sget-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibleAllViews(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;)V

    .line 1373
    return-void
.end method

.method protected setVisibleAllViews(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;)V
    .locals 8
    .param p1, "layout"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    .prologue
    const v5, 0x7f0d01e6

    const/4 v7, 0x4

    const v6, 0x7f0d01c9

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1376
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v3, :cond_5

    move v0, v1

    .line 1378
    .local v0, "LSCREEN_VISIBILITY":I
    :goto_0
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v4, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne v3, v4, :cond_0

    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-eq p1, v3, :cond_1

    :cond_0
    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne p1, v3, :cond_7

    .line 1379
    :cond_1
    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    .line 1380
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleNormalLayout()V

    .line 1382
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01e4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1383
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01e7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1384
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01e8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1387
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->getFrom()I

    move-result v3

    const/16 v4, 0x73

    if-eq v3, v4, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFileUSB()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1388
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1393
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d01e9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1394
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d01ea

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1395
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d000a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1396
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d0009

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1397
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d000b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1437
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateAllShareBtn()V

    .line 1438
    return-void

    .end local v0    # "LSCREEN_VISIBILITY":I
    :cond_5
    move v0, v2

    .line 1376
    goto/16 :goto_0

    .line 1390
    .restart local v0    # "LSCREEN_VISIBILITY":I
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 1399
    :cond_7
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v5, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne v3, v5, :cond_8

    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NONE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-eq p1, v3, :cond_9

    :cond_8
    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne p1, v3, :cond_c

    :cond_9
    const/4 v3, 0x1

    :goto_3
    and-int/2addr v3, v4

    if-eqz v3, :cond_d

    .line 1400
    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    .line 1404
    :goto_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleAllShareLayout()V

    .line 1405
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01d7

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1406
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01e1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1407
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01e2

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1408
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d0138

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1409
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d010d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1411
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1412
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1419
    :goto_5
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WATCH_ON:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->isWatchONEnable()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1420
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d01de

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1423
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d01db

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1424
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v3, 0x7f0d01e3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1425
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v2, :cond_b

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v2, :cond_b

    .line 1426
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    .line 1429
    :cond_b
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    .line 1430
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateKDrmLayout(I)V

    .line 1432
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v1, :cond_4

    .line 1433
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d01e9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_c
    move v3, v1

    .line 1399
    goto/16 :goto_3

    .line 1402
    :cond_d
    sget-object v3, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    goto/16 :goto_4

    .line 1413
    :cond_e
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1414
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1416
    :cond_f
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5
.end method

.method public setVolumeSeekbarLevel()V
    .locals 2

    .prologue
    .line 2366
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2374
    :cond_0
    :goto_0
    return-void

    .line 2370
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 2371
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAudioShockWarningEnabled()V

    .line 2372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 6

    .prologue
    const v5, 0x7f0d01d7

    const/4 v4, 0x0

    .line 1233
    sget-boolean v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isTitleShowed()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1234
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateChangePlayerButton()V

    .line 1235
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibleAllViews()V

    .line 1236
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setPadding()V

    .line 1237
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateVolume()V

    .line 1239
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isTitleShowed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1240
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1243
    :cond_1
    const/4 v0, 0x0

    .line 1245
    .local v0, "dy":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1246
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getHeight()I

    move-result v3

    add-int v0, v2, v3

    .line 1251
    :goto_0
    const/4 v1, 0x0

    .line 1252
    .local v1, "translateOn":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    neg-int v2, v0

    int-to-float v2, v2

    invoke-direct {v1, v4, v4, v2, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1253
    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1254
    invoke-virtual {v1}, Landroid/view/animation/Animation;->startNow()V

    .line 1255
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isTitleShowed()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1260
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 1262
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mShowing:Z

    .line 1263
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateTitle()V

    .line 1265
    .end local v0    # "dy":I
    .end local v1    # "translateOn":Landroid/view/animation/Animation;
    :cond_3
    return-void

    .line 1248
    .restart local v0    # "dy":I
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0

    .line 1258
    .restart local v1    # "translateOn":Landroid/view/animation/Animation;
    :cond_5
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public showAllshareBtn(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1677
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 1678
    const-string v0, "VideoTitleController"

    const-string v1, "showAllshareBtn : mChangePlayerBtn is NOT init"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1716
    :cond_0
    :goto_0
    return-void

    .line 1682
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHANGE_PLAYER_SUPPORT_WFD:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isKnoxMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1683
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setEnableChangePlayerBtn(Z)V

    goto :goto_0

    .line 1687
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v0

    if-gtz v0, :cond_5

    .line 1688
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setEnableChangePlayerBtn(Z)V

    .line 1689
    const-string v0, "VideoTitleController"

    const-string v1, "showAllshareBtn : Browser is NOT init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1700
    :cond_5
    if-eqz p1, :cond_8

    .line 1701
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne v0, v1, :cond_7

    .line 1702
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-eqz v0, :cond_6

    .line 1703
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const v1, 0x7f02015a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1704
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 1705
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1707
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    .line 1709
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mLayoutMode:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    if-ne v0, v1, :cond_0

    .line 1710
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateChangePlayerButton()V

    .line 1711
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setEnableChangePlayerBtn(Z)V

    goto :goto_0

    .line 1714
    :cond_8
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setEnableChangePlayerBtn(Z)V

    goto :goto_0
.end method

.method public showAllshareTitleControl(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 1667
    const-string v0, "VideoTitleController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showAllshareTitleControl visible:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1668
    if-eqz p1, :cond_0

    .line 1669
    sget-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->ALL_SHARE:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibleAllViews(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;)V

    .line 1673
    :goto_0
    return-void

    .line 1671
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;->NORMAL:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibleAllViews(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$LayoutMode;)V

    goto :goto_0
.end method

.method public showPopupAllshareVolbar(Z)V
    .locals 8
    .param p1, "focused_state"    # Z

    .prologue
    .line 1603
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    if-eqz v5, :cond_3

    .line 1604
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1605
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    const v6, 0x1030002

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 1607
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    const v6, 0x7f0d0008

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1609
    .local v3, "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    iget v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1610
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    iget v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1611
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1612
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v5, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1614
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    new-instance v6, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$18;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$18;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1623
    const/16 v0, 0x35

    .line 1624
    .local v0, "gravity":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080231

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    add-int v2, v5, v6

    .line 1626
    .local v2, "margin_Y":I
    const/4 v1, 0x0

    .line 1628
    .local v1, "margin_Right":I
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1629
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080232

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v1, v5

    .line 1630
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0802dd

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v2, v5

    .line 1633
    :cond_0
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v5, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1634
    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v5, :cond_1

    .line 1635
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f080089

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    add-int/2addr v2, v5

    .line 1638
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v4

    .line 1639
    .local v4, "windowHeight":I
    iget v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    add-int/2addr v5, v2

    if-ge v4, v5, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTopLocationMultiwindow()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1640
    const/16 v0, 0x55

    .line 1641
    const/4 v2, 0x0

    .line 1647
    .end local v4    # "windowHeight":I
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupAllshareVolBar:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareVolPopupLayout:Landroid/view/View;

    invoke-virtual {v5, v6, v0, v1, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 1649
    .end local v0    # "gravity":I
    .end local v1    # "margin_Right":I
    .end local v2    # "margin_Y":I
    .end local v3    # "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_3
    return-void

    .line 1643
    .restart local v0    # "gravity":I
    .restart local v1    # "margin_Right":I
    .restart local v2    # "margin_Y":I
    .restart local v3    # "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1644
    const/16 v1, 0x76

    goto :goto_0
.end method

.method public showPopupMenu()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const v6, 0x800005

    const/4 v5, 0x0

    .line 752
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 804
    :cond_0
    :goto_0
    return-void

    .line 755
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v1

    if-nez v1, :cond_0

    .line 758
    const-string v1, "VideoTitleController"

    const-string v2, "showPopupMenu() E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 760
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 762
    .local v0, "activity":Landroid/app/Activity;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_3

    .line 763
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 764
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    .line 767
    :cond_2
    iput-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 770
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    if-nez v1, :cond_4

    .line 771
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_T:Z

    if-nez v1, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 772
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllShareMoreBtn:Landroid/widget/ImageButton;

    invoke-direct {v1, v2, v3, v6}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    .line 777
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$5;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 785
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$6;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    .line 797
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/Menu;->clear()V

    .line 798
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 799
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v1

    invoke-virtual {v0, v5, v7, v1}, Landroid/app/Activity;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    .line 801
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 802
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    goto :goto_0

    .line 774
    :cond_5
    new-instance v1, Landroid/widget/PopupMenu;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    const v4, 0x7f0d01d8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v1, v2, v3, v6}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupMenu:Landroid/widget/PopupMenu;

    goto :goto_1
.end method

.method public showPopupVolbar(Z)V
    .locals 13
    .param p1, "focused_state"    # Z

    .prologue
    const v12, 0x7f0d0206

    const v11, 0x7f0d0204

    const v10, 0x7f0d0203

    .line 1507
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    if-eqz v8, :cond_6

    .line 1508
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1509
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    const v9, 0x1030002

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    .line 1511
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1512
    .local v3, "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1513
    .local v4, "paramVolumeSeekbar":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1515
    .local v5, "paramVolumetext":Landroid/widget/RelativeLayout$LayoutParams;
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v8, :cond_7

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v8

    if-nez v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1516
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08022d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1517
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080230

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1518
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080227

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1519
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1520
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1521
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1531
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1532
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08022f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1533
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080226

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1534
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1535
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1538
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1539
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget v9, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1540
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    new-instance v9, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v9}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1541
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v8, p1}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1542
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    if-eqz v8, :cond_1

    .line 1543
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 1546
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    new-instance v9, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$17;

    invoke-direct {v9, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$17;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v8, v9}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1558
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeBtnPopup()V

    .line 1560
    const/16 v0, 0x35

    .line 1561
    .local v0, "gravity":I
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f080231

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    float-to-int v9, v9

    add-int v2, v8, v9

    .line 1563
    .local v2, "margin_Y":I
    const/4 v1, 0x0

    .line 1565
    .local v1, "margin_Right":I
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1566
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080232

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v1, v8

    .line 1567
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0802dd

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v2, v8

    .line 1570
    :cond_2
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v8, :cond_8

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1571
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v8, :cond_3

    .line 1572
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080089

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    add-int/2addr v2, v8

    .line 1575
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v7

    .line 1576
    .local v7, "windowHeight":I
    iget v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    add-int/2addr v8, v2

    if-ge v7, v8, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTopLocationMultiwindow()Z

    move-result v8

    if-nez v8, :cond_4

    .line 1577
    const/16 v0, 0x55

    .line 1578
    const/4 v2, 0x0

    .line 1585
    .end local v7    # "windowHeight":I
    :cond_4
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v8

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1586
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v8

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getRotation()I

    move-result v6

    .line 1588
    .local v6, "rotation":I
    if-nez v6, :cond_9

    .line 1589
    const/16 v0, 0x35

    .line 1597
    .end local v6    # "rotation":I
    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->dismissVolumePanel()V

    .line 1598
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v9, v0, v1, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 1600
    .end local v0    # "gravity":I
    .end local v1    # "margin_Right":I
    .end local v2    # "margin_Y":I
    .end local v3    # "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "paramVolumeSeekbar":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "paramVolumetext":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    return-void

    .line 1523
    .restart local v3    # "paramVolumePopup":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v4    # "paramVolumeSeekbar":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v5    # "paramVolumetext":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08022c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v5, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1524
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08022e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1525
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080225

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 1526
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1527
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1528
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolPopupLayout:Landroid/view/View;

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 1580
    .restart local v0    # "gravity":I
    .restart local v1    # "margin_Right":I
    .restart local v2    # "margin_Y":I
    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1581
    const/16 v1, 0x76

    goto/16 :goto_1

    .line 1590
    .restart local v6    # "rotation":I
    :cond_9
    const/4 v8, 0x3

    if-ne v6, v8, :cond_a

    .line 1591
    const/16 v0, 0x53

    goto :goto_2

    .line 1593
    :cond_a
    const/16 v0, 0x33

    goto :goto_2
.end method

.method public showShareViaMenu(Landroid/content/Intent;)V
    .locals 3
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 1013
    new-instance v0, Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    .line 1014
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mMoreBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setHeight(I)V

    .line 1019
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v2, 0x7f030011

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSelectShareMenuAdapter:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    .line 1020
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSelectShareMenuAdapter:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->setAppList(Landroid/content/Intent;)V

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSelectShareMenuAdapter:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->setListPopupWindow(Landroid/widget/ListPopupWindow;)V

    .line 1022
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSelectShareMenuAdapter:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 1027
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$11;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$11;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1037
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$12;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$12;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1044
    return-void
.end method

.method public startChangePlayer()V
    .locals 3

    .prologue
    .line 2608
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2609
    return-void
.end method

.method public updateAllShareBtn()V
    .locals 1

    .prologue
    .line 1219
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkSupportChangePlayer()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showAllshareBtn(Z)V

    .line 1220
    return-void

    .line 1219
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateCaptureBtn()V
    .locals 2

    .prologue
    .line 1066
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 1078
    :goto_0
    return-void

    .line 1069
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_1

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->isCaptureModeOn()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->canCaptureVideoFrame()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1071
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1077
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateOneFrameForwardBackwardBtn()V

    goto :goto_0

    .line 1073
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mCaptureBtn:Landroid/widget/ImageButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateChangePlayerButton()V
    .locals 2

    .prologue
    const v1, 0x7f02015a

    .line 2422
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtnOnDLNA:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 2429
    :goto_0
    return-void

    .line 2424
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->isWfdConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 2427
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mChangePlayerBtn:Landroid/widget/ImageButton;

    const v1, 0x7f02015c

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method public updateKDrmLayout(I)V
    .locals 0
    .param p1, "visible"    # I

    .prologue
    .line 1063
    return-void
.end method

.method public updateOneFrameForwardBackwardBtn()V
    .locals 2

    .prologue
    .line 1081
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_1

    .line 1086
    :cond_0
    :goto_0
    return-void

    .line 1084
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateOneFrameForwardBackwardBtn(Landroid/view/View;)V

    goto :goto_0
.end method

.method public updatePopupPlayerBtn()V
    .locals 2

    .prologue
    .line 2454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-nez v0, :cond_1

    .line 2458
    :cond_0
    :goto_0
    return-void

    .line 2457
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updatePopupPlayerBtn(Landroid/view/View;)V

    goto :goto_0
.end method

.method public updateTitle(Ljava/lang/String;)V
    .locals 4
    .param p1, "titleText"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 2377
    const-string v0, "VideoTitleController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTitle. titleText : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2379
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0145

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 2381
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2382
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2383
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mTitleText:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2385
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2386
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 2387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mAllshareTitleText:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 2388
    return-void
.end method

.method public updateTitleControllerBtns()V
    .locals 1

    .prologue
    .line 2485
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    if-eqz v0, :cond_0

    .line 2486
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mControllerUtils:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->updateAutoRotationBtn()V

    .line 2488
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2489
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateEasyModeBtn()V

    .line 2491
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateSoundAliveBtn()V

    .line 2492
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updatePopupPlayerBtn()V

    .line 2493
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateAllShareBtn()V

    .line 2494
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updatePersonalPageImage()V

    .line 2495
    return-void
.end method

.method public updateVolume()V
    .locals 0

    .prologue
    .line 1748
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeBtnPopup()V

    .line 1749
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 1750
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    .line 1751
    return-void
.end method
