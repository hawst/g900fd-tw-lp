.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 2804
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 2806
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2825
    :cond_0
    :goto_0
    return v1

    .line 2811
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$25;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method
