.class public Lcom/sec/android/app/videoplayer/service/Presentationservice;
.super Landroid/app/Service;
.source "Presentationservice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;,
        Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;
    }
.end annotation


# static fields
.field public static final ACTION_NAME:Ljava/lang/String; = "com.samsung.action.SHOW_PRESENTATION"

.field private static final DUMMY_NOTIFICATION_ID:I = 0x1

.field public static final KEY_PREFERENCES:Ljava/lang/String; = "com.sec.android.app.videoplayer.presentation_preferences"

.field private static final MOTION_MOVE_GESTURE_ADJUSTMENT:I = 0x3

.field private static final MOTION_START_GESTURE_THRESHOLD:I = 0x2

.field public static final PREF_IS_INITIAL_PLAYER_ACCESS:Ljava/lang/String; = "initial_player_access"

.field private static final START_GESTURE:I = 0x1

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAm:Landroid/app/IActivityManager;

.field private mAttached:Z

.field private final mBinder:Lcom/sec/android/app/videoplayer/service/IPresentationService$Stub;

.field private mContext:Landroid/content/Context;

.field private mDisplayContext:Landroid/content/Context;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private final mDisplays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/Display;",
            ">;"
        }
    .end annotation
.end field

.field private mDownYPos:I

.field private mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

.field private mForegroundToken:Landroid/os/IBinder;

.field private mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

.field private mGestureThreshold:I

.field private mHandler:Landroid/os/Handler;

.field private mIsVideoGestureStart:Z

.field private mMainview:Landroid/widget/RelativeLayout;

.field private mNotificationDummy:Landroid/app/Notification;

.field private mPresentationDialog:Landroid/app/Presentation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mTextView:Landroid/widget/TextView;

.field private mTutorialView:Landroid/view/View;

.field private mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

.field private mVideoGestureMode:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

.field private mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

.field private mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

.field protected mWindowManager:Landroid/view/WindowManager;

.field private mXTouchPos:I

.field private mYTouchPos:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "PresentationService"

    sput-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 68
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    .line 70
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mIsVideoGestureStart:Z

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 72
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureThreshold:I

    .line 73
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mXTouchPos:I

    .line 74
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mYTouchPos:I

    .line 75
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDownYPos:I

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mContext:Landroid/content/Context;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .line 168
    new-instance v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice$1;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 725
    new-instance v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice$3;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mHandler:Landroid/os/Handler;

    .line 740
    new-instance v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice$4;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mBinder:Lcom/sec/android/app/videoplayer/service/IPresentationService$Stub;

    return-void
.end method

.method private Presentation_ChangeSurfaceAddView()V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 553
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 556
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 560
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 561
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setSubtitleView()V

    .line 562
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 564
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout()V

    .line 566
    :cond_2
    return-void
.end method

.method private Presentation_ChangeSurfaceRemoveView()V
    .locals 2

    .prologue
    .line 570
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_2

    .line 571
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 574
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 577
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_2

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 579
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 580
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setSubtitleView()V

    .line 583
    :cond_2
    return-void
.end method

.method private Presentation_DisableTvOut()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 527
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Presentation_DisableTvOut() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 528
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    if-nez v1, :cond_0

    .line 529
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    .line 530
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 531
    .local v0, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 532
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    const v2, 0x7f0a005d

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    const/high16 v2, 0x41b00000    # 22.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    .line 534
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 535
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 536
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 537
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 539
    .end local v0    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void
.end method

.method private Presentation_Dismiss()V
    .locals 4

    .prologue
    .line 465
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Presentation_Dismiss() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v1}, Landroid/app/Presentation;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 471
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 472
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private Presentation_RemoveDisableTvOut()V
    .locals 3

    .prologue
    .line 543
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Presentation_RemoveDisableTvOut() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTextView:Landroid/widget/TextView;

    .line 548
    :cond_0
    return-void
.end method

.method private Presentation_Show()V
    .locals 4

    .prologue
    .line 452
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Presentation_Show() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v1}, Landroid/app/Presentation;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    .line 458
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_1
    move-exception v0

    .line 459
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private Presentation_dismissTutorial()V
    .locals 3

    .prologue
    .line 516
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Presentation_dismissTutorial() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 519
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v1, "Presentation_dismissTutorial!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    .line 522
    :cond_0
    return-void
.end method

.method private Presentation_isShowTutorial()Z
    .locals 3

    .prologue
    .line 507
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Presentation_showTutorial() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 508
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    if-ne v0, v1, :cond_0

    .line 509
    const/4 v0, 0x1

    .line 511
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Presentation_showTutorial()V
    .locals 3

    .prologue
    .line 496
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Presentation_showTutorial() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    const v1, 0x7f03000a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mTutorialView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 501
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v1, "Presentation_showTutorial!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_Dismiss()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->isShowing_Presentation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_DisableTvOut()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_RemoveDisableTvOut()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_ChangeSurfaceAddView()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_ChangeSurfaceRemoveView()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mXTouchPos:I

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mYTouchPos:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/service/Presentationservice;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDownYPos:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/service/Presentationservice;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDownYPos:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/service/Presentationservice;)Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/videoplayer/service/Presentationservice;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mIsVideoGestureStart:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_Show()V

    return-void
.end method

.method private createLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 203
    const/16 v2, 0x7d2

    .line 205
    .local v2, "windowType":I
    :try_start_0
    const-class v3, Landroid/view/WindowManager$LayoutParams;

    const-string v4, "TYPE_FAKE_APPLICATION"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 206
    .local v0, "field":Ljava/lang/reflect/Field;
    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {v0, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 218
    .end local v0    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :goto_0
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x140

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    .line 223
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 224
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 225
    const/16 v3, 0x50

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 226
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 228
    return-object v1

    .line 212
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    :catch_0
    move-exception v3

    goto :goto_0

    .line 211
    :catch_1
    move-exception v3

    goto :goto_0

    .line 210
    :catch_2
    move-exception v3

    goto :goto_0

    .line 209
    :catch_3
    move-exception v3

    goto :goto_0
.end method

.method private isShowing_Presentation()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 478
    sget-object v2, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isShowing_Presentation() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-eqz v2, :cond_0

    .line 481
    sget-object v2, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isShowing : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v4}, Landroid/app/Presentation;->isShowing()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v2}, Landroid/app/Presentation;->isShowing()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 491
    :cond_0
    :goto_0
    return v1

    .line 485
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 487
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 488
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private register()V
    .locals 3

    .prologue
    .line 157
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v2, "register() - BroadcastReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 159
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    return-void
.end method

.method private unregister()V
    .locals 2

    .prologue
    .line 164
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v1, "unregister() - BroadcastReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 166
    return-void
.end method


# virtual methods
.method public attachVideoGestureView()V
    .locals 3

    .prologue
    .line 703
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-eqz v0, :cond_0

    .line 704
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v2}, Landroid/app/Presentation;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;-><init>(Landroid/content/Context;Landroid/view/Window;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->addViewTo(Landroid/view/View;)V

    .line 707
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 413
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dismiss() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    if-nez v1, :cond_0

    .line 448
    :goto_0
    return-void

    .line 418
    :cond_0
    sput-boolean v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    .line 419
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_2

    .line 420
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_4

    .line 421
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    .line 422
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 431
    :cond_1
    :goto_1
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v1, :cond_2

    .line 432
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 433
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 434
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setSubtitleView()V

    .line 438
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->Presentation_RemoveDisableTvOut()V

    .line 439
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    .line 442
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-eqz v1, :cond_3

    .line 443
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v1}, Landroid/app/Presentation;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 424
    :cond_4
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_5

    .line 425
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 427
    :cond_5
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_1

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    goto :goto_1

    .line 444
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2
.end method

.method public finishUpVideoGesture()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 710
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 711
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 713
    :cond_0
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mIsVideoGestureStart:Z

    .line 715
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 716
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->hide()V

    .line 717
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    .line 722
    :goto_0
    return v0

    .line 721
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    move v0, v1

    .line 722
    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mBinder:Lcom/sec/android/app/videoplayer/service/IPresentationService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 106
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 107
    sget-object v2, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v3, "onCreate() "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mContext:Landroid/content/Context;

    .line 110
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mWindowManager:Landroid/view/WindowManager;

    .line 112
    const-string v2, "display"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/DisplayManager;

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const-string v3, "android.hardware.display.category.PRESENTATION"

    invoke-virtual {v2, v3}, Landroid/hardware/display/DisplayManager;->getDisplays(Ljava/lang/String;)[Landroid/view/Display;

    move-result-object v0

    .line 114
    .local v0, "displays":[Landroid/view/Display;
    sget-object v2, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate() : displays size="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    aget-object v3, v0, v1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 116
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-le v2, v3, :cond_1

    .line 121
    new-instance v2, Landroid/os/Binder;

    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mForegroundToken:Landroid/os/IBinder;

    .line 122
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAm:Landroid/app/IActivityManager;

    .line 124
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAm:Landroid/app/IActivityManager;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v4

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 134
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 135
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->register()V

    .line 136
    return-void

    .line 129
    :cond_1
    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mNotificationDummy:Landroid/app/Notification;

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mNotificationDummy:Landroid/app/Notification;

    invoke-virtual {p0, v5, v2}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->startForeground(ILandroid/app/Notification;)V

    goto :goto_1

    .line 125
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 185
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 186
    sget-object v0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    const-string v1, "onDestroy() : "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->dismiss()V

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->unregister()V

    .line 190
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAm:Landroid/app/IActivityManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_0
    return-void

    .line 198
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->stopForeground(Z)V

    goto :goto_0

    .line 194
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v4, 0x2

    .line 139
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStartCommand() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    .line 142
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStartCommand() : duplicated start command! just ignore. (mAttached="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :goto_0
    return v4

    .line 146
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 148
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->show(Ljava/lang/String;)V

    .line 150
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    goto :goto_0
.end method

.method public show(Ljava/lang/String;)V
    .locals 9
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 232
    sget-object v4, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "show() E. : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    if-eqz v4, :cond_1

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mAttached:Z

    .line 239
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_2

    .line 240
    sget-object v4, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "show() : Device Size is :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 244
    :cond_2
    const/4 v2, 0x0

    .line 246
    .local v2, "index":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v7, :cond_4

    .line 247
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 248
    sget-object v5, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "show() : Discovered displays["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] ="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    invoke-virtual {v4}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    invoke-virtual {v4}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "HDMI"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 250
    sget-object v5, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "show() : Selected displays["

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "] ="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    invoke-virtual {v4}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    move v2, v1

    .line 247
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 255
    .end local v1    # "i":I
    :cond_4
    const/4 v2, 0x0

    .line 258
    :cond_5
    sput-boolean v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    .line 259
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->createDisplayContext(Landroid/view/Display;)Landroid/content/Context;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    const v5, 0x7f030024

    invoke-static {v4, v5, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    .line 261
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->createLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    .line 263
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    if-nez v4, :cond_6

    .line 264
    new-instance v5, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/Display;

    invoke-direct {v5, p0, v6, v4}, Lcom/sec/android/app/videoplayer/service/Presentationservice$2;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;Landroid/content/Context;Landroid/view/Display;)V

    iput-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    .line 359
    :cond_6
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v4}, Landroid/app/Presentation;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 360
    .local v3, "window":Landroid/view/Window;
    invoke-virtual {v3, v7}, Landroid/view/Window;->requestFeature(I)Z

    .line 361
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mWindowAttributes:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v3, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 362
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    const/16 v5, 0x308

    const-string v6, "SAMSUNG_FLAG_NO_RESIZE_ANIMATION_INCLUDE_CHILD"

    invoke-virtual {v4, v3, v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(Landroid/view/Window;ILjava/lang/String;)V

    .line 363
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/app/Presentation;->setContentView(Landroid/view/View;)V

    .line 365
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v4, :cond_a

    .line 366
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoSurfaceLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 375
    :cond_7
    :goto_2
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v4, :cond_8

    .line 376
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 377
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setSubtitleView()V

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 380
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout()V

    .line 383
    :cond_8
    new-instance v4, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    new-instance v6, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;

    invoke-direct {v6, p0, v8}, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;Lcom/sec/android/app/videoplayer/service/Presentationservice$1;)V

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    .line 384
    new-instance v4, Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 386
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    if-eqz v4, :cond_9

    .line 387
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->addViewTo(Landroid/view/View;)V

    .line 389
    :cond_9
    const/high16 v4, 0x40000000    # 2.0f

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mGestureThreshold:I

    .line 392
    new-instance v4, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplayContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .line 393
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 394
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setFocusable(Z)V

    .line 395
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setFocusableInTouchMode(Z)V

    .line 396
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoExtScreenController:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->bringToFront()V

    .line 400
    :try_start_0
    sget-object v4, Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "show() : [displays info] :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mDisplays:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    if-eqz p1, :cond_0

    const-string v4, "com.samsung.action.SHOW_PRESENTATION"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mPresentationDialog:Landroid/app/Presentation;

    invoke-virtual {v4}, Landroid/app/Presentation;->show()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 405
    :catch_0
    move-exception v0

    .line 406
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto/16 :goto_0

    .line 368
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_a
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v4, :cond_b

    .line 369
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 371
    :cond_b
    sget-object v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v4, :cond_7

    .line 372
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mMainview:Landroid/widget/RelativeLayout;

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 407
    :catch_1
    move-exception v0

    .line 408
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public startShowGesture()V
    .locals 2

    .prologue
    .line 692
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-nez v0, :cond_0

    .line 694
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->attachVideoGestureView()V

    .line 696
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v0, :cond_1

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureMode;

    if-ne v0, v1, :cond_1

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->showVolume()V

    .line 700
    :cond_1
    return-void
.end method
