.class interface abstract Lcom/sec/android/app/videoplayer/service/ITouchPlayer;
.super Ljava/lang/Object;
.source "ITouchPlayer.java"


# virtual methods
.method public abstract addOutbandSubTitle(Ljava/lang/String;)V
.end method

.method public abstract addOutbandSubTitle(Ljava/lang/String;Z)V
.end method

.method public abstract deselectSubtitleTrack()V
.end method

.method public abstract getCurrentFrame()Landroid/graphics/Bitmap;
.end method

.method public abstract getFPS()I
.end method

.method public abstract initSelectSubtitleTrack()V
.end method

.method public abstract initSubtitle(Ljava/lang/String;Z)V
.end method

.method public abstract isSecVideo()Z
.end method

.method public abstract oneFrameSeek(I)V
.end method

.method public abstract reset()V
.end method

.method public abstract resetPlayer()V
.end method

.method public abstract resetSubtitle()V
.end method

.method public abstract selectSubtitleTrack(I)V
.end method

.method public abstract setAdaptSound(ZZ)V
.end method

.method public abstract setAudioTrack(I)V
.end method

.method public abstract setDataSource(Landroid/net/Uri;)V
.end method

.method public abstract setDataSourceAsync(Landroid/net/Uri;)V
.end method

.method public abstract setHandler(Landroid/os/Handler;)V
.end method

.method public abstract setInbandSubtitle()V
.end method

.method public abstract setPlaySpeed(F)V
.end method

.method public abstract setSoundAliveMode()V
.end method

.method public abstract setSoundAliveMode(I)V
.end method

.method public abstract setSubtitleSyncTime(I)V
.end method

.method public abstract setVideoCrop(IIII)V
.end method

.method public abstract setVolume(F)V
.end method
