.class Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SCoverPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 2
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 172
    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 173
    const-string v0, "SCoverPlayer"

    const-string v1, "SCover : State Open"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->changeLayoutOpenState()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$600(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    .line 179
    :goto_0
    return-void

    .line 176
    :cond_0
    const-string v0, "SCoverPlayer"

    const-string v1, "SCover : State Close"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->ExitApp()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$500(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    goto :goto_0
.end method
