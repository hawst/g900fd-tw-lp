.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "SubtitleColorPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ColorsGridAdapter"
.end annotation


# instance fields
.field public colors:[Ljava/lang/Integer;

.field public contentDescriptions:[I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Landroid/content/Context;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0xa

    .line 616
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 596
    new-array v0, v3, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, 0x7f02017e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f02017f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f020180

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f020181

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f020182

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const v2, 0x7f020183

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f020184

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f020185

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const v2, 0x7f020186

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const v2, 0x7f02017d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    .line 609
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->contentDescriptions:[I

    .line 617
    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;
    invoke-static {p1, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Landroid/content/Context;)Landroid/content/Context;

    .line 618
    return-void

    .line 609
    :array_0
    .array-data 4
        0x7f0a01c8
        0x7f0a0021
        0x7f0a0100
        0x7f0a007c
        0x7f0a0022
        0x7f0a01ca
        0x7f0a009f
        0x7f0a0043
        0x7f0a0042
        0x7f0a00d7
    .end array-data
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 627
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 632
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "viewGroup"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 636
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 637
    .local v3, "vi":Landroid/view/LayoutInflater;
    const v4, 0x7f030031

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 638
    const v4, 0x7f0d019d

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 639
    .local v1, "not_selected":Landroid/view/View;
    const v4, 0x7f0d01a1

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 642
    .local v2, "selected":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v4

    if-eq p1, v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v4

    const/4 v5, -0x1

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    if-ne p1, v4, :cond_1

    .line 644
    :cond_0
    const v4, 0x7f0d01a2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 645
    .local v0, "color":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 646
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 653
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->contentDescriptions:[I

    aget v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 655
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x2

    if-eq p1, v4, :cond_2

    .line 656
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 671
    :goto_1
    return-object p2

    .line 648
    .end local v0    # "color":Landroid/view/View;
    :cond_1
    const v4, 0x7f0d019e

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 649
    .restart local v0    # "color":Landroid/view/View;
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 650
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 658
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 659
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 660
    const v4, 0x7f0d01a3

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 664
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 665
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setPointerVisible(Z)V

    goto :goto_1

    .line 662
    :cond_3
    const v4, 0x7f0d019f

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 667
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->colors:[Ljava/lang/Integer;

    aget-object v4, v4, p1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 668
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setPointerVisible(Z)V

    goto :goto_1
.end method
