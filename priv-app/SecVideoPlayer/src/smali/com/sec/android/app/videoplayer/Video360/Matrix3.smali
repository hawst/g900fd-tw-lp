.class public Lcom/sec/android/app/videoplayer/Video360/Matrix3;
.super Ljava/lang/Object;
.source "Matrix3.java"


# instance fields
.field public m:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 10
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->identity()V

    .line 11
    return-void
.end method

.method public constructor <init>(FFFFFFFFF)V
    .locals 2
    .param p1, "m0"    # F
    .param p2, "m1"    # F
    .param p3, "m2"    # F
    .param p4, "m3"    # F
    .param p5, "m4"    # F
    .param p6, "m5"    # F
    .param p7, "m6"    # F
    .param p8, "m7"    # F
    .param p9, "m8"    # F

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 17
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x4

    aput p5, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x5

    aput p6, v0, v1

    .line 19
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x6

    aput p7, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x7

    aput p8, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v1, 0x8

    aput p9, v0, v1

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Matrix3;)V
    .locals 1
    .param p1, "mat"    # Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 77
    iget-object v0, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->set([F)V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Quaternion;)V
    .locals 22
    .param p1, "q"    # Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    .prologue
    .line 23
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v20, 0x9

    move/from16 v0, v20

    new-array v0, v0, [F

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 24
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v2, v20, v21

    .line 25
    .local v2, "c1":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v3, v20, v21

    .line 26
    .local v3, "c2":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v4, v20, v21

    .line 27
    .local v4, "c3":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v5, v20, v21

    .line 28
    .local v5, "c4":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v6, v20, v21

    .line 29
    .local v6, "c5":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mX:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 30
    .local v7, "c6":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v8, v20, v21

    .line 31
    .local v8, "c7":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mY:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 32
    .local v9, "c8":F
    const/high16 v20, 0x40000000    # 2.0f

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mZ:F

    move/from16 v21, v0

    mul-float v20, v20, v21

    move-object/from16 v0, p1

    iget v0, v0, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mW:F

    move/from16 v21, v0

    mul-float v10, v20, v21

    .line 34
    .local v10, "c9":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v3

    sub-float v11, v20, v4

    .line 35
    .local v11, "m00":F
    sub-float v12, v5, v10

    .line 36
    .local v12, "m01":F
    add-float v13, v6, v9

    .line 38
    .local v13, "m02":F
    add-float v14, v5, v10

    .line 39
    .local v14, "m10":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v4

    sub-float v15, v20, v2

    .line 40
    .local v15, "m11":F
    sub-float v16, v8, v7

    .line 42
    .local v16, "m12":F
    sub-float v17, v6, v9

    .line 43
    .local v17, "m20":F
    add-float v18, v8, v7

    .line 44
    .local v18, "m21":F
    const/high16 v20, 0x3f800000    # 1.0f

    sub-float v20, v20, v3

    sub-float v19, v20, v2

    .line 46
    .local v19, "m22":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput v11, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aput v12, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x2

    aput v13, v20, v21

    .line 47
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x3

    aput v14, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x4

    aput v15, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x5

    aput v16, v20, v21

    .line 48
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x6

    aput v17, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x7

    aput v18, v20, v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    move-object/from16 v20, v0

    const/16 v21, 0x8

    aput v19, v20, v21

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Vector3;D)V
    .locals 10
    .param p1, "axis"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .param p2, "radians"    # D

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v6, 0x9

    new-array v6, v6, [F

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 53
    invoke-static {p2, p3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v2, v6

    .line 54
    .local v2, "sA":F
    invoke-static {p2, p3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 56
    .local v0, "cA":F
    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v1, v6, v0

    .line 57
    .local v1, "oneMinusCos":F
    iget v6, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float v3, v6, v2

    .line 58
    .local v3, "sinX":F
    iget v6, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float v4, v6, v2

    .line 59
    .local v4, "sinY":F
    iget v6, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float v5, v6, v2

    .line 61
    .local v5, "sinZ":F
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x0

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v0

    aput v8, v6, v7

    .line 62
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x1

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v5

    aput v8, v6, v7

    .line 63
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x2

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    sub-float/2addr v8, v4

    aput v8, v6, v7

    .line 64
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x3

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    sub-float/2addr v8, v5

    aput v8, v6, v7

    .line 65
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x4

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v0

    aput v8, v6, v7

    .line 66
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x5

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v3

    aput v8, v6, v7

    .line 67
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x6

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v4

    aput v8, v6, v7

    .line 68
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x7

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    sub-float/2addr v8, v3

    aput v8, v6, v7

    .line 69
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v7, 0x8

    iget v8, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget v9, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    add-float/2addr v8, v0

    aput v8, v6, v7

    .line 70
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "mat"    # [F

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->set([F)V

    .line 74
    return-void
.end method


# virtual methods
.method public asString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x3

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x6

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "s":Ljava/lang/String;
    return-object v0
.end method

.method public getDeterminant()F
    .locals 10

    .prologue
    const/4 v9, 0x7

    const/4 v8, 0x6

    const/4 v4, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    .line 165
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v1, v1, v7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v2, v2, v9

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v4

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v2, v2, v4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v8

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v6

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x2

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v2, v2, v6

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v9

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v8

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v4, v4, v7

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public identity()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x0

    aput v3, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x1

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x3

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x4

    aput v3, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x5

    aput v2, v0, v1

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x6

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x7

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v1, 0x8

    aput v3, v0, v1

    .line 130
    return-void
.end method

.method public invert()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->getDeterminant()F

    move-result v0

    .line 144
    .local v0, "d":F
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-nez v2, :cond_0

    .line 161
    :goto_0
    return-void

    .line 147
    :cond_0
    new-instance v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;-><init>(Lcom/sec/android/app/videoplayer/Video360/Matrix3;)V

    .line 149
    .local v1, "tmp":Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->transpose()V

    .line 150
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v0, v2, v0

    .line 152
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v11

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    aput v3, v2, v7

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v10

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    neg-float v3, v3

    aput v3, v2, v8

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v10

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v11

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    aput v3, v2, v9

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v8

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v9

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    neg-float v3, v3

    aput v3, v2, v10

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v3, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v3, v3, v7

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v5, 0x8

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v9

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v3, v0

    aput v3, v2, v11

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x5

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v4, v4, v7

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x6

    aget v5, v5, v6

    iget-object v6, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v6, v6, v8

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v0

    neg-float v4, v4

    aput v4, v2, v3

    .line 158
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x6

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v4, v4, v8

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v11

    iget-object v6, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v6, v6, v9

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v0

    aput v4, v2, v3

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x7

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v4, v4, v7

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v10

    iget-object v6, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v6, v6, v9

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v0

    neg-float v4, v4

    aput v4, v2, v3

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v3, 0x8

    iget-object v4, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v4, v4, v7

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v11

    mul-float/2addr v4, v5

    iget-object v5, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v5, v5, v10

    iget-object v6, v1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v6, v6, v8

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    mul-float/2addr v4, v0

    aput v4, v2, v3

    goto/16 :goto_0
.end method

.method public multiply(Lcom/sec/android/app/videoplayer/Video360/Matrix3;)Lcom/sec/android/app/videoplayer/Video360/Matrix3;
    .locals 13
    .param p1, "mat"    # Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    .prologue
    .line 100
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x4

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    iget-object v4, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    iget-object v5, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    iget-object v5, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x3

    aget v4, v4, v5

    iget-object v5, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x0

    aget v5, v5, v6

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x4

    aget v5, v5, v6

    iget-object v6, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    iget-object v6, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x6

    aget v6, v6, v7

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x3

    aget v5, v5, v6

    iget-object v6, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x1

    aget v6, v6, v7

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x4

    aget v6, v6, v7

    iget-object v7, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    iget-object v7, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x7

    aget v7, v7, v8

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    iget-object v7, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    mul-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    iget-object v8, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x5

    aget v7, v7, v8

    iget-object v8, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v9, 0x8

    aget v8, v8, v9

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v8, 0x6

    aget v7, v7, v8

    iget-object v8, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v9, 0x0

    aget v8, v8, v9

    mul-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v9, 0x7

    aget v8, v8, v9

    iget-object v9, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v10, 0x3

    aget v9, v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v9, 0x8

    aget v8, v8, v9

    iget-object v9, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v9, 0x6

    aget v8, v8, v9

    iget-object v9, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v10, 0x1

    aget v9, v9, v10

    mul-float/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v10, 0x7

    aget v9, v9, v10

    iget-object v10, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v11, 0x4

    aget v10, v10, v11

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v10, 0x8

    aget v9, v9, v10

    iget-object v10, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    iget-object v10, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v11, 0x2

    aget v10, v10, v11

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    iget-object v11, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v12, 0x5

    aget v11, v11, v12

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v11, 0x8

    aget v10, v10, v11

    iget-object v11, p1, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/app/videoplayer/Video360/Matrix3;-><init>(FFFFFFFFF)V

    return-object v0
.end method

.method public multiply(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 7
    .param p1, "vec"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 113
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x4

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x7

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x5

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public multiply2(Lcom/sec/android/app/videoplayer/Video360/Vector3;)Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 7
    .param p1, "vec"    # Lcom/sec/android/app/videoplayer/Video360/Vector3;

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget v1, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x4

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x5

    aget v4, v4, v5

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->x:F

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v5, 0x6

    aget v4, v4, v5

    mul-float/2addr v3, v4

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->y:F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v6, 0x7

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Lcom/sec/android/app/videoplayer/Video360/Vector3;->z:F

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v6, 0x8

    aget v5, v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method set([F)V
    .locals 7
    .param p1, "mat"    # [F

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 81
    array-length v0, p1

    const/16 v1, 0x9

    if-ne v0, v1, :cond_1

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    array-length v1, v1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    array-length v0, p1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v1, p1, v2

    aput v1, v0, v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v1, p1, v3

    aput v1, v0, v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v1, p1, v4

    aput v1, v0, v4

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x3

    aget v2, p1, v5

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v1, p1, v6

    aput v1, v0, v5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x6

    aget v1, p1, v1

    aput v1, v0, v6

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x6

    const/16 v2, 0x8

    aget v2, p1, v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v1, 0x7

    const/16 v2, 0x9

    aget v2, p1, v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v1, 0x8

    const/16 v2, 0xa

    aget v2, p1, v2

    aput v2, v0, v1

    goto :goto_0
.end method

.method public transpose()V
    .locals 8

    .prologue
    const/4 v7, 0x6

    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 135
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v0, v1, v3

    .local v0, "t":F
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v2, v2, v5

    aput v2, v1, v3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aput v0, v1, v5

    .line 136
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v0, v1, v4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v2, v2, v7

    aput v2, v1, v4

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aput v0, v1, v7

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    aget v0, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    aput v2, v1, v6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x7

    aput v0, v1, v2

    .line 138
    return-void
.end method

.method public xAxis()Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 5

    .prologue
    .line 172
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x1

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public yAxis()Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 5

    .prologue
    .line 177
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x3

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x4

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v4, 0x5

    aget v3, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public zAxis()Lcom/sec/android/app/videoplayer/Video360/Vector3;
    .locals 5

    .prologue
    .line 182
    new-instance v0, Lcom/sec/android/app/videoplayer/Video360/Vector3;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/Video360/Matrix3;->m:[F

    const/16 v4, 0x8

    aget v3, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/Video360/Vector3;-><init>(FFF)V

    return-object v0
.end method
