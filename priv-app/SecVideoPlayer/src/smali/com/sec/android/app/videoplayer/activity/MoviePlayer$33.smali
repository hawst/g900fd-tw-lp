.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 5042
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0xce

    .line 5044
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 5046
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorType:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 5076
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "any error other than lic acq fail in CreateErrorDialog"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5077
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->handleErrorPopup()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 5081
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 5082
    return-void

    .line 5048
    :pswitch_0
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "VIDEO_DRM_LICENSE_ACQUISITION_FAILED."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5049
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VIDEO_DRM_LICENSE_ACQUISITION_FAILED. url =: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5051
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 5052
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5053
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorUri:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 5055
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5057
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->startActivity(Landroid/content/Intent;)V

    .line 5058
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 5059
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xce

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 5060
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xce

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5071
    .end local v0    # "browserIntent":Landroid/content/Intent;
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 5061
    .restart local v0    # "browserIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 5062
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "could not find a suitable activity for launching error url"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 5065
    .end local v1    # "ex":Landroid/content/ActivityNotFoundException;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 5066
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 5069
    .end local v0    # "browserIntent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$33;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1

    .line 5046
    nop

    :pswitch_data_0
    .packed-switch 0x6d
        :pswitch_0
    .end packed-switch
.end method
