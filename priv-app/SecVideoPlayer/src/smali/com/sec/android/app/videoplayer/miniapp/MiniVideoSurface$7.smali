.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnTimedTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimedText(Landroid/media/MediaPlayer;Landroid/media/TimedText;)V
    .locals 9
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "text"    # Landroid/media/TimedText;

    .prologue
    .line 571
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mini onTimedText start"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    if-eqz v6, :cond_4

    .line 574
    if-eqz p2, :cond_6

    .line 576
    const/16 v6, 0x11

    :try_start_0
    invoke-virtual {p2, v6}, Landroid/media/TimedText;->getObject(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 577
    .local v3, "textIndex":I
    const/4 v6, 0x7

    invoke-virtual {p2, v6}, Landroid/media/TimedText;->getObject(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 578
    .local v4, "textStartTimeMS":I
    invoke-virtual {p2}, Landroid/media/TimedText;->getText()Ljava/lang/String;

    move-result-object v5

    .line 580
    .local v5, "textString":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onTimedText(). textIndex : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " textStartTimeMS : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " text = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    if-nez v5, :cond_0

    .line 583
    const-string v5, ""

    .line 586
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getIsMultiSubtitle()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getSelectedSubtitleIndex()[I

    move-result-object v6

    if-eqz v6, :cond_5

    .line 587
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    aput-object v7, v6, v3

    .line 588
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 590
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    const-string v7, ""

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Ljava/lang/String;)Ljava/lang/String;

    .line 591
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v6, v6

    if-ge v1, v6, :cond_3

    .line 592
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    aget v2, v6, v1

    .line 593
    .local v2, "idx":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v2

    if-nez v6, :cond_2

    const-string v6, ""

    :goto_1
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v7, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Ljava/lang/String;)Ljava/lang/String;

    .line 594
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->getMultiSelectSubtitleCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-eq v1, v6, :cond_1

    .line 595
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\r\n"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Ljava/lang/String;)Ljava/lang/String;

    .line 591
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 593
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleTempText:[Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v2

    goto :goto_1

    .line 598
    .end local v2    # "idx":I
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSubtitleText:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->updateSubtitle(Ljava/lang/String;)V

    .line 611
    .end local v1    # "i":I
    .end local v3    # "textIndex":I
    .end local v4    # "textStartTimeMS":I
    .end local v5    # "textString":Ljava/lang/String;
    :cond_4
    :goto_2
    return-void

    .line 600
    .restart local v3    # "textIndex":I
    .restart local v4    # "textStartTimeMS":I
    .restart local v5    # "textString":Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onTimedText() - , text = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->updateSubtitle(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 604
    .end local v3    # "textIndex":I
    .end local v4    # "textStartTimeMS":I
    .end local v5    # "textString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 605
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "IllegalArgumentException"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 608
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$7;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v6

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniSubtitle:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->clearSubtitle()V

    goto :goto_2
.end method
