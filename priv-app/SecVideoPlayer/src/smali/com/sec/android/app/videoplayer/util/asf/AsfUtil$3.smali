.class Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;
.super Ljava/lang/Object;
.source "AsfUtil.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0

    .prologue
    .line 606
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 610
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "error"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 613
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mServerRemovedListener - onDeviceRemoved : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProvider:Lcom/samsung/android/allshare/media/Provider;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Lcom/samsung/android/allshare/media/Provider;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/Provider;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mCurrentProviderName:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 617
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$3;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v1, 0x2eb

    const/16 v2, 0x2ca

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    .line 620
    :cond_2
    return-void
.end method
