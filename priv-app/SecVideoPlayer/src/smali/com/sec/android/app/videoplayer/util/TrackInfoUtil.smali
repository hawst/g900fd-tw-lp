.class public Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
.super Ljava/lang/Object;
.source "TrackInfoUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TrackInfoUtil"

.field private static mTrackInfoUtil:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

.field private static final mTrackTypes:[Ljava/lang/String;


# instance fields
.field private mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

.field private mInbandSize:I

.field private mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

.field private mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

.field private mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MEDIA_TRACK_TYPE_UNKNOWN"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "MEDIA_TRACK_TYPE_VIDEO"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "MEDIA_TRACK_TYPE_AUDIO"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MEDIA_TRACK_TYPE_TIMEDTEXT"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "MEDIA_TRACK_TYPE_SUBTITLE"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "MEDIA_TRACK_TYPE_SMPTE_TT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandSize:I

    .line 19
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 22
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 25
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 28
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 42
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackInfoUtil:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    if-nez v0, :cond_0

    .line 46
    const-string v0, "TrackInfoUtil"

    const-string v1, "getInstance() : mTrackInfoUtil is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackInfoUtil:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    .line 49
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackInfoUtil:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    return-object v0
.end method

.method private saveInbadTrackInfo(Landroid/media/MediaPlayer$TrackInfo;I)V
    .locals 4
    .param p1, "info"    # Landroid/media/MediaPlayer$TrackInfo;
    .param p2, "index"    # I

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 73
    const-string v1, "TrackInfoUtil"

    const-string v2, "saveInbadTrackInfo : TrackInfo is NULL!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :goto_0
    :pswitch_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v0

    .line 79
    .local v0, "trackType":I
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 80
    if-ltz v0, :cond_1

    sget-object v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 81
    const-string v1, "TrackInfoUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveInbadTrackInfo : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    :cond_1
    packed-switch v0, :pswitch_data_0

    .line 116
    const-string v1, "TrackInfoUtil"

    const-string v2, "saveInbadTrackInfo : TrackType is NOT in the list Please Check the list with MMFW!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_2

    .line 91
    new-instance v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 94
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    goto :goto_0

    .line 98
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_3

    .line 99
    new-instance v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 102
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;I)V

    goto :goto_0

    .line 108
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-nez v1, :cond_4

    .line 109
    new-instance v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 112
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {p1}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p2, v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;II)V

    goto :goto_0

    .line 85
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public addOutbandTextTrackInfo([Landroid/media/MediaPlayer$TrackInfo;)V
    .locals 6
    .param p1, "tinfo"    # [Landroid/media/MediaPlayer$TrackInfo;

    .prologue
    .line 122
    if-nez p1, :cond_1

    .line 123
    const-string v2, "TrackInfoUtil"

    const-string v3, "addOutbandTextTrackInfo : TrackInfo is NULL!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    return-void

    .line 127
    :cond_1
    const-string v2, "TrackInfoUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addOutbandTextTrackInfo. mInbandSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    new-instance v2, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;-><init>(Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 130
    array-length v0, p1

    .line 132
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 133
    aget-object v2, p1, v1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    aget-object v2, p1, v1

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x5

    if-gt v2, v3, :cond_2

    .line 135
    const-string v2, "TrackInfoUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[i] = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tinfo[i].getLanguage() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v1

    invoke-virtual {v4}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " tinfo[i].getTrackType() = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mTrackTypes:[Ljava/lang/String;

    aget-object v5, p1, v1

    invoke-virtual {v5}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v5

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    aget-object v3, p1, v1

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandSize:I

    add-int/2addr v4, v1

    aget-object v5, p1, v1

    invoke-virtual {v5}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->add(Ljava/lang/String;II)V

    .line 132
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public clearInbandTracks()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 142
    const-string v0, "TrackInfoUtil"

    const-string v1, "clearInbandTracks()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->clear()V

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->clear()V

    .line 150
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->clear()V

    .line 155
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 157
    :cond_2
    return-void
.end method

.method public clearOutbandTextTrack()V
    .locals 2

    .prologue
    .line 160
    const-string v0, "TrackInfoUtil"

    const-string v1, "clearOutbandTextTrack()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->clear()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 165
    :cond_0
    return-void
.end method

.method public getAudioTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mAudioTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public getInbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public getOutbandTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public getTextTrackType()I
    .locals 2

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v0

    .line 202
    .local v0, "track":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->getTrackType()I

    move-result v1

    .line 205
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    .locals 3

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleType()I

    move-result v0

    .line 186
    .local v0, "type":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 187
    const-string v1, "TrackInfoUtil"

    const-string v2, "getTextTracks() : return outband text track"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mOutbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    .line 191
    :goto_0
    return-object v1

    .line 190
    :cond_0
    const-string v1, "TrackInfoUtil"

    const-string v2, "getTextTracks() : return inband text track"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandTextTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    goto :goto_0
.end method

.method public getVideoTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mVideoTracks:Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    return-object v0
.end method

.method public resetAllTrackInfo()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "TrackInfoUtil"

    const-string v1, "resetAllTrackInfo()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->clearInbandTracks()V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->clearOutbandTextTrack()V

    .line 69
    return-void
.end method

.method public updateInbandTrackinfo([Landroid/media/MediaPlayer$TrackInfo;)V
    .locals 5
    .param p1, "tinfo"    # [Landroid/media/MediaPlayer$TrackInfo;

    .prologue
    .line 53
    const-string v2, "TrackInfoUtil"

    const-string v3, "updateTrackinfo()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->clearInbandTracks()V

    .line 56
    array-length v0, p1

    .line 57
    .local v0, "N":I
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandSize:I

    .line 58
    const-string v2, "TrackInfoUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTrackinfo() : mInbandSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->mInbandSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 61
    aget-object v2, p1, v1

    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->saveInbadTrackInfo(Landroid/media/MediaPlayer$TrackInfo;I)V

    .line 60
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return-void
.end method
