.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;
.super Ljava/lang/Object;
.source "SubtitleSyncPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# static fields
.field private static final LONGMINUSMSG:I = 0x2

.field private static final LONGPLUSMSG:I = 0x1

.field private static final LONGPRESSDELAY:I = 0xc8

.field private static final SYNC_RANGE_VALUE:I = 0x96

.field private static final SYNC_TIME_VALUE:I = 0xc8

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field public mCurPosition:I

.field private mHandler:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mMinusButton:Landroid/widget/ImageButton;

.field private mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

.field private mPlusButton:Landroid/widget/ImageButton;

.field private mPrevSyncTime:I

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSubtitleSyncPopup:Landroid/app/AlertDialog;

.field private mSubtitleSyncText:Landroid/widget/TextView;

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialog"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 31
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    .line 35
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    .line 36
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPrevSyncTime:I

    .line 287
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$9;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    .line 309
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$10;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 48
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->createPopup()V

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->sendMessage(IJ)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;
    .param p1, "x1"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->removeMessage(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private createPopup()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 53
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    const-string v4, "createPopup E"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->resume()V

    .line 55
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPrevSyncTime:I

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 58
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030036

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 60
    .local v2, "view":Landroid/view/View;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 62
    .local v1, "popup":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0a0141

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 63
    const v3, 0x7f0a00dd

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 72
    const v3, 0x7f0a0026

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    const v3, 0x7f0d0188

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 82
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 83
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v4, 0x12c

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setMax(I)V

    .line 84
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 86
    const v3, 0x7f0d01bb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->setSyncProgress()V

    .line 89
    const v3, 0x7f0d0189

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    .line 90
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    .line 91
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a016d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 93
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$4;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 125
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPlusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$5;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 147
    :cond_0
    const v3, 0x7f0d0187

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    .line 149
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0161

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v6}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$6;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$6;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 157
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$7;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 183
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mMinusButton:Landroid/widget/ImageButton;

    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup$8;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 205
    :cond_1
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 206
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 209
    return-void
.end method

.method private removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 307
    return-void
.end method

.method private revertSubtitleSyncTime()V
    .locals 3

    .prologue
    .line 354
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "revertSubtitleSyncTime() : mPrevSyncTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPrevSyncTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mPrevSyncTime:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 356
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleSyncTime()V

    .line 357
    return-void
.end method

.method private sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 300
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 302
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 303
    return-void
.end method


# virtual methods
.method public clickMINUSBtn()V
    .locals 4

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v0

    int-to-double v0, v0

    const-wide v2, -0x3f22b40000000000L    # -30000.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v1

    add-int/lit16 v1, v1, -0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 281
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 282
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    .line 283
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleSyncTime()V

    .line 285
    :cond_0
    return-void
.end method

.method public clickPLUSBtn()V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v0

    int-to-double v0, v0

    const-wide v2, 0x40dd4c0000000000L    # 30000.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v1

    add-int/lit16 v1, v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleSyncTime()V

    .line 276
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 220
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->revertSubtitleSyncTime()V

    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    .line 224
    :cond_0
    return-void
.end method

.method public isShowSyncPopup()Z
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 215
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const v8, 0x7f0a0120

    .line 246
    if-eqz p3, :cond_1

    .line 247
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    if-ge v1, p2, :cond_2

    .line 248
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    sub-int v3, p2, v3

    mul-int/lit16 v3, v3, 0xc8

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 249
    iput p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    .line 254
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleSyncTime()V

    .line 257
    :cond_1
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onProgressChanged - position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mSyncTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 261
    return-void

    .line 250
    .end local v0    # "time":Ljava/lang/String;
    :cond_2
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    if-le v1, p2, :cond_0

    .line 251
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    sub-int/2addr v3, p2

    mul-int/lit16 v3, v3, 0xc8

    sub-int/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSyncTime(I)V

    .line 252
    iput p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    goto/16 :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 264
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 267
    return-void
.end method

.method public setSyncProgress()V
    .locals 9

    .prologue
    const v8, 0x7f0a0120

    .line 231
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSyncProgress. current sync time : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v1

    if-nez v1, :cond_0

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v2, 0x96

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 239
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mCurPosition:I

    .line 240
    const-string v1, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v4

    int-to-double v4, v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 241
    .local v0, "time":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleSyncText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    return-void

    .line 236
    .end local v0    # "time":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSyncTime()I

    move-result v2

    div-int/lit16 v2, v2, 0xc8

    add-int/lit16 v2, v2, 0x96

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSyncPopup;->createPopup()V

    .line 228
    return-void
.end method
