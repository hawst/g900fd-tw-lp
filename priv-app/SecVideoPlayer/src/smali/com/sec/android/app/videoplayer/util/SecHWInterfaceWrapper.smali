.class public Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;
.super Ljava/lang/Object;
.source "SecHWInterfaceWrapper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecHWInterfaceWrapper"

.field private static mDNIeUIMode:I

.field private static mDNIeUIOutDoor:I

.field private static mDNIeUserMode:I

.field private static mTconUIMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 15
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIMode:I

    .line 16
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIOutDoor:I

    .line 17
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mTconUIMode:I

    .line 18
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static resetMode()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 21
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIMode:I

    .line 22
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIOutDoor:I

    .line 23
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mTconUIMode:I

    .line 24
    sput v0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    .line 25
    return-void
.end method

.method public static setBatteryADC(Ljava/lang/String;Z)V
    .locals 3
    .param p0, "str"    # Ljava/lang/String;
    .param p1, "set"    # Z

    .prologue
    .line 74
    :try_start_0
    invoke-static {p0, p1}, Lcom/sec/android/hardware/SecHardwareInterface;->setBatteryADC(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setBatteryADC()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setTconUIMode(I)V
    .locals 4
    .param p0, "set"    # I

    .prologue
    .line 87
    :try_start_0
    sget v1, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mTconUIMode:I

    if-eq v1, p0, :cond_0

    .line 88
    invoke-static {p0}, Lcom/sec/android/hardware/SecHardwareInterface;->setTconUIMode(I)Z

    .line 89
    sput p0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mTconUIMode:I

    .line 90
    const-string v1, "SecHWInterfaceWrapper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setTconUIMode : set as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setTconUIMode()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setmDNIeOutDoor(Z)V
    .locals 5
    .param p0, "set"    # Z

    .prologue
    .line 41
    if-eqz p0, :cond_1

    const/4 v1, 0x1

    .line 42
    .local v1, "mode":I
    :goto_0
    :try_start_0
    sget v2, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIOutDoor:I

    if-eq v2, v1, :cond_0

    .line 43
    invoke-static {p0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeOutDoor(Z)Z

    .line 44
    sput v1, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIOutDoor:I

    .line 45
    const-string v2, "SecHWInterfaceWrapper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setmDNIeOutDoor : set as "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :cond_0
    :goto_1
    return-void

    .line 41
    .end local v1    # "mode":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 47
    .restart local v1    # "mode":I
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "SecHWInterfaceWrapper"

    const-string v3, "NoSuchMethod - SecHardwareInterface.setmDNIeOutDoor()"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static setmDNIeUIMode(I)V
    .locals 4
    .param p0, "set"    # I

    .prologue
    .line 29
    :try_start_0
    sget v1, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIMode:I

    if-eq v1, p0, :cond_0

    .line 30
    invoke-static {p0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 31
    sput p0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUIMode:I

    .line 32
    const-string v1, "SecHWInterfaceWrapper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setmDNIeUIMode : set as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :cond_0
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    .line 35
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setmDNIeUIMode()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setmDNIeUserMode(I)V
    .locals 3
    .param p0, "mode"    # I

    .prologue
    .line 68
    const-string v0, "SecHWInterfaceWrapper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setmDNIeUserMode(mode) : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " set as : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    sput p0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    .line 70
    return-void
.end method

.method public static setmDNIeUserMode(ILandroid/content/Context;)V
    .locals 4
    .param p0, "set"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const-string v1, "SecHWInterfaceWrapper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setmDNIeUserMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " set as : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :try_start_0
    sget v1, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    if-eq v1, p0, :cond_0

    .line 56
    invoke-static {p0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUserMode(I)Z

    .line 57
    sput p0, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_mode_setting"

    sget v3, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->mDNIeUserMode:I

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 60
    const-string v1, "SecHWInterfaceWrapper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setmDNIeUserMode : set as "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/lang/NoSuchMethodError;
    const-string v1, "SecHWInterfaceWrapper"

    const-string v2, "NoSuchMethod - SecHardwareInterface.setmDNIeUserMode()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
