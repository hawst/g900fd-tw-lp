.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 6751
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;
    .param p2, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 6753
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PresentationService - onServiceConnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6754
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-static {p2}, Lcom/sec/android/app/videoplayer/service/IPresentationService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$7102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/service/IPresentationService;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 6755
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$7100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPresentatnionService(Lcom/sec/android/app/videoplayer/service/IPresentationService;)V

    .line 6756
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "classname"    # Landroid/content/ComponentName;

    .prologue
    .line 6759
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PresentationService - onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6760
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$52;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$7102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/service/IPresentationService;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 6761
    return-void
.end method
