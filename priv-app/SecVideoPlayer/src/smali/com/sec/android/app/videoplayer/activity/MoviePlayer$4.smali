.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->initMainView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 1342
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 7
    .param p1, "isMultiWindow"    # Z

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1356
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onModeChanged. isMultiWindow : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1358
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1359
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->statusBarSetting(Z)V

    .line 1366
    :cond_0
    if-eqz p1, :cond_6

    .line 1367
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Landroid/view/Window;Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 1368
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v0

    .line 1369
    .local v0, "previewMgr":Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1370
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->dismiss()V

    .line 1373
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1374
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    if-ne v1, v5, :cond_2

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1, v5, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 1376
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 1377
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 1378
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    .line 1382
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateMWFontSize()V

    .line 1413
    .end local v0    # "previewMgr":Lcom/sec/android/app/videoplayer/preview/PreviewManager;
    :cond_3
    :goto_0
    if-nez p1, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1414
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V
    invoke-static {v1, v5}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)V

    .line 1417
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateLayoutForMultiWindow()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 1419
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_5

    .line 1420
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->forceUpdateProgressBarSize()V

    .line 1422
    :cond_5
    return-void

    .line 1384
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sget-object v3, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->OTHER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    .line 1386
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->AMOLED_DEVICE:Z

    if-eqz v1, :cond_7

    .line 1387
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "screen_mode_settings"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v6, :cond_9

    .line 1388
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    .line 1389
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "screen_mode_settings"

    invoke-virtual {v1, v2, v6}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    .line 1395
    :cond_7
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1396
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    if-ne v1, v6, :cond_8

    .line 1397
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PresentationServiceRunning!! PLAYER_STOP"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1400
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1404
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 1405
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 1408
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1409
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setFontSize()V

    goto/16 :goto_0

    .line 1391
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    .line 1392
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "screen_mode_settings"

    invoke-virtual {v1, v2, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto :goto_1

    .line 1402
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setCurrentResumePosition()V

    goto :goto_2
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rectInfo"    # Landroid/graphics/Rect;

    .prologue
    .line 1349
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$4;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateMWFontSize()V

    .line 1352
    :cond_0
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 0
    .param p1, "zoneInfo"    # I

    .prologue
    .line 1345
    return-void
.end method
