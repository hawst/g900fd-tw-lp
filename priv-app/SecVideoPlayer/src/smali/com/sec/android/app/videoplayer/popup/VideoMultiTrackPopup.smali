.class public Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;
.super Ljava/lang/Object;
.source "VideoMultiTrackPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# static fields
.field private static final ID:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideoMultiTrackPopup"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mAudioTrack:I

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    .line 29
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mAudioTrack:I

    .line 117
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 126
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 35
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    .line 36
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 37
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->showPopup()V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private showPopup()V
    .locals 10

    .prologue
    .line 61
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v8, "VideoMultiTrackPopup"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 62
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 63
    .local v3, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getAudioTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 66
    .local v1, "audioTracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    const/4 v2, 0x0

    .line 67
    .local v2, "countAudioTrack":I
    if-eqz v1, :cond_0

    .line 68
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    .line 70
    :cond_0
    if-lez v2, :cond_4

    move v7, v2

    :goto_0
    new-array v0, v7, [Ljava/lang/String;

    .line 71
    .local v0, "audioTrackArray":[Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_5

    .line 72
    iget-object v7, v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->Languages:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->getLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 73
    .local v5, "lang":Ljava/lang/String;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 74
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v8, 0x7f0a0019

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 76
    :cond_2
    aput-object v5, v0, v4

    .line 78
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSelectedAudioTrack()I

    move-result v7

    iget-object v8, v1, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->TrackIndex:[I

    aget v8, v8, v4

    if-ne v7, v8, :cond_3

    .line 79
    iput v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mAudioTrack:I

    .line 71
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 70
    .end local v0    # "audioTrackArray":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "lang":Ljava/lang/String;
    :cond_4
    const/4 v7, 0x1

    goto :goto_0

    .line 83
    .restart local v0    # "audioTrackArray":[Ljava/lang/String;
    .restart local v4    # "i":I
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v8, 0x7f0a00a8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 84
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v8, 0x7f0a0026

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$1;

    invoke-direct {v8, p0}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)V

    invoke-virtual {v3, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    const/4 v6, 0x0

    .line 91
    .local v6, "nSelectedID":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getSelectedAudioTrack()I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_6

    .line 92
    iget v6, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mAudioTrack:I

    .line 95
    :cond_6
    const-string v7, "VideoMultiTrackPopup"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "createSelectAudioTrack() nSelectedID: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    if-lez v2, :cond_7

    .line 98
    new-instance v7, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$2;

    invoke-direct {v7, p0}, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;)V

    invoke-virtual {v3, v0, v6, v7}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 111
    :goto_2
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    .line 112
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 113
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 114
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    .line 115
    return-void

    .line 108
    :cond_7
    const v7, 0x7f060005

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 48
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 43
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x3

    return v0
.end method

.method public getLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 163
    move-object v1, p1

    .line 164
    .local v1, "retValue":Ljava/lang/String;
    const-string v3, "eng"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 165
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01ba

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 196
    :goto_0
    return-object v3

    .line 166
    :cond_0
    const-string v3, "kor"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01bf

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 168
    :cond_1
    const-string v3, "chi"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 169
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01b8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 170
    :cond_2
    const-string v3, "jpn"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 171
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01be

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 172
    :cond_3
    const-string v3, "fre"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 173
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01bc

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 174
    :cond_4
    const-string v3, "ger"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01b9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 176
    :cond_5
    const-string v3, "spa"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 177
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01bb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 178
    :cond_6
    const-string v3, "ita"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a01bd

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 181
    :cond_7
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v2

    .line 183
    .local v2, "temp":[Ljava/util/Locale;
    if-eqz v2, :cond_9

    array-length v3, v2

    if-lez v3, :cond_9

    .line 184
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_9

    .line 185
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 186
    const-string v3, "VideoMultiTrackPopup"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getISO3Language() "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v2, v0

    invoke-virtual {v5}, Ljava/util/Locale;->getISO3Language()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " retValue : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/util/Locale;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 190
    :cond_8
    const-string v3, "VideoMultiTrackPopup"

    const-string v4, "Not Founud!!!"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 194
    .end local v0    # "i":I
    :cond_9
    const/4 v1, 0x0

    move-object v3, v1

    .line 196
    goto/16 :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoMultiTrackPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
