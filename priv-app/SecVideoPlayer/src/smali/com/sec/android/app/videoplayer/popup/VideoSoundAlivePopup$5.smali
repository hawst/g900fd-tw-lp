.class Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;
.super Ljava/lang/Object;
.source "VideoSoundAlivePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 259
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-eqz v0, :cond_1

    .line 260
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSoundAlivePopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/content/res/TypedArray;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 263
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mAliveArray:Landroid/content/res/TypedArray;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 266
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;->access$502(Lcom/sec/android/app/videoplayer/popup/VideoSoundAlivePopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 267
    :cond_3
    return-void
.end method
