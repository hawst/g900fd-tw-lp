.class public Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
.super Ljava/lang/Object;
.source "VideoCloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;,
        Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;
    }
.end annotation


# static fields
.field public static final ACTION_SETUP_DROPBOX_ACCOUNT:I = 0x1194

.field public static final CONTENTS_SHOW_ALL:I = 0x0

.field public static final CONTENTS_SHOW_DROPBOX_CONTENT:I = 0x2

.field public static final CONTENTS_SHOW_LOCAL_CONTENT:I = 0x1

.field public static final CONTENTS_SHOW_SKT_DROPBOX_CONTENT:I = 0x3

.field public static final IN_CLOUD_SERVER:Ljava/lang/String; = "cloud_server_id"

.field private static final RECENTLY_PLAYED_COLUMN:Ljava/lang/String; = "isPlayed"

.field private static final TAG:Ljava/lang/String; = "VideoCloudUtil"

.field public static final VIDEO_URI:Landroid/net/Uri;

.field private static mCloudAgentExistence:Z

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

.field private mResolver:Landroid/content/ContentResolver;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    .line 38
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    .line 42
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 44
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 46
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    .line 62
    if-eqz p1, :cond_0

    .line 63
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 66
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->checkCloudAgentExistence()V

    .line 68
    :cond_0
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 27
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    return-object v0
.end method

.method private checkCloudAgentExistence()V
    .locals 5

    .prologue
    .line 498
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_1

    .line 518
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 504
    const/4 v1, 0x0

    .line 507
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.cloudagent"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 513
    :goto_1
    if-nez v1, :cond_2

    .line 514
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    goto :goto_0

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "VideoCloudUtil"

    const-string v3, "checkCloudAgentExistence() - NameNotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 516
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    goto :goto_0
.end method

.method public static get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    .line 75
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    return-object v0
.end method

.method private getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 524
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 525
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 526
    const/4 p2, 0x0

    .line 529
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudActivated()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 530
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "list_view_by_cloud"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 531
    .local v0, "viewBy":I
    const-string v1, "VideoCloudUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFilteredCursor. viewBy : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    packed-switch v0, :pswitch_data_0

    .line 560
    .end local v0    # "viewBy":I
    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 p1, 0x0

    .line 564
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    return-object p1

    .line 535
    .restart local v0    # "viewBy":I
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    if-eqz p2, :cond_1

    .line 536
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 537
    const/4 p2, 0x0

    goto :goto_0

    .line 541
    :pswitch_1
    if-eqz p1, :cond_1

    .line 542
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 543
    const/4 p1, 0x0

    goto :goto_0

    .line 547
    :pswitch_2
    if-eqz p1, :cond_3

    .line 548
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 549
    const/4 p1, 0x0

    .line 551
    :cond_3
    if-eqz p2, :cond_1

    .line 552
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 553
    const/4 p2, 0x0

    goto :goto_0

    .line 561
    .end local v0    # "viewBy":I
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 562
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object p1, p2

    goto :goto_1

    .line 564
    :cond_5
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_1

    .line 533
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getSortOrderString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 101
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "sortorder"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 102
    .local v1, "sortOrder":I
    const-string v2, "VideoCloudUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSortOrderString() order:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    packed-switch v1, :pswitch_data_0

    .line 125
    const-string v2, "_display_name COLLATE LOCALIZED ASC"

    :goto_0
    return-object v2

    .line 106
    :pswitch_0
    const-string v2, "_display_name COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 109
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 116
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "(_size * 1) DESC"

    goto :goto_0

    .line 119
    :pswitch_3
    const-string v2, "mime_type ASC"

    goto :goto_0

    .line 122
    :pswitch_4
    const-string v2, "isPlayed DESC"

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 369
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 370
    :cond_0
    const/4 v0, 0x0

    .line 372
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 583
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 575
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDataOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 595
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 602
    :cond_0
    :goto_0
    return v1

    .line 598
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 599
    .local v0, "tpManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 600
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 579
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWifiOn()Z
    .locals 3

    .prologue
    .line 587
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 588
    const/4 v1, 0x0

    .line 591
    :goto_0
    return v1

    .line 590
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 591
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 568
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 571
    .local v0, "cursorArray":[Landroid/database/Cursor;
    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v1
.end method


# virtual methods
.method public checkInvalidCloudId(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 393
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content://cloud/data/video/media/-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    const/4 v0, 0x1

    .line 397
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteContent(Landroid/net/Uri;)Z
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 325
    sget-boolean v3, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v3, :cond_1

    .line 338
    :cond_0
    :goto_0
    return v1

    .line 328
    :cond_1
    if-nez p1, :cond_2

    .line 329
    const-string v2, "VideoCloudUtil"

    const-string v3, "deleteContent. uri is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 333
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {v3, p1, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 335
    .local v0, "deletedContentCount":I
    if-ne v0, v2, :cond_0

    move v1, v2

    .line 336
    goto :goto_0
.end method

.method public getCloudCachedPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 459
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_1

    move-object v9, v10

    .line 491
    :cond_0
    :goto_0
    return-object v9

    .line 462
    :cond_1
    if-nez p1, :cond_2

    move-object v9, v10

    .line 463
    goto :goto_0

    .line 465
    :cond_2
    const/4 v7, 0x0

    .line 466
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 467
    .local v6, "cached":Ljava/lang/String;
    const/4 v9, 0x0

    .line 468
    .local v9, "localPath":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "cloud_is_cached"

    aput-object v0, v2, v1

    const-string v0, "cloud_cached_path"

    aput-object v0, v2, v3

    .line 473
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 474
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 475
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 476
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 477
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 482
    :cond_3
    if-eqz v7, :cond_4

    .line 483
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 484
    const/4 v7, 0x0

    .line 488
    :cond_4
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    move-object v9, v10

    .line 491
    goto :goto_0

    .line 479
    :catch_0
    move-exception v8

    .line 480
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 482
    if-eqz v7, :cond_4

    .line 483
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 484
    const/4 v7, 0x0

    goto :goto_1

    .line 482
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_6

    .line 483
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 484
    const/4 v7, 0x0

    :cond_6
    throw v0
.end method

.method public getCloudTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 347
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 349
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 350
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 352
    :cond_0
    return-object p1
.end method

.method public getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 137
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 154
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 141
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_display_name like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 144
    :cond_2
    const/4 v6, 0x0

    .line 147
    .local v6, "cloudCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 154
    :goto_1
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0

    .line 148
    :catch_0
    move-exception v7

    .line 149
    .local v7, "e":Ljava/lang/SecurityException;
    const-string v0, "VideoCloudUtil"

    const-string v1, "getMergedCursor - SecurityException"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 150
    .end local v7    # "e":Ljava/lang/SecurityException;
    :catch_1
    move-exception v7

    .line 151
    .local v7, "e":Ljava/lang/RuntimeException;
    const-string v0, "VideoCloudUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMergedCursor - RuntimeException :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 164
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 170
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 167
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 168
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 170
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursorByBucketId(Landroid/database/Cursor;ILjava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "bucketID"    # I
    .param p3, "limitCount"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 182
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 189
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 185
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 186
    .local v3, "where":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 187
    .local v5, "orderBy":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 189
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getUrlByUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 361
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 366
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    const-string v0, "VideoCloudUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUrlByFilePath(uri)nia.noh : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;-><init>(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$GetCloudUrlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public isCachedContent(Landroid/net/Uri;)Z
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 420
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v9

    .line 450
    :goto_0
    return v0

    .line 423
    :cond_0
    if-nez p1, :cond_1

    move v0, v9

    .line 424
    goto :goto_0

    .line 426
    :cond_1
    const/4 v7, 0x0

    .line 427
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 428
    .local v6, "cached":Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "cloud_is_cached"

    aput-object v0, v2, v9

    .line 433
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 435
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 436
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 437
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 441
    :cond_2
    if-eqz v7, :cond_3

    .line 442
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 443
    const/4 v7, 0x0

    .line 447
    :cond_3
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v10

    .line 448
    goto :goto_0

    .line 438
    :catch_0
    move-exception v8

    .line 439
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 441
    if-eqz v7, :cond_3

    .line 442
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 443
    const/4 v7, 0x0

    goto :goto_1

    .line 441
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 442
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 443
    const/4 v7, 0x0

    :cond_4
    throw v0

    :cond_5
    move v0, v9

    .line 450
    goto :goto_0
.end method

.method public isCloudActivated()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 87
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    .line 89
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudContent(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 249
    sget-boolean v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v4, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v3

    .line 252
    :cond_1
    if-eqz p1, :cond_0

    .line 255
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 258
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 261
    :cond_2
    const-string v4, "cloud_server_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 262
    .local v2, "inServerIndex":I
    const/4 v1, 0x0

    .line 265
    .local v1, "inCloud":Ljava/lang/String;
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 266
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 272
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    .line 273
    const/4 v3, 0x1

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isCloudContent(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 305
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 316
    :cond_0
    :goto_0
    return v0

    .line 308
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 311
    const-string v1, "VideoCloudUtil"

    const-string v2, "isCloudContent(uri)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_2

    .line 314
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 316
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isCloudContent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 284
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v0

    .line 287
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 290
    const-string v1, "VideoCloudUtil"

    const-string v2, "isCloudContent(string)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const-wide/16 v2, -0x1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 293
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isContentReady(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 407
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isDataNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataNetworkConnected()Z
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isWifiOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isDataOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    :cond_0
    const/4 v0, 0x1

    .line 240
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isServerOnlyContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 381
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 384
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public requestThumbnail(J)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 198
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object v1

    .line 201
    :cond_1
    const-string v2, "VideoCloudUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestThumbnail() : videoId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const-wide/16 v2, -0x1

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 204
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 205
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/cloudagent/CloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 216
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-object v1

    .line 219
    :cond_1
    if-eqz p1, :cond_0

    .line 222
    const-string v2, "VideoCloudUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestThumbnail() : filePath:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 225
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 226
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/sec/android/cloudagent/CloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public setOnUrlUpdatedListener(Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil$OnUrlUpdatedListener;

    .line 611
    return-void
.end method
