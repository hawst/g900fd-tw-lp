.class public Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;
.super Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.source "MoviePlayerTranslucentStyle.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MoviePlayerTS"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 18
    const-string v0, "android.intent.action.SECURE_LOCK"

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "WhereFrom"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/16 v1, 0x74

    if-ne v0, v1, :cond_1

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 21
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->onCreate(Landroid/os/Bundle;)V

    .line 22
    return-void
.end method

.method public setFrom(Landroid/content/Intent;)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->setFrom(Landroid/content/Intent;)I

    move-result v0

    .line 27
    .local v0, "whereFrom":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_1

    .line 28
    const-string v1, "android.intent.action.SECURE_LOCK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayerTranslucentStyle;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "WhereFrom"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/16 v2, 0x74

    if-ne v1, v2, :cond_1

    .line 29
    :cond_0
    const/16 v0, 0x74

    .line 32
    :cond_1
    const-string v1, "MoviePlayerTS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setFrom : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    return v0
.end method
