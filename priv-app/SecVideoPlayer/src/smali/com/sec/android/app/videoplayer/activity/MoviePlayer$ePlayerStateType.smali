.class public final enum Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
.super Ljava/lang/Enum;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ePlayerStateType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

.field public static final enum PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

.field public static final enum PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 366
    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    new-instance v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->$VALUES:[Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 366
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 366
    const-class v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    .locals 1

    .prologue
    .line 366
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->$VALUES:[Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    return-object v0
.end method
