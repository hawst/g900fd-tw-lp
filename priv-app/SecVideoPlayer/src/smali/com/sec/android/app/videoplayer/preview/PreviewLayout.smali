.class public Lcom/sec/android/app/videoplayer/preview/PreviewLayout;
.super Landroid/widget/LinearLayout;
.source "PreviewLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    }
.end annotation


# instance fields
.field private DisplayHeight:I

.field private DisplayWidth:I

.field private PreviewMargin:I

.field private mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

.field private vi:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/PreviewLayout;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    return-object v0
.end method


# virtual methods
.method public addView()Lcom/sec/android/app/videoplayer/preview/Preview;
    .locals 14

    .prologue
    .line 45
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->vi:Landroid/view/LayoutInflater;

    const v11, 0x7f03000e

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 47
    .local v9, "v":Landroid/view/View;
    const v10, 0x7f0d008e

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 48
    .local v8, "previewlinearlayout":Landroid/widget/LinearLayout;
    new-instance v10, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$1;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    new-instance v10, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;

    invoke-direct {v10, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$2;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 75
    new-instance v10, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;

    invoke-direct {v10, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$3;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 105
    new-instance v10, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$4;

    invoke-direct {v10, p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout$4;-><init>(Lcom/sec/android/app/videoplayer/preview/PreviewLayout;)V

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 124
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x2

    invoke-direct {v7, v10, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 125
    .local v7, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget v10, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->PreviewMargin:I

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v7, v10, v11, v12, v13}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 126
    invoke-virtual {v8, v7}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 131
    const v10, 0x7f0d008f

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 133
    .local v1, "previewlayout":Landroid/widget/LinearLayout;
    const v10, 0x7f0d0090

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    .line 134
    .local v2, "chapterview":Lcom/sec/android/app/videoplayer/videowall/ChapterView;
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    iget v10, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->DisplayWidth:I

    add-int/lit8 v10, v10, -0x1

    iget v11, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->DisplayHeight:I

    add-int/lit8 v11, v11, -0x1

    const/16 v12, 0x11

    invoke-direct {v6, v10, v11, v12}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 135
    .local v6, "fp":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    invoke-virtual {v6, v10, v11, v12, v13}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 136
    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVisibility(I)V

    .line 139
    const v10, 0x7f0d0092

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    .line 140
    .local v3, "progresss":Landroid/widget/ProgressBar;
    const v10, 0x7f0d0091

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 141
    .local v4, "nochapterimage":Landroid/widget/ImageView;
    const v10, 0x7f0d0093

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 143
    .local v5, "chaptertime":Landroid/widget/TextView;
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/preview/Preview;-><init>(Landroid/widget/LinearLayout;Lcom/sec/android/app/videoplayer/videowall/ChapterView;Landroid/widget/ProgressBar;Landroid/widget/ImageView;Landroid/widget/TextView;)V

    .line 145
    .local v0, "pv":Lcom/sec/android/app/videoplayer/preview/Preview;
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->addView(Landroid/view/View;)V

    .line 147
    return-object v0
.end method

.method public init(III)V
    .locals 2
    .param p1, "DisplayWidth"    # I
    .param p2, "DisplayHeight"    # I
    .param p3, "PreviewMargin"    # I

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->vi:Landroid/view/LayoutInflater;

    .line 39
    iput p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->DisplayWidth:I

    .line 40
    iput p2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->DisplayHeight:I

    .line 41
    iput p3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->PreviewMargin:I

    .line 42
    return-void
.end method

.method public setSelectedListener(Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewLayout;->mListener:Lcom/sec/android/app/videoplayer/preview/PreviewLayout$onSelectedListener;

    .line 160
    return-void
.end method
