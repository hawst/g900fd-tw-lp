.class Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$5;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initButtonAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V
    .locals 0

    .prologue
    .line 586
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$5;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "valueAnimator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$5;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$5;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # getter for: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->mMaxRadius:F
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1200(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)F

    move-result v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->updateMask(F)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$1300(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;F)V

    .line 590
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$5;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->invalidate()V

    .line 591
    return-void
.end method
