.class public Lcom/sec/android/app/videoplayer/type/SchemeType;
.super Ljava/lang/Object;
.source "SchemeType.java"


# static fields
.field public static final FROM_ASP_LOCAL:I = 0x6f

.field public static final FROM_ASP_NEAR_BY_DEVICES:I = 0x71

.field public static final FROM_ASP_REMOTE:I = 0x70

.field public static final FROM_ASP_UNKNOWN:I = 0x6e

.field public static final FROM_ASP_WEB_STORAGE:I = 0x72

.field public static final FROM_DMS:I = 0x73

.field public static final FROM_GALLERY:I = 0x64

.field public static final FROM_GALLERY_SECURE_LOCK:I = 0x74

.field public static final FROM_HELP_CLIP:I = 0x6a

.field public static final FROM_HELP_MOTION_PEEK:I = 0x6c

.field public static final FROM_HELP_PROGRESSBAR_PREVIEW:I = 0x68

.field public static final FROM_HELP_SMART_PAUSE:I = 0x69

.field public static final FROM_LOCAL_FILE_PATH:I = 0x66

.field public static final FROM_PHOTORING:I = 0x75

.field public static final FROM_SETUP_WIZARD:I = 0x6b

.field public static final FROM_UNKNOWN:I = -0x1

.field public static final FROM_VZW_GUIDED_TOUR:I = 0x65

.field public static final FROM_WIDGET:I = 0xc8

.field public static final SCHEME_BROWSER_RTSP_TYPE:I = 0x1c

.field public static final SCHEME_BROWSER_SDP_TYPE:I = 0x20

.field public static final SCHEME_BROWSER_TYPE:I = 0x1b

.field public static final SCHEME_CONTENT_EXTERNAL_TYPE:I = 0x16

.field public static final SCHEME_CONTENT_INTERNAL_TYPE:I = 0x15

.field public static final SCHEME_DIRECT_DMC_TYPE:I = 0x25

.field public static final SCHEME_DOWNLOAD_LIST:I = 0x1e

.field public static final SCHEME_FILE_EXTERNAL_TYPE:I = 0x18

.field public static final SCHEME_FILE_HIDDEN_TYPE:I = 0x29

.field public static final SCHEME_FILE_INTERNAL_TYPE:I = 0x17

.field public static final SCHEME_FILE_USB_STORAGE_TYPE:I = 0x24

.field public static final SCHEME_MOVIESTORE:I = 0x1d

.field public static final SCHEME_SIDESYNC:I = 0x28

.field public static final SCHEME_SLINK_LOCAL_TYPE:I = 0x2b

.field public static final SCHEME_SLINK_TYPE:I = 0x2a

.field public static final SCHEME_SSTREAM_TYPE:I = 0x27

.field public static final SCHEME_UNKNOWN_TYPE:I = 0x1a

.field public static final SCHEME_VIDEO_LIST:I = 0x14

.field public static final SCHME_ALLSHARE_VIDEO_LIST:I = 0x21

.field public static final SCHME_BROWSER_HLS_TYPE:I = 0x22

.field public static final SCHME_DROPBOX_HLS_TYPE:I = 0x23


# instance fields
.field private mFrom:I

.field private mKeyType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    .line 85
    iput v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    return-void
.end method


# virtual methods
.method public getFrom()I
    .locals 1

    .prologue
    .line 204
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    return v0
.end method

.method public getKeyType()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    return v0
.end method

.method public isAllShareList()Z
    .locals 2

    .prologue
    .line 188
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x21

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBrowser()Z
    .locals 2

    .prologue
    .line 116
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x1b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContentExternal()Z
    .locals 2

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x16

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDirectDMC()Z
    .locals 2

    .prologue
    .line 192
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x25

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDownloadList()Z
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDropboxHLS()Z
    .locals 2

    .prologue
    .line 140
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x23

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExternal()Z
    .locals 2

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFileBrowsableMode()Z
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDirectDMC()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFileExternal()Z
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFileHidden()Z
    .locals 2

    .prologue
    .line 200
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x29

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFileUSB()Z
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x24

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromASPNearByDevices()Z
    .locals 2

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x71

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromDms()Z
    .locals 2

    .prologue
    .line 260
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x73

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromGallery()Z
    .locals 2

    .prologue
    .line 212
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x74

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromGallerySecureLock()Z
    .locals 2

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x74

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromGuidedTour()Z
    .locals 2

    .prologue
    .line 232
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromHelp()Z
    .locals 1

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromHelpClip()Z
    .locals 2

    .prologue
    .line 248
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x6a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromHelpMotionPeek()Z
    .locals 2

    .prologue
    .line 244
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x6c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromHelpProgressBarPreview()Z
    .locals 2

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromHelpSmartPause()Z
    .locals 2

    .prologue
    .line 240
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromLocalFilePath()Z
    .locals 2

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromPhotoring()Z
    .locals 2

    .prologue
    .line 216
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x75

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromSetupWizard()Z
    .locals 2

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    const/16 v1, 0x6b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFullBrowsableMode()Z
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDirectDMC()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHLS()Z
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x22

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInternal()Z
    .locals 2

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x17

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocalType()Z
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovieStore()Z
    .locals 2

    .prologue
    .line 152
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x1d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovieStoreContent()Z
    .locals 1

    .prologue
    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPreviewMode()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSideSync()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromPhotoring()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRtsp()Z
    .locals 2

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x1c

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLink()Z
    .locals 2

    .prologue
    .line 120
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x2a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x2b

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSLinkStreamingType()Z
    .locals 2

    .prologue
    .line 124
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSStream()Z
    .locals 2

    .prologue
    .line 196
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x27

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSdp()Z
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSideSync()Z
    .locals 2

    .prologue
    .line 156
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x28

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStreamingType()Z
    .locals 1

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLinkStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStore()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDropboxHLS()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSdp()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSideSync()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportPlaylist()Z
    .locals 1

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTypeUnknown()Z
    .locals 2

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoList()Z
    .locals 2

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x14

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    const/16 v1, 0x23

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFrom(I)V
    .locals 0
    .param p1, "from"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mFrom:I

    .line 209
    return-void
.end method

.method public setKeyType(I)V
    .locals 0
    .param p1, "keyType"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/sec/android/app/videoplayer/type/SchemeType;->mKeyType:I

    .line 93
    return-void
.end method
