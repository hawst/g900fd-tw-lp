.class public final Lcom/sec/android/app/videoplayer/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final Details_ID:I = 0x7f0d01b9

.field public static final Details_Value:I = 0x7f0d01ba

.field public static final SS_endtime_ID:I = 0x7f0d0195

.field public static final SS_starttime_ID:I = 0x7f0d0194

.field public static final SS_text_ID:I = 0x7f0d0196

.field public static final SS_time_ID:I = 0x7f0d0193

.field public static final VideoView:I = 0x7f0d01d3

.field public static final action_allshare_refresh:I = 0x7f0d020d

.field public static final action_allshare_scan_for_nearby_devices:I = 0x7f0d020c

.field public static final action_delete:I = 0x7f0d020a

.field public static final action_search:I = 0x7f0d020b

.field public static final action_share_inallshare:I = 0x7f0d0209

.field public static final allshare_refresh_progressbar:I = 0x7f0d0122

.field public static final allshare_title:I = 0x7f0d0121

.field public static final allshare_volume_down:I = 0x7f0d000a

.field public static final allshare_volume_mute:I = 0x7f0d000b

.field public static final allshare_volume_up:I = 0x7f0d0009

.field public static final allsharecast_check:I = 0x7f0d000e

.field public static final allsharecast_check_layout:I = 0x7f0d000d

.field public static final allsharecast_check_text:I = 0x7f0d000f

.field public static final allsharecast_explain:I = 0x7f0d000c

.field public static final asf_display_change_button:I = 0x7f0d01ea

.field public static final asf_first_layout:I = 0x7f0d01e4

.field public static final asf_more_btn:I = 0x7f0d01e6

.field public static final asf_more_btn_layout:I = 0x7f0d01e5

.field public static final asf_second_layout:I = 0x7f0d01e9

.field public static final asf_title_text:I = 0x7f0d01e8

.field public static final asf_title_text_layout:I = 0x7f0d01e7

.field public static final asf_volume_layout:I = 0x7f0d0008

.field public static final audio_only_icon:I = 0x7f0d0115

.field public static final audio_only_text:I = 0x7f0d0116

.field public static final audio_only_text_mini:I = 0x7f0d0081

.field public static final auto_brightness_checkbox:I = 0x7f0d0118

.field public static final auto_brightness_text:I = 0x7f0d0119

.field public static final auto_brightness_text_level:I = 0x7f0d011c

.field public static final back:I = 0x7f0d0005

.field public static final back_layout:I = 0x7f0d0004

.field public static final block_all:I = 0x7f0d00ef

.field public static final block_bottom:I = 0x7f0d00f3

.field public static final block_top:I = 0x7f0d00f2

.field public static final brightness_gesture_img:I = 0x7f0d016a

.field public static final brightness_gesture_img_layout:I = 0x7f0d0169

.field public static final brightness_gesture_seekbar:I = 0x7f0d016c

.field public static final brightness_gesture_seekbar_layout:I = 0x7f0d016b

.field public static final brightness_gesture_text:I = 0x7f0d016e

.field public static final brightness_gesture_text_layout:I = 0x7f0d016d

.field public static final brightness_gesture_vertical:I = 0x7f0d0168

.field public static final brightness_level:I = 0x7f0d011a

.field public static final brightness_text:I = 0x7f0d011b

.field public static final btn_control_l_screen_arrow_left:I = 0x7f0d0144

.field public static final btn_control_l_screen_arrow_right:I = 0x7f0d0143

.field public static final btn_control_mini_arrow:I = 0x7f0d0142

.field public static final btn_exit:I = 0x7f0d0157

.field public static final btn_ff:I = 0x7f0d0158

.field public static final btn_parent:I = 0x7f0d0105

.field public static final btn_pause:I = 0x7f0d015a

.field public static final btn_pause_layout:I = 0x7f0d0159

.field public static final btn_rew:I = 0x7f0d015b

.field public static final button:I = 0x7f0d0001

.field public static final button_layout:I = 0x7f0d01bf

.field public static final buttons_layout:I = 0x7f0d0156

.field public static final capture_btn:I = 0x7f0d012a

.field public static final capture_layout:I = 0x7f0d0129

.field public static final capture_preview:I = 0x7f0d00e7

.field public static final capture_preview_layout:I = 0x7f0d00e6

.field public static final cbSmpteSubtitle:I = 0x7f0d01ad

.field public static final chap_progresss0:I = 0x7f0d0015

.field public static final chap_progresss1:I = 0x7f0d001b

.field public static final chap_progresss10:I = 0x7f0d0051

.field public static final chap_progresss11:I = 0x7f0d0057

.field public static final chap_progresss2:I = 0x7f0d0021

.field public static final chap_progresss3:I = 0x7f0d0027

.field public static final chap_progresss4:I = 0x7f0d002d

.field public static final chap_progresss5:I = 0x7f0d0033

.field public static final chap_progresss6:I = 0x7f0d0039

.field public static final chap_progresss7:I = 0x7f0d003f

.field public static final chap_progresss8:I = 0x7f0d0045

.field public static final chap_progresss9:I = 0x7f0d004b

.field public static final chapterview0:I = 0x7f0d0012

.field public static final chapterview1:I = 0x7f0d0018

.field public static final chapterview10:I = 0x7f0d004e

.field public static final chapterview11:I = 0x7f0d0054

.field public static final chapterview2:I = 0x7f0d001e

.field public static final chapterview3:I = 0x7f0d0024

.field public static final chapterview4:I = 0x7f0d002a

.field public static final chapterview5:I = 0x7f0d0030

.field public static final chapterview6:I = 0x7f0d0036

.field public static final chapterview7:I = 0x7f0d003c

.field public static final chapterview8:I = 0x7f0d0042

.field public static final chapterview9:I = 0x7f0d0048

.field public static final checkbox_value:I = 0x7f0d009a

.field public static final cloud:I = 0x7f0d0182

.field public static final color_palette_view:I = 0x7f0d019a

.field public static final color_text_view:I = 0x7f0d0198

.field public static final connectionPopupChecktext:I = 0x7f0d005b

.field public static final connectionPopupcheckbox:I = 0x7f0d005a

.field public static final connectionexPopupDetailtext:I = 0x7f0d0058

.field public static final connectiontextlayout:I = 0x7f0d0059

.field public static final contextual_checkbox:I = 0x7f0d0068

.field public static final contextual_data:I = 0x7f0d0067

.field public static final contextual_date:I = 0x7f0d0060

.field public static final contextual_date_layout:I = 0x7f0d005f

.field public static final contextual_location:I = 0x7f0d0063

.field public static final contextual_location_image:I = 0x7f0d006e

.field public static final contextual_location_layout:I = 0x7f0d0062

.field public static final contextual_preview_layout:I = 0x7f0d005d

.field public static final contextual_time_image:I = 0x7f0d006c

.field public static final contextual_title_text_location_layout:I = 0x7f0d006b

.field public static final contextual_title_text_time_layout:I = 0x7f0d006a

.field public static final contextualoption_main:I = 0x7f0d005c

.field public static final contextualtitlelayout:I = 0x7f0d0069

.field public static final contextualtitletext_location:I = 0x7f0d006f

.field public static final contextualtitletext_time:I = 0x7f0d006d

.field public static final controller_bottom_line:I = 0x7f0d013f

.field public static final controller_layout:I = 0x7f0d012f

.field public static final controller_layout_background:I = 0x7f0d0130

.field public static final controller_top_layout:I = 0x7f0d0128

.field public static final ctl_bottom_line:I = 0x7f0d015c

.field public static final ctrl_button:I = 0x7f0d0135

.field public static final ctrl_checkbox:I = 0x7f0d0117

.field public static final ctrl_playtime_and_buttons:I = 0x7f0d0131

.field public static final ctrl_playtime_text:I = 0x7f0d0132

.field public static final ctrl_progress:I = 0x7f0d0141

.field public static final ctrl_progressbar:I = 0x7f0d0140

.field public static final ctrl_vol_seekbar_layout_popup:I = 0x7f0d0204

.field public static final ctrl_vol_text_layout2:I = 0x7f0d0206

.field public static final ctrl_vol_vertical_popup:I = 0x7f0d0203

.field public static final default_thumb:I = 0x7f0d0180

.field public static final delete_btn:I = 0x7f0d01d9

.field public static final details:I = 0x7f0d0151

.field public static final details_seek_layout:I = 0x7f0d0146

.field public static final details_seek_ll:I = 0x7f0d0070

.field public static final device_list:I = 0x7f0d0123

.field public static final display_change_button:I = 0x7f0d01c1

.field public static final display_change_button_layout:I = 0x7f0d01dc

.field public static final divider:I = 0x7f0d0003

.field public static final dummy_for_anchor_rotation_0:I = 0x7f0d0106

.field public static final dummy_for_anchor_rotation_180:I = 0x7f0d0108

.field public static final dummy_for_anchor_rotation_90:I = 0x7f0d0107

.field public static final easy_volume_btn:I = 0x7f0d01c0

.field public static final edit_text_contextual:I = 0x7f0d005e

.field public static final ext_screen_ctrl_layout:I = 0x7f0d0152

.field public static final ext_screen_ctrl_layout_background:I = 0x7f0d0153

.field public static final ext_screen_ctrl_playtime_buttons_layout:I = 0x7f0d0154

.field public static final ext_screen_ctrl_progressbar_layout:I = 0x7f0d015d

.field public static final first_layout:I = 0x7f0d01d7

.field public static final first_row_text:I = 0x7f0d0184

.field public static final fit_to_src_btn:I = 0x7f0d0136

.field public static final framelayout0:I = 0x7f0d0011

.field public static final framelayout1:I = 0x7f0d0017

.field public static final framelayout10:I = 0x7f0d004d

.field public static final framelayout11:I = 0x7f0d0053

.field public static final framelayout2:I = 0x7f0d001d

.field public static final framelayout3:I = 0x7f0d0023

.field public static final framelayout4:I = 0x7f0d0029

.field public static final framelayout5:I = 0x7f0d002f

.field public static final framelayout6:I = 0x7f0d0035

.field public static final framelayout7:I = 0x7f0d003b

.field public static final framelayout8:I = 0x7f0d0041

.field public static final framelayout9:I = 0x7f0d0047

.field public static final gesture_layout:I = 0x7f0d0167

.field public static final grid_view:I = 0x7f0d0199

.field public static final half_details_seek_arrow_01:I = 0x7f0d0150

.field public static final half_details_seek_arrow_02:I = 0x7f0d014f

.field public static final half_details_seek_arrow_03:I = 0x7f0d014e

.field public static final half_details_seek_layout:I = 0x7f0d014c

.field public static final half_details_seek_text:I = 0x7f0d014d

.field public static final help_btn1:I = 0x7f0d00ec

.field public static final help_btn2:I = 0x7f0d00ed

.field public static final help_btn3:I = 0x7f0d00ee

.field public static final help_btn_layout:I = 0x7f0d00eb

.field public static final help_description:I = 0x7f0d00ea

.field public static final help_direction_text_view:I = 0x7f0d00f5

.field public static final help_text_layout:I = 0x7f0d00e8

.field public static final help_title:I = 0x7f0d00e9

.field public static final index_view0:I = 0x7f0d0014

.field public static final index_view1:I = 0x7f0d001a

.field public static final index_view10:I = 0x7f0d0050

.field public static final index_view11:I = 0x7f0d0056

.field public static final index_view2:I = 0x7f0d0020

.field public static final index_view3:I = 0x7f0d0026

.field public static final index_view4:I = 0x7f0d002c

.field public static final index_view5:I = 0x7f0d0032

.field public static final index_view6:I = 0x7f0d0038

.field public static final index_view7:I = 0x7f0d003e

.field public static final index_view8:I = 0x7f0d0044

.field public static final index_view9:I = 0x7f0d004a

.field public static final inner_not_selected_color:I = 0x7f0d019f

.field public static final inner_selected_color:I = 0x7f0d01a3

.field public static final layout_listItem:I = 0x7f0d01b8

.field public static final left_flick_1:I = 0x7f0d0162

.field public static final left_flick_layout:I = 0x7f0d0161

.field public static final left_sec:I = 0x7f0d0163

.field public static final linearlayout0:I = 0x7f0d0010

.field public static final linearlayout1:I = 0x7f0d0016

.field public static final linearlayout10:I = 0x7f0d004c

.field public static final linearlayout11:I = 0x7f0d0052

.field public static final linearlayout2:I = 0x7f0d001c

.field public static final linearlayout3:I = 0x7f0d0022

.field public static final linearlayout4:I = 0x7f0d0028

.field public static final linearlayout5:I = 0x7f0d002e

.field public static final linearlayout6:I = 0x7f0d0034

.field public static final linearlayout7:I = 0x7f0d003a

.field public static final linearlayout8:I = 0x7f0d0040

.field public static final linearlayout9:I = 0x7f0d0046

.field public static final list_effect_frame:I = 0x7f0d017e

.field public static final list_main:I = 0x7f0d0066

.field public static final list_title:I = 0x7f0d0179

.field public static final list_title_layout:I = 0x7f0d0178

.field public static final loadingtext:I = 0x7f0d01ef

.field public static final lock_btn:I = 0x7f0d0176

.field public static final main_view:I = 0x7f0d0177

.field public static final menu_cancel:I = 0x7f0d020e

.field public static final menu_chapterview:I = 0x7f0d021e

.field public static final menu_color_and_opacity_separator:I = 0x7f0d01b6

.field public static final menu_delete:I = 0x7f0d021c

.field public static final menu_details:I = 0x7f0d0224

.field public static final menu_done:I = 0x7f0d020f

.field public static final menu_download:I = 0x7f0d0210

.field public static final menu_edit:I = 0x7f0d0213

.field public static final menu_group_edit:I = 0x7f0d0212

.field public static final menu_layout:I = 0x7f0d0065

.field public static final menu_listen_via_bluetooth:I = 0x7f0d0220

.field public static final menu_listen_via_device:I = 0x7f0d021f

.field public static final menu_overview_separator:I = 0x7f0d01b0

.field public static final menu_play_via_groupplay:I = 0x7f0d0221

.field public static final menu_popup_play:I = 0x7f0d021d

.field public static final menu_sendto_another:I = 0x7f0d0211

.field public static final menu_settings:I = 0x7f0d0223

.field public static final menu_share:I = 0x7f0d021b

.field public static final menu_sstudio:I = 0x7f0d0219

.field public static final menu_subtitle:I = 0x7f0d0222

.field public static final menu_text_separator:I = 0x7f0d01b3

.field public static final menu_trim:I = 0x7f0d0218

.field public static final menu_videomaker:I = 0x7f0d021a

.field public static final miniVideoSurfaceLayout:I = 0x7f0d007d

.field public static final mini_audio_only:I = 0x7f0d0080

.field public static final mini_empty_layout:I = 0x7f0d007f

.field public static final mini_surface_view:I = 0x7f0d007e

.field public static final mini_video_btn_layout:I = 0x7f0d0078

.field public static final mini_video_controller_layout:I = 0x7f0d0075

.field public static final mini_video_exit_img:I = 0x7f0d0086

.field public static final mini_video_exit_layout:I = 0x7f0d0085

.field public static final mini_video_loading_layout:I = 0x7f0d0082

.field public static final mini_video_loading_progress:I = 0x7f0d0083

.field public static final mini_video_loading_text:I = 0x7f0d0084

.field public static final mini_video_play_speed_layout:I = 0x7f0d0076

.field public static final mini_video_play_speed_text:I = 0x7f0d0077

.field public static final mini_video_player_btn_next:I = 0x7f0d007b

.field public static final mini_video_player_btn_playause:I = 0x7f0d007a

.field public static final mini_video_player_btn_prev:I = 0x7f0d0079

.field public static final mini_video_progressbar_layout:I = 0x7f0d0087

.field public static final minus:I = 0x7f0d0187

.field public static final more_btn:I = 0x7f0d01c9

.field public static final more_btn_layout:I = 0x7f0d01d8

.field public static final motion_peek_help_text_layout:I = 0x7f0d00f0

.field public static final nochapterimage0:I = 0x7f0d0013

.field public static final nochapterimage1:I = 0x7f0d0019

.field public static final nochapterimage10:I = 0x7f0d004f

.field public static final nochapterimage11:I = 0x7f0d0055

.field public static final nochapterimage2:I = 0x7f0d001f

.field public static final nochapterimage3:I = 0x7f0d0025

.field public static final nochapterimage4:I = 0x7f0d002b

.field public static final nochapterimage5:I = 0x7f0d0031

.field public static final nochapterimage6:I = 0x7f0d0037

.field public static final nochapterimage7:I = 0x7f0d003d

.field public static final nochapterimage8:I = 0x7f0d0043

.field public static final nochapterimage9:I = 0x7f0d0049

.field public static final not_selected:I = 0x7f0d019d

.field public static final not_selected_color:I = 0x7f0d019e

.field public static final not_selected_color_border:I = 0x7f0d01a0

.field public static final notification_parent_layout:I = 0x7f0d00f7

.field public static final novideos1:I = 0x7f0d01f1

.field public static final novideos2:I = 0x7f0d01f2

.field public static final one_frame_backward_btn:I = 0x7f0d012e

.field public static final one_frame_backward_layout:I = 0x7f0d012d

.field public static final one_frame_forward_btn:I = 0x7f0d012c

.field public static final one_frame_forward_layout:I = 0x7f0d012b

.field public static final opacity_text_view:I = 0x7f0d019b

.field public static final opacitytext:I = 0x7f0d019c

.field public static final personalPage:I = 0x7f0d00fc

.field public static final play_speed:I = 0x7f0d010a

.field public static final play_speed_layout:I = 0x7f0d0109

.field public static final play_speed_message:I = 0x7f0d010b

.field public static final player_list_btn:I = 0x7f0d013a

.field public static final playerdescription:I = 0x7f0d0126

.field public static final playericon:I = 0x7f0d0124

.field public static final playername:I = 0x7f0d0125

.field public static final playspeed:I = 0x7f0d0186

.field public static final playtime_text_layout:I = 0x7f0d0155

.field public static final plus:I = 0x7f0d0189

.field public static final popup:I = 0x7f0d0007

.field public static final preview_arrow:I = 0x7f0d01d5

.field public static final preview_backgound:I = 0x7f0d01d1

.field public static final preview_current_time:I = 0x7f0d01d4

.field public static final previewchaptertime:I = 0x7f0d0093

.field public static final previewchapterview:I = 0x7f0d0090

.field public static final previewdialog:I = 0x7f0d0094

.field public static final previewitem:I = 0x7f0d0095

.field public static final previewlayout:I = 0x7f0d008f

.field public static final previewlinearlayout:I = 0x7f0d008e

.field public static final previewnochapterimage:I = 0x7f0d0091

.field public static final previewprogresss:I = 0x7f0d0092

.field public static final progress:I = 0x7f0d015f

.field public static final progressBar_preview_seeked_bitmap:I = 0x7f0d018a

.field public static final progressBar_preview_show_time:I = 0x7f0d018b

.field public static final progress_area:I = 0x7f0d00f4

.field public static final progress_layout:I = 0x7f0d015e

.field public static final progress_view:I = 0x7f0d017b

.field public static final progressview:I = 0x7f0d0183

.field public static final quarter_details_seek_arrow_01:I = 0x7f0d014b

.field public static final quarter_details_seek_arrow_02:I = 0x7f0d014a

.field public static final quarter_details_seek_arrow_03:I = 0x7f0d0149

.field public static final quarter_details_seek_layout:I = 0x7f0d0147

.field public static final quarter_details_seek_text:I = 0x7f0d0148

.field public static final quick_panel_call_thumbnail:I = 0x7f0d00f9

.field public static final quick_panel_close:I = 0x7f0d0103

.field public static final quick_panel_default:I = 0x7f0d00fe

.field public static final quick_panel_default_layout:I = 0x7f0d0104

.field public static final quick_panel_ff:I = 0x7f0d0102

.field public static final quick_panel_left_layout:I = 0x7f0d00f8

.field public static final quick_panel_middle_layout:I = 0x7f0d00ff

.field public static final quick_panel_play_pause:I = 0x7f0d0101

.field public static final quick_panel_rew:I = 0x7f0d00fb

.field public static final quick_panel_thumbnail:I = 0x7f0d00fa

.field public static final quick_panel_thumbnail_button:I = 0x7f0d00fd

.field public static final quick_panel_title:I = 0x7f0d0100

.field public static final refresh_progressbar:I = 0x7f0d01ee

.field public static final relative_presentation_view:I = 0x7f0d0160

.field public static final relative_progress_preview:I = 0x7f0d01cf

.field public static final right_flick_1:I = 0x7f0d0165

.field public static final right_flick_layout:I = 0x7f0d0164

.field public static final right_sec:I = 0x7f0d0166

.field public static final rotate_ctrl_icon:I = 0x7f0d010d

.field public static final rotate_ctrl_icon_layout:I = 0x7f0d01e0

.field public static final rotate_layout:I = 0x7f0d010c

.field public static final row_main_layout:I = 0x7f0d017d

.field public static final scover_video_main:I = 0x7f0d0089

.field public static final search_speed:I = 0x7f0d010f

.field public static final search_speed_layout:I = 0x7f0d010e

.field public static final search_speed_message:I = 0x7f0d0110

.field public static final second_layout:I = 0x7f0d01e3

.field public static final second_row_text:I = 0x7f0d0185

.field public static final seekText:I = 0x7f0d0071

.field public static final seekbar:I = 0x7f0d0188

.field public static final selected:I = 0x7f0d01a1

.field public static final selected_color:I = 0x7f0d01a2

.field public static final selected_color_border:I = 0x7f0d01a4

.field public static final selected_player_check:I = 0x7f0d0127

.field public static final selected_subtitle_check:I = 0x7f0d018f

.field public static final shareAppIcon:I = 0x7f0d009b

.field public static final shareAppName:I = 0x7f0d009c

.field public static final sidechap_progresss0:I = 0x7f0d00a3

.field public static final sidechap_progresss1:I = 0x7f0d00a9

.field public static final sidechap_progresss10:I = 0x7f0d00df

.field public static final sidechap_progresss11:I = 0x7f0d00e5

.field public static final sidechap_progresss2:I = 0x7f0d00af

.field public static final sidechap_progresss3:I = 0x7f0d00b5

.field public static final sidechap_progresss4:I = 0x7f0d00bb

.field public static final sidechap_progresss5:I = 0x7f0d00c1

.field public static final sidechap_progresss6:I = 0x7f0d00c7

.field public static final sidechap_progresss7:I = 0x7f0d00cd

.field public static final sidechap_progresss8:I = 0x7f0d00d3

.field public static final sidechap_progresss9:I = 0x7f0d00d9

.field public static final sidechapterview:I = 0x7f0d009d

.field public static final sidechapterview0:I = 0x7f0d00a0

.field public static final sidechapterview1:I = 0x7f0d00a6

.field public static final sidechapterview10:I = 0x7f0d00dc

.field public static final sidechapterview11:I = 0x7f0d00e2

.field public static final sidechapterview2:I = 0x7f0d00ac

.field public static final sidechapterview3:I = 0x7f0d00b2

.field public static final sidechapterview4:I = 0x7f0d00b8

.field public static final sidechapterview5:I = 0x7f0d00be

.field public static final sidechapterview6:I = 0x7f0d00c4

.field public static final sidechapterview7:I = 0x7f0d00ca

.field public static final sidechapterview8:I = 0x7f0d00d0

.field public static final sidechapterview9:I = 0x7f0d00d6

.field public static final sideframelayout0:I = 0x7f0d009f

.field public static final sideframelayout1:I = 0x7f0d00a5

.field public static final sideframelayout10:I = 0x7f0d00db

.field public static final sideframelayout11:I = 0x7f0d00e1

.field public static final sideframelayout2:I = 0x7f0d00ab

.field public static final sideframelayout3:I = 0x7f0d00b1

.field public static final sideframelayout4:I = 0x7f0d00b7

.field public static final sideframelayout5:I = 0x7f0d00bd

.field public static final sideframelayout6:I = 0x7f0d00c3

.field public static final sideframelayout7:I = 0x7f0d00c9

.field public static final sideframelayout8:I = 0x7f0d00cf

.field public static final sideframelayout9:I = 0x7f0d00d5

.field public static final sideindex_view0:I = 0x7f0d00a1

.field public static final sideindex_view1:I = 0x7f0d00a8

.field public static final sideindex_view10:I = 0x7f0d00de

.field public static final sideindex_view11:I = 0x7f0d00e4

.field public static final sideindex_view2:I = 0x7f0d00ae

.field public static final sideindex_view3:I = 0x7f0d00b4

.field public static final sideindex_view4:I = 0x7f0d00ba

.field public static final sideindex_view5:I = 0x7f0d00c0

.field public static final sideindex_view6:I = 0x7f0d00c6

.field public static final sideindex_view7:I = 0x7f0d00cc

.field public static final sideindex_view8:I = 0x7f0d00d2

.field public static final sideindex_view9:I = 0x7f0d00d8

.field public static final sidelinearlayout0:I = 0x7f0d009e

.field public static final sidelinearlayout1:I = 0x7f0d00a4

.field public static final sidelinearlayout10:I = 0x7f0d00da

.field public static final sidelinearlayout11:I = 0x7f0d00e0

.field public static final sidelinearlayout2:I = 0x7f0d00aa

.field public static final sidelinearlayout3:I = 0x7f0d00b0

.field public static final sidelinearlayout4:I = 0x7f0d00b6

.field public static final sidelinearlayout5:I = 0x7f0d00bc

.field public static final sidelinearlayout6:I = 0x7f0d00c2

.field public static final sidelinearlayout7:I = 0x7f0d00c8

.field public static final sidelinearlayout8:I = 0x7f0d00ce

.field public static final sidelinearlayout9:I = 0x7f0d00d4

.field public static final sidenochapterimage0:I = 0x7f0d00a2

.field public static final sidenochapterimage1:I = 0x7f0d00a7

.field public static final sidenochapterimage10:I = 0x7f0d00dd

.field public static final sidenochapterimage11:I = 0x7f0d00e3

.field public static final sidenochapterimage2:I = 0x7f0d00ad

.field public static final sidenochapterimage3:I = 0x7f0d00b3

.field public static final sidenochapterimage4:I = 0x7f0d00b9

.field public static final sidenochapterimage5:I = 0x7f0d00bf

.field public static final sidenochapterimage6:I = 0x7f0d00c5

.field public static final sidenochapterimage7:I = 0x7f0d00cb

.field public static final sidenochapterimage8:I = 0x7f0d00d1

.field public static final sidenochapterimage9:I = 0x7f0d00d7

.field public static final slideText:I = 0x7f0d0072

.field public static final smart_pause_help_text_layout:I = 0x7f0d00f6

.field public static final smpteCustomLine:I = 0x7f0d01ae

.field public static final snap_shot_layout:I = 0x7f0d0145

.field public static final soundalive_btn:I = 0x7f0d0208

.field public static final ss_title:I = 0x7f0d0191

.field public static final ss_title_layout:I = 0x7f0d0190

.field public static final stub_detail:I = 0x7f0d0111

.field public static final stub_detail_inflated:I = 0x7f0d0112

.field public static final stub_detail_portrait:I = 0x7f0d0113

.field public static final stub_detail_portrait_inflated:I = 0x7f0d0114

.field public static final submenu_group_edit:I = 0x7f0d0214

.field public static final submenu_sstudio:I = 0x7f0d0216

.field public static final submenu_trim:I = 0x7f0d0215

.field public static final submenu_videomaker:I = 0x7f0d0217

.field public static final subtitle_lable_color:I = 0x7f0d01b5

.field public static final subtitle_lable_overview:I = 0x7f0d01af

.field public static final subtitle_lable_text:I = 0x7f0d01b2

.field public static final subtitle_layout:I = 0x7f0d01a5

.field public static final subtitle_main:I = 0x7f0d01a7

.field public static final subtitle_menu_color:I = 0x7f0d01b7

.field public static final subtitle_menu_overview:I = 0x7f0d01b1

.field public static final subtitle_menu_text:I = 0x7f0d01b4

.field public static final subtitle_name:I = 0x7f0d018d

.field public static final subtitle_name_layout:I = 0x7f0d018c

.field public static final subtitle_path:I = 0x7f0d018e

.field public static final subtitle_preview:I = 0x7f0d01aa

.field public static final subtitle_preview_layout:I = 0x7f0d01a8

.field public static final subtitle_scrollview:I = 0x7f0d0197

.field public static final subtitle_smpte_activation:I = 0x7f0d01ab

.field public static final subtitle_smpte_onoff:I = 0x7f0d01ac

.field public static final subtitle_textview:I = 0x7f0d01a6

.field public static final subtitle_thumb:I = 0x7f0d01a9

.field public static final subview_controller_layout:I = 0x7f0d01be

.field public static final subview_ctrl_button:I = 0x7f0d01ca

.field public static final subview_ctrl_button_divider:I = 0x7f0d01ce

.field public static final subview_ctrl_playtime_text:I = 0x7f0d01c3

.field public static final subview_ctrl_progress:I = 0x7f0d01c7

.field public static final subview_ctrl_progress_divider:I = 0x7f0d01c2

.field public static final subview_ctrl_progressbar:I = 0x7f0d01c6

.field public static final subview_time_current:I = 0x7f0d01c4

.field public static final subview_time_total:I = 0x7f0d01c5

.field public static final subview_videocontroller_progress:I = 0x7f0d01c8

.field public static final subview_videoplayer_btn_ff:I = 0x7f0d01cd

.field public static final subview_videoplayer_btn_pause:I = 0x7f0d01cc

.field public static final subview_videoplayer_btn_rew:I = 0x7f0d01cb

.field public static final switch_action:I = 0x7f0d0002

.field public static final switch_layout:I = 0x7f0d0098

.field public static final switch_value:I = 0x7f0d0099

.field public static final synctext:I = 0x7f0d01bb

.field public static final text1:I = 0x7f0d00f1

.field public static final text_contextual_date:I = 0x7f0d0061

.field public static final text_contextual_location:I = 0x7f0d0064

.field public static final text_item_list:I = 0x7f0d01bc

.field public static final text_menu_item_view:I = 0x7f0d01bd

.field public static final text_value:I = 0x7f0d0097

.field public static final thumbnail:I = 0x7f0d0181

.field public static final thumbnail_border_layout:I = 0x7f0d017f

.field public static final time_current:I = 0x7f0d0133

.field public static final time_total:I = 0x7f0d0134

.field public static final title:I = 0x7f0d0006

.field public static final title_divider:I = 0x7f0d017a

.field public static final title_layout:I = 0x7f0d01d6

.field public static final title_text:I = 0x7f0d01e2

.field public static final title_text_layout:I = 0x7f0d01e1

.field public static final titlebar:I = 0x7f0d0000

.field public static final trans_window:I = 0x7f0d007c

.field public static final tutorial_img:I = 0x7f0d0073

.field public static final tutorial_text:I = 0x7f0d0074

.field public static final value:I = 0x7f0d0096

.field public static final video_exit:I = 0x7f0d008c

.field public static final video_layout:I = 0x7f0d008a

.field public static final video_play:I = 0x7f0d008d

.field public static final video_surface:I = 0x7f0d008b

.field public static final video_widget_bottom_layout:I = 0x7f0d01f5

.field public static final videocontroller_layout:I = 0x7f0d011d

.field public static final videocontroller_layout_sub:I = 0x7f0d011f

.field public static final videocontroller_progress:I = 0x7f0d011e

.field public static final videocontroller_progress_auto:I = 0x7f0d0120

.field public static final videohub_watch_on_tv:I = 0x7f0d01de

.field public static final videohub_watch_on_tv_layout:I = 0x7f0d01dd

.field public static final videoplayer_animated_play_pause_btn:I = 0x7f0d013d

.field public static final videoplayer_animated_play_stop_btn:I = 0x7f0d013e

.field public static final videoplayer_btn_app_in_app:I = 0x7f0d0138

.field public static final videoplayer_btn_app_in_app_layout:I = 0x7f0d01df

.field public static final videoplayer_btn_ff:I = 0x7f0d0139

.field public static final videoplayer_btn_pause:I = 0x7f0d013c

.field public static final videoplayer_btn_pause_layout:I = 0x7f0d013b

.field public static final videoplayer_btn_rew:I = 0x7f0d0137

.field public static final videoplayer_list:I = 0x7f0d017c

.field public static final videoplayer_ss_list:I = 0x7f0d0192

.field public static final videowidget_2x1_def:I = 0x7f0d01f0

.field public static final videowidget_bg:I = 0x7f0d01ed

.field public static final videowidget_bottom_background:I = 0x7f0d01f6

.field public static final videowidget_divider:I = 0x7f0d01fe

.field public static final videowidget_duration:I = 0x7f0d01fa

.field public static final videowidget_filename:I = 0x7f0d01f8

.field public static final videowidget_magazine_2x1_def:I = 0x7f0d01fb

.field public static final videowidget_magazine_bg:I = 0x7f0d01ff

.field public static final videowidget_magazine_details:I = 0x7f0d01fd

.field public static final videowidget_magazine_noitem_ic:I = 0x7f0d0200

.field public static final videowidget_magazine_novideo1:I = 0x7f0d0201

.field public static final videowidget_magazine_novideo2:I = 0x7f0d0202

.field public static final videowidget_magazine_title:I = 0x7f0d01fc

.field public static final videowidget_play:I = 0x7f0d01f7

.field public static final videowidget_progressbar:I = 0x7f0d0088

.field public static final videowidget_remain:I = 0x7f0d01f9

.field public static final videowidget_shadow:I = 0x7f0d01f3

.field public static final videowidget_view:I = 0x7f0d01f4

.field public static final vol_seekbar_popup:I = 0x7f0d0205

.field public static final vol_text_popup:I = 0x7f0d0207

.field public static final volume_btn:I = 0x7f0d01db

.field public static final volume_btn_layout:I = 0x7f0d01da

.field public static final volume_gesture_img:I = 0x7f0d0171

.field public static final volume_gesture_img_layout:I = 0x7f0d0170

.field public static final volume_gesture_seekbar:I = 0x7f0d0173

.field public static final volume_gesture_seekbar_layout:I = 0x7f0d0172

.field public static final volume_gesture_text:I = 0x7f0d0175

.field public static final volume_gesture_text_layout:I = 0x7f0d0174

.field public static final volume_gesture_vertical:I = 0x7f0d016f

.field public static final vppp_content_area:I = 0x7f0d01d0

.field public static final vppp_surface_and_timetext:I = 0x7f0d01d2

.field public static final watermark:I = 0x7f0d01ec

.field public static final watermark_layout:I = 0x7f0d01eb


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
