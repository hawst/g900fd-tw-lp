.class Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;
.super Ljava/lang/Object;
.source "VideoTagBuddySetting.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->updateTitlebar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 10
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const v9, 0x7f0d0068

    const/16 v8, 0x8

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 239
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$300(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/Switch;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$300(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/Switch;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Switch;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .line 241
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    .line 245
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget v2, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .local v2, "position":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget v3, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    if-gt v2, v3, :cond_2

    .line 246
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$500(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->individual_item_view:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 247
    .local v1, "itemView":Landroid/view/View;
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 249
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 250
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 258
    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 259
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    .line 245
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 260
    :cond_1
    if-ne v2, v6, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 261
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    goto :goto_1

    .line 265
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v1    # "itemView":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/ListView;->setAlpha(F)V

    .line 266
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ListView;->setClickable(Z)V

    .line 289
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$000(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mSwitch:Landroid/widget/Switch;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$300(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/Switch;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Switch;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->setTagBuddyShowStatus(Z)V

    .line 290
    return-void

    .line 269
    .end local v2    # "position":I
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .line 270
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v4

    iput v4, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    .line 274
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget v2, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mFirstVisibleItem:I

    .restart local v2    # "position":I
    :goto_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    iget v3, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mLastVisibleItem:I

    if-gt v2, v3, :cond_4

    .line 275
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->adapter:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$500(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;

    move-result-object v3

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$ContextualListAdapter;->individual_item_view:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 277
    .restart local v1    # "itemView":Landroid/view/View;
    invoke-virtual {v1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 278
    .restart local v0    # "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 274
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 281
    .end local v0    # "cb":Landroid/widget/CheckBox;
    .end local v1    # "itemView":Landroid/view/View;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    const v4, 0x3e99999a    # 0.3f

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAlpha(F)V

    .line 282
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeLocationTagVisibility(I)V

    .line 283
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    invoke-virtual {v3, v8}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->changeDateTagVisibility(I)V

    .line 285
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 286
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting$2;->this$0:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;

    # getter for: Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;->access$400(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddySetting;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setClickable(Z)V

    goto :goto_2
.end method
