.class public Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;
.super Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
.source "WfdUtilNew.java"


# static fields
.field private static sWfdUtilNew:Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->sWfdUtilNew:Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;-><init>()V

    .line 16
    const-class v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method private connectWifiDisplay(Ljava/lang/String;)V
    .locals 3
    .param p1, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 191
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->bHasPresentaionFocus:Z

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->RESUME:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 193
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "connectWifiDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WifiDisplayStatus.CONN_STATE_CHANGEPLAYER_VIDEO. deviceAddress : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v1, 0x6

    invoke-virtual {v0, v1, p1}, Landroid/hardware/display/DisplayManager;->connectWifiDisplayWithMode(ILjava/lang/String;)V

    .line 197
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->sWfdUtilNew:Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    return-object v0
.end method


# virtual methods
.method public connect(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "dev"    # Landroid/os/Parcelable;

    .prologue
    .line 35
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v2, "connect"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p1

    .line 36
    check-cast v0, Landroid/hardware/display/WifiDisplay;

    .line 37
    .local v0, "d":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->connectWifiDisplay(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public forceTerminateWfd()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "forceTerminateWfd. E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 48
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "forceTerminateWfd. DisplayManager.disconnectWifiDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method public getConnectedDisplayInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getConnectedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getDeviceName(Landroid/os/Parcelable;)Ljava/lang/String;
    .locals 3
    .param p1, "d"    # Landroid/os/Parcelable;

    .prologue
    .line 130
    const/4 v1, 0x0

    .local v1, "name":Ljava/lang/String;
    move-object v0, p1

    .line 131
    check-cast v0, Landroid/hardware/display/WifiDisplay;

    .line 132
    .local v0, "dev":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    .line 133
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v1

    .line 137
    :cond_0
    return-object v1
.end method

.method public getPrimaryDeviceType(Landroid/os/Parcelable;)Ljava/lang/String;
    .locals 2
    .param p1, "d"    # Landroid/os/Parcelable;

    .prologue
    .line 142
    move-object v0, p1

    check-cast v0, Landroid/hardware/display/WifiDisplay;

    .line 143
    .local v0, "dev":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public isWfdConnected()Z
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "isWfdConnected >> true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    const/4 v0, 0x1

    .line 154
    :goto_0
    return v0

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "isWfdConnected >> false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWfdConnectedByOtherApp()Z
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isWfdConnectedByOtherApp : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v2

    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplayStatus;->getConnectedState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getConnectedState()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getConnectedState()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "isWfdConnectedByOtherApp >> true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "isWfdConnectedByOtherApp >> false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyChanged(II)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mMsg:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mMsg:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 108
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->setChanged()V

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mMsg:[I

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->notifyObservers(Ljava/lang/Object;)V

    .line 111
    return-void
.end method

.method public prepare()V
    .locals 3

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "airplane_mode_on"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->scanWifiDisplays()V

    .line 31
    :cond_0
    return-void
.end method

.method public refreshWifiDisplays()V
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "refreshWifiDisplays E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "refreshWifiDisplays. DisplayManager.scanWifiDisplays()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    return-void
.end method

.method public scanWifiDisplays()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "scanWifiDisplays E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->SETUP:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 76
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "scanWifiDisplays. DisplayManager.scanWifiDisplays()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    :cond_0
    return-void
.end method

.method public setActivityState(Z)V
    .locals 3
    .param p1, "start"    # Z

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setActivityState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 175
    if-eqz p1, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->SETUP:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->RESUME:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->PAUSE:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    sget-object v1, Landroid/hardware/display/DisplayManager$WfdAppState;->TEARDOWN:Landroid/hardware/display/DisplayManager$WfdAppState;

    invoke-virtual {v0, v1}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    goto :goto_0
.end method

.method public setWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V
    .locals 0
    .param p1, "wfdStatus"    # Landroid/hardware/display/WifiDisplayStatus;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 116
    return-void
.end method

.method public stopScanWifiDisplays()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "stopScanWifiDisplays E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "DisplayManager.stopScanWifiDisplays()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    return-void
.end method

.method public terminateWfd()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "terminateWfd. E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->bHasPresentaionFocus:Z

    if-nez v0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-nez v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mContext:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 63
    :cond_2
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v1}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getConnectedState()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->TAG:Ljava/lang/String;

    const-string v1, "terminateWfd. DisplayManager.disconnectWifiDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
