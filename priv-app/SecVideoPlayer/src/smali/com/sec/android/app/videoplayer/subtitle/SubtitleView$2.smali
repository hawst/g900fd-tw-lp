.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;
.super Ljava/lang/Object;
.source "SubtitleView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 205
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 206
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 207
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v4, v8, v4

    if-gtz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpl-float v4, v8, v4

    if-gtz v4, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 216
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v4, v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v4, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    if-ne v4, v7, :cond_2

    .line 217
    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$200()Ljava/lang/String;

    move-result-object v4

    const-string v5, "gestureDector return true"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    :cond_1
    :goto_0
    return v7

    .line 219
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v2, v4

    .line 220
    .local v2, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 221
    .local v3, "y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 223
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelX:I
    invoke-static {v4, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$302(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I

    .line 224
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelY:I
    invoke-static {v4, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$402(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 228
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z
    invoke-static {v4, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$502(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z

    .line 229
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelX:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I

    move-result v4

    sub-int v0, v2, v4

    .line 230
    .local v0, "tx":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelY:I
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I

    move-result v4

    sub-int v1, v3, v4

    .line 231
    .local v1, "ty":I
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getTop()I

    move-result v5

    add-int/2addr v5, v1

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getBottom()I

    move-result v6

    add-int/2addr v6, v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->layoutMove(II)V
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;II)V

    .line 232
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout(Z)V

    goto :goto_0

    .line 237
    .end local v0    # "tx":I
    .end local v1    # "ty":I
    :pswitch_2
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 238
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout()V

    .line 240
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z
    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z

    .line 241
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setBackgroundColor(I)V

    goto :goto_0

    .line 244
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z
    invoke-static {v4, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z

    .line 245
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v4, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setBackgroundColor(I)V

    goto :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
