.class Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$6;
.super Ljava/lang/Object;
.source "SCoverPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$6;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 191
    const-string v0, "SCoverPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mInfoListener. info = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " / "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    sparse-switch p2, :sswitch_data_0

    .line 203
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 198
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$6;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->createErrorDialog(I)V
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$700(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;I)V

    goto :goto_0

    .line 193
    nop

    :sswitch_data_0
    .sparse-switch
        0x3b6 -> :sswitch_0
        0x3b7 -> :sswitch_0
        0x3cc -> :sswitch_0
        0x3cd -> :sswitch_0
    .end sparse-switch
.end method
