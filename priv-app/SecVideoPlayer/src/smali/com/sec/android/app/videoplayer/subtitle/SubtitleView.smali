.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
.super Landroid/widget/RelativeLayout;
.source "SubtitleView.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final BOTTOM:I

.field private final HANDLE_UPDATE_LAYOUT:I

.field private final LAYOUT_CONTROLLER_MODE:I

.field private final LAYOUT_DEFAULT_MODE:I

.field private final LAYOUT_FLOATING_MODE:I

.field private final LAYOUT_FULL_SCREEN_MODE:I

.field private final LAYOUT_PLAYERLIST_MODE:I

.field private final LAYOUT_TV_OUT_MODE:I

.field private final TOP:I

.field final gestureDetector:Landroid/view/GestureDetector;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ResourceAsColor"
        }
    .end annotation
.end field

.field private isFloatLongPressed:Z

.field private isFloatTouchMoved:Z

.field private mBeginTimeSmpte:Ljava/lang/String;

.field private mCheckSameString:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFloatBottom:I

.field private mFloatSelX:I

.field private mFloatSelY:I

.field private mFloatTop:I

.field private final mHandler:Landroid/os/Handler;

.field private mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

.field private mIsSameBeginTime:Z

.field private mLayoutMode:I

.field private mRect:[I

.field private mSavedSurfaceHeight:I

.field private mSubtitleLayout:Landroid/widget/RelativeLayout;

.field private mSubtitleText:Landroid/widget/TextView;

.field private mSubtitleTextLine:I

.field private mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

.field private mSubtitleView:Landroid/view/View;

.field private onFloatingTouchListener:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 53
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mBeginTimeSmpte:Ljava/lang/String;

    .line 55
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mCheckSameString:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    .line 74
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TOP:I

    .line 76
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->BOTTOM:I

    .line 78
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    .line 83
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_DEFAULT_MODE:I

    .line 85
    iput v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FULL_SCREEN_MODE:I

    .line 87
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_TV_OUT_MODE:I

    .line 89
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FLOATING_MODE:I

    .line 91
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_CONTROLLER_MODE:I

    .line 93
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_PLAYERLIST_MODE:I

    .line 95
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->HANDLE_UPDATE_LAYOUT:I

    .line 97
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mLayoutMode:I

    .line 99
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I

    .line 101
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 176
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->gestureDetector:Landroid/view/GestureDetector;

    .line 203
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->onFloatingTouchListener:Landroid/view/View$OnTouchListener;

    .line 840
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initView()V

    .line 106
    return-void

    .line 78
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 109
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 53
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mBeginTimeSmpte:Ljava/lang/String;

    .line 55
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mCheckSameString:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    .line 74
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TOP:I

    .line 76
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->BOTTOM:I

    .line 78
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    .line 83
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_DEFAULT_MODE:I

    .line 85
    iput v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FULL_SCREEN_MODE:I

    .line 87
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_TV_OUT_MODE:I

    .line 89
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FLOATING_MODE:I

    .line 91
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_CONTROLLER_MODE:I

    .line 93
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_PLAYERLIST_MODE:I

    .line 95
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->HANDLE_UPDATE_LAYOUT:I

    .line 97
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mLayoutMode:I

    .line 99
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I

    .line 101
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 176
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->gestureDetector:Landroid/view/GestureDetector;

    .line 203
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->onFloatingTouchListener:Landroid/view/View$OnTouchListener;

    .line 840
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    .line 110
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initView()V

    .line 111
    return-void

    .line 78
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    .line 47
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 51
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 53
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mBeginTimeSmpte:Ljava/lang/String;

    .line 55
    const-string v0, "nostring"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mCheckSameString:Ljava/lang/String;

    .line 57
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    .line 59
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 61
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    .line 74
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TOP:I

    .line 76
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->BOTTOM:I

    .line 78
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    .line 83
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_DEFAULT_MODE:I

    .line 85
    iput v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FULL_SCREEN_MODE:I

    .line 87
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_TV_OUT_MODE:I

    .line 89
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_FLOATING_MODE:I

    .line 91
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_CONTROLLER_MODE:I

    .line 93
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->LAYOUT_PLAYERLIST_MODE:I

    .line 95
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->HANDLE_UPDATE_LAYOUT:I

    .line 97
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mLayoutMode:I

    .line 99
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I

    .line 101
    iput v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 176
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->gestureDetector:Landroid/view/GestureDetector;

    .line 203
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->onFloatingTouchListener:Landroid/view/View$OnTouchListener;

    .line 840
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    .line 115
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initView()V

    .line 116
    return-void

    .line 78
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    return p1
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelX:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelX:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelY:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatSelY:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->layoutMove(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .prologue
    .line 42
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p1, "x1"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleTextLine:I

    return p1
.end method

.method private getControllerHeight()I
    .locals 4

    .prologue
    const v2, 0x7f080192

    .line 395
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 397
    .local v0, "mainView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isAdditionalControlButtonShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 398
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800fe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 407
    :goto_0
    return v1

    .line 401
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08017e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08017d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    goto :goto_0

    .line 404
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 405
    const/4 v1, 0x0

    goto :goto_0

    .line 407
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08015a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08015b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method private getSetLayoutMode()I
    .locals 2

    .prologue
    .line 422
    const/4 v0, 0x1

    .line 424
    .local v0, "mode":I
    sget-boolean v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-eqz v1, :cond_0

    .line 425
    const/4 v0, 0x3

    .line 437
    :goto_0
    return v0

    .line 426
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    if-eqz v1, :cond_1

    .line 427
    const/4 v0, 0x4

    goto :goto_0

    .line 428
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 429
    const/4 v0, 0x6

    goto :goto_0

    .line 430
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isSetLayoutFullScreenCase()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 431
    const/4 v0, 0x5

    goto :goto_0

    .line 432
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isSetLayoutFullScreenCase()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v1

    if-nez v1, :cond_4

    .line 433
    const/4 v0, 0x2

    goto :goto_0

    .line 435
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSurfaceHeight()I
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 375
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHeight()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHeight()I

    move-result v1

    .line 376
    .local v1, "surfaceHeight":I
    :goto_0
    if-ne v1, v2, :cond_0

    .line 377
    iget v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    if-eq v3, v2, :cond_3

    .line 378
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 384
    :cond_0
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v0, v2, v3

    .line 386
    .local v0, "screenWidth":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceWidth()I

    move-result v2

    if-le v2, v0, :cond_1

    .line 387
    mul-int v2, v0, v1

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceWidth()I

    move-result v3

    div-int v1, v2, v3

    .line 390
    :cond_1
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 391
    return v1

    .end local v0    # "screenWidth":I
    .end local v1    # "surfaceHeight":I
    :cond_2
    move v1, v2

    .line 375
    goto :goto_0

    .line 380
    .restart local v1    # "surfaceHeight":I
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080055

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    goto :goto_1
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    .line 121
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleView(Landroid/content/Context;)V

    .line 123
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayoutDefaultPosition()V

    .line 124
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setSubtitlePrefDataChangedListener(Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr$OnSubtitlePrefChangeListener;)V

    .line 125
    return-void
.end method

.method private isSetLayoutFullScreenCase()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 413
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v0

    .line 414
    .local v0, "screenMode":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceHeight()I

    move-result v2

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceWidth()I

    move-result v3

    if-gt v2, v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private layoutMove(II)V
    .locals 5
    .param p1, "top"    # I
    .param p2, "bottom"    # I

    .prologue
    const/4 v4, 0x0

    .line 258
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    .line 259
    iput p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    .line 261
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 263
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 265
    .local v0, "deviceHeight":I
    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    if-gtz v2, :cond_0

    .line 266
    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    .line 269
    :cond_0
    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getHeight()I

    move-result v3

    sub-int v3, v0, v3

    if-lt v2, v3, :cond_1

    .line 270
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    .line 273
    :cond_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getHeight()I

    move-result v3

    if-gt v2, v3, :cond_2

    .line 274
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    .line 277
    :cond_2
    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    if-lt v2, v0, :cond_3

    .line 278
    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    .line 281
    :cond_3
    invoke-virtual {p0, v4, p1, v4, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->layoutChange(IIII)V

    .line 282
    return-void
.end method

.method private makeView(Landroid/content/Context;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    iget-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    .line 138
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initTextView(Landroid/content/Context;)V

    .line 139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    return-object v0
.end method

.method private setHeadStyleSubtitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "headString"    # Ljava/lang/String;

    .prologue
    .line 540
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v1, "setHeadStyleSubtitle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 542
    if-eqz p1, :cond_0

    .line 543
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    .line 544
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    .line 545
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->parseTag(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 547
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    if-eqz v0, :cond_1

    .line 549
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setStyleSmpte(I)V

    .line 551
    :cond_1
    return-void
.end method

.method private setPtagStyleSmpte(Ljava/lang/String;)V
    .locals 20
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 612
    new-instance v8, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-direct {v8}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;-><init>()V

    .line 613
    .local v8, "p_parser":Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;
    move-object/from16 v0, p1

    invoke-virtual {v8, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->parseTag(Ljava/lang/String;)Ljava/util/ArrayList;

    .line 614
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->printAllNode(Z)V

    .line 617
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 618
    .local v2, "beforeSpanText":Landroid/text/SpannableStringBuilder;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 620
    new-instance v9, Landroid/text/SpannableStringBuilder;

    invoke-direct {v9}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 621
    .local v9, "spanText":Landroid/text/SpannableStringBuilder;
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 624
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_c

    .line 625
    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [Ljava/lang/String;

    .line 628
    .local v12, "tempParsedArray":[Ljava/lang/String;
    const/16 v17, 0x3

    aget-object v17, v12, v17

    if-eqz v17, :cond_1

    .line 629
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mBeginTimeSmpte:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x3

    aget-object v18, v12, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 630
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 632
    :cond_0
    const/16 v17, 0x3

    aget-object v17, v12, v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mBeginTimeSmpte:Ljava/lang/String;

    .line 636
    :cond_1
    const/16 v17, 0x2

    aget-object v14, v12, v17

    .line 637
    .local v14, "tempStyle":Ljava/lang/String;
    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    if-eqz v17, :cond_2

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [Ljava/lang/String;

    const/16 v18, 0x1

    aget-object v17, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 653
    :cond_2
    const/16 v17, 0x0

    aget-object v15, v12, v17

    .line 654
    .local v15, "tempText":Ljava/lang/String;
    const/4 v3, 0x0

    .line 655
    .local v3, "beginIndex":I
    const/4 v5, 0x0

    .line 658
    .local v5, "endIndex":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 659
    invoke-virtual {v9, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 660
    const-string v17, "\r\n"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 661
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mIsSameBeginTime:Z

    .line 664
    :cond_3
    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    .line 667
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->isContainString(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 668
    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    .line 669
    invoke-virtual {v9, v15}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 672
    :cond_4
    invoke-virtual {v9}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    .line 675
    const/16 v17, 0xd

    aget-object v13, v12, v17

    .line 676
    .local v13, "tempSize":Ljava/lang/String;
    if-eqz v13, :cond_5

    .line 677
    new-instance v17, Landroid/text/style/AbsoluteSizeSpan;

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v18

    invoke-direct/range {v17 .. v18}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/16 v18, 0x21

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v3, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 682
    :cond_5
    const/16 v17, 0x6

    aget-object v10, v12, v17

    .line 683
    .local v10, "tempBackground":Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 684
    new-instance v17, Landroid/text/style/BackgroundColorSpan;

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v18

    invoke-direct/range {v17 .. v18}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v18, 0x21

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v3, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 690
    :cond_6
    const/16 v17, 0x7

    aget-object v16, v12, v17

    .line 691
    .local v16, "tempTextColor":Ljava/lang/String;
    if-eqz v16, :cond_7

    .line 692
    new-instance v17, Landroid/text/style/ForegroundColorSpan;

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v18

    invoke-direct/range {v17 .. v18}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v18, 0x21

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v3, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 698
    :cond_7
    const/16 v17, 0x11

    aget-object v11, v12, v17

    .line 699
    .local v11, "tempOpacity":Ljava/lang/String;
    if-eqz v11, :cond_9

    .line 700
    if-eqz v10, :cond_8

    .line 701
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v17

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToFloat(Ljava/lang/String;)F

    move-result v18

    const/high16 v19, 0x42c80000    # 100.0f

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v4

    .line 704
    .local v4, "color":I
    new-instance v17, Landroid/text/style/BackgroundColorSpan;

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v18, 0x21

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v3, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 707
    .end local v4    # "color":I
    :cond_8
    if-eqz v16, :cond_9

    .line 708
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v17

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToFloat(Ljava/lang/String;)F

    move-result v18

    const/high16 v19, 0x42c80000    # 100.0f

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v4

    .line 710
    .restart local v4    # "color":I
    new-instance v17, Landroid/text/style/ForegroundColorSpan;

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v18, 0x21

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v3, v5, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 624
    .end local v4    # "color":I
    :cond_9
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 643
    .end local v3    # "beginIndex":I
    .end local v5    # "endIndex":I
    .end local v10    # "tempBackground":Ljava/lang/String;
    .end local v11    # "tempOpacity":Ljava/lang/String;
    .end local v13    # "tempSize":Ljava/lang/String;
    .end local v15    # "tempText":Ljava/lang/String;
    .end local v16    # "tempTextColor":Ljava/lang/String;
    :cond_a
    const/4 v7, 0x1

    .local v7, "j":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v7, v0, :cond_2

    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getParsedArray()Ljava/util/ArrayList;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, [Ljava/lang/String;

    const/16 v18, 0x1

    aget-object v17, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 646
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleForSmpte()V

    .line 647
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setStyleSmpte(I)V

    .line 643
    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 716
    .end local v7    # "j":I
    .end local v12    # "tempParsedArray":[Ljava/lang/String;
    .end local v14    # "tempStyle":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 717
    return-void
.end method

.method private setStyleSmpte(I)V
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v6, 0x0

    const v5, -0xf423f

    .line 554
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setStyleSmpte mHeadParser.getStyleFontSize(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleFontSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleFontSize(I)I

    move-result v2

    const v3, -0x989676

    if-eq v2, v3, :cond_0

    .line 557
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleFontSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 561
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setStyleSmpte mHeadParser.getStyleTextBackgroundColor(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextBackgroundColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextBackgroundColor(I)I

    move-result v2

    if-eq v2, v5, :cond_1

    .line 564
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextBackgroundColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 568
    :cond_1
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setStyleSmpte mHeadParser.getStyleTextColor(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextColor(I)I

    move-result v2

    if-eq v2, v5, :cond_2

    .line 571
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 575
    :cond_2
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setStyleSmpte mHeadParser.getStyleOpacity(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleOpacity(I)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextBackgroundColor(I)I

    move-result v2

    if-eq v2, v5, :cond_3

    .line 578
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextBackgroundColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleOpacity(I)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 579
    .local v0, "color":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 582
    .end local v0    # "color":I
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextColor(I)I

    move-result v2

    if-eq v2, v5, :cond_4

    .line 583
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleTextColor(I)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getStyleOpacity(I)F

    move-result v3

    float-to-int v3, v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 584
    .restart local v0    # "color":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 588
    .end local v0    # "color":I
    :cond_4
    const/4 v1, 0x0

    .line 589
    .local v1, "tempFontFamily":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getFontFamily(I)Ljava/lang/String;

    move-result-object v1

    .line 590
    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setStyleSmpte mHeadParser.getFontFamily(): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHeadParser:Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;

    invoke-virtual {v4, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->getFontFamily(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    if-eqz v1, :cond_5

    .line 593
    const-string v2, "monospace"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 594
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 609
    :cond_5
    :goto_0
    return-void

    .line 595
    :cond_6
    const-string v2, "proportionalSansSerif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 596
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 597
    :cond_7
    const-string v2, "monospaceSerif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 598
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 599
    :cond_8
    const-string v2, "undefined"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 600
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 601
    :cond_9
    const-string v2, "SansSerif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 602
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 603
    :cond_a
    const-string v2, "monospaceSansSerif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 604
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->MONOSPACE:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    .line 605
    :cond_b
    const-string v2, "proportionalSerif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 606
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v3, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0
.end method

.method private updateFont()V
    .locals 2

    .prologue
    .line 479
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSelectedTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 480
    .local v0, "typeface":Landroid/graphics/Typeface;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 481
    return-void
.end method

.method private updateFontBGColorOpacity()V
    .locals 7

    .prologue
    .line 720
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGColor()I

    move-result v0

    .line 721
    .local v0, "color":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getText()Ljava/lang/String;

    move-result-object v1

    .line 723
    .local v1, "text":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 731
    :goto_0
    return-void

    .line 727
    :cond_0
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 728
    .local v2, "textSpan":Landroid/text/Spannable;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 729
    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v3, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-interface {v2, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 730
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateFontColorOpacity(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 734
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontOpacity()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result p1

    .line 735
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 736
    return-void
.end method

.method private updateFontSize(II)V
    .locals 3
    .param p1, "size"    # I
    .param p2, "delta"    # I

    .prologue
    const/4 v2, 0x1

    .line 469
    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-ne v0, v2, :cond_0

    .line 470
    add-int/lit8 v0, p1, 0x6

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p1, v0

    .line 475
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 476
    return-void

    .line 472
    :cond_0
    add-int/2addr p1, p2

    goto :goto_0
.end method


# virtual methods
.method public initSubtitleForSmpte()V
    .locals 2

    .prologue
    .line 532
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v1, "initSubtitleForSmpte"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAllTextEffect()V

    .line 534
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 537
    return-void
.end method

.method public initSubtitleView(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->removeAllViews()V

    .line 129
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->makeView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    .line 130
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->addView(Landroid/view/View;)V

    .line 131
    return-void
.end method

.method public initTextView(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    const-string v6, "layout_inflater"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 144
    .local v2, "inflate":Landroid/view/LayoutInflater;
    const v6, 0x7f030032

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 145
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    const v7, 0x7f0d01a6

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    .line 146
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    const v7, 0x7f0d01a5

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout;

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    .line 149
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080286

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v5, v6

    .line 151
    .local v5, "text_padding":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v6, v5, v5, v5, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 153
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontColor()I

    move-result v3

    .line 154
    .local v3, "tColor":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontOpacity()I

    move-result v4

    .line 155
    .local v4, "tOpacity":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v1

    .line 156
    .local v1, "fontSize":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f09000f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 158
    .local v0, "delta":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    .line 159
    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontSize(II)V

    .line 161
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getTextAlignment()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateSubTitleAlignment(I)V

    .line 162
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontEdge()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontEdge(I)V

    .line 163
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFont()V

    .line 165
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 170
    .end local v0    # "delta":I
    .end local v1    # "fontSize":I
    .end local v3    # "tColor":I
    .end local v4    # "tOpacity":I
    .end local v5    # "text_padding":I
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 171
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->onFloatingTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 173
    :cond_1
    return-void
.end method

.method public isFloatLongPress()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatLongPressed:Z

    return v0
.end method

.method public layoutChange(IIII)V
    .locals 3
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    const/4 v2, 0x0

    .line 254
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatBottom:I

    invoke-super {p0, v2, v0, v2, v1}, Landroid/widget/RelativeLayout;->layout(IIII)V

    .line 255
    return-void
.end method

.method public onSubtitlePrefDataChanged(I)V
    .locals 4
    .param p1, "selected"    # I

    .prologue
    .line 783
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSubtitlePrefDataChanged. selected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 785
    packed-switch p1, :pswitch_data_0

    .line 838
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 790
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontEdge()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontEdge(I)V

    goto :goto_0

    .line 794
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getTextAlignment()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateSubTitleAlignment(I)V

    goto :goto_0

    .line 799
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontColor()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontColorOpacity(I)V

    goto :goto_0

    .line 804
    :pswitch_4
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontBGColorOpacity()V

    goto :goto_0

    .line 809
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinColor()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateCaptionWinColorOpacity(I)V

    goto :goto_0

    .line 813
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 814
    .local v0, "sizeDelta":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v1, :cond_0

    .line 816
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateMWFontSize()V

    goto :goto_0

    .line 819
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontSize(II)V

    goto :goto_0

    .line 824
    .end local v0    # "sizeDelta":I
    :pswitch_7
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFont()V

    goto :goto_0

    .line 827
    :pswitch_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSplitSubtitleActive()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 828
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->showSplitSubtitle()V

    goto :goto_0

    .line 830
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->hideSplitSubtitle()V

    goto :goto_0

    .line 785
    nop

    :pswitch_data_0
    .packed-switch 0x190
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 744
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 745
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleView:Landroid/view/View;

    .line 746
    return-void
.end method

.method public setLayout()V
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout(Z)V

    .line 294
    return-void
.end method

.method public setLayout(Z)V
    .locals 12
    .param p1, "forceUpdate"    # Z

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 301
    const/4 v7, 0x2

    new-array v4, v7, [I

    fill-array-data v4, :array_0

    .line 302
    .local v4, "rect":[I
    const/16 v0, 0xa

    .line 303
    .local v0, "addRules":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getSetLayoutMode()I

    move-result v2

    .line 305
    .local v2, "layoutMode":I
    sget-object v7, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setLayout : E mode:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    packed-switch v2, :pswitch_data_0

    .line 350
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->stackTrace()V

    .line 372
    :goto_0
    return-void

    .line 309
    :pswitch_0
    const/16 v0, 0xc

    .line 310
    const/16 v7, 0x18

    aput v7, v4, v11

    .line 354
    :cond_0
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    aget v8, v4, v10

    aput v8, v7, v10

    .line 355
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    aget v8, v4, v11

    aput v8, v7, v11

    .line 356
    iput v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mLayoutMode:I

    .line 358
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 359
    .local v3, "params":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v3, :cond_1

    .line 360
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v3    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v3, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 368
    .restart local v3    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 369
    aget v7, v4, v10

    aget v8, v4, v11

    invoke-virtual {v3, v10, v7, v10, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 370
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    sget-object v7, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setLayout Updated : TOP:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / BOTTOM:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mRect:[I

    aget v9, v9, v11

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " / Rule:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    .end local v3    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :pswitch_1
    const/16 v0, 0xa

    .line 315
    iget v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mFloatTop:I

    aput v7, v4, v10

    .line 316
    aput v10, v4, v11

    goto :goto_1

    .line 320
    :pswitch_2
    const/16 v0, 0xc

    .line 321
    aput v10, v4, v11

    goto :goto_1

    .line 325
    :pswitch_3
    const/16 v0, 0xc

    .line 326
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getControllerHeight()I

    move-result v7

    aput v7, v4, v11

    goto :goto_1

    .line 330
    :pswitch_4
    const/16 v0, 0xa

    .line 331
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getSurfaceHeight()I

    move-result v6

    .line 332
    .local v6, "surfaceHeight":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v7

    aget v5, v7, v11

    .line 333
    .local v5, "screenHeight":I
    sub-int v7, v5, v6

    div-int/lit8 v1, v7, 0x2

    .line 334
    .local v1, "darkAreaHeight":I
    add-int v7, v1, v6

    aput v7, v4, v10

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getHeight()I

    move-result v7

    if-lt v7, v1, :cond_0

    .line 337
    const/4 v2, 0x2

    .line 338
    const/16 v0, 0xc

    .line 339
    aput v10, v4, v11

    .line 340
    aput v10, v4, v10

    .line 341
    sget-object v7, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v8, "setLayout : textHeight is bigger than darkAreaHeight, change to full screen mode"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 345
    .end local v1    # "darkAreaHeight":I
    .end local v5    # "screenHeight":I
    .end local v6    # "surfaceHeight":I
    :pswitch_5
    const/16 v0, 0xc

    .line 346
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getControllerHeight()I

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f08015c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    add-int/2addr v7, v8

    aput v7, v4, v11

    goto/16 :goto_1

    .line 363
    .restart local v3    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_1
    const/16 v7, 0xd

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 364
    const/16 v7, 0xa

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 365
    const/16 v7, 0xc

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto/16 :goto_2

    .line 301
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 307
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public setLayoutDefaultPosition()V
    .locals 2

    .prologue
    .line 286
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v1, "setLayoutDefaultPosition : E "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->isFloatTouchMoved:Z

    .line 288
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSavedSurfaceHeight:I

    .line 289
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout(Z)V

    .line 290
    return-void
.end method

.method public setSubtitleView()V
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSubtitleView(Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;)V

    .line 485
    return-void
.end method

.method public showSubtitleText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "headString"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x64

    .line 488
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showSubtitleText E text = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    const/4 v2, 0x0

    .line 491
    .local v2, "visibility":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 493
    :cond_1
    const/4 v2, 0x4

    .line 525
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 526
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 528
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 529
    return-void

    .line 495
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFileType()I

    move-result v3

    const/16 v4, 0x68

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSMPTETTCustomMode()Z

    move-result v3

    if-nez v3, :cond_4

    .line 498
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->initSubtitleForSmpte()V

    .line 501
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setHeadStyleSubtitle(Ljava/lang/String;)V

    .line 504
    if-eqz p1, :cond_3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mCheckSameString:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 505
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setPtagStyleSmpte(Ljava/lang/String;)V

    .line 509
    :goto_1
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mCheckSameString:Ljava/lang/String;

    goto :goto_0

    .line 507
    :cond_3
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v4, "showSubtitleText smpte same text skip!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 512
    :cond_4
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinColor()I

    move-result v0

    .line 513
    .local v0, "color":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 514
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 517
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGColor()I

    move-result v0

    .line 518
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 519
    .local v1, "textSpan":Landroid/text/Spannable;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontBGOpacity()I

    move-result v3

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result v0

    .line 520
    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v3, v0}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0x21

    invoke-interface {v1, v3, v4, v5, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 521
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public updateCaptionWinColorOpacity(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 739
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getCaptionWinOpacity()I

    move-result v0

    invoke-static {p1, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->applyOpacity(II)I

    move-result p1

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 741
    return-void
.end method

.method public updateFontEdge(I)V
    .locals 3
    .param p1, "edge"    # I

    .prologue
    .line 749
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateFontEdge() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 756
    :goto_0
    return-void

    .line 755
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontEdge(Landroid/widget/TextView;IZ)V

    goto :goto_0
.end method

.method public updateMWFontSize(II)V
    .locals 6
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 443
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_0

    .line 444
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    const-string v4, "updateMWFontSize() return"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :goto_0
    return-void

    .line 448
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 449
    int-to-float v3, p1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v4

    const/4 v5, 0x0

    aget v4, v4, v5

    int-to-float v4, v4

    div-float v0, v3, v4

    .line 454
    .local v0, "ratio":F
    :goto_1
    const/high16 v3, 0x41f00000    # 30.0f

    mul-float/2addr v3, v0

    float-to-int v2, v3

    .line 456
    .local v2, "textSize":I
    const/16 v3, 0xf

    if-ge v2, v3, :cond_1

    .line 457
    const/16 v2, 0xf

    .line 459
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 460
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getFontSize()I

    move-result v2

    .line 463
    :cond_2
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateMWFontSize() textSize = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 465
    .local v1, "sizeDelta":I
    invoke-direct {p0, v2, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->updateFontSize(II)V

    goto :goto_0

    .line 451
    .end local v0    # "ratio":F
    .end local v1    # "sizeDelta":I
    .end local v2    # "textSize":I
    :cond_3
    int-to-float v3, p2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenWidthHeight(Landroid/content/Context;)[I

    move-result-object v4

    const/4 v5, 0x1

    aget v4, v4, v5

    int-to-float v4, v4

    div-float v0, v3, v4

    .restart local v0    # "ratio":F
    goto :goto_1
.end method

.method public updateSubTitleAlignment(I)V
    .locals 5
    .param p1, "alignment"    # I

    .prologue
    const/16 v4, 0xb

    const/16 v3, 0x9

    .line 759
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getGravity()I

    move-result v2

    and-int/lit8 v1, v2, -0x8

    .line 760
    .local v1, "textGravity":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 762
    .local v0, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v2, 0x1e

    if-ne p1, v2, :cond_1

    .line 763
    or-int/lit8 v1, v1, 0x3

    .line 764
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 765
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 776
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 777
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 778
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->mSubtitleText:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->refreshDrawableState()V

    .line 779
    return-void

    .line 766
    :cond_1
    const/16 v2, 0x1f

    if-ne p1, v2, :cond_2

    .line 767
    or-int/lit8 v1, v1, 0x1

    .line 768
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 769
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_0

    .line 770
    :cond_2
    const/16 v2, 0x20

    if-ne p1, v2, :cond_0

    .line 771
    or-int/lit8 v1, v1, 0x5

    .line 772
    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 773
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_0
.end method
