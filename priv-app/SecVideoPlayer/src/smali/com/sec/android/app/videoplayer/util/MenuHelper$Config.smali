.class public final enum Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;
.super Ljava/lang/Enum;
.source "MenuHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/MenuHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

.field public static final enum EXCEPTIONAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

.field public static final enum NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37
    new-instance v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    new-instance v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    const-string v1, "EXCEPTIONAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->EXCEPTIONAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->NORMAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->EXCEPTIONAL:Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->$VALUES:[Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    const-class v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->$VALUES:[Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/util/MenuHelper$Config;

    return-object v0
.end method
