.class Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;
.super Ljava/lang/Object;
.source "PlayerListView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V
    .locals 0

    .prologue
    .line 520
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 529
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mPlayerListShowState:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$502(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;Z)Z

    .line 530
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # invokes: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->resetPlayerLayout()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$600(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)V

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView$7;->this$0:Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;->access$200(Lcom/sec/android/app/videoplayer/playerlist/PlayerListView;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    .line 536
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 525
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 522
    return-void
.end method
