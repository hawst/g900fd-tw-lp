.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;
.super Landroid/database/ContentObserver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 6035
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6037
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v2

    const-string v3, "registerContentObserver mUserRotationObserver onChange()"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6039
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->PLAYING_ON_VZW_GUIDE_TOUR:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGuidedTour()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 6040
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "registerContentObserver mUserRotationObserver onChange() DONOT UPDATE FOR Came from VZW Guide Tour!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 6054
    :goto_0
    return-void

    .line 6042
    :cond_0
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_4

    .line 6043
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 6044
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 6047
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateSecondScreen(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2

    .line 6051
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$44;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getUserRotationModefromAssistantMenu()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0
.end method
