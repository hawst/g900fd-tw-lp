.class Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;
.super Ljava/lang/Object;
.source "Sensor360.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/Video360/Sensor360;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CalculateFusedOrientationTask"
.end annotation


# static fields
.field public static final FILTER_COEFFICIENT:F = 0.98f

.field public static final ONE_MINUS_FILTER_COEFFICIENT:F = 0.01999998f

.field public static final PI:F = 3.1415927f

.field public static final RAD2DEG:F = 57.295776f


# instance fields
.field private fusedOrientation:[F


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 314
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->fusedOrientation:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/Video360/Sensor360$1;

    .prologue
    .line 307
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v5, 0x0

    const v13, 0x42652ee0

    .line 319
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mat3ToQuat([F)Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    move-result-object v9

    .line 320
    .local v9, "gQuat":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sAccMagMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$900()[F

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->mat3ToQuat([F)Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    move-result-object v0

    .line 322
    .local v0, "amQuat":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->normalize()V

    .line 323
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->normalize()V

    .line 325
    const v1, 0x3ca3d700    # 0.01999998f

    invoke-static {v9, v0, v1}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->slerp(Lcom/sec/android/app/videoplayer/Video360/Quaternion;Lcom/sec/android/app/videoplayer/Video360/Quaternion;F)Lcom/sec/android/app/videoplayer/Video360/Quaternion;

    move-result-object v7

    .line 326
    .local v7, "fused":Lcom/sec/android/app/videoplayer/Video360/Quaternion;
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->normalize()V

    .line 328
    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/Video360/Quaternion;->asMatrix3x3()[F

    move-result-object v8

    .line 330
    .local v8, "fusedMat":[F
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroMatrix:[F
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$800()[F

    move-result-object v4

    array-length v4, v4

    invoke-static {v8, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 331
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->fusedOrientation:[F

    invoke-static {v8, v1}, Landroid/hardware/SensorManager;->getOrientation([F[F)[F

    .line 333
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sGyroTimeStamp:J
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1100()J

    move-result-wide v2

    .line 335
    .local v2, "timeStamp":J
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mObject:Ljava/lang/Object;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1200()Ljava/lang/Object;

    move-result-object v11

    monitor-enter v11

    .line 336
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    :try_start_0
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1300()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v10, v1, :cond_0

    .line 337
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->sRotationListeners:Ljava/util/Vector;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1300()Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->fusedOrientation:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    mul-float/2addr v4, v13

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->fusedOrientation:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    mul-float/2addr v5, v13

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;->fusedOrientation:[F

    const/4 v12, 0x2

    aget v6, v6, v12

    mul-float/2addr v6, v13

    invoke-virtual/range {v1 .. v7}, Lcom/sec/android/app/videoplayer/Video360/Sensor360$RotationListener;->onRotationChanged(JFFFLcom/sec/android/app/videoplayer/Video360/Quaternion;)V

    .line 336
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 343
    :cond_0
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1500()Landroid/os/Handler;

    move-result-object v1

    # getter for: Lcom/sec/android/app/videoplayer/Video360/Sensor360;->mSampleTask:Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;
    invoke-static {}, Lcom/sec/android/app/videoplayer/Video360/Sensor360;->access$1400()Lcom/sec/android/app/videoplayer/Video360/Sensor360$CalculateFusedOrientationTask;

    move-result-object v4

    const-wide/16 v12, 0xf

    invoke-virtual {v1, v4, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 347
    return-void

    .line 343
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
