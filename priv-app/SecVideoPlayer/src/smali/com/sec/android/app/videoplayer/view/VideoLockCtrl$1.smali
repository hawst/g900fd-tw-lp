.class Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;
.super Ljava/lang/Object;
.source "VideoLockCtrl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 50
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 68
    :cond_0
    :goto_0
    :pswitch_0
    return v4

    .line 55
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpg-float v1, v3, v1

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpg-float v1, v3, v1

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->access$000(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->access$000(Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .line 61
    .local v0, "mainVideoView":Lcom/sec/android/app/videoplayer/view/MainVideoView;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
