.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 0

    .prologue
    .line 1050
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v4, 0x7f0d0137

    const/4 v1, 0x1

    const/16 v3, 0xbb8

    const/4 v2, 0x0

    .line 1052
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1098
    :goto_0
    :pswitch_0
    return v2

    .line 1055
    :pswitch_1
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isInformationPreviewEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1056
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 1057
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1067
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->setHapticEffect(Landroid/content/Context;Landroid/view/View;)V

    .line 1070
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto :goto_0

    .line 1060
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020195

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1062
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1063
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1065
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02018b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1074
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto/16 :goto_0

    .line 1078
    :pswitch_3
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v1, :cond_7

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isInformationPreviewEnable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1079
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_9

    .line 1080
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1081
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02019a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1092
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto/16 :goto_0

    .line 1083
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1085
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isTypeUnknown()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1086
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020189

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1088
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1052
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
