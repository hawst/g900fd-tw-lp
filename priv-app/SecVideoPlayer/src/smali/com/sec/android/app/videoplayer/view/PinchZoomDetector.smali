.class public Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;
.super Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
.source "PinchZoomDetector.java"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# static fields
.field private static final NUM_POINTER_ALLOWED_FOR_PINCH:I = 0x2

.field private static final SMOOTH_ZOOM_FACTOR:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PinchZoomDetector"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

.field private mNumberOfPointer:I

.field mScaleGesture:Landroid/view/ScaleGestureDetector;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "CropListener"    # Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;-><init>(Landroid/content/Context;)V

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mScaleGesture:Landroid/view/ScaleGestureDetector;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mNumberOfPointer:I

    .line 47
    const-string v0, "PinchZoomDetector"

    const-string v1, "PinchZoomDetector"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mContext:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    .line 50
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, p1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mScaleGesture:Landroid/view/ScaleGestureDetector;

    .line 51
    return-void
.end method

.method private initZoomParams()V
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->resetCropRect()V

    .line 220
    return-void
.end method

.method private isSurfaceAreaNotTouched(Landroid/view/MotionEvent;I)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "i"    # I

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 77
    .local v0, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getBottom()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getRight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 28
    .param p1, "gesture"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 86
    const/4 v8, 0x0

    .line 96
    .local v8, "consumed":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mNumberOfPointer:I

    move/from16 v24, v0

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_2

    .line 97
    const/4 v8, 0x1

    .line 98
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v20

    .line 99
    .local v20, "newScale":F
    const/high16 v24, 0x3f800000    # 1.0f

    const/high16 v25, 0x3f800000    # 1.0f

    div-float v25, v25, v20

    sub-float v22, v24, v25

    .line 101
    .local v22, "scaleRatio":F
    sget-object v23, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 103
    .local v23, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v16

    .line 104
    .local v16, "nSurfaceViewWidth":I
    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v15

    .line 106
    .local v15, "nSurfaceViewHeight":I
    const-string v24, "PinchZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Current Scale: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    const/high16 v24, 0x3f800000    # 1.0f

    cmpl-float v24, v20, v24

    if-nez v24, :cond_0

    .line 110
    const/16 v24, 0x0

    .line 186
    .end local v15    # "nSurfaceViewHeight":I
    .end local v16    # "nSurfaceViewWidth":I
    .end local v20    # "newScale":F
    .end local v22    # "scaleRatio":F
    .end local v23    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :goto_0
    return v24

    .line 114
    .restart local v15    # "nSurfaceViewHeight":I
    .restart local v16    # "nSurfaceViewWidth":I
    .restart local v20    # "newScale":F
    .restart local v22    # "scaleRatio":F
    .restart local v23    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_0
    const/high16 v24, 0x3f800000    # 1.0f

    sub-float v20, v20, v24

    .line 115
    const/high16 v24, 0x40000000    # 2.0f

    div-float v20, v20, v24

    .line 116
    const/high16 v24, 0x3f800000    # 1.0f

    add-float v20, v20, v24

    .line 126
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v24

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getLeft()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v6, v24, v25

    .line 127
    .local v6, "adjustX":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v24

    invoke-virtual/range {v23 .. v23}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getTop()I

    move-result v25

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    sub-float v7, v24, v25

    .line 129
    .local v7, "adjustY":F
    mul-float v24, v6, v22

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->round(F)I

    move-result v11

    .line 130
    .local v11, "deltaX_Left":I
    mul-float v24, v7, v22

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->round(F)I

    move-result v14

    .line 131
    .local v14, "deltaY_Top":I
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    sub-float v24, v24, v6

    mul-float v24, v24, v22

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->round(F)I

    move-result v12

    .line 132
    .local v12, "deltaX_Right":I
    int-to-float v0, v15

    move/from16 v24, v0

    sub-float v24, v24, v7

    mul-float v24, v24, v22

    invoke-static/range {v24 .. v24}, Ljava/lang/Math;->round(F)I

    move-result v13

    .line 135
    .local v13, "deltaY_Bottom":I
    if-eqz v11, :cond_1

    if-eqz v14, :cond_1

    .line 138
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getCropRect()Landroid/graphics/Rect;

    move-result-object v9

    .line 139
    .local v9, "cropRect":Landroid/graphics/Rect;
    const-string v24, "PinchZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Crop Rect l:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " T:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " R:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " B:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    add-int v24, v24, v11

    move/from16 v0, v24

    iput v0, v9, Landroid/graphics/Rect;->left:I

    .line 143
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    add-int v24, v24, v14

    move/from16 v0, v24

    iput v0, v9, Landroid/graphics/Rect;->top:I

    .line 144
    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    sub-int v24, v24, v12

    move/from16 v0, v24

    iput v0, v9, Landroid/graphics/Rect;->right:I

    .line 145
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    sub-int v24, v24, v13

    move/from16 v0, v24

    iput v0, v9, Landroid/graphics/Rect;->bottom:I

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->adjustBoundary(Landroid/graphics/Rect;)V

    .line 149
    iget v0, v9, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    .line 150
    .local v18, "newLeft":I
    iget v0, v9, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    .line 151
    .local v21, "newTop":I
    iget v0, v9, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    .line 152
    .local v19, "newRight":I
    iget v0, v9, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    .line 154
    .local v17, "newBottom":I
    const-string v24, "PinchZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "New Rect l:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " T:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " R:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " B:"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    sub-int v24, v19, v18

    sub-int v25, v17, v21

    mul-int v5, v24, v25

    .line 157
    .local v5, "Area":I
    const-string v24, "PinchZoomDetector"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "## Area "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->withinScaleBounds(I)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 162
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->getZoomPercent(I)F

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v25, v0

    invoke-interface/range {v25 .. v25}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getZoomPercentWithMargin()F

    move-result v25

    cmpl-float v24, v24, v25

    if-lez v24, :cond_3

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    new-instance v25, Landroid/graphics/Rect;

    move-object/from16 v0, v25

    move/from16 v1, v18

    move/from16 v2, v21

    move/from16 v3, v19

    move/from16 v4, v17

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-interface/range {v24 .. v25}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->applyCrop(Landroid/graphics/Rect;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->getDelta()Landroid/graphics/Point;

    move-result-object v10

    .line 166
    .local v10, "deltaXY":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    new-instance v25, Landroid/graphics/Point;

    iget v0, v10, Landroid/graphics/Point;->x:I

    move/from16 v26, v0

    add-int v26, v26, v11

    iget v0, v10, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    add-int v27, v27, v14

    invoke-direct/range {v25 .. v27}, Landroid/graphics/Point;-><init>(II)V

    invoke-interface/range {v24 .. v25}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->setDelta(Landroid/graphics/Point;)V

    .line 181
    .end local v5    # "Area":I
    .end local v9    # "cropRect":Landroid/graphics/Rect;
    .end local v10    # "deltaXY":Landroid/graphics/Point;
    .end local v17    # "newBottom":I
    .end local v18    # "newLeft":I
    .end local v19    # "newRight":I
    .end local v21    # "newTop":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->isGestureTracked()Z

    move-result v24

    if-nez v24, :cond_2

    .line 182
    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->setGestureBeingTracked(Z)V

    .end local v6    # "adjustX":F
    .end local v7    # "adjustY":F
    .end local v11    # "deltaX_Left":I
    .end local v12    # "deltaX_Right":I
    .end local v13    # "deltaY_Bottom":I
    .end local v14    # "deltaY_Top":I
    .end local v15    # "nSurfaceViewHeight":I
    .end local v16    # "nSurfaceViewWidth":I
    .end local v20    # "newScale":F
    .end local v22    # "scaleRatio":F
    .end local v23    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_2
    move/from16 v24, v8

    .line 186
    goto/16 :goto_0

    .line 169
    .restart local v5    # "Area":I
    .restart local v6    # "adjustX":F
    .restart local v7    # "adjustY":F
    .restart local v9    # "cropRect":Landroid/graphics/Rect;
    .restart local v11    # "deltaX_Left":I
    .restart local v12    # "deltaX_Right":I
    .restart local v13    # "deltaY_Bottom":I
    .restart local v14    # "deltaY_Top":I
    .restart local v15    # "nSurfaceViewHeight":I
    .restart local v16    # "nSurfaceViewWidth":I
    .restart local v17    # "newBottom":I
    .restart local v18    # "newLeft":I
    .restart local v19    # "newRight":I
    .restart local v20    # "newScale":F
    .restart local v21    # "newTop":I
    .restart local v22    # "scaleRatio":F
    .restart local v23    # "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->reset()V

    move/from16 v24, v8

    .line 170
    goto/16 :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 193
    const-string v0, "PinchZoomDetector"

    const-string v1, " onScaleBegin "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->updateVideoFrameProperties()V

    .line 196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->updateVideoFrameData()V

    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/ScaleGestureDetector;

    .prologue
    .line 213
    const-string v0, "PinchZoomDetector"

    const-string v1, " onScaleEnd "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->setGestureBeingTracked(Z)V

    .line 215
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 55
    const/4 v0, 0x0

    .line 56
    .local v0, "eventHandled":Z
    const-string v2, "PinchZoomDetector"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onTouchEvent- Is Panning: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mCropListener:Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;

    invoke-interface {v4}, Lcom/sec/android/app/videoplayer/view/ZoomCropInterface;->isPanEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mNumberOfPointer:I

    .line 61
    iget v2, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mNumberOfPointer:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 62
    invoke-direct {p0, p1, v1}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->isSurfaceAreaNotTouched(Landroid/view/MotionEvent;I)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->isSurfaceAreaNotTouched(Landroid/view/MotionEvent;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v1

    .line 68
    :cond_1
    const-string v1, "PinchZoomDetector"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTouchEvent pointer event: Consume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->mScaleGesture:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    move v1, v0

    .line 71
    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 225
    const-string v0, "PinchZoomDetector"

    const-string v1, " reset "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    invoke-super {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->reset()V

    .line 227
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->initZoomParams()V

    .line 228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/PinchZoomDetector;->setGestureBeingTracked(Z)V

    .line 229
    return-void
.end method
