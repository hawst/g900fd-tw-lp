.class public Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;
.super Ljava/lang/Object;
.source "BitmapAtTimeMsg.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMeasured:Landroid/graphics/Rect;

.field private mPath:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "s"    # Landroid/net/Uri;
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mUri:Landroid/net/Uri;

    .line 24
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mContext:Landroid/content/Context;

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v1, 0x0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mPath:Ljava/lang/String;

    .line 15
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, p2, p3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    .line 16
    return-void
.end method


# virtual methods
.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;->mMeasured:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method
