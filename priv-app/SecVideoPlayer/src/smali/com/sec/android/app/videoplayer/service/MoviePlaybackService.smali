.class public Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
.super Landroid/app/Service;
.source "MoviePlaybackService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;,
        Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;,
        Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;,
        Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$LocalBinder;
    }
.end annotation


# static fields
.field private static final CHECK_CNT:I = 0xa

.field private static final CHECK_DELAY:I = 0xc8

.field private static final CMD_NOACTION:I = 0x64

.field private static final CMD_PAUSE:I = 0x66

.field private static final CMD_PLAY:I = 0x65

.field private static final CMD_STOP:I = 0x67

.field private static final CROP_REQUEST:I = 0x3039

.field private static final DELAY_TREED_VIDE:I = 0xf

.field private static final DELAY_TRY_TO_PLAY:I = 0x1f4

.field private static final ErrDrmLicenseExpired:I = 0x8

.field private static final ErrDrmLicenseNotFound:I = 0x9

.field private static final ErrDrmRightsAcquisitionFailed:I = 0xa

.field private static final FADEIN:I = 0x4

.field private static final HANDLER_EXIT_PLAYER:I = 0xe

.field private static final IDLE_DELAY:I = 0x3a98

.field public static final INVALID_URI_ERROR:I = 0x15f90

.field private static final ISPAUSECHECK:I = 0x6

.field private static final ISPLAYCHECK:I = 0x5

.field private static final ISSTOPCHECK:I = 0x7

.field private static final LIVE_PLAY:I = 0x3ea

.field private static final LOCAL_PLAY:I = 0x3e8

.field public static final MAX_MULTI_SUBTITLE:I = 0x32

.field private static final MAX_TRY_TO_PLAY:I = 0x5

.field public static final MEDIA_ERROR_ACCESS_TOKEN_EXPIRED:I = -0x3fc

.field public static final MEDIA_ERROR_CONNECTION_LOST:I = -0x3ed

.field public static final MEDIA_ERROR_DIVX_NOTAUTHORIZED:I = -0x62

.field public static final MEDIA_ERROR_DRM_OPL_BLOCKED:I = -0x7fffbff9

.field public static final MEDIA_ERROR_DRM_TAMPER_DETECTED:I = -0x7fffbffa

.field public static final MEDIA_ERROR_EXPIRED_RENTALCOUNT:I = -0x63

.field public static final MEDIA_ERROR_MALFORMED:I = -0x3ef

.field public static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0xc8

.field public static final MEDIA_ERROR_RESOURCE_OVERSPEC:I = -0x3fd

.field public static final MEDIA_ERROR_TRANSCODING_CODEC_ALLOCATION_ERROR:I = -0x177b

.field public static final MEDIA_ERROR_TRANSCODING_DRM_CONTENTS_IS_ALREADY_PLAYING:I = -0x177c

.field public static final MEDIA_ERROR_TRANSCODING_LACK_OF_RESOURCE:I = -0x177d

.field public static final MEDIA_ERROR_TRANSCODING_UNSPECIFIED_ERROR:I = -0x17d4

.field public static final MEDIA_ERROR_UNKNOWN_ERROR:I = 0x2714

.field public static final MEDIA_ERROR_UNSUPPORTED:I = -0x3f2

.field public static final MEDIA_INFO_AUDIO_ONLY_HLS:I = 0x38e

.field public static final MEDIA_INFO_AUDIO_VIDEO_HLS:I = 0x38f

.field public static final PLAYBACK_BUFFERING_END:I = 0x8d

.field public static final PLAYBACK_COMPLETE:I = 0x65

.field public static final PLAYBACK_DRM_DISMISS_ACQUIRING_POPUP:I = 0x6f

.field public static final PLAYBACK_DRM_LICENSE_ACQUISITION_FAILED:I = 0x6d

.field public static final PLAYBACK_DRM_LICENSE_EXPIRED:I = 0x71

.field public static final PLAYBACK_DRM_LICENSE_NOT_FOUND:I = 0x72

.field public static final PLAYBACK_DRM_SHOW_ACQUIRING_POPUP:I = 0x6e

.field public static final PLAYBACK_DRM_STRING_DIVX_NOT_AUTHORIZED:I = 0x79

.field public static final PLAYBACK_DRM_STRING_DIVX_NOT_REGISTERED:I = 0x7a

.field public static final PLAYBACK_DRM_STRING_DIVX_RENTAL_EXPIRED:I = 0x7b

.field public static final PLAYBACK_DRM_STRING_DIVX_RENTAL_INFO:I = 0x7c

.field public static final PLAYBACK_DRM_STRING_FIRST_INTERVAL_RENDER:I = 0x77

.field public static final PLAYBACK_DRM_STRING_INVALID_CD:I = 0x75

.field public static final PLAYBACK_DRM_STRING_INVALID_SD:I = 0x76

.field public static final PLAYBACK_DRM_STRING_NULL:I = 0x73

.field public static final PLAYBACK_DRM_STRING_VALID_COUNT:I = 0x74

.field public static final PLAYBACK_DRM_STRING_WMDRM_INVALID:I = 0x78

.field public static final PLAYBACK_NO_DATA_CONNECTIVITY_POPUP:I = 0x70

.field public static final PLAYBACK_PREPARED:I = 0x85

.field public static final PLAYBACK_SIZE_CHANGED:I = 0x8c

.field public static final PLAYBACK_SPLIT_SUBTITLE:I = 0x83

.field public static final PLAYBACK_SUBTITLE:I = 0x82

.field public static final PLAYBACK_UPDATE:I = 0x66

.field private static final PLAY_ENDED:I = 0x1

.field private static final RELEASE_WAKELOCK:I = 0x2

.field private static final RESUME_PLAYBACK_BY_AUDIOFOCUS:I = 0x10

.field private static final RESUME_PLAYBACK_BY_CALLSTATE:I = 0xc

.field private static final STREAM_PLAY:I = 0x3e9

.field private static final TAG:Ljava/lang/String; = "MoviePlaybackService"

.field public static final UPDATE_DURATION:I = 0x8e

.field public static final UPDATE_SURFACE_SECURE_MODE:I = 0x8f

.field private static final dla_Success_start_playback:I = 0xb

.field public static mCompletedListenrCall:Z

.field public static mContext:Landroid/content/Context;

.field public static mDrmPopupType:I

.field private static mHasAudioFocus:Z

.field private static mPausedByCallStateChanged:Z

.field public static sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

.field public static sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# instance fields
.field private final LARGE_THUMBNAIL_DATA:Ljava/lang/String;

.field private final SEND_TO_BT_METADATA:I

.field private final SEND_TO_BT_PLAYSTATUS:I

.field private drm_uri:Ljava/lang/String;

.field private mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;

.field private mAm:Landroid/app/IActivityManager;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final mBinder:Landroid/os/IBinder;

.field private final mBroadcastListener:Landroid/content/BroadcastReceiver;

.field private mBufferPercentage:I

.field public mBufferingToast:Landroid/widget/Toast;

.field private mCMDAction:I

.field private mCMDCheckCnt:I

.field private mCallState:I

.field private mCallStateChangedResumePlaybackCnt:I

.field private mCallStateString:[Ljava/lang/String;

.field mCurrentVolume:F

.field private final mDelayedStopHandler:Landroid/os/Handler;

.field private mDimWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mDlnaPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private final mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

.field private final mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

.field mDrmStringType:[I

.field private mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

.field private mForegroundToken:Landroid/os/IBinder;

.field private mIB:Landroid/os/IBinder;

.field private mKeepAudioFocus:Z

.field private mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

.field private mNMCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

.field private mNotification:Landroid/app/Notification;

.field private mNotificationDummy:Landroid/app/Notification;

.field private mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

.field private mPausedByTransientLossOfFocus:Z

.field private mPercentchecker:Z

.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPlayType:I

.field private mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

.field private mResumePlaybackCnt:I

.field private mSA:Landroid/media/audiofx/SoundAlive;

.field private mSavedVolumeByTransientLossOfFocus:I

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceInUse:Z

.field private final mServiceStartId:I

.field private mSoundAliveMode:I

.field private mStreamingResumePosition:J

.field private mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mVideoHeight:I

.field private final mVideoPlayerHandler:Landroid/os/Handler;

.field private mVideoWidth:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 114
    sput-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    .line 116
    sput-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 118
    sput-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 142
    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 144
    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 168
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    .line 323
    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByCallStateChanged:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/16 v1, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 97
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 102
    const-string v0, "large_poster_url"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->LARGE_THUMBNAIL_DATA:Ljava/lang/String;

    .line 104
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 106
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 108
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationDummy:Landroid/app/Notification;

    .line 120
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 122
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 124
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 126
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    .line 128
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .line 130
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDlnaPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

    .line 132
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCurrentVolume:F

    .line 134
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferingToast:Landroid/widget/Toast;

    .line 136
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z

    .line 138
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    .line 140
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I

    .line 146
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mKeepAudioFocus:Z

    .line 148
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPercentchecker:Z

    .line 150
    iput v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSoundAliveMode:I

    .line 152
    const/16 v0, 0x3e7

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceStartId:I

    .line 154
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallState:I

    .line 162
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 164
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 166
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I

    .line 304
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    .line 306
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    .line 321
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 325
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    .line 327
    iput v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I

    .line 329
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    .line 331
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CALL_STATE_IDLE"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "CALL_STATE_RINGING"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "CALL_STATE_OFFHOOK"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateString:[Ljava/lang/String;

    .line 339
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$1;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 477
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$2;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    .line 606
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$3;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    .line 762
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$LocalBinder;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBinder:Landroid/os/IBinder;

    .line 801
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$5;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    .line 885
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$6;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    .line 913
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$7;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    .line 932
    const/16 v0, 0x12d

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->SEND_TO_BT_PLAYSTATUS:I

    .line 934
    const/16 v0, 0x12e

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->SEND_TO_BT_METADATA:I

    .line 1323
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$8;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 1758
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    .line 1760
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    .line 1832
    new-instance v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$9;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNMCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    .line 2240
    return-void

    .line 306
    :array_0
    .array-data 4
        0x73
        0x74
        0x75
        0x76
        0x77
        0x78
        0x79
        0x7a
        0x7b
        0x7c
    .end array-data
.end method

.method private abandonAudioFocus()V
    .locals 4

    .prologue
    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1449
    const-string v0, "MoviePlaybackService"

    const-string v1, "abandonAudioFocus Hide State View"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1450
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "videoplayer.stateview.visibility"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "visibility"

    const-string v3, "hide"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1454
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    if-eqz v0, :cond_1

    .line 1455
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 1456
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 1458
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateString:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlayCheck()V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallState:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPauseCheck()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isStopCheck()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->drm_uri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I

    return p1
.end method

.method static synthetic access$1504(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->checkIsRinging(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 97
    sput-boolean p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    return p0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->gotoIdleState()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSavedVolumeByTransientLossOfFocus:I

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/app/Notification;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/AdaptSound;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Lcom/sec/android/app/videoplayer/common/AdaptSound;)Lcom/sec/android/app/videoplayer/common/AdaptSound;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/common/AdaptSound;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAdaptSound:Lcom/sec/android/app/videoplayer/common/AdaptSound;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSoundAliveMode:I

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSoundAliveMode:I

    return p1
.end method

.method static synthetic access$2700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Landroid/media/audiofx/SoundAlive;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Landroid/media/audiofx/SoundAlive;)Landroid/media/audiofx/SoundAlive;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # Landroid/media/audiofx/SoundAlive;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSA:Landroid/media/audiofx/SoundAlive;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I

    return p1
.end method

.method static synthetic access$2902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I

    return p1
.end method

.method static synthetic access$300()Z
    .locals 1

    .prologue
    .line 97
    sget-boolean v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByCallStateChanged:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/db/VideoDB;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    return-object v0
.end method

.method static synthetic access$302(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 97
    sput-boolean p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByCallStateChanged:Z

    return p0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$3302(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$3400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$3402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$3802(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # J

    .prologue
    .line 97
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPercentchecker:Z

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPercentchecker:Z

    return p1
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$4200(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDlnaPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;
    .param p1, "x1"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return p1
.end method

.method static synthetic access$908(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;

    .prologue
    .line 97
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCallStateChangedResumePlaybackCnt:I

    return v0
.end method

.method private checkIsRinging(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 1314
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1315
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkIsRinging() : state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1320
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private createNotification()V
    .locals 3

    .prologue
    .line 1763
    const-string v1, "MoviePlaybackService"

    const-string v2, "createNotification E"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-direct {v0, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1766
    .local v0, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    .line 1768
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x22

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 1769
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    const/4 v2, 0x2

    iput v2, v1, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 1770
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f0200f7

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 1772
    new-instance v1, Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    .line 1773
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->getRemoteView()Landroid/widget/RemoteViews;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 1775
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->getLaunchPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 1776
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mIB:Landroid/os/IBinder;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->setButtons(Landroid/os/IBinder;)V

    .line 1777
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNMCallback:Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->addCallback(Lcom/sec/android/app/videoplayer/service/NotificationMgr$Callback;)V

    .line 1779
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->hideNotification(Z)V

    .line 1780
    return-void
.end method

.method private gotoIdleState()V
    .locals 4

    .prologue
    .line 1628
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1629
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1630
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1631
    return-void
.end method

.method private isLiveStreaming()Z
    .locals 2

    .prologue
    .line 1604
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayType:I

    const/16 v1, 0x3ea

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPauseCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x6

    .line 430
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 451
    :goto_0
    return-void

    .line 435
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x66

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 436
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->pause()V

    .line 443
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 444
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 445
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 438
    :cond_1
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 447
    :cond_2
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 448
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private isPlayCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x5

    .line 405
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 427
    :goto_0
    return-void

    .line 410
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 411
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->play()Z

    .line 418
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 419
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 420
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 413
    :cond_1
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 414
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 423
    :cond_2
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 424
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private isStopCheck()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x7

    .line 454
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 475
    :goto_0
    return-void

    .line 459
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    const/16 v1, 0x67

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop()V

    .line 467
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    .line 468
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 462
    :cond_1
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 471
    :cond_2
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 472
    iput v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private manageProcessForeground(Z)V
    .locals 4
    .param p1, "isForeground"    # Z

    .prologue
    .line 840
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-le v1, v2, :cond_1

    .line 843
    :try_start_0
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->APP_IN_APP:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getExitByAppinApp()Z

    move-result v1

    if-nez v1, :cond_0

    .line 844
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "manageProcessForeground() : setProcessForeground = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAm:Landroid/app/IActivityManager;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-interface {v1, v2, v3, p1}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 859
    :cond_0
    :goto_0
    return-void

    .line 847
    :catch_0
    move-exception v0

    .line 848
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "MoviePlaybackService"

    const-string v2, "manageProcessForeground - SecurityException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 853
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_1
    if-eqz p1, :cond_2

    .line 854
    const/16 v1, 0x3e7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationDummy:Landroid/app/Notification;

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->startForeground(ILandroid/app/Notification;)V

    goto :goto_0

    .line 856
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stopForeground(Z)V

    goto :goto_0

    .line 849
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private notifyChange(I)V
    .locals 2
    .param p1, "what"    # I

    .prologue
    .line 924
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;->onSvcNotification(ILjava/lang/String;)V

    .line 925
    :cond_0
    return-void
.end method

.method private notifyChange(ILjava/lang/String;)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "errorUrl"    # Ljava/lang/String;

    .prologue
    .line 928
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyChange() - string :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mOnSvcNoticationListener:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$OnSvcNotificationListener;->onSvcNotification(ILjava/lang/String;)V

    .line 930
    return-void
.end method

.method private requestAudioFocus()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1434
    sget-boolean v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    if-nez v2, :cond_1

    .line 1435
    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 1436
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->gainAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    .line 1437
    .local v0, "ret":I
    if-ne v0, v1, :cond_0

    .line 1444
    .end local v0    # "ret":I
    :goto_0
    return v1

    .line 1440
    .restart local v0    # "ret":I
    :cond_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 1444
    .end local v0    # "ret":I
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    goto :goto_0
.end method

.method private stop(Z)V
    .locals 2
    .param p1, "removeStatusIcon"    # Z

    .prologue
    .line 1555
    const-string v0, "MoviePlaybackService"

    const-string v1, "stop() : start!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->stop()V

    .line 1566
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1569
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mKeepAudioFocus:Z

    if-nez v0, :cond_1

    .line 1570
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->abandonAudioFocus()V

    .line 1573
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    .line 1575
    if-eqz p1, :cond_2

    .line 1576
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->gotoIdleState()V

    .line 1579
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->resetTrackInfo()V

    .line 1580
    return-void
.end method

.method private stringForTime(IZ)Ljava/lang/String;
    .locals 1
    .param p1, "timeMs"    # I
    .param p2, "durationTime"    # Z

    .prologue
    .line 661
    if-eqz p2, :cond_2

    .line 662
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 663
    :cond_0
    const-string v0, "--:--:--"

    .line 668
    :goto_0
    return-object v0

    .line 665
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 668
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public addOutbandSubTitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->addOutbandSubTitle(Ljava/lang/String;Z)V

    .line 1663
    return-void
.end method

.method public changePlayer(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->changePlayer(I)V

    .line 1748
    return-void
.end method

.method public checkDRMFile()Z
    .locals 12

    .prologue
    const/16 v11, 0x1e

    const/4 v10, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1002
    sget-object v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v5

    .line 1004
    .local v5, "path":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-nez v8, :cond_1

    .line 1005
    const-string v7, "MoviePlaybackService"

    const-string v8, "checkDRMFile => mDrmUtil is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    :cond_0
    :goto_0
    return v6

    .line 1009
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->reset()V

    .line 1010
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v1

    .line 1011
    .local v1, "DrmType":I
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 1013
    .local v0, "DrmRigthsStatus":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1015
    :pswitch_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initOMADrmDeliveryType(Ljava/lang/String;)V

    .line 1017
    if-nez v0, :cond_3

    .line 1018
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initOMADrmConstraintsInfo(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1019
    const-string v6, "MoviePlaybackService"

    const-string v8, "checkDRMFile - get DRM information error"

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    const/16 v6, 0x2714

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1021
    goto :goto_0

    .line 1023
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->setValidOMADrmMsg(Ljava/lang/String;)I

    move-result v8

    sput v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    .line 1024
    const-string v8, "MoviePlaybackService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDRMFile - mDrmPopupType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1026
    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    if-eq v11, v8, :cond_0

    .line 1027
    const-string v6, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkDRMFile : pop up type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    sget v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    add-int/lit8 v10, v10, -0x1e

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1028
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    add-int/lit8 v8, v8, -0x1e

    aget v6, v6, v8

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1029
    goto/16 :goto_0

    .line 1033
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->setInvalidOMADrmMsg()I

    move-result v8

    sput v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    .line 1034
    const-string v8, "MoviePlaybackService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkDRMFile - Invalid rights. mDrmPopupType : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1036
    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    if-eq v11, v8, :cond_0

    .line 1037
    const-string v6, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkDRMFile : pop up type = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    sget v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    add-int/lit8 v10, v10, -0x1e

    aget v9, v9, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    add-int/lit8 v8, v8, -0x1e

    aget v6, v6, v8

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1039
    goto/16 :goto_0

    .line 1047
    :pswitch_1
    const-string v8, "MoviePlaybackService"

    const-string v9, "checkDRMFile. VIDEO_DRM_PRDRM type."

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WMDRM_NOT_SUPPORT:Z

    if-eqz v8, :cond_4

    if-ne v1, v10, :cond_4

    .line 1050
    const/16 v6, 0x71

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1051
    goto/16 :goto_0

    .line 1054
    :cond_4
    if-eq v0, v10, :cond_5

    const/4 v8, 0x2

    if-ne v0, v8, :cond_a

    .line 1056
    :cond_5
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->drm_uri:Ljava/lang/String;

    .line 1058
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsNetworkConnected()Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1059
    const/16 v6, 0x6e

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1061
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v6, :cond_6

    .line 1062
    new-instance v6, Landroid/drm/DrmManagerClient;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v6, v8}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 1065
    :cond_6
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1067
    .local v4, "mimeType":Ljava/lang/String;
    new-instance v2, Landroid/drm/DrmInfoRequest;

    invoke-direct {v2, v10, v4}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 1068
    .local v2, "drmInfoReq":Landroid/drm/DrmInfoRequest;
    const-string v6, "drm_path"

    invoke-virtual {v2, v6, v5}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1070
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v3

    .line 1071
    .local v3, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->isFromStore()Z

    move-result v6

    if-nez v6, :cond_7

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1072
    :cond_7
    const-string v6, "user_guid"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getUserGuid()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1073
    const-string v6, "imei"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getImei()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1074
    const-string v6, "app_Id"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getAppID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1075
    const-string v6, "orderId"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getOrderID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1076
    const-string v6, "media_hub"

    const-string v8, "Media_hub"

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1077
    const-string v6, "mv_id"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getMVID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1078
    const-string v6, "svr_id"

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getSvrID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v6, v8}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1080
    const-string v6, "MoviePlaybackService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkDRMFile() : get custom data. user_guid: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "user_guid"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "imei"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "imei"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "app_Id"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "app_Id"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "orderId"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "orderId"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "media_hub"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "media_hub"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "mv_id"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "mv_id"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " ,"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "svr_id"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "svr_id"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfoRequest;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    invoke-virtual {v6, v8}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    .line 1091
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    invoke-virtual {v6, v8}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    .line 1092
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v6, v2}, Landroid/drm/DrmManagerClient;->acquireRights(Landroid/drm/DrmInfoRequest;)I

    .line 1093
    const-string v6, "MoviePlaybackService"

    const-string v8, "License Acquisitin has started"

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 1094
    goto/16 :goto_0

    .line 1096
    .end local v2    # "drmInfoReq":Landroid/drm/DrmInfoRequest;
    .end local v3    # "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    .end local v4    # "mimeType":Ljava/lang/String;
    :cond_9
    const-string v6, "MoviePlaybackService"

    const-string v8, "No Network Connection"

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    const/16 v6, 0x70

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1098
    goto/16 :goto_0

    .line 1100
    :cond_a
    if-ne v0, v6, :cond_0

    .line 1101
    const/16 v6, 0x23

    sput v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    .line 1102
    const/16 v6, 0x78

    invoke-direct {p0, v6}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    move v6, v7

    .line 1103
    goto/16 :goto_0

    .line 1108
    :pswitch_2
    const-string v8, "MoviePlaybackService"

    const-string v9, "checkDRMFile Divx type."

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v8, v0, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->divxPopupType(ILjava/lang/String;)I

    move-result v8

    sput v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    .line 1110
    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    if-eq v8, v11, :cond_0

    .line 1111
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmStringType:[I

    sget v9, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    add-int/lit8 v9, v9, -0x1e

    aget v8, v8, v9

    invoke-direct {p0, v8}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1112
    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    const/16 v9, 0x24

    if-eq v8, v9, :cond_b

    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    const/16 v9, 0x26

    if-eq v8, v9, :cond_b

    sget v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmPopupType:I

    const/16 v9, 0x27

    if-ne v8, v9, :cond_0

    .line 1115
    :cond_b
    const-string v6, "MoviePlaybackService"

    const-string v8, "checkDRMFile Divx return false"

    invoke-static {v6, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v7

    .line 1116
    goto/16 :goto_0

    .line 1013
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public duration()J
    .locals 6

    .prologue
    .line 1194
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1196
    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDur()J

    move-result-wide v0

    .line 1210
    :cond_0
    :goto_0
    return-wide v0

    .line 1199
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1200
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->duration()J

    move-result-wide v0

    goto :goto_0

    .line 1203
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    .line 1204
    .local v2, "type":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1205
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSavedDuration()J

    move-result-wide v0

    .line 1206
    .local v0, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gtz v3, :cond_0

    .line 1210
    .end local v0    # "duration":J
    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getBufferPercentage()I
    .locals 1

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1171
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I

    .line 1174
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferPercentage:I

    return v0
.end method

.method public getCurrentFrame()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1741
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->getCurrentFrame()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1743
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFPS()I
    .locals 2

    .prologue
    .line 1294
    const/4 v0, -0x1

    .line 1295
    .local v0, "fps":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1296
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->getFPS()I

    move-result v0

    .line 1298
    :cond_0
    return v0
.end method

.method public getFullDumpSubtitle(Ljava/lang/String;Z)[B
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1670
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->getFullDumpSubtitle(Ljava/lang/String;Z)[B

    move-result-object v0

    return-object v0
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1187
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I

    .line 1190
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoHeight:I

    return v0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1179
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I

    .line 1182
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoWidth:I

    return v0
.end method

.method public hideNotification(Z)V
    .locals 5
    .param p1, "onlyNotification"    # Z

    .prologue
    .line 1799
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hideNotification E. only : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->hide()V

    .line 1802
    :cond_0
    if-eqz p1, :cond_1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->manageProcessForeground(Z)V

    .line 1805
    :cond_1
    :try_start_0
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 1806
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v2, 0x3e7

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1810
    .end local v1    # "nm":Landroid/app/NotificationManager;
    :goto_0
    return-void

    .line 1807
    :catch_0
    move-exception v0

    .line 1808
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public initSelectSubtitleTrack()V
    .locals 1

    .prologue
    .line 1679
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->initSelectSubtitleTrack()V

    .line 1680
    return-void
.end method

.method public initSubtitle(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "isOutbandType"    # Z

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->initSubtitle(Ljava/lang/String;Z)V

    .line 1667
    return-void
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 1310
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    return v0
.end method

.method public isPausedByTransientLossOfFocus()Z
    .locals 3

    .prologue
    .line 1751
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPausedByTransientLossOfFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1752
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 1280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1281
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isPlaying()Z

    move-result v0

    .line 1284
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSecVideo()Z
    .locals 1

    .prologue
    .line 1302
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isSecVideo()Z

    move-result v0

    .line 1306
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 765
    const-string v0, "MoviePlaybackService"

    const-string v1, "onBind E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 766
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 767
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z

    .line 768
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 679
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 680
    const-string v4, "MoviePlaybackService"

    const-string v5, "onCreate() : start!"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    sput-boolean v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCompletedListenrCall:Z

    .line 684
    new-instance v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    .line 685
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->setHandler(Landroid/os/Handler;)V

    .line 686
    new-instance v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDlnaPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$DlnaPlayer;

    .line 688
    new-instance v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-direct {v4, p0, v7}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$1;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    .line 689
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setPlayer(Lcom/sec/android/app/videoplayer/service/IPlayer;)V

    .line 691
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 692
    iput v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 694
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 695
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 696
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 698
    new-instance v4, Landroid/app/Notification;

    invoke-direct {v4}, Landroid/app/Notification;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationDummy:Landroid/app/Notification;

    .line 699
    new-instance v4, Landroid/os/Binder;

    invoke-direct {v4}, Landroid/os/Binder;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mForegroundToken:Landroid/os/IBinder;

    .line 700
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mAm:Landroid/app/IActivityManager;

    .line 701
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->manageProcessForeground(Z)V

    .line 704
    new-instance v4, Landroid/os/Binder;

    invoke-direct {v4}, Landroid/os/Binder;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mIB:Landroid/os/IBinder;

    .line 705
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->createNotification()V

    .line 707
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 708
    .local v3, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v5, 0x20

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 710
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 711
    .local v1, "powermanager":Landroid/os/PowerManager;
    const v4, 0x2000000a

    const-string v5, "MoviePlayer-Sleep"

    invoke-virtual {v1, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 712
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 714
    const-string v4, "power"

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 715
    .local v2, "powermanagerForDim":Landroid/os/PowerManager;
    const v4, 0x20000006

    const-string v5, "MoviePlayer-Dim"

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 716
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v4, v6}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 718
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 719
    .local v0, "intentfilter":Landroid/content/IntentFilter;
    const-string v4, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 721
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5, v0}, Landroid/content/IntentFilter;-><init>(Landroid/content/IntentFilter;)V

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 722
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4, v7, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBufferingToast:Landroid/widget/Toast;

    .line 726
    new-instance v4, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;

    invoke-direct {v6, p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$4;-><init>(Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;)V

    invoke-direct {v4, v5, v6}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mMotionManager:Lcom/sec/android/app/videoplayer/common/manager/MotionManager;

    .line 760
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 812
    const-string v1, "MoviePlaybackService"

    const-string v2, "onDestroy() : start!"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->setWakeMode(Z)V

    .line 815
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mBroadcastListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 817
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isActivityPauseState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 818
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 819
    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->unbindFromPresentationService()V

    .line 823
    :cond_0
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->manageProcessForeground(Z)V

    .line 825
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 826
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 828
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 829
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 831
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v1, :cond_1

    .line 832
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1}, Landroid/drm/DrmManagerClient;->release()V

    .line 833
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 836
    :cond_1
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 837
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 772
    const-string v0, "MoviePlaybackService"

    const-string v1, "onRebind E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 774
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z

    .line 775
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 779
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 780
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 781
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDelayedStopHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 784
    const/4 v1, 0x1

    return v1
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 788
    const-string v0, "MoviePlaybackService"

    const-string v1, "onUnbind E"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mTouchPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$TouchPlayer;->release()V

    .line 794
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mServiceInUse:Z

    .line 796
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stopForeground(Z)V

    .line 797
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stopSelf()V

    .line 798
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public oneFrameBackward()V
    .locals 2

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1244
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->oneFrameSeek(I)V

    .line 1246
    :cond_0
    return-void
.end method

.method public oneFrameForward()V
    .locals 2

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1238
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->oneFrameSeek(I)V

    .line 1240
    :cond_0
    return-void
.end method

.method public openPath(Ljava/lang/String;)Z
    .locals 8
    .param p1, "videoUri"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1127
    const-string v4, "MoviePlaybackService"

    const-string v5, "openPath"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    if-eqz p1, :cond_1

    .line 1130
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 1131
    iput v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 1134
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA_CSFB_STREAMING:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1139
    const-string v4, "MoviePlaybackService"

    const-string v5, "openPath() - call connect. Do not play video."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1140
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    const v5, 0x7f0a0067

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1141
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "videoplayer.stateview.visibility"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "visibility"

    const-string v7, "hide"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1144
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1145
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1147
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v2

    .line 1148
    .local v2, "resumePos":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1

    .line 1149
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "openPath() - call connect. Do not play video. : Save Resume position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1166
    .end local v2    # "resumePos":J
    :cond_1
    :goto_0
    return v1

    .line 1156
    :cond_2
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setDataSourcePrepare(Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1163
    const/4 v1, 0x1

    goto :goto_0

    .line 1157
    :catch_0
    move-exception v0

    .line 1158
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1159
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException occured  1 :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 1592
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1593
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isLiveStreaming()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->pause()V

    .line 1595
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1599
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->abandonAudioFocus()V

    .line 1600
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->gotoIdleState()V

    .line 1601
    return-void
.end method

.method public pauseOrStopByAlertSound()V
    .locals 3

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1611
    sget-object v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1612
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->pause()V

    .line 1616
    :goto_0
    const/16 v0, 0x66

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1619
    :cond_0
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pauseOrStopByAlertSound() E. mPlayer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1620
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->gotoIdleState()V

    .line 1621
    return-void

    .line 1614
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->stop()V

    goto :goto_0
.end method

.method public play()Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x1f4

    const-wide/16 v10, -0x1

    const/4 v4, 0x1

    const/16 v8, 0x10

    const/4 v1, 0x0

    .line 1468
    const-string v5, "MoviePlaybackService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "play() : start! : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1470
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    sget-object v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1471
    const-string v5, "MoviePlaybackService"

    const-string v6, "play() - call connect. Do not play video."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1472
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    const v6, 0x7f0a0067

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1473
    sput-boolean v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByCallStateChanged:Z

    .line 1474
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v2

    .line 1475
    .local v2, "resumePos":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 1476
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "play() - call connect. Do not play video. : Save Resume position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1477
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1551
    .end local v2    # "resumePos":J
    :cond_0
    :goto_0
    return v1

    .line 1482
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v5, :cond_2

    sget-object v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v5

    if-nez v5, :cond_2

    .line 1484
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->resetPlayer()V

    goto :goto_0

    .line 1488
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1492
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v5

    if-nez v5, :cond_3

    .line 1494
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendVHLastPlayedItemBroadcast(Z)V

    .line 1497
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1498
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->requestAudioFocus()Z

    move-result v5

    if-nez v5, :cond_5

    .line 1499
    const-string v4, "MoviePlaybackService"

    const-string v5, "play() - requestAudioFocus failed. do not play this time."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1500
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1501
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1503
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1507
    :cond_5
    sget-boolean v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    if-eqz v5, :cond_8

    .line 1508
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->start()V

    .line 1509
    iput-wide v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    .line 1510
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1511
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->resetSavedPosition()V

    .line 1538
    :cond_6
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayerState(I)V

    .line 1539
    sput-boolean v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByCallStateChanged:Z

    .line 1540
    const/16 v5, 0x66

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->notifyChange(I)V

    .line 1541
    sget-object v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 1544
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 1545
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/feature/Feature;->isGateEnabled()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1546
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    sget-object v6, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 1547
    .local v0, "gatePath":Ljava/lang/String;
    const-string v5, "GATE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "<GATE-M> VIDEO_PLAYING: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " </GATE-M>"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1550
    .end local v0    # "gatePath":Ljava/lang/String;
    :cond_7
    iput v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mResumePlaybackCnt:I

    move v1, v4

    .line 1551
    goto/16 :goto_0

    .line 1514
    :cond_8
    const-string v5, "MoviePlaybackService"

    const-string v6, "play() - AudioFocus not gained. try to gain again."

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->requestAudioFocus()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1516
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->start()V

    .line 1517
    iput-wide v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    .line 1518
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1519
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/slink/SLink;->resetSavedPosition()V

    goto :goto_1

    .line 1522
    :cond_9
    const-string v4, "MoviePlaybackService"

    const-string v5, "play() - AudioFocus not gained. do not play this time."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1523
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1524
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1526
    :cond_a
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v4, v8, v12, v13}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1531
    :cond_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v5

    if-nez v5, :cond_c

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->isDirectDmcMode()Z

    move-result v5

    if-nez v5, :cond_c

    .line 1533
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->requestAudioFocus()Z

    .line 1535
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->start()V

    goto/16 :goto_1
.end method

.method public position()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1214
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1216
    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurPos()J

    move-result-wide v0

    .line 1233
    :cond_0
    :goto_0
    return-wide v0

    .line 1219
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1220
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->position()J

    move-result-wide v0

    goto :goto_0

    .line 1223
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    .line 1224
    .local v2, "type":Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1225
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->getSavedPosition()J

    move-result-wide v0

    .line 1226
    .local v0, "position":J
    cmp-long v3, v0, v6

    if-gtz v3, :cond_0

    .line 1233
    .end local v0    # "position":J
    :cond_3
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1227
    :cond_4
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    .line 1228
    const-string v3, "MoviePlaybackService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentPosition : Streaming type, never been played. return previous saved position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1229
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    goto :goto_0
.end method

.method public realSeek(II)J
    .locals 4
    .param p1, "pos"    # I
    .param p2, "seekMode"    # I

    .prologue
    .line 1264
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1267
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/videoplayer/slink/SLink;->setSavedPosition(J)V

    .line 1270
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1271
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    .line 1273
    :cond_1
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "realSeek pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seekMode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->realSeek(II)J

    move-result-wide v0

    .line 1276
    :goto_0
    return-wide v0

    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->reset()V

    .line 1625
    return-void
.end method

.method public resetSubtitle()V
    .locals 1

    .prologue
    .line 1683
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->resetSubtitle()V

    .line 1684
    return-void
.end method

.method public resetTrackInfo()V
    .locals 3

    .prologue
    .line 1733
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    .line 1734
    .local v0, "trackInfoUtil":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v0, :cond_0

    .line 1735
    const-string v1, "MoviePlaybackService"

    const-string v2, "resetTrackInfo()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->resetAllTrackInfo()V

    .line 1738
    :cond_0
    return-void
.end method

.method public seek(J)J
    .locals 5
    .param p1, "pos"    # J

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1250
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251
    invoke-static {}, Lcom/sec/android/app/videoplayer/slink/SLink;->getInstance()Lcom/sec/android/app/videoplayer/slink/SLink;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/slink/SLink;->setSavedPosition(J)V

    .line 1254
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1255
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mStreamingResumePosition:J

    .line 1257
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->seek(J)J

    move-result-wide v0

    .line 1260
    :goto_0
    return-wide v0

    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public sendBTbroadcast(I)V
    .locals 13
    .param p1, "mode"    # I

    .prologue
    .line 937
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v2

    .line 939
    .local v2, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    .line 941
    const/16 v10, 0x12d

    if-ne p1, v10, :cond_1

    .line 942
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->sendPlayState(J)V

    .line 944
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. ACTION_SEND_TO_BT_PLAYSTATUS = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isPlaying()Z

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 945
    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.sec.android.videoplayer.playerstatus"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 946
    .local v5, "i":Landroid/content/Intent;
    const-string v10, "playing"

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isPlaying()Z

    move-result v11

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 947
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBroadcast(Landroid/content/Intent;)V

    .line 997
    .end local v5    # "i":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 948
    :cond_1
    const/16 v10, 0x12e

    if-ne p1, v10, :cond_0

    .line 949
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-nez v10, :cond_2

    .line 950
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 953
    :cond_2
    const/4 v3, 0x0

    .line 954
    .local v3, "curIdx":I
    const/4 v9, 0x0

    .line 955
    .local v9, "totalVideoCnt":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->duration()J

    move-result-wide v6

    .line 957
    .local v6, "duration":J
    sget-object v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    .line 959
    .local v4, "curUri":Landroid/net/Uri;
    sget-object v10, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getFileTitle()Ljava/lang/String;

    move-result-object v8

    .line 960
    .local v8, "title":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getArtist(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 961
    .local v1, "artist":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v10, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getAlbum(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 963
    .local v0, "album":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getTotalVideoFileCnt()I

    move-result v9

    .line 964
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/util/VideoPlayListUtil;->getCurIdx()I

    move-result v3

    .line 966
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-gtz v10, :cond_4

    .line 967
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0145

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 970
    :cond_4
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v10, :cond_5

    .line 971
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_TITLE = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_ALBUMARTIST = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_ALBUM = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_CD_TRACK_NUMBER = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_DISC_NUMBER = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 976
    const-string v10, "MoviePlaybackService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "sendBTbroadcast. METADATA_KEY_DURATION = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    :cond_5
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->initMetaData()V

    .line 980
    const-string v10, "android.media.metadata.TITLE"

    invoke-virtual {v2, v10, v8}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 981
    const-string v10, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {v2, v10, v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    const-string v10, "android.media.metadata.ALBUM"

    invoke-virtual {v2, v10, v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataString(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    const-string v10, "android.media.metadata.TRACK_NUMBER"

    add-int/lit8 v11, v3, 0x1

    invoke-virtual {v2, v10, v11}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataInt(Ljava/lang/String;I)V

    .line 984
    const-string v10, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v2, v10, v9}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataInt(Ljava/lang/String;I)V

    .line 985
    const-string v10, "android.media.metadata.DURATION"

    invoke-virtual {v2, v10, v6, v7}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setMetaDataLong(Ljava/lang/String;J)V

    .line 986
    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->updateMetadata()V

    .line 988
    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.sec.android.videoplayer.metachanged"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 989
    .restart local v5    # "i":Landroid/content/Intent;
    const-string v10, "title"

    invoke-virtual {v5, v10, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 990
    const-string v10, "artist"

    invoke-virtual {v5, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 991
    const-string v10, "album"

    invoke-virtual {v5, v10, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 992
    const-string v10, "mediaCount"

    invoke-virtual {v5, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 993
    const-string v10, "listpos"

    add-int/lit8 v11, v3, 0x1

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 994
    const-string v10, "duration"

    invoke-virtual {v5, v10, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 995
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public sendSideSyncLastPlayedItemBroadcast(J)V
    .locals 5
    .param p1, "curPos"    # J

    .prologue
    .line 648
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sidesync.common.MEDIA_SHARE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 650
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendSideSyncLastPlayedItemBroadcast. curPos : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    const-string v1, "MoviePlaybackService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendSideSyncLastPlayedItemBroadcast. uri : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    const-string v1, "CONTENT"

    sget-object v2, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 654
    const-string v1, "resumePos"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 655
    const-string v1, "UPDATEONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 657
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBroadcast(Landroid/content/Intent;)V

    .line 658
    return-void
.end method

.method public sendVHLastPlayedItemBroadcast(Z)V
    .locals 10
    .param p1, "isStart"    # Z

    .prologue
    .line 620
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;

    move-result-object v6

    sget-object v7, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/videoplayer/util/hub/VideoHubUtil;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 621
    .local v5, "uri":Landroid/net/Uri;
    const-string v6, "MoviePlaybackService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sendVHLastPlayedItemBroadcast. uri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    const-string v6, "MoviePlaybackService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "sendVHLastPlayedItemBroadcast. isStart : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", uri : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", postion : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", duration : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->duration()J

    move-result-wide v8

    long-to-int v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v0

    .line 626
    .local v0, "currentPos":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->duration()J

    move-result-wide v6

    long-to-int v6, v6

    const/4 v7, 0x1

    invoke-direct {p0, v6, v7}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    .line 627
    .local v1, "duration":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    const-string v7, "large_poster_url"

    invoke-virtual {v6, v5, v7}, Lcom/sec/android/app/videoplayer/db/VideoDB;->fetchString(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 629
    .local v4, "thumbnailUrl":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getInstance()Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;

    move-result-object v2

    .line 631
    .local v2, "hubInfo":Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 632
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "com.sec.everglades.action.RECENT_DATA_CHANGED"

    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 633
    const-string v6, "from_where"

    const/16 v7, 0x45e

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 634
    const-string v6, "recent_thumbnail"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 635
    const-string v6, "recent_title"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 636
    const-string v6, "recent_current_progress"

    invoke-virtual {v3, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 637
    const-string v6, "recent_total_progress"

    invoke-virtual {v3, v6, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    const-string v6, "recent_paused"

    invoke-virtual {v3, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 639
    const-string v6, "launch_player_productid"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getProductId()I

    move-result v7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 640
    const-string v6, "launch_player_attributetype"

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/hub/HubContentInfo;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 642
    if-eqz v3, :cond_0

    .line 643
    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sendBroadcast(Landroid/content/Intent;)V

    .line 645
    :cond_0
    return-void
.end method

.method public setAdaptSound(ZZ)V
    .locals 1
    .param p1, "isOn"    # Z
    .param p2, "showToast"    # Z

    .prologue
    .line 1640
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1641
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setAdaptSound(ZZ)V

    .line 1643
    :cond_0
    return-void
.end method

.method public setAudioTrack(I)V
    .locals 1
    .param p1, "mTrackNumber"    # I

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setAudioTrack(I)V

    .line 1291
    :cond_0
    return-void
.end method

.method public setHasAudioFocus()V
    .locals 1

    .prologue
    .line 1462
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPausedByTransientLossOfFocus:Z

    if-eqz v0, :cond_0

    .line 1463
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mHasAudioFocus:Z

    .line 1465
    :cond_0
    return-void
.end method

.method public setInbandSubtitle()V
    .locals 1

    .prologue
    .line 1675
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setInbandSubtitle()V

    .line 1676
    return-void
.end method

.method public setKeepAudioFocus(Z)V
    .locals 3
    .param p1, "keepAudioFocus"    # Z

    .prologue
    .line 1583
    const-string v0, "MoviePlaybackService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setKeepAudioFocus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1584
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mKeepAudioFocus:Z

    .line 1585
    return-void
.end method

.method public setPlaySpeed(F)V
    .locals 1
    .param p1, "speed"    # F

    .prologue
    .line 1652
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1653
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setPlaySpeed(F)V

    .line 1655
    :cond_0
    return-void
.end method

.method public setSoundAliveMode(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1647
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setSoundAliveMode(I)V

    .line 1649
    :cond_0
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 1
    .param p1, "synctime"    # I

    .prologue
    .line 1687
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setSubtitleSyncTime(I)V

    .line 1688
    return-void
.end method

.method public setVideoCrop(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setVideoCrop(IIII)V

    .line 1659
    return-void
.end method

.method public setVolume(F)V
    .locals 1
    .param p1, "vol"    # F

    .prologue
    .line 1634
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setVolume(F)V

    .line 1637
    :cond_0
    return-void
.end method

.method public setWakeMode(Z)V
    .locals 2
    .param p1, "mode"    # Z

    .prologue
    .line 862
    if-eqz p1, :cond_3

    .line 863
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 864
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 869
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getKeepScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    .line 870
    const-string v0, "MoviePlaybackService"

    const-string v1, "setWakeMode setKeepScreenOn true"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setKeepScreenOn(Z)V

    .line 883
    :cond_1
    :goto_1
    return-void

    .line 866
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    goto :goto_0

    .line 874
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDimWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 876
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 878
    :cond_5
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getKeepScreenOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 879
    const-string v0, "MoviePlaybackService"

    const-string v1, "setWakeMode setKeepScreenOn false"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 880
    sget-object v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setKeepScreenOn(Z)V

    goto :goto_1
.end method

.method public showNotification()V
    .locals 4

    .prologue
    .line 1783
    const-string v2, "MoviePlaybackService"

    const-string v3, "showNotification E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    iget-boolean v2, v2, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->mShowing:Z

    if-eqz v2, :cond_1

    .line 1796
    :cond_0
    :goto_0
    return-void

    .line 1786
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->show()V

    .line 1787
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->setAllView(Lcom/sec/android/app/videoplayer/common/VideoFileInfo;)V

    .line 1788
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->setPlayStatus(Z)V

    .line 1791
    :try_start_0
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 1792
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v2, 0x3e7

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1793
    .end local v1    # "nm":Landroid/app/NotificationManager;
    :catch_0
    move-exception v0

    .line 1794
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public startPlay()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1695
    const-string v4, "MoviePlaybackService"

    const-string v5, "startPlay"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDAction:I

    .line 1697
    iput v1, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mCMDCheckCnt:I

    .line 1700
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA_CSFB_STREAMING:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    sget-object v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->AudioFocusOrCallCheckingNeeded(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1705
    const-string v4, "MoviePlaybackService"

    const-string v5, "startPlay() - call connect. Do not play video."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1706
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    const v5, 0x7f0a0067

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 1707
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v4

    new-instance v5, Landroid/content/Intent;

    const-string v6, "videoplayer.stateview.visibility"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "visibility"

    const-string v7, "hide"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 1710
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->position()J

    move-result-wide v2

    .line 1711
    .local v2, "resumePos":J
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 1712
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startPlay() - call connect. Do not play video. : Save Resume position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1713
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    .line 1729
    .end local v2    # "resumePos":J
    :cond_0
    :goto_0
    return v1

    .line 1719
    :cond_1
    :try_start_0
    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v4, :cond_2

    sget-object v4, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1720
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    sget-object v5, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->sServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getProperPathToOpen()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->setDataSourcePrepare(Landroid/net/Uri;)V

    .line 1729
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 1722
    :cond_2
    const-string v4, "MoviePlaybackService"

    const-string v5, "mPlayer.startPlay() : mServiceUtil or mServiceUtil.getProperPathToOpen() is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1724
    :catch_0
    move-exception v0

    .line 1725
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1726
    const-string v4, "MoviePlaybackService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException occured  1 :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startSubtitle()V
    .locals 1

    .prologue
    .line 1691
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mPlayer:Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService$PlayerSelector;->startSubtitle()V

    .line 1692
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 1588
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->stop(Z)V

    .line 1589
    return-void
.end method

.method public updateNotification(Z)V
    .locals 5
    .param p1, "contentChanged"    # Z

    .prologue
    .line 1813
    const-string v2, "MoviePlaybackService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateNotification E. contentChanged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    if-nez v2, :cond_0

    .line 1830
    :goto_0
    return-void

    .line 1817
    :cond_0
    if-eqz p1, :cond_1

    .line 1818
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    sget-object v3, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->setAllView(Lcom/sec/android/app/videoplayer/common/VideoFileInfo;)V

    .line 1822
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotificationMgr:Lcom/sec/android/app/videoplayer/service/NotificationMgr;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->isPlaying()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/service/NotificationMgr;->setPlayStatus(Z)V

    .line 1825
    :try_start_0
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 1826
    .local v1, "nm":Landroid/app/NotificationManager;
    const/16 v2, 0x3e7

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/service/MoviePlaybackService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1827
    .end local v1    # "nm":Landroid/app/NotificationManager;
    :catch_0
    move-exception v0

    .line 1828
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
