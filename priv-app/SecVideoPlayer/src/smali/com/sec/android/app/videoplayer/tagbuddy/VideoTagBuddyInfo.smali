.class public Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;
.super Ljava/lang/Object;
.source "VideoTagBuddyInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$1;,
        Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;,
        Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;
    }
.end annotation


# static fields
.field public static final CONTEXTUAL_URI:Landroid/net/Uri;

.field public static final DATE:Ljava/lang/String; = "Date"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LOCATION:Ljava/lang/String; = "Location"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field private static TAG:Ljava/lang/String; = null

.field public static final TAKEN_TIME:Ljava/lang/String; = "datetaken"

.field private static final UPDATE_TAG_BUDDY_DATE_VISIBLITY:I = 0x517

.field private static final UPDATE_TAG_BUDDY_LOCATION_VISIBLITY:I = 0x516

.field private static final UPDATE_TAG_BUDDY_VISIBLITY:I = 0x514

.field private static mInstance:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;


# instance fields
.field private dLatitude:D

.field private dLongitude:D

.field private mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mDate:Ljava/lang/String;

.field private mLocation:Ljava/lang/String;

.field private mLocationTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

.field private mShowDateTag:Z

.field private mShowLocationTag:Z

.field private mShowTagBuddy:Z

.field private mTagBuddyUpdateListener:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "VideoTagBuddyInfo"

    sput-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    .line 39
    const-string v0, "content://media/external/contextural_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->CONTEXTUAL_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    .line 41
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    .line 43
    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowTagBuddy:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowLocationTag:Z

    .line 49
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowDateTag:Z

    .line 59
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    .line 337
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mTagBuddyUpdateListener:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;

    .line 68
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    .line 69
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->initSettingValue(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mInstance:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mInstance:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    .line 80
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mInstance:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;

    return-object v0
.end method

.method private initSettingValue(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "showsswitchstate_contextual"

    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowTagBuddy:Z

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "showscontextuallocationstatus"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowLocationTag:Z

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    const-string v1, "showscontextualdatestatus"

    invoke-virtual {v0, v1, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowDateTag:Z

    .line 89
    :cond_0
    return-void
.end method

.method private makeDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "DateString"    # Ljava/lang/String;

    .prologue
    .line 158
    if-nez p1, :cond_0

    .line 159
    const/4 v7, 0x0

    .line 179
    :goto_0
    return-object v7

    .line 161
    :cond_0
    const-wide/16 v2, 0x0

    .line 164
    .local v2, "dateTaken":J
    if-eqz p1, :cond_1

    .line 165
    :try_start_0
    invoke-static {p1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 166
    const/4 p1, 0x0

    .line 172
    :cond_1
    :goto_1
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 173
    .local v1, "date":Ljava/util/Date;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 174
    .local v6, "tmpTime":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 176
    .local v5, "tmpDate":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 177
    .local v0, "DateTime":Ljava/lang/StringBuilder;
    sget-object v7, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "makeDate() - tmpText : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 168
    .end local v0    # "DateTime":Ljava/lang/StringBuilder;
    .end local v1    # "date":Ljava/util/Date;
    .end local v5    # "tmpDate":Ljava/lang/String;
    .end local v6    # "tmpTime":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 169
    .local v4, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private searchLocation()V
    .locals 2

    .prologue
    .line 151
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v1, "searchLocation"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;-><init>(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$UpdateLocationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    .line 155
    :cond_0
    return-void
.end method

.method private updatePerference(IZ)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 249
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    if-nez v1, :cond_0

    .line 250
    sget-object v1, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v2, "updatePreference. mPrefMgr is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :goto_0
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x0

    .line 256
    .local v0, "keyStr":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 258
    :pswitch_1
    const-string v0, "showsswitchstate_contextual"

    .line 273
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mPrefMgr:Lcom/sec/android/app/videoplayer/db/SharedPreference;

    invoke-virtual {v1, v0, p2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    goto :goto_0

    .line 262
    :pswitch_2
    const-string v0, "showscontextuallocationstatus"

    .line 263
    goto :goto_1

    .line 266
    :pswitch_3
    const-string v0, "showscontextualdatestatus"

    .line 267
    goto :goto_1

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x514
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public cancelTask()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocationTask:Landroid/os/AsyncTask;

    .line 246
    :cond_0
    return-void
.end method

.method public findAddress()Ljava/lang/String;
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 198
    sget-object v2, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v3, "findAddress E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :try_start_0
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;)V

    .line 202
    .local v1, "gc":Landroid/location/Geocoder;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v9

    .line 204
    .local v9, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 205
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v3, "findAddress. no network"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 207
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 238
    .end local v1    # "gc":Landroid/location/Geocoder;
    .end local v9    # "networkInfo":Landroid/net/NetworkInfo;
    :goto_0
    return-object v2

    .line 210
    .restart local v1    # "gc":Landroid/location/Geocoder;
    .restart local v9    # "networkInfo":Landroid/net/NetworkInfo;
    :cond_1
    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    const/16 v6, 0xa

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;

    move-result-object v7

    .line 211
    .local v7, "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 213
    .local v10, "sbuilder":Ljava/lang/StringBuilder;
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 214
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    .line 216
    .local v0, "address":Landroid/location/Address;
    invoke-virtual {v0}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 217
    invoke-virtual {v0}, Landroid/location/Address;->getThoroughfare()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :goto_1
    if-eqz v10, :cond_2

    .line 227
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0091

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 231
    .end local v0    # "address":Landroid/location/Address;
    :cond_2
    const/4 v1, 0x0

    .line 232
    sget-object v2, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "findAddress. add location : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 238
    .end local v1    # "gc":Landroid/location/Geocoder;
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v9    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v10    # "sbuilder":Ljava/lang/StringBuilder;
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    goto :goto_0

    .line 218
    .restart local v0    # "address":Landroid/location/Address;
    .restart local v1    # "gc":Landroid/location/Geocoder;
    .restart local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v9    # "networkInfo":Landroid/net/NetworkInfo;
    .restart local v10    # "sbuilder":Ljava/lang/StringBuilder;
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 219
    invoke-virtual {v0}, Landroid/location/Address;->getLocality()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 233
    .end local v0    # "address":Landroid/location/Address;
    .end local v1    # "gc":Landroid/location/Geocoder;
    .end local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .end local v9    # "networkInfo":Landroid/net/NetworkInfo;
    .end local v10    # "sbuilder":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v8

    .line 234
    .local v8, "e":Ljava/lang/Exception;
    iput-object v11, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 235
    sget-object v2, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v3, "findAddress. failed to find location"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 220
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v0    # "address":Landroid/location/Address;
    .restart local v1    # "gc":Landroid/location/Geocoder;
    .restart local v7    # "addresses":Ljava/util/List;, "Ljava/util/List<Landroid/location/Address;>;"
    .restart local v9    # "networkInfo":Landroid/net/NetworkInfo;
    .restart local v10    # "sbuilder":Ljava/lang/StringBuilder;
    :cond_4
    :try_start_2
    invoke-virtual {v0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 221
    invoke-virtual {v0}, Landroid/location/Address;->getAdminArea()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 223
    :cond_5
    invoke-virtual {v0}, Landroid/location/Address;->getCountryName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public getDateString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    return-object v0
.end method

.method public getDateTagShowStatus()Z
    .locals 3

    .prologue
    .line 326
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDateTagShowStatus. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowDateTag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowDateTag:Z

    return v0
.end method

.method public getLocationString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getLocationTagShowStatus()Z
    .locals 3

    .prologue
    .line 321
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLocationTagShowStatus. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowLocationTag:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowLocationTag:Z

    return v0
.end method

.method public getTagBuddyShowStatus()Z
    .locals 3

    .prologue
    .line 316
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getTagBuddyShowStatus. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowTagBuddy:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowTagBuddy:Z

    return v0
.end method

.method public getTagInfo(J)V
    .locals 9
    .param p1, "videoId"    # J

    .prologue
    .line 99
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    .line 100
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v2, "getTagInfo. video id is invalid. return"

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->initVariable()V

    .line 105
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->CONTEXTUAL_URI:Landroid/net/Uri;

    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 106
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 107
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 110
    .local v7, "date":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 112
    if-eqz v6, :cond_5

    .line 113
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 115
    const-string v0, "latitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    .line 116
    const-string v0, "longitude"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    .line 117
    const-string v0, "datetaken"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 118
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->makeDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    .line 120
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->hasLocationInfo()Z

    move-result v0

    if-nez v0, :cond_3

    .line 121
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->hasDateInfo()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->sendTagBuddyInfoUpdateNotify()V

    .line 125
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    const-string v2, "getTagInfo. Tag buddy Location info not exist"

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    if-eqz v6, :cond_2

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 148
    :cond_2
    :goto_0
    return-void

    .line 129
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->hasLocationInfo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 130
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->searchLocation()V

    .line 133
    :cond_4
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_5

    .line 134
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTagInfo. latitude : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",  longitude : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", date : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mDate : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    :cond_5
    if-eqz v6, :cond_2

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 139
    :catch_0
    move-exception v8

    .line 140
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_2
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTagInfo. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 144
    if-eqz v6, :cond_2

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 141
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catch_1
    move-exception v8

    .line 142
    .local v8, "e":Ljava/lang/Exception;
    :try_start_3
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTagInfo. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 144
    if-eqz v6, :cond_2

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 144
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method public hasDateInfo()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasLocationInfo()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 297
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 298
    const/4 v0, 0x1

    .line 300
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initVariable()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 92
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mLocation:Ljava/lang/String;

    .line 93
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mDate:Ljava/lang/String;

    .line 94
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLatitude:D

    .line 95
    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->dLongitude:D

    .line 96
    return-void
.end method

.method public sendTagBuddyInfoUpdateNotify()V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mTagBuddyUpdateListener:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mTagBuddyUpdateListener:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;->onTagBuddyUpdate()V

    .line 335
    :cond_0
    return-void
.end method

.method public setDateTagShowStatus(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 290
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDateTagShowStatus. visible : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    const/16 v0, 0x517

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->updatePerference(IZ)V

    .line 292
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowDateTag:Z

    .line 293
    return-void
.end method

.method public setLocationTagShowStatus(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 284
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLocationTagShowStatus. visible : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    const/16 v0, 0x516

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->updatePerference(IZ)V

    .line 286
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowLocationTag:Z

    .line 287
    return-void
.end method

.method public setTagBuddyShowStatus(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 278
    sget-object v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTagBuddyShowStatus. visible : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const/16 v0, 0x514

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->updatePerference(IZ)V

    .line 280
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mShowTagBuddy:Z

    .line 281
    return-void
.end method

.method public setTagBuddyUpdateListener(Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo;->mTagBuddyUpdateListener:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyInfo$OnTagBuddyUpdateListener;

    .line 341
    return-void
.end method
