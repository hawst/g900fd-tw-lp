.class Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;
.super Ljava/lang/Object;
.source "VideoFlickView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoFlickView;->leftFlickView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 134
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$400(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 132
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 105
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    const/4 v3, 0x0

    .line 91
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$300(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # ++operator for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickCounter:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$104(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)I

    move-result v2

    # invokes: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->getFlickText(I)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$200(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$400(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mLeftFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$400(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$1;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationleft:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$500(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 102
    return-void
.end method
