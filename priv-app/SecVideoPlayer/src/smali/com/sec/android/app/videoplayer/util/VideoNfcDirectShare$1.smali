.class Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;
.super Ljava/lang/Object;
.source "VideoNfcDirectShare.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 13
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 75
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "createNdefMessage"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v7, 0x0

    .line 80
    .local v7, "subtitleFilePath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "filePath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$200(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 83
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$200(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleFilePath()Ljava/lang/String;

    move-result-object v7

    .line 88
    :goto_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 90
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->isSbeamOn()Z
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$400(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 91
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "createNdefMessage :: S Beam !!"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$500(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/nfc/NfcAdapter;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v9, v10, v8}, Landroid/nfc/NfcAdapter;->setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V

    .line 94
    if-eqz v7, :cond_2

    .line 95
    const/4 v8, 0x2

    new-array v3, v8, [Ljava/lang/String;

    .line 96
    .local v3, "filepath":[Ljava/lang/String;
    const/4 v8, 0x0

    aput-object v1, v3, v8

    .line 97
    const/4 v8, 0x1

    aput-object v7, v3, v8

    .line 99
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 100
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage video : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v3, v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage subtitle : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v10, v3, v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :cond_0
    :goto_1
    new-instance v6, Landroid/nfc/NdefRecord;

    const/4 v8, 0x2

    const-string v9, "text/DirectShareVideos"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    new-array v10, v10, [B

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const-string v12, "text/DirectShareVideos"

    # invokes: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->getJsonStr(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v11, v12, v3}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$600(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->getBytes()[B

    move-result-object v11

    invoke-direct {v6, v8, v9, v10, v11}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    .line 116
    .local v6, "record":Landroid/nfc/NdefRecord;
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v6}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v8

    invoke-direct {v5, v8}, Ljava/lang/String;-><init>([B)V

    .line 119
    .local v5, "payload":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 121
    .local v4, "i":Landroid/content/Intent;
    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 122
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x2

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 123
    const-string v8, "POPUP_MODE"

    const-string v9, "no_file_selected"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 125
    const/4 v8, 0x0

    .line 202
    .end local v3    # "filepath":[Ljava/lang/String;
    .end local v4    # "i":Landroid/content/Intent;
    .end local v5    # "payload":Ljava/lang/String;
    .end local v6    # "record":Landroid/nfc/NdefRecord;
    :goto_2
    return-object v8

    .line 85
    :cond_1
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "createNdefMessage. subtitle path is NULL"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 104
    :cond_2
    const/4 v8, 0x1

    new-array v3, v8, [Ljava/lang/String;

    .line 105
    .restart local v3    # "filepath":[Ljava/lang/String;
    const/4 v8, 0x0

    aput-object v1, v3, v8

    .line 107
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 108
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage video : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v3, v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 126
    .restart local v4    # "i":Landroid/content/Intent;
    .restart local v5    # "payload":Ljava/lang/String;
    .restart local v6    # "record":Landroid/nfc/NdefRecord;
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$700(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDRM()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 127
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x4

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 128
    const-string v8, "POPUP_MODE"

    const-string v9, "from_drm_file"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 129
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 130
    const/4 v8, 0x0

    goto :goto_2

    .line 131
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$800(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 132
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x3

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 133
    const-string v8, "POPUP_MODE"

    const-string v9, "from_cloud_file"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 135
    const/4 v8, 0x0

    goto :goto_2

    .line 138
    :cond_5
    new-instance v8, Landroid/nfc/NdefMessage;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/nfc/NdefRecord;

    const/4 v10, 0x0

    aput-object v6, v9, v10

    const/4 v10, 0x1

    const-string v11, "com.sec.android.directshare"

    invoke-static {v11}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-direct {v8, v9}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto/16 :goto_2

    .line 145
    .end local v3    # "filepath":[Ljava/lang/String;
    .end local v4    # "i":Landroid/content/Intent;
    .end local v5    # "payload":Ljava/lang/String;
    .end local v6    # "record":Landroid/nfc/NdefRecord;
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$500(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/nfc/NfcAdapter;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 146
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "createNdefMessage :: Android Beam !!"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    if-eqz v7, :cond_8

    .line 149
    const/4 v8, 0x2

    new-array v2, v8, [Landroid/net/Uri;

    .line 150
    .local v2, "fileUris":[Landroid/net/Uri;
    const/4 v8, 0x0

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    aput-object v9, v2, v8

    .line 151
    const/4 v8, 0x1

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    aput-object v9, v2, v8

    .line 153
    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v8, :cond_7

    .line 154
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage video : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v2, v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage subtitle : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v10, v2, v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_7
    :goto_3
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 167
    .restart local v4    # "i":Landroid/content/Intent;
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$700(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDRM()Z

    move-result v8

    if-eqz v8, :cond_9

    .line 168
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x4

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 169
    const-string v8, "POPUP_MODE"

    const-string v9, "from_drm_file"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 171
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 158
    .end local v2    # "fileUris":[Landroid/net/Uri;
    .end local v4    # "i":Landroid/content/Intent;
    :cond_8
    const/4 v8, 0x1

    new-array v2, v8, [Landroid/net/Uri;

    .line 159
    .restart local v2    # "fileUris":[Landroid/net/Uri;
    const/4 v8, 0x0

    new-instance v9, Ljava/io/File;

    invoke-direct {v9, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    aput-object v9, v2, v8

    .line 161
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "createNdefMessage video : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x0

    aget-object v10, v2, v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 172
    .restart local v4    # "i":Landroid/content/Intent;
    :cond_9
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mSchemeType:Lcom/sec/android/app/videoplayer/type/VideoSchemeType;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$800(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 173
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    const/4 v9, 0x3

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNdefStatus:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$302(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;I)I

    .line 174
    const-string v8, "POPUP_MODE"

    const-string v9, "from_cloud_file"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 175
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 176
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 180
    :cond_a
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mNfcAdapter:Landroid/nfc/NfcAdapter;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$500(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/nfc/NfcAdapter;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1$1;

    invoke-direct {v10, p0, v2}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1$1;-><init>(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;[Landroid/net/Uri;)V

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare$1;->this$0:Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;->access$100(Lcom/sec/android/app/videoplayer/util/VideoNfcDirectShare;)Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v9, v10, v8}, Landroid/nfc/NfcAdapter;->setBeamPushUrisCallback(Landroid/nfc/NfcAdapter$CreateBeamUrisCallback;Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 202
    .end local v2    # "fileUris":[Landroid/net/Uri;
    .end local v4    # "i":Landroid/content/Intent;
    :cond_b
    :goto_4
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 194
    .restart local v2    # "fileUris":[Landroid/net/Uri;
    .restart local v4    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v0}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_4

    .line 196
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 197
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 198
    const/4 v8, 0x0

    goto/16 :goto_2
.end method
