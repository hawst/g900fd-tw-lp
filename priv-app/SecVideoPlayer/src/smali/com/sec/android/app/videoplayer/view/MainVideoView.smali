.class public Lcom/sec/android/app/videoplayer/view/MainVideoView;
.super Landroid/widget/RelativeLayout;
.source "MainVideoView.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;
.implements Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;,
        Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;,
        Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    }
.end annotation


# static fields
.field private static final ATTACH_AUDIO_ONLY_VIEW:I = 0x7

.field private static final BACK_KEY_EXPIRE_TIMEOUT:I = 0xa28

.field public static final DEFAULT_TIME_OUT:I = 0xbb8

.field private static final FADE_OUT:I = 0x1

.field private static final FFW_RWD_NOT_SUPPORT:I = 0x1e

.field private static final GESTURE_DELAY:J = 0x64L

.field private static final HANDLE_BACK_KEY:I = 0x3

.field private static final HIDE_SYSTEM_UI:I = 0x2

.field private static final MOTION_MOVE_GESTURE_ADJUSTMENT:I = 0x2

.field private static final MOTION_START_GESTURE_THRESHOLD:I = 0x3

.field private static final MSG_FROM_HANDLER:I = 0x0

.field private static final MSG_FROM_USER:I = 0x1

.field private static final RESTART_PROGRESS_HELP:I = 0x6

.field private static final START_GESTURE:I = 0x4

.field private static final START_LONGTOUCH:I = 0x8

.field private static final TAG:Ljava/lang/String; = "MainVideoView"

.field private static final TOGGLE_CONTROLS_VISIBILITY:I = 0x5

.field public static final TRANSLATE_TIME:I = 0x1f4


# instance fields
.field private mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

.field private mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

.field private mBackKeyTimer:I

.field private mBlockShowControllerMWmode:Z

.field public mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

.field private mBtnControllerStatus:Z

.field private mChangeView:Ljava/lang/Runnable;

.field private mChangeViewDone:Z

.field private mContext:Landroid/content/Context;

.field private mDownTime:J

.field private mDownYPos:I

.field public mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

.field private mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

.field private mGestureThreshold:I

.field private mHandler:Landroid/os/Handler;

.field private mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

.field private mIsCocktailBarVisible:Z

.field private mIsTitleShowed:Z

.field private mIsVideoGestureStart:Z

.field public mLongtouchflag:Z

.field private mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

.field public mOnFlingFlag:Z

.field private mOpenPopupPlayer:Z

.field private mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

.field private mRootView:Landroid/widget/RelativeLayout;

.field private mScaleDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleFactor:F

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

.field private mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

.field private mStatePercentView:Landroid/widget/TextView;

.field private mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

.field public mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

.field private mSubviewBtnControllerStatus:Z

.field private mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

.field public mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

.field private mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

.field private mTitleControllerStatus:Z

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

.field private mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

.field private mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

.field private mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

.field private mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

.field private mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

.field private mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

.field private mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

.field private mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

.field private mXTouchPos:I

.field private mYTouchPos:I

.field private mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 210
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 101
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 103
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOnFlingFlag:Z

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I

    .line 107
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 111
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 113
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 117
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 119
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 125
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 127
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    .line 133
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    .line 135
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 137
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    .line 143
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    .line 153
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    .line 157
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    .line 159
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnControllerStatus:Z

    .line 161
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    .line 163
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .line 165
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I

    .line 167
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I

    .line 169
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    .line 171
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureThreshold:I

    .line 175
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownTime:J

    .line 182
    sget-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .line 184
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 188
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 192
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z

    .line 194
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F

    .line 197
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    .line 199
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    .line 201
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 203
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsCocktailBarVisible:Z

    .line 207
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsTitleShowed:Z

    .line 438
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 1561
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    .line 1989
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$3;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    .line 211
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->initView()V

    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 215
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 216
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->initView()V

    .line 217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 220
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 101
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 103
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOnFlingFlag:Z

    .line 105
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I

    .line 107
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 109
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 111
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 113
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 115
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 117
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 119
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 123
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 125
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 127
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 129
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 131
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    .line 133
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    .line 135
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 137
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 139
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 141
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    .line 143
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    .line 145
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    .line 147
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .line 149
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    .line 151
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    .line 153
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .line 155
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    .line 157
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    .line 159
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnControllerStatus:Z

    .line 161
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    .line 163
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .line 165
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I

    .line 167
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I

    .line 169
    iput v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    .line 171
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureThreshold:I

    .line 175
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownTime:J

    .line 182
    sget-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .line 184
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 186
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 188
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    .line 190
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 192
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z

    .line 194
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F

    .line 197
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    .line 199
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    .line 201
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 203
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsCocktailBarVisible:Z

    .line 207
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsTitleShowed:Z

    .line 438
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 1561
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$2;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    .line 1989
    new-instance v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$3;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    .line 221
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->initView()V

    .line 222
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/view/ScaleGestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/view/MainVideoView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/videoplayer/view/MainVideoView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoCaptureView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureThreshold:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->createTalkBackOffDialog()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isNotificationAreaTouched(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/AudioOnlyView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoSurface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;
    .param p1, "x1"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/MainVideoView;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addPercentTextView(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    .line 2093
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 2094
    check-cast v1, Landroid/widget/RelativeLayout;

    .line 2095
    .local v1, "parentView":Landroid/widget/RelativeLayout;
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 2096
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->getId()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 2097
    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 2098
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    .line 2099
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2100
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09000e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 2101
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070077

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2102
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07006f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 2103
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    const v3, 0x7f070009

    invoke-virtual {v2, v6, v6, v6, v3}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 2105
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2106
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->bringToFront()V

    .line 2107
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showStateView()V

    .line 2110
    .end local v0    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v1    # "parentView":Landroid/widget/RelativeLayout;
    :goto_0
    return-void

    .line 2109
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    goto :goto_0
.end method

.method private applySettings(I)V
    .locals 3
    .param p1, "selected"    # I

    .prologue
    .line 2629
    packed-switch p1, :pswitch_data_0

    .line 2694
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 2631
    :pswitch_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_1

    .line 2632
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 2633
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateCaptureBtn()V

    goto :goto_0

    .line 2634
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    .line 2635
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateCaptureBtn()V

    goto :goto_0

    .line 2640
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPlaySpeedVisibility()I

    move-result v0

    .line 2641
    .local v0, "visible":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 2642
    sget v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->OFF:I

    if-ne v0, v1, :cond_2

    .line 2643
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->resetPlaySpeed()V

    .line 2644
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoPlaySpeedPopup;->getCurrentSpeed()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlaySpeed(I)V

    .line 2647
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v1, :cond_3

    .line 2648
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->updatePlaySpeedBtn()V

    goto :goto_0

    .line 2649
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_0

    .line 2650
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlaySpeedBtn()V

    goto :goto_0

    .line 2655
    .end local v0    # "visible":I
    :pswitch_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2656
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 2659
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_0

    .line 2660
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    goto :goto_0

    .line 2665
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v1, :cond_0

    .line 2666
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getSAEffectMode()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setSoundAliveMode(I)V

    goto :goto_0

    .line 2671
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_0

    .line 2672
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->changeControllerLayout(I)V

    goto/16 :goto_0

    .line 2677
    :pswitch_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v1, :cond_5

    .line 2678
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 2680
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_6

    .line 2681
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 2684
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    goto/16 :goto_0

    .line 2688
    :pswitch_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getPremiumMode()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/util/SecHWInterfaceWrapper;->setmDNIeUserMode(ILandroid/content/Context;)V

    goto/16 :goto_0

    .line 2629
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private blockHideController()Z
    .locals 2

    .prologue
    .line 1513
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->getInstance()Lcom/sec/android/app/videoplayer/preview/PreviewManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewManager;->isShowing()Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private blockShowController()Z
    .locals 1

    .prologue
    .line 1521
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createTalkBackOffDialog()V
    .locals 4

    .prologue
    .line 2596
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_0

    .line 2597
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const-string v3, "MainVideoView"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    .line 2599
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2600
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0148

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2601
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 2602
    const v2, 0x7f0a00dd

    new-instance v3, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$4;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2611
    const v2, 0x7f0a0026

    new-instance v3, Lcom/sec/android/app/videoplayer/view/MainVideoView$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView$5;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2617
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 2618
    .local v1, "dialog":Landroid/app/AlertDialog;
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 2619
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 2620
    return-void
.end method

.method private getDownTime()J
    .locals 2

    .prologue
    .line 2543
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownTime:J

    return-wide v0
.end method

.method private getResourceHeight()I
    .locals 4

    .prologue
    .line 2561
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0802dc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08017e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0802e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08015a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 2567
    .local v0, "resHeight":I
    return v0
.end method

.method private getResourceWidth()I
    .locals 5

    .prologue
    const v4, 0x7f08018f

    .line 2551
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080161

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08018c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080180

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080164

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 2557
    .local v0, "resWidth":I
    return v0
.end method

.method private initView()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    .line 226
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSettingInfo:Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSettingDataChangedListener(Lcom/sec/android/app/videoplayer/common/VideoSettingInfo$OnSettingDataChangedListener;)V

    .line 229
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 231
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setFocusable(Z)V

    .line 242
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setFocusableInTouchMode(Z)V

    .line 243
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestFocus()Z

    .line 247
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    .line 248
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 249
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 251
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureListener;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$1;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;-><init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    .line 253
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_TABLET:Z

    if-nez v0, :cond_1

    .line 254
    new-instance v0, Landroid/view/ScaleGestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView$ScaleListener;-><init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;

    .line 257
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    if-eqz v0, :cond_2

    .line 258
    new-instance v0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    .line 261
    :cond_2
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    .line 262
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 263
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    .line 264
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .line 265
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    .line 266
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    .line 267
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .line 269
    const/high16 v0, 0x40400000    # 3.0f

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureThreshold:I

    .line 271
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->TAG_BUDDY_INFO:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 272
    new-instance v0, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    .line 273
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->setVisibility(I)V

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addView(Landroid/view/View;)V

    .line 276
    :cond_3
    return-void
.end method

.method private isNotificationAreaTouched(I)Z
    .locals 3
    .param p1, "y"    # I

    .prologue
    .line 670
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    add-int/lit8 v0, v0, 0xa

    if-le p1, v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keyUpResetSeek(JI)Z
    .locals 3
    .param p1, "pressTime"    # J
    .param p3, "command"    # I

    .prologue
    .line 1265
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1268
    const-wide/16 v0, 0x1f4

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1269
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "short press. pressTime : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 1270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0, p3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1272
    :cond_0
    const/4 v0, 0x1

    .line 1276
    :goto_0
    return v0

    .line 1274
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 1275
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 1276
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public ChangeView()V
    .locals 6

    .prologue
    .line 1967
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachController()V

    .line 1968
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 1969
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    .line 1970
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setControllerUpdate()V

    .line 1971
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->resetLayout()V

    .line 1972
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setTitleName()V

    .line 1974
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1976
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_1

    .line 1987
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 1979
    .restart local v0    # "context":Landroid/content/Context;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-eqz v1, :cond_2

    .line 1980
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 1981
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x6

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1982
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1983
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto :goto_0

    .line 1984
    :cond_3
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_0

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1985
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto :goto_0
.end method

.method public DismissVideoVisualSeek()V
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->removeViewTo(Landroid/view/View;)V

    .line 730
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->video_dismiss()V

    .line 731
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 732
    return-void
.end method

.method public addAudioOnlyView()V
    .locals 2

    .prologue
    .line 2377
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 2378
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2380
    :cond_0
    return-void
.end method

.method public applyAllSettings()V
    .locals 2

    .prologue
    .line 2623
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSettingChangeAllList:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2624
    sget-object v1, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->mSettingChangeAllList:[I

    aget v1, v1, v0

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->applySettings(I)V

    .line 2623
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2626
    :cond_0
    return-void
.end method

.method public attachAudioOnlyView()V
    .locals 2

    .prologue
    .line 2318
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    if-nez v0, :cond_0

    .line 2319
    new-instance v0, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    .line 2320
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->addViewTo(Landroid/view/View;)V

    .line 2321
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 2323
    :cond_0
    return-void
.end method

.method public attachController()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 815
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 816
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 817
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAnchorView()V

    .line 820
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibility(I)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setAnchorView()V

    .line 828
    :cond_1
    :goto_0
    return-void

    .line 823
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibility(I)V

    .line 825
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setAnchorView()V

    .line 826
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setButtonArrange()V

    goto :goto_0
.end method

.method public attachHelpClip()V
    .locals 2

    .prologue
    .line 2369
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-nez v0, :cond_0

    .line 2370
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    .line 2371
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->addViewTo(Landroid/view/View;)V

    .line 2372
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->start()V

    .line 2374
    :cond_0
    return-void
.end method

.method public attachMotionPeekHelp()V
    .locals 2

    .prologue
    .line 2355
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2356
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    .line 2357
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->addViewTo(Landroid/view/View;)V

    .line 2358
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->start()V

    .line 2360
    :cond_0
    return-void
.end method

.method public attachProgressbarPreview()V
    .locals 2

    .prologue
    .line 2326
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2327
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .line 2328
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->addViewTo(Landroid/view/View;)V

    .line 2329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->start()V

    .line 2331
    :cond_0
    return-void
.end method

.method public attachSmartPause()V
    .locals 2

    .prologue
    .line 2341
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2342
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    .line 2343
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->addViewTo(Landroid/view/View;)V

    .line 2344
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->start()V

    .line 2346
    :cond_0
    return-void
.end method

.method public attachTVOutView()V
    .locals 2

    .prologue
    .line 2299
    new-instance v0, Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/TVOutView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    .line 2300
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->addViewTo(Landroid/view/View;)V

    .line 2301
    return-void
.end method

.method public attachVideoCaptureView()V
    .locals 2

    .prologue
    .line 2311
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-nez v0, :cond_0

    .line 2312
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    .line 2313
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->addViewTo(Landroid/view/View;)V

    .line 2315
    :cond_0
    return-void
.end method

.method public attachVideoGestureView()V
    .locals 2

    .prologue
    .line 2304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-nez v0, :cond_0

    .line 2305
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 2306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->addViewTo(Landroid/view/View;)V

    .line 2308
    :cond_0
    return-void
.end method

.method public changeLockStatus(Z)V
    .locals 11
    .param p1, "lockState"    # Z

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x2

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1795
    const-string v3, "MainVideoView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "changeLockStatus E. lockState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1797
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpMotionPeek()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1799
    :cond_0
    const-string v3, "MainVideoView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "changeLockStatus : Lock not supported on Help lockState = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1874
    :goto_0
    return-void

    .line 1803
    :cond_1
    const/4 v2, 0x1

    .line 1805
    .local v2, "isCtrlBtnEnable":Z
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1807
    .local v0, "configuration":Landroid/content/res/Configuration;
    if-nez p1, :cond_6

    .line 1809
    const/4 v2, 0x1

    .line 1810
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->setLockState(Z)V

    .line 1811
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    instance-of v3, v3, Landroid/app/Activity;

    if-eqz v3, :cond_2

    .line 1812
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 1814
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1815
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xbb

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1816
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v8, :cond_3

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v9, :cond_3

    .line 1818
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1821
    :cond_3
    invoke-virtual {p0, v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->statusBarSetting(Z)V

    .line 1823
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v3, :cond_4

    .line 1824
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->hideLockIcon()V

    .line 1826
    :cond_4
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    if-eqz v3, :cond_5

    .line 1827
    const-string v3, "MainVideoView"

    const-string v4, "changeLockStatus. show controller"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    .line 1831
    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->setRemoveSystemUI(Z)V

    .line 1867
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setEnabled(Z)V

    .line 1868
    const-string v3, "MainVideoView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "changeLockStatus(). VideoServiceUtil.getLockState() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.sec.android.app.videoplayer.PLAYER_LOCK"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1872
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "isLocked"

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1873
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1835
    .end local v1    # "i":Landroid/content/Intent;
    :cond_6
    const/4 v2, 0x0

    .line 1836
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->setLockState(Z)V

    .line 1838
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v3, :cond_7

    .line 1839
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1842
    :cond_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v8, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1843
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xbb

    invoke-virtual {v3, v4, v5, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1844
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v8, :cond_8

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v9, :cond_8

    .line 1846
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4, v10, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 1849
    :cond_8
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->statusBarSetting(Z)V

    .line 1851
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v3, :cond_9

    .line 1852
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 1854
    :cond_9
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceBtnRelease()V

    .line 1855
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeHandler()V

    .line 1857
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1858
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 1860
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_b

    .line 1861
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    .line 1864
    :cond_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/android/app/videoplayer/common/VUtils;->setRemoveSystemUI(Z)V

    goto/16 :goto_1
.end method

.method public fadeOutController(I)V
    .locals 4
    .param p1, "showTime"    # I

    .prologue
    const/4 v2, 0x1

    .line 1444
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1445
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1446
    return-void
.end method

.method public finishUpVideoGesture()Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x0

    .line 2442
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2443
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2445
    :cond_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 2447
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2448
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->hide()V

    .line 2449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v0, v1, :cond_1

    .line 2450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 2452
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .line 2453
    const/4 v0, 0x1

    .line 2457
    :goto_0
    return v0

    .line 2456
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    goto :goto_0
.end method

.method public forceBtnRelease()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1742
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1743
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setBtnPress(Z)V

    .line 1745
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 1746
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setBtnPress(Z)V

    .line 1750
    :cond_1
    :goto_0
    return-void

    .line 1747
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 1748
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setBtnPress(Z)V

    goto :goto_0
.end method

.method public forceHideController()V
    .locals 2

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1491
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->forceHide()V

    .line 1495
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1496
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->forceHide()V

    .line 1497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_1

    .line 1498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->hide()V

    .line 1501
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_2

    .line 1502
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->forceHide()V

    .line 1505
    :cond_2
    const-string v0, "MainVideoView"

    const-string v1, "setSystemUiVisibility - set hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1508
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1509
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1510
    :cond_3
    return-void
.end method

.method public getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;
    .locals 1

    .prologue
    .line 2284
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_0

    .line 2285
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 2287
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getBtnInstance()Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .locals 1

    .prologue
    .line 2270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 2271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 2273
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPeekHelpInstance()Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;
    .locals 1

    .prologue
    .line 2292
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    if-eqz v0, :cond_0

    .line 2293
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    .line 2295
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSubviewBtnController()Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .locals 1

    .prologue
    .line 2741
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    return-object v0
.end method

.method public getSurfaceViewGL()Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .locals 1

    .prologue
    .line 2026
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    return-object v0
.end method

.method public getTitleController()Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .locals 1

    .prologue
    .line 2737
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    return-object v0
.end method

.method public getTitleInstance()Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    .locals 1

    .prologue
    .line 2277
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 2278
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 2280
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoCaptureView()Lcom/sec/android/app/videoplayer/view/VideoCaptureView;
    .locals 1

    .prologue
    .line 2395
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    return-object v0
.end method

.method public getVideoLockCtrl()Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
    .locals 1

    .prologue
    .line 2725
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    return-object v0
.end method

.method public hasNavigationBar()Z
    .locals 1

    .prologue
    .line 2400
    const/4 v0, 0x0

    return v0
.end method

.method public hideAudioOnlyView()V
    .locals 2

    .prologue
    .line 2146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    if-eqz v0, :cond_0

    .line 2147
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->setVisibility(I)V

    .line 2148
    :cond_0
    return-void
.end method

.method public hideController()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1459
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->blockHideController()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1460
    const-string v0, "MainVideoView"

    const-string v1, "blockHideController - Don\'t Hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1485
    :cond_0
    :goto_0
    return-void

    .line 1464
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_2

    .line 1465
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide()V

    .line 1468
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_3

    .line 1469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->hide()V

    .line 1470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_3

    .line 1471
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->hide()V

    .line 1474
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_4

    .line 1475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hide()V

    .line 1478
    :cond_4
    const-string v0, "MainVideoView"

    const-string v1, "setSystemUiVisibility - set hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1481
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1483
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_0

    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->moveButtonDown(Z)V

    goto :goto_0
.end method

.method public hideMotionPeekHelpText()V
    .locals 1

    .prologue
    .line 2363
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    if-eqz v0, :cond_0

    .line 2364
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->hideHelpText()V

    .line 2366
    :cond_0
    return-void
.end method

.method public hideSmartPauseText()V
    .locals 1

    .prologue
    .line 2349
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    if-eqz v0, :cond_0

    .line 2350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->hideHelpText()V

    .line 2352
    :cond_0
    return-void
.end method

.method public hideStateView()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 2048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_0

    .line 2049
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 2051
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2052
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2053
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateStatePercentView(I)V

    .line 2056
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->OTHER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    .line 2057
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateControllerBtns()V

    .line 2058
    return-void
.end method

.method public hideTVOutView()V
    .locals 2

    .prologue
    .line 2246
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-eqz v0, :cond_0

    .line 2247
    const-string v0, "MainVideoView"

    const-string v1, "hideTVOutView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2248
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->hide()V

    .line 2250
    :cond_0
    return-void
.end method

.method public hideTagBuddy(Z)V
    .locals 1
    .param p1, "showAni"    # Z

    .prologue
    .line 2713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    if-eqz v0, :cond_0

    .line 2714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->hideTagBuddy(Z)V

    .line 2716
    :cond_0
    return-void
.end method

.method public invisibleStateView()V
    .locals 3

    .prologue
    const/4 v1, 0x4

    .line 2037
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_0

    .line 2038
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 2040
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 2041
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2043
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateControllerBtns()V

    .line 2044
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->OTHER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    .line 2045
    return-void
.end method

.method public isAdditionalControlButtonShowing()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1349
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1350
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->isBtnShow()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1352
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1350
    goto :goto_0

    .line 1352
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isBtnShow()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public isAudioOnlyViewShowing()Z
    .locals 1

    .prologue
    .line 2151
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    if-eqz v0, :cond_0

    .line 2152
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBlockShowControllerMWmode()Z
    .locals 1

    .prologue
    .line 1706
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    return v0
.end method

.method public isChangeViewDone()Z
    .locals 1

    .prologue
    .line 2158
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    return v0
.end method

.method public isCocktailBarVisible()Z
    .locals 1

    .prologue
    .line 2547
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsCocktailBarVisible:Z

    return v0
.end method

.method public isControls()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1290
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1291
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1293
    :cond_1
    :goto_0
    return v0

    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public isControlsShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1334
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControls()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1345
    :cond_0
    :goto_0
    return v0

    .line 1338
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1339
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1340
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v0

    goto :goto_0

    .line 1342
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isShowing()Z

    move-result v0

    goto :goto_0

    .line 1345
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOnLongSeekMode()Z
    .locals 1

    .prologue
    .line 2260
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_0

    .line 2261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isOnLongSeekMode()Z

    move-result v0

    .line 2266
    :goto_0
    return v0

    .line 2262
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 2263
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isOnLongSeekMode()Z

    move-result v0

    goto :goto_0

    .line 2266
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPossibleZoom()Z
    .locals 2

    .prologue
    .line 674
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isQcifOrLowerResClip()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 681
    :cond_2
    const/4 v0, 0x0

    .line 683
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isStateViewVisible()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2082
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v1, :cond_0

    .line 2083
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 2084
    const/4 v0, 0x1

    .line 2089
    :cond_0
    return v0
.end method

.method public isSurfaceExist()Z
    .locals 1

    .prologue
    .line 2019
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 2020
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getSurfaceExists()Z

    move-result v0

    .line 2022
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSurfaceTextureExist()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2030
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v1, :cond_0

    .line 2031
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 2033
    :cond_0
    return v0
.end method

.method public isTVOutViewVisible()Z
    .locals 1

    .prologue
    .line 2253
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-eqz v0, :cond_0

    .line 2254
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->isShowing()Z

    move-result v0

    .line 2256
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTitleCtrlShowing()Z
    .locals 1

    .prologue
    .line 1356
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v0

    return v0
.end method

.method public isTitleShowed()Z
    .locals 1

    .prologue
    .line 2729
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsTitleShowed:Z

    return v0
.end method

.method public isVolumeCtrlShowing()Z
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-nez v0, :cond_0

    .line 1361
    const/4 v0, 0x0

    .line 1364
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isVolumeBarShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public keepShowingController(I)V
    .locals 1
    .param p1, "timeout"    # I

    .prologue
    .line 1449
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1450
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    .line 1454
    :goto_0
    return-void

    .line 1452
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0
.end method

.method protected keyVolumeDown()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1225
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1226
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setVolume(Z)V

    .line 1241
    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    .line 1228
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v2, :cond_0

    .line 1229
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1230
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1231
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    goto :goto_1

    .line 1234
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeDown()V

    .line 1235
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupVolbar(Z)V

    .line 1236
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 1237
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_0
.end method

.method protected keyVolumeUp()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1245
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1246
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setVolume(Z)V

    :cond_0
    :goto_0
    move v1, v2

    .line 1261
    :goto_1
    return v1

    .line 1248
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_0

    .line 1249
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1250
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAllSoundOff(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1251
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeSame()V

    goto :goto_1

    .line 1254
    :cond_2
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeUp()V

    .line 1255
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupVolbar(Z)V

    .line 1256
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 1257
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 316
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v0, :cond_1

    .line 318
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->isLockBtnVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 320
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 326
    :goto_0
    return v0

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 324
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 326
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xbb8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v2, 0x1

    .line 839
    const-string v3, "MainVideoView"

    const-string v4, "onKeyDown"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 843
    .local v0, "configuration":Landroid/content/res/Configuration;
    const/16 v3, 0x1a

    if-eq p1, v3, :cond_1

    const/16 v3, 0x7a

    if-eq p1, v3, :cond_1

    const/16 v3, 0x18

    if-eq p1, v3, :cond_1

    const/16 v3, 0x19

    if-eq p1, v3, :cond_1

    const/16 v3, 0xa4

    if-eq p1, v3, :cond_1

    const/16 v3, 0x5b

    if-eq p1, v3, :cond_1

    const/16 v3, 0x55

    if-eq p1, v3, :cond_1

    const/16 v3, 0x57

    if-eq p1, v3, :cond_1

    const/16 v3, 0x58

    if-eq p1, v3, :cond_1

    const/16 v3, 0x56

    if-eq p1, v3, :cond_1

    const/16 v3, 0x59

    if-eq p1, v3, :cond_1

    const/16 v3, 0x5a

    if-eq p1, v3, :cond_1

    const/16 v3, 0x7e

    if-eq p1, v3, :cond_1

    const/16 v3, 0x7f

    if-eq p1, v3, :cond_1

    const/16 v3, 0x4f

    if-eq p1, v3, :cond_1

    const/16 v3, 0xa8

    if-eq p1, v3, :cond_1

    const/16 v3, 0x52

    if-eq p1, v3, :cond_1

    const/16 v3, 0xa9

    if-eq p1, v3, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1051
    :cond_0
    :goto_0
    :sswitch_0
    return v2

    .line 865
    :cond_1
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_2

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_2

    .line 867
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownTime:J

    .line 871
    :cond_2
    sparse-switch p1, :sswitch_data_0

    .line 1051
    :cond_3
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    .line 873
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    .line 878
    :sswitch_2
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_6

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_6

    .line 880
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-nez v3, :cond_4

    .line 881
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto :goto_0

    .line 883
    :cond_4
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 884
    const-string v3, "MainVideoView"

    const-string v4, "mRewKeyListener. allshare mode skip longseek"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 885
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 886
    .local v1, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 888
    .end local v1    # "msg":Landroid/os/Message;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_0

    .line 893
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1

    .line 897
    :sswitch_3
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_9

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_9

    .line 899
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-nez v3, :cond_7

    .line 900
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 902
    :cond_7
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 903
    const-string v3, "MainVideoView"

    const-string v4, "mFfKeyListener. allshare mode skip longseek"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/16 v4, 0x1e

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 905
    .restart local v1    # "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 907
    .end local v1    # "msg":Landroid/os/Message;
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_0

    .line 912
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_1

    .line 917
    :sswitch_4
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_0

    .line 919
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 920
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 929
    :sswitch_5
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_c

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_c

    .line 931
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-nez v3, :cond_a

    .line 932
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 934
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_b

    .line 935
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setFocus()V

    .line 937
    :cond_b
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 941
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyVolumeUp()Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0

    .line 946
    :sswitch_6
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    if-ne v3, v7, :cond_e

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    if-ne v3, v6, :cond_e

    .line 948
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v3

    if-nez v3, :cond_d

    .line 949
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto/16 :goto_0

    .line 951
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setFocus()V

    .line 952
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 956
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyVolumeDown()Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0

    .line 962
    :sswitch_7
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyVolumeUp()Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0

    .line 968
    :sswitch_8
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyVolumeDown()Z

    move-result v3

    if-eqz v3, :cond_3

    goto/16 :goto_0

    .line 973
    :sswitch_9
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    if-eqz v3, :cond_3

    .line 975
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v3

    const/16 v4, 0x221

    if-ne v3, v4, :cond_0

    .line 979
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 980
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v3, :cond_0

    .line 981
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setVolume(Z)V

    goto/16 :goto_0

    .line 985
    :cond_f
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v3

    if-eqz v3, :cond_11

    .line 986
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    .line 994
    :cond_10
    :goto_2
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    .line 995
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    .line 997
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeUp()V

    .line 998
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 987
    :cond_11
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_10

    .line 988
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v3

    if-nez v3, :cond_10

    .line 989
    :cond_12
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 990
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    goto :goto_2

    .line 1005
    :sswitch_a
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->HIGH_SPEED_PLAY:Z

    if-eqz v3, :cond_3

    .line 1007
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v3

    const/16 v4, 0x222

    if-ne v3, v4, :cond_0

    .line 1011
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 1012
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    if-eqz v3, :cond_0

    .line 1013
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setVolume(Z)V

    goto/16 :goto_0

    .line 1017
    :cond_13
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1018
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    .line 1026
    :cond_14
    :goto_3
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    .line 1027
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    .line 1029
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeDown()V

    .line 1030
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 1019
    :cond_15
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v3, :cond_14

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_14

    .line 1020
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_16

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->isShowing()Z

    move-result v3

    if-nez v3, :cond_14

    .line 1021
    :cond_16
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibility(I)V

    .line 1022
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setInvisibleControllers()V

    goto :goto_3

    .line 1038
    :sswitch_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_17

    .line 1039
    const-string v3, "MainVideoView"

    const-string v4, "onKeyDown :: DLNA MUTE"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1040
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAsfUtil:Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->setMute()V

    goto/16 :goto_0

    .line 1041
    :cond_17
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v3, :cond_0

    .line 1042
    const-string v3, "MainVideoView"

    const-string v4, "onKeyDown :: MUTE TOGGLE"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1043
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->makeToggleMute()V

    goto/16 :goto_0

    .line 871
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_5
        0x14 -> :sswitch_6
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x18 -> :sswitch_7
        0x19 -> :sswitch_8
        0x42 -> :sswitch_4
        0x52 -> :sswitch_1
        0x5b -> :sswitch_b
        0xa4 -> :sswitch_b
        0xa8 -> :sswitch_9
        0xa9 -> :sswitch_a
        0x117 -> :sswitch_8
        0x118 -> :sswitch_7
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1055
    const-string v4, "MainVideoView"

    const-string v5, "onKeyUp"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v4, :cond_0

    .line 1058
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1059
    const-string v4, "MainVideoView"

    const-string v5, "onKeyUp() - MoviePlayer isResumed() == false, not handle the KeyEvent"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    const/4 v4, 0x1

    .line 1221
    :goto_0
    return v4

    .line 1064
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 1066
    .local v0, "configuration":Landroid/content/res/Configuration;
    const/16 v4, 0x1a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x18

    if-eq p1, v4, :cond_2

    const/16 v4, 0x19

    if-eq p1, v4, :cond_2

    const/16 v4, 0xa4

    if-eq p1, v4, :cond_2

    const/16 v4, 0x5b

    if-eq p1, v4, :cond_2

    const/16 v4, 0x55

    if-eq p1, v4, :cond_2

    const/16 v4, 0x57

    if-eq p1, v4, :cond_2

    const/16 v4, 0x58

    if-eq p1, v4, :cond_2

    const/16 v4, 0x56

    if-eq p1, v4, :cond_2

    const/16 v4, 0x59

    if-eq p1, v4, :cond_2

    const/16 v4, 0x5a

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7e

    if-eq p1, v4, :cond_2

    const/16 v4, 0x7f

    if-eq p1, v4, :cond_2

    const/16 v4, 0x4f

    if-eq p1, v4, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1082
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v4, :cond_1

    .line 1083
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 1085
    :cond_1
    const/4 v4, 0x1

    goto :goto_0

    .line 1088
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 1090
    const-wide/16 v2, 0x0

    .line 1091
    .local v2, "pressTime":J
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 1095
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 1100
    :goto_1
    sparse-switch p1, :sswitch_data_0

    .line 1221
    :cond_3
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v4

    goto :goto_0

    .line 1097
    :cond_4
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    goto :goto_1

    .line 1103
    :sswitch_0
    const-wide/16 v4, 0x1f4

    cmp-long v4, v2, v4

    if-gez v4, :cond_5

    .line 1104
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1105
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->changeLockStatus(Z)V

    .line 1110
    :cond_5
    :goto_3
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1107
    :cond_6
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->changeLockStatus(Z)V

    goto :goto_3

    .line 1113
    :sswitch_1
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1115
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->destroyCaptureView()V

    .line 1116
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1119
    :cond_7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1120
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1132
    :cond_8
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x3

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    .line 1133
    .local v1, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1135
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1138
    .end local v1    # "msg":Landroid/os/Message;
    :sswitch_2
    iget-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    if-nez v4, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1140
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1143
    :cond_9
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelp()Z

    move-result v4

    if-nez v4, :cond_b

    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1144
    :cond_b
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1147
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v4

    if-nez v4, :cond_d

    .line 1148
    const/16 v4, 0xbb8

    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1151
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->hasHwMenukey()Z

    move-result v4

    if-nez v4, :cond_e

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v4, :cond_e

    .line 1152
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupMenu()V

    .line 1156
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1154
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1161
    :sswitch_3
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 1165
    :sswitch_4
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_11

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_11

    .line 1167
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v4, :cond_10

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1168
    const-wide/16 v4, 0x1f4

    cmp-long v4, v2, v4

    if-gez v4, :cond_f

    .line 1169
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1170
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1175
    :cond_f
    :goto_4
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1173
    :cond_10
    const/16 v4, 0xd

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyUpResetSeek(JI)Z

    goto :goto_4

    .line 1178
    :cond_11
    const/16 v4, 0xd

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1179
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1184
    :sswitch_5
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_14

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_14

    .line 1186
    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v4, :cond_13

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1187
    const-wide/16 v4, 0x1f4

    cmp-long v4, v2, v4

    if-gez v4, :cond_12

    .line 1188
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x1e

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1189
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1194
    :cond_12
    :goto_5
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1192
    :cond_13
    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyUpResetSeek(JI)Z

    goto :goto_5

    .line 1196
    :cond_14
    const/16 v4, 0xc

    invoke-direct {p0, v2, v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->keyUpResetSeek(JI)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1197
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1203
    :sswitch_6
    iget v4, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    iget v4, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 1205
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1206
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v4, :cond_15

    .line 1207
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 1209
    :cond_15
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1215
    :sswitch_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1100
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_6
        0x14 -> :sswitch_6
        0x15 -> :sswitch_4
        0x16 -> :sswitch_5
        0x17 -> :sswitch_3
        0x18 -> :sswitch_7
        0x19 -> :sswitch_7
        0x1a -> :sswitch_0
        0x3e -> :sswitch_3
        0x42 -> :sswitch_3
        0x52 -> :sswitch_2
        0x7a -> :sswitch_0
    .end sparse-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 751
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 752
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->onPause()V

    .line 760
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 762
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_1

    .line 763
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler(Z)V

    .line 764
    :cond_1
    return-void

    .line 753
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->onPause()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 735
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBackKeyTimer:I

    .line 737
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 739
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 740
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->onResume()V

    .line 745
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_1

    .line 746
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->reStart()V

    .line 748
    :cond_1
    return-void

    .line 741
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->onResume()V

    goto :goto_0
.end method

.method public onSettingDataChanged(I)V
    .locals 3
    .param p1, "selected"    # I

    .prologue
    .line 2698
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSettingDataChanged E. selected : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2699
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_0

    .line 2700
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->applyAllSettings()V

    .line 2704
    :goto_0
    return-void

    .line 2702
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->applySettings(I)V

    goto :goto_0
.end method

.method public onSurfaceSizeChanged()V
    .locals 0

    .prologue
    .line 2708
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 2709
    return-void
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 831
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    .line 835
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public prepareChangeView(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/16 v5, 0xd

    const/4 v4, -0x1

    .line 1907
    const-string v2, "MainVideoView"

    const-string v3, "prepareChangeView() : start!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1909
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 1910
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 1912
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-eqz v2, :cond_0

    .line 1913
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->changeCaptureViewLayout()V

    .line 1915
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-eqz v2, :cond_1

    .line 1916
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->removeAllView()V

    .line 1919
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeAllInController()V

    .line 1920
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->requestLayout()V

    .line 1922
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1923
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getSYSTEM_UI_FLAG_REMOVE_NAVIGATION()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1926
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_3

    .line 1927
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1929
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1932
    .end local v0    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v2, :cond_4

    .line 1933
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1935
    .local v1, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1936
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1939
    .end local v1    # "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v2, :cond_5

    .line 1940
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1942
    .restart local v1    # "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1943
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1946
    .end local v1    # "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isAudioOnlyViewShowing()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isHLS()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1947
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->setImage()V

    .line 1950
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v2, :cond_7

    .line 1951
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 1954
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 1955
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceBtnRelease()V

    .line 1956
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeHandler()V

    .line 1958
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v2, :cond_8

    .line 1959
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayoutDefaultPosition()V

    .line 1962
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1963
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeView:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1964
    return-void
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2471
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v0, :cond_0

    .line 2472
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->releaseView()V

    .line 2473
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    .line 2476
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_1

    .line 2477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->releaseView()V

    .line 2478
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 2481
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v0, :cond_2

    .line 2482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->releaseView()V

    .line 2483
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 2486
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-eqz v0, :cond_3

    .line 2487
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->releaseView()V

    .line 2488
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    .line 2491
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-eqz v0, :cond_4

    .line 2492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->releaseView()V

    .line 2494
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    if-eqz v0, :cond_5

    .line 2495
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->releaseView()V

    .line 2497
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    if-eqz v0, :cond_6

    .line 2498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->releaseView()V

    .line 2500
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_7

    .line 2501
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->releaseView()V

    .line 2503
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    if-eqz v0, :cond_8

    .line 2504
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->releaseView()V

    .line 2506
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-eqz v0, :cond_9

    .line 2507
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->releaseView()V

    .line 2509
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_a

    .line 2510
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->releaseView()V

    .line 2511
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 2514
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_b

    .line 2515
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->releaseView()V

    .line 2516
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 2519
    :cond_b
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_c

    .line 2520
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->releaseView()V

    .line 2521
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 2524
    :cond_c
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_d

    .line 2525
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->releaseView()V

    .line 2526
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 2529
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    if-eqz v0, :cond_e

    .line 2530
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->releaseView()V

    .line 2533
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 2534
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 2536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2537
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 2539
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->removeAllViews()V

    .line 2540
    return-void
.end method

.method public removeAllInController()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1297
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1298
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hide(Z)V

    .line 1299
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->removeAllViewsInLayout()V

    .line 1302
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 1303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->hide(Z)V

    .line 1304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeAllViewsInLayout()V

    .line 1307
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 1308
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->hide(Z)V

    .line 1309
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeAllViewsInLayout()V

    .line 1312
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 1314
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$myView;->CONTROLLER_VIEW:Lcom/sec/android/app/videoplayer/common/VUtils$myView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setNullBackgroundColor(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/VUtils$myView;)V

    .line 1317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z

    .line 1320
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1321
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->hide()V

    .line 1322
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v0, v1, :cond_3

    .line 1323
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->syncBrightnessWithSystemLevel()V

    .line 1328
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_4

    .line 1329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 1331
    :cond_4
    return-void
.end method

.method public removeControllerFadeOut()V
    .locals 2

    .prologue
    .line 1530
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1531
    return-void
.end method

.method public removeHandler()V
    .locals 1

    .prologue
    .line 1753
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1754
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->removeHandler()V

    .line 1757
    :cond_0
    :goto_0
    return-void

    .line 1755
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1756
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->removeHandler()V

    goto :goto_0
.end method

.method public reorderViews()V
    .locals 1

    .prologue
    .line 2166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 2167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->bringToFront()V

    .line 2169
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v0, :cond_1

    .line 2170
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->bringToFront()V

    .line 2172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    if-eqz v0, :cond_2

    .line 2173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->bringToFront()V

    .line 2175
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_3

    .line 2176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->bringToFront()V

    .line 2178
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-eqz v0, :cond_4

    .line 2179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->bringToFront()V

    .line 2181
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    if-eqz v0, :cond_5

    .line 2182
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->bringToFront()V

    .line 2184
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_6

    .line 2185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->bringToFront()V

    .line 2187
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_7

    .line 2188
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->bringToFront()V

    .line 2190
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_8

    .line 2191
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->bringToFront()V

    .line 2193
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    if-eqz v0, :cond_9

    .line 2194
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 2196
    :cond_9
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    if-eqz v0, :cond_a

    .line 2197
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->bringToFront()V

    .line 2199
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-eqz v0, :cond_b

    .line 2200
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->bringToFront()V

    .line 2202
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    if-eqz v0, :cond_c

    .line 2203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSmartPauseHelpView:Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/SmartPauseHelp;->bringToFront()V

    .line 2205
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    if-eqz v0, :cond_d

    .line 2206
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mMotionPeekHelpView:Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/MotionPeekHelp;->bringToFront()V

    .line 2208
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_e

    .line 2209
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->bringToFront()V

    .line 2211
    :cond_e
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    if-eqz v0, :cond_f

    .line 2212
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->bringToFront()V

    .line 2214
    :cond_f
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    if-eqz v0, :cond_10

    .line 2215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->bringToFront()V

    .line 2218
    :cond_10
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    if-eqz v0, :cond_11

    .line 2219
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->bringToFront()V

    .line 2222
    :cond_11
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    if-eqz v0, :cond_12

    .line 2223
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->bringToFront()V

    .line 2226
    :cond_12
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v0, :cond_13

    .line 2227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->bringToFront()V

    .line 2229
    :cond_13
    return-void
.end method

.method public resetLongSeekHold()V
    .locals 1

    .prologue
    .line 1996
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1997
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->resetHoldLongSeek()V

    .line 2001
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updatePopupPlayerBtn()V

    .line 2002
    return-void

    .line 1998
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1999
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->resetHoldLongSeek()V

    goto :goto_0
.end method

.method public resetZoom()V
    .locals 1

    .prologue
    .line 2461
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 2462
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    if-eqz v0, :cond_0

    .line 2464
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->setZoomReset()V

    .line 2468
    :cond_0
    return-void
.end method

.method public restartProgressbarPreview()V
    .locals 1

    .prologue
    .line 2334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    if-eqz v0, :cond_0

    .line 2335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->addViewTo(Landroid/view/View;)V

    .line 2336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mProgressbarPreview:Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->restart()V

    .line 2338
    :cond_0
    return-void
.end method

.method public runOrStopHelpClip()V
    .locals 1

    .prologue
    .line 2383
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_0

    .line 2384
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->runOrStopHandler()V

    .line 2386
    :cond_0
    return-void
.end method

.method public setAdditionalViewToChildView(Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;)V
    .locals 0
    .param p1, "additionalView"    # Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .prologue
    .line 310
    if-eqz p1, :cond_0

    .line 311
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    .line 313
    :cond_0
    return-void
.end method

.method public setAudioShockWarningEnabled()V
    .locals 1

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v0, :cond_0

    .line 2430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setAudioShockWarningEnabled()V

    .line 2433
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_1

    .line 2434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setAudioShockWarningEnabled()V

    .line 2437
    :cond_1
    return-void
.end method

.method public setChangeViewDone(Z)V
    .locals 0
    .param p1, "mChangeViewDone"    # Z

    .prologue
    .line 2162
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z

    .line 2163
    return-void
.end method

.method public setChildViewReferences(Lcom/sec/android/app/videoplayer/view/VideoSurface;Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;Lcom/sec/android/app/videoplayer/view/VideoStateView;Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;Lcom/sec/android/app/videoplayer/view/VideoFlickView;)V
    .locals 1
    .param p1, "surface"    # Lcom/sec/android/app/videoplayer/view/VideoSurface;
    .param p2, "surfacegl"    # Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    .param p3, "state"    # Lcom/sec/android/app/videoplayer/view/VideoStateView;
    .param p4, "subtitle"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;
    .param p5, "lockCtrl"    # Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
    .param p6, "flickView"    # Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 288
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->setSurfaceSizeChangedListener(Lcom/sec/android/app/videoplayer/view/VideoSurface$OnSurafceSizeChangedListener;)V

    .line 293
    :cond_0
    if-eqz p3, :cond_1

    .line 294
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    .line 295
    invoke-direct {p0, p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->addPercentTextView(Landroid/view/View;)V

    .line 298
    :cond_1
    if-eqz p4, :cond_2

    .line 299
    iput-object p4, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    .line 301
    :cond_2
    if-eqz p5, :cond_3

    .line 302
    iput-object p5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    .line 304
    :cond_3
    if-eqz p6, :cond_4

    .line 305
    iput-object p6, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mFlickView:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    .line 307
    :cond_4
    return-void
.end method

.method public setControllerPlayerStop()V
    .locals 1

    .prologue
    .line 804
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_0

    .line 805
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->resetZoom()V

    .line 806
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 807
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->playerStop()V

    .line 812
    :cond_0
    :goto_0
    return-void

    .line 809
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->playerStop()V

    goto :goto_0
.end method

.method public setControllerProgress()V
    .locals 1

    .prologue
    .line 2012
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 2013
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setProgress()I

    .line 2016
    :cond_0
    :goto_0
    return-void

    .line 2014
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 2015
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setProgress()I

    goto :goto_0
.end method

.method public setControllerUpdate()V
    .locals 2

    .prologue
    .line 767
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControls()Z

    move-result v0

    if-nez v0, :cond_0

    .line 768
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ALLSHARE_FRAMEWORK:Z

    if-eqz v0, :cond_2

    .line 769
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 770
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 771
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setUpdate()V

    .line 776
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateChangePlayerButton()V

    .line 794
    :cond_0
    :goto_1
    return-void

    .line 773
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setUpdate()V

    goto :goto_0

    .line 781
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 782
    const-string v0, "MainVideoView"

    const-string v1, "setControllerUpdate - previous status is pause."

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 784
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 788
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_4

    .line 789
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setUpdate()V

    goto :goto_1

    .line 791
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setUpdate()V

    goto :goto_1
.end method

.method public setHelpClip4SeekMode(I)V
    .locals 1
    .param p1, "seekPos"    # I

    .prologue
    .line 2389
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v0, :cond_0

    .line 2390
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->setDescriptionAndTitle4SeekMode(I)V

    .line 2392
    :cond_0
    return-void
.end method

.method public setInvisibleControllers()V
    .locals 1

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1535
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setInvisibleAllViews()V

    .line 1538
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setInvisibleAllViews()V

    .line 1542
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_2

    .line 1543
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setInvisibleAllViews()V

    .line 1545
    :cond_2
    return-void
.end method

.method public setRootViewReference(Landroid/widget/RelativeLayout;)V
    .locals 0
    .param p1, "rootView"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 279
    if-eqz p1, :cond_0

    .line 280
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mRootView:Landroid/widget/RelativeLayout;

    .line 281
    :cond_0
    return-void
.end method

.method public setScreenSizeBtnEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1721
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1722
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 1723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1728
    :cond_0
    :goto_0
    return-void

    .line 1724
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1725
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setClickable(Z)V

    .line 1726
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->getFitToSrcBtn()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setSpeedTextViewVisibility(ZI)V
    .locals 2
    .param p1, "visibility"    # Z
    .param p2, "speed"    # I

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1761
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    int-to-float v1, p2

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setSpeedTextViewVisibility(ZF)V

    .line 1762
    :cond_0
    return-void
.end method

.method public setTitleName()V
    .locals 2

    .prologue
    .line 797
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 799
    .local v0, "context":Landroid/content/Context;
    instance-of v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v1, :cond_0

    .line 800
    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateTitle()V

    .line 801
    :cond_0
    return-void
.end method

.method public setTitleShowed(Z)V
    .locals 0
    .param p1, "showed"    # Z

    .prologue
    .line 2733
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsTitleShowed:Z

    .line 2734
    return-void
.end method

.method public setTitleVolumeControl(Z)V
    .locals 2
    .param p1, "isCheck"    # Z

    .prologue
    .line 1782
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1783
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    if-eqz p1, :cond_1

    .line 1784
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeUp()V

    .line 1788
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    .line 1789
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateVolume()V

    .line 1791
    :cond_0
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1792
    return-void

    .line 1786
    :cond_1
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->volumeDown()V

    goto :goto_0
.end method

.method public setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 1
    .param p1, "btnctrl"    # Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
    .param p2, "titlectrl"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 688
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 691
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    .line 692
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 694
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachController()V

    .line 695
    return-void
.end method

.method public setVideoController(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 1
    .param p1, "subviewbtnctrl"    # Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
    .param p2, "titlectrl"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 702
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    .line 703
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 705
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachController()V

    .line 706
    return-void
.end method

.method public setVideoVisualSeek(ILandroid/net/Uri;)V
    .locals 3
    .param p1, "pos"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    if-nez v0, :cond_0

    .line 710
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    if-eqz v0, :cond_1

    .line 714
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setCurrentUri(Landroid/net/Uri;)V

    .line 715
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setCurrentPosition(I)V

    .line 717
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->addViewTo(Landroid/view/View;II)V

    .line 719
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 720
    return-void
.end method

.method public setViewEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1731
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1732
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setEnabled(Z)V

    .line 1734
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 1735
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setEnabled(Z)V

    .line 1739
    :cond_1
    :goto_0
    return-void

    .line 1736
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 1737
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setVisibleControllers()V
    .locals 1

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1549
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->setVisibleAllViews()V

    .line 1552
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_1

    .line 1553
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setVisibleAllViews()V

    .line 1556
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_2

    .line 1557
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVisibleAllViews()V

    .line 1559
    :cond_2
    return-void
.end method

.method public showAllshareControls(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 1710
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_2

    .line 1711
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->showAllshareBtnControl(Z)V

    .line 1716
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_1

    .line 1717
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showAllshareTitleControl(Z)V

    .line 1718
    :cond_1
    return-void

    .line 1712
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->showAllshareBtnControl(Z)V

    goto :goto_0
.end method

.method public showAudioOnlyView()V
    .locals 2

    .prologue
    .line 2139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    if-eqz v0, :cond_0

    .line 2140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->setImage()V

    .line 2141
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mAudioOnlyView:Lcom/sec/android/app/videoplayer/view/AudioOnlyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/AudioOnlyView;->setVisibility(I)V

    .line 2143
    :cond_0
    return-void
.end method

.method public showController()V
    .locals 1

    .prologue
    .line 1368
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1369
    return-void
.end method

.method public showController(I)V
    .locals 5
    .param p1, "showTime"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1372
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1374
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1375
    .local v0, "context":Landroid/content/Context;
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 1376
    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local v0    # "context":Landroid/content/Context;
    iget-object v1, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1436
    :cond_0
    :goto_0
    return-void

    .line 1381
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->blockShowController()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1382
    const-string v1, "MainVideoView"

    const-string v2, "blockShowController - Don\'t show!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelp()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1385
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->setMoviePlayerWindowFocus(Z)V

    goto :goto_0

    .line 1390
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1391
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    .line 1396
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v1, :cond_4

    .line 1397
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    if-nez v1, :cond_9

    .line 1398
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->show()V

    .line 1403
    :cond_4
    :goto_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    if-eqz v1, :cond_5

    .line 1404
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnControllerStatus:Z

    if-nez v1, :cond_a

    .line 1405
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->show()V

    .line 1406
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoAdditionalView:Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->show()V

    .line 1411
    :cond_5
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_6

    .line 1412
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    if-nez v1, :cond_b

    .line 1413
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->show()V

    .line 1418
    :cond_6
    :goto_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 1420
    const-string v1, "MainVideoView"

    const-string v2, "setSystemUiVisibility - set show!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hasNavigationBar()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1423
    const/16 v1, 0x200

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setSystemUiVisibility(I)V

    .line 1428
    :cond_7
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->fadeOutController(I)V

    .line 1430
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    if-eqz v1, :cond_8

    .line 1431
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHelpClipView:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->moveButtonDown(Z)V

    .line 1433
    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1434
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setTitleShowed(Z)V

    goto/16 :goto_0

    .line 1400
    :cond_9
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnControllerStatus:Z

    goto :goto_1

    .line 1408
    :cond_a
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnControllerStatus:Z

    goto :goto_2

    .line 1415
    :cond_b
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleControllerStatus:Z

    goto :goto_3
.end method

.method public showStateView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2061
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2062
    const-string v0, "MainVideoView"

    const-string v1, "showStateView : State Mode should not show on DLNA mode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2079
    :cond_0
    :goto_0
    return-void

    .line 2066
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2069
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    if-eqz v0, :cond_2

    .line 2070
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoStateView:Lcom/sec/android/app/videoplayer/view/VideoStateView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoStateView;->setVisibility(I)V

    .line 2072
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 2073
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2074
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateStatePercentView(I)V

    .line 2077
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateControllerBtns()V

    .line 2078
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$Color;->BLACK:Lcom/sec/android/app/videoplayer/common/VUtils$Color;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->changeWindowBackgroundColor(Lcom/sec/android/app/videoplayer/common/VUtils$Color;)V

    goto :goto_0
.end method

.method public showTVOutView()V
    .locals 2

    .prologue
    .line 2232
    const-string v0, "MainVideoView"

    const-string v1, "showTVOutView()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2234
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-nez v0, :cond_0

    .line 2235
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachTVOutView()V

    .line 2238
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    if-eqz v0, :cond_1

    .line 2239
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTVOutView:Lcom/sec/android/app/videoplayer/view/TVOutView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/TVOutView;->show()V

    .line 2241
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->reorderViews()V

    .line 2243
    :cond_1
    return-void
.end method

.method public showTagBuddy(J)V
    .locals 1
    .param p1, "videoId"    # J

    .prologue
    .line 2719
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    if-eqz v0, :cond_0

    .line 2720
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTagBuddyView:Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/app/videoplayer/tagbuddy/VideoTagBuddyView;->showTagBuddyInfo(J)V

    .line 2722
    :cond_0
    return-void
.end method

.method public startShowGesture()V
    .locals 3

    .prologue
    .line 2404
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 2406
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2407
    .local v0, "context":Landroid/content/Context;
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    check-cast v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .end local v0    # "context":Landroid/content/Context;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPlayerListShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2426
    :cond_0
    :goto_0
    return-void

    .line 2413
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-nez v1, :cond_2

    .line 2414
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->attachVideoGestureView()V

    .line 2416
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    if-eqz v1, :cond_0

    .line 2417
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2418
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 2420
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    sget-object v2, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v1, v2, :cond_4

    .line 2421
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->showVolume()V

    goto :goto_0

    .line 2423
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->showBrightness()V

    goto :goto_0
.end method

.method public statusBarSetting(Z)V
    .locals 6
    .param p1, "request"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1877
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1880
    .local v1, "set":Z
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "statusbar"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/StatusBarManager;

    .line 1882
    .local v2, "statusBar":Landroid/app/StatusBarManager;
    if-eqz v1, :cond_2

    .line 1883
    const-string v3, "MainVideoView"

    const-string v4, " statusBarSetting disable statusBar"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1884
    const/high16 v3, 0x1210000

    invoke-virtual {v2, v3}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1888
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v3, :cond_0

    .line 1889
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const/16 v4, 0x309

    const-string v5, "MULTIWINDOW_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(ILjava/lang/String;)V

    .line 1891
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const/16 v4, 0x308

    const-string v5, "SAMSUNG_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->unsetWindowFlag(ILjava/lang/String;)V

    .line 1904
    .end local v2    # "statusBar":Landroid/app/StatusBarManager;
    :goto_1
    return-void

    .end local v1    # "set":Z
    :cond_1
    move v1, p1

    .line 1877
    goto :goto_0

    .line 1893
    .restart local v1    # "set":Z
    .restart local v2    # "statusBar":Landroid/app/StatusBarManager;
    :cond_2
    const-string v3, "MainVideoView"

    const-string v4, " statusBarSetting enable statusBar"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1894
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/StatusBarManager;->disable(I)V

    .line 1896
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v3, :cond_3

    .line 1897
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const/16 v4, 0x309

    const-string v5, "MULTIWINDOW_FLAG_DISABLE_MULTI_WINDOW_TRAY_BAR"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->unsetWindowFlag(ILjava/lang/String;)V

    .line 1899
    :cond_3
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    const/16 v4, 0x308

    const-string v5, "SAMSUNG_FLAG_ENABLE_STATUSBAR_OPEN_BY_NOTIFICATION"

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->setWindowFlag(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1901
    .end local v2    # "statusBar":Landroid/app/StatusBarManager;
    :catch_0
    move-exception v0

    .line 1902
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_1
.end method

.method public toggleControlsVisiblity()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1281
    const-string v0, "MainVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "toggleControlsVisiblity - isControlsShowing() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsCocktailBarVisible:Z

    if-eqz v0, :cond_0

    .line 1287
    :goto_0
    return-void

    .line 1285
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1286
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public updateAllShareBtn()V
    .locals 1

    .prologue
    .line 1765
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 1766
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateAllShareBtn()V

    .line 1768
    :cond_0
    return-void
.end method

.method public updateBtnForKDRM()V
    .locals 2

    .prologue
    .line 1771
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_L_SCREEN_CONCEPT:Z

    if-eqz v0, :cond_0

    .line 1772
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePlayListBtn()V

    .line 1776
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_1

    .line 1777
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateKDrmLayout(I)V

    .line 1779
    :cond_1
    return-void
.end method

.method public updateControllerBtns()V
    .locals 1

    .prologue
    .line 2124
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_0

    .line 2125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateTitleControllerBtns()V

    .line 2128
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    if-eqz v0, :cond_3

    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubviewBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->updateBtnControllerBtns()V

    .line 2134
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2135
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateSubtitleLayout()V

    .line 2136
    :cond_2
    return-void

    .line 2130
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_1

    .line 2131
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updateBtnControllerBtns()V

    goto :goto_0
.end method

.method public updatePopupPlayerBtn()V
    .locals 1

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    if-eqz v0, :cond_0

    .line 2006
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBtnController:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->updatePopupPlayerBtn()V

    .line 2007
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v0, :cond_1

    .line 2008
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updatePopupPlayerBtn()V

    .line 2009
    :cond_1
    return-void
.end method

.method public updateScreen(II)V
    .locals 9
    .param p1, "windowWidth"    # I
    .param p2, "windowHeight"    # I

    .prologue
    const v7, 0x7f08018f

    const/4 v8, 0x1

    .line 1654
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    .line 1656
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1657
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v4

    .line 1659
    .local v4, "screenMode":I
    if-ne v4, v8, :cond_1

    .line 1660
    const/4 v4, 0x0

    .line 1661
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    if-eqz v5, :cond_0

    .line 1662
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->requestLayout()V

    .line 1664
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v5, :cond_1

    .line 1665
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->requestLayout()V

    .line 1669
    :cond_1
    const/4 v3, 0x0

    .line 1670
    .local v3, "resWidth":I
    const/4 v2, 0x0

    .line 1672
    .local v2, "resHeight":I
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;

    invoke-direct {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;-><init>()V

    .line 1673
    .local v0, "mMultiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    new-instance v1, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-direct {v1, v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 1675
    .local v1, "mMultiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->isFeatureEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindow;->getVersionCode()I

    move-result v5

    if-lez v5, :cond_5

    .line 1676
    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v5

    if-nez v5, :cond_2

    .line 1677
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResourceWidth()I

    move-result v3

    .line 1678
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResourceHeight()I

    move-result v2

    .line 1696
    :cond_2
    :goto_0
    const-string v5, "MainVideoView"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "updateScreen : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1698
    if-gt v3, p1, :cond_3

    if-le v2, p2, :cond_4

    .line 1699
    :cond_3
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mBlockShowControllerMWmode:Z

    .line 1700
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->forceHideController()V

    .line 1703
    .end local v0    # "mMultiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    .end local v1    # "mMultiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .end local v2    # "resHeight":I
    .end local v3    # "resWidth":I
    .end local v4    # "screenMode":I
    :cond_4
    return-void

    .line 1681
    .restart local v0    # "mMultiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindow;
    .restart local v1    # "mMultiWindowActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .restart local v2    # "resHeight":I
    .restart local v3    # "resWidth":I
    .restart local v4    # "screenMode":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 1682
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08018c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080164

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v3, v5

    goto :goto_0

    .line 1688
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0802dc

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0802e5

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f08017e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0802e3

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    float-to-int v2, v5

    goto/16 :goto_0
.end method

.method public updateStatePercentView(I)V
    .locals 3
    .param p1, "percent"    # I

    .prologue
    .line 2113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 2114
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2115
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    const-string v1, "0%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2121
    :cond_0
    :goto_0
    return-void

    .line 2116
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2119
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mStatePercentView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateSubtitleLayout()V
    .locals 1

    .prologue
    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    if-eqz v0, :cond_0

    .line 1440
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mSubtitleView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleView;->setLayout()V

    .line 1441
    :cond_0
    return-void
.end method

.method public updateVideoVisualSeek(I)V
    .locals 1
    .param p1, "pos"    # I

    .prologue
    .line 723
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    if-eqz v0, :cond_0

    .line 724
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoVisualSeekView:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->seekto(I)V

    .line 726
    :cond_0
    return-void
.end method
