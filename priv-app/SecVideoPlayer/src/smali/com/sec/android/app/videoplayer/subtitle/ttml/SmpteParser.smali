.class public Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;
.super Ljava/lang/Object;
.source "SmpteParser.java"


# static fields
.field public static final BACKGROUND_COLOR:I = 0x6

.field public static final BEGIN:I = 0x3

.field public static final COLOR:I = 0x7

.field public static final DIRECTION:I = 0x8

.field public static final DISPLAY:I = 0x9

.field public static final DISPLAY_ALIGN:I = 0xa

.field public static final DURATION:I = 0x1b

.field public static final ELEMENT_STRINGS:[Ljava/lang/String;

.field public static final END:I = 0x4

.field public static final EXTENT:I = 0xb

.field public static final FONT_FAMILY:I = 0xc

.field public static final FONT_HEIGHT:I = 0x10

.field public static final FONT_SIZE:I = 0xd

.field public static final FONT_STYLE:I = 0xe

.field public static final FONT_WEIGHT:I = 0xf

.field public static final OPACITY:I = 0x11

.field public static final ORIGIN:I = 0x12

.field public static final PADDING:I = 0x13

.field public static final REGION:I = 0x5

.field public static final SHOW_BACKGROUND:I = 0x14

.field public static final STYLE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "TTML_Parser"

.field public static final TAG_BR:Ljava/lang/String; = "br"

.field public static final TAG_HEAD:Ljava/lang/String; = "head"

.field public static final TAG_P:Ljava/lang/String; = "p"

.field public static final TAG_REGION:Ljava/lang/String; = "region"

.field public static final TAG_SPAN:Ljava/lang/String; = "span"

.field public static final TAG_STYLE:Ljava/lang/String; = "style"

.field public static final TAG_STYLING:Ljava/lang/String; = "styling"

.field public static final TEXT:I = 0x0

.field public static final TEXT_ALIGN:I = 0x15

.field public static final TEXT_DECORATION:I = 0x16

.field public static final TEXT_OUTLINE:I = 0x17

.field public static final VISIBILITY:I = 0x18

.field public static final WARP_OPTION:I = 0x19

.field public static final WRITING_MODE:I = 0x1a

.field public static final XML_ID:I = 0x1


# instance fields
.field private mParsedArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "xml:id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "style"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "end"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "region"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "tts:backgroundColor"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "tts:color"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "tts:direction"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "tts:display"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "tts:displayAlign"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "tts:extent"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "tts:fontFamily"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "tts:fontSize"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "tts:fontStyle"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "tts:fontWeight"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "tts:fontHeight"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "tts:opacity"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "tts:origin"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "tts:padding"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "tts:showBackground"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "tts:textAlign"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "tts:textDecoration"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "tts:textOutline"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "tts:visibility"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "tts:warpOption"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "tts:writingMode"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "dur"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    .line 98
    return-void
.end method

.method public static getElementString()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    return-object v0
.end method

.method private initParsedArray()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 102
    return-void
.end method

.method private parseByTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/xml/parsers/ParserConfigurationException;,
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    sget-object v11, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v11, v11

    new-array v11, v11, [Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v3

    .line 130
    .local v3, "factory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v3}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 131
    .local v1, "builder":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v5, Ljava/io/ByteArrayInputStream;

    const-string v10, "utf-8"

    invoke-virtual {p2, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 133
    .local v5, "istream":Ljava/io/InputStream;
    invoke-virtual {v1, v5}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    .line 134
    .local v2, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v2, p1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 136
    .local v9, "nodeList":Lorg/w3c/dom/NodeList;
    const-string v10, "p"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 137
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v4, v10, :cond_3

    .line 138
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 141
    .local v7, "node":Lorg/w3c/dom/Node;
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v8

    .line 142
    .local v8, "nodeAttrs":Lorg/w3c/dom/NamedNodeMap;
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    invoke-interface {v8}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v10

    if-ge v6, v10, :cond_0

    .line 143
    invoke-interface {v8, v6}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 144
    .local v0, "attr":Lorg/w3c/dom/Node;
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveElementValues(Lorg/w3c/dom/Node;)V

    .line 142
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 146
    .end local v0    # "attr":Lorg/w3c/dom/Node;
    :cond_0
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveChildTag(Lorg/w3c/dom/NodeList;)V

    .line 137
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 148
    .end local v4    # "i":I
    .end local v6    # "j":I
    .end local v7    # "node":Lorg/w3c/dom/Node;
    .end local v8    # "nodeAttrs":Lorg/w3c/dom/NamedNodeMap;
    :cond_1
    const-string v10, "head"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 149
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_2
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    if-ge v4, v10, :cond_3

    .line 150
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v7

    .line 151
    .restart local v7    # "node":Lorg/w3c/dom/Node;
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "head"

    invoke-virtual {v10, v11}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 152
    invoke-interface {v7}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveChildNode(Lorg/w3c/dom/NodeList;)V

    .line 149
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 156
    .end local v4    # "i":I
    .end local v7    # "node":Lorg/w3c/dom/Node;
    :cond_3
    return-void
.end method

.method private saveChildNode(Lorg/w3c/dom/NodeList;)V
    .locals 6
    .param p1, "pChildList"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 159
    const/4 v2, 0x0

    .line 161
    .local v2, "savedCnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 162
    invoke-interface {p1, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 163
    .local v0, "Child":Lorg/w3c/dom/Node;
    const-string v3, "TTML_Parser"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveChildNode : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "style"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 165
    if-lez v2, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    sget-object v4, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v4, v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    :cond_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveNodeElements(Lorg/w3c/dom/Node;)V

    .line 167
    add-int/lit8 v2, v2, 0x1

    .line 161
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 168
    :cond_2
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "styling"

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 169
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveChildNode(Lorg/w3c/dom/NodeList;)V

    goto :goto_1

    .line 172
    .end local v0    # "Child":Lorg/w3c/dom/Node;
    :cond_3
    return-void
.end method

.method private saveChildTag(Lorg/w3c/dom/NodeList;)V
    .locals 7
    .param p1, "pChildList"    # Lorg/w3c/dom/NodeList;

    .prologue
    .line 203
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 204
    .local v3, "nodeVal":Ljava/lang/String;
    const/4 v2, 0x0

    .line 205
    .local v2, "isSpaned":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v1, v5, :cond_4

    .line 206
    invoke-interface {p1, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 207
    .local v0, "Child":Lorg/w3c/dom/Node;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    .line 208
    .local v4, "val":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 209
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 210
    move-object v3, v4

    .line 216
    :cond_0
    :goto_1
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "br"

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 217
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 205
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 212
    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    .line 218
    :cond_3
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "span"

    invoke-virtual {v5, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 219
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveTextValues(Ljava/lang/String;)V

    .line 220
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v6, v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveNodeElements(Lorg/w3c/dom/Node;)V

    .line 222
    new-instance v3, Ljava/lang/String;

    .end local v3    # "nodeVal":Ljava/lang/String;
    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 223
    .restart local v3    # "nodeVal":Ljava/lang/String;
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveChildTag(Lorg/w3c/dom/NodeList;)V

    .line 224
    const/4 v2, 0x1

    goto :goto_2

    .line 228
    .end local v0    # "Child":Lorg/w3c/dom/Node;
    .end local v4    # "val":Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    .line 229
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    sget-object v6, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v6, v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_5
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveTextValues(Ljava/lang/String;)V

    .line 232
    return-void
.end method

.method private saveElementValues(Lorg/w3c/dom/Node;)V
    .locals 5
    .param p1, "attr"    # Lorg/w3c/dom/Node;

    .prologue
    .line 240
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 241
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 243
    const-string v2, "TTML_Parser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "saveElementValues : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_1
    return-void
.end method

.method private saveNodeElements(Lorg/w3c/dom/Node;)V
    .locals 4
    .param p1, "Child"    # Lorg/w3c/dom/Node;

    .prologue
    .line 175
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 176
    .local v2, "nodeAttrs":Lorg/w3c/dom/NamedNodeMap;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 177
    invoke-interface {v2, v1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 178
    .local v0, "attr":Lorg/w3c/dom/Node;
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->saveElementValues(Lorg/w3c/dom/Node;)V

    .line 176
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "attr":Lorg/w3c/dom/Node;
    :cond_0
    return-void
.end method

.method private saveTextValues(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 235
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 237
    :cond_0
    return-void
.end method


# virtual methods
.method public getFontFamily(I)Ljava/lang/String;
    .locals 4
    .param p1, "i"    # I

    .prologue
    const/16 v3, 0xc

    .line 301
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 302
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    if-eqz v0, :cond_0

    .line 303
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getFontFamily"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v3

    .line 307
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getParsedArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getStyleFontSize(I)I
    .locals 5
    .param p1, "i"    # I

    .prologue
    const/16 v4, 0xd

    .line 279
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    if-eqz v0, :cond_0

    .line 281
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleFontSize"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleFontSize"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToInt(Ljava/lang/String;)I

    move-result v0

    .line 286
    :goto_0
    return v0

    :cond_0
    const v0, -0x989676

    goto :goto_0
.end method

.method public getStyleOpacity(I)F
    .locals 6
    .param p1, "i"    # I

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    const/16 v5, 0x11

    .line 290
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    if-eqz v0, :cond_0

    .line 292
    const-string v2, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStyleOpacity"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    const-string v2, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStyleOpacity"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v4, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToFloat(Ljava/lang/String;)F

    move-result v0

    mul-float/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToFloat(Ljava/lang/String;)F

    move-result v0

    mul-float/2addr v0, v1

    .line 297
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getStyleTextBackgroundColor(I)I
    .locals 5
    .param p1, "i"    # I

    .prologue
    const/4 v4, 0x6

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    if-eqz v0, :cond_0

    .line 270
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleTextBackgroundColor"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleTextBackgroundColor"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v4

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v0

    .line 275
    :goto_0
    return v0

    :cond_0
    const v0, -0xf423f

    goto :goto_0
.end method

.method public getStyleTextColor(I)I
    .locals 6
    .param p1, "i"    # I

    .prologue
    const/4 v5, 0x7

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    if-eqz v0, :cond_0

    .line 259
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleTextColor"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    const-string v1, "TTML_Parser"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getStyleTextColor"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v3, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v5

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteConverter;->stringToColor(Ljava/lang/String;)I

    move-result v0

    .line 264
    :goto_0
    return v0

    :cond_0
    const v0, -0xf423f

    goto :goto_0
.end method

.method public parseTag(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .param p1, "str"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    const-string v1, "TTML_Parser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseTag E : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->initParsedArray()V

    .line 108
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "<p"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 109
    const-string v1, "p"

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->parseByTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 120
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 121
    const-string v2, "TTML_Parser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseTTML : E"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    return-object v1

    .line 110
    :cond_2
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "<head"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 111
    const-string v1, "head"

    invoke-direct {p0, v1, p1}, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->parseByTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v1, "TTML_Parser"

    const-string v2, "parseTag : ParserConfigurationException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :catch_1
    move-exception v0

    .line 116
    .local v0, "e":Lorg/xml/sax/SAXException;
    const-string v1, "TTML_Parser"

    const-string v2, "parseTag : SAXException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 117
    .end local v0    # "e":Lorg/xml/sax/SAXException;
    :catch_2
    move-exception v0

    .line 118
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "TTML_Parser"

    const-string v2, "parseTag : IOException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public printAllNode(Z)V
    .locals 6
    .param p1, "isHeadParse"    # Z

    .prologue
    .line 187
    const-string v2, ""

    .line 188
    .local v2, "log":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 189
    const-string v3, "Head tag "

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 193
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 194
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    sget-object v3, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->ELEMENT_STRINGS:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 195
    const-string v4, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mParsedArray "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "]: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/subtitle/ttml/SmpteParser;->mParsedArray:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 191
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_0
    const-string v3, "P tag "

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 193
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 198
    .end local v1    # "j":I
    :cond_2
    return-void
.end method
