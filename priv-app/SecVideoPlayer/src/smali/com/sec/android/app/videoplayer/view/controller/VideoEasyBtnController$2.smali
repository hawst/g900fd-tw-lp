.class Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;
.super Ljava/lang/Object;
.source "VideoEasyBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 178
    const/4 v1, 0x0

    .line 179
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 180
    sparse-switch p2, :sswitch_data_0

    .line 199
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 200
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 218
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 183
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 185
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 188
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->resetHoldLongSeek()V

    .line 189
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 203
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 205
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 206
    goto :goto_0

    .line 209
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoEasyBtnController;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 210
    goto :goto_0

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 183
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 203
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
