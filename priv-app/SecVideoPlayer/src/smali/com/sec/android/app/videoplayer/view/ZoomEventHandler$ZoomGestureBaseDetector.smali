.class public abstract Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;
.super Ljava/lang/Object;
.source "ZoomEventHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ZoomGestureBaseDetector"
.end annotation


# static fields
.field private static final MAX_HD_ZOOM_FACTOR:F = 20.0f

.field private static final MAX_SD_ZOOM_FACTOR:F = 10.0f

.field private static final TAG:Ljava/lang/String; = "ZoomGestureBaseDetector"

.field private static final ZOOM_TOLERANCE:I = 0xa


# instance fields
.field protected final PAN_TOUCH_POINTER_COUNT:I

.field protected final PINCH_ZOOM_POINTER_COUNT:I

.field protected mContext:Landroid/content/Context;

.field protected mIsGestureBeingTracked:Z

.field protected mMaxZoomFactor:F

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field protected mVideoFrameHeight:I

.field protected mVideoFrameWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "app"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 281
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mContext:Landroid/content/Context;

    .line 283
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mIsGestureBeingTracked:Z

    .line 286
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    .line 287
    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    .line 289
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mMaxZoomFactor:F

    .line 290
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->PAN_TOUCH_POINTER_COUNT:I

    .line 291
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->PINCH_ZOOM_POINTER_COUNT:I

    .line 296
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 299
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mContext:Landroid/content/Context;

    .line 300
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 301
    return-void
.end method


# virtual methods
.method protected frameHeight()I
    .locals 1

    .prologue
    .line 329
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    return v0
.end method

.method protected frameWidth()I
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    return v0
.end method

.method protected getZoomPercent(I)F
    .locals 6
    .param p1, "area"    # I

    .prologue
    .line 349
    const/high16 v2, 0x42c80000    # 100.0f

    .line 351
    .local v2, "zoomedPercent":F
    sget-object v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 353
    .local v1, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v4

    mul-int v0, v3, v4

    .line 354
    .local v0, "orgArea":I
    add-int/lit8 v3, p1, 0xa

    if-le v0, v3, :cond_0

    .line 355
    int-to-float v3, v0

    int-to-float v4, p1

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v4, v5

    div-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float v2, v3, v4

    .line 357
    :cond_0
    const-string v3, "ZoomGestureBaseDetector"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ZOOM Percent = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    return v2
.end method

.method public isGestureTracked()Z
    .locals 1

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mIsGestureBeingTracked:Z

    return v0
.end method

.method public onLongPress()V
    .locals 0

    .prologue
    .line 345
    return-void
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public printVideoFrameProperties(Ljava/lang/String;)V
    .locals 2
    .param p1, "TAG"    # Ljava/lang/String;

    .prologue
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "wf:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " hf:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 338
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->setGestureBeingTracked(Z)V

    .line 340
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->updateVideoFrameProperties()V

    .line 341
    return-void
.end method

.method protected setGestureBeingTracked(Z)V
    .locals 0
    .param p1, "status"    # Z

    .prologue
    .line 308
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mIsGestureBeingTracked:Z

    .line 309
    return-void
.end method

.method protected updateVideoFrameProperties()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    .line 316
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    if-eqz v0, :cond_0

    .line 317
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameWidth:I

    iget v1, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mVideoFrameHeight:I

    mul-int/2addr v0, v1

    const v1, 0xe1000

    if-lt v0, v1, :cond_0

    .line 319
    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mMaxZoomFactor:F

    .line 322
    :cond_0
    return-void
.end method

.method protected withinScaleBounds(F)Z
    .locals 8
    .param p1, "area"    # F

    .prologue
    const/16 v7, 0x140

    .line 373
    const/4 v2, 0x1

    .line 375
    .local v2, "isWithinBounds":Z
    sget-object v3, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;

    .line 377
    .local v3, "surface":Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getWidth()I

    move-result v4

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/VideoSurface;->getHeight()I

    move-result v5

    mul-int/2addr v4, v5

    int-to-long v0, v4

    .line 378
    .local v0, "Area":J
    const-string v4, "ZoomGestureBaseDetector"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Initial Area :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 382
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v4

    if-lt v4, v7, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v4

    if-ge v4, v7, :cond_1

    .line 383
    :cond_0
    const/4 v4, 0x0

    .line 388
    :goto_0
    return v4

    .line 385
    :cond_1
    long-to-float v4, v0

    iget v5, p0, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->mMaxZoomFactor:F

    div-float/2addr v4, v5

    cmpg-float v4, p1, v4

    if-gez v4, :cond_2

    .line 386
    const/4 v2, 0x0

    :cond_2
    move v4, v2

    .line 388
    goto :goto_0
.end method

.method protected withinScaleBounds(I)Z
    .locals 2
    .param p1, "area"    # I

    .prologue
    .line 363
    int-to-float v0, p1

    .line 364
    .local v0, "f":F
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler$ZoomGestureBaseDetector;->withinScaleBounds(F)Z

    move-result v1

    return v1
.end method
