.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;
.super Landroid/view/OrientationEventListener;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 494
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    const/16 v2, 0x8

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 499
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v0

    const/16 v1, 0x46

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v0

    const/16 v1, 0x6e

    if-ge v0, v1, :cond_1

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    .line 504
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getScreenOrientation()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v0

    const/16 v1, 0xfa

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mOrientation:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v0

    const/16 v1, 0x122

    if-ge v0, v1, :cond_0

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$1;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen(I)V

    goto :goto_0
.end method
