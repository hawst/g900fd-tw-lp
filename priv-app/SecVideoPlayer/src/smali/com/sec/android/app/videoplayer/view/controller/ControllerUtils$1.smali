.class Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;
.super Ljava/lang/Object;
.source "ControllerUtils.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 102
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 129
    :cond_0
    :goto_0
    const/4 v2, 0x0

    return v2

    .line 104
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$000(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 108
    :pswitch_1
    const-string v2, "ControllerUtils"

    const-string v3, "mRotationBtnTouchListener call"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v4, v2

    if-gtz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_0

    .line 110
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 111
    .local v0, "pressTime":J
    const-wide/16 v2, 0x1f4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 112
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_2

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$100(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$200(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 116
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateSecondScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$300(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 119
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$1;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$200(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
