.class final enum Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
.super Ljava/lang/Enum;
.source "AnimatedToggleButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AnimationState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

.field public static final enum A:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

.field public static final enum AB:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

.field public static final enum B:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

.field public static final enum BA:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    new-instance v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    const-string v1, "A"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->A:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    new-instance v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    const-string v1, "AB"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    new-instance v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    const-string v1, "BA"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->BA:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    new-instance v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    const-string v1, "B"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->B:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->A:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->BA:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->B:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->$VALUES:[Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->$VALUES:[Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$AnimationState;

    return-object v0
.end method
