.class Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$8;
.super Ljava/lang/Object;
.source "SCoverPlayer.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$8;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    .line 259
    packed-switch p1, :pswitch_data_0

    .line 267
    :goto_0
    return-void

    .line 261
    :pswitch_0
    const-string v0, "SCoverPlayer"

    const-string v1, "mAudioFocusListener. AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :pswitch_1
    const-string v0, "SCoverPlayer"

    const-string v1, "mAudioFocusListener. pause by alert sound"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer$8;->this$0:Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->pause()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;->access$300(Lcom/sec/android/app/videoplayer/activity/SCoverPlayer;)V

    goto :goto_0

    .line 259
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
