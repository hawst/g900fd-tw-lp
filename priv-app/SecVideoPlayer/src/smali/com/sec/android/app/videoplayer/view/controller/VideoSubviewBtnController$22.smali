.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field IsFirstTouchOnProgressbar:Z

.field position:J

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 2519
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v8, 0x0

    .line 2525
    if-eqz p3, :cond_3

    .line 2526
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2587
    :goto_0
    return-void

    .line 2531
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVideoProgressBarScrubbing:Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/android/app/videoplayer/widget/VideoProgressBarScrubbing;->reprocessProgress(I)I

    move-result p2

    .line 2534
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDuration:J
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v2

    int-to-long v4, p2

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x186a0

    div-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    .line 2535
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 2537
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->IsFirstTouchOnProgressbar:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 2538
    iput-boolean v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->IsFirstTouchOnProgressbar:Z

    .line 2576
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v2, :cond_2

    .line 2577
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v4, v4

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v3, v4, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2578
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-wide v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v6, v6

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v5, v6, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2581
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2583
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpClip()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v2, :cond_3

    .line 2584
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setHelpClip4SeekMode(I)V

    .line 2586
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setContentsDescription4TWProgressBar()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    goto/16 :goto_0

    .line 2540
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2541
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isHttpBrowser()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2542
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "onProgressChanged - DASH - S K I P"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569
    :cond_5
    :goto_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2571
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "updateVideoVisualSeek"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2572
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->updateVideoVisualSeek(I)V

    goto/16 :goto_1

    .line 2543
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2544
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v1

    .line 2545
    .local v1, "secProgress":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 2546
    .local v0, "currentProgress":I
    if-ge v0, v1, :cond_5

    .line 2547
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_2

    .line 2550
    .end local v0    # "currentProgress":I
    .end local v1    # "secProgress":I
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_2

    .line 2553
    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2556
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2558
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isBrowser()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2559
    :cond_9
    const-string v2, "VideoSubViewBtnController"

    const-string v3, "onProgressChanged - streaming - S K I P"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 2561
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto/16 :goto_2

    .line 2564
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v3, v4

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v4, 0x1

    .line 2640
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2642
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2660
    :cond_0
    :goto_0
    return-void

    .line 2645
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->resetHoldLongSeek()V

    .line 2646
    invoke-virtual {p1}, Landroid/widget/SeekBar;->invalidate()V

    .line 2647
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$902(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 2648
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v1

    int-to-long v2, v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mDuration:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3802(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    .line 2649
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->IsFirstTouchOnProgressbar:Z

    .line 2652
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 2653
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setContentsDescription4TWProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    .line 2655
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2656
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideTVOutView()V

    .line 2657
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "setVideoVisualSeek"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2658
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setVideoVisualSeek(ILandroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const-wide/16 v8, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2590
    const-string v3, "VideoSubViewBtnController"

    const-string v4, "onStopTrackingTouch"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2591
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2592
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v3, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setProgress(I)V

    .line 2637
    :goto_0
    return-void

    .line 2596
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPresentationServiceRunning(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->isDRMFile()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isKDrmPlayingNow()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2598
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->DismissVideoVisualSeek()V

    .line 2599
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showTVOutView()V

    .line 2602
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$902(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 2604
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isMovieStoreContent()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2605
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2606
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getSecondaryProgress()I

    move-result v2

    .line 2607
    .local v2, "secProgress":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    .line 2608
    .local v0, "currentProgress":I
    if-ge v0, v2, :cond_2

    .line 2609
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 2629
    .end local v0    # "currentProgress":I
    .end local v2    # "secProgress":I
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setProgress()I

    .line 2632
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v6

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->stringForTime(IZ)Ljava/lang/String;
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;IZ)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2634
    .local v1, "currentnewTime":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2636
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v3, v3, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v4, 0xbb8

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto/16 :goto_0

    .line 2612
    .end local v1    # "currentnewTime":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_1

    .line 2615
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2617
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    long-to-int v4, v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    goto :goto_1

    .line 2619
    :cond_7
    const-string v3, "VideoSubViewBtnController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStopTrackingTouch local play. mScrubbingSpeed : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2621
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mScrubbingSpeed:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$4000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v3

    if-ne v3, v6, :cond_8

    .line 2622
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    add-long/2addr v4, v8

    long-to-int v4, v4

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_1

    .line 2624
    :cond_8
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$22;->position:J

    add-long/2addr v4, v8

    long-to-int v4, v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_1
.end method
