.class public Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;
.super Ljava/lang/Object;
.source "PackageInfoFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public create()Landroid/util/SparseArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 15
    .local v0, "packageInfoList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;>;"
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->NXP_EDITOR:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 16
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 17
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_STUDIO:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 18
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 19
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->VIDEO_EDITOR_ON_S_APPS:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 20
    sget v1, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->SHARE_VIDEO:I

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory$PackageInfo;-><init>(Lcom/sec/android/app/videoplayer/cmd/PackageInfoFactory;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 22
    return-object v0
.end method
