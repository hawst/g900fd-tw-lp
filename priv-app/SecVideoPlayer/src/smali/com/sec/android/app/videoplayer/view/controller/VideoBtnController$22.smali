.class Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;
.super Ljava/lang/Object;
.source "VideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isOutside:Z

.field private mDownPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)V
    .locals 1

    .prologue
    .line 2283
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->isOutside:Z

    .line 2286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->mDownPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2289
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2290
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    sub-long v2, v4, v6

    .line 2291
    .local v2, "pressTime":J
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    .line 2293
    .local v0, "isDlna":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2389
    .end local v0    # "isDlna":Z
    .end local v2    # "pressTime":J
    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 2295
    .restart local v0    # "isDlna":Z
    .restart local v2    # "pressTime":J
    :pswitch_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->isOutside:Z

    .line 2297
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->blockSpeedSeek()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2298
    const-string v1, "VideoBtnController"

    const-string v4, "mFfTouchListener. skip longseek"

    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/os/Handler;

    move-result-object v1

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0x1e

    const/16 v6, 0x8

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v6, 0x1f4

    invoke-virtual {v1, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2300
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const v4, 0x36ee80

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    .line 2305
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->mDownPath:Ljava/lang/String;

    goto :goto_0

    .line 2302
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/4 v4, 0x6

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_1

    .line 2310
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->blockSpeedSeek()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2311
    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    .line 2312
    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 2313
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/os/Handler;

    move-result-object v1

    const/16 v4, 0x1e

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2315
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isPreviewMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2316
    if-eqz v0, :cond_3

    .line 2317
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2371
    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/16 v4, 0xbb8

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->keepShowingController(I)V
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;I)V

    goto/16 :goto_0

    .line 2319
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 2320
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2321
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto :goto_2

    .line 2327
    :cond_4
    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    .line 2329
    const-wide/16 v4, 0x1f4

    cmp-long v1, v2, v4

    if-gez v1, :cond_6

    .line 2330
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2338
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->resetHoldLongSeek()V

    goto :goto_2

    .line 2356
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setLongSeekMode(I)V

    .line 2357
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2358
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 2361
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->mDownPath:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->mDownPath:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2362
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    const/4 v4, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mHoldLongSeekSpeed:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$1902(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;Z)Z

    .line 2363
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0xf

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 2365
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0xb

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    goto/16 :goto_2

    .line 2375
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->isOutside:Z

    if-nez v1, :cond_0

    .line 2376
    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    cmpg-float v1, v1, v4

    if-ltz v1, :cond_8

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpl-float v1, v1, v4

    if-gtz v1, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    cmpg-float v1, v1, v4

    if-gez v1, :cond_0

    .line 2378
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 2379
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoBtnController$22;->isOutside:Z

    goto/16 :goto_0

    .line 2293
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
