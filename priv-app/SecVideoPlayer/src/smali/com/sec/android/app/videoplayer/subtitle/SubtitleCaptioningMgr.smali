.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
.super Ljava/lang/Object;
.source "SubtitleCaptioningMgr.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static mInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;


# instance fields
.field private mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

.field private mManager:Landroid/view/accessibility/CaptioningManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    .line 28
    const-string v0, "captioning"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    .line 30
    return-void
.end method

.method public static get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    .line 37
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    return-object v0
.end method


# virtual methods
.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->backgroundColor:I

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getEdgeColor()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeColor:I

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getEdgeType()I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->edgeType:I

    .line 92
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getFontScale()F
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v0

    .line 60
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getForegroundColor()I
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    iget v0, v0, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->foregroundColor:I

    .line 84
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTypeface()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mCaptionStyle:Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager$CaptionStyle;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCaptionEnable()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->mManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v0

    .line 44
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
