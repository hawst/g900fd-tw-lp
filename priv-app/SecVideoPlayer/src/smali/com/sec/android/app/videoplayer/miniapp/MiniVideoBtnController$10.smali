.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;
.super Ljava/lang/Object;
.source "MiniVideoBtnController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isOutside:Z

.field private mDownPath:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 1

    .prologue
    .line 888
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 890
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->isOutside:Z

    .line 892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->mDownPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v9, 0x8

    const-wide/16 v10, 0x1f4

    const/16 v8, 0xb

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 895
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 896
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 897
    .local v0, "pressTime":J
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 963
    .end local v0    # "pressTime":J
    :cond_0
    :goto_0
    return v6

    .line 899
    .restart local v0    # "pressTime":J
    :pswitch_0
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->isOutside:Z

    .line 901
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->blockSpeedSeek()Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 902
    const-string v2, "MiniVideoBtnController"

    const-string v3, "mNextTouchListener. skip longseek"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x1e

    invoke-virtual {v3, v4, v6, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 904
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v3, 0xbb8

    invoke-virtual {v2, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    .line 909
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->mDownPath:Ljava/lang/String;

    goto :goto_0

    .line 906
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto :goto_1

    .line 914
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->blockSpeedSeek()Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 915
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v7, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v7, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 916
    cmp-long v2, v0, v10

    if-gez v2, :cond_2

    .line 917
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 918
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setLongSeekMode(I)V

    .line 919
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 920
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v2

    invoke-interface {v2, v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    .line 945
    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v3, 0xbb8

    invoke-virtual {v2, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto/16 :goto_0

    .line 924
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v2, v7, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v2, v7, v2

    if-gtz v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    .line 926
    cmp-long v2, v0, v10

    if-gez v2, :cond_5

    .line 927
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 928
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    goto :goto_2

    .line 930
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setLongSeekMode(I)V

    .line 931
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 932
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v2

    invoke-interface {v2, v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    goto :goto_2

    .line 935
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->mDownPath:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->mDownPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 936
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setHoldLongSeek()V

    .line 937
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto :goto_2

    .line 939
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v2, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto/16 :goto_2

    .line 949
    :pswitch_2
    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->isOutside:Z

    if-nez v2, :cond_0

    .line 950
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpl-float v2, v7, v2

    if-gtz v2, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    cmpg-float v2, v2, v3

    if-ltz v2, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpl-float v2, v7, v2

    if-gtz v2, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 952
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 953
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;->isOutside:Z

    goto/16 :goto_0

    .line 897
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
