.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->createSubtitleSizePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1049
    .local p1, "adapterview":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    if-eqz v0, :cond_0

    .line 1050
    packed-switch p3, :pswitch_data_0

    .line 1065
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1066
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleSizeDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$1400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 1069
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refreshSubtitleMenu()V

    .line 1070
    return-void

    .line 1052
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontSize(I)V

    goto :goto_0

    .line 1056
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontSize(I)V

    goto :goto_0

    .line 1060
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$10;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    const/16 v1, 0x1c

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontSize(I)V

    goto :goto_0

    .line 1050
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
