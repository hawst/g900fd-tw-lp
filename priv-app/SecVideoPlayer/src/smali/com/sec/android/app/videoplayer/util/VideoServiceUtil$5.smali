.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)V
    .locals 0

    .prologue
    .line 2407
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 2409
    const-string v9, "VideoServiceUtil"

    const-string v10, "mSDPDownloader Start"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411
    const/4 v5, 0x0

    .line 2412
    .local v5, "inputStream":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 2415
    .local v7, "out":Ljava/io/OutputStream;
    const v0, 0xea60

    .line 2416
    .local v0, "DEFAULT_TIMEOUT":I
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2417
    .local v1, "DownloadURL":Ljava/lang/String;
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v8

    .line 2418
    .local v8, "t_connection":Ljava/net/URLConnection;
    invoke-virtual {v8, v0}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 2419
    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/net/URLConnection;->setUseCaches(Z)V

    .line 2421
    invoke-virtual {v8}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 2422
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v9

    const-string v10, ".down.sdp"

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v7

    .line 2423
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v9, v5, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->writeFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 2424
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    .line 2425
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 2427
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "file://"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/content/Context;

    move-result-object v11

    const-string v12, ".down.sdp"

    invoke-virtual {v11, v12}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setPlayingFileInfo(Landroid/net/Uri;)Z

    .line 2428
    const-string v9, "VideoServiceUtil"

    const-string v10, "init() File Write Successful !! "

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2430
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$1000(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v9

    const/16 v10, 0x1c

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/type/SchemeType;->setKeyType(I)V

    .line 2432
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 2433
    .local v6, "msg":Landroid/os/Message;
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 2434
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$400(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;)Landroid/os/Handler;

    move-result-object v9

    const-wide/16 v10, 0x64

    invoke-virtual {v9, v6, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2454
    .end local v1    # "DownloadURL":Ljava/lang/String;
    .end local v6    # "msg":Landroid/os/Message;
    .end local v8    # "t_connection":Ljava/net/URLConnection;
    :cond_0
    :goto_0
    return-void

    .line 2435
    :catch_0
    move-exception v2

    .line 2436
    .local v2, "e":Ljava/lang/Exception;
    const-string v9, "VideoServiceUtil"

    const-string v10, "init() File Write Failed! "

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2437
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 2438
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$5;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stopPlayingChecker()V

    .line 2439
    if-eqz v5, :cond_1

    .line 2441
    :try_start_1
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 2446
    :cond_1
    :goto_1
    if-eqz v7, :cond_0

    .line 2448
    :try_start_2
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2449
    :catch_1
    move-exception v4

    .line 2450
    .local v4, "e2":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2442
    .end local v4    # "e2":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 2443
    .local v3, "e1":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
