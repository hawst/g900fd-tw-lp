.class Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V
    .locals 0

    .prologue
    .line 831
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 833
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 835
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSettingsPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->removePauseSet(Ljava/lang/Object;)V

    .line 839
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mIsHeadsetUnplugged:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$602(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Z)Z

    .line 842
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 843
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$702(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 846
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 847
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 848
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mUIInfoList:Ljava/util/ArrayList;
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$802(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 851
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 852
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 853
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mAvailableList:Ljava/util/ArrayList;
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$202(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 858
    :cond_3
    return-void

    .line 837
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    const-string v1, "VideoSettingsPopup"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_0
.end method
