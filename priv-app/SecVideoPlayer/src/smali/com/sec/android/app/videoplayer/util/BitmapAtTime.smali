.class public Lcom/sec/android/app/videoplayer/util/BitmapAtTime;
.super Ljava/lang/Thread;
.source "BitmapAtTime.java"


# static fields
.field public static mBackHandler:Landroid/os/Handler;


# instance fields
.field private final TAG:Ljava/lang/String;

.field public final mMainHandler:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private pos:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 15
    const-string v0, "BitmapAtTime"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->TAG:Ljava/lang/String;

    .line 21
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mMainHandler:Ljava/lang/ref/WeakReference;

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/BitmapAtTime;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/BitmapAtTime;

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->pos:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/util/BitmapAtTime;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/BitmapAtTime;
    .param p1, "x1"    # J

    .prologue
    .line 14
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->pos:J

    return-wide p1
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 26
    new-instance v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/BitmapAtTime$1;-><init>(Lcom/sec/android/app/videoplayer/util/BitmapAtTime;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    .line 84
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 85
    return-void
.end method
