.class Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;
.super Ljava/lang/Object;
.source "ImageCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 194
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    # getter for: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$200()Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mCloudUtil:Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$200()Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    # invokes: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->downScaler(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$300(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    # invokes: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->getLocalBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    invoke-static {v1, p1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$400(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 174
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    :cond_0
    return-void

    .line 176
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 177
    invoke-static {}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->get()Ljava/lang/String;

    move-result-object v1

    .line 179
    .local v1, "path":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_1

    .line 181
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 182
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->removeQueueInProcess(Ljava/lang/String;)Z

    .line 184
    if-eqz v0, :cond_1

    .line 185
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$MemCache;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 186
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$100(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$1;->this$0:Lcom/sec/android/app/videoplayer/playerlist/ImageCache;

    # getter for: Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/playerlist/ImageCache;->access$100(Lcom/sec/android/app/videoplayer/playerlist/ImageCache;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
