.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    }
.end annotation


# static fields
.field public static final CAPTION_WINDOW_COLOR:I = 0x1

.field private static final DEFAULT_SELECTED_CUSTOM_COLOR:I = -0x2

.field public static final FONT_BACKGROUND_COLOR:I = 0x2

.field public static final FONT_COLOR:I = 0x0

.field private static final LONGMINUSMSG:I = 0x2

.field private static final LONGPLUSMSG:I = 0x1

.field public static final LONG_PRESS_DELEY:I = 0x1f4

.field private static final PRESET_COLOR:I = 0x1

.field private static final RESET_COLOR:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

.field private mCheckCustomColor:Z

.field private mColor:I

.field private mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

.field private mContext:Landroid/content/Context;

.field public mCurPosition:I

.field private mHandler:Landroid/os/Handler;

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mMinusButton:Landroid/view/View;

.field private mOpacity:I

.field private mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

.field private mPlusButton:Landroid/view/View;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private mSelectedCustomColor:I

.field private mSelectedItem:I

.field private mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

.field private mSubtitleOpacityText:Landroid/widget/TextView;

.field private mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

.field private mTouchListener:Landroid/view/View$OnTouchListener;

.field private mType:I

.field private mWhite:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/Dialog;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    .line 37
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 41
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 43
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleOpacityText:Landroid/widget/TextView;

    .line 49
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    .line 71
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    .line 75
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    .line 77
    iput v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    .line 79
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I

    .line 81
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    .line 83
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mWhite:I

    .line 529
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$11;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mHandler:Landroid/os/Handler;

    .line 551
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$12;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 675
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$13;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 88
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    .line 89
    check-cast p2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .end local p2    # "dialog":Landroid/app/Dialog;
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    .line 90
    invoke-static {}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    .line 91
    iput p3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    .line 92
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setWhite()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mWhite:I

    .line 93
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->createPopup()V

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setOpacity(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultOpacity()I

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/widget/SeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateOpacity(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorFromPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    return p1
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Landroid/content/Context;)Landroid/content/Context;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateColor(I)V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setColor(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;IJ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I
    .param p2, "x2"    # J

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->sendMessage(IJ)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->removeMessage(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->clickPlusBtn()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->clickMinusBtn()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    return-object v0
.end method

.method private clickMinusBtn()V
    .locals 2

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    .line 522
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateOpacity(I)V

    .line 523
    return-void
.end method

.method private clickPlusBtn()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    .line 516
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateOpacity(I)V

    .line 517
    return-void
.end method

.method private createPopup()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 108
    sget-object v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->TAG:Ljava/lang/String;

    const-string v5, "createPopup E"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->isShowingSubtitleSubPopup()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    sget-object v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->TAG:Ljava/lang/String;

    const-string v5, "createPopup() - isShowingSubtitleSubPopup() is true"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 117
    invoke-direct {p0, v7}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColorPosition(I)I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedItem:I

    .line 118
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getOpacity()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    .line 119
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColor()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    .line 121
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 122
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030030

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 124
    .local v3, "view":Landroid/view/View;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    invoke-direct {v2, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 125
    .local v2, "popup":Landroid/app/AlertDialog$Builder;
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    if-nez v4, :cond_4

    .line 126
    const v4, 0x7f0a0078

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 133
    :goto_1
    const v4, 0x7f0d019a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    .line 134
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 136
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    invoke-direct {p0, v4}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->isCustomColor(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 137
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    iput v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    .line 138
    iput-boolean v7, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    .line 141
    :cond_1
    const v4, 0x7f0a0108

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 150
    const v4, 0x7f0a0026

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$2;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    const v4, 0x7f0d0188

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    .line 157
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 158
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v5, 0x64

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 159
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 160
    const v4, 0x7f0d019c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleOpacityText:Landroid/widget/TextView;

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setOpacityProgress()V

    .line 164
    const v4, 0x7f0d0189

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    .line 166
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    if-eqz v4, :cond_2

    .line 167
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0168

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 169
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$3;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$4;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mPlusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$5;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 220
    :cond_2
    const v4, 0x7f0d0187

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    .line 222
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    if-eqz v4, :cond_3

    .line 223
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0167

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 225
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$6;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$7;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$7;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 255
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mMinusButton:Landroid/view/View;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$8;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$8;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 277
    :cond_3
    const v4, 0x7f0d0199

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 278
    .local v0, "gridView":Landroid/widget/GridView;
    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    .line 279
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 280
    new-instance v4, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$9;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 300
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 301
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    .line 302
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    sget-boolean v5, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 303
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 304
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 305
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    new-instance v5, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$10;

    invoke-direct {v5, p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$10;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0

    .line 127
    .end local v0    # "gridView":Landroid/widget/GridView;
    :cond_4
    iget v4, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    if-ne v4, v7, :cond_5

    .line 128
    const v4, 0x7f0a0029

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 130
    :cond_5
    const v4, 0x7f0a013a

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1
.end method

.method private getColor()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 404
    const/high16 v0, -0x1000000

    :goto_0
    return v0

    .line 398
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontColor()I

    move-result v0

    goto :goto_0

    .line 400
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinColor()I

    move-result v0

    goto :goto_0

    .line 402
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGColor()I

    move-result v0

    goto :goto_0

    .line 396
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getColorFromPosition(I)I
    .locals 4
    .param p1, "pos"    # I

    .prologue
    const/4 v3, 0x1

    .line 769
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    .line 770
    .local v0, "c":I
    packed-switch p1, :pswitch_data_0

    .line 812
    :goto_0
    return v0

    .line 772
    :pswitch_0
    const/4 v0, 0x0

    .line 773
    goto :goto_0

    .line 775
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mWhite:I

    .line 776
    goto :goto_0

    .line 778
    :pswitch_2
    const/high16 v0, -0x1000000

    .line 779
    goto :goto_0

    .line 781
    :pswitch_3
    const/high16 v0, -0x10000

    .line 782
    goto :goto_0

    .line 784
    :pswitch_4
    const v0, -0xff4500

    .line 785
    goto :goto_0

    .line 787
    :pswitch_5
    const v0, -0xffff01

    .line 788
    goto :goto_0

    .line 790
    :pswitch_6
    const/16 v0, -0x100

    .line 791
    goto :goto_0

    .line 793
    :pswitch_7
    const v0, -0xff01

    .line 794
    goto :goto_0

    .line 796
    :pswitch_8
    const v0, -0xff0001

    .line 797
    goto :goto_0

    .line 799
    :pswitch_9
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultColor()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 801
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultColor()I

    move-result v0

    goto :goto_0

    .line 803
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    .line 804
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    .line 805
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->setPointerVisible(Z)V

    .line 806
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCPView:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPaletteView;->invalidate()V

    goto :goto_0

    .line 770
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private getColorPosition(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 425
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 426
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getColor()I

    move-result v0

    .line 431
    .local v0, "color":I
    :goto_0
    sparse-switch v0, :sswitch_data_0

    .line 461
    const/16 v1, 0x8

    .line 465
    .local v1, "pos":I
    :goto_1
    return v1

    .line 428
    .end local v0    # "color":I
    .end local v1    # "pos":I
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getDefaultColor()I

    move-result v0

    .restart local v0    # "color":I
    goto :goto_0

    .line 433
    :sswitch_0
    const/4 v1, -0x1

    .line 434
    .restart local v1    # "pos":I
    goto :goto_1

    .line 437
    .end local v1    # "pos":I
    :sswitch_1
    const/4 v1, 0x0

    .line 438
    .restart local v1    # "pos":I
    goto :goto_1

    .line 440
    .end local v1    # "pos":I
    :sswitch_2
    const/4 v1, 0x1

    .line 441
    .restart local v1    # "pos":I
    goto :goto_1

    .line 443
    .end local v1    # "pos":I
    :sswitch_3
    const/4 v1, 0x2

    .line 444
    .restart local v1    # "pos":I
    goto :goto_1

    .line 446
    .end local v1    # "pos":I
    :sswitch_4
    const/4 v1, 0x3

    .line 447
    .restart local v1    # "pos":I
    goto :goto_1

    .line 449
    .end local v1    # "pos":I
    :sswitch_5
    const/4 v1, 0x4

    .line 450
    .restart local v1    # "pos":I
    goto :goto_1

    .line 452
    .end local v1    # "pos":I
    :sswitch_6
    const/4 v1, 0x5

    .line 453
    .restart local v1    # "pos":I
    goto :goto_1

    .line 455
    .end local v1    # "pos":I
    :sswitch_7
    const/4 v1, 0x6

    .line 456
    .restart local v1    # "pos":I
    goto :goto_1

    .line 458
    .end local v1    # "pos":I
    :sswitch_8
    const/4 v1, 0x7

    .line 459
    .restart local v1    # "pos":I
    goto :goto_1

    .line 431
    :sswitch_data_0
    .sparse-switch
        -0x1000000 -> :sswitch_2
        -0xffff01 -> :sswitch_5
        -0xff4500 -> :sswitch_4
        -0xff0001 -> :sswitch_8
        -0xa0a0b -> :sswitch_1
        -0x10000 -> :sswitch_3
        -0xff01 -> :sswitch_7
        -0x100 -> :sswitch_6
        -0x1 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private getDefaultColor()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 409
    iget v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v1, :pswitch_data_0

    .line 417
    const/high16 v0, -0x1000000

    :goto_0
    :pswitch_0
    return v0

    .line 411
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mWhite:I

    goto :goto_0

    .line 409
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getDefaultOpacity()I
    .locals 3

    .prologue
    const/16 v0, 0x64

    const/4 v1, 0x0

    .line 367
    iget v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v2, :pswitch_data_0

    .line 375
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 371
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 373
    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getOpacity()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 362
    const/16 v0, 0x64

    :goto_0
    return v0

    .line 356
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontOpacity()I

    move-result v0

    goto :goto_0

    .line 358
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getCaptionWinOpacity()I

    move-result v0

    goto :goto_0

    .line 360
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->getFontBGOpacity()I

    move-result v0

    goto :goto_0

    .line 354
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isCustomColor(I)Z
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 314
    sparse-switch p1, :sswitch_data_0

    .line 327
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 325
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 314
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1000000 -> :sswitch_0
        -0xffff01 -> :sswitch_0
        -0xff4500 -> :sswitch_0
        -0xff0001 -> :sswitch_0
        -0xa0a0b -> :sswitch_0
        -0x10000 -> :sswitch_0
        -0xff01 -> :sswitch_0
        -0x100 -> :sswitch_0
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 549
    return-void
.end method

.method private sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 543
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 544
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 545
    return-void
.end method

.method private setColor(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 469
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 482
    :goto_0
    return-void

    .line 471
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontColor(I)V

    goto :goto_0

    .line 474
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCaptionWinColor(I)V

    goto :goto_0

    .line 477
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontBGColor(I)V

    goto :goto_0

    .line 469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setOpacity(I)V
    .locals 1
    .param p1, "val"    # I

    .prologue
    .line 380
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    packed-switch v0, :pswitch_data_0

    .line 393
    :goto_0
    return-void

    .line 382
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontOpacity(I)V

    goto :goto_0

    .line 385
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setCaptionWinOpacity(I)V

    goto :goto_0

    .line 388
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitlePrefMgr:Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitlePrefMgr;->setFontBGOpacity(I)V

    goto :goto_0

    .line 380
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setWhite()I
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070077

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 98
    .local v0, "white":I
    sparse-switch v0, :sswitch_data_0

    .line 103
    const/4 v0, -0x1

    .end local v0    # "white":I
    :sswitch_0
    return v0

    .line 98
    :sswitch_data_0
    .sparse-switch
        -0xa0a0b -> :sswitch_0
        -0x1 -> :sswitch_0
    .end sparse-switch
.end method

.method private updateColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 763
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I

    .line 764
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    .line 765
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColorGridAdapter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$ColorsGridAdapter;->notifyDataSetChanged()V

    .line 766
    return-void
.end method

.method private updateOpacity(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 500
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    if-ge v0, p1, :cond_1

    .line 501
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 502
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    if-le v0, p1, :cond_0

    .line 503
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I

    goto :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 341
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    .line 344
    :cond_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mWhite:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSelectedCustomColor:I

    .line 345
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCheckCustomColor:Z

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->setShowingSubtitleSubPopup(Z)V

    .line 347
    return-void
.end method

.method public isShow()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleColorAndOpacityPopup:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 492
    if-eqz p3, :cond_0

    .line 493
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->updateOpacity(I)V

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleOpacityText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 497
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 508
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 511
    return-void
.end method

.method public setOpacityProgress()V
    .locals 6

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getOpacity()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 486
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mCurPosition:I

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mSubtitleOpacityText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->getOpacity()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mContext:Landroid/content/Context;

    const v3, 0x7f0a00e6

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 526
    iput p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mType:I

    .line 527
    return-void
.end method

.method public show()V
    .locals 0

    .prologue
    .line 350
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->createPopup()V

    .line 351
    return-void
.end method
