.class public Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
.super Ljava/lang/Object;
.source "VideoDRMUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    }
.end annotation


# static fields
.field private static final DRM_DIVX_MODE_ACTIVATE_RENTAL:I = 0x4

.field private static final DRM_DIVX_MODE_RENTAL:I = 0x2

.field public static final RIGHTS_EXPIRED:I = 0x2

.field public static final RIGHTS_INVALID:I = 0x1

.field public static final RIGHTS_NOT_ACQUIRED:I = 0x3

.field public static final RIGHTS_VALID:I = 0x0

.field public static final VIDEO_DRM_CONSTRAINTS_ACCUMULATED:I = 0xf

.field public static final VIDEO_DRM_CONSTRAINTS_COUNT:I = 0xb

.field public static final VIDEO_DRM_CONSTRAINTS_DATETIME:I = 0xd

.field public static final VIDEO_DRM_CONSTRAINTS_INDIVIDUAL:I = 0x10

.field public static final VIDEO_DRM_CONSTRAINTS_INTERVAL:I = 0xc

.field public static final VIDEO_DRM_CONSTRAINTS_NONE:I = 0xa

.field public static final VIDEO_DRM_CONSTRAINTS_TIMED_COUNT:I = 0xe

.field public static final VIDEO_DRM_CONSTRAINTS_UNLIMITED:I = 0x11

.field public static final VIDEO_DRM_DIVX:I = 0x4

.field public static final VIDEO_DRM_NEXTI_PV_PLAYREADY:I = 0x6

.field public static final VIDEO_DRM_OMADRM:I = 0x1

.field public static final VIDEO_DRM_PRDRM:I = 0x2

.field public static final VIDEO_DRM_SDRM:I = 0x5

.field public static final VIDEO_DRM_STR_DIVX_NOT_AUTHORIZED:I = 0x24

.field public static final VIDEO_DRM_STR_DIVX_NOT_REGISTERED:I = 0x25

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_BACK_KEY:I = 0x28

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_EXPIRED:I = 0x26

.field public static final VIDEO_DRM_STR_DIVX_RENTAL_INFO:I = 0x27

.field public static final VIDEO_DRM_STR_FIRST_INTERVAL_RENDER:I = 0x22

.field public static final VIDEO_DRM_STR_INVALID_CD:I = 0x20

.field public static final VIDEO_DRM_STR_INVALID_SD:I = 0x21

.field public static final VIDEO_DRM_STR_INVALID_WMDRM:I = 0x23

.field public static final VIDEO_DRM_STR_NULL:I = 0x1e

.field public static final VIDEO_DRM_STR_VALID_COUNT:I = 0x1f

.field public static final VIDEO_DRM_TYPE_CD:I = 0x1

.field public static final VIDEO_DRM_TYPE_FL:I = 0x0

.field public static final VIDEO_DRM_TYPE_SD:I = 0x2

.field public static final VIDEO_DRM_TYPE_SSD:I = 0x3

.field public static final VIDEO_DRM_WMDRM:I = 0x3

.field public static final VIDEO_NON_DRM:I = -0x1

.field public static mPath:Ljava/lang/String;

.field private static sVideoDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mDrmType:I

.field private mOMADrmConstraint:[Ljava/lang/String;

.field private mOMADrmConstraintType:I

.field private mOMADrmDelivery:[Ljava/lang/String;

.field private mOMADrmDeliveryType:I

.field private mRightStatus:I

.field private mRightStatusString:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "VideoDRMUtil"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 80
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    .line 82
    iput v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatus:I

    .line 83
    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 86
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "None"

    aput-object v1, v0, v3

    const-string v1, "Count"

    aput-object v1, v0, v4

    const-string v1, "Interval"

    aput-object v1, v0, v5

    const-string v1, "DateTime"

    aput-object v1, v0, v6

    const-string v1, "Timed Count"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Accumulated"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Individual"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Unlimited"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraint:[Ljava/lang/String;

    .line 87
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "VIDEO_DRM_TYPE_FL"

    aput-object v1, v0, v3

    const-string v1, "VIDEO_DRM_TYPE_CD"

    aput-object v1, v0, v4

    const-string v1, "VIDEO_DRM_TYPE_SD"

    aput-object v1, v0, v5

    const-string v1, "VIDEO_DRM_TYPE_SSD"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDelivery:[Ljava/lang/String;

    .line 88
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "RIGHTS_VALID"

    aput-object v1, v0, v3

    const-string v1, "RIGHTS_INVALID"

    aput-object v1, v0, v4

    const-string v1, "RIGHTS_EXPIRED"

    aput-object v1, v0, v5

    const-string v1, "RIGHTS_NOT_ACQUIRED"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatusString:[Ljava/lang/String;

    .line 102
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    .line 103
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 104
    return-void
.end method

.method public static getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 700
    const-string v2, "VideoDRMUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDrmMimeType : drmFilename => "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    if-eqz p0, :cond_0

    .line 703
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 704
    .local v0, "fileName":Ljava/lang/String;
    const-string v2, ".dcf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 705
    const-string v1, "application/vnd.oma.drm.content"

    .line 729
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 706
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_1
    const-string v2, ".avi"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 707
    const-string v1, "video/mux/AVI"

    goto :goto_0

    .line 708
    :cond_2
    const-string v2, ".mkv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 709
    const-string v1, "video/mux/MKV"

    goto :goto_0

    .line 710
    :cond_3
    const-string v2, ".divx"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 711
    const-string v1, "video/mux/DivX"

    goto :goto_0

    .line 712
    :cond_4
    const-string v2, ".pyv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 713
    const-string v1, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 714
    :cond_5
    const-string v2, ".pya"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 715
    const-string v1, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 716
    :cond_6
    const-string v2, ".wmv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 717
    const-string v1, "video/x-ms-wmv"

    goto :goto_0

    .line 718
    :cond_7
    const-string v2, ".wma"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 719
    const-string v1, "audio/x-ms-wma"

    goto :goto_0

    .line 720
    :cond_8
    const-string v2, ".mp4"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 721
    const-string v1, "video/mp4"

    goto :goto_0

    .line 722
    :cond_9
    const-string v2, ".sm4"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 723
    const-string v1, "video/vnd.sdrm-media.sm4"

    goto :goto_0

    .line 724
    :cond_a
    const-string v2, "DCM"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ".ismv"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 725
    const-string v1, "video/ismv"

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->sVideoDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    if-nez v0, :cond_0

    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->sVideoDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    .line 109
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->sVideoDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    return-object v0
.end method

.method private getOMARemainedCnt(Ljava/lang/String;)I
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 448
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 450
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p1, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v3

    .line 451
    .local v3, "rightDetails":Landroid/content/ContentValues;
    const-string v5, "license_category"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 452
    .local v1, "licenseCategory":Ljava/lang/String;
    const-string v4, ""

    .line 453
    .local v4, "validityRemainingCount":Ljava/lang/String;
    const/4 v2, 0x0

    .line 455
    .local v2, "remainCnt":I
    if-eqz v1, :cond_0

    .line 456
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 457
    .local v0, "license":I
    if-ne v0, v6, :cond_0

    .line 458
    const-string v5, "remaining_repeat_count"

    invoke-virtual {v3, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 460
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getOMARemainedCnt. validityRemainingCount = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    if-eqz v4, :cond_0

    .line 463
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 467
    .end local v0    # "license":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getOMARemainedCnt. remainCnt = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    return v2
.end method

.method private initDrmMgrClient()V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 132
    :cond_0
    return-void
.end method


# virtual methods
.method public checkIsNetworkConnected()Z
    .locals 3

    .prologue
    .line 1011
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 1012
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1013
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v2, "checkIsNetworkConnected() NO Connected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    const/4 v1, 0x0

    .line 1016
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public checkIsShare(Landroid/net/Uri;)Z
    .locals 13
    .param p1, "videoUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x1

    .line 342
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 343
    .local v5, "filePath":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare()"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    if-nez v5, :cond_0

    .line 346
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare() - file path is null."

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 394
    :goto_0
    return v0

    .line 350
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 352
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 353
    .local v7, "mimeType":Ljava/lang/String;
    const/4 v4, -0x1

    .line 354
    .local v4, "drmType":I
    const/4 v0, 0x1

    .line 356
    .local v0, "bResult":Z
    if-eqz v7, :cond_1

    .line 357
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v10, v5, v7}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 359
    .local v6, "isDrmSupported":Z
    if-eqz v6, :cond_3

    .line 360
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v4

    .line 361
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkIsShare. drmFileType = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    .end local v6    # "isDrmSupported":Z
    :cond_1
    :goto_1
    if-ne v4, v9, :cond_5

    .line 369
    new-instance v3, Landroid/drm/DrmInfoRequest;

    const/16 v9, 0x10

    const-string v10, "application/vnd.oma.drm.content"

    invoke-direct {v3, v9, v10}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 371
    .local v3, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v9, "drm_path"

    invoke-virtual {v3, v9, v5}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 372
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v9, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 373
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    const-string v9, "bSendAs"

    invoke-virtual {v2, v9}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 375
    .local v8, "shareObj":Ljava/lang/Object;
    if-eqz v8, :cond_2

    .line 376
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "bSendAs":Ljava/lang/String;
    const-string v9, "1"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 379
    const/4 v0, 0x1

    .line 384
    :goto_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkIsShare. bSendAs = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v8    # "shareObj":Ljava/lang/Object;
    :cond_2
    :goto_3
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkIsShare. bResult = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 363
    .restart local v6    # "isDrmSupported":Z
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v11, "checkIsShare. canHandle returned false. Not a drm file by extension"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 381
    .end local v6    # "isDrmSupported":Z
    .restart local v1    # "bSendAs":Ljava/lang/String;
    .restart local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .restart local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .restart local v8    # "shareObj":Ljava/lang/Object;
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 386
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v8    # "shareObj":Ljava/lang/Object;
    :cond_5
    const/4 v9, 0x2

    if-eq v4, v9, :cond_6

    const/4 v9, 0x5

    if-ne v4, v9, :cond_7

    .line 387
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 388
    :cond_7
    const/4 v9, 0x4

    if-ne v4, v9, :cond_2

    .line 389
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public checkIsShare(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 293
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v9, "checkIsShare()"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 297
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 298
    .local v6, "mimeType":Ljava/lang/String;
    const/4 v4, -0x1

    .line 299
    .local v4, "drmType":I
    const/4 v0, 0x1

    .line 301
    .local v0, "bResult":Z
    if-eqz v6, :cond_0

    .line 302
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v8, p1, v6}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    .line 304
    .local v5, "isDrmSupported":Z
    if-eqz v5, :cond_2

    .line 305
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v4

    .line 306
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. drmFileType = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    .end local v5    # "isDrmSupported":Z
    :cond_0
    :goto_0
    const/4 v8, 0x1

    if-ne v4, v8, :cond_4

    .line 314
    new-instance v3, Landroid/drm/DrmInfoRequest;

    const/16 v8, 0x10

    const-string v9, "application/vnd.oma.drm.content"

    invoke-direct {v3, v8, v9}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 316
    .local v3, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v8, "drm_path"

    invoke-virtual {v3, v8, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 317
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v8, v3}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 318
    .local v2, "drmInfo":Landroid/drm/DrmInfo;
    const-string v8, "bSendAs"

    invoke-virtual {v2, v8}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 320
    .local v7, "shareObj":Ljava/lang/Object;
    if-eqz v7, :cond_1

    .line 321
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 323
    .local v1, "bSendAs":Ljava/lang/String;
    const-string v8, "1"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 324
    const/4 v0, 0x1

    .line 329
    :goto_1
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. bSendAs = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v7    # "shareObj":Ljava/lang/Object;
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "checkIsShare. bResult = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    return v0

    .line 308
    .restart local v5    # "isDrmSupported":Z
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v9, "checkIsShare. canHandle returned false. Not a drm file by extension"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 326
    .end local v5    # "isDrmSupported":Z
    .restart local v1    # "bSendAs":Ljava/lang/String;
    .restart local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .restart local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .restart local v7    # "shareObj":Ljava/lang/Object;
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 331
    .end local v1    # "bSendAs":Ljava/lang/String;
    .end local v2    # "drmInfo":Landroid/drm/DrmInfo;
    .end local v3    # "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    .end local v7    # "shareObj":Ljava/lang/Object;
    :cond_4
    const/4 v8, 0x2

    if-eq v4, v8, :cond_5

    const/4 v8, 0x5

    if-ne v4, v8, :cond_6

    .line 332
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 333
    :cond_6
    const/4 v8, 0x4

    if-ne v4, v8, :cond_1

    .line 334
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public checkIsWMDRM(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 473
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 475
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 476
    .local v3, "mimeType":Ljava/lang/String;
    const/4 v0, -0x1

    .line 477
    .local v0, "fileType":I
    const/4 v1, 0x0

    .line 478
    .local v1, "isDrmSupported":Z
    const/4 v2, 0x0

    .line 480
    .local v2, "isPlayready":Z
    if-eqz v3, :cond_1

    .line 481
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1, v3}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 482
    if-eqz v1, :cond_1

    .line 483
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 484
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    const/4 v4, 0x5

    if-eq v0, v4, :cond_0

    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WMDRM_NOT_SUPPORT:Z

    if-nez v4, :cond_1

    const/4 v4, 0x3

    if-ne v0, v4, :cond_1

    .line 485
    :cond_0
    const/4 v2, 0x1

    .line 490
    :cond_1
    return v2
.end method

.method public checkIsWifiEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1022
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1023
    .local v0, "mWifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1024
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "checkIsWifiEnabled, Wifi is Not Enabled"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1033
    :goto_0
    return v2

    .line 1027
    :cond_0
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    .line 1028
    .local v1, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1029
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v3, "checkIsWifiEnabled Wifi is Connected"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    const/4 v2, 0x1

    goto :goto_0

    .line 1032
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "checkIsWifiEnabled Wifi is Enabled, but not connected"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkRightsStatus(Ljava/lang/String;)I
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 177
    if-nez p1, :cond_0

    .line 178
    const/4 v1, 0x1

    .line 207
    :goto_0
    return v1

    .line 180
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 182
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 183
    .local v0, "LicenseStatus":I
    const/4 v1, 0x1

    .line 184
    .local v1, "rightStatus":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRightsStatus. LicenseStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    packed-switch v0, :pswitch_data_0

    .line 201
    :pswitch_0
    const/4 v1, 0x1

    .line 205
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRightsStatus. rightStatus = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatusString:[Ljava/lang/String;

    aget-object v4, v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatus:I

    goto :goto_0

    .line 188
    :pswitch_1
    const/4 v1, 0x2

    .line 189
    goto :goto_1

    .line 192
    :pswitch_2
    const/4 v1, 0x3

    .line 193
    goto :goto_1

    .line 196
    :pswitch_3
    const/4 v1, 0x0

    .line 197
    goto :goto_1

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public divxPopupType(ILjava/lang/String;)I
    .locals 8
    .param p1, "rightStatus"    # I
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 600
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType() rightStatus: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    const/16 v2, 0x1e

    .line 603
    .local v2, "result":I
    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v3, v2

    .line 645
    .end local v2    # "result":I
    .local v3, "result":I
    :goto_1
    return v3

    .line 605
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :pswitch_1
    const/16 v2, 0x24

    .line 606
    goto :goto_0

    .line 611
    :pswitch_2
    const/16 v2, 0x26

    .line 612
    goto :goto_0

    .line 614
    :pswitch_3
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 615
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v6, 0x1

    invoke-virtual {v5, p2, v6}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v4

    .line 616
    .local v4, "rightDetails":Landroid/content/ContentValues;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType. rightDetails = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    if-eqz v4, :cond_1

    const-string v5, "license_category"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_2

    .line 619
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. null check return"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v2

    .line 620
    .end local v2    # "result":I
    .restart local v3    # "result":I
    goto :goto_1

    .line 622
    .end local v3    # "result":I
    .restart local v2    # "result":I
    :cond_2
    const-string v5, "license_category"

    invoke-virtual {v4, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 624
    .local v1, "licenseCategory":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "divxPopupType. licenseCategory = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    if-eqz v1, :cond_0

    .line 627
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 628
    .local v0, "license":I
    const/4 v5, 0x2

    if-eq v0, v5, :cond_3

    const/4 v5, 0x4

    if-ne v0, v5, :cond_5

    .line 629
    :cond_3
    invoke-virtual {p0, p2}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v5

    if-nez v5, :cond_4

    .line 630
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_NULL "

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const/16 v2, 0x1e

    goto :goto_0

    .line 633
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_DIVX_RENTAL_INFO "

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    const/16 v2, 0x27

    goto/16 :goto_0

    .line 637
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "divxPopupType. licenseCategory VIDEO_DRM_STR_NULL "

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    const/16 v2, 0x1e

    goto/16 :goto_0

    .line 603
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public getDetailInfo(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 19
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    if-nez p1, :cond_1

    .line 802
    const/4 v3, 0x0

    .line 1007
    :cond_0
    :goto_0
    return-object v3

    .line 804
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 806
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 807
    .local v3, "detailInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;>;"
    const/4 v6, 0x0

    .line 809
    .local v6, "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v14, v0, v15}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v11

    .line 811
    .local v11, "rightDetails":Landroid/content/ContentValues;
    if-nez v11, :cond_2

    .line 812
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo. rightDetails is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 813
    const/4 v3, 0x0

    goto :goto_0

    .line 816
    :cond_2
    const-string v14, "license_category"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    .line 818
    .local v8, "object":Ljava/lang/Object;
    if-nez v8, :cond_3

    .line 819
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo() rightDetails is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 820
    const/4 v3, 0x0

    goto :goto_0

    .line 823
    :cond_3
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 824
    .local v7, "licenseCategory":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. licenseCategory = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    if-eqz v7, :cond_4

    const-string v14, "NOT_FOUND_FOR_ACTION_SPECIFIED"

    invoke-virtual {v7, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 827
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v15, "getDetailInfo. licenseCategory is null"

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    const/4 v3, 0x0

    goto :goto_0

    .line 831
    :cond_5
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_6

    .line 832
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 833
    .local v1, "categoryType":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. categoryType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 835
    new-instance v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;

    .end local v6    # "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    invoke-direct {v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;-><init>()V

    .line 837
    .restart local v6    # "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    const/4 v14, 0x1

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    .line 838
    iput v1, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->constraintType:I

    .line 840
    iget v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    sparse-switch v14, :sswitch_data_0

    .line 851
    const v14, 0x7f0a0145

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    .line 855
    :goto_1
    sparse-switch v1, :sswitch_data_1

    .line 899
    :goto_2
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. info.typeStr[0] = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 900
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. info.validityStr = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 902
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 842
    :sswitch_1
    const v14, 0x7f0a019d

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 845
    :sswitch_2
    const v14, 0x7f0a019b

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 848
    :sswitch_3
    const v14, 0x7f0a019c

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_1

    .line 857
    :sswitch_4
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a019a

    aput v16, v14, v15

    goto :goto_2

    .line 861
    :sswitch_5
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 862
    .local v2, "curCount":Ljava/lang/String;
    const-string v14, "max_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 864
    .local v9, "orgCount":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0195

    aput v16, v14, v15

    .line 865
    const-string v14, "%d/%d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 869
    .end local v2    # "curCount":Ljava/lang/String;
    .end local v9    # "orgCount":Ljava/lang/String;
    :sswitch_6
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    .line 870
    .local v13, "startTime":Ljava/lang/String;
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 872
    .local v5, "endTime":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0198

    aput v16, v14, v15

    .line 873
    const-string v14, "%s - %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v13, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 877
    .end local v5    # "endTime":Ljava/lang/String;
    .end local v13    # "startTime":Ljava/lang/String;
    :sswitch_7
    const-string v14, "license_available_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    .line 879
    .local v10, "remainInterval":Ljava/lang/String;
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0197

    aput v16, v14, v15

    .line 880
    const-string v14, "%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_2

    .line 884
    .end local v10    # "remainInterval":Ljava/lang/String;
    :sswitch_8
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0198

    aput v16, v14, v15

    .line 885
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x1

    const v16, 0x7f0a0195

    aput v16, v14, v15

    goto/16 :goto_2

    .line 903
    .end local v1    # "categoryType":I
    :cond_6
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x2

    if-eq v14, v15, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    const/4 v15, 0x5

    if-ne v14, v15, :cond_0

    .line 904
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 906
    .restart local v1    # "categoryType":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "getDetailInfo. categoryType : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    new-instance v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;

    .end local v6    # "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    invoke-direct {v6}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;-><init>()V

    .line 910
    .restart local v6    # "info":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;
    const/4 v14, 0x1

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    .line 911
    iput v1, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->constraintType:I

    .line 913
    iget v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionType:I

    sparse-switch v14, :sswitch_data_2

    .line 924
    const v14, 0x7f0a0145

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    .line 928
    :goto_3
    const/4 v12, 0x0

    .line 929
    .local v12, "startDate":Ljava/lang/String;
    const/4 v4, 0x0

    .line 930
    .local v4, "endDate":Ljava/lang/String;
    const/4 v2, 0x0

    .line 932
    .restart local v2    # "curCount":Ljava/lang/String;
    packed-switch v1, :pswitch_data_0

    .line 1004
    :goto_4
    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 915
    .end local v2    # "curCount":Ljava/lang/String;
    .end local v4    # "endDate":Ljava/lang/String;
    .end local v12    # "startDate":Ljava/lang/String;
    :sswitch_9
    const v14, 0x7f0a019d

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 918
    :sswitch_a
    const v14, 0x7f0a019b

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 921
    :sswitch_b
    const v14, 0x7f0a019c

    iput v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->permissionStr:I

    goto :goto_3

    .line 934
    .restart local v2    # "curCount":Ljava/lang/String;
    .restart local v4    # "endDate":Ljava/lang/String;
    .restart local v12    # "startDate":Ljava/lang/String;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v15, 0x7f0a019a

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto :goto_4

    .line 938
    :pswitch_1
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 940
    const-string v14, "%s %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e8

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 941
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0196

    aput v16, v14, v15

    goto :goto_4

    .line 945
    :pswitch_2
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 947
    const-string v14, "%s %d %s"

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e7

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0144

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 949
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0195

    aput v16, v14, v15

    goto/16 :goto_4

    .line 953
    :pswitch_3
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 955
    const-string v14, "%s %s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e9

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 956
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0196

    aput v16, v14, v15

    goto/16 :goto_4

    .line 960
    :pswitch_4
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 961
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 963
    const-string v14, "%s %s-%s "

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e8

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    aput-object v4, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 964
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0196

    aput v16, v14, v15

    goto/16 :goto_4

    .line 968
    :pswitch_5
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 969
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 971
    const-string v14, "%s %s %d %s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e8

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0144

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 973
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0195

    aput v16, v14, v15

    goto/16 :goto_4

    .line 977
    :pswitch_6
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 978
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 980
    const-string v14, "%s %s %d %s"

    const/4 v15, 0x4

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e9

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v4, v15, v16

    const/16 v16, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0144

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 982
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0195

    aput v16, v14, v15

    goto/16 :goto_4

    .line 986
    :pswitch_7
    const-string v14, "license_start_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 987
    const-string v14, "license_expiry_time"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 988
    const-string v14, "remaining_repeat_count"

    invoke-virtual {v11, v14}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 990
    const-string v14, "%s %s-%s %d %s"

    const/4 v15, 0x5

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a00e8

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    aput-object v12, v15, v16

    const/16 v16, 0x2

    aput-object v4, v15, v16

    const/16 v16, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    const v18, 0x7f0a0144

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    .line 992
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0195

    aput v16, v14, v15

    goto/16 :goto_4

    .line 996
    :pswitch_8
    iget-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->typeStr:[I

    const/4 v15, 0x0

    const v16, 0x7f0a0002

    aput v16, v14, v15

    .line 997
    const/4 v14, 0x0

    iput-object v14, v6, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil$VideoDrmDetailInfo;->validityStr:Ljava/lang/String;

    goto/16 :goto_4

    .line 840
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x6 -> :sswitch_3
        0x7 -> :sswitch_2
    .end sparse-switch

    .line 855
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_5
        0x2 -> :sswitch_6
        0x4 -> :sswitch_7
        0x8 -> :sswitch_8
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
    .end sparse-switch

    .line 913
    :sswitch_data_2
    .sparse-switch
        0x1 -> :sswitch_9
        0x6 -> :sswitch_b
        0x7 -> :sswitch_a
    .end sparse-switch

    .line 932
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getDivxcurCnt(Ljava/lang/String;)I
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 649
    const/4 v3, -0x1

    .line 650
    .local v3, "remainedCnt":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 651
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v5

    .line 652
    .local v5, "rightDetails":Landroid/content/ContentValues;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt. rightDetails = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    if-eqz v5, :cond_0

    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 655
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxcurCnt. return;"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 670
    .end local v3    # "remainedCnt":I
    .local v4, "remainedCnt":I
    :goto_0
    return v4

    .line 658
    .end local v4    # "remainedCnt":I
    .restart local v3    # "remainedCnt":I
    :cond_1
    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 659
    .local v2, "licenseCategory":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt. licenseCategory = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    if-eqz v2, :cond_3

    .line 662
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 663
    .local v1, "license":I
    const/4 v6, 0x2

    if-eq v1, v6, :cond_2

    const/4 v6, 0x4

    if-ne v1, v6, :cond_3

    .line 664
    :cond_2
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 665
    .local v0, "divxRemainingCount":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 666
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxcurCnt() - remained count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "divxRemainingCount":Ljava/lang/String;
    .end local v1    # "license":I
    :cond_3
    move v4, v3

    .line 670
    .end local v3    # "remainedCnt":I
    .restart local v4    # "remainedCnt":I
    goto :goto_0
.end method

.method public getDivxtotalCnt(Ljava/lang/String;)I
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 674
    const/4 v3, -0x1

    .line 675
    .local v3, "orgCount":I
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 676
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v7, 0x1

    invoke-virtual {v6, p1, v7}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v5

    .line 677
    .local v5, "rightDetails":Landroid/content/ContentValues;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt. rightDetails: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    if-eqz v5, :cond_0

    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 680
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxtotalCnt. return;"

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v3

    .line 696
    .end local v3    # "orgCount":I
    .local v4, "orgCount":I
    :goto_0
    return v4

    .line 683
    .end local v4    # "orgCount":I
    .restart local v3    # "orgCount":I
    :cond_1
    const-string v6, "license_category"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 684
    .local v2, "licenseCategory":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt. licenseCategory = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    if-eqz v2, :cond_3

    .line 687
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v7, "getDivxtotalCnt() licenseCategory is not null"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 689
    .local v1, "license":I
    const/4 v6, 0x2

    if-eq v1, v6, :cond_2

    const/4 v6, 0x4

    if-ne v1, v6, :cond_3

    .line 690
    :cond_2
    const-string v6, "max_repeat_count"

    invoke-virtual {v5, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 691
    .local v0, "divxOriginalCount":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 692
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getDivxtotalCnt() - original count : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "divxOriginalCount":Ljava/lang/String;
    .end local v1    # "license":I
    :cond_3
    move v4, v3

    .line 696
    .end local v3    # "orgCount":I
    .restart local v4    # "orgCount":I
    goto :goto_0
.end method

.method public getDrmType()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    return v0
.end method

.method public getFileType(Ljava/lang/String;)I
    .locals 4
    .param p1, "drmFilename"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v2, -0x1

    .line 735
    if-eqz p1, :cond_8

    .line 736
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 738
    .local v0, "fileName":Ljava/lang/String;
    const-string v3, ".dcf"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 739
    const/4 v1, 0x1

    .line 755
    .end local v0    # "fileName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v1

    .line 740
    .restart local v0    # "fileName":Ljava/lang/String;
    :cond_1
    const-string v3, ".avi"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, ".mkv"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, ".divx"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 741
    :cond_2
    const/4 v1, 0x4

    goto :goto_0

    .line 742
    :cond_3
    const-string v3, ".pyv"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ".pya"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 744
    const-string v3, ".wmv"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, ".wma"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 745
    :cond_4
    const/4 v1, 0x3

    goto :goto_0

    .line 746
    :cond_5
    const-string v3, ".mp4"

    invoke-virtual {v0, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 748
    const-string v1, ".sm4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 749
    const/4 v1, 0x5

    goto :goto_0

    .line 750
    :cond_6
    const-string v1, "DCM"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, ".ismv"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 751
    const/4 v1, 0x6

    goto :goto_0

    :cond_7
    move v1, v2

    .line 753
    goto :goto_0

    .end local v0    # "fileName":Ljava/lang/String;
    :cond_8
    move v1, v2

    .line 755
    goto :goto_0
.end method

.method public getPopupString(IZ)Ljava/lang/String;
    .locals 20
    .param p1, "strType"    # I
    .param p2, "fromAIA"    # Z

    .prologue
    .line 494
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v12, "getPopupString - start"

    invoke-static {v9, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const/4 v8, 0x0

    .line 496
    .local v8, "str":Ljava/lang/String;
    const/4 v4, 0x0

    .line 497
    .local v4, "fileName":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 499
    .local v6, "remainedCnt":J
    const-wide/16 v10, -0x1

    .line 500
    .local v10, "totalCnt":J
    const-wide/16 v2, -0x1

    .line 502
    .local v2, "curCnt":J
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    const/16 v12, 0x2f

    invoke-virtual {v9, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v5

    .line 504
    .local v5, "lastIndex":I
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v5, v9, :cond_1

    .line 505
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    add-int/lit8 v12, v5, 0x1

    invoke-virtual {v9, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 509
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 596
    :cond_0
    :goto_1
    return-object v8

    .line 507
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0145

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 511
    :pswitch_0
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    const/4 v12, 0x1

    if-ne v9, v12, :cond_2

    .line 512
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getOMARemainedCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v6, v9

    .line 515
    :cond_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - remainedCnt = "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    const-wide/16 v12, 0x1

    cmp-long v9, v6, v12

    if-nez v9, :cond_3

    .line 518
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01b1

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 519
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 520
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 521
    if-nez p2, :cond_0

    .line 522
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ad

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 523
    :cond_3
    const-wide/16 v12, 0x2

    cmp-long v9, v6, v12

    if-nez v9, :cond_0

    .line 524
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01b2

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    const/4 v14, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 525
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 526
    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 527
    if-nez p2, :cond_0

    .line 528
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ad

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 534
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ae

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 535
    goto/16 :goto_1

    .line 538
    :pswitch_2
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    const/4 v12, 0x3

    if-ne v9, v12, :cond_4

    .line 539
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01aa

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 541
    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ab

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 543
    goto/16 :goto_1

    .line 546
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ac

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v4, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 547
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 548
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01b0

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 550
    goto/16 :goto_1

    .line 554
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a9

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 555
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 556
    if-nez p2, :cond_0

    .line 557
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01ad

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 562
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a4

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 563
    goto/16 :goto_1

    .line 566
    :pswitch_6
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 567
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 568
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a7

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 571
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 572
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a8

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 573
    goto/16 :goto_1

    .line 576
    :pswitch_7
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 577
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 578
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a7

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 580
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a0056

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 581
    if-nez p2, :cond_0

    .line 582
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a3

    invoke-virtual {v9, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    .line 586
    :pswitch_8
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxcurCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v2, v9

    .line 587
    sget-object v9, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDivxtotalCnt(Ljava/lang/String;)I

    move-result v9

    int-to-long v10, v9

    .line 588
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "getPopupString() - totalCnt : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", curCnt :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v9, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    const v12, 0x7f0a01a2

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    sub-long v16, v10, v2

    const-wide/16 v18, 0x1

    add-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v9, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 590
    goto/16 :goto_1

    .line 509
    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public getRightStatus()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatus:I

    return v0
.end method

.method public getURLInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 760
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 762
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 764
    .local v3, "mimeType":Ljava/lang/String;
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v4, 0x3

    invoke-direct {v0, v4, v3}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 765
    .local v0, "mDrmInfoRequest_ILA":Landroid/drm/DrmInfoRequest;
    const-string v4, "drm_path"

    invoke-virtual {v0, v4, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 766
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, v0}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    .line 767
    .local v1, "mDrmInfo_ILA":Landroid/drm/DrmInfo;
    const-string v4, "URL"

    invoke-virtual {v1, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 768
    .local v2, "mLicense_url":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getURLInfo. mLicense_url : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 770
    return-object v2
.end method

.method public hasDrmConstrains(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 1039
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v2, p1, v1}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1040
    .local v0, "values":Landroid/content/ContentValues;
    if-eqz v0, :cond_0

    .line 1043
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public initOMADrmConstraintsInfo(Ljava/lang/String;)Z
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x10

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 249
    if-nez p1, :cond_1

    .line 250
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "initOMADrmConstraintsInfo() - file path is null."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_0
    :goto_0
    return v3

    .line 254
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 256
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    .line 258
    .local v2, "rightDetails":Landroid/content/ContentValues;
    const-string v5, "license_category"

    invoke-virtual {v2, v5}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 259
    .local v1, "licenseCategory":Ljava/lang/String;
    const/4 v0, 0x0

    .line 261
    .local v0, "license":I
    if-eqz v1, :cond_0

    .line 263
    const-string v3, "NOT_FOUND_FOR_ACTION_SPECIFIED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 264
    const/4 v0, -0x1

    .line 268
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initOMADrmConstraintsInfo. licenseCategory = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    if-nez v0, :cond_3

    .line 271
    const/16 v3, 0x11

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 288
    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initOMADrmConstraintsInfo. mOMADrmConstraintType = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraint:[Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    add-int/lit8 v7, v7, -0xa

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v3, v4

    .line 289
    goto :goto_0

    .line 266
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 272
    :cond_3
    if-ne v0, v4, :cond_4

    .line 273
    const/16 v3, 0xb

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 274
    :cond_4
    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    .line 275
    const/16 v3, 0xd

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 276
    :cond_5
    const/4 v3, 0x4

    if-ne v0, v3, :cond_6

    .line 277
    const/16 v3, 0xc

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 278
    :cond_6
    if-ne v0, v7, :cond_7

    .line 279
    const/16 v3, 0xf

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 280
    :cond_7
    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    .line 281
    iput v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 282
    :cond_8
    const/16 v3, 0x8

    if-ne v0, v3, :cond_9

    .line 283
    const/16 v3, 0xe

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2

    .line 285
    :cond_9
    const/16 v3, 0xa

    iput v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    goto :goto_2
.end method

.method public initOMADrmDeliveryType(Ljava/lang/String;)V
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 212
    if-nez p1, :cond_1

    .line 213
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v6, "initOMADrmDeliveryType() - file path is null."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 219
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/16 v5, 0xe

    const-string v6, "application/vnd.oma.drm.content"

    invoke-direct {v2, v5, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 221
    .local v2, "drmInfoRequest_drmFileInfo":Landroid/drm/DrmInfoRequest;
    const-string v5, "drm_path"

    invoke-virtual {v2, v5, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 222
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v5, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v1

    .line 224
    .local v1, "drmInfo":Landroid/drm/DrmInfo;
    const-string v5, "version"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    .line 225
    .local v4, "drmVersion":Ljava/lang/Object;
    const-string v5, "type"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 227
    .local v3, "drmType":Ljava/lang/Object;
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    .line 230
    new-array v0, v10, [I

    .line 231
    .local v0, "DrmInfo":[I
    const-string v5, "version"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v9

    .line 232
    const-string v5, "type"

    invoke-virtual {v1, v5}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v8

    .line 233
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initOMADrmDeliveryType. version = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    aget v5, v0, v8

    if-ne v5, v8, :cond_3

    .line 236
    iput v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 244
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "initOMADrmDeliveryType. mOMADrmDeliveryType = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDelivery:[Ljava/lang/String;

    iget v8, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    aget-object v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 237
    :cond_3
    aget v5, v0, v8

    if-ne v5, v11, :cond_4

    .line 238
    iput v10, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1

    .line 239
    :cond_4
    aget v5, v0, v8

    if-nez v5, :cond_5

    .line 240
    iput v9, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1

    .line 241
    :cond_5
    aget v5, v0, v8

    if-ne v5, v10, :cond_2

    .line 242
    iput v11, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    goto :goto_1
.end method

.method public isDRM()Z
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->isDrmFile(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDivXRentalFile()Z
    .locals 5

    .prologue
    .line 1050
    const/4 v1, 0x0

    .line 1052
    .local v1, "ret":Z
    const/4 v0, -0x1

    .line 1054
    .local v0, "mDrmPopupType":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    .line 1055
    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatus:I

    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->divxPopupType(ILjava/lang/String;)I

    move-result v0

    .line 1056
    const/16 v2, 0x27

    if-ne v0, v2, :cond_1

    const/4 v1, 0x1

    .line 1059
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isDivXRentalFile. return value = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    return v1

    .line 1056
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDrmFile(Ljava/lang/String;)I
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 135
    if-nez p1, :cond_0

    .line 136
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "isDrmFile. filePath is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const/4 v0, -0x1

    .line 161
    :goto_0
    return v0

    .line 140
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 142
    sput-object p1, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    .line 143
    const/4 v0, -0x1

    .line 145
    .local v0, "drmType":I
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 147
    .local v2, "mimeType":Ljava/lang/String;
    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v3, p1, v2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 150
    .local v1, "isDrmSupported":Z
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. isDrmSupported = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    if-eqz v1, :cond_2

    .line 152
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 153
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. drmFileType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    .end local v1    # "isDrmSupported":Z
    :cond_1
    :goto_1
    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    .line 160
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isDrmFile. drmType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    .restart local v1    # "isDrmSupported":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v4, "isDrmFile. canHandle returned false. Not a drm file by extension"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public launchBrowser(Ljava/lang/String;)Z
    .locals 6
    .param p1, "mLicense_url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 774
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "launchBrowser() start "

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    if-nez p1, :cond_0

    .line 777
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "launchBrowser. url is null"

    invoke-static {v4, v5}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    :goto_0
    return v3

    .line 781
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 782
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 783
    .local v0, "browserIntent":Landroid/content/Intent;
    const/high16 v4, 0x10000000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 784
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 785
    .local v2, "mUri":Landroid/net/Uri;
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 787
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/high16 v5, 0x10000

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 790
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    :cond_1
    const/4 v3, 0x1

    goto :goto_0

    .line 791
    :catch_0
    move-exception v1

    .line 792
    .local v1, "ex":Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v5, "could not find a suitable activity for launching license url"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeDrmMgrClient()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "removeDrmClient"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 127
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 113
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mPath:Ljava/lang/String;

    .line 115
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmType:I

    .line 116
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mRightStatus:I

    .line 117
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    .line 118
    iput v1, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    .line 119
    return-void
.end method

.method public setInvalidOMADrmMsg()I
    .locals 3

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setInvalidOMADrmMsg. mOMADrmDeliveryType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget v0, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmDeliveryType:I

    packed-switch v0, :pswitch_data_0

    .line 443
    const/16 v0, 0x1e

    :goto_0
    return v0

    .line 436
    :pswitch_0
    const/16 v0, 0x20

    goto :goto_0

    .line 440
    :pswitch_1
    const/16 v0, 0x21

    goto :goto_0

    .line 433
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public setValidOMADrmMsg(Ljava/lang/String;)I
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x1e

    .line 399
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->initDrmMgrClient()V

    .line 401
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mDrmClient:Landroid/drm/DrmManagerClient;

    const/4 v8, 0x1

    invoke-virtual {v7, p1, v8}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v2

    .line 402
    .local v2, "rightDetails":Landroid/content/ContentValues;
    const-string v7, "license_category"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 403
    .local v0, "licenseCategory":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg. licenseCategory = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    iget v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    const/16 v8, 0xb

    if-ne v7, v8, :cond_1

    .line 406
    const-string v7, "remaining_repeat_count"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 407
    .local v4, "validityRemainingCount":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 409
    .local v1, "remainedCnt":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - remained count : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    const/4 v7, 0x2

    if-gt v1, v7, :cond_0

    .line 412
    const/16 v6, 0x1f

    .line 427
    .end local v1    # "remainedCnt":I
    .end local v4    # "validityRemainingCount":Ljava/lang/String;
    :cond_0
    :goto_0
    return v6

    .line 415
    :cond_1
    iget v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->mOMADrmConstraintType:I

    const/16 v8, 0xc

    if-ne v7, v8, :cond_0

    .line 416
    const-string v7, "license_original_interval"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 417
    .local v3, "validityOriginalInterval":Ljava/lang/String;
    const-string v7, "license_available_time"

    invoke-virtual {v2, v7}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 419
    .local v5, "validityRemainingInterval":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - validityOriginalInterval : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setValidOMADrmMsg() - validityRemainingInterval : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 423
    const/16 v6, 0x22

    goto :goto_0
.end method
