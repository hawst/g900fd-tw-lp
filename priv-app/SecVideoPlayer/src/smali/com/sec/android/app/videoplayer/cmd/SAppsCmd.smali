.class public Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;
.super Ljava/lang/Object;
.source "SAppsCmd.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;


# static fields
.field private static final DEFAULT_URI:Ljava/lang/String; = "samsungapps://ProductDetail/"

.field private static final DOWNLOAD_LINK:Ljava/lang/String; = "http://apps.samsung.com/mw/apps311.as"

.field public static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.samsungapps"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;->TAG:Ljava/lang/String;

    const-string v4, "execute"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    if-nez p1, :cond_0

    .line 50
    :goto_0
    return-void

    .line 30
    :cond_0
    sget v3, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->S_APPS:I

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/cmd/PackageChecker;->isAvailable(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 31
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v3, "samsungapps://ProductDetail/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 33
    .local v2, "sb":Ljava/lang/StringBuffer;
    sget-boolean v3, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_EDITOR_FULL:Z

    if-eqz v3, :cond_1

    .line 34
    const-string v3, "com.sec.android.app.vefull"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 39
    :goto_1
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 40
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x14000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 46
    .end local v2    # "sb":Ljava/lang/StringBuffer;
    :goto_2
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    .line 48
    .local v0, "excp":Landroid/content/ActivityNotFoundException;
    sget-object v3, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;->TAG:Ljava/lang/String;

    const-string v4, "ActivityNotFoundException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 36
    .end local v0    # "excp":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "sb":Ljava/lang/StringBuffer;
    :cond_1
    const-string v3, "com.sec.android.app.ve"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 42
    .end local v2    # "sb":Ljava/lang/StringBuffer;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "http://apps.samsung.com/mw/apps311.as"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_2
.end method
