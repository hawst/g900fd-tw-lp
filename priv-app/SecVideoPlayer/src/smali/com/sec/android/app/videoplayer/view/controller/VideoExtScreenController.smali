.class public Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
.super Landroid/widget/RelativeLayout;
.source "VideoExtScreenController.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field public static final DEFAULT_TIME_OUT:I = 0x1770

.field private static final FADE_OUT:I = 0x1

.field private static final FFW_RWD_NOT_SUPPORT:I = 0x3

.field private static final LONG_PRESS_TIME:J = 0x1f4L

.field public static final NO_TIMEOUT:I = 0x36ee80

.field private static final PROGRESS_RESOLUTION:J = 0x186a0L

.field private static final SHOW_PROGRESS:I = 0x2

.field private static final TAG:Ljava/lang/String; = "VideoExtScreenController"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mControllerView:Landroid/widget/RelativeLayout;

.field private mCtrlLayoutShow:Z

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDuration:J

.field private mEndTime:Landroid/widget/TextView;

.field private mExitBtn:Landroid/widget/ImageButton;

.field private mExitBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mFFBtn:Landroid/widget/ImageButton;

.field private mFFBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private final mHandler:Landroid/os/Handler;

.field private mHoldLongSeekSpeed:Z

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mPlayPauseBtn:Landroid/widget/ImageButton;

.field private mPlayPauseBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressBar:Landroid/widget/SeekBar;

.field private mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mREWBtn:Landroid/widget/ImageButton;

.field private mREWBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

.field private mbProgressDragStatus:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 78
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 55
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    .line 57
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    .line 58
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    .line 60
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    .line 61
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    .line 62
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    .line 63
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    .line 66
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 67
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 69
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    .line 70
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    .line 71
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mDuration:J

    .line 74
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z

    .line 75
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z

    .line 141
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 160
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 185
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 265
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 526
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 567
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$7;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 783
    new-instance v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$8;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    .line 79
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    .line 80
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    .line 81
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 82
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->initView()V

    .line 83
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setInvisibleCtrlViews()V

    .line 84
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->resetHoldLongSeek()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    .param p1, "x1"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->updatePausePlayBtn()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->blockSpeedSeek()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setInvisibleCtrlViews()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I

    move-result v0

    return v0
.end method

.method private blockSpeedSeek()Z
    .locals 1

    .prologue
    .line 769
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isSLinkStreamingType()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromDms()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideCtrl()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 391
    const/4 v0, 0x0

    .line 392
    .local v0, "translateOff":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOff":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0152

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v3, v3, v3, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 393
    .restart local v0    # "translateOff":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 394
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 397
    new-instance v1, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$5;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 411
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    .line 412
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 413
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 414
    return-void
.end method

.method private initProgress(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 496
    const v0, 0x7f0d015f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    .line 498
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 502
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 503
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 504
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setFocusable(Z)V

    .line 508
    :cond_0
    const v0, 0x7f0d0134

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    .line 509
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 510
    const v0, 0x7f0d0133

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 515
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 516
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 520
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 524
    :cond_2
    return-void
.end method

.method private initView()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 88
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->removeAllViews()V

    .line 89
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 92
    .local v0, "params":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->makeView()Landroid/view/View;

    move-result-object v1

    .line 93
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->addView(Landroid/view/View;)V

    .line 94
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 95
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->initViews(Landroid/view/View;)V

    .line 96
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->initProgress(Landroid/view/View;)V

    .line 97
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->updatePausePlayBtn()V

    .line 98
    return-void
.end method

.method private initViews(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 108
    const v0, 0x7f0d0157

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 111
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 112
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mExitBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 116
    :cond_0
    const v0, 0x7f0d015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 120
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 124
    :cond_1
    const v0, 0x7f0d015b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mREWBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 132
    :cond_2
    const v0, 0x7f0d0158

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    .line 133
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 134
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusableInTouchMode(Z)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mFFBtn:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 139
    :cond_3
    return-void
.end method

.method private makeView()Landroid/view/View;
    .locals 3

    .prologue
    .line 101
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 102
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030023

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    .line 103
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0152

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    .line 104
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    return-object v1
.end method

.method private resetHoldLongSeek()V
    .locals 2

    .prologue
    .line 776
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z

    if-eqz v0, :cond_0

    .line 777
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->controlRequest(I)V

    .line 778
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHoldLongSeekSpeed:Z

    .line 779
    const/16 v0, 0x1770

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    .line 781
    :cond_0
    return-void
.end method

.method private setDisableRewFfBtn()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x0

    const v2, 0x7f0d015b

    const v1, 0x7f0d0158

    .line 473
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 476
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 481
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 484
    :cond_0
    return-void
.end method

.method private setEnableProgressbar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 704
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 707
    :cond_0
    return-void
.end method

.method private setEnableRewFfBtn()V
    .locals 5

    .prologue
    const v4, 0x7f0d015b

    const v3, 0x7f0d0158

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 488
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 489
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 491
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 492
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 494
    :cond_0
    return-void
.end method

.method private setInvisibleCtrlViews()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 417
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 418
    const-string v0, "VideoExtScreenController"

    const-string v1, "setInvisibleCtrlViews: mParentView is NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 440
    :goto_0
    return-void

    .line 422
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0152

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0153

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0154

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0155

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0133

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 427
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0134

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 429
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0159

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 430
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 431
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0158

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0157

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private setPlayPauseButton(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 743
    const v1, 0x7f0a016c

    .line 744
    .local v1, "descID":I
    const v2, 0x7f02001c

    .line 745
    .local v2, "resID":I
    const-string v0, ""

    .line 747
    .local v0, "desc":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 748
    const v2, 0x7f02001c

    .line 749
    const v1, 0x7f0a016c

    .line 757
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 759
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 760
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 761
    return-void

    .line 750
    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 751
    const v2, 0x7f02001b

    .line 752
    const v1, 0x7f0a016b

    goto :goto_0

    .line 753
    :cond_2
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 754
    const v2, 0x7f02001f

    .line 755
    const v1, 0x7f0a0172

    goto :goto_0
.end method

.method private setProgress()I
    .locals 18

    .prologue
    .line 589
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z

    if-eqz v14, :cond_2

    .line 590
    :cond_0
    const/4 v10, 0x0

    .line 684
    :cond_1
    :goto_0
    return v10

    .line 593
    :cond_2
    const-string v14, "VideoExtScreenController"

    const-string v15, "setProgress E."

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v10

    .line 596
    .local v10, "position":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v4

    .line 598
    .local v4, "duration":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    if-eqz v14, :cond_f

    .line 599
    const/16 v14, 0x3e8

    if-le v4, v14, :cond_e

    .line 600
    const-wide/32 v14, 0x186a0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    int-to-long v0, v4

    move-wide/from16 v16, v0

    div-long v8, v14, v16

    .line 601
    .local v8, "pos":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v14

    if-nez v14, :cond_3

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v14

    if-eqz v14, :cond_b

    .line 602
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 607
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getBufferPercentage()I

    move-result v7

    .line 608
    .local v7, "percent":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v14

    if-nez v14, :cond_4

    .line 609
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    mul-int/lit16 v15, v7, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 613
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->checkMediaHubIsPyv()Z

    move-result v14

    if-eqz v14, :cond_6

    .line 614
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->IncompletedMediaHub()Z

    move-result v14

    if-eqz v14, :cond_d

    .line 615
    const-string v14, "VideoExtScreenController"

    const-string v15, "setProgress: MediaHub file download incomplete"

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    const-wide/16 v12, 0x0

    .line 619
    .local v12, "progress_prctg":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getMediahubDownloadingPercent()J

    move-result-wide v12

    .line 621
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_5

    .line 622
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v14

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->setIncompletedMediaHub(Z)V

    .line 625
    :cond_5
    const-wide/16 v14, 0x64

    cmp-long v14, v12, v14

    if-nez v14, :cond_c

    .line 626
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 631
    :goto_2
    const-string v14, "VideoExtScreenController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "MediaHub file downloading percent: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    .end local v12    # "progress_prctg":J
    :cond_6
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 646
    if-gtz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 647
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 651
    :cond_7
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v5

    .line 652
    .local v5, "endnewTime":Ljava/lang/String;
    const/4 v6, 0x0

    .line 653
    .local v6, "endnowTime":Ljava/lang/String;
    const/4 v3, 0x0

    .line 655
    .local v3, "currentnowTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    .line 656
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 657
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    if-eqz v14, :cond_8

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 658
    if-lez v4, :cond_8

    .line 659
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v14, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 660
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mEndTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v5}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 665
    :cond_8
    move v11, v10

    .line 666
    .local v11, "time":I
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->stringForTime(IZ)Ljava/lang/String;

    move-result-object v2

    .line 668
    .local v2, "currentnewTime":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v14, :cond_9

    .line 669
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 671
    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 672
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v14, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCurrentTime:Landroid/widget/TextView;

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 677
    :cond_9
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v14, :cond_1

    .line 678
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    if-nez v14, :cond_a

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_a

    .line 679
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    .line 681
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v14}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v14

    invoke-virtual {v14, v11}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getPlaingMode(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_1

    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    invoke-virtual {v14}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->getMeshShape()I

    move-result v14

    if-nez v14, :cond_1

    .line 682
    sget-object v14, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    const/4 v15, 0x2

    invoke-virtual {v14, v15}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setMeshShape(I)V

    goto/16 :goto_0

    .line 604
    .end local v2    # "currentnewTime":Ljava/lang/String;
    .end local v3    # "currentnowTime":Ljava/lang/String;
    .end local v5    # "endnewTime":Ljava/lang/String;
    .end local v6    # "endnowTime":Ljava/lang/String;
    .end local v11    # "time":I
    .restart local v8    # "pos":J
    :cond_b
    const-string v14, "VideoExtScreenController"

    const-string v15, "setProgress: mServiceUtil.isPauseEnable() is true."

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 628
    .restart local v7    # "percent":I
    .restart local v12    # "progress_prctg":J
    :cond_c
    long-to-float v14, v12

    const v15, 0x3f7ae148    # 0.98f

    mul-float/2addr v14, v15

    float-to-long v12, v14

    .line 629
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    long-to-int v15, v12

    mul-int/lit16 v15, v15, 0x3e8

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_2

    .line 633
    .end local v12    # "progress_prctg":J
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    goto/16 :goto_3

    .line 638
    .end local v7    # "percent":I
    .end local v8    # "pos":J
    :cond_e
    const-string v14, "VideoExtScreenController"

    const-string v15, "setProgress: duration is less than zero"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/SeekBar;->setProgress(I)V

    goto/16 :goto_3

    .line 642
    :cond_f
    const/4 v10, 0x0

    goto/16 :goto_0
.end method

.method private setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mProgressBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 701
    return-void
.end method

.method private setVisibleCtrlViews()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 443
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 444
    const-string v0, "VideoExtScreenController"

    const-string v1, "setVisibleCtrlViews: mParentView is NUL"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    :goto_0
    return-void

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0152

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0153

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0154

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0155

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0133

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0134

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0159

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d0157

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015c

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015d

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 462
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015e

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 463
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v1, 0x7f0d015f

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 466
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setDisableRewFfBtn()V

    goto/16 :goto_0

    .line 468
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setEnableRewFfBtn()V

    goto/16 :goto_0
.end method

.method private show()V
    .locals 0

    .prologue
    .line 370
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showCtrl()V

    .line 371
    return-void
.end method

.method private showCtrl()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 374
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setVisibleCtrlViews()V

    .line 375
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I

    .line 376
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->updatePausePlayBtn()V

    .line 377
    const/4 v0, 0x0

    .line 378
    .local v0, "translateOn":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    .end local v0    # "translateOn":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d0152

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v3, v3, v1, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 379
    .restart local v0    # "translateOn":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 380
    invoke-virtual {v0}, Landroid/view/animation/Animation;->startNow()V

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 382
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mControllerView:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 383
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    .line 385
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 388
    return-void
.end method

.method private stringForTime(IZ)Ljava/lang/String;
    .locals 1
    .param p1, "timeMs"    # I
    .param p2, "durationTime"    # Z

    .prologue
    .line 688
    if-eqz p2, :cond_2

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    .line 690
    :cond_0
    const-string v0, "--:--:--"

    .line 695
    :goto_0
    return-object v0

    .line 692
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 695
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->converTimeToString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updatePausePlayBtn()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 711
    const-string v0, "VideoExtScreenController"

    const-string v1, "updatePausePlayBtn E."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string v0, "VideoExtScreenController"

    const-string v1, "updatePausePlayBtn mPlayPauseBtn == null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mPlayPauseBtn:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 740
    :goto_0
    return-void

    .line 717
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 718
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setEnableRewFfBtn()V

    .line 719
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setEnableProgressbar(Z)V

    .line 721
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 722
    const-string v0, "VideoExtScreenController"

    const-string v1, "setPlayPauseButton : PAUSE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 723
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 725
    :cond_2
    const-string v0, "VideoExtScreenController"

    const-string v1, "setPlayPauseButton : PLAY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 729
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setEnableProgressbar(Z)V

    .line 730
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setDisableRewFfBtn()V

    .line 732
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 733
    const-string v0, "VideoExtScreenController"

    const-string v1, "setPlayPauseButton : STOP"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 736
    :cond_4
    const-string v0, "VideoExtScreenController"

    const-string v1, "setPlayPauseButton : PLAY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setPlayPauseButton(I)V

    goto :goto_0
.end method


# virtual methods
.method public bringToFront()V
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 349
    :cond_0
    return-void
.end method

.method public hideController()V
    .locals 2

    .prologue
    .line 365
    const-string v0, "VideoExtScreenController"

    const-string v1, "setSystemUiVisibility - set hide!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->hideCtrl()V

    .line 367
    return-void
.end method

.method public isShowing()Z
    .locals 3

    .prologue
    .line 764
    const-string v0, "VideoExtScreenController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isShowing is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 817
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 818
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 822
    const/4 v0, 0x0

    return v0
.end method

.method public showController(I)V
    .locals 4
    .param p1, "showTime"    # I

    .prologue
    const/4 v2, 0x1

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 354
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mCtrlLayoutShow:Z

    if-nez v0, :cond_0

    .line 355
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->show()V

    .line 360
    :goto_0
    const-string v0, "VideoExtScreenController"

    const-string v1, "setSystemUiVisibility - set show!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 362
    return-void

    .line 357
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I

    .line 358
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->updatePausePlayBtn()V

    goto :goto_0
.end method
