.class final Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;
.super Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;
.source "Presentationservice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/service/Presentationservice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "VideoGestureListener"
.end annotation


# static fields
.field private static final SWIPE_MAX_Y_DISTANCE:I = 0x5dc

.field private static final SWIPE_MIN_X_DISTANCE:I = 0xc8

.field private static final SWIPE_SEEK_VALUE:I = 0x1388

.field private static final SWIPE_THRESHOLD_VELOCITY:I = 0x7d0


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;->this$0:Lcom/sec/android/app/videoplayer/service/Presentationservice;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;Lcom/sec/android/app/videoplayer/service/Presentationservice$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/service/Presentationservice;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/service/Presentationservice$1;

    .prologue
    .line 585
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/service/Presentationservice$VideoGestureListener;-><init>(Lcom/sec/android/app/videoplayer/service/Presentationservice;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 636
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 642
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 629
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 648
    # getter for: Lcom/sec/android/app/videoplayer/service/Presentationservice;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/service/Presentationservice;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSingleTapConfirmed :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    invoke-super {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
