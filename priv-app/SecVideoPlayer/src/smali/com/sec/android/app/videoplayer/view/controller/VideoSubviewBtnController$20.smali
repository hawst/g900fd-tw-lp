.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mPositionStatus:I

.field msg:Landroid/os/Message;

.field position:J

.field preProgress:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 2272
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private abs(I)I
    .locals 0
    .param p1, "a"    # I

    .prologue
    .line 2283
    if-ltz p1, :cond_0

    .line 2286
    .end local p1    # "a":I
    :goto_0
    return p1

    .restart local p1    # "a":I
    :cond_0
    neg-int p1, p1

    goto :goto_0
.end method

.method private isUpdateNeeded(II)Z
    .locals 5
    .param p1, "preProgress"    # I
    .param p2, "progress"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2290
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 2291
    .local v0, "configuration":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_2

    .line 2292
    if-gez p1, :cond_1

    .line 2305
    :cond_0
    :goto_0
    return v1

    .line 2294
    :cond_1
    sub-int v3, p1, p2

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->abs(I)I

    move-result v3

    const/16 v4, 0xfa0

    if-ge v3, v4, :cond_0

    move v1, v2

    .line 2297
    goto :goto_0

    .line 2300
    :cond_2
    if-ltz p1, :cond_0

    .line 2302
    sub-int v3, p1, p2

    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->abs(I)I

    move-result v3

    const/16 v4, 0x7d0

    if-ge v3, v4, :cond_0

    move v1, v2

    .line 2305
    goto :goto_0
.end method


# virtual methods
.method public onHoverChanged(Landroid/widget/SeekBar;IZ)V
    .locals 16
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 2311
    const-string v10, "VideoSubViewBtnController"

    const-string v11, "SeekHoverListener onHoverChanged."

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2313
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v10

    if-eqz v10, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v11

    int-to-long v12, v11

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J
    invoke-static {v10, v12, v13}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2002(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_2

    .line 2413
    :cond_1
    :goto_0
    return-void

    .line 2320
    :cond_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 2321
    const-string v10, "VideoSubViewBtnController"

    const-string v11, "SeekHoverListener onHoverChanged - call onStartTrackingHover"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2322
    invoke-virtual/range {p0 .. p2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->onStartTrackingHover(Landroid/widget/SeekBar;I)V

    .line 2325
    :cond_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)J

    move-result-wide v10

    move/from16 v0, p2

    int-to-long v12, v0

    mul-long/2addr v10, v12

    const-wide/32 v12, 0x186a0

    div-long/2addr v10, v12

    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->position:J

    .line 2326
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v10, v10, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v11, 0xbb8

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 2328
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v10, :cond_4

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    if-eqz v10, :cond_4

    .line 2329
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->position:J

    long-to-int v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 2333
    :cond_4
    move-object/from16 v0, p0

    iget v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->preProgress:I

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v10, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->isUpdateNeeded(II)Z

    move-result v10

    if-nez v10, :cond_5

    .line 2334
    const-string v10, "VideoSubViewBtnController"

    const-string v11, "returned"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2338
    :cond_5
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v10, :cond_7

    .line 2339
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->preProgress:I

    .line 2340
    const-string v10, "VideoSubViewBtnController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "onHoverChanged. progress: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p2

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2342
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2343
    .local v3, "seekBarRect":Landroid/graphics/Rect;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2344
    const/4 v9, 0x0

    .line 2346
    .local v9, "x_position":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape()Z

    move-result v10

    if-nez v10, :cond_6

    .line 2347
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/widget/SeekBar;->getPaddingTop()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/widget/SeekBar;->getPaddingBottom()I

    move-result v12

    add-int/2addr v11, v12

    sub-int/2addr v10, v11

    mul-int v10, v10, p2

    int-to-long v10, v10

    const-wide/32 v12, 0x186a0

    div-long/2addr v10, v12

    long-to-int v9, v10

    .line 2352
    :goto_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 2353
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->mPositionStatus:I

    move/from16 v0, p2

    invoke-virtual {v10, v9, v11, v12, v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setRawX(IIII)V

    .line 2354
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 2355
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->position:J

    long-to-int v11, v12

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 2356
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->sendDelayedMoveMessage()V

    .line 2357
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v10

    const/16 v11, 0x384

    invoke-virtual {v10, v11}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->sendDelayedStartMessage(I)V

    goto/16 :goto_0

    .line 2349
    :cond_6
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual/range {p1 .. p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v12

    add-int/2addr v11, v12

    sub-int/2addr v10, v11

    mul-int v10, v10, p2

    int-to-long v10, v10

    const-wide/32 v12, 0x186a0

    div-long/2addr v10, v12

    long-to-int v9, v10

    goto :goto_1

    .line 2360
    .end local v3    # "seekBarRect":Landroid/graphics/Rect;
    .end local v9    # "x_position":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    if-gez v10, :cond_a

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    if-gez v10, :cond_a

    .line 2361
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v7

    .line 2362
    .local v7, "videoWidth":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v6

    .line 2364
    .local v6, "videoHeight":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08014f

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    .line 2365
    .local v5, "thumb_width":F
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08014e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    .line 2367
    .local v4, "thumb_height":F
    if-nez v7, :cond_8

    if-eqz v6, :cond_a

    .line 2368
    :cond_8
    if-le v7, v6, :cond_b

    .line 2369
    int-to-float v10, v7

    mul-float/2addr v10, v4

    int-to-float v11, v6

    div-float v5, v10, v11

    .line 2374
    :goto_2
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    float-to-int v11, v5

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2302(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I

    .line 2375
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    float-to-int v11, v4

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I

    .line 2377
    sget-boolean v10, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v10, :cond_9

    .line 2378
    const-string v10, "VideoSubViewBtnController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "videowidth : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " videoheight : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2379
    const-string v10, "VideoSubViewBtnController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mMeasuredW: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v12}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "mMeasuredH: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v12}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2382
    :cond_9
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mLeftPadding:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRightPadding:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int v8, v10, v11

    .line 2383
    .local v8, "w":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTopPadding:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mBottomPadding:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mTimetextHeight:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mArrowHeight:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v11

    add-int v2, v10, v11

    .line 2385
    .local v2, "h":I
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v11

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/HoverPopupWindow;)Landroid/widget/HoverPopupWindow;

    .line 2387
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v10

    if-eqz v10, :cond_a

    .line 2388
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v11

    new-instance v12, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v12, v8, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v10, v11, v12}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2393
    .end local v2    # "h":I
    .end local v4    # "thumb_height":F
    .end local v5    # "thumb_width":F
    .end local v6    # "videoHeight":I
    .end local v7    # "videoWidth":I
    .end local v8    # "w":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    if-lez v10, :cond_1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v10

    if-lez v10, :cond_1

    .line 2394
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->position:J

    long-to-int v10, v10

    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->preProgress:I

    .line 2396
    new-instance v10, Landroid/os/Message;

    invoke-direct {v10}, Landroid/os/Message;-><init>()V

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    .line 2397
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    const/4 v11, 0x0

    iput v11, v10, Landroid/os/Message;->what:I

    .line 2398
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->position:J

    long-to-int v11, v12

    iput v11, v10, Landroid/os/Message;->arg1:I

    .line 2399
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    move/from16 v0, p2

    iput v0, v10, Landroid/os/Message;->arg2:I

    .line 2401
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v10

    if-eqz v10, :cond_c

    const-string v10, "android.resource"

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 2402
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    new-instance v11, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v13

    invoke-static {v13}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v13

    invoke-virtual {v13}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v15}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v15

    invoke-direct {v11, v12, v13, v14, v15}, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;-><init>(Landroid/content/Context;Landroid/net/Uri;II)V

    iput-object v11, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2407
    :goto_3
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 2408
    sget-object v10, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    iget v11, v11, Landroid/os/Message;->what:I

    invoke-virtual {v10, v11}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_1

    .line 2409
    sget-object v10, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    invoke-virtual {v10, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 2371
    .restart local v4    # "thumb_height":F
    .restart local v5    # "thumb_width":F
    .restart local v6    # "videoHeight":I
    .restart local v7    # "videoWidth":I
    :cond_b
    int-to-float v10, v6

    mul-float/2addr v10, v5

    int-to-float v11, v7

    div-float v4, v10, v11

    goto/16 :goto_2

    .line 2404
    .end local v4    # "thumb_height":F
    .end local v5    # "thumb_width":F
    .end local v6    # "videoHeight":I
    .end local v7    # "videoWidth":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->msg:Landroid/os/Message;

    new-instance v11, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v12}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v13}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v14}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)I

    move-result v14

    invoke-direct {v11, v12, v13, v14}, Lcom/sec/android/app/videoplayer/util/BitmapAtTimeMsg;-><init>(Ljava/lang/String;II)V

    iput-object v11, v10, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_3
.end method

.method public onStartTrackingHover(Landroid/widget/SeekBar;I)V
    .locals 12
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    const-wide/32 v10, 0x186a0

    const/4 v2, -0x1

    .line 2416
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "SeekHoverListener onStartTrackingHover"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2417
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_0

    .line 2418
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2202(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    .line 2421
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoWidth:I
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2302(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I

    .line 2422
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMeasuredVideoHeight:I
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;I)I

    .line 2423
    iput v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->preProgress:I

    .line 2425
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->isProgressZoomPossible()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPenWindow()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v1

    int-to-long v4, v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverDuration:J
    invoke-static {v0, v4, v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2002(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;J)J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 2471
    :cond_2
    :goto_0
    return-void

    .line 2432
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 2434
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_5

    .line 2435
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 2436
    .local v7, "mHoverRect":Landroid/graphics/Rect;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2437
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v0

    int-to-long v0, v0

    int-to-long v4, p2

    mul-long/2addr v0, v4

    div-long/2addr v0, v10

    long-to-int v8, v0

    .line 2439
    .local v8, "progress_position":I
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 2440
    .local v3, "seekBarRect":Landroid/graphics/Rect;
    invoke-virtual {p1, v3}, Landroid/widget/SeekBar;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 2442
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    mul-int/2addr v0, p2

    int-to-long v0, v0

    div-long/2addr v0, v10

    long-to-int v9, v0

    .line 2444
    .local v9, "x_position":I
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2445
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setCurrentPosition(I)V

    .line 2447
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getVideoUri()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setParam(Landroid/content/Context;ILandroid/graphics/Rect;Landroid/net/Uri;Lcom/sec/android/app/videoplayer/type/SchemeType;)V

    .line 2448
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setHoverEventStart(Z)V

    .line 2449
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->init()V

    .line 2450
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->getVideoViewSize()Z

    .line 2451
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->mPositionStatus:I

    invoke-virtual {v0, v9, v1, v2, p2}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setRawX(IIII)V

    .line 2452
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setDialogPosition()V

    .line 2453
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->sendDelayedShowMessage()V

    .line 2470
    .end local v3    # "seekBarRect":Landroid/graphics/Rect;
    .end local v7    # "mHoverRect":Landroid/graphics/Rect;
    .end local v8    # "progress_position":I
    .end local v9    # "x_position":I
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setContentsDescription4TWProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V

    goto/16 :goto_0

    .line 2456
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 2457
    .local v6, "inflate":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 2459
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    const v1, 0x7f03002c

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3302(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/view/View;)Landroid/view/View;

    .line 2460
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 2461
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d018b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewShowTime:Landroid/widget/TextView;
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 2462
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 2463
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0d018a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressPreviewImage:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 2465
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/widget/HoverPopupWindow;)Landroid/widget/HoverPopupWindow;

    .line 2466
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2467
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverPopupWindowProgress:Landroid/widget/HoverPopupWindow;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mProgressContent:Landroid/view/View;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$3300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    goto/16 :goto_1
.end method

.method public onStopTrackingHover(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 2474
    const-string v0, "VideoSubViewBtnController"

    const-string v1, "SeekHoverListener onStopTrackingHover"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2475
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2102(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Z)Z

    .line 2477
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2478
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHoverEventStart:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->setHoverEventStart(Z)V

    .line 2481
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutState:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2482
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 2484
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIR_DUAL_VIEW:Z

    if-eqz v0, :cond_3

    .line 2485
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2486
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$20;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mVTPP:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$2200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->removeDelayedMessage()V

    .line 2493
    :cond_2
    :goto_0
    return-void

    .line 2489
    :cond_3
    sget-object v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2490
    sget-object v0, Lcom/sec/android/app/videoplayer/util/BitmapAtTime;->mBackHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method
