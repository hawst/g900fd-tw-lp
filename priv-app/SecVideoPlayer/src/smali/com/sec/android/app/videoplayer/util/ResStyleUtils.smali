.class public Lcom/sec/android/app/videoplayer/util/ResStyleUtils;
.super Ljava/lang/Object;
.source "ResStyleUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mResStyleUtils:Lcom/sec/android/app/videoplayer/util/ResStyleUtils;


# instance fields
.field public res_ProgressBar:[I

.field public res_ProgressBar_animationResolution:I

.field public res_ProgressBar_indeterminate:I

.field public res_ProgressBar_indeterminateBehavior:I

.field public res_ProgressBar_indeterminateDrawable:I

.field public res_ProgressBar_indeterminateDuration:I

.field public res_ProgressBar_indeterminateOnly:I

.field public res_ProgressBar_interpolator:I

.field public res_ProgressBar_max:I

.field public res_ProgressBar_maxHeight:I

.field public res_ProgressBar_maxWidth:I

.field public res_ProgressBar_minHeight:I

.field public res_ProgressBar_minWidth:I

.field public res_ProgressBar_progress:I

.field public res_ProgressBar_progressDrawable:I

.field public res_ProgressBar_secondaryProgress:I

.field public res_SeekBar:[I

.field public res_SeekBar_thumb:I

.field public res_SeekBar_thumbOffset:I

.field public res_Theme:[I

.field public res_Theme_disabledAlpha:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->TAG:Ljava/lang/String;

    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->mResStyleUtils:Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, "Theme"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_Theme:[I

    .line 14
    const-string v0, "SeekBar"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_SeekBar:[I

    .line 15
    const-string v0, "ProgressBar"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar:[I

    .line 17
    const-string v0, "Theme_disabledAlpha"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_Theme_disabledAlpha:I

    .line 18
    const-string v0, "SeekBar_thumb"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_SeekBar_thumb:I

    .line 19
    const-string v0, "SeekBar_thumbOffset"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_SeekBar_thumbOffset:I

    .line 20
    const-string v0, "ProgressBar_progressDrawable"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_progressDrawable:I

    .line 21
    const-string v0, "ProgressBar_indeterminateDuration"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_indeterminateDuration:I

    .line 22
    const-string v0, "ProgressBar_minWidth"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_minWidth:I

    .line 23
    const-string v0, "ProgressBar_maxWidth"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_maxWidth:I

    .line 24
    const-string v0, "ProgressBar_minHeight"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_minHeight:I

    .line 25
    const-string v0, "ProgressBar_maxHeight"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_maxHeight:I

    .line 26
    const-string v0, "ProgressBar_interpolator"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_interpolator:I

    .line 27
    const-string v0, "ProgressBar_max"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_max:I

    .line 28
    const-string v0, "ProgressBar_progress"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_progress:I

    .line 29
    const-string v0, "ProgressBar_secondaryProgress"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_secondaryProgress:I

    .line 30
    const-string v0, "ProgressBar_indeterminateDrawable"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_indeterminateDrawable:I

    .line 31
    const-string v0, "ProgressBar_indeterminateBehavior"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_indeterminateBehavior:I

    .line 32
    const-string v0, "ProgressBar_indeterminateOnly"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_indeterminateOnly:I

    .line 33
    const-string v0, "ProgressBar_indeterminate"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_indeterminate:I

    .line 34
    const-string v0, "ProgressBar_animationResolution"

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->getResStyleableInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->res_ProgressBar_animationResolution:I

    .line 36
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/ResStyleUtils;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->mResStyleUtils:Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->mResStyleUtils:Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    .line 41
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->mResStyleUtils:Lcom/sec/android/app/videoplayer/util/ResStyleUtils;

    return-object v0
.end method

.method private getResStyleableInt(Ljava/lang/String;)I
    .locals 9
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    sget-object v7, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->TAG:Ljava/lang/String;

    const-string v8, "@---getResStyleableInt"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :try_start_0
    const-string v7, "com.android.internal.R$styleable"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 83
    .local v2, "fields2":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 85
    .local v1, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v1}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 87
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 95
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    .end local v2    # "fields2":[Ljava/lang/reflect/Field;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :goto_1
    return v5

    .line 83
    .restart local v0    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v1    # "f":Ljava/lang/reflect/Field;
    .restart local v2    # "fields2":[Ljava/lang/reflect/Field;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 91
    .end local v0    # "arr$":[Ljava/lang/reflect/Field;
    .end local v1    # "f":Ljava/lang/reflect/Field;
    .end local v2    # "fields2":[Ljava/lang/reflect/Field;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :catch_0
    move-exception v6

    .line 92
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->TAG:Ljava/lang/String;

    const-string v8, "getResDeclareStyleableInt exception"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .end local v6    # "t":Ljava/lang/Throwable;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getResStyleableIntArray(Ljava/lang/String;)[I
    .locals 10
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 55
    sget-object v8, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->TAG:Ljava/lang/String;

    const-string v9, "@---getResStyleableIntArray"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :try_start_0
    const-string v8, "com.android.internal.R$styleable"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    .line 61
    .local v3, "fields2":[Ljava/lang/reflect/Field;
    move-object v1, v3

    .local v1, "arr$":[Ljava/lang/reflect/Field;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v1, v4

    .line 63
    .local v2, "f":Ljava/lang/reflect/Field;
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 65
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [I

    move-object v0, v8

    check-cast v0, [I

    move-object v6, v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "f":Ljava/lang/reflect/Field;
    .end local v3    # "fields2":[Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    :goto_1
    return-object v6

    .line 61
    .restart local v1    # "arr$":[Ljava/lang/reflect/Field;
    .restart local v2    # "f":Ljava/lang/reflect/Field;
    .restart local v3    # "fields2":[Ljava/lang/reflect/Field;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 69
    .end local v1    # "arr$":[Ljava/lang/reflect/Field;
    .end local v2    # "f":Ljava/lang/reflect/Field;
    .end local v3    # "fields2":[Ljava/lang/reflect/Field;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :catch_0
    move-exception v7

    .line 70
    .local v7, "t":Ljava/lang/Throwable;
    sget-object v8, Lcom/sec/android/app/videoplayer/util/ResStyleUtils;->TAG:Ljava/lang/String;

    const-string v9, "getResDeclareStyleableIntArray exception"

    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
