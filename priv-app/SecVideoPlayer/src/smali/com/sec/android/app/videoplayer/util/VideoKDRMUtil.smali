.class public Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;
.super Ljava/lang/Object;
.source "VideoKDRMUtil.java"


# static fields
.field public static final DRM_BEFORE_DATE:I = -0xfffdff9

.field public static final DRM_CHECK_COUNT_POPUP:I = 0xb

.field public static final DRM_DATE_FILE:I = 0x2

.field public static final DRM_ERROR:I = -0x1

.field public static final DRM_EXIT_POPUP:I = 0xd

.field public static final DRM_EXPIRE_POPUP:I = 0xe

.field public static final DRM_INVALID_FLASH:I = -0xfffdff3

.field public static final DRM_LIMIT_DATE:I = -0xfffdffc

.field public static final DRM_MON_FILE:I = 0x1

.field public static final DRM_NEXT_POPUP:I = 0xf

.field public static final DRM_NONE_POPUP:I = 0xa

.field public static final DRM_NOT_EXIST_FILE:I = -0xfffdfff

.field public static final DRM_OUT_OF_COUNT:I = -0xfffdffa

.field public static final DRM_PARSER_ERROR:I = -0xfffdffe

.field public static final DRM_PREV_POPUP:I = 0x10

.field public static final DRM_REPLAY_POPUP:I = 0xc

.field public static final DRM_TYPE_INVALID_TERUTEN:I = 0x4

.field public static final DRM_TYPE_MAX:I = 0x5

.field public static final DRM_TYPE_NED:I = 0x2

.field public static final DRM_TYPE_NOMEDIA:I = 0x3

.field public static final DRM_TYPE_NORMAL:I = 0x0

.field public static final DRM_TYPE_TERUTEN:I = 0x1

.field public static final KDRM_SUCCESS:I

.field private static sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

.field private static sVideoKDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "VideoKDRMUtil"

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    .line 49
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->mContext:Landroid/content/Context;

    .line 51
    return-void
.end method

.method public static LmsLogClose(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 179
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 180
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LogLmsClose(I)V

    .line 182
    :cond_0
    return-void
.end method

.method public static LmsLogOpen()V
    .locals 2

    .prologue
    .line 173
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 174
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LogLmsOpen()V

    .line 176
    :cond_0
    return-void
.end method

.method public static LmsLogPlay(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 185
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 186
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LogLmsPlay(I)V

    .line 188
    :cond_0
    return-void
.end method

.method public static LmsLogStop(I)V
    .locals 2
    .param p0, "pos"    # I

    .prologue
    .line 191
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 192
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->LogLmsStop(I)V

    .line 194
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sVideoKDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sVideoKDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    .line 57
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sVideoKDRMUtil:Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;

    return-object v0
.end method


# virtual methods
.method public checkKdrmFile()I
    .locals 6

    .prologue
    .line 72
    const/16 v0, 0xa

    .line 74
    .local v0, "retValue":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getTotalCount()I

    move-result v3

    .line 75
    .local v3, "totalCnt":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getDrmPlaybackStatus()I

    move-result v2

    .line 76
    .local v2, "status":I
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getDrmType()I

    move-result v4

    .line 78
    .local v4, "type":I
    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    move v1, v0

    .line 95
    .end local v0    # "retValue":I
    .local v1, "retValue":I
    :goto_0
    return v1

    .line 82
    .end local v1    # "retValue":I
    .restart local v0    # "retValue":I
    :cond_0
    const v5, -0xfffdffa

    if-eq v2, v5, :cond_1

    const v5, -0xfffdffc

    if-eq v2, v5, :cond_1

    const v5, -0xfffdfff

    if-eq v2, v5, :cond_1

    const v5, -0xfffdffe

    if-eq v2, v5, :cond_1

    const v5, -0xfffdff9

    if-eq v2, v5, :cond_1

    const v5, -0xfffdff3

    if-eq v2, v5, :cond_1

    const/4 v5, -0x1

    if-ne v2, v5, :cond_3

    .line 90
    :cond_1
    const/16 v0, 0xe

    :cond_2
    :goto_1
    move v1, v0

    .line 95
    .end local v0    # "retValue":I
    .restart local v1    # "retValue":I
    goto :goto_0

    .line 91
    .end local v1    # "retValue":I
    .restart local v0    # "retValue":I
    :cond_3
    if-lez v3, :cond_2

    .line 92
    const/16 v0, 0xb

    goto :goto_1
.end method

.method public checkMTPConnected()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 61
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getDrmType()I

    move-result v0

    .line 62
    .local v0, "type":I
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "mtp_running_status"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v1, v3, :cond_1

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    if-ne v0, v1, :cond_1

    .line 64
    :cond_0
    const-string v2, "TAG"

    const-string v3, "MTP Connected"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    const v3, 0x7f0a0059

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 68
    :goto_0
    return v1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public discountKDRM(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 149
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 150
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->DiscountCall(Ljava/lang/String;)V

    .line 152
    :cond_0
    return-void
.end method

.method public displayLog(Ljava/lang/String;)V
    .locals 3
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 197
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "##################################"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM File path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-nez v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "displayLog() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDrmPlaybackStatus()= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :goto_1
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDRmType()          = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getAvailableCount     = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getAvailableCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getTotalCount         = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getTotalCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDisableCapture     = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDisableCapture()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDisableTVOut       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDisableTVOut()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getwatermarkFlag      = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getWatermarkFlag()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getWatermark().length = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getWatermark()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getWaterMarkStr       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->getWaterMarkString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDRmType()          = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DRM getDrmPlaybackStatus()= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "##################################"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 207
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_OUT_OF_COUNT"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 210
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_LIMIT_DATE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 213
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_NOT_EXIST_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 216
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_PARSER_ERROR"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 219
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_BEFORE_DATE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 222
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_INVALID_FLASH"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 225
    :sswitch_6
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_ERROR"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 228
    :sswitch_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_MON_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 231
    :sswitch_8
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDrmPlaybackStatus()= DRM_DATE_FILE"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 241
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NORMAL"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 244
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_TERUTEN"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 247
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NED"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 250
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_NOMEDIA"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 253
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_INVALID_TERUTEN"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 256
    :pswitch_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "DRM getDRmType()          = DRM_TYPE_MAX"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 205
    :sswitch_data_0
    .sparse-switch
        -0xfffdfff -> :sswitch_2
        -0xfffdffe -> :sswitch_3
        -0xfffdffc -> :sswitch_1
        -0xfffdffa -> :sswitch_0
        -0xfffdff9 -> :sswitch_4
        -0xfffdff3 -> :sswitch_5
        -0x1 -> :sswitch_6
        0x1 -> :sswitch_7
        0x2 -> :sswitch_8
    .end sparse-switch

    .line 239
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getAvailableCount()I
    .locals 2

    .prologue
    .line 131
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 132
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getAvailableCount()I

    move-result v0

    .line 135
    :goto_0
    return v0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getAvailableCount() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisableCapture()Z
    .locals 2

    .prologue
    .line 113
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 114
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDisableCapture()Z

    move-result v0

    .line 117
    :goto_0
    return v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getDisableCapture() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDisableTVOut()Z
    .locals 2

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 105
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDisableTVOut()Z

    move-result v0

    .line 108
    :goto_0
    return v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getDisableTVOut() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrmPlaybackStatus()I
    .locals 2

    .prologue
    .line 140
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmPlaybackStatus()I

    move-result v0

    .line 144
    :goto_0
    return v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getDrmPlaybackStatus() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDrmType()I
    .locals 2

    .prologue
    .line 155
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getDrmType()I

    move-result v0

    .line 159
    :goto_0
    return v0

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getDrmType() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalCount()I
    .locals 2

    .prologue
    .line 122
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getTotalCount()I

    move-result v0

    .line 126
    :goto_0
    return v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->TAG:Ljava/lang/String;

    const-string v1, "getTotalCount() :: sDrmInfo is null"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWaterMarkString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "waterMarkStr":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getWatermarkFlag()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    new-instance v0, Ljava/lang/String;

    .end local v0    # "waterMarkStr":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;->getWatermark()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    .line 168
    .restart local v0    # "waterMarkStr":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public roadKDRMInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 99
    new-instance v0, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    invoke-direct {v0, p1}, Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->sDrmInfo:Lcom/sec/android/app/videoplayer/util/DrmKDRMInfo;

    .line 100
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoKDRMUtil;->displayLog(Ljava/lang/String;)V

    .line 101
    return-void
.end method
