.class Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;
.super Landroid/database/ContentObserver;
.source "VideoWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/16 v4, 0x7d0

    const/16 v3, 0x3e8

    .line 430
    const-string v0, "VideoWidget"

    const-string v1, "onChange()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const-string v0, "VideoWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChange() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    sget-object v0, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v0

    if-nez v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 434
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 443
    :goto_0
    return-void

    .line 435
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {p2, v0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    move-result v0

    if-nez v0, :cond_2

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 438
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService$1;->this$0:Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;

    # getter for: Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->mContentHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;->access$000(Lcom/sec/android/app/videoplayer/appwidget/VideoWidget$UpdateService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
