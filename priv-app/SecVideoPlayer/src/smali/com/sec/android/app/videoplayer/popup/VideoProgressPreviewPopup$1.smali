.class Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;
.super Ljava/lang/Object;
.source "VideoProgressPreviewPopup.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->playVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 558
    const-string v0, "VideoProgressPreviewPopup"

    const-string v1, "onPrepared"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mStartPosition:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$1000(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 560
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 561
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->mIsInitialized:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;->access$702(Lcom/sec/android/app/videoplayer/popup/VideoProgressPreviewPopup;Z)Z

    .line 562
    return-void
.end method
