.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;
.super Ljava/lang/Object;
.source "ChangePlayerScanGuidePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I

    move-result v0

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 232
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 233
    return-void

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$5;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x12c

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
