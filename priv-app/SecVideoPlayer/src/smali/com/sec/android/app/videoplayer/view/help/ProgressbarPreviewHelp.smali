.class public Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;
.super Ljava/lang/Object;
.source "ProgressbarPreviewHelp.java"


# static fields
.field private static final ANIMATIOT_START_OFFSET:I = 0x2bc


# instance fields
.field private mBlockBottom:Landroid/widget/RelativeLayout;

.field private mBlockKeyListener:Landroid/view/View$OnKeyListener;

.field private mBlockTop:Landroid/widget/RelativeLayout;

.field private mBlockTouchListener:Landroid/view/View$OnTouchListener;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mHelpText:Landroid/widget/RelativeLayout;

.field private mHelpTextView:Landroid/widget/TextView;

.field private mHelpTextVisibility:I

.field private mHoverListenerDirection:Landroid/view/View$OnHoverListener;

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mProgressArea:Landroid/widget/RelativeLayout;

.field private mProgressHelpView:Landroid/widget/RelativeLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 27
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressArea:Landroid/widget/RelativeLayout;

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTop:Landroid/widget/RelativeLayout;

    .line 33
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockBottom:Landroid/widget/RelativeLayout;

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    .line 37
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    .line 39
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    .line 95
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$1;-><init>(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    .line 104
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$2;-><init>(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    .line 112
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$3;-><init>(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHoverListenerDirection:Landroid/view/View$OnHoverListener;

    .line 128
    new-instance v0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp$4;-><init>(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHandler:Landroid/os/Handler;

    .line 45
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    .line 46
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;

    .prologue
    .line 22
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->setVisibilityHelpText(Z)V

    return-void
.end method

.method private setVisibilityHelpText(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 153
    if-eqz p1, :cond_1

    .line 154
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilityProgressbarPreview(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 175
    :cond_0
    return-void
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 58
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 51
    .restart local p1    # "view":Landroid/view/View;
    :cond_1
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 53
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f030016

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->initViews()V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->hide()V

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 170
    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->setVisibilityProgressbarPreview(I)V

    .line 166
    return-void
.end method

.method public initViews()V
    .locals 4

    .prologue
    const v3, 0x7f0a0082

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d00f2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTop:Landroid/widget/RelativeLayout;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTop:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 64
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTop:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 66
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d00f3

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockBottom:Landroid/widget/RelativeLayout;

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockBottom:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 68
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockBottom:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBlockKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 70
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d00e8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    .line 71
    iget v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->setVisibilityHelpText(Z)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d00f5

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextView:Landroid/widget/TextView;

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isHelpHoverType()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "hoverType":Ljava/lang/String;
    const-string v1, "help"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextView:Landroid/widget/TextView;

    const v2, 0x7f0a0083

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 82
    :cond_2
    :goto_2
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_FINGER_AIR_VIEW:Z

    if-nez v1, :cond_3

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 86
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    const v2, 0x7f0d00f4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressArea:Landroid/widget/RelativeLayout;

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressArea:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHoverListenerDirection:Landroid/view/View$OnHoverListener;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    const/high16 v2, 0x7f040000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 90
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-eqz v1, :cond_0

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x2bc

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    goto/16 :goto_0

    .line 71
    .end local v0    # "hoverType":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 78
    .restart local v0    # "hoverType":Ljava/lang/String;
    :cond_5
    const-string v1, "pen"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method public isEnabledProgressOptionByHoverType(I)Z
    .locals 5
    .param p1, "toolType"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 196
    const/4 v0, 0x0

    .line 198
    .local v0, "enabled":Z
    if-ne p1, v3, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "finger_air_view_information_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v3, :cond_1

    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "pen_hovering_progress_preview"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 202
    :cond_1
    const/4 v0, 0x1

    .line 205
    :cond_2
    return v0
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public releaseView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 187
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    .line 188
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    .line 189
    return-void
.end method

.method public removeAllView()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViewsInLayout()V

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mProgressHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 182
    :cond_1
    return-void
.end method

.method public restart()V
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->show()V

    .line 149
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpTextVisibility:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->setVisibilityHelpText(Z)V

    .line 150
    return-void

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->setVisibilityProgressbarPreview(I)V

    .line 162
    return-void
.end method

.method public showInvalidAction()V
    .locals 2

    .prologue
    .line 192
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    const v1, 0x7f0a007e

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 193
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->show()V

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mHelpText:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/help/ProgressbarPreviewHelp;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->start()V

    .line 145
    return-void
.end method
