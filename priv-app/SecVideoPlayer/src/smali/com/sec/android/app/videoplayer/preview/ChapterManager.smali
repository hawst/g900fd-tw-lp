.class public Lcom/sec/android/app/videoplayer/preview/ChapterManager;
.super Ljava/lang/Object;
.source "ChapterManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/preview/ChapterManager$Const;,
        Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
    }
.end annotation


# static fields
.field static ChapterCount:I

.field static DisplayHeight:I

.field static DisplayWidth:I

.field static Duration:I

.field static FileName:Ljava/lang/String;

.field static FilePath:Ljava/lang/String;

.field static Fps:I

.field static LongDuration:I

.field static ShortDuration:I

.field private static final TAG:Ljava/lang/String;

.field static ThumbnailHeight:I

.field static ThumbnailPath:Ljava/lang/String;

.field static ThumbnailWidth:I

.field static TimeInterval:I

.field static bStart:Z

.field private static sChapterManager:Lcom/sec/android/app/videoplayer/preview/ChapterManager;


# instance fields
.field private bExternalSd:Z

.field mContext:Landroid/content/Context;

.field private mFileMgr:Lcom/sec/android/videowall/FileMgr;

.field mHandler:Landroid/os/Handler;

.field mTaskReference:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;",
            ">;"
        }
    .end annotation
.end field

.field protected mVideoId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    .line 22
    new-instance v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->sChapterManager:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/preview/ChapterManager;)Lcom/sec/android/videowall/FileMgr;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    return-object v0
.end method

.method private createLiveThumbnail(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "duration"    # I

    .prologue
    .line 281
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 282
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    .line 285
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    if-ge v0, v2, :cond_1

    sget-boolean v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bStart:Z

    if-eqz v2, :cond_1

    .line 286
    new-instance v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;

    invoke-direct {v1, p0, v0, p3}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;-><init>(Lcom/sec/android/app/videoplayer/preview/ChapterManager;II)V

    .line 287
    .local v1, "task":Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 288
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 285
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    .end local v1    # "task":Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
    :cond_1
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/preview/ChapterManager;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->sChapterManager:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    return-object v0
.end method

.method private getVideowallData()V
    .locals 1

    .prologue
    .line 259
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getNumberOfChapter()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    .line 260
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getThumbnailFps()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Fps:I

    .line 262
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getShortTimeDuration()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ShortDuration:I

    .line 263
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getLongTimeDuration()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->LongDuration:I

    .line 265
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailDisplayWidth()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayWidth:I

    .line 266
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailDisplayHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayHeight:I

    .line 267
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailWidth()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    .line 268
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_getChapterViewThumbnailHeight()I

    move-result v0

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    .line 269
    return-void
.end method

.method private printInfo()Ljava/lang/String;
    .locals 2

    .prologue
    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PreviewDialog [mFilePath="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimeInterval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bExternalSd="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bExternalSd:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mChapterCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThumbnailWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mThumbnailHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Fps:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ShortDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ShortDuration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", LongDuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->LongDuration:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DisplayWidth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayWidth:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", DisplayHeight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->DisplayHeight:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 159
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_stopTranscoding(I)I

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 163
    .local v0, "chapterIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;

    .line 165
    .local v1, "task":Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->cancel(Z)Z

    goto :goto_0

    .line 169
    .end local v0    # "chapterIter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;>;"
    .end local v1    # "task":Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_close()I

    .line 170
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mTaskReference:Ljava/util/ArrayList;

    .line 180
    :cond_0
    return-void
.end method

.method public deleteThumbnailFile(I)V
    .locals 7
    .param p1, "chapter"    # I

    .prologue
    .line 188
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    .line 189
    return-void
.end method

.method public execute()V
    .locals 3

    .prologue
    .line 139
    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    const-string v2, "execute"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    invoke-virtual {v1}, Lcom/sec/android/videowall/FileMgr;->hasFreeSpace()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ShortDuration:I

    if-lt v1, v2, :cond_2

    .line 143
    const/4 v0, 0x3

    .line 145
    .local v0, "duration":I
    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->LongDuration:I

    if-lt v1, v2, :cond_0

    .line 146
    const/16 v0, 0xa

    .line 148
    :cond_0
    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->createLiveThumbnail(Ljava/lang/String;Ljava/lang/String;I)V

    .line 153
    .end local v0    # "duration":I
    :cond_1
    :goto_0
    return-void

    .line 150
    :cond_2
    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    const-string v2, "duration is too small ==> not Live but Static thumbnail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public existThumbnailFile(I)Z
    .locals 7
    .param p1, "chapter"    # I

    .prologue
    .line 198
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v0

    return v0
.end method

.method public getBitmapAtTime(ILandroid/graphics/Bitmap;)Z
    .locals 8
    .param p1, "chapter"    # I
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 229
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    mul-int/2addr v1, p1

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    sget v4, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    sget v6, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    move-object v2, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getBitmapAtTime(Ljava/lang/String;ILandroid/graphics/Bitmap;IIZI)I

    move-result v0

    if-ne v0, v7, :cond_0

    move v5, v7

    :cond_0
    return v5
.end method

.method public getKeyframeTime(I)I
    .locals 1
    .param p1, "chapter"    # I

    .prologue
    .line 250
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailPath:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getKeyframeTime(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getThumbnailFileName(I)Ljava/lang/String;
    .locals 7
    .param p1, "chapter"    # I

    .prologue
    .line 208
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public prepare()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 111
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_create()V

    .line 112
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_open(Z)I

    .line 114
    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_getDurationTime(Ljava/lang/String;Z)I

    move-result v2

    sput v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    .line 115
    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    if-gtz v2, :cond_0

    .line 132
    :goto_0
    return v0

    .line 119
    :cond_0
    sget v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    const/16 v2, 0x3c

    if-ge v0, v2, :cond_2

    .line 120
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    const-string v2, "duration is shorter than 60sec"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    sget v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    div-int/lit8 v0, v0, 0x5

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    .line 122
    sget v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    if-nez v0, :cond_1

    .line 123
    sput v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    .line 125
    :cond_1
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    .line 130
    :goto_1
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->printInfo()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 132
    goto :goto_0

    .line 127
    :cond_2
    sget v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Duration:I

    sget v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    div-int/2addr v0, v2

    sput v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    goto :goto_1
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/preview/ChapterManager;
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mContext:Landroid/content/Context;

    .line 60
    return-object p0
.end method

.method public setData(J)Z
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->Global_setDevice(Landroid/content/Context;)V

    .line 85
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    .line 86
    new-instance v0, Lcom/sec/android/videowall/FileMgr;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/videowall/FileMgr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    .line 88
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoid : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/videowall/FileMgr;->getPathFileNameByVideoid(J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/videowall/FileMgr;->getFileNameByVideoid(J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    .line 93
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    :cond_0
    const/4 v0, 0x0

    .line 102
    :goto_0
    return v0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFilePath(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailPath:Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/videowall/FileMgr;->checkExtSdCardFile(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->bExternalSd:Z

    .line 100
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->getVideowallData()V

    .line 102
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/preview/ChapterManager;
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mHandler:Landroid/os/Handler;

    .line 73
    return-object p0
.end method

.method public setKeyframeTime(I)I
    .locals 1
    .param p1, "chapter"    # I

    .prologue
    .line 240
    sget-object v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailPath:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setKeyframeTime(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public setThumbnail(I)Z
    .locals 7
    .param p1, "chapter"    # I

    .prologue
    const/4 v0, 0x1

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;

    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    move v6, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setThumbnail(ILjava/lang/String;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
