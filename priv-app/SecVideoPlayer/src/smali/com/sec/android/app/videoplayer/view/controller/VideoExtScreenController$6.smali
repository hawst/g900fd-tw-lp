.class Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;
.super Ljava/lang/Object;
.source "VideoExtScreenController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field position:J

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 530
    if-eqz p3, :cond_0

    .line 531
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getDuration()I

    move-result v1

    int-to-long v2, v1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mDuration:J
    invoke-static {v0, v2, v3}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$702(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;J)J

    .line 536
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mDuration:J
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$700(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)J

    move-result-wide v0

    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x186a0

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->position:J

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$802(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->position:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 540
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)I

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const/16 v1, 0x1770

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 562
    const-string v0, "VideoExtScreenController"

    const-string v1, "onStartTrackingTouch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$802(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z

    .line 564
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 6
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x0

    .line 547
    const-string v0, "VideoExtScreenController"

    const-string v1, "onStopTrackingTouch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getPauseEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress(I)V
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;I)V

    .line 559
    :goto_0
    return-void

    .line 553
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mbProgressDragStatus:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$802(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;Z)Z

    .line 554
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->position:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 556
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->setProgress()I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;)I

    .line 558
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController$6;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;

    const/16 v1, 0x1770

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoExtScreenController;->showController(I)V

    goto :goto_0
.end method
