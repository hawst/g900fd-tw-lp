.class public Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;
.super Ljava/lang/Object;
.source "VideoAdaptSoundPopup.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/popup/IVideoPopup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;
    }
.end annotation


# static fields
.field public static final ADAPT_SOUND_OFF:I = 0x1

.field public static final ADAPT_SOUND_ON:I = 0x0

.field private static final ID:I = 0x8

.field private static final TAG:Ljava/lang/String; = "VideoAdaptSoundPopup"


# instance fields
.field private final transient dismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;

.field mIsActive:Z

.field private mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    .line 36
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    .line 151
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$4;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    .line 191
    new-instance v0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$5;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 41
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->isEnableAdaptSoundPath()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->setAdaptSound(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method private isEnableAdaptSoundPath()Z
    .locals 2

    .prologue
    .line 202
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 203
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 204
    :cond_0
    const/4 v1, 0x1

    .line 207
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setAdaptSound(Z)V
    .locals 1
    .param p1, "turnOn"    # Z

    .prologue
    .line 211
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setAdaptSound(Z)V

    .line 212
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    .line 52
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 47
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0x8

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 62
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnSelectedListener(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mOnSelectedListener:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$OnSelectedListener;

    .line 149
    return-void
.end method

.method public showPopup()V
    .locals 5

    .prologue
    .line 66
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    const-string v4, "VideoAdaptSoundPopup"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mIsActive:Z

    .line 69
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 70
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0008

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 71
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mKeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    const v4, 0x7f0a0026

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$1;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 79
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/AdaptSound;->isAlreadyDiagnosed(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    const v3, 0x7f0a0009

    new-instance v4, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$2;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    :cond_0
    const/4 v2, 0x0

    .line 96
    .local v2, "selected":I
    iget-boolean v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mIsActive:Z

    if-eqz v3, :cond_1

    .line 97
    const/4 v2, 0x0

    .line 103
    :goto_0
    const/4 v0, 0x0

    .line 105
    .local v0, "arrayEntries":I
    const/high16 v0, 0x7f060000

    .line 107
    new-instance v3, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$3;-><init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V

    invoke-virtual {v1, v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 137
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    .line 138
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    sget-boolean v4, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 141
    return-void

    .line 100
    .end local v0    # "arrayEntries":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method
