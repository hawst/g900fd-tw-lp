.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 3727
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 3729
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3730
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSPenReceiver. action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3732
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-nez v1, :cond_1

    .line 3753
    :cond_0
    :goto_0
    return-void

    .line 3734
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3735
    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3736
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3737
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    .line 3741
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 3739
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto :goto_1

    .line 3742
    :cond_3
    sget-object v1, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3744
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    if-ne v1, v2, :cond_4

    .line 3745
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    .line 3746
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto :goto_0

    .line 3747
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    if-ne v1, v2, :cond_0

    .line 3748
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 3749
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$19;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    sget-object v2, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3502(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto :goto_0
.end method
