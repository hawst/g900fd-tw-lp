.class public Lcom/sec/android/app/videoplayer/common/manager/MotionManager;
.super Ljava/lang/Object;
.source "MotionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;
    }
.end annotation


# static fields
.field public static final PAUSE_VIDEO:I = 0xa

.field public static final SIDE_MIRROR:I = 0x3d

.field public static final SYSTEM_MOTION_PEEK_CHAPTER_PREVIEW:Ljava/lang/String; = "motion_peek_chapter_preview"

.field public static final TAG:Ljava/lang/String; = "MotionManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsMotionListenerRegistered:Z

.field private mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private final mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onMotionListener"    # Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mIsMotionListenerRegistered:Z

    .line 33
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mContext:Landroid/content/Context;

    .line 50
    const-string v0, "MotionManager"

    const-string v1, "MotionManager E"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mContext:Landroid/content/Context;

    .line 54
    if-eqz p1, :cond_1

    .line 55
    const-string v0, "motion_recognition"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager$1;

    invoke-direct {v0, p0, p2}, Lcom/sec/android/app/videoplayer/common/manager/MotionManager$1;-><init>(Lcom/sec/android/app/videoplayer/common/manager/MotionManager;Lcom/sec/android/app/videoplayer/common/manager/MotionManager$OnMotionListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 72
    :cond_0
    return-void

    .line 57
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 58
    const-string v0, "MotionManager"

    const-string v1, "MotionManager Context is NULL!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->stackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public isMotionPeekEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_1

    .line 93
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_peek_chapter_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 95
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 93
    goto :goto_0

    :cond_1
    move v0, v1

    .line 95
    goto :goto_0
.end method

.method public registerMotionListener()V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mIsMotionListenerRegistered:Z

    if-nez v0, :cond_0

    .line 76
    const-string v0, "MotionManager"

    const-string v1, "registerMotionListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    const v2, 0x20020

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mIsMotionListenerRegistered:Z

    .line 80
    :cond_0
    return-void
.end method

.method public unregisterMotionListener()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mIsMotionListenerRegistered:Z

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "MotionManager"

    const-string v1, "unregisterMotionListener"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/common/manager/MotionManager;->mIsMotionListenerRegistered:Z

    .line 89
    :cond_0
    return-void
.end method
