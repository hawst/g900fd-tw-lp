.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;
.super Ljava/lang/Object;
.source "VideoSubviewBtnController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 1482
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x2

    .line 1484
    const/4 v1, 0x0

    .line 1485
    .local v1, "retVal":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mRoot:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1486
    sparse-switch p2, :sswitch_data_0

    .line 1508
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->commonKeyListener(ILandroid/view/KeyEvent;)Z
    invoke-static {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$600(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 1509
    .local v0, "isReturn":Z
    if-eqz v0, :cond_1

    .line 1527
    .end local v0    # "isReturn":Z
    :cond_0
    :goto_0
    return v1

    .line 1489
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1491
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const v3, 0x36ee80

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1492
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 1496
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->playSoundEffect(I)V

    .line 1497
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    .line 1498
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1499
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1512
    .restart local v0    # "isReturn":Z
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 1514
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1515
    goto :goto_0

    .line 1518
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$13;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 1519
    goto :goto_0

    .line 1486
    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 1489
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 1512
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
