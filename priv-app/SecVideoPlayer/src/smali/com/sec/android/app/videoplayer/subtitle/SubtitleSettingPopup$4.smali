.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;
.super Ljava/lang/Object;
.source "SubtitleSettingPopup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;)V
    .locals 0

    .prologue
    .line 623
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 626
    const v1, 0x7f0d01ad

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 628
    .local v0, "cb":Landroid/widget/CheckBox;
    if-nez v0, :cond_0

    .line 647
    :goto_0
    return-void

    .line 632
    :cond_0
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 633
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 634
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSMPTETTCustomMode(Z)V

    .line 640
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSubtitleActive()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->getSMPTETTCustomMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    .line 646
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refresh()V

    goto :goto_0

    .line 636
    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 637
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mSubtitleUtil:Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;

    invoke-virtual {v1, v3}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleUtil;->setSMPTETTCustomMode(Z)V

    goto :goto_1

    .line 643
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->mActivation:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->access$102(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;Z)Z

    goto :goto_2
.end method
