.class Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;
.super Ljava/lang/Object;
.source "VideoDrmPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->createDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 82
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "createDialog() - select positive"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmPopupType:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 88
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$200(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->play(Z)V

    .line 89
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.update.title"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 93
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 94
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoFileInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/common/VideoFileInfo;->getCurPlayingPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getURLInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 96
    .local v1, "urlInfo":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 97
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.exit"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    .line 101
    :cond_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_OMA_FL_ONLY:Z

    if-eqz v2, :cond_2

    .line 103
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string v2, "videoplayer.show.error.popup"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 105
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "popup.type"

    const/16 v3, -0x3f2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 108
    .end local v0    # "i":Landroid/content/Intent;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDrmUtil:Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->launchBrowser(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.exit"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 120
    .end local v1    # "urlInfo":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$400(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 122
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;->access$300(Lcom/sec/android/app/videoplayer/popup/VideoDrmPopup;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "videoplayer.exit"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
