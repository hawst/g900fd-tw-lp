.class public interface abstract Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerPopup;
.super Ljava/lang/Object;
.source "IChangePlayerPopup.java"


# static fields
.field public static final ALLTOGETHER_RUNNING:I = 0x8

.field public static final ASF_DEVICE:I = 0xb

.field public static final DISMISS_POPUP:I = 0x190

.field public static final DISMISS_PROGRESS_ICON:I = 0x64

.field public static final GROUP_PLAY_RUNNING:I = 0x7

.field public static final HDMI:I = 0x3

.field public static final HOTSPOT:I = 0x1

.field public static final LIMITED_CONTENTS:I = 0x6

.field public static final MY_DEVICE:I = 0xa

.field public static final NORMAL:I = 0x0

.field public static final POWER_SAVING_MODE_ON:I = 0x5

.field public static final PROCEED_CHANGE_PLAYER_SHOW:I = 0x12c

.field public static final PROCEED_DIABLE_ALLTOGETHER:I = 0x1f4

.field public static final SIDE_SYNC_RUNNING:I = 0x4

.field public static final UPDATE_CHANGE_PLAYER_LIST:I = 0xc8

.field public static final WFD_DEVICE:I = 0xc

.field public static final WIFI_DIRECT:I = 0x2
