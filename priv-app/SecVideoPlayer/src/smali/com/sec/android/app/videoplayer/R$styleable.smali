.class public final Lcom/sec/android/app/videoplayer/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AnimatedToggleButton:[I

.field public static final AnimatedToggleButton_buttonAnimationTime:I = 0xb

.field public static final AnimatedToggleButton_circleRadius:I = 0x8

.field public static final AnimatedToggleButton_edgeAnimationTime:I = 0xa

.field public static final AnimatedToggleButton_edgeColor:I = 0x9

.field public static final AnimatedToggleButton_edgeWidth:I = 0x6

.field public static final AnimatedToggleButton_focusedImageA:I = 0x2

.field public static final AnimatedToggleButton_focusedImageB:I = 0x3

.field public static final AnimatedToggleButton_imageA:I = 0x0

.field public static final AnimatedToggleButton_imageB:I = 0x1

.field public static final AnimatedToggleButton_maskRadius:I = 0x7

.field public static final AnimatedToggleButton_pressedImageA:I = 0x4

.field public static final AnimatedToggleButton_pressedImageB:I = 0x5


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2930
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/videoplayer/R$styleable;->AnimatedToggleButton:[I

    return-void

    :array_0
    .array-data 4
        0x7f010000
        0x7f010001
        0x7f010002
        0x7f010003
        0x7f010004
        0x7f010005
        0x7f010006
        0x7f010007
        0x7f010008
        0x7f010009
        0x7f01000a
        0x7f01000b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2897
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
