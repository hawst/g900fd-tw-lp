.class Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->initTouchAndKeyListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isTouchDown(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$000(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->performPressWithCustomBitmap()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$200(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    .line 332
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 324
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isTouchDown(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$000(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->startDefaultPressedAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$300(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0

    .line 326
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isTouchUp(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$400(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$100(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->performReleaseWithCustomBitmap()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$500(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0

    .line 328
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->isTouchUp(Landroid/view/MotionEvent;)Z
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$400(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton$1;->this$0:Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;

    # invokes: Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->revertDefaultPressedAnimation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;->access$600(Lcom/sec/android/app/videoplayer/widget/AnimatedToggleButton;)V

    goto :goto_0
.end method
