.class Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;
.super Landroid/database/ContentObserver;
.source "VideoBrightnessPopup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->registerContentObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2
    .param p1, "selfChange"    # Z

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 261
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoValText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$7;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->brightnessValueLocale()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :cond_0
    return-void
.end method
