.class Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$2;
.super Ljava/lang/Object;
.source "VideoAdaptSoundPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->showPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup$2;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoAdaptSoundPopup;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.hearingadjust.launch"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v1, "VideoAdaptSoundPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Not found hearingadjust application :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
