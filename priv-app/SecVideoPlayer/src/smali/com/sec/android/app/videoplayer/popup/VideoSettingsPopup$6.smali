.class Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$6;
.super Ljava/lang/Object;
.source "VideoSettingsPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)V
    .locals 0

    .prologue
    .line 861
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    .line 863
    sparse-switch p2, :sswitch_data_0

    .line 891
    :cond_0
    :goto_0
    return v2

    .line 865
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v3

    const/16 v4, 0x20

    if-eq v3, v4, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 866
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 868
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getDownTime()J

    move-result-wide v6

    sub-long v0, v4, v6

    .line 870
    .local v0, "pressTime":J
    const-wide/16 v4, 0x1f4

    cmp-long v3, v0, v4

    if-gez v3, :cond_0

    .line 871
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup$6;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->access$000(Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-string v5, "videoplayer.set.lock"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto :goto_0

    .line 878
    .end local v0    # "pressTime":J
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_0

    .line 879
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    .line 888
    :sswitch_2
    const/4 v2, 0x0

    goto :goto_0

    .line 863
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x17 -> :sswitch_2
        0x1a -> :sswitch_0
        0x3e -> :sswitch_2
        0x42 -> :sswitch_2
        0x6f -> :sswitch_1
    .end sparse-switch
.end method
