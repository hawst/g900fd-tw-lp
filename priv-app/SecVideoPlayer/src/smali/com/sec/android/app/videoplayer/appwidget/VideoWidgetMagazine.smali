.class public Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;
.super Landroid/appwidget/AppWidgetProvider;
.source "VideoWidgetMagazine.java"


# static fields
.field private static final ACTION_SEC_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field private static final ACTION_SEC_WIDGET_UPDATE:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE"

.field private static final HANDLE_UPDATE_CLOUD_CONTENT:I = 0x7d0

.field private static final HANDLE_UPDATE_CONTENT:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "VideoWidgetMagazine"

.field private static mContentHandler:Landroid/os/Handler;

.field private static mContext:Landroid/content/Context;

.field private static mDBObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    .line 37
    sput-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    .line 348
    new-instance v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine$2;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContentHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContentHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static buildUpdate(Landroid/content/Context;)Landroid/widget/RemoteViews;
    .locals 24
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const-string v20, "VideoWidgetMagazine"

    const-string v21, "bulidUpdate E"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v18

    .line 114
    .local v18, "videoDB":Lcom/sec/android/app/videoplayer/db/VideoDB;
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->updateVersion()V

    .line 115
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v20

    const-string v21, "lastPlayedItem"

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadStringKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 116
    .local v10, "lastPath":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 117
    .local v17, "uri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 118
    .local v4, "CloudUtil":Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;
    const/4 v5, 0x0

    .line 120
    .local v5, "SKTCloudUtil":Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
    const/4 v9, 0x0

    .line 121
    .local v9, "isNewlyLaunched":Z
    if-nez v17, :cond_0

    .line 122
    sget-boolean v20, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v20, :cond_0

    .line 123
    sget-object v20, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v20

    const-string v21, "newlyLaunched"

    const/16 v22, 0x1

    invoke-virtual/range {v20 .. v22}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v9

    .line 124
    if-eqz v9, :cond_0

    .line 125
    sget-object v20, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v20

    sget-object v21, Lcom/sec/android/app/videoplayer/common/feature/Path;->DEFAULT_WIDGET_VIDEO:Ljava/lang/String;

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v17

    .line 129
    :cond_0
    const-string v20, "VideoWidgetMagazine"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "widget is newly launched : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/16 v19, 0x0

    .line 132
    .local v19, "views":Landroid/widget/RemoteViews;
    const/4 v8, 0x0

    .line 134
    .local v8, "intent":Landroid/content/Intent;
    if-nez v17, :cond_3

    .line 135
    new-instance v19, Landroid/widget/RemoteViews;

    .end local v19    # "views":Landroid/widget/RemoteViews;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v20

    const v21, 0x7f030045

    invoke-direct/range {v19 .. v21}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 136
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    new-instance v8, Landroid/content/Intent;

    .end local v8    # "intent":Landroid/content/Intent;
    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 138
    .restart local v8    # "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v20

    const-string v21, "com.samsung.everglades.video"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 139
    const-string v20, "com.samsung.everglades.video"

    const-string v21, "com.samsung.everglades.video.VideoMain"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 143
    :cond_1
    :goto_0
    const-string v20, "android.intent.action.MAIN"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v20, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    :goto_1
    const/16 v20, 0x0

    const/high16 v21, 0x8000000

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v8, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    .line 213
    .local v11, "pendingIntent":Landroid/app/PendingIntent;
    const v20, 0x7f0d01fb

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 215
    return-object v19

    .line 140
    .end local v11    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v20

    const-string v21, "com.sec.android.app.videolist"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, p0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isPackageAvailable(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 141
    const-string v20, "com.sec.android.app.videolist"

    const-string v21, "com.sec.android.app.videolist.activity.VPMainTab"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 146
    :cond_3
    const/4 v13, 0x0

    .line 147
    .local v13, "thumb":Landroid/graphics/Bitmap;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getTitleName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    .line 148
    .local v16, "title":Ljava/lang/String;
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getResumePosition(Landroid/net/Uri;)J

    move-result-wide v14

    .line 149
    .local v14, "remainTime":J
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDurationTime(Landroid/net/Uri;)J

    move-result-wide v6

    .line 151
    .local v6, "durationTime":J
    invoke-static/range {p0 .. p0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;

    move-result-object v4

    .line 152
    sget-boolean v20, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v20, :cond_4

    .line 153
    sget-object v20, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    move-result-object v5

    .line 155
    :cond_4
    if-eqz v4, :cond_7

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 156
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    .line 157
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 172
    :goto_2
    new-instance v19, Landroid/widget/RemoteViews;

    .end local v19    # "views":Landroid/widget/RemoteViews;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v20

    const v21, 0x7f030044

    invoke-direct/range {v19 .. v21}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 173
    .restart local v19    # "views":Landroid/widget/RemoteViews;
    if-eqz v13, :cond_5

    .line 174
    const v20, 0x7f0d01f4

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v1, v13}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 177
    :cond_5
    const/4 v12, 0x1

    .line 179
    .local v12, "remainPercent":I
    const-wide/16 v20, 0x0

    cmp-long v20, v6, v20

    if-lez v20, :cond_6

    const-wide/16 v20, 0x0

    cmp-long v20, v14, v20

    if-lez v20, :cond_6

    .line 180
    const-wide/16 v20, 0x64

    mul-long v20, v20, v14

    div-long v20, v20, v6

    move-wide/from16 v0, v20

    long-to-int v12, v0

    .line 181
    if-gtz v12, :cond_6

    .line 182
    const/4 v12, 0x1

    .line 186
    :cond_6
    const v20, 0x7f0d01f8

    move-object/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 187
    sget-boolean v20, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v20, :cond_a

    if-eqz v9, :cond_a

    .line 188
    const v20, 0x7f0d01f9

    const-wide/16 v22, 0x0

    invoke-static/range {v22 .. v23}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->stringForTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 189
    const v20, 0x7f0d01fa

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->stringForTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 190
    const v20, 0x7f0d0088

    const/16 v21, 0x64

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v19 .. v23}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    .line 206
    :goto_3
    new-instance v8, Landroid/content/Intent;

    .end local v8    # "intent":Landroid/content/Intent;
    const-class v20, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 207
    .restart local v8    # "intent":Landroid/content/Intent;
    const-string v20, "isFromWidget"

    const/16 v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    const-string v20, "filePath"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 209
    const-string v20, "uri"

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_1

    .line 159
    .end local v12    # "remainPercent":I
    :cond_7
    sget-boolean v20, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v20, :cond_8

    if-eqz v5, :cond_8

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 161
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getDisplayName(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v16

    .line 162
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileId(Landroid/net/Uri;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->requestThumbnail(J)Landroid/graphics/Bitmap;

    move-result-object v13

    goto/16 :goto_2

    .line 165
    :cond_8
    sget-boolean v20, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MODEL_TAB_S:Z

    if-eqz v20, :cond_9

    if-eqz v9, :cond_9

    .line 166
    sget-object v20, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    const-wide/32 v22, 0xf230

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v13

    goto/16 :goto_2

    .line 168
    :cond_9
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-static {v0, v14, v15, v1}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v13

    goto/16 :goto_2

    .line 192
    .restart local v12    # "remainPercent":I
    :cond_a
    const v20, 0x7f0d01f9

    invoke-static {v14, v15}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->stringForTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 193
    const v20, 0x7f0d01fa

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->stringForTime(J)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v19 .. v21}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 194
    const v20, 0x7f0d0088

    const/16 v21, 0x64

    const/16 v22, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v12, v3}, Landroid/widget/RemoteViews;->setProgressBar(IIIZ)V

    goto/16 :goto_3
.end method

.method private static getThumbnail(Landroid/content/Context;JLjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "remainTime"    # J
    .param p3, "path"    # Ljava/lang/String;

    .prologue
    .line 228
    const-string v4, "VideoWidgetMagazine"

    const-string v5, "getWidgetThumbnail"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/4 v3, 0x0

    .line 230
    .local v3, "thumb":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v2}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 233
    .local v2, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v2, p3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 234
    const/16 v4, 0x500

    const/16 v5, 0x2d0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/media/MediaMetadataRetriever;->setVideoSize(IIZZ)V

    .line 235
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p1

    invoke-virtual {v2, v4, v5}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 241
    :try_start_1
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 247
    :goto_0
    if-nez v3, :cond_0

    .line 248
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020280

    invoke-static {v4, v5}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 251
    :cond_0
    return-object v3

    .line 242
    :catch_0
    move-exception v1

    .line 243
    .local v1, "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoWidgetMagazine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getThumbnail() RuntimeException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 236
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    .line 237
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 238
    const-string v4, "VideoWidgetMagazine"

    const-string v5, "getThumbnail() - setDataSource failed. DRM or anything else?"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 241
    :try_start_3
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 242
    :catch_2
    move-exception v1

    .line 243
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    const-string v4, "VideoWidgetMagazine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getThumbnail() RuntimeException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 240
    .end local v0    # "e":Ljava/lang/RuntimeException;
    .end local v1    # "ex":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v4

    .line 241
    :try_start_4
    invoke-virtual {v2}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 244
    :goto_1
    throw v4

    .line 242
    :catch_3
    move-exception v1

    .line 243
    .restart local v1    # "ex":Ljava/lang/RuntimeException;
    const-string v5, "VideoWidgetMagazine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getThumbnail() RuntimeException : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static magazineWidgetRotation(Landroid/content/Context;III)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "widgetId"    # I
    .param p2, "spanX"    # I
    .param p3, "spanY"    # I

    .prologue
    .line 105
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 106
    .local v0, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    invoke-static {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->buildUpdate(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 107
    const/4 v1, 0x1

    return v1
.end method

.method private registerContentObserver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 255
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "registerContentObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    if-nez v1, :cond_1

    .line 257
    new-instance v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine$1;-><init>(Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;Landroid/os/Handler;)V

    sput-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    .line 275
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 276
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/videoplayer/db/VideoDB;->EXTERNAL_MEDIA_DB_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 277
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 279
    sget-boolean v1, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SKT_CLOUD:Z

    if-eqz v1, :cond_0

    .line 280
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 282
    :cond_0
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "registerContentObserver - mDBObserver is now registered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :goto_0
    return-void

    .line 284
    :cond_1
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "registerContentObserver - mDBObserver already exists"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static stringForTime(J)Ljava/lang/String;
    .locals 12
    .param p0, "timeMs"    # J

    .prologue
    const-wide/16 v10, 0x3c

    .line 219
    const-wide/16 v8, 0x3e8

    div-long v6, p0, v8

    .line 220
    .local v6, "totalSeconds":J
    rem-long v4, v6, v10

    .line 221
    .local v4, "seconds":J
    div-long v8, v6, v10

    rem-long v2, v8, v10

    .line 222
    .local v2, "minutes":J
    const-wide/16 v8, 0xe10

    div-long v0, v6, v8

    .line 224
    .local v0, "hours":J
    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method private static unregisterContentObserver()V
    .locals 3

    .prologue
    .line 289
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "unregisterContentObserver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    if-eqz v1, :cond_0

    .line 292
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 293
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 294
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mDBObserver:Landroid/database/ContentObserver;

    .line 295
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "unregisterContentObserver - mDBObserver is unregistered"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :goto_0
    return-void

    .line 297
    :cond_0
    const-string v1, "VideoWidgetMagazine"

    const-string v2, "unregisterContentObserver : registerContentObserver is NULL"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 94
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 100
    invoke-static {}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->unregisterContentObserver()V

    .line 101
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 83
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 76
    sput-object p1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    .line 77
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->registerContentObserver()V

    .line 78
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, -0x1

    .line 46
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "action":Ljava/lang/String;
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    .line 48
    .local v7, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    sput-object p1, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->mContext:Landroid/content/Context;

    .line 49
    const-string v8, "VideoWidgetMagazine"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive. : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const-string v8, "videowidget.update"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 52
    :cond_0
    new-instance v4, Landroid/content/ComponentName;

    const-class v8, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;

    invoke-direct {v4, p1, v8}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 53
    .local v4, "thisWidget":Landroid/content/ComponentName;
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->buildUpdate(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v8

    invoke-virtual {v7, v4, v8}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    .line 69
    .end local v4    # "thisWidget":Landroid/content/ComponentName;
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 70
    return-void

    .line 54
    :cond_2
    const-string v8, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    const-string v8, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 55
    :cond_3
    const-string v8, "widgetId"

    invoke-virtual {p2, v8, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 57
    .local v3, "targetWidgetId":I
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-class v9, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .local v1, "cn":Landroid/content/ComponentName;
    invoke-virtual {v7, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    .line 60
    .local v6, "widgetIds":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v8, v6

    if-ge v2, v8, :cond_1

    .line 61
    aget v8, v6, v2

    if-eq v8, v3, :cond_4

    if-ne v3, v11, :cond_5

    .line 62
    :cond_4
    const-string v8, "VideoWidgetMagazine"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onReceive. : UpdateViews : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " / "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget v10, v6, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-static {p1}, Lcom/sec/android/app/videoplayer/appwidget/VideoWidgetMagazine;->buildUpdate(Landroid/content/Context;)Landroid/widget/RemoteViews;

    move-result-object v5

    .line 64
    .local v5, "view":Landroid/widget/RemoteViews;
    aget v8, v6, v2

    invoke-virtual {v7, v8, v5}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 60
    .end local v5    # "view":Landroid/widget/RemoteViews;
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 87
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 88
    return-void
.end method
