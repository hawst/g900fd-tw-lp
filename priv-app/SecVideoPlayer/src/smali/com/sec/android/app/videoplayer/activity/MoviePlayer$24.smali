.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;
.super Landroid/os/Handler;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 3860
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v2, 0xd2

    const/16 v1, 0xd1

    .line 3862
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 3898
    :goto_0
    return-void

    .line 3865
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3867
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-nez v0, :cond_1

    .line 3868
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPresentationHandler. HIDE_PRESENTATION_VIEW"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3869
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->DisablePresentationMode()V

    .line 3870
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->dismissShowingPopup()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 3871
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    if-eqz v0, :cond_0

    .line 3872
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSubviewBtnControllerForTvOutView(Z)V

    .line 3873
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onResume()V

    .line 3875
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartMoviPlay4Presentation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 3877
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 3882
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3883
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsConnectedPresentation:Z

    if-nez v0, :cond_2

    .line 3884
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mPresentationHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 3886
    :cond_2
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mPresentationHandler. SHOW_PRESENTATION_VIEW"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3887
    sget-boolean v0, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_2ND_SCREEN:Z

    if-eqz v0, :cond_3

    .line 3888
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeSubviewBtnControllerForTvOutView(Z)V

    .line 3889
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onResume()V

    .line 3891
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$24;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->StartMoviPlay4Presentation()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 3862
    nop

    :pswitch_data_0
    .packed-switch 0xd1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
