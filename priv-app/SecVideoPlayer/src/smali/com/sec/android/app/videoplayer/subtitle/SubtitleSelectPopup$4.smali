.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->callSelectSubtitlePopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x0

    .line 267
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mInvaildSubtitle:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$700(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 268
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mSubFilesList:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$4;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;

    # setter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;->access$1002(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 277
    :cond_1
    return-void
.end method
