.class public abstract Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
.super Ljava/util/Observable;
.source "IWfdUtil.java"


# static fields
.field private static final BASE:I = 0x320

.field public static final DONGLE:I = 0x1

.field public static final NONE:I = 0x0

.field public static final VIDEO_WIFIDISPLAY_DISMISS_PROGRESS:I = 0x321

.field public static final VIDEO_WIFIDISPLAY_SHOW_PROGRESS:I = 0x320

.field public static bHasPresentaionFocus:Z


# instance fields
.field protected mAvailableDisplays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field protected mContext:Landroid/content/Context;

.field protected mDisplayManager:Landroid/hardware/display/DisplayManager;

.field protected mMsg:[I

.field protected mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->bHasPresentaionFocus:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    .line 34
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mContext:Landroid/content/Context;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mAvailableDisplays:Ljava/util/List;

    .line 38
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 40
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mMsg:[I

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    .locals 1

    .prologue
    .line 50
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public checkExceptionalCase()I
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->checkExceptionalCase()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract connect(Landroid/os/Parcelable;)V
.end method

.method public abstract forceTerminateWfd()V
.end method

.method public getAvailableDisplays(Z)Ljava/util/List;
    .locals 5
    .param p1, "bNew"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    if-eqz p1, :cond_1

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mAvailableDisplays:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 137
    sget-boolean v4, Lcom/sec/android/app/videoplayer/popup/changeplayer/IChangePlayerListener;->STARTED:Z

    if-eqz v4, :cond_1

    .line 138
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-eqz v4, :cond_1

    .line 139
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v4}, Landroid/hardware/display/WifiDisplayStatus;->getDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    .local v0, "arr$":[Landroid/hardware/display/WifiDisplay;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 140
    .local v1, "d":Landroid/hardware/display/WifiDisplay;
    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->isAvailable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mAvailableDisplays:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 147
    .end local v0    # "arr$":[Landroid/hardware/display/WifiDisplay;
    .end local v1    # "d":Landroid/hardware/display/WifiDisplay;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mAvailableDisplays:Ljava/util/List;

    return-object v4
.end method

.method public abstract getConnectedDisplayInfo()Ljava/lang/String;
.end method

.method public abstract getConnectedName()Ljava/lang/String;
.end method

.method public abstract getDeviceName(Landroid/os/Parcelable;)Ljava/lang/String;
.end method

.method public getP2pChannel()Landroid/net/wifi/p2p/WifiP2pManager$Channel;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getPrimaryDeviceType(Landroid/os/Parcelable;)Ljava/lang/String;
.end method

.method public getWifiManger()Landroid/net/wifi/p2p/WifiP2pManager;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract isWfdConnected()Z
.end method

.method public abstract isWfdConnectedByOtherApp()Z
.end method

.method public isWifiBusy(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p1, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 116
    const/4 v0, 0x0

    return v0
.end method

.method public isWifiDisplayDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p1, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method public abstract notifyChanged(II)V
.end method

.method public abstract prepare()V
.end method

.method public abstract refreshWifiDisplays()V
.end method

.method public abstract scanWifiDisplays()V
.end method

.method public abstract setActivityState(Z)V
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;->getInstance()Lcom/sec/android/app/videoplayer/util/wfd/WfdUtilNew;

    move-result-object v0

    .line 62
    .local v0, "instance":Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mContext:Landroid/content/Context;

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mContext:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 65
    return-object v0
.end method

.method public setWifiDisplayStatus(Landroid/hardware/display/WifiDisplayStatus;)V
    .locals 0
    .param p1, "wfdStatus"    # Landroid/hardware/display/WifiDisplayStatus;

    .prologue
    .line 125
    return-void
.end method

.method public abstract stopScanWifiDisplays()V
.end method

.method public abstract terminateWfd()V
.end method
