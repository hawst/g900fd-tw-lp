.class public Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
.super Ljava/lang/Object;
.source "VideoSKTCloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;,
        Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;,
        Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    }
.end annotation


# static fields
.field public static final ACTION_SETUP_DROPBOX_ACCOUNT:I = 0x1194

.field private static final FOLDER_COLUMNS:[Ljava/lang/String;

.field private static final FOLDER_SORT_ORDER:Ljava/lang/String; = "bucket_display_name COLLATE LOCALIZED ASC"

.field public static final IN_CLOUD_SERVER:Ljava/lang/String; = "tcloud_server_id"

.field private static final LIST_SORT_ORDER:Ljava/lang/String; = "_display_name COLLATE LOCALIZED ASC"

.field private static final RECENTLY_PLAYED_COLUMN:Ljava/lang/String; = "isPlayed"

.field private static final TAG:Ljava/lang/String; = "VideoSKTCloudUtil"

.field public static final VIDEO_URI:Landroid/net/Uri;

.field private static mCloudAgentExistence:Z

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

.field private mResolver:Landroid/content/ContentResolver;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/skp/tcloud/service/lib/TcloudStore$Videos;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    .line 41
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoListType;->getSKTCloudFolderColumns()[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->FOLDER_COLUMNS:[Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    .line 50
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 51
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 52
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    .line 57
    if-eqz p1, :cond_0

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    .line 59
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    .line 60
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 61
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->checkCloudAgentExistence()V

    .line 63
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    return-object v0
.end method

.method private checkCloudAgentExistence()V
    .locals 5

    .prologue
    .line 555
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_1

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 561
    const/4 v1, 0x0

    .line 563
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.skp.tcloud.agent"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 569
    :goto_1
    if-nez v1, :cond_2

    .line 570
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    goto :goto_0

    .line 565
    :catch_0
    move-exception v0

    .line 566
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 572
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_2
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    goto :goto_0
.end method

.method private getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 681
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 682
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 683
    const/4 p2, 0x0

    .line 686
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudActivated()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 687
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const-string v2, "list_view_by_cloud"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v0

    .line 689
    .local v0, "viewBy":I
    packed-switch v0, :pswitch_data_0

    .line 707
    .end local v0    # "viewBy":I
    :cond_1
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 p1, 0x0

    .line 711
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    return-object p1

    .line 692
    .restart local v0    # "viewBy":I
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    if-eqz p2, :cond_1

    .line 693
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 694
    const/4 p2, 0x0

    goto :goto_0

    .line 698
    :pswitch_1
    if-eqz p1, :cond_1

    .line 699
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 700
    const/4 p1, 0x0

    goto :goto_0

    .line 708
    .end local v0    # "viewBy":I
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 709
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object p1, p2

    goto :goto_1

    .line 711
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_1

    .line 689
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    if-nez v0, :cond_0

    .line 67
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    .line 70
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    return-object v0
.end method

.method private getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "searchKey"    # Ljava/lang/String;

    .prologue
    .line 154
    if-nez p1, :cond_0

    .line 155
    const-string v0, "_"

    .line 161
    :goto_0
    return-object v0

    .line 157
    :cond_0
    const-string v0, "!"

    const-string v1, "!!"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 158
    const-string v0, "\'"

    const-string v1, "\'\'"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 159
    const-string v0, "_"

    const-string v1, "!_"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 160
    const-string v0, "%"

    const-string v1, "!%"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 161
    goto :goto_0
.end method

.method private getSortOrderString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 821
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "sortorder"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 822
    .local v1, "sortOrder":I
    const-string v2, "VideoSKTCloudUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSortOrderString() order:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    packed-switch v1, :pswitch_data_0

    .line 845
    const-string v2, "_display_name COLLATE LOCALIZED ASC"

    :goto_0
    return-object v2

    .line 826
    :pswitch_0
    const-string v2, "_display_name COLLATE LOCALIZED ASC"

    goto :goto_0

    .line 829
    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 830
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, "(date_added * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 831
    const-string v2, "(date_modified * 1) DESC, "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 832
    const-string v2, "(datetaken * 1) DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 833
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 836
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :pswitch_2
    const-string v2, "(_size * 1) DESC"

    goto :goto_0

    .line 839
    :pswitch_3
    const-string v2, "mime_type ASC"

    goto :goto_0

    .line 842
    :pswitch_4
    const-string v2, "isPlayed DESC"

    goto :goto_0

    .line 824
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 728
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 720
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDataOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 740
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 747
    :cond_0
    :goto_0
    return v1

    .line 743
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 744
    .local v0, "tpManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 745
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 724
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWifiOn()Z
    .locals 3

    .prologue
    .line 732
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 733
    const/4 v1, 0x0

    .line 736
    :goto_0
    return v1

    .line 735
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 736
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method private mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "cloudCursor"    # Landroid/database/Cursor;

    .prologue
    .line 715
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/database/Cursor;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 716
    .local v0, "cursorArray":[Landroid/database/Cursor;
    new-instance v1, Landroid/database/MergeCursor;

    invoke-direct {v1, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v1
.end method

.method private syncWithTcloud()V
    .locals 2

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;-><init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$SyncWithTcloud;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method


# virtual methods
.method public checkInitializedState()V
    .locals 1

    .prologue
    .line 548
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 549
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->initTcloud()V

    .line 550
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->syncWithTcloud()V

    .line 552
    :cond_0
    return-void
.end method

.method public deleteContent(J)Z
    .locals 7
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 538
    sget-boolean v3, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v3, :cond_2

    :cond_0
    move v1, v2

    .line 544
    :cond_1
    :goto_0
    return v1

    .line 541
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "_id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 542
    .local v0, "deletedContentCount":I
    if-eq v0, v1, :cond_1

    move v1, v2

    .line 544
    goto :goto_0
.end method

.method public downloadCloudFile(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 615
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 618
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    .line 620
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$1;-><init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Landroid/net/Uri;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 630
    .local v0, "loadingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public downloadCloudFile(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 635
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object v1, p1

    .line 637
    .local v1, "mUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 656
    :cond_0
    :goto_0
    return-void

    .line 640
    :cond_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 641
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$2;-><init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Ljava/util/ArrayList;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 654
    .local v0, "loadingThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public getCloudCachedPath(Landroid/net/Uri;)Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v10, 0x0

    .line 504
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_1

    move-object v9, v10

    .line 534
    :cond_0
    :goto_0
    return-object v9

    .line 507
    :cond_1
    if-nez p1, :cond_2

    move-object v9, v10

    .line 508
    goto :goto_0

    .line 510
    :cond_2
    const/4 v7, 0x0

    .line 511
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 512
    .local v6, "cached":Ljava/lang/String;
    const/4 v9, 0x0

    .line 513
    .local v9, "localPath":Ljava/lang/String;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "tcloud_is_cached"

    aput-object v0, v2, v1

    const-string v0, "tcloud_cached_path"

    aput-object v0, v2, v3

    .line 516
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 517
    if-eqz v7, :cond_3

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 518
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 519
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 520
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 525
    :cond_3
    if-eqz v7, :cond_4

    .line 526
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 527
    const/4 v7, 0x0

    .line 531
    :cond_4
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_5
    move-object v9, v10

    .line 534
    goto :goto_0

    .line 522
    :catch_0
    move-exception v8

    .line 523
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 525
    if-eqz v7, :cond_4

    .line 526
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 527
    const/4 v7, 0x0

    goto :goto_1

    .line 525
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_6

    .line 526
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 527
    const/4 v7, 0x0

    :cond_6
    throw v0
.end method

.method public getCloudTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 399
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 400
    .local v0, "index":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 401
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 402
    :cond_0
    return-object p1
.end method

.method public getCursorByBucketId(I)Landroid/database/Cursor;
    .locals 7
    .param p1, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 186
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    :cond_0
    move-object v6, v2

    .line 192
    :goto_0
    return-object v6

    .line 189
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 190
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 192
    .local v6, "cloudCursor":Landroid/database/Cursor;
    goto :goto_0
.end method

.method public getCursorCached(Z)Landroid/database/Cursor;
    .locals 7
    .param p1, "isCached"    # Z

    .prologue
    const/4 v2, 0x0

    .line 110
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    :cond_0
    move-object v6, v2

    .line 120
    :goto_0
    return-object v6

    .line 113
    :cond_1
    const/4 v3, 0x0

    .line 114
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 115
    const-string v3, "tcloud_is_cached = 1"

    .line 119
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 120
    .local v6, "cloudCursor":Landroid/database/Cursor;
    goto :goto_0

    .line 117
    .end local v6    # "cloudCursor":Landroid/database/Cursor;
    :cond_2
    const-string v3, "tcloud_is_cached = 0"

    goto :goto_1
.end method

.method public getCursorCachedByBucketId(ZI)Landroid/database/Cursor;
    .locals 7
    .param p1, "isCached"    # Z
    .param p2, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 124
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    :cond_0
    move-object v6, v2

    .line 135
    :goto_0
    return-object v6

    .line 127
    :cond_1
    const/4 v3, 0x0

    .line 128
    .local v3, "where":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tcloud_is_cached = 1 and bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 133
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 135
    .local v6, "cloudCursor":Landroid/database/Cursor;
    goto :goto_0

    .line 131
    .end local v6    # "cloudCursor":Landroid/database/Cursor;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tcloud_is_cached = 0 and bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public getFolderMergedCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 219
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 223
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 222
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->FOLDER_COLUMNS:[Ljava/lang/String;

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 223
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursor(Landroid/database/Cursor;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "searchKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 85
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 92
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 88
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_display_name like \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getQuerySafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v3, p2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 92
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursorByBucketId(Landroid/database/Cursor;I)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 165
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 171
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 168
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 169
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v5

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 171
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursorByBucketId(Landroid/database/Cursor;ILjava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "bucketID"    # I
    .param p3, "limitCount"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 175
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 182
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 178
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "where":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getSortOrderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 180
    .local v5, "orderBy":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 182
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getMergedCursorCached(Landroid/database/Cursor;Z)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "isCached"    # Z

    .prologue
    const/4 v2, 0x0

    .line 96
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 106
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 99
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/4 v3, 0x0

    .line 100
    .local v3, "where":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 101
    const-string v3, "tcloud_is_cached = 1"

    .line 105
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 106
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0

    .line 103
    .end local v6    # "cloudCursor":Landroid/database/Cursor;
    :cond_2
    const-string v3, "tcloud_is_cached = 0"

    goto :goto_1
.end method

.method public getMergedCursorCachedByBucketId(Landroid/database/Cursor;ZI)Landroid/database/Cursor;
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "isCached"    # Z
    .param p3, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    .line 139
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return-object v2

    .line 142
    :cond_1
    const/4 v3, 0x0

    .line 143
    .local v3, "where":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tcloud_is_cached = 1 and bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 148
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 150
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getFilteredCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v2

    goto :goto_0

    .line 146
    .end local v6    # "cloudCursor":Landroid/database/Cursor;
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tcloud_is_cached = 0 and bucket_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public getMergedCursorFolderByBucketId(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 196
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_1

    .line 215
    .end local p1    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 199
    .restart local p1    # "cursor":Landroid/database/Cursor;
    :cond_1
    const/4 v6, 0x0

    .line 200
    .local v6, "cloudCursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->FOLDER_COLUMNS:[Ljava/lang/String;

    const-string v5, "bucket_display_name COLLATE LOCALIZED ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 201
    .local v7, "tmpCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_3

    .line 202
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 203
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 204
    const-string v0, "bucket_id"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string v1, "limit 1"

    invoke-virtual {p0, v6, v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getMergedCursorByBucketId(Landroid/database/Cursor;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 205
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 207
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 208
    const/4 v7, 0x0

    .line 211
    :cond_3
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->hasNoCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object p1, v3

    goto :goto_0

    .line 212
    :cond_4
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isLocalOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 213
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudOnly(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_5

    move-object p1, v6

    goto :goto_0

    .line 215
    :cond_5
    invoke-direct {p0, p1, v6}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mergeCursor(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    goto :goto_0
.end method

.method public getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 416
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-object v0

    .line 419
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 422
    sget-object v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    sget-object v3, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v2, p1, v3}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getUrlByUri(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 406
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 411
    const-string v0, "VideoSKTCloudUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUrlByFilePath(uri)nia.noh : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    new-instance v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;-><init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public hasCloudContents(Landroid/database/Cursor;)Z
    .locals 10
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 283
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v8

    .line 302
    :goto_0
    return v0

    .line 286
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    move v0, v8

    .line 287
    goto :goto_0

    .line 289
    :cond_1
    if-nez p1, :cond_2

    move v0, v8

    .line 290
    goto :goto_0

    .line 292
    :cond_2
    const-string v3, "tcloud_server_id not NULL"

    .line 293
    .local v3, "where":Ljava/lang/String;
    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    .line 294
    .local v2, "column":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const-string v5, "_display_name COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 295
    .local v6, "cloudCursor":Landroid/database/Cursor;
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v7

    .line 297
    .local v7, "cursorCount":I
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 298
    const/4 v6, 0x0

    .line 300
    if-lt v7, v9, :cond_3

    move v0, v9

    .line 301
    goto :goto_0

    :cond_3
    move v0, v8

    .line 302
    goto :goto_0
.end method

.method public initTcloud()V
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 590
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->init(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public isCachedContent(J)Z
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 492
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 500
    :cond_0
    :goto_0
    return v1

    .line 495
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 498
    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 500
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCachedContent(Landroid/net/Uri;)Z
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 448
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v9

    .line 476
    :goto_0
    return v0

    .line 451
    :cond_0
    if-nez p1, :cond_1

    move v0, v9

    .line 452
    goto :goto_0

    .line 454
    :cond_1
    const/4 v7, 0x0

    .line 455
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v6, 0x0

    .line 456
    .local v6, "cached":Ljava/lang/String;
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "tcloud_is_cached"

    aput-object v0, v2, v9

    .line 459
    .local v2, "col":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 461
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 462
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 463
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 467
    :cond_2
    if-eqz v7, :cond_3

    .line 468
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 469
    const/4 v7, 0x0

    .line 473
    :cond_3
    :goto_1
    if-eqz v6, :cond_5

    const-string v0, "1"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v10

    .line 474
    goto :goto_0

    .line 464
    :catch_0
    move-exception v8

    .line 465
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 467
    if-eqz v7, :cond_3

    .line 468
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 469
    const/4 v7, 0x0

    goto :goto_1

    .line 467
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 468
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 469
    const/4 v7, 0x0

    :cond_4
    throw v0

    :cond_5
    move v0, v9

    .line 476
    goto :goto_0
.end method

.method public isCachedContent(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 480
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v2, :cond_1

    .line 488
    :cond_0
    :goto_0
    return v1

    .line 483
    :cond_1
    if-eqz p1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getUriByPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 488
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCloudAccountCheck(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 427
    :try_start_0
    invoke-static {p1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->isCloudAvailable(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 429
    :goto_0
    return v1

    .line 428
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCloudActivated()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 74
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return v0

    .line 77
    :cond_1
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v1, :cond_0

    .line 78
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudContent(J)Z
    .locals 3
    .param p1, "videoId"    # J

    .prologue
    .line 368
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_0

    .line 369
    const/4 v1, 0x0

    .line 372
    :goto_0
    return v1

    .line 371
    :cond_0
    invoke-static {p1, p2}, Lcom/skp/tcloud/service/lib/TcloudStore$Videos;->getVideoUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 372
    .local v0, "uri":Landroid/net/Uri;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCloudContent(Landroid/database/Cursor;)Z
    .locals 5
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v3, 0x0

    .line 306
    sget-boolean v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v4, :cond_1

    .line 330
    :cond_0
    :goto_0
    return v3

    .line 309
    :cond_1
    if-eqz p1, :cond_0

    .line 312
    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_0

    .line 315
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 318
    :cond_2
    const-string v4, "tcloud_server_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 319
    .local v2, "inServerIndex":I
    const/4 v1, 0x0

    .line 321
    .local v1, "inCloud":Ljava/lang/String;
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 322
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 327
    :cond_3
    :goto_1
    if-eqz v1, :cond_0

    .line 328
    const/4 v3, 0x1

    goto :goto_0

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Landroid/database/CursorIndexOutOfBoundsException;
    invoke-virtual {v0}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public isCloudContent(Landroid/net/Uri;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 351
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 364
    :cond_0
    :goto_0
    return v0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 357
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    const-string v1, "VideoSKTCloudUtil"

    const-string v2, "isCloudContent(uri)"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2f

    if-ne v0, v1, :cond_2

    .line 362
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 364
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public isCloudContent(Ljava/lang/String;)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 334
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 346
    :cond_0
    :goto_0
    return v0

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 340
    const-string v1, "VideoSKTCloudUtil"

    const-string v2, "isCloudContent(string)"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-wide/16 v2, -0x1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    sget-object v4, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    invoke-virtual {v1, p1, v4}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFileIdByPath(Ljava/lang/String;Landroid/net/Uri;)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 343
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isCloudFolder(I)Z
    .locals 8
    .param p1, "bucketID"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 376
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v0, :cond_0

    move v0, v7

    .line 395
    :goto_0
    return v0

    .line 379
    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    move v0, v7

    .line 380
    goto :goto_0

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_2

    move v0, v7

    .line 383
    goto :goto_0

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->VIDEO_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bucket_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 386
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    .line 387
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 388
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 389
    const/4 v6, 0x0

    .line 390
    const/4 v0, 0x1

    goto :goto_0

    .line 392
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 393
    const/4 v6, 0x0

    :cond_4
    move v0, v7

    .line 395
    goto :goto_0
.end method

.method public isContentReady(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 441
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 444
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isDataNetworkConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDataNetworkConnected()Z
    .locals 1

    .prologue
    .line 265
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isWifiOn()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isDataOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    :cond_0
    const/4 v0, 0x1

    .line 268
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isServerOnlyContent(Landroid/net/Uri;)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 434
    sget-boolean v1, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-nez v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCloudContent(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->isCachedContent(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public makeAvailableOffline(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 593
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 599
    :cond_0
    :goto_0
    return-void

    .line 596
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public makeAvailableOffline(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 602
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 612
    :cond_0
    return-void

    .line 605
    :cond_1
    const/4 v1, 0x0

    .line 606
    .local v1, "uri":Landroid/net/Uri;
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 607
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 608
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "uri":Landroid/net/Uri;
    check-cast v1, Landroid/net/Uri;

    .line 609
    .restart local v1    # "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    .line 607
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public openWifiConnecting()V
    .locals 3

    .prologue
    .line 272
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 280
    :goto_0
    return-void

    .line 275
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 276
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const/high16 v1, 0x30800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 278
    const-string v1, "wifiSettingsAppear"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    const/high16 v2, 0x20000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public requestThumbnail(J)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "videoId"    # J

    .prologue
    const/4 v1, 0x0

    .line 227
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object v1

    .line 230
    :cond_1
    const-string v2, "VideoSKTCloudUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestThumbnail() : videoId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-wide/16 v2, -0x1

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    .line 233
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Lcom/skp/tcloud/service/lib/TcloudStore$Videos;->getVideoUri(J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/skp/tcloud/service/lib/TcloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Lcom/skp/tcloud/service/lib/TcloudException;
    invoke-virtual {v0}, Lcom/skp/tcloud/service/lib/TcloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public requestThumbnail(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 244
    sget-boolean v3, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v3, :cond_1

    .line 261
    :cond_0
    :goto_0
    return-object v2

    .line 247
    :cond_1
    if-eqz p1, :cond_0

    .line 250
    const-string v3, "VideoSKTCloudUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "requestThumbnail() : filePath:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    sget-boolean v3, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v3, :cond_0

    .line 252
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->getUriByFilepath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 254
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Lcom/skp/tcloud/service/lib/TcloudException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Lcom/skp/tcloud/service/lib/TcloudException;
    invoke-virtual {v0}, Lcom/skp/tcloud/service/lib/TcloudException;->printStackTrace()V

    goto :goto_0
.end method

.method public revertAvailableOffline(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 672
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 675
    :cond_1
    sget-boolean v0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public revertAvailableOffline(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 659
    .local p1, "selectedListUri":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    .line 669
    :cond_0
    return-void

    .line 662
    :cond_1
    const/4 v1, 0x0

    .line 663
    .local v1, "uri":Landroid/net/Uri;
    sget-boolean v2, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z

    if-eqz v2, :cond_0

    .line 664
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 665
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "uri":Landroid/net/Uri;
    check-cast v1, Landroid/net/Uri;

    .line 666
    .restart local v1    # "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V

    .line 664
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setOnUrlUpdatedSKTcloudListener(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    .prologue
    .line 756
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    .line 757
    return-void
.end method
