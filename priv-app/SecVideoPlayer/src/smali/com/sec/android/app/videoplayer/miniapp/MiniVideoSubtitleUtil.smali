.class public Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;
.super Ljava/lang/Object;
.source "MiniVideoSubtitleUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;
    }
.end annotation


# static fields
.field private static final CLEAR_SUBTITLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MiniVideoSubtitleUtil"

.field private static final baseSubtitleDir:Ljava/lang/String; = "/storage/sdcard0/.SubTitle/"

.field private static final baseSubtitleFile:Ljava/lang/String; = "SubTitleFile"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCountSubtitleOn:I

.field private final mHandler:Landroid/os/Handler;

.field public mHasSubtitleFile:Z

.field private mIsMultiSubtitle:Z

.field private mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

.field private mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

.field private mRemoteSubtitleFileExist:Z

.field public mSelectedSubtitleIndex:[I

.field public mSelectedTrackIndex:[I

.field public mSubtitleActivation:Z

.field private mSubtitleFile:Ljava/lang/String;

.field private mSubtitleFileType:I

.field public mSubtitleLanguageIdx:I

.field private mSubtitlePath:Ljava/lang/String;

.field private mSubtitleSyncTime:I

.field private mSubtitleUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 37
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 39
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 41
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    .line 43
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 45
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mIsMultiSubtitle:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 49
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 51
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    .line 53
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    .line 57
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mCountSubtitleOn:I

    .line 59
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedTrackIndex:[I

    .line 63
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    .line 67
    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleSyncTime:I

    .line 294
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$1;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHandler:Landroid/os/Handler;

    .line 70
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "MiniVideoSubtitleUtil E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mContext:Landroid/content/Context;

    .line 72
    return-void
.end method

.method private checkSubtitleFileType(Ljava/lang/String;)I
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 172
    if-nez p1, :cond_0

    .line 173
    const-string v1, "MiniVideoSubtitleUtil"

    const-string v2, "checkSubtitleFileType() - path is null"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/16 v0, 0x64

    .line 192
    :goto_0
    return v0

    .line 177
    :cond_0
    const/16 v0, 0x64

    .line 179
    .local v0, "filetype":I
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "smi"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    const/16 v0, 0x65

    .line 191
    :cond_1
    :goto_1
    const-string v1, "MiniVideoSubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkSubtitleFileType. subtitle file type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "srt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 182
    const/16 v0, 0x66

    goto :goto_1

    .line 183
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 184
    const/16 v0, 0x67

    goto :goto_1

    .line 185
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "smptett"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 186
    const/16 v0, 0x68

    goto :goto_1

    .line 187
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, "vtt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    const/16 v0, 0x69

    goto :goto_1
.end method

.method private existFile(Ljava/lang/String;)Z
    .locals 2
    .param p1, "subTitleFileName"    # Ljava/lang/String;

    .prologue
    .line 290
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private removeHandlerMsg(I)V
    .locals 1
    .param p1, "msg"    # I

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 310
    :cond_0
    return-void
.end method


# virtual methods
.method public DeleteRemoteSubTitleFile()V
    .locals 7

    .prologue
    .line 401
    new-instance v3, Ljava/io/File;

    const-string v6, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 403
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 404
    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 406
    .local v2, "childFileList":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 407
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 408
    .local v1, "childFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 407
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 412
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "childFile":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 414
    .end local v2    # "childFileList":[Ljava/io/File;
    :cond_1
    return-void
.end method

.method public DownloadRemoteSubTitleFile(Ljava/lang/String;)V
    .locals 4
    .param p1, "subtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 440
    const-string v1, "MiniVideoSubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DownloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 444
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 445
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 446
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 449
    :cond_0
    const-string v1, ".smi"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 450
    const-string v1, "/storage/sdcard0/.SubTitle/SubTitleFile.smi"

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 457
    :cond_1
    :goto_0
    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    .line 458
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 460
    return-void

    .line 451
    :cond_2
    const-string v1, ".srt"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 452
    const-string v1, "/storage/sdcard0/.SubTitle/SubTitleFile.srt"

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    goto :goto_0

    .line 453
    :cond_3
    const-string v1, ".sub"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 454
    const-string v1, "/storage/sdcard0/.SubTitle/SubTitleFile.sub"

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    goto :goto_0
.end method

.method public DownloadRemoteSubTitleFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "subtitleUrl"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 463
    const-string v1, "MiniVideoSubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DownloadRemoteSubTitleFile subtitleUrl: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 467
    new-instance v0, Ljava/io/File;

    const-string v1, "/storage/sdcard0/.SubTitle/"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 468
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 469
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 472
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/storage/sdcard0/.SubTitle/SubTitleFile"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    .line 474
    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    .line 475
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;->start()V

    .line 477
    return-void
.end method

.method public GetRemoteSubtitlePath()Ljava/lang/String;
    .locals 3

    .prologue
    .line 427
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetRemoteSubtitlePath: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitlePath:Ljava/lang/String;

    return-object v0
.end method

.method public GetRemoteSubtitleUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 422
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    return-object v0
.end method

.method public RemoteSubTitledownloadThreadCheck()Z
    .locals 3

    .prologue
    .line 481
    :try_start_0
    const-string v1, "MiniVideoSubtitleUtil"

    const-string v2, "RemoteSubTitledownloadThreadCheck "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubTitledownloadThread:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil$RemoteSubTitledownloadThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 487
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 483
    :catch_0
    move-exception v0

    .line 484
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 485
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public SetRemoteSubtitleUrl(Ljava/lang/String;)V
    .locals 3
    .param p1, "SubtitleUrl"    # Ljava/lang/String;

    .prologue
    .line 417
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SetRemoteSubtitleUrl: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleUrl:Ljava/lang/String;

    .line 419
    return-void
.end method

.method public checkExistSubtitle(Ljava/lang/String;)Z
    .locals 11
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    .line 239
    if-nez p1, :cond_0

    .line 240
    const-string v7, "MiniVideoSubtitleUtil"

    const-string v8, "checkExistSubtitle() - path is null"

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    :goto_0
    return v6

    .line 244
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    .line 245
    .local v4, "subtitleFileName":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 246
    .local v1, "smiFileName":Ljava/lang/String;
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    .line 247
    .local v2, "srtFileName":Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 248
    .local v3, "subFileName":Ljava/lang/String;
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    .line 250
    .local v5, "vttFileName":Ljava/lang/String;
    const/16 v7, 0x2e

    invoke-virtual {p1, v7}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 252
    .local v0, "pos":I
    if-lez v0, :cond_1

    .line 253
    invoke-virtual {p1, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 256
    :cond_1
    const-string v7, "MiniVideoSubtitleUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkExistSubtitle() - subtitleFileName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v7, ".smi"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 259
    const-string v7, ".srt"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 260
    const-string v7, ".sub"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 261
    const-string v7, ".vtt"

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 263
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 264
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 265
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 266
    const/16 v6, 0x65

    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    .line 285
    :goto_1
    const-string v6, "MiniVideoSubtitleUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateSubtitle. mHasSubtitleFile : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", mSubtitleFileType : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/sec/android/app/videoplayer/subtitle/SubtitleConst;->SUBTITLE_FILE_TYPE:[Ljava/lang/String;

    iget v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    add-int/lit8 v9, v9, -0x64

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    iget-boolean v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    goto/16 :goto_0

    .line 267
    :cond_2
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 268
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 269
    iput-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 270
    const/16 v6, 0x66

    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    goto :goto_1

    .line 271
    :cond_3
    invoke-direct {p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 272
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 273
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 274
    const/16 v6, 0x67

    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    goto :goto_1

    .line 275
    :cond_4
    invoke-direct {p0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->existFile(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 276
    iput-boolean v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 277
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 278
    const/16 v6, 0x69

    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    goto :goto_1

    .line 280
    :cond_5
    iput-boolean v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 281
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 282
    const/16 v6, 0x64

    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    goto :goto_1
.end method

.method public checkRemoteSubtitleFile(Z)V
    .locals 0
    .param p1, "check"    # Z

    .prologue
    .line 432
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubtitleFileExist:Z

    .line 433
    return-void
.end method

.method public clearSubtitle()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 157
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "mini clearSubtitle E : "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->removeHandlerMsg(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 160
    return-void
.end method

.method public getHasSubtitleFile()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    return v0
.end method

.method public getIsMultiSubtitle()Z
    .locals 1

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mIsMultiSubtitle:Z

    return v0
.end method

.method public getMultiSelectSubtitleCount()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mCountSubtitleOn:I

    return v0
.end method

.method public getSelectedSubtitleIndex()[I
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    return-object v0
.end method

.method public getSubtitleActive()Z
    .locals 4

    .prologue
    .line 119
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleActive() : mSubtitleActivation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mSubtitleFileType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/android/app/videoplayer/subtitle/SubtitleConst;->SUBTITLE_FILE_TYPE:[Ljava/lang/String;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    add-int/lit8 v3, v3, -0x64

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleCaptioningMgr;->isCaptionEnable()Z

    move-result v0

    .line 124
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    goto :goto_0
.end method

.method public getSubtitleFile()Ljava/lang/String;
    .locals 3

    .prologue
    .line 134
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleFile() : mSubtitleFile  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    return-object v0
.end method

.method public getSubtitleFileTyp()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    return v0
.end method

.method public getSubtitleLanguageIndex()I
    .locals 3

    .prologue
    .line 129
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSubtitleLanguageIndex() : mSubtitleLanguageIdx  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    return v0
.end method

.method public getSubtitleSyncTime()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleSyncTime:I

    return v0
.end method

.method public initRemoteSubtitle(Landroid/net/Uri;ILjava/lang/String;Ljava/lang/String;)V
    .locals 13
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "KeyType"    # I
    .param p3, "CaptionURL"    # Ljava/lang/String;
    .param p4, "CaptionType"    # Ljava/lang/String;

    .prologue
    .line 322
    const/16 v1, 0x1f

    .line 323
    .local v1, "SCHEME_PCLOUD_LAUNCH_TYPE":I
    const/16 v2, 0x21

    .line 324
    .local v2, "SCHME_ALLSHARE_VIDEO_LIST":I
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 325
    .local v0, "CurPlayingPath":Ljava/lang/String;
    const-string v10, "MiniVideoSubtitleUtil"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "initSubtitle uri="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " mKeyType="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    const-string v10, "sshttp://"

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/16 v10, 0x21

    if-ne p2, v10, :cond_1

    .line 328
    const-string v6, ""

    .line 329
    .local v6, "pathEx":Ljava/lang/String;
    const/4 v7, 0x0

    .line 330
    .local v7, "subtitleUrl":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/allshare/extension/SECVideoCaption;

    invoke-direct {v3}, Lcom/samsung/android/allshare/extension/SECVideoCaption;-><init>()V

    .line 332
    .local v3, "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    const-string v10, "sshttp://"

    invoke-virtual {v0, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 333
    const-string v10, "ss"

    const-string v11, ""

    invoke-virtual {v0, v10, v11}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 336
    :cond_0
    invoke-virtual {v3, v6}, Lcom/samsung/android/allshare/extension/SECVideoCaption;->getSubTitleURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 338
    if-eqz v7, :cond_5

    .line 339
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 340
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->SetRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->DownloadRemoteSubTitleFile(Ljava/lang/String;)V

    .line 342
    const-string v10, "MiniVideoSubtitleUtil"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Subtitle is exist : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    .end local v3    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .end local v6    # "pathEx":Ljava/lang/String;
    .end local v7    # "subtitleUrl":Ljava/lang/String;
    :cond_1
    :goto_0
    const/16 v10, 0x1f

    if-ne p2, v10, :cond_2

    if-eqz p3, :cond_2

    if-eqz p4, :cond_2

    .line 351
    const/4 v7, 0x0

    .line 352
    .restart local v7    # "subtitleUrl":Ljava/lang/String;
    const/4 v8, 0x0

    .line 353
    .local v8, "subtitletype":Ljava/lang/String;
    const/4 v9, 0x0

    .line 355
    .local v9, "uri2string":Ljava/lang/String;
    move-object/from16 v9, p3

    .line 356
    move-object/from16 v8, p4

    .line 358
    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 360
    if-eqz v7, :cond_7

    .line 361
    const-string v10, "http://"

    invoke-virtual {v7, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 362
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 363
    invoke-virtual {p0, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->SetRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 364
    invoke-virtual {p0, v7, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->DownloadRemoteSubTitleFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v10, "MiniVideoSubtitleUtil"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Subtitle is exist : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    .end local v7    # "subtitleUrl":Ljava/lang/String;
    .end local v8    # "subtitletype":Ljava/lang/String;
    .end local v9    # "uri2string":Ljava/lang/String;
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->isRemoteSubtitleFile()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 379
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->resetSubtitleSettings()V

    .line 381
    const/4 v5, 0x0

    .line 382
    .local v5, "mRemoteSubtitleUrl":Ljava/lang/String;
    const/4 v4, 0x0

    .line 384
    .local v4, "mRemoteSubtitlePath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->GetRemoteSubtitleUrl()Ljava/lang/String;

    move-result-object v5

    .line 386
    if-eqz v5, :cond_4

    .line 388
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->RemoteSubTitledownloadThreadCheck()Z

    move-result v10

    if-eqz v10, :cond_3

    .line 389
    const-string v10, "MiniVideoSubtitleUtil"

    const-string v11, "DownloadCompelet!!!!"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->GetRemoteSubtitlePath()Ljava/lang/String;

    move-result-object v4

    .line 392
    const-string v10, "MiniVideoSubtitleUtil"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mRemoteSubtitlePath  :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    .line 398
    .end local v4    # "mRemoteSubtitlePath":Ljava/lang/String;
    .end local v5    # "mRemoteSubtitleUrl":Ljava/lang/String;
    :cond_4
    return-void

    .line 344
    .restart local v3    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .restart local v6    # "pathEx":Ljava/lang/String;
    .restart local v7    # "subtitleUrl":Ljava/lang/String;
    :cond_5
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 345
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->SetRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 346
    const-string v10, "MiniVideoSubtitleUtil"

    const-string v11, "Subtitle is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 367
    .end local v3    # "getSubtitle":Lcom/samsung/android/allshare/extension/SECVideoCaption;
    .end local v6    # "pathEx":Ljava/lang/String;
    .restart local v8    # "subtitletype":Ljava/lang/String;
    .restart local v9    # "uri2string":Ljava/lang/String;
    :cond_6
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 368
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->SetRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 369
    const-string v10, "MiniVideoSubtitleUtil"

    const-string v11, "Subtitle is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 372
    :cond_7
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkRemoteSubtitleFile(Z)V

    .line 373
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->SetRemoteSubtitleUrl(Ljava/lang/String;)V

    .line 374
    const-string v10, "MiniVideoSubtitleUtil"

    const-string v11, "Subtitle is null"

    invoke-static {v10, v11}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public isRemoteSubtitleFile()Z
    .locals 1

    .prologue
    .line 436
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mRemoteSubtitleFileExist:Z

    return v0
.end method

.method public processWebVTTOutband(I)V
    .locals 3
    .param p1, "what"    # I

    .prologue
    const/4 v2, 0x0

    .line 202
    sparse-switch p1, :sswitch_data_0

    .line 217
    :goto_0
    return-void

    .line 205
    :sswitch_0
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "processWebVTTOutband. MEDIA_INFO_UNSUPPORTED_SUBTITLE E or MEDIA_INFO_SUBTITLE_TIMED_OUT E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleFile(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :sswitch_1
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "processWebVTTOutband. MEDIA_INFO_METADATA_UPDATE E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0, v2, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 202
    nop

    :sswitch_data_0
    .sparse-switch
        0x322 -> :sswitch_1
        0x385 -> :sswitch_0
        0x386 -> :sswitch_0
    .end sparse-switch
.end method

.method public resetSubtitleLang()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 566
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "resetSubtitleLang()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mCountSubtitleOn:I

    .line 568
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mIsMultiSubtitle:Z

    .line 569
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 570
    return-void
.end method

.method public resetSubtitleSettings()V
    .locals 2

    .prologue
    .line 313
    const-string v0, "MiniVideoSubtitleUtil"

    const-string v1, "resetSubtitleSettings E"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 315
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    .line 316
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleActive(Z)V

    .line 317
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->setSubtitleViewVisiblity()V

    .line 318
    return-void
.end method

.method public setMultiEnable(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    .line 549
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIsMultiSubtitle on: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mIsMultiSubtitle:Z

    .line 551
    return-void
.end method

.method public setMultiSelectSubtitleCount(I)V
    .locals 0
    .param p1, "subtitleCount"    # I

    .prologue
    .line 558
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mCountSubtitleOn:I

    .line 559
    return-void
.end method

.method public setMultiSelectedSubtitleIndex([I)V
    .locals 4
    .param p1, "multiSubtitleIndex"    # [I

    .prologue
    .line 573
    if-nez p1, :cond_1

    .line 574
    const-string v1, "MiniVideoSubtitleUtil"

    const-string v2, "setMultiSelectSubtitle multiSubtitleIndex is null return"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    :cond_0
    return-void

    .line 578
    :cond_1
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    .line 580
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 581
    const-string v1, "MiniVideoSubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMultiSelectSubtitle mSelectedSubtitleIndex["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSelectedSubtitleIndex:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setSubtitleActive(Z)V
    .locals 3
    .param p1, "active"    # Z

    .prologue
    .line 75
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleActive = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    const/16 v1, 0x69

    if-eq v0, v1, :cond_0

    .line 78
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    .line 80
    :cond_0
    return-void
.end method

.method public setSubtitleFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 102
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSubtitleFile = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 104
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->checkSubtitleFileType(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFileType:I

    .line 109
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleFile:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mHasSubtitleFile:Z

    .line 112
    :cond_0
    return-void
.end method

.method public setSubtitleLanguageIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 97
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mini setSubtitleLanguageIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    .line 99
    return-void
.end method

.method public setSubtitleSyncTime(I)V
    .locals 0
    .param p1, "mSubtitleSyncTime"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleSyncTime:I

    .line 200
    return-void
.end method

.method public setSubtitleView(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;)V
    .locals 0
    .param p1, "miniSubtitleView"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .prologue
    .line 163
    if-eqz p1, :cond_0

    .line 164
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 165
    :cond_0
    return-void
.end method

.method public setSubtitleViewVisiblity()V
    .locals 2

    .prologue
    .line 85
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-eqz v1, :cond_1

    .line 86
    const/4 v0, 0x0

    .line 92
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    .line 94
    :cond_0
    return-void

    .line 88
    .end local v0    # "visibility":I
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->stopSubtitle(Z)V

    .line 89
    const/4 v0, 0x4

    .restart local v0    # "visibility":I
    goto :goto_0
.end method

.method public setTimedTextTrack()V
    .locals 5

    .prologue
    .line 590
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;

    move-result-object v0

    .line 591
    .local v0, "trackInfo":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil;->getTextTracks()Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;

    move-result-object v1

    .line 593
    .local v1, "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v3

    if-lt v2, v3, :cond_2

    .line 594
    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    .line 595
    const-string v2, "MiniVideoSubtitleUtil"

    const-string v3, "setTimedTextTrack mSubtitleLanguageIndex is out of index reset mSubtitleLanguageIndex value!!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :goto_1
    return-void

    .line 591
    .end local v1    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 597
    .restart local v1    # "tracks":Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;
    :cond_2
    const-string v2, "MiniVideoSubtitleUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setTimedTextTrack : mTracks.size() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/TrackInfoUtil$Tracks;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 598
    const-string v2, "MiniVideoSubtitleUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setTimedTextTrack : mSubtitleLanguageIndex : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleLanguageIdx:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public stopSubtitle(Z)V
    .locals 2
    .param p1, "isExitByMoviePlayer"    # Z

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubTitle(Ljava/lang/String;)V

    .line 143
    :cond_0
    return-void
.end method

.method public updateSubtitle(Ljava/lang/String;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 146
    const-string v1, "MiniVideoSubtitleUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mini updateSubtitle E : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->removeHandlerMsg(I)V

    .line 149
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-nez v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 152
    :cond_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/common/VUtils;->getHTMLString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "subString":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public updateSubtitle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "headString"    # Ljava/lang/String;

    .prologue
    .line 221
    const-string v0, "MiniVideoSubtitleUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSubtitle E text="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->removeHandlerMsg(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-nez v0, :cond_0

    .line 236
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mSubtitleActivation:Z

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateSubTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleUtil;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->setVisibility(I)V

    goto :goto_0
.end method
