.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;
.super Ljava/lang/Object;
.source "MiniVideoSurface.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnBufferingUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mPrevCurrentPos:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)V
    .locals 1

    .prologue
    .line 1320
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1321
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->mPrevCurrentPos:I

    return-void
.end method


# virtual methods
.method public onBufferingUpdate(Landroid/media/MediaPlayer;I)V
    .locals 6
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "percent"    # I

    .prologue
    const/4 v5, 0x0

    .line 1323
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1324
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBufferingUpdateListener - miniService is NULL : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v1

    if-nez v1, :cond_2

    .line 1329
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getSchemeType()Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-result-object v2

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;
    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Lcom/sec/android/app/videoplayer/type/SchemeType;)Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 1331
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBufferingUpdateListener - total download : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isBufferingStreamingType()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isDownloadStreamingType()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1333
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1334
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/content/Context;

    move-result-object v3

    const/high16 v4, 0x7f0a0000

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0a00e6

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->changeLoadingDialogText(Ljava/lang/String;)V

    .line 1337
    :cond_4
    const/16 v1, 0x64

    if-ne p2, v1, :cond_5

    .line 1338
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->mPrevCurrentPos:I

    .line 1339
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    goto/16 :goto_0

    .line 1341
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPercentchecker:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2502(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Z)Z

    .line 1342
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isDownloadStreamingType()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$2400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1343
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v0

    .line 1344
    .local v0, "currentPos":I
    iget v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->mPrevCurrentPos:I

    if-ne v1, v0, :cond_6

    .line 1345
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1349
    :goto_1
    iput v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->mPrevCurrentPos:I

    goto/16 :goto_0

    .line 1347
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    goto :goto_1

    .line 1351
    .end local v0    # "currentPos":I
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$11;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->miniService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    goto/16 :goto_0
.end method
