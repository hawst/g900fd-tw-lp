.class Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;
.super Ljava/lang/Object;
.source "ControllerUtils.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->setPopupPlayerBtn(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 229
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$100(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->createTalkBackOffDialog()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$400(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$3;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->openPopupPlayer()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$500(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0
.end method
