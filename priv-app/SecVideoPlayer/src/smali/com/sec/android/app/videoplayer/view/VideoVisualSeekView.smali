.class public Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
.super Ljava/lang/Object;
.source "VideoVisualSeekView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentPosition:I

.field private mIsInitialized:Z

.field private mParentView:Landroid/widget/RelativeLayout;

.field private mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mUri:Landroid/net/Uri;

.field private mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

.field private mediaPlayer:Landroid/media/MediaPlayer;

.field private surfaceHolder:Landroid/view/SurfaceHolder;

.field private surfaceView:Landroid/view/SurfaceView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "VideoVisualSeekView"

    sput-object v0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mContext:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 31
    iput v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mCurrentPosition:I

    .line 32
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    .line 33
    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    .line 35
    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    .line 70
    new-instance v0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$1;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    .line 38
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method private ShowVideo()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 104
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v2, :cond_1

    .line 105
    :cond_0
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v3, "ShowVideo() uri null or surfaceholder null"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    .line 110
    iput-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 113
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    if-nez v2, :cond_3

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/db/VideoDB;->createInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/VideoDB;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    .line 117
    :cond_3
    :try_start_0
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    .line 119
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$2;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 130
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$3;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 136
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$4;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$5;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$5;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 149
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$6;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$6;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    new-instance v3, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$7;

    invoke-direct {v3, p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$7;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    .line 185
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 186
    .local v0, "Path":Ljava/lang/String;
    if-nez v0, :cond_4

    .line 187
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v3, "playVideo() :: Path is null"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 207
    .end local v0    # "Path":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 208
    .local v1, "ex":Ljava/io/IOException;
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IOException - Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 190
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v0    # "Path":Ljava/lang/String;
    :cond_4
    :try_start_1
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playVideo() :: mVideoDB.getFilePath(mUri) is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mVideoDB:Lcom/sec/android/app/videoplayer/db/VideoDB;

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/videoplayer/db/VideoDB;->getFilePath(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const-string v2, "content://"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 192
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 193
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playvideo mUri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/16 v3, 0x79e

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 202
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 203
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setScreenOnWhilePlaying(Z)V

    .line 204
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setAIAContext(Z)V

    .line 205
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 209
    .end local v0    # "Path":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 210
    .local v1, "ex":Ljava/lang/IllegalStateException;
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalStateException - Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 195
    .end local v1    # "ex":Ljava/lang/IllegalStateException;
    .restart local v0    # "Path":Ljava/lang/String;
    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 196
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "playvideo Path: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 211
    .end local v0    # "Path":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 212
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IllegalArgumentException - Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
    .param p1, "x1"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->ShowVideo()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)Landroid/media/MediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
    .param p1, "x1"    # Landroid/media/MediaPlayer;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setAIAContext(Z)V

    return-void
.end method

.method static synthetic access$502(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    .prologue
    .line 20
    iget v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mCurrentPosition:I

    return v0
.end method

.method private setAIAContext(Z)V
    .locals 3
    .param p1, "set"    # Z

    .prologue
    .line 294
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->setAIAContext(Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 300
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "setAIAContext - NullPointerException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 297
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/IllegalStateException;
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "setAIAContext - IllegalStateException "

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addViewTo(Landroid/view/View;II)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    const/4 v3, -0x1

    .line 43
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    if-nez v1, :cond_0

    .line 44
    new-instance v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$VisualSeekSurface;-><init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    .line 46
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 47
    .local v0, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 48
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 51
    .end local v0    # "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 52
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 54
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    if-eqz v1, :cond_1

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 60
    :cond_1
    return-void
.end method

.method public bringToFront()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->bringToFront()V

    .line 290
    :cond_0
    return-void
.end method

.method public getVideoHeight()I
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    .line 313
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoWidth()I
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    if-eqz v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    .line 306
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 233
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "video pause()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 235
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "video pause() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 239
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseView()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/ViewUnbindUtil;->unbindReferences(Landroid/view/View;)V

    .line 318
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    .line 319
    return-void
.end method

.method public removeViewTo(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    check-cast p1, Landroid/widget/RelativeLayout;

    .end local p1    # "view":Landroid/view/View;
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    .line 66
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    .line 68
    :cond_0
    return-void
.end method

.method public seekto(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 250
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_1

    .line 251
    if-gtz p1, :cond_0

    .line 252
    const/4 p1, 0x0

    .line 254
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    if-eqz v1, :cond_1

    .line 256
    :try_start_0
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "seek() position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1}, Landroid/media/MediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_1
    :goto_0
    return-void

    .line 258
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public setCurrentPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mCurrentPosition:I

    .line 97
    return-void
.end method

.method public setCurrentUri(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mUri:Landroid/net/Uri;

    .line 101
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 217
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "video start()"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 219
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "video start() called"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    iget v2, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mCurrentPosition:I

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 225
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public video_dismiss()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 267
    sget-object v1, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;

    const-string v2, "video dismiss() "

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 271
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->setAIAContext(Z)V

    .line 272
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 273
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v1, :cond_1

    .line 281
    iput-object v4, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->surfaceHolder:Landroid/view/SurfaceHolder;

    .line 284
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z

    .line 285
    return-void

    .line 274
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
