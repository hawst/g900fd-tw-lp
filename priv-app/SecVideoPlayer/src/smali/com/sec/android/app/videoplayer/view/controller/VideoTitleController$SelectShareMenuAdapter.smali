.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VideoTitleController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectShareMenuAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private intent:Landroid/content/Intent;

.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSharePopupMenu:Landroid/widget/ListPopupWindow;

.field private packageManager:Landroid/content/pm/PackageManager;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;Landroid/content/Context;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I

    .prologue
    .line 2618
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    .line 2619
    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2620
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->context:Landroid/content/Context;

    .line 2621
    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    .prologue
    .line 2611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    .prologue
    .line 2611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;)Landroid/widget/ListPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;

    .prologue
    .line 2611
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 2635
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2640
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/content/Context;

    move-result-object v6

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 2641
    .local v3, "mLayoutInflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030011

    const/4 v7, 0x0

    invoke-virtual {v3, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 2642
    .local v5, "v":Landroid/view/View;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->packageManager:Landroid/content/pm/PackageManager;

    if-eqz v6, :cond_0

    .line 2643
    const v6, 0x7f0d009c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2644
    .local v1, "appName":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "\u200f"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2645
    const v6, 0x7f0d009b

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2646
    .local v0, "appIcon":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2647
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 2648
    .local v4, "packageName":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    iget-object v6, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 2649
    .local v2, "className":Ljava/lang/String;
    new-instance v6, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter$1;

    invoke-direct {v6, p0, v4, v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2657
    new-instance v6, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter$2;

    invoke-direct {v6, p0, v4}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 2666
    .end local v0    # "appIcon":Landroid/widget/ImageView;
    .end local v1    # "appName":Landroid/widget/TextView;
    .end local v2    # "className":Ljava/lang/String;
    .end local v4    # "packageName":Ljava/lang/String;
    :cond_0
    return-object v5
.end method

.method public setAppList(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2624
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->intent:Landroid/content/Intent;

    .line 2625
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->packageManager:Landroid/content/pm/PackageManager;

    .line 2626
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->packageManager:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mAppList:Ljava/util/List;

    .line 2627
    return-void
.end method

.method public setListPopupWindow(Landroid/widget/ListPopupWindow;)V
    .locals 0
    .param p1, "mSharePopupMenu"    # Landroid/widget/ListPopupWindow;

    .prologue
    .line 2630
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$SelectShareMenuAdapter;->mSharePopupMenu:Landroid/widget/ListPopupWindow;

    .line 2631
    return-void
.end method
