.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;
.super Landroid/content/BroadcastReceiver;
.source "MiniVideoPlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 864
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 866
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 867
    .local v0, "action":Ljava/lang/String;
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mReceiver : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    const-string v9, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 869
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mStatusReceiver - ACTION_BATTERY_CHANGED."

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 870
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const-string v10, "status"

    const/4 v11, 0x1

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1102(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I

    .line 871
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const-string v10, "scale"

    const/16 v11, 0x64

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I

    .line 872
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const-string v10, "level"

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v11

    invoke-virtual {p2, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I

    .line 874
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mReceiver. battScale : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattScale:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", battLevel : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", battStatus : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattLevel:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v9

    const/4 v10, 0x1

    if-gt v9, v10, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mBattStatus:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_0

    .line 878
    const-string v9, "MiniVideoPlayerService"

    const-string v10, " Minimode exiting due to low battery"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f0a009e

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/Toast;->show()V

    .line 880
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 1006
    :cond_0
    :goto_0
    return-void

    .line 883
    :cond_1
    const-string v9, "android.intent.action.SCREEN_ON"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 884
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "ACTION_SCREEN_ON"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSeekPosition()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 886
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->openVideo()V

    .line 888
    :cond_2
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v9

    const/4 v10, 0x5

    if-eq v9, v10, :cond_0

    .line 889
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setPlaySpeed(I)V

    goto :goto_0

    .line 891
    :cond_3
    const-string v9, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 892
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "ACTION_SCREEN_OFF"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 894
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mExitLayout:Landroid/widget/RelativeLayout;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 895
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/4 v10, 0x0

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setProgressBarVisibility(I)V
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)V

    .line 897
    :cond_5
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->saveResumePos()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 898
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->stopPlayback()V

    goto/16 :goto_0

    .line 899
    :cond_6
    const-string v9, "intent.stop.app-in-app"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 900
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mReceiver - stop called !! maybe camera or outside."

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 901
    const-string v9, "stopFromInternal"

    const-string v10, "stopFrom"

    invoke-virtual {p2, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 902
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v10, 0x7f0a0011

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 904
    :cond_7
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 905
    :cond_8
    const-string v9, "com.android.settings.action.talkback_off"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 906
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 907
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v10, 0x7f0a0011

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 908
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 910
    :cond_9
    const-string v9, "android.intent.action.PALM_DOWN"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 911
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v1

    .line 912
    .local v1, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v9

    if-nez v9, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v9

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 913
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto/16 :goto_0

    .line 915
    .end local v1    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_a
    const-string v9, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 916
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mReceiver - receive ACTION_AUDIO_BECOMING_NOISY"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 918
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_b

    .line 919
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 922
    :cond_b
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    const/4 v10, 0x0

    iput-boolean v10, v9, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->mPausedByTransientLossOfFocus:Z

    goto/16 :goto_0

    .line 929
    :cond_c
    const-string v9, "AppInAppResumePositionRequest"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_d

    .line 930
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "AIA - APP_IN_APP_RESUME_POS_REQUEST :"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 931
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 932
    .local v2, "i":Landroid/content/Intent;
    const-string v9, "AppInAppResumePositionReply"

    invoke-virtual {v2, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 933
    const-string v9, "resumePosByAIA"

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v10

    invoke-virtual {v2, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 934
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v9, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 935
    .end local v2    # "i":Landroid/content/Intent;
    :cond_d
    const-string v9, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 937
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "Exit MiniPlayer for connecting SideSync"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 939
    :cond_e
    sget-boolean v9, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WIFI_DISPLAY:Z

    if-eqz v9, :cond_f

    const-string v9, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 940
    const-string v9, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v9}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v8

    check-cast v8, Landroid/hardware/display/WifiDisplayStatus;

    .line 941
    .local v8, "status":Landroid/hardware/display/WifiDisplayStatus;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v9

    const/4 v10, 0x2

    if-ne v9, v10, :cond_0

    .line 942
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "ACTION_WIFI_DISPLAY_STATUS_CHANGED. DISPLAY_STATE_CONNECTED"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v10, 0x7f0a0011

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 944
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 946
    .end local v8    # "status":Landroid/hardware/display/WifiDisplayStatus;
    :cond_f
    const-string v9, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_10

    .line 947
    const-string v9, "state"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 948
    .local v5, "isHDMIPlugged":Z
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mStatusReceiver - ACTION_HDMI_PLUG. plug is = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    if-eqz v5, :cond_0

    .line 951
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v10, 0x7f0a0011

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 952
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 954
    .end local v5    # "isHDMIPlugged":Z
    :cond_10
    const-string v9, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 955
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 956
    :cond_11
    const-string v9, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 957
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "ACTION_USER_SWITCHED "

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 959
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto/16 :goto_0

    .line 960
    :cond_12
    sget-object v9, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PAUSE_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_14

    .line 961
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mReceiver - receive ACTION_PAUSE_BY_SPEN"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_13

    .line 963
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    .line 967
    :goto_1
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 968
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto/16 :goto_0

    .line 965
    :cond_13
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto :goto_1

    .line 969
    :cond_14
    sget-object v9, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_PLAY_BY_SPEN:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_17

    .line 970
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mReceiver - receive ACTION_PLAY_BY_SPEN"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    if-ne v9, v10, :cond_15

    .line 972
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->resume()V

    .line 973
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PLAY:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto/16 :goto_0

    .line 974
    :cond_15
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    move-result-object v9

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    if-ne v9, v10, :cond_0

    .line 975
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_16

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 976
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    .line 977
    :cond_16
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    sget-object v10, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;->PAUSE:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPrevPlayerState:Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;
    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;)Lcom/sec/android/app/videoplayer/activity/MoviePlayer$ePlayerStateType;

    goto/16 :goto_0

    .line 979
    :cond_17
    const-string v9, "com.android.internal.policy.impl.sec.glanceview.eventinfo"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_19

    .line 980
    const-string v9, "isStart"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 981
    .local v6, "isStart":Z
    const-string v9, "isEnd"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 983
    .local v4, "isEnd":Z
    if-eqz v6, :cond_18

    .line 984
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->hide()V

    goto/16 :goto_0

    .line 985
    :cond_18
    if-eqz v4, :cond_0

    .line 986
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->show()V

    goto/16 :goto_0

    .line 988
    .end local v4    # "isEnd":Z
    .end local v6    # "isStart":Z
    :cond_19
    const-string v9, "com.sec.android.app.videoplayer.SORT_BY"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1a

    .line 989
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "mReceiver - receive ACTION_SORT_BY"

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    const-string v9, "sortby"

    const/4 v10, 0x2

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 991
    .local v7, "sortOrder":I
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mReceiver - sortOrder -> "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 992
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v9

    const-string v10, "sortorder"

    invoke-virtual {v9, v10, v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 993
    .end local v7    # "sortOrder":I
    :cond_1a
    const-string v9, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1b

    .line 994
    const-string v9, "easymode"

    const/4 v10, 0x0

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 995
    .local v3, "isEasyMode":Z
    const-string v9, "MiniVideoPlayerService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "EASY_MODE : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 996
    if-eqz v3, :cond_0

    .line 997
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    const v10, 0x7f0a0011

    invoke-virtual {v9, v10}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 998
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->exitThis()V
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto/16 :goto_0

    .line 1000
    .end local v3    # "isEasyMode":Z
    :cond_1b
    const-string v9, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1001
    const-string v9, "MiniVideoPlayerService"

    const-string v10, "Home is resumed. Check lock screen status on or off"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkLockScreenOn(Landroid/content/Context;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1003
    iget-object v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$3;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->pause()V

    goto/16 :goto_0
.end method
