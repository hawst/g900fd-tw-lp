.class public Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
.super Landroid/widget/RelativeLayout;
.source "MiniVideoBtnController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    }
.end annotation


# static fields
.field public static final ANIMATION_DURATION:J = 0x12cL

.field public static final CONTROLLER_TIME_OUT:I = 0xbb8

.field private static final DELAYED_CMD_PLAYSHORT:I = 0x20

.field private static final FADE_OUT:I = 0x64

.field private static final FFLONGSEEK:I = 0x0

.field protected static final FFW_RWD_NOT_SUPPORT:I = 0x1e

.field private static final HIDE_SPEEDTEXT:I = 0xc

.field public static final IFRAME_SEEK:I = 0x0

.field private static final LONGSEEKMSG:I = 0x3

.field private static final LONG_PRESS_TIME:J = 0x1f4L

.field private static final PROGRESS_RESOLUTION:J = 0x186a0L

.field public static final REAL_SEEK:I = 0x1

.field private static final REWLONGSEEK:I = 0x1

.field private static final SHOW_PROGRESS:I = 0x1

.field private static final SHOW_SPEEDTEXT:I = 0xb

.field private static final TAG:Ljava/lang/String; = "MiniVideoBtnController"


# instance fields
.field private mButtonEnable:Z

.field private mContext:Landroid/content/Context;

.field private mCtrlLayoutShow:Z

.field private mDownKeyPressTime:J

.field private mExitLayout:Landroid/widget/RelativeLayout;

.field private final mHandler:Landroid/os/Handler;

.field private mHoldLongSeekSpeed:Z

.field private mHoldLongSeekSpeed4SeekSpeedChange:Z

.field private mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

.field private mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

.field private mLastPos:J

.field private mLongKeyCnt:I

.field private mLongSeekMode:I

.field private mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

.field public mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

.field private mNextButton:Landroid/widget/ImageButton;

.field private mNextKeyListener:Landroid/view/View$OnKeyListener;

.field private mNextTouchListener:Landroid/view/View$OnTouchListener;

.field private mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

.field protected mParent:Landroid/widget/RelativeLayout;

.field private mPauseKeyListener:Landroid/view/View$OnKeyListener;

.field private mPauseTouchListener:Landroid/view/View$OnTouchListener;

.field private mPlayPauseButton:Landroid/widget/ImageButton;

.field private mPrevButton:Landroid/widget/ImageButton;

.field private mPrevKeyListener:Landroid/view/View$OnKeyListener;

.field private mPrevTouchListener:Landroid/view/View$OnTouchListener;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field protected mRoot:Landroid/view/View;

.field private mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

.field private mSeekPos:I

.field private mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

.field protected mSpeedLayout:Landroid/widget/RelativeLayout;

.field protected mSpeedText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;Lcom/sec/android/app/videoplayer/type/SchemeType;Landroid/widget/ProgressBar;Landroid/widget/RelativeLayout;Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/widget/RelativeLayout;
    .param p3, "miniSurface"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .param p4, "miniSubtitleView"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;
    .param p5, "schemeType"    # Lcom/sec/android/app/videoplayer/type/SchemeType;
    .param p6, "progressBar"    # Landroid/widget/ProgressBar;
    .param p7, "exitLayout"    # Landroid/widget/RelativeLayout;
    .param p8, "miniVideoPlayerService"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedText:Landroid/widget/TextView;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    .line 78
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mParent:Landroid/widget/RelativeLayout;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedLayout:Landroid/widget/RelativeLayout;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    .line 96
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mButtonEnable:Z

    .line 100
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed:Z

    .line 102
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed4SeekSpeedChange:Z

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 110
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J

    .line 112
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 113
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 114
    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 115
    iput-wide v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 490
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    .line 685
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$6;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    .line 716
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$7;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    .line 747
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$8;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevTouchListener:Landroid/view/View$OnTouchListener;

    .line 822
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$9;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevKeyListener:Landroid/view/View$OnKeyListener;

    .line 888
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$10;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextTouchListener:Landroid/view/View$OnTouchListener;

    .line 967
    new-instance v0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$11;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextKeyListener:Landroid/view/View$OnKeyListener;

    .line 119
    iput-object p0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    .line 120
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mParent:Landroid/widget/RelativeLayout;

    .line 121
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    .line 122
    iput-object p5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    .line 123
    iput-object p3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    .line 124
    iput-object p4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    .line 125
    iput-object p6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    .line 126
    iput-object p7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    .line 127
    iput-object p8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    .line 128
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->initView()V

    .line 129
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/type/SchemeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->rewSeek()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->blockSpeedSeek()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
    .param p1, "x1"    # J

    .prologue
    .line 34
    iput-wide p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mDownKeyPressTime:J

    return-wide p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mService:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/widget/HoverPopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->ffSeek()V

    return-void
.end method

.method private blockSpeedSeek()Z
    .locals 1

    .prologue
    .line 370
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isAllShareList()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ffSeek()V
    .locals 11

    .prologue
    const/16 v10, 0xa

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/16 v6, 0xc

    .line 1226
    const-string v2, "MiniVideoBtnController"

    const-string v3, "ffSeek E"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1229
    const/4 v1, 0x0

    .line 1231
    .local v1, "scale":I
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-le v2, v6, :cond_0

    .line 1232
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 1235
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v2

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 1236
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek4SpeedChange()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1237
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    .line 1294
    :cond_1
    :goto_0
    return-void

    .line 1242
    :cond_2
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    if-nez v2, :cond_3

    .line 1243
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1246
    :cond_3
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-lt v2, v9, :cond_6

    .line 1247
    const/4 v1, 0x1

    .line 1255
    :goto_1
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 1256
    .local v0, "powVal":I
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    mul-int/lit16 v3, v1, 0x3e8

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1257
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1259
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v3

    if-le v2, v3, :cond_4

    .line 1260
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1261
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1264
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1265
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    .line 1270
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgress()I

    .line 1272
    if-ne v1, v7, :cond_9

    .line 1273
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    if-gtz v2, :cond_9

    .line 1274
    const/16 v2, 0xbb8

    invoke-virtual {p0, v8, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    .line 1275
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1276
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    .line 1277
    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto :goto_0

    .line 1248
    .end local v0    # "powVal":I
    :cond_6
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-lt v2, v7, :cond_7

    .line 1249
    const/4 v1, 0x1

    .line 1250
    iput v9, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    goto :goto_1

    .line 1252
    :cond_7
    const/4 v1, -0x1

    goto :goto_1

    .line 1267
    .restart local v0    # "powVal":I
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    invoke-virtual {v2, v3, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(II)V

    goto :goto_2

    .line 1282
    :cond_9
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v3

    if-ge v2, v3, :cond_a

    .line 1283
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setSpeedText(I)V

    .line 1284
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1285
    const/4 v2, 0x3

    const-wide/16 v4, 0x1f4

    invoke-virtual {p0, v2, v4, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->sendMessage(IJ)V

    .line 1287
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek4SpeedChange()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1288
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    goto/16 :goto_0

    .line 1291
    :cond_a
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    .line 1292
    invoke-virtual {p0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    goto/16 :goto_0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->initProgress()V

    .line 200
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->initCtrlButton(Landroid/view/View;)V

    .line 201
    return-void
.end method

.method private initCtrlButton(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 212
    const v1, 0x7f0d007a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080140

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 214
    .local v0, "hoverCustomPopupOffset":I
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 215
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPauseTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 216
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPauseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 217
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 218
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updatePausePlayBtn()V

    .line 222
    :cond_0
    const v1, 0x7f0d0079

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 225
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 227
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 228
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 232
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 233
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 235
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowPrev:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$2;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0170

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 274
    :cond_2
    const v1, 0x7f0d007b

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    .line 276
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_4

    .line 277
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 278
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 279
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 280
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setHoverPopupType(I)V

    .line 281
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    .line 282
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    if-eqz v1, :cond_3

    .line 283
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v5}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 284
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 285
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v0}, Landroid/widget/HoverPopupWindow;->setGuideLineFadeOffset(I)V

    .line 286
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/HoverPopupWindow;->setInfoPickerMoveEabled(Z)V

    .line 287
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoverPopupWindowFF:Landroid/widget/HoverPopupWindow;

    new-instance v2, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$3;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    invoke-virtual {v1, v2}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 321
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a015c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 322
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 325
    :cond_4
    return-void
.end method

.method private initProgress()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 208
    :cond_0
    return-void
.end method

.method private initView()V
    .locals 5

    .prologue
    .line 132
    const-string v3, "MiniVideoBtnController"

    const-string v4, "MiniVideoBtnController - initFloatingWindow"

    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->getRootView()Landroid/view/View;

    move-result-object v2

    .line 134
    .local v2, "mDecor":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080174

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 135
    .local v0, "height":I
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v1, v3, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 136
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 137
    const/16 v3, 0xe

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 139
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mParent:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_0

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mParent:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    :cond_0
    const/16 v3, 0x8

    invoke-virtual {p0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setVisibility(I)V

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->addControllerView()V

    .line 144
    return-void
.end method

.method private rewSeek()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v5, -0x2

    const/16 v4, -0xc

    const/4 v6, 0x0

    .line 1297
    const-string v2, "MiniVideoBtnController"

    const-string v3, "rewSeek E."

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1300
    const/4 v1, 0x0

    .line 1302
    .local v1, "scale":I
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-ge v2, v4, :cond_0

    .line 1303
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 1306
    :cond_0
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    if-nez v2, :cond_1

    .line 1307
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1310
    :cond_1
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-gt v2, v5, :cond_6

    .line 1311
    const/4 v1, -0x1

    .line 1319
    :goto_0
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v0, v2

    .line 1320
    .local v0, "powVal":I
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setSpeedText(I)V

    .line 1321
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1322
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    mul-int/lit16 v3, v1, 0x3e8

    mul-int/2addr v3, v0

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1323
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1325
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    if-gtz v2, :cond_2

    .line 1326
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1329
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1330
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    .line 1335
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgress()I

    .line 1337
    if-ne v1, v7, :cond_4

    .line 1338
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v3

    if-lt v2, v3, :cond_4

    .line 1339
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    .line 1340
    const/16 v2, 0xa

    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 1344
    :cond_4
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    if-lez v2, :cond_9

    .line 1345
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v8, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->sendMessage(IJ)V

    .line 1346
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek4SpeedChange()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1347
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 1356
    :cond_5
    :goto_2
    return-void

    .line 1312
    .end local v0    # "powVal":I
    :cond_6
    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    if-gt v2, v7, :cond_7

    .line 1313
    const/4 v1, -0x1

    .line 1314
    iput v5, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    goto :goto_0

    .line 1316
    :cond_7
    const/4 v1, 0x1

    goto :goto_0

    .line 1332
    .restart local v0    # "powVal":I
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    invoke-virtual {v2, v3, v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(II)V

    goto :goto_1

    .line 1350
    :cond_9
    const/16 v2, 0xbb8

    invoke-virtual {p0, v6, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    .line 1351
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1352
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek()V

    .line 1353
    invoke-virtual {p0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->removeMessage(I)V

    .line 1354
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    goto :goto_2
.end method

.method private setEnableProgressbar(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    .line 1072
    :cond_0
    return-void
.end method

.method private setPlayPauseButton(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 639
    const v1, 0x7f0a016c

    .line 640
    .local v1, "descID":I
    const v2, 0x7f02001c

    .line 641
    .local v2, "resID":I
    const-string v0, ""

    .line 643
    .local v0, "desc":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 644
    const v2, 0x7f020007

    .line 645
    const v1, 0x7f0a016c

    .line 653
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 655
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 656
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 657
    return-void

    .line 646
    :cond_1
    const/4 v3, 0x1

    if-ne p1, v3, :cond_2

    .line 647
    const v2, 0x7f020006

    .line 648
    const v1, 0x7f0a016b

    goto :goto_0

    .line 649
    :cond_2
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 650
    const v2, 0x7f02000a

    .line 651
    const v1, 0x7f0a0172

    goto :goto_0
.end method

.method private setProgressBarVisibility(IZ)V
    .locals 8
    .param p1, "visibility"    # I
    .param p2, "isAnim"    # Z

    .prologue
    const-wide/16 v6, 0x12c

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 539
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->IsPauseEnable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 540
    if-eqz p2, :cond_0

    .line 541
    if-nez p1, :cond_2

    .line 542
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 543
    .local v0, "anim_fadein":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 544
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    .line 551
    .end local v0    # "anim_fadein":Landroid/view/animation/AlphaAnimation;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 553
    :cond_1
    return-void

    .line 546
    :cond_2
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 547
    .local v1, "anim_fadeout":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 548
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public addControllerView()V
    .locals 5

    .prologue
    const/4 v3, -0x2

    .line 151
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->removeAllViews()V

    .line 153
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 155
    .local v1, "rp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080170

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 156
    const/16 v3, 0xc

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 158
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->makeControllerView()Landroid/view/View;

    move-result-object v2

    .line 159
    .local v2, "v":Landroid/view/View;
    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 162
    .local v0, "config":Landroid/content/res/Configuration;
    iget v3, v0, Landroid/content/res/Configuration;->keyboard:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    iget v3, v0, Landroid/content/res/Configuration;->navigation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 164
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 167
    :cond_0
    const v3, 0x7f0d0075

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$1;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 187
    return-void
.end method

.method public forceHide()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 453
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v0, :cond_1

    .line 454
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setVisibility(I)V

    .line 455
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->hideProgress(Z)V

    .line 456
    invoke-virtual {p0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setExitLayoutVisibility(IZ)V

    .line 457
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateLayout()V

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 462
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    .line 464
    :cond_1
    return-void
.end method

.method public hide(Z)V
    .locals 5
    .param p1, "isAnim"    # Z

    .prologue
    const/16 v4, 0x8

    .line 414
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    if-eqz v1, :cond_0

    .line 415
    const v1, 0x7f0d0075

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 416
    const-string v1, "MiniVideoBtnController"

    const-string v2, "hide : findViewById() is null view not inflate yet!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 420
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->hideProgress(Z)V

    .line 421
    invoke-virtual {p0, v4, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setExitLayoutVisibility(IZ)V

    .line 423
    if-eqz p1, :cond_2

    .line 424
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 425
    .local v0, "anim_fadeout":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 426
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->startAnimation(Landroid/view/animation/Animation;)V

    .line 428
    new-instance v1, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$4;-><init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 444
    .end local v0    # "anim_fadeout":Landroid/view/animation/AlphaAnimation;
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setVisibility(I)V

    .line 446
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    .line 447
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 448
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public hideProgress(Z)V
    .locals 1
    .param p1, "isAnim"    # Z

    .prologue
    .line 590
    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgressBarVisibility(IZ)V

    .line 591
    return-void
.end method

.method public isHoldLongSeek()Z
    .locals 1

    .prologue
    .line 1113
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed:Z

    return v0
.end method

.method public isHoldLongSeek4SpeedChange()Z
    .locals 1

    .prologue
    .line 1125
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed4SeekSpeedChange:Z

    return v0
.end method

.method public isInSeekingMode()Z
    .locals 1

    .prologue
    .line 375
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 379
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    return v0
.end method

.method public isSupportNextPrev()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 357
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromGallerySecureLock()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v0

    .line 361
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isVideoList()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isAllShareList()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isSLink()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDirectDMC()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 363
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected makeControllerView()Landroid/view/View;
    .locals 3

    .prologue
    .line 190
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 191
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const v1, 0x7f03000b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    .line 192
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0076

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedLayout:Landroid/widget/RelativeLayout;

    .line 193
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    const v2, 0x7f0d0077

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedText:Landroid/widget/TextView;

    .line 194
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->initControllerView(Landroid/view/View;)V

    .line 195
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 467
    const-string v0, "MiniVideoBtnController"

    const-string v1, "MiniVideoBtnController - onDetachedFromWindow"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 471
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    .line 472
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 473
    return-void
.end method

.method public performCommand(I)V
    .locals 10
    .param p1, "cmd"    # I

    .prologue
    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    const v3, 0x36ee80

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1129
    packed-switch p1, :pswitch_data_0

    .line 1223
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1132
    :pswitch_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setHoldLongSeek4SpeedChange()V

    goto :goto_0

    .line 1136
    :pswitch_2
    const-string v0, "MiniVideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performCommand() - CMD_FFLONGSEEK state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1138
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-nez v0, :cond_2

    .line 1139
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1140
    iput v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1141
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    .line 1142
    const/4 v0, 0x3

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->sendMessage(IJ)V

    .line 1149
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek4SpeedChange()V

    goto :goto_0

    .line 1143
    :cond_2
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-ne v0, v7, :cond_3

    .line 1144
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_1

    .line 1145
    :cond_3
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-ne v0, v4, :cond_1

    .line 1146
    iput v7, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1147
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_1

    .line 1153
    :pswitch_3
    const-string v0, "MiniVideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performCommand() - CMD_REWLONGSEEK state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1155
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-nez v0, :cond_5

    .line 1156
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSeekPos:I

    .line 1157
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1158
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    .line 1159
    const/4 v0, 0x3

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->sendMessage(IJ)V

    .line 1167
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek4SpeedChange()V

    goto :goto_0

    .line 1160
    :cond_5
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-ne v0, v4, :cond_6

    .line 1161
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_2

    .line 1162
    :cond_6
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-ne v0, v7, :cond_4

    .line 1164
    iput v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1165
    invoke-virtual {p0, v6, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_2

    .line 1171
    :pswitch_4
    const-string v0, "MiniVideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performCommand() - CMD_RESETSEEK state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-eqz v0, :cond_0

    .line 1176
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->removeMessage(I)V

    .line 1179
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1181
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    cmp-long v0, v0, v8

    if-gez v0, :cond_7

    .line 1182
    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1184
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isDownloadList()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1185
    :cond_8
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_9

    .line 1186
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(I)V

    .line 1199
    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1201
    iput-wide v8, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    .line 1202
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1203
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 1204
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek4SpeedChange()V

    goto/16 :goto_0

    .line 1188
    :cond_a
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isInternal()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isExternal()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1189
    :cond_b
    iget-wide v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_c

    .line 1190
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(II)V

    goto :goto_3

    .line 1192
    :cond_c
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0, v7, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(II)V

    goto :goto_3

    .line 1195
    :cond_d
    const-string v0, "MiniVideoBtnController"

    const-string v1, "Real seek called!!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLastPos:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    long-to-int v1, v2

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->seekTo(II)V

    goto :goto_3

    .line 1208
    :pswitch_5
    const-string v0, "MiniVideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "performCommand() - CMD_RESET state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->removeMessage(I)V

    .line 1212
    iget v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    if-eqz v0, :cond_e

    .line 1213
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 1215
    :cond_e
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1216
    iput v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongKeyCnt:I

    .line 1217
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->resetHoldLongSeek4SpeedChange()V

    goto/16 :goto_0

    .line 1129
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public playerStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1075
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1076
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1077
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 1079
    :cond_0
    return-void
.end method

.method public removeMessage(I)V
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 488
    :goto_0
    return-void

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public resetHoldLongSeek()V
    .locals 1

    .prologue
    .line 1102
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isHoldLongSeek()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1103
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->performCommand(I)V

    .line 1104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed:Z

    .line 1106
    :cond_0
    return-void
.end method

.method public resetHoldLongSeek4SpeedChange()V
    .locals 1

    .prologue
    .line 1117
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed4SeekSpeedChange:Z

    .line 1118
    return-void
.end method

.method public sendMessage(IJ)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "delay"    # J

    .prologue
    .line 476
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 482
    :goto_0
    return-void

    .line 478
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 480
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p2, p3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public setBtnPress(Z)V
    .locals 1
    .param p1, "pressed"    # Z

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setPressed(Z)V

    .line 1091
    return-void
.end method

.method public setDisableNextPrev()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 350
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d007b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    const v1, 0x7f0d0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 354
    :cond_0
    return-void
.end method

.method public setEnableNextPrev()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const v3, 0x7f0d007b

    const v2, 0x7f0d0079

    .line 328
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 331
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 332
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 333
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 334
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 338
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->isSupportNextPrev()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020005

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 340
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020008

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 345
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 348
    :cond_0
    return-void

    .line 342
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020004

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 343
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f020009

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1041
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mButtonEnable:Z

    if-ne v0, p1, :cond_0

    .line 1066
    :goto_0
    return-void

    .line 1045
    :cond_0
    const-string v0, "MiniVideoBtnController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setEnabled : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1047
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1048
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 1051
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 1052
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1054
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1057
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_4

    .line 1058
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPrevButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1060
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_5

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setEnabled(Z)V

    .line 1064
    :cond_5
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mButtonEnable:Z

    .line 1065
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setExitLayoutVisibility(IZ)V
    .locals 8
    .param p1, "visibility"    # I
    .param p2, "isAnim"    # Z

    .prologue
    const-wide/16 v6, 0x12c

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 568
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    if-eqz v2, :cond_1

    .line 569
    if-eqz p2, :cond_0

    .line 570
    if-nez p1, :cond_2

    .line 571
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 572
    .local v0, "anim_fadein":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 580
    .end local v0    # "anim_fadein":Landroid/view/animation/AlphaAnimation;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 582
    :cond_1
    return-void

    .line 575
    :cond_2
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 576
    .local v1, "anim_fadeout":Landroid/view/animation/AlphaAnimation;
    invoke-virtual {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mExitLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public setFocus()V
    .locals 1

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    .line 1095
    return-void
.end method

.method public setHoldLongSeek()V
    .locals 1

    .prologue
    .line 1109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed:Z

    .line 1110
    return-void
.end method

.method public setHoldLongSeek4SpeedChange()V
    .locals 1

    .prologue
    .line 1121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHoldLongSeekSpeed4SeekSpeedChange:Z

    .line 1122
    return-void
.end method

.method public setLongSeekMode(I)V
    .locals 0
    .param p1, "LongSeekMode"    # I

    .prologue
    .line 1098
    iput p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I

    .line 1099
    return-void
.end method

.method public setOnCommandListener(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    .prologue
    .line 1086
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    .line 1087
    return-void
.end method

.method public setProgress()I
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 594
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    if-nez v6, :cond_1

    move v4, v5

    .line 627
    :cond_0
    :goto_0
    return v4

    .line 598
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v4

    .line 599
    .local v4, "position":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getDuration()I

    move-result v0

    .line 601
    .local v0, "duration":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v6, :cond_5

    .line 602
    const/16 v6, 0x3e8

    if-le v0, v6, :cond_4

    .line 603
    const-wide/32 v6, 0x186a0

    int-to-long v8, v4

    mul-long/2addr v6, v8

    int-to-long v8, v0

    div-long v2, v6, v8

    .line 604
    .local v2, "pos":J
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->IsPauseEnable()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 605
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    long-to-int v7, v2

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 610
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getBufferPercentage()I

    move-result v1

    .line 611
    .local v1, "percent":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isRtsp()Z

    move-result v6

    if-nez v6, :cond_2

    .line 612
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    mul-int/lit16 v7, v1, 0x3e8

    invoke-virtual {v6, v7}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 622
    .end local v1    # "percent":I
    .end local v2    # "pos":J
    :cond_2
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isInPlaybackState()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 623
    if-gtz v0, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 624
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v6, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0

    .line 607
    .restart local v2    # "pos":J
    :cond_3
    const-string v6, "MiniVideoBtnController"

    const-string v7, "setProgress: mMiniPlayer.isPauseEnable() is false."

    invoke-static {v6, v7}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 615
    .end local v2    # "pos":J
    :cond_4
    const-string v6, "MiniVideoBtnController"

    const-string v7, "setProgress: duration is less than zero"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v6, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_2

    :cond_5
    move v4, v5

    .line 619
    goto :goto_0
.end method

.method public setProgressMax()V
    .locals 2

    .prologue
    .line 631
    const-string v0, "MiniVideoBtnController"

    const-string v1, "setProgressMax()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mProgressBar:Landroid/widget/ProgressBar;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 635
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 636
    return-void
.end method

.method protected setSpeedText(I)V
    .locals 6
    .param p1, "speed"    # I

    .prologue
    .line 562
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    const v3, 0x7f0a0123

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 565
    :cond_0
    return-void
.end method

.method protected setSpeedTextVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 556
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 557
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mSpeedLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 559
    :cond_0
    return-void
.end method

.method public setUpdate()V
    .locals 0

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updatePausePlayBtn()V

    .line 148
    return-void
.end method

.method public show(ZI)V
    .locals 7
    .param p1, "isAnim"    # Z
    .param p2, "timeout"    # I

    .prologue
    const/16 v6, 0x64

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 383
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    if-nez v1, :cond_1

    .line 384
    const v1, 0x7f0d0075

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 385
    const-string v1, "MiniVideoBtnController"

    const-string v2, "show : findViewById() is null view not inflate yet!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->showProgress(Z)V

    .line 391
    invoke-virtual {p0, v5, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setExitLayoutVisibility(IZ)V

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->updatePausePlayBtn()V

    .line 393
    if-eqz p1, :cond_2

    .line 394
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 395
    .local v0, "anim_fadein":Landroid/view/animation/AlphaAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 396
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->startAnimation(Landroid/view/animation/Animation;)V

    .line 398
    .end local v0    # "anim_fadein":Landroid/view/animation/AlphaAnimation;
    :cond_2
    invoke-virtual {p0, v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setVisibility(I)V

    .line 399
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    if-eqz v1, :cond_3

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniSubtitleView:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSubtitleView;->updateLayout()V

    .line 403
    :cond_3
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z

    .line 405
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 406
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 408
    const v1, 0x36ee80

    if-eq p2, v1, :cond_0

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public showProgress(Z)V
    .locals 1
    .param p1, "isAnim"    # Z

    .prologue
    .line 585
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgressBarVisibility(IZ)V

    .line 586
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgress()I

    .line 587
    return-void
.end method

.method public updateLayout(Z)V
    .locals 5
    .param p1, "isShrink"    # Z

    .prologue
    .line 1359
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1360
    .local v2, "lp_btn_playpause":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1363
    .local v1, "lp_btn_next":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz p1, :cond_0

    .line 1364
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080173

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 1368
    .local v0, "leftMarfgin":I
    :goto_0
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1369
    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1371
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1372
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mNextButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1373
    return-void

    .line 1366
    .end local v0    # "leftMarfgin":I
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080172

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .restart local v0    # "leftMarfgin":I
    goto :goto_0
.end method

.method public updatePausePlayBtn()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 660
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->IsPauseEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 663
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setEnableNextPrev()V

    .line 664
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setEnableProgressbar(Z)V

    .line 666
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 667
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setPlayPauseButton(I)V

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 672
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setEnableProgressbar(Z)V

    .line 673
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setDisableNextPrev()V

    .line 675
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 676
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0

    .line 678
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setPlayPauseButton(I)V

    goto :goto_0
.end method
