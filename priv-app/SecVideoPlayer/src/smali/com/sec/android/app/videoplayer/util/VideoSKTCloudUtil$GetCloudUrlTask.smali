.class Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;
.super Landroid/os/AsyncTask;
.source "VideoSKTCloudUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetCloudUrlTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GetCloudUrlTask"


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 763
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 761
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    .line 764
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    .line 765
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 759
    check-cast p1, [Landroid/net/Uri;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->doInBackground([Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p1, "uri"    # [Landroid/net/Uri;

    .prologue
    const/4 v5, 0x0

    .line 768
    if-nez p1, :cond_0

    .line 769
    const-string v2, "GetCloudUrlTask"

    const-string v3, "doInBackground() - uri is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const/4 v1, 0x0

    .line 783
    :goto_0
    return-object v1

    .line 773
    :cond_0
    const/4 v1, 0x0

    .line 774
    .local v1, "url":Ljava/lang/String;
    const-string v2, "GetCloudUrlTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doIn - uri is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, p1, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    :try_start_0
    # getter for: Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mCloudAgentExistence:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->access$100()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 778
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v2, v3}, Lcom/skp/tcloud/service/lib/TcloudStore$API;->getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 782
    :cond_1
    :goto_1
    const-string v2, "GetCloudUrlTask"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doIn - url is :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 779
    :catch_0
    move-exception v0

    .line 780
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 759
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$GetCloudUrlTask;->this$0:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->mOnUrlUpdatedListener:Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;->access$200(Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil;)Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/app/videoplayer/util/VideoSKTCloudUtil$OnUrlUpdatedSKTcloudListener;->onUpdated(Ljava/lang/String;)V

    .line 789
    :cond_0
    return-void
.end method
