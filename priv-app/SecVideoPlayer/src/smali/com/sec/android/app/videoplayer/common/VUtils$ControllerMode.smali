.class public final enum Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;
.super Ljava/lang/Enum;
.source "VUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/common/VUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControllerMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field public static final enum MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field public static final enum MULTIWINDOW_FULL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field public static final enum MULTIWINDOW_MINIMUM:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field public static final enum NORMAL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

.field public static final enum UNDEFINED:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1313
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1314
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->NORMAL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1315
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    const-string v1, "MINI_CONTROLLER"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1316
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    const-string v1, "MULTIWINDOW_FULL"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_FULL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1317
    new-instance v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    const-string v1, "MULTIWINDOW_MINIMUM"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_MINIMUM:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    .line 1312
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->UNDEFINED:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->NORMAL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MINI_CONTROLLER:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_FULL:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->MULTIWINDOW_MINIMUM:Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->$VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1312
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1312
    const-class v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;
    .locals 1

    .prologue
    .line 1312
    sget-object v0, Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->$VALUES:[Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    invoke-virtual {v0}, [Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/app/videoplayer/common/VUtils$ControllerMode;

    return-object v0
.end method
