.class Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;
.super Landroid/os/Handler;
.source "AsfUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V
    .locals 0

    .prologue
    .line 1058
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x1f6

    const/16 v7, 0x1f4

    const/4 v5, 0x0

    const/16 v4, 0x1f7

    const/4 v6, 0x1

    .line 1060
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 1111
    :goto_0
    :pswitch_0
    return-void

    .line 1062
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 1064
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    iget-boolean v2, v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->bWaitStateListener:Z

    if-eqz v2, :cond_0

    .line 1065
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    iput-boolean v5, v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->bWaitStateListener:Z

    .line 1066
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # setter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mPlayerState:I
    invoke-static {v2, v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1302(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;I)I

    .line 1069
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getPlayerState()I

    move-result v2

    if-ne v2, v6, :cond_2

    .line 1070
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1072
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v0

    .line 1073
    .local v0, "pos":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    const-wide/16 v0, 0x0

    .line 1075
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaAllShare(JZ)V
    invoke-static {v2, v0, v1, v5}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;JZ)V

    goto :goto_0

    .line 1077
    .end local v0    # "pos":J
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    iput-boolean v6, v2, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->bWaitStateListener:Z

    .line 1078
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->stop()V

    .line 1079
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1084
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1085
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mUseASF:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1086
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 1088
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->connectAllShareFwk()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$600(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)V

    .line 1089
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1094
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1095
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmrUdn:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1096
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mHandler - START_DIRECT_DMC success!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mDmrUdn:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->setSelectedDevice(Ljava/lang/String;)Z
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2300(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1098
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getResumePosition()J

    move-result-wide v4

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->playViaAllShare(JZ)V
    invoke-static {v2, v4, v5, v6}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2100(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;JZ)V

    goto/16 :goto_0

    .line 1100
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$2000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1103
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    # getter for: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mHandler - START_DIRECT_DMC DRMUDN is null!"

    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1104
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil$6;->this$0:Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    const/16 v3, 0x2eb

    const/16 v4, 0x2c2

    # invokes: Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->notifyChanged(II)V
    invoke-static {v2, v3, v4}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->access$1200(Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;II)V

    goto/16 :goto_0

    .line 1060
    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
