.class Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;
.super Landroid/os/Handler;
.source "PreviewDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/preview/PreviewDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field index:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v2, p1, Landroid/os/Message;->arg1:I

    if-gt v0, v2, :cond_1

    .line 420
    :cond_0
    :goto_0
    return-void

    .line 385
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    .line 387
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$200(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHandler. msg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", idx: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", arg: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 390
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/videoplayer/preview/Preview;

    .line 391
    .local v6, "pv":Lcom/sec/android/app/videoplayer/preview/Preview;
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/preview/Preview;->getProgressBar()Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 392
    iget v0, p1, Landroid/os/Message;->arg2:I

    if-nez v0, :cond_2

    .line 393
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/preview/Preview;->getChapterView()Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setVisibility(I)V

    goto :goto_0

    .line 394
    :cond_2
    iget v0, p1, Landroid/os/Message;->arg2:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 395
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/preview/Preview;->getNoChapterImage()Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 400
    .end local v6    # "pv":Lcom/sec/android/app/videoplayer/preview/Preview;
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/Preview;->getChapterTime()Landroid/widget/TextView;

    move-result-object v8

    .line 402
    .local v8, "tv":Landroid/widget/TextView;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    iget v2, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->getChapterTimeView(I)Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$300(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;I)Ljava/lang/String;

    move-result-object v7

    .line 404
    .local v7, "time":Ljava/lang/String;
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$400(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2, v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->getTimeTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 409
    .end local v7    # "time":Ljava/lang/String;
    .end local v8    # "tv":Landroid/widget/TextView;
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/Preview;->getChapterView()Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    sget v4, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    sget v5, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Fps:I

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapView(IIII)V

    goto/16 :goto_0

    .line 413
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mBitmap:Ljava/util/WeakHashMap;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$500(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/WeakHashMap;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 414
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->this$0:Lcom/sec/android/app/videoplayer/preview/PreviewDialog;

    # getter for: Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->mPreview:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/preview/PreviewDialog;->access$100(Lcom/sec/android/app/videoplayer/preview/PreviewDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/preview/Preview;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/preview/Preview;->getChapterView()Lcom/sec/android/app/videoplayer/videowall/ChapterView;

    move-result-object v0

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/PreviewDialog$3;->index:I

    sget v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    sget v4, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    sget v5, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Fps:I

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/videowall/ChapterView;->setBitmapImage(Landroid/graphics/Bitmap;IIII)V

    goto/16 :goto_0

    .line 388
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
