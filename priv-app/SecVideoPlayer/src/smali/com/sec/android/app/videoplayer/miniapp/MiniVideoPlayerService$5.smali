.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/16 v4, 0xa

    .line 1090
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mSPM:Lcom/sec/android/app/videoplayer/db/SharedPreference;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v2

    const-string v3, "sound_effect"

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadIntKey(Ljava/lang/String;I)I

    move-result v1

    .line 1093
    .local v1, "saved":I
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 1094
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1095
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    .line 1106
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mAudioTrack:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setAudioTrack(I)V

    .line 1108
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_2

    .line 1109
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mPlaySpeed:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$1500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setPlaySpeed(I)V

    .line 1111
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setSubtitleSyncTime()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$2200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    .line 1112
    return-void

    .line 1096
    :cond_3
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1097
    const/16 v2, 0xb

    if-le v1, v2, :cond_4

    .line 1098
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    goto :goto_0

    .line 1100
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    goto :goto_0

    .line 1103
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSoundAliveMode(I)V

    goto :goto_0
.end method
