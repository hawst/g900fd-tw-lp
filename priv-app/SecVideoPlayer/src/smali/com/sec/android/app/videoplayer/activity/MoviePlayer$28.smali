.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 4577
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4579
    if-eqz p2, :cond_0

    .line 4580
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 4581
    .local v4, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver - action : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mVideoActivityOnResume:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-nez v18, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-nez v18, :cond_1

    .line 4879
    .end local v4    # "action":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 4585
    .restart local v4    # "action":Ljava/lang/String;
    :cond_1
    const-string v18, "android.intent.action.BATTERY_CHANGED"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 4586
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver - ACTION_BATTERY_CHANGED."

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const-string v19, "status"

    const/16 v20, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4602(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 4588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const-string v19, "scale"

    const/16 v20, 0x64

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4702(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 4589
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const-string v19, "level"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v20

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4802(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)I

    .line 4591
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver. battScale : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattScale:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", battLevel : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", battStatus : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4593
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v18

    const/16 v20, 0x5

    move/from16 v0, v18

    move/from16 v1, v20

    if-gt v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v18

    const/16 v20, 0x2

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_2

    const/16 v18, 0x1

    :goto_1
    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->setIsCriticalLowBatteryStatus(Landroid/content/Context;Z)V

    .line 4595
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattLevel:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-gt v0, v1, :cond_0

    .line 4596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mBattStatus:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 4597
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-result-object v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->isShowing()Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->dismiss()V

    .line 4599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->resumeOrStartPlaying()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4593
    :cond_2
    const/16 v18, 0x0

    goto :goto_1

    .line 4602
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-result-object v18

    if-nez v18, :cond_4

    .line 4603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v20, v0

    invoke-direct/range {v19 .. v20}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4902(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    .line 4606
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLowBtteryPopup:Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4900(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoLowBatteryPopup;->show()V

    goto/16 :goto_0

    .line 4610
    :cond_5
    const-string v18, "android.intent.action.HEADSET_PLUG"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_11

    .line 4611
    const-string v18, "state"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    const/4 v11, 0x1

    .line 4612
    .local v11, "isHeadsetPlugged":Z
    :goto_2
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver - receive ACTION_HEADSET_PLUG - isHeadsetPlugged : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/Menu;

    move-result-object v18

    if-eqz v18, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/Menu;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v18

    if-eqz v18, :cond_6

    .line 4615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMenu:Landroid/view/Menu;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/view/Menu;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Landroid/view/Menu;->close()V

    .line 4618
    :cond_6
    if-eqz v11, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v18

    if-nez v18, :cond_8

    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v18

    if-nez v18, :cond_8

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityStopState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3400()Z

    move-result v18

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 4620
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0011

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4621
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4611
    .end local v11    # "isHeadsetPlugged":Z
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 4623
    .restart local v11    # "isHeadsetPlugged":Z
    :cond_8
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v6

    .line 4625
    .local v6, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    if-nez v11, :cond_10

    .line 4626
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v18

    if-nez v18, :cond_9

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isHDMIConnected()Z

    move-result v18

    if-nez v18, :cond_9

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isExtraSpeakerDockOn()Z

    move-result v18

    if-eqz v18, :cond_f

    .line 4628
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 4634
    :cond_a
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->invalidateOptionsMenu()V

    .line 4636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    move-result-object v18

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->getID()I

    move-result v18

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 4637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsSubPopup:Lcom/sec/android/app/videoplayer/popup/IVideoPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/IVideoPopup;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/IVideoPopup;->dismiss()V

    .line 4640
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    move-result-object v18

    if-eqz v18, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->isPopupShowing()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 4641
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->setDismiss4UnpluggedHeadset()V

    .line 4642
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSettingsPopup:Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5100(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/VideoSettingsPopup;->dismiss()V

    .line 4652
    :cond_c
    :goto_4
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5200()Z

    move-result v18

    if-nez v18, :cond_d

    .line 4654
    const/16 v18, 0x32

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sleep(I)V

    .line 4657
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-object/from16 v18, v0

    if-eqz v18, :cond_d

    .line 4658
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->showPopupVolbar(Z)V

    .line 4659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeSeekbarLevel()V

    .line 4660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4667
    :cond_d
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    .line 4668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setAudioShockWarningEnabled()V

    .line 4671
    :cond_e
    const/16 v18, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mInitialHeadsetAction:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5202(Z)Z

    goto/16 :goto_0

    .line 4629
    :cond_f
    sget-boolean v18, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTI_SPEAKER:Z

    if-nez v18, :cond_a

    .line 4630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getSAEffectMode()I

    move-result v18

    const/16 v19, 0xb

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_a

    .line 4631
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    goto/16 :goto_3

    .line 4646
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->invalidateOptionsMenu()V

    .line 4648
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v17

    .line 4649
    .local v17, "volume":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setMovieVolume(F)V

    goto/16 :goto_4

    .line 4662
    .end local v17    # "volume":F
    :catch_0
    move-exception v7

    .line 4663
    .local v7, "e":Ljava/lang/RuntimeException;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver - RuntimeException"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    .line 4673
    .end local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .end local v7    # "e":Ljava/lang/RuntimeException;
    .end local v11    # "isHeadsetPlugged":Z
    :cond_11
    const-string v18, "android.media.AUDIO_BECOMING_NOISY"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_17

    .line 4674
    const-string v18, "android.bluetooth.a2dp.extra.DISCONNECT_A2DP"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 4675
    .local v8, "fromBT":Z
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver - receive ACTION_AUDIO_BECOMING_NOISY. fromBT : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4677
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v6

    .line 4678
    .restart local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v18

    if-eqz v18, :cond_12

    .line 4679
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver - bluetooth is connected."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 4683
    :cond_12
    if-eqz v8, :cond_13

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v18

    if-nez v18, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-nez v18, :cond_14

    .line 4685
    :cond_13
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->isRmsConnected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_14

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v18

    if-eqz v18, :cond_15

    .line 4700
    :cond_14
    :goto_6
    if-eqz v8, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4701
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isTransferToDeviceSelected:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5302(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 4702
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver - init isTransferToDeviceSelected flag"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4688
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4689
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->checkIsCalling(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_16

    .line 4690
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 4692
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v18

    if-eqz v18, :cond_14

    .line 4693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 4694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 4695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    goto/16 :goto_6

    .line 4704
    .end local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .end local v8    # "fromBT":Z
    :cond_17
    const-string v18, "android.intent.action.USB_ANLG_HEADSET_PLUG"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_18

    .line 4705
    const-string v18, "state"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 4706
    .local v12, "isLineOutHeadset":I
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver. USB_ANLG_HEADSET_PLUG isLineOutHeadset state: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4708
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v12, v0, :cond_0

    .line 4709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    goto/16 :goto_0

    .line 4711
    .end local v12    # "isLineOutHeadset":I
    :cond_18
    const-string v18, "com.sec.android.sidesync.source.SIDESYNC_CHANGE_SINK_WORK"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_19

    .line 4713
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "Exit MoviePlayer for connecting SideSync"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4714
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sendSideSyncDisableVideoBroadcast()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4716
    :cond_19
    sget-boolean v18, Lcom/sec/android/app/videoplayer/common/feature/Feature;->WIFI_DISPLAY:Z

    if-eqz v18, :cond_1b

    sget-object v18, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_WIFI_DISPLAY:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1b

    .line 4717
    const-string v18, "state"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1a

    const/4 v13, 0x1

    .line 4718
    .local v13, "isWFDConnected":Z
    :goto_7
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ACTION_WFD_CONNECTED isWFDConnected: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4720
    if-nez v13, :cond_0

    .line 4721
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 4723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    goto/16 :goto_0

    .line 4717
    .end local v13    # "isWFDConnected":Z
    :cond_1a
    const/4 v13, 0x0

    goto :goto_7

    .line 4726
    :cond_1b
    const-string v18, "android.intent.action.HDMI_PLUGGED"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1d

    .line 4727
    const-string v18, "state"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 4728
    .local v10, "isHDMIPlugged":Z
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver - ACTION_HDMI_PLUG."

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4729
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ACTION_HDMI_PLUG. HDMI plug is = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4731
    if-eqz v10, :cond_1c

    .line 4732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    .line 4735
    :cond_1c
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->dismiss()V

    goto/16 :goto_0

    .line 4736
    .end local v10    # "isHDMIPlugged":Z
    :cond_1d
    const-string v18, "android.media.action.HDMI_AUDIO_PLUG"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1f

    .line 4737
    const-string v18, "state"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1e

    const/4 v9, 0x1

    .line 4738
    .local v9, "isHDMIAudioPlugged":Z
    :goto_8
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver - ACTION_HDMI_AUDIO_PLUG. plug is = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4740
    if-eqz v9, :cond_0

    .line 4741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    .line 4742
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getImplicitVideoVolume()F

    move-result v17

    .line 4743
    .restart local v17    # "volume":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setMovieVolume(F)V

    .line 4744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v18

    const/16 v19, 0xa

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->setSAEffectMode(I)V

    goto/16 :goto_0

    .line 4737
    .end local v9    # "isHDMIAudioPlugged":Z
    .end local v17    # "volume":F
    :cond_1e
    const/4 v9, 0x0

    goto :goto_8

    .line 4747
    :cond_1f
    const-string v18, "android.intent.action.PALM_DOWN"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_23

    .line 4748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpSmartPause()Z

    move-result v18

    if-nez v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSchemeType:Lcom/sec/android/app/videoplayer/type/SchemeType;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/type/SchemeType;->isFromHelpProgressBarPreview()Z

    move-result v18

    if-nez v18, :cond_0

    .line 4752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_20

    .line 4753
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v6

    .line 4755
    .restart local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v18

    if-nez v18, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPlaying()Z

    move-result v18

    if-eqz v18, :cond_22

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isWiredConnected()Z

    move-result v18

    if-nez v18, :cond_22

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->isAudioPathBT()Z

    move-result v18

    if-nez v18, :cond_22

    .line 4756
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const-string v19, "com.sec.android.app.videoplayer"

    const-string v20, "VPLD"

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/videoplayer/common/VUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 4757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pause()V

    .line 4758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    .line 4763
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 4764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSmartPauseMgr:Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5500(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/manager/VideoSmartFaceMgr;->blockSmartPause()V

    .line 4767
    .end local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v18, v0

    if-eqz v18, :cond_21

    .line 4768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    .line 4771
    :cond_21
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->setRemoveSystemUI(Z)V

    goto/16 :goto_0

    .line 4760
    .restart local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    goto :goto_9

    .line 4772
    .end local v6    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    :cond_23
    const-string v18, "android.intent.action.PALM_UP"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_24

    .line 4773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPlayingBeforePalm(Z)V

    goto/16 :goto_0

    .line 4774
    :cond_24
    const-string v18, "AppInAppResumePositionReply"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_25

    .line 4775
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. APP_IN_APP_RESUME_POS_REPLY"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const-string v19, "resumePosByAIA"

    const/16 v20, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setResumePosition(J)V

    goto/16 :goto_0

    .line 4777
    :cond_25
    const-string v18, "android.intent.action.USER_PRESENT"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2c

    .line 4778
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0x1a

    const/16 v21, 0x1

    invoke-virtual/range {v18 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 4779
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v18

    if-eqz v18, :cond_26

    .line 4780
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0x3

    const/16 v21, 0x1

    invoke-virtual/range {v18 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 4781
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    const/16 v20, 0xbb

    const/16 v21, 0x1

    invoke-virtual/range {v18 .. v21}, Lcom/sec/android/app/videoplayer/common/VUtils;->requestSystemKeyEvent(Landroid/content/Context;IZ)Z

    .line 4784
    :cond_26
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isActivityPauseState()Z

    move-result v18

    if-nez v18, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v18

    sget-object v19, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    if-eqz v18, :cond_27

    .line 4785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resetPausedByUserFlag()V

    .line 4786
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getIntent()Landroid/content/Intent;

    move-result-object v18

    sget-object v19, Lcom/sec/android/app/videoplayer/common/feature/Vintent;->ACTION_LAUNCH_BY_NOTIFICATION:Ljava/lang/String;

    invoke-virtual/range {v18 .. v19}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 4789
    :cond_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v18

    if-nez v18, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isPausedByUser()Z

    move-result v18

    if-eqz v18, :cond_29

    :cond_28
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isFromGallerySecureLock()Z

    move-result v18

    if-eqz v18, :cond_2b

    .line 4791
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_2a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isShowingPresentation()Z

    move-result v18

    if-nez v18, :cond_2a

    .line 4792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->ShowPresentation()V

    .line 4794
    :cond_2a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->startPlayback()V

    .line 4797
    :cond_2b
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->registerRemoteControlReceiver()V

    goto/16 :goto_0

    .line 4798
    :cond_2c
    const-string v18, "android.media.IMediaPlayer.videoexist"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_2d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_2d

    .line 4799
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3902(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 4800
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. IMEDIA_PLAYER_VIDEO_EXIST"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4801
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0136

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4803
    :cond_2d
    const-string v18, "intent.stop.app-in-app"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_31

    .line 4804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_2e

    .line 4805
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. INTENT_STOP_APP_IN_APP presentation exitApp"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4806
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0011

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4807
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4808
    :cond_2e
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VUtils;->isScaleWindow()Z

    move-result v18

    if-eqz v18, :cond_2f

    .line 4809
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. INTENT_STOP_APP_IN_APP isScaleWindow exitApp"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4810
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0011

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4811
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4812
    :cond_2f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSymmetricMode()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5600(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-nez v18, :cond_30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isSourceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5700(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4813
    :cond_30
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. INTENT_STOP_APP_IN_APP presentation exitApp"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4814
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0011

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4815
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4817
    :cond_31
    const-string v18, "com.sec.android.app.PRESENTATION_FOCUS_CHANGED"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_37

    .line 4818
    const-string v18, "app_name"

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 4819
    .local v5, "appName":Ljava/lang/String;
    const-string v18, "launch_home"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    .line 4820
    .local v14, "launchHome":Z
    const-string v18, "user_id"

    const/16 v19, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    .line 4821
    .local v16, "userId":I
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver. ACTION_PRESENTATION_FOCUS_CHANGED : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , launchHome : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " , userId : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->isInitialized()Z

    move-result v18

    if-nez v18, :cond_32

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v18

    move/from16 v0, v16

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    .line 4825
    :cond_32
    const-string v18, "video"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_33

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v18

    move/from16 v0, v16

    move/from16 v1, v18

    if-ne v0, v1, :cond_33

    .line 4826
    const/16 v18, 0x1

    sput-boolean v18, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->bHasPresentaionFocus:Z

    goto/16 :goto_0

    .line 4828
    :cond_33
    const-string v18, "video"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_34

    .line 4829
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    const v19, 0x7f0a0011

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    .line 4832
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    if-eqz v18, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setHasAudioFocus()V

    .line 4833
    :cond_35
    const/16 v18, 0x0

    sput-boolean v18, Lcom/sec/android/app/videoplayer/util/wfd/IWfdUtil;->bHasPresentaionFocus:Z

    .line 4834
    if-eqz v14, :cond_36

    .line 4835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mShouldBackToList:Z
    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3902(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 4836
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v19, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v19 .. v19}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/VUtils;->launchHome(Landroid/content/Context;)V

    .line 4838
    :cond_36
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto/16 :goto_0

    .line 4840
    .end local v5    # "appName":Ljava/lang/String;
    .end local v14    # "launchHome":Z
    .end local v16    # "userId":I
    :cond_37
    const-string v18, "android.intent.action.SCREEN_OFF"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_38

    const-string v18, "android.intent.action.ACTION_SHUTDOWN"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3b

    .line 4841
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4842
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    const-string v19, "mStatusReceiver. ACTION_SCREEN_OFF in PresentationMode"

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isVideoExtensionState()Z

    move-result v18

    if-nez v18, :cond_39

    sget-boolean v18, Lcom/sec/android/app/videoplayer/common/feature/Feature;->MULTIWINDOW:Z

    if-eqz v18, :cond_3a

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMultiWindow()Z

    move-result v18

    if-eqz v18, :cond_3a

    .line 4844
    :cond_39
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v18

    if-nez v18, :cond_0

    .line 4845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 4846
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 4847
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 4848
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 4849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    .line 4850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    goto/16 :goto_0

    .line 4853
    :cond_3a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->setPausedByUser()V

    .line 4855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    goto/16 :goto_0

    .line 4858
    :cond_3b
    const-string v18, "com.sec.android.app.videoplayer.SORT_BY"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_3c

    .line 4859
    const-string v18, "sortby"

    const/16 v19, 0x2

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 4860
    .local v15, "sortOrder":I
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "mStatusReceiver - receive ACTION_SORT_BY. sortOrder : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateSortByInfo(I)V
    invoke-static {v0, v15}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$5800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;I)V

    goto/16 :goto_0

    .line 4862
    .end local v15    # "sortOrder":I
    :cond_3c
    const-string v18, "android.intent.action.CONFIGURATION_CHANGED"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4863
    sget-boolean v18, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_DUAL_LCD:Z

    if-eqz v18, :cond_0

    sget-boolean v18, Lcom/sec/android/app/videoplayer/common/feature/Feature;->IS_FOLDER_TYPE:Z

    if-eqz v18, :cond_0

    .line 4864
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$1000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    const-string v19, "sub_lcd_auto_lock"

    const/16 v20, 0x0

    invoke-static/range {v18 .. v20}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    if-eqz v18, :cond_0

    .line 4865
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsActivityPauseState:Z
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3600()Z

    move-result v18

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->isPresentationServiceRunning()Z
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$800(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 4866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->pauseOrStopPlaying()V
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$3000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 4867
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->updateNotification(Z)V

    .line 4869
    sget-boolean v18, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mIsLeaveByUser:Z

    if-eqz v18, :cond_0

    .line 4870
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 4871
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 4872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$28;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    goto/16 :goto_0
.end method
