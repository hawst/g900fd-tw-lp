.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;
.super Ljava/lang/Object;
.source "SubtitleColorPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->createPopup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mOpacity:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$000(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setOpacity(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$100(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mColor:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)I

    move-result v1

    # invokes: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->setColor(I)V
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$300(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;I)V

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->mParentDialog:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->access$400(Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;)Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSettingPopup;->refreshSubtitleMenu()V

    .line 146
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup$1;->this$0:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorPopup;->hide()V

    .line 147
    return-void
.end method
