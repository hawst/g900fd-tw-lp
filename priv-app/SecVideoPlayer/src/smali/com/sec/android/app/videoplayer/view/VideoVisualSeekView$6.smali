.class Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$6;
.super Ljava/lang/Object;
.source "VideoVisualSeekView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->ShowVideo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$6;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1, "player"    # Landroid/media/MediaPlayer;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x0

    .line 151
    # getter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnErrorListener "

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->reset()V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView$6;->this$0:Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->mIsInitialized:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;->access$502(Lcom/sec/android/app/videoplayer/view/VideoVisualSeekView;Z)Z

    .line 156
    return v2
.end method
