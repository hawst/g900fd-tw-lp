.class Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;
.super Ljava/lang/Object;
.source "VideoFlickView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->onAnimationEnd(Landroid/animation/Animator;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 174
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$800(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->frameAnimationright:Landroid/graphics/drawable/AnimationDrawable;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$900(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 164
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mRightFlickCounter:I
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$602(Lcom/sec/android/app/videoplayer/view/VideoFlickView;I)I

    .line 165
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->is2ndScreenMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2$1;->this$1:Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/view/VideoFlickView$2;->this$0:Lcom/sec/android/app/videoplayer/view/VideoFlickView;

    # getter for: Lcom/sec/android/app/videoplayer/view/VideoFlickView;->mParentView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/view/VideoFlickView;->access$000(Lcom/sec/android/app/videoplayer/view/VideoFlickView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getAdditionalViewInstance()Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/VideoAdditionalView;->getRootView()Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 172
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 160
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "arg0"    # Landroid/animation/Animator;

    .prologue
    .line 176
    return-void
.end method
