.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 4898
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 4900
    if-nez p2, :cond_1

    .line 4910
    :cond_0
    :goto_0
    return-void

    .line 4902
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 4903
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mVolumeChangeReceiver. intent.getAction() = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4905
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4906
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    if-eqz v1, :cond_0

    .line 4907
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$29;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->updateVolume()V

    goto :goto_0
.end method
