.class public Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
.super Landroid/opengl/GLSurfaceView;
.source "VideoSurfaceGL.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VideoSurfaceGL"

.field private static mSurfaceCount:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

.field private mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

.field private mSurfaceExists:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 31
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mContext:Landroid/content/Context;

    .line 32
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->initVideoView()V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    .line 26
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 37
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mContext:Landroid/content/Context;

    .line 38
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->initVideoView()V

    .line 39
    return-void
.end method

.method private initVideoView()V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 140
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 142
    .local v0, "surfacelp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 143
    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-nez v1, :cond_0

    .line 148
    new-instance v1, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    .line 150
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setEGLContextClientVersion(I)V

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 152
    return-void
.end method


# virtual methods
.method public getMeshShape()I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getMeshShape()I

    move-result v0

    .line 236
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSurfaceExists()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    return v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method public getSurfaceTexture()Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v8, 0x78

    .line 42
    const/4 v2, 0x1

    .line 43
    .local v2, "surfaceWidth":I
    const/4 v1, 0x1

    .line 45
    .local v1, "surfaceHeight":I
    const/4 v4, 0x0

    .line 46
    .local v4, "videoWidth":I
    const/4 v3, 0x0

    .line 48
    .local v3, "videoHeight":I
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v5, :cond_0

    .line 49
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoWidth()I

    move-result v4

    .line 50
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v5, v5, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getVideoHeight()I

    move-result v3

    .line 53
    :cond_0
    if-nez v4, :cond_1

    if-nez v3, :cond_1

    .line 54
    const-string v5, "VideoSurfaceGL"

    const-string v6, "onMeasure() - videoWidth is 0 && videoHeight is 0 "

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v5, "VideoSurfaceGL"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - caculated size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, v2, v1}, Landroid/opengl/GLSurfaceView;->setMeasuredDimension(II)V

    .line 111
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual {p0, v4, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->resolveAdjustedSize(II)I

    move-result v2

    .line 60
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->resolveAdjustedSize(II)I

    move-result v1

    .line 63
    const-string v5, "VideoSurfaceGL"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - param size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v5, "VideoSurfaceGL"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - real size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " x "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v5, "VideoSurfaceGL"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - surfaceWidth : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", surfaceHeight : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    if-lez v4, :cond_2

    if-lez v3, :cond_2

    .line 67
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->get(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/common/VideoSettingInfo;->getScreenMode()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 109
    :cond_2
    :goto_1
    const-string v5, "VideoSurfaceGL"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onMeasure() - caculated size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-super {p0, v2, v1}, Landroid/opengl/GLSurfaceView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    .line 69
    :pswitch_0
    const-string v5, "VideoSurfaceGL"

    const-string v6, "onMeasure() - VideoServiceUtil.KEEP_ASPECT_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    mul-int v5, v4, v1

    mul-int v6, v3, v2

    if-lt v5, v6, :cond_3

    .line 71
    mul-int v5, v2, v3

    div-int v1, v5, v4

    goto :goto_1

    .line 73
    :cond_3
    mul-int v5, v1, v4

    div-int v2, v5, v3

    .line 74
    goto :goto_1

    .line 77
    :pswitch_1
    if-lez v4, :cond_2

    if-lez v3, :cond_2

    .line 78
    const-string v5, "VideoSurfaceGL"

    const-string v6, "onMeasure() - VideoServiceUtil.ORIGINAL_SIZE"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    if-gt v4, v2, :cond_4

    if-le v3, v1, :cond_6

    .line 80
    :cond_4
    mul-int v5, v4, v1

    mul-int v6, v3, v2

    if-lt v5, v6, :cond_5

    .line 81
    mul-int v5, v2, v3

    div-int v1, v5, v4

    goto :goto_1

    .line 83
    :cond_5
    mul-int v5, v1, v4

    div-int v2, v5, v3

    goto :goto_1

    .line 85
    :cond_6
    move v2, v4

    .line 86
    move v1, v3

    goto :goto_1

    .line 92
    :pswitch_2
    const-string v5, "VideoSurfaceGL"

    const-string v6, "onMeasure() - VideoServiceUtil.FIT_TO_HEIGHT_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->resolveAdjustedSize(II)I

    move-result v0

    .line 94
    .local v0, "screenHeight":I
    mul-int v5, v0, v4

    div-int v2, v5, v3

    .line 95
    move v1, v0

    .line 96
    goto :goto_1

    .line 99
    .end local v0    # "screenHeight":I
    :pswitch_3
    const-string v5, "VideoSurfaceGL"

    const-string v6, "onMeasure() - VideoServiceUtil.FULL_SCREEN_RATIO"

    invoke-static {v5, v6}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0, v4, p1}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->resolveAdjustedSize(II)I

    move-result v2

    .line 101
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->resolveAdjustedSize(II)I

    move-result v1

    .line 102
    goto :goto_1

    .line 67
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public resolveAdjustedSize(II)I
    .locals 3
    .param p1, "desiredSize"    # I
    .param p2, "measureSpec"    # I

    .prologue
    .line 114
    move v0, p1

    .line 116
    .local v0, "result":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 117
    .local v1, "specMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 119
    .local v2, "specSize":I
    sparse-switch v1, :sswitch_data_0

    .line 136
    :goto_0
    return v0

    .line 121
    :sswitch_0
    move v0, p1

    .line 122
    goto :goto_0

    .line 125
    :sswitch_1
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 126
    goto :goto_0

    .line 129
    :sswitch_2
    move v0, v2

    .line 130
    goto :goto_0

    .line 119
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method public segSurfaceExists(Z)V
    .locals 0
    .param p1, "SurfaceExists"    # Z

    .prologue
    .line 167
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    .line 168
    return-void
.end method

.method public setMeshShape(I)V
    .locals 1
    .param p1, "shapehint"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->setMeshShape(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public setSurfaceHolder(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "surfaceHolder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 160
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "w"    # I
    .param p4, "h"    # I

    .prologue
    .line 172
    invoke-super {p0, p1, p2, p3, p4}, Landroid/opengl/GLSurfaceView;->surfaceChanged(Landroid/view/SurfaceHolder;III)V

    .line 174
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 178
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 179
    const-string v0, "VideoSurfaceGL"

    const-string v1, ">>>>>>>>surfaceCreated<<<<<<<<<<<<"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 181
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    .line 183
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 188
    const-string v0, "VideoSurfaceGL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>>>>>>surfaceDestroyed<<<<<<<<<<<<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 191
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceExists:Z

    .line 192
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    .line 194
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->release()V

    .line 198
    :cond_0
    sget v0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mSurfaceCount:I

    if-lez v0, :cond_2

    .line 205
    :cond_1
    :goto_0
    return-void

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mMainActivity:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->reset()V

    goto :goto_0
.end method

.method public toggleMeshShape()V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->toggleMeshShape()V

    .line 224
    :cond_0
    return-void
.end method

.method public touchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->mRenderer:Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/Video360/Video360Renderer;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 218
    :cond_0
    return-void
.end method
