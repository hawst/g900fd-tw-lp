.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;
.super Landroid/content/BroadcastReceiver;
.source "MoviePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->registerLocalReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    .line 537
    if-nez p2, :cond_1

    .line 593
    :cond_0
    :goto_0
    return-void

    .line 539
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 540
    .local v1, "action":Ljava/lang/String;
    const-string v6, "caller"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 542
    .local v2, "caller":Ljava/lang/String;
    const-string v6, "videoplayer.exit"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 543
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->exitApp()V
    invoke-static {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$200(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    goto :goto_0

    .line 544
    :cond_2
    const-string v6, "videoplayer.set.lock"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 545
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->changeLockStatus(Z)V
    invoke-static {v6, v9}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)V

    goto :goto_0

    .line 546
    :cond_3
    const-string v6, "videoplayer.pauseby"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 547
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->pauseBy(Ljava/lang/Object;)V

    goto :goto_0

    .line 548
    :cond_4
    const-string v6, "videoplayer.resumeby"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 549
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->resumeBy(Ljava/lang/Object;)V

    goto :goto_0

    .line 550
    :cond_5
    const-string v6, "videoplayer.set.controller.play.stop"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 551
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->setControllerPlayerStop()V

    goto :goto_0

    .line 552
    :cond_6
    const-string v6, "videoplayer.update.title"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 553
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->updateTitle()V

    goto :goto_0

    .line 554
    :cond_7
    const-string v6, "videoplayer.show.error.popup"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 555
    const-string v6, "popup.type"

    const/4 v7, -0x1

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 557
    .local v5, "type":I
    const/16 v6, -0x3f2

    if-ne v5, v6, :cond_0

    .line 558
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V
    invoke-static {v6, v5, v7}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$400(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 560
    .end local v5    # "type":I
    :cond_8
    const-string v6, "videoplayer.swipe"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 561
    const-string v6, "SwipeSeekValue"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 562
    .local v0, "SwipeSeekValue":I
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SwipeSeekValue is : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v3

    .line 564
    .local v3, "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-virtual {v3}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getCurrentPosition()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v3, v6, v9}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(II)V

    goto/16 :goto_0

    .line 565
    .end local v0    # "SwipeSeekValue":I
    .end local v3    # "serviceUtil":Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    :cond_9
    const-string v6, "videoplayer.dismiss.presentation"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 566
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6, v9, v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveResumePosition(ZZ)V

    .line 567
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->saveCurrentResumePosition()V

    .line 568
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->stop()V

    .line 569
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->DismissPresentation()V

    goto/16 :goto_0

    .line 570
    :cond_a
    const-string v6, "videoplayer.show.controller.smartpause"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 571
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_0

    .line 572
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController()V

    goto/16 :goto_0

    .line 579
    :cond_b
    const-string v6, "videoplayer.stateview.visibility"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 580
    const-string v6, "visibility"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 582
    .local v4, "show":Ljava/lang/String;
    const-string v6, "show"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 584
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v6

    if-nez v6, :cond_0

    .line 585
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showStateView()V

    goto/16 :goto_0

    .line 588
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 589
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$2;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v6, v6, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v6}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideStateView()V

    goto/16 :goto_0
.end method
