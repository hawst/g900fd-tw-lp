.class public Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;
.super Ljava/lang/Object;
.source "AsfObserver.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static sAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->sAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-class v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->TAG:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    .line 23
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private checkStateErrorPopup(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 61
    const/16 v0, 0x2db

    if-eq p1, v0, :cond_0

    const/16 v0, 0x2ee

    if-eq p1, v0, :cond_0

    .line 62
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->sAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    return-object v0
.end method

.method private onAllShareStateChanged(II)V
    .locals 5
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    const/4 v4, 0x6

    .line 67
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAllShareStateChanged() - key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 72
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OnAllShareStateChanged. VIDEO_ALLSHARE_STATE_CHANGED : mPlayerState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->mState:[Ljava/lang/String;

    aget-object v3, v3, p2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2e5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 76
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->isSupportDynamicBuffering()Z

    move-result v1

    if-eqz v1, :cond_2

    if-ne p2, v4, :cond_2

    .line 77
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    instance-of v1, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    if-eqz v1, :cond_2

    .line 78
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->TAG:Ljava/lang/String;

    const-string v2, "onAllShareStateChanged. AsfDynamicBufferingPopup shows. so AsfProgress doesn\'t show."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 85
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    instance-of v1, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    if-eqz v1, :cond_3

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 87
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->checkStateErrorPopup(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 93
    :cond_3
    if-eq p2, v4, :cond_4

    const/4 v1, 0x2

    if-ne p2, v1, :cond_6

    .line 94
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    instance-of v1, v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    if-eqz v1, :cond_5

    .line 95
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 97
    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->setContext(Landroid/content/Context;)V

    .line 99
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->show()V

    goto/16 :goto_0

    .line 101
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    goto/16 :goto_0

    .line 109
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2e7

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 113
    :pswitch_3
    const/16 v1, 0x2de

    if-ne p2, v1, :cond_7

    .line 114
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfProgress;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 115
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->setContext(Landroid/content/Context;)V

    .line 116
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->show()V

    .line 123
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2e8

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 118
    :cond_7
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 119
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->setContext(Landroid/content/Context;)V

    .line 120
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->show()V

    goto :goto_1

    .line 128
    :pswitch_4
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v1, :cond_8

    .line 129
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 132
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2e9

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 137
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v1, :cond_9

    .line 138
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 141
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2ea

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 146
    :pswitch_6
    const/16 v1, 0x2dc

    if-ne p2, v1, :cond_a

    .line 147
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 149
    :cond_a
    const/16 v1, 0x2c0

    if-ne p2, v1, :cond_b

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 151
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-nez v1, :cond_b

    .line 152
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "videoplayer.exit"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/content/LocalBroadcastManager;->sendBroadcast(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 157
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_b
    invoke-direct {p0, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->checkStateErrorPopup(I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 158
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x2eb

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 161
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v1, :cond_d

    .line 162
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 165
    :cond_d
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    .line 166
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->setContext(Landroid/content/Context;)V

    .line 167
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    check-cast v1, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/videoplayer/util/asf/AsfErrorPopup;->show(I)V

    goto/16 :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x2e5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public dismissChangePlayerPopup()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mChangePlayerDialog:Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;->dismiss()V

    .line 52
    :cond_0
    return-void
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->deleteObservers()V

    .line 46
    return-void
.end method

.method public setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mContext:Landroid/content/Context;

    .line 35
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->addObserver(Ljava/util/Observer;)V

    .line 36
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->sAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    return-object v0
.end method

.method public setHandler(Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;
    .locals 1
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->mHandler:Landroid/os/Handler;

    .line 41
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->sAsfObserver:Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;

    return-object v0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 56
    check-cast p2, [I

    .end local p2    # "data":Ljava/lang/Object;
    move-object v0, p2

    check-cast v0, [I

    .line 57
    .local v0, "msg":[I
    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/videoplayer/util/asf/AsfObserver;->onAllShareStateChanged(II)V

    .line 58
    return-void
.end method
