.class Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;
.super Ljava/lang/Object;
.source "MoviePlayer.java"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->createErrorDialog(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V
    .locals 0

    .prologue
    .line 5086
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x0

    .line 5088
    const/4 v0, 0x0

    .line 5089
    .local v0, "resultOnKey":Z
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mAlertmessage:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6402(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 5091
    sparse-switch p2, :sswitch_data_0

    .line 5120
    :cond_0
    :goto_0
    return v0

    .line 5094
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 5095
    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$500()Ljava/lang/String;

    move-result-object v1

    const-string v2, "dialog - KEYCODE_POWER_ACTION_UP"

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5097
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->handleErrorPopup()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 5098
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # setter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mLicErrorRelatedPopupShow:Z
    invoke-static {v1, v3}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6102(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;Z)Z

    .line 5099
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # getter for: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mErrorPopup:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$4000(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 5101
    :cond_1
    const/4 v0, 0x1

    .line 5102
    goto :goto_0

    .line 5105
    :sswitch_1
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-nez v1, :cond_2

    .line 5106
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/activity/MoviePlayer$34;->this$0:Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    # invokes: Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->handleErrorPopup()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->access$6300(Lcom/sec/android/app/videoplayer/activity/MoviePlayer;)V

    .line 5108
    :cond_2
    const/4 v0, 0x1

    .line 5109
    goto :goto_0

    .line 5112
    :sswitch_2
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5113
    const/4 v0, 0x1

    goto :goto_0

    .line 5091
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x1a -> :sswitch_0
        0x54 -> :sswitch_1
        0x7a -> :sswitch_0
    .end sparse-switch
.end method
