.class Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;
.super Ljava/lang/Object;
.source "VideoBrightnessPopup.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setDialogBuilderExtra(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 390
    const-string v0, "VideoBrightnessDialog"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNormalDialog() - isChecked:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mAutoCheckbox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$600(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 395
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    if-nez p2, :cond_3

    const/4 v0, 0x1

    :goto_0
    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setBrightProgressBarEnable(Z)V
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$700(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;Z)V

    .line 396
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->toggleBrightProgressBar()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1100(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 398
    if-nez p2, :cond_1

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mWindow:Landroid/view/Window;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1200(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->resetWindowBrightness(Landroid/view/Window;)V

    .line 402
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->setAutoBrightness(Z)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$800(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getBrightnessRange()I

    move-result v1

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightProgressBarAuto:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$900(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Landroid/widget/SeekBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->mBrightUtil:Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$500(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/util/VideoBrightnessUtil;->getSystemBrightnessLevel()I

    move-result v1

    div-int/lit8 v1, v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 409
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessDetailLevelText()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1300(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 410
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup$9;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;

    # invokes: Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->setAutoBrightnessContentDescription()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;->access$1400(Lcom/sec/android/app/videoplayer/popup/VideoBrightnessPopup;)V

    .line 411
    return-void

    .line 395
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
