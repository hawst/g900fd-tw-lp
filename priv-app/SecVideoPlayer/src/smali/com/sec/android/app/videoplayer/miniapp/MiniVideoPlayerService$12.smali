.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface$OnSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1742
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getInitWidth(ZIIII)I
    .locals 8
    .param p1, "isWideClip"    # Z
    .param p2, "w"    # I
    .param p3, "h"    # I
    .param p4, "width"    # I
    .param p5, "height"    # I

    .prologue
    const v7, 0x7f080031

    const v6, 0x7f080030

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1781
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 1782
    .local v0, "initWidth":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f080039

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 1783
    .local v1, "minWidth":I
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v2, :cond_0

    .line 1784
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1785
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 1786
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f08003a

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v1, v2

    .line 1790
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-le p2, p3, :cond_3

    move v2, v3

    :goto_0
    if-ne v2, p1, :cond_1

    .line 1791
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mOldOrientation:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    if-ne v2, v3, :cond_5

    .line 1792
    if-eqz p1, :cond_4

    .line 1793
    if-lt p2, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    if-gt p2, v2, :cond_1

    .line 1794
    move v0, p2

    .line 1814
    :cond_1
    :goto_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SUPPORT_AIA_CONTROLLER:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isWideClip()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1815
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    if-ne v0, v2, :cond_2

    .line 1816
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 1822
    :cond_2
    :goto_2
    const-string v2, "MiniVideoPlayerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getInitWidth() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1823
    return v0

    :cond_3
    move v2, v4

    .line 1790
    goto :goto_0

    .line 1796
    :cond_4
    if-lt p3, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    if-gt p3, v2, :cond_1

    .line 1797
    int-to-float v2, p3

    int-to-float v3, p5

    int-to-float v5, p4

    div-float/2addr v3, v5

    div-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 1798
    move v0, p3

    goto :goto_1

    .line 1802
    :cond_5
    if-eqz p1, :cond_6

    .line 1803
    if-lt p2, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenHeight:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$4000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    if-gt p2, v2, :cond_1

    .line 1804
    int-to-float v2, p2

    int-to-float v3, p4

    int-to-float v5, p5

    div-float/2addr v3, v5

    div-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static {v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v3

    if-gt v2, v3, :cond_1

    .line 1805
    move v0, p2

    goto :goto_1

    .line 1808
    :cond_6
    if-lt p3, v1, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mScreenWidth:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    if-gt p3, v2, :cond_1

    .line 1809
    move v0, p3

    goto :goto_1

    .line 1818
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    if-ne v0, v2, :cond_2

    .line 1819
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsResized:Z
    invoke-static {v2, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3702(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    goto :goto_2
.end method


# virtual methods
.method public onSizeChanged(II)V
    .locals 11
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 1745
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 1746
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    if-le p1, p2, :cond_2

    move v1, v9

    :goto_0
    iget v2, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v3, v8, Landroid/view/WindowManager$LayoutParams;->height:I

    move-object v0, p0

    move v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->getInitWidth(ZIIII)I

    move-result v7

    .line 1747
    .local v7, "initWidth":I
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3202(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I

    .line 1748
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I
    invoke-static {v0, p2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3302(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;I)I

    .line 1750
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isAudioOnlyClip()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1751
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->createAudioOnlyClipWindow()V

    .line 1773
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget v1, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->checkAndUpdateCtrlLayout(I)V

    .line 1774
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget v1, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    iget v2, v8, Landroid/view/WindowManager$LayoutParams;->height:I

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->updateSubtitleTextSize(II)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;II)V

    .line 1775
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    iget v1, v8, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v2, v8, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setXY(II)V

    .line 1777
    const-string v0, "MiniVideoPlayerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged() - width:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1778
    return-void

    .end local v7    # "initWidth":I
    :cond_2
    move v1, v10

    .line 1746
    goto :goto_0

    .line 1753
    .restart local v7    # "initWidth":I
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # setter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mIsAudioClipOnlyWindowCreated:Z
    invoke-static {v0, v10}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3402(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;Z)Z

    .line 1755
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mEmptyLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->isVideoRenderingStarted()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1756
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibleEmptyView(I)V

    .line 1757
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->requestLayout()V

    .line 1758
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->invalidate()V

    .line 1761
    :cond_4
    if-lez p1, :cond_5

    if-gtz p2, :cond_6

    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->getInstance()Lcom/sec/android/app/videoplayer/type/VideoSchemeType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/type/VideoSchemeType;->isStreamingType()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1762
    :cond_6
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance4PopupPlayer()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoWidth:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3200(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mVideoHeight:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3300(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)I

    move-result v2

    invoke-virtual {v0, v7, v1, v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->getWidthAndHeight(III)[I

    move-result-object v6

    .line 1763
    .local v6, "i":[I
    aget v0, v6, v10

    iput v0, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1764
    aget v0, v6, v9

    iput v0, v8, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1766
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/common/VUtils;->isLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget v0, v8, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v1, v8, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v0, v1

    if-gez v0, :cond_7

    .line 1767
    iput v10, v8, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1769
    :cond_7
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$12;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto/16 :goto_1
.end method
