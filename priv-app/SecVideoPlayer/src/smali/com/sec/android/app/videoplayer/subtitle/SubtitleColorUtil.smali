.class public Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;
.super Ljava/lang/Object;
.source "SubtitleColorUtil.java"


# static fields
.field private static final HUE_LENGTH:I = 0x136

.field private static mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;


# instance fields
.field final color:[F

.field private colorTable:[[I

.field private hue:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->color:[F

    .line 12
    const/16 v0, 0x136

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    .line 14
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v0, v0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v1, v1

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->colorTable:[[I

    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->buildHueColorArray()V

    .line 26
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->buildColorTable()V

    .line 27
    return-void

    .line 10
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static applyOpacity(II)I
    .locals 4
    .param p0, "color"    # I
    .param p1, "opacity"    # I

    .prologue
    .line 75
    mul-int/lit16 v0, p1, 0xff

    div-int/lit8 v0, v0, 0x64

    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    return v0
.end method

.method private buildColorTable()V
    .locals 11

    .prologue
    const/16 v10, 0xff

    .line 36
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v6, v6

    if-ge v3, v6, :cond_2

    .line 37
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    aget v6, v6, v3

    invoke-static {v6}, Landroid/graphics/Color;->red(I)I

    move-result v5

    .line 38
    .local v5, "r":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    aget v6, v6, v3

    invoke-static {v6}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 39
    .local v1, "g":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    aget v6, v6, v3

    invoke-static {v6}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 40
    .local v0, "b":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v6, v6

    div-int/lit8 v2, v6, 0x2

    .line 41
    .local v2, "half":I
    const/4 v4, 0x0

    .line 44
    .local v4, "j":I
    :goto_1
    if-ge v4, v2, :cond_0

    .line 45
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->colorTable:[[I

    aget-object v6, v6, v3

    rsub-int v7, v5, 0xff

    mul-int/2addr v7, v4

    div-int/2addr v7, v2

    rsub-int v7, v7, 0xff

    rsub-int v8, v1, 0xff

    mul-int/2addr v8, v4

    div-int/2addr v8, v2

    rsub-int v8, v8, 0xff

    rsub-int v9, v0, 0xff

    mul-int/2addr v9, v4

    div-int/2addr v9, v2

    rsub-int v9, v9, 0xff

    invoke-static {v10, v7, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v4

    .line 44
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 52
    :cond_0
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v6, v6

    if-ge v4, v6, :cond_1

    .line 53
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->colorTable:[[I

    aget-object v6, v6, v3

    sub-int v7, v4, v2

    mul-int/2addr v7, v5

    div-int/2addr v7, v2

    sub-int v7, v5, v7

    sub-int v8, v4, v2

    mul-int/2addr v8, v1

    div-int/2addr v8, v2

    sub-int v8, v1, v8

    sub-int v9, v4, v2

    mul-int/2addr v9, v0

    div-int/2addr v9, v2

    sub-int v9, v0, v9

    invoke-static {v10, v7, v8, v9}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    aput v7, v6, v4

    .line 52
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 36
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 60
    .end local v0    # "b":I
    .end local v1    # "g":I
    .end local v2    # "half":I
    .end local v4    # "j":I
    .end local v5    # "r":I
    :cond_2
    return-void
.end method

.method private buildHueColorArray()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 30
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    const/4 v2, 0x3

    new-array v2, v2, [F

    const/4 v3, 0x0

    int-to-float v4, v0

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v5, v2, v3

    const/4 v3, 0x2

    aput v5, v2, v3

    invoke-static {v2}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v2

    aput v2, v1, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    if-nez v0, :cond_0

    .line 20
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    .line 21
    :cond_0
    sget-object v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->mUniqueInstance:Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;

    return-object v0
.end method


# virtual methods
.method public getColorTable()[[I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->colorTable:[[I

    return-object v0
.end method

.method public getHueColorArray()[I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    return-object v0
.end method

.method public getHueLength()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleColorUtil;->hue:[I

    array-length v0, v0

    return v0
.end method
