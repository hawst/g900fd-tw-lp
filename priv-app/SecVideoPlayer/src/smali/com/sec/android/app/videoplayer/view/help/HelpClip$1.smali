.class Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;
.super Ljava/lang/Object;
.source "HelpClip.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/view/help/HelpClip;->createHelpButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$000(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v4

    if-nez v4, :cond_1

    .line 142
    const-string v3, "HelpClip"

    const-string v4, "onTouch mServiceUtil is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_0
    :goto_0
    return v2

    .line 146
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$100(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/view/help/HelpData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v4

    array-length v1, v4

    .line 147
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mJumpID:[I
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$200(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)[I

    move-result-object v5

    aget v5, v5, v0

    if-ne v4, v5, :cond_2

    .line 149
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 147
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_0
    move v2, v3

    .line 151
    goto :goto_0

    .line 153
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$000(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v2

    iget-object v4, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    # getter for: Lcom/sec/android/app/videoplayer/view/help/HelpClip;->mData:Lcom/sec/android/app/videoplayer/view/help/HelpData;
    invoke-static {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->access$100(Lcom/sec/android/app/videoplayer/view/help/HelpClip;)Lcom/sec/android/app/videoplayer/view/help/HelpData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/videoplayer/view/help/HelpData;->getTimeStemp()[J

    move-result-object v4

    aget-wide v4, v4, v0

    long-to-int v4, v4

    invoke-virtual {v2, v4}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->seekTo(I)V

    .line 154
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/help/HelpClip$1;->this$0:Lcom/sec/android/app/videoplayer/view/help/HelpClip;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/videoplayer/view/help/HelpClip;->updateContents(I)V

    move v2, v3

    .line 155
    goto :goto_0

    .line 149
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
