.class public abstract Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
.super Ljava/util/Observable;
.source "IAsfUtil.java"


# static fields
.field private static final BASE:I = 0x2bc

.field public static final DYNAMIC_BUFFRERING_NOT_SUPPORT:I = 0x2de

.field public static final DYNAMIC_BUFFRERING_SUPPORT:I = 0x2dd

.field public static final EXCEPTIONAL_CASE_HDMI_DURING_PLAYING:I = 0x2ee

.field private static final HANDLE_ACTION_START:I = 0x2da

.field public static final HANDLE_END_OF_CLIP:I = 0x2dc

.field public static final HANDLE_ERROR_BAD_RESPOSE:I = 0x2bc

.field public static final HANDLE_ERROR_CONTENT_NOT_AVAILABLE:I = 0x2c8

.field public static final HANDLE_ERROR_CURRENT_PROVIDER_REMOVED:I = 0x2ca

.field public static final HANDLE_ERROR_FAIL:I = 0x2bd

.field public static final HANDLE_ERROR_FEATURE_NOT_SUPPORTED:I = 0x2be

.field public static final HANDLE_ERROR_INVALID_ARGUMENT:I = 0x2bf

.field public static final HANDLE_ERROR_INVALID_DEVICE:I = 0x2c0

.field public static final HANDLE_ERROR_INVALID_OBJECT:I = 0x2c1

.field public static final HANDLE_ERROR_INVALID_STATE:I = 0x2c9

.field public static final HANDLE_ERROR_ITEM_NOT_EXIST:I = 0x2c2

.field public static final HANDLE_ERROR_NETWORK_NOT_AVAILABLE:I = 0x2c3

.field public static final HANDLE_ERROR_NO_RESPONSE:I = 0x2c4

.field public static final HANDLE_ERROR_OUT_OF_MEMORY:I = 0x2c5

.field public static final HANDLE_ERROR_PERMISSION_NOT_ALLOWED:I = 0x2c6

.field public static final HANDLE_ERROR_SERVICE_NOT_CONNECTED:I = 0x2c7

.field private static final HANDLE_ERROR_START:I = 0x2bc

.field public static final HANDLE_ERROR_SUCCESS:I = 0x2d0

.field public static final HANDLE_USER_CANCEL:I = 0x2db

.field public static final PLAYER_MODE_ASF_DEVICE:I = 0x1

.field public static final PLAYER_MODE_ASF_DEVICE_WITH_DIRECT_DMC:I = 0x2

.field public static final PLAYER_MODE_MY_DEVICE:I = 0x0

.field public static final PLAYER_STATE_BUFFERING:I = 0x6

.field public static final PLAYER_STATE_PAUSE:I = 0x5

.field public static final PLAYER_STATE_PAUSE_REQ:I = 0x4

.field public static final PLAYER_STATE_PLAY:I = 0x3

.field public static final PLAYER_STATE_PLAY_REQ:I = 0x2

.field public static final PLAYER_STATE_STOP:I = 0x1

.field public static final PLAYER_STATE_STOP_REQ:I = 0x0

.field private static final STATE_CHANGED_START:I = 0x2e4

.field public static final VIDEO_ALLSHARE_DEVICE_CHANGED:I = 0x2e7

.field public static final VIDEO_ALLSHARE_DISCONNECT:I = 0x2ea

.field public static final VIDEO_ALLSHARE_ERROR_STATE:I = 0x2eb

.field public static final VIDEO_ALLSHARE_FINISH_CONNECT:I = 0x2e9

.field public static final VIDEO_ALLSHARE_START_CONNECT:I = 0x2e8

.field public static final VIDEO_ALLSHARE_STATE_CHANGED:I = 0x2e5

.field public static final VIDEO_ALLSHARE_VOLUME_CHANGED:I = 0x2e6

.field public static final mState:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "STOP_REQ"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "STOP"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "PLAY_REQ"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "PLAY"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "PAUSE_REQ"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "PAUSE"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "BUFFERING"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;->mState:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/util/Observable;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
    .locals 1

    .prologue
    .line 118
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfUtil;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract changePlayer(IJZ)V
.end method

.method public abstract checkDmcDisabled()Z
.end method

.method public abstract connect()V
.end method

.method public abstract controlable()Z
.end method

.method public abstract disconnect()V
.end method

.method public abstract download(Landroid/net/Uri;)V
.end method

.method public abstract getAllShareIntentSubtitle()Ljava/lang/String;
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getPlayerList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPlayerState()I
.end method

.method public abstract getProvider()Ljava/lang/String;
.end method

.method public abstract getSelectedPlayer()Lcom/samsung/android/allshare/Device;
.end method

.method public abstract getStateAllShareIntent()Z
.end method

.method public abstract isASFConnected()Z
.end method

.method public abstract isContentChanged()Z
.end method

.method public abstract isDirectDmcMode()Z
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract isSupportDynamicBuffering()Z
.end method

.method public abstract notify(II)V
.end method

.method public abstract notifyPlayerState()V
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract refreshPlayerList()V
.end method

.method public abstract release()V
.end method

.method public abstract reset()V
.end method

.method public abstract seek(J)V
.end method

.method public abstract setAllShareIntentSubtitle(Landroid/net/Uri;)V
.end method

.method public abstract setAutoPlayingSetting(Z)V
.end method

.method public abstract setContentChanged(Z)V
.end method

.method public abstract setContext(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/asf/IAsfUtil;
.end method

.method public abstract setDirectDmcMode(Z)V
.end method

.method public abstract setDmrUdn(Ljava/lang/String;)V
.end method

.method public abstract setDuration(J)V
.end method

.method public abstract setInfoFromAllShareIntent(Lcom/samsung/android/allshare/Item;Z)V
.end method

.method public abstract setMute()V
.end method

.method public abstract setNIC(Ljava/lang/String;)V
.end method

.method public abstract setPlayer(I)V
.end method

.method public abstract setPlayerState(I)V
.end method

.method public abstract setPowerWakeLock(Z)V
.end method

.method public abstract setProvider(Ljava/lang/String;)V
.end method

.method public abstract setStateAllShareIntent(Z)V
.end method

.method public abstract setVolume(Z)V
.end method

.method public abstract skipDynamicBuffering()V
.end method

.method public abstract stop()V
.end method

.method public abstract updatePlayerList()V
.end method
