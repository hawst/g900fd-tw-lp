.class Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;
.super Landroid/os/AsyncTask;
.source "ChapterManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/preview/ChapterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChapterTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field duration:I

.field end:J

.field index:I

.field start:J

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/preview/ChapterManager;II)V
    .locals 0
    .param p2, "chapter"    # I
    .param p3, "duration"    # I

    .prologue
    .line 320
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 321
    iput p2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->index:I

    .line 322
    iput p3, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->duration:I

    .line 323
    return-void
.end method


# virtual methods
.method CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIII)Z
    .locals 15
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "videoId"    # J
    .param p5, "thischapter"    # I
    .param p6, "duration"    # I
    .param p7, "time_interval"    # I

    .prologue
    .line 364
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "CreateLiveThumbnailofThisChapter - chapter [ "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$100(Lcom/sec/android/app/videoplayer/preview/ChapterManager;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v3

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;JI)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    const/4 v13, 0x0

    .line 386
    :goto_0
    return v13

    .line 369
    :cond_0
    const/4 v13, 0x0

    .line 370
    .local v13, "ret":Z
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$100(Lcom/sec/android/app/videoplayer/preview/ChapterManager;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v3

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move/from16 v8, p5

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/videowall/FileMgr;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;JI)Ljava/lang/String;

    move-result-object v3

    .line 372
    .local v3, "filename":Ljava/lang/String;
    mul-int v4, p7, p5

    sget v6, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ChapterCount:I

    sget v7, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailWidth:I

    sget v8, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->ThumbnailHeight:I

    sget v9, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->Fps:I

    const/4 v11, 0x0

    move-object/from16 v2, p1

    move/from16 v5, p5

    move/from16 v10, p6

    move/from16 v12, p7

    invoke-static/range {v2 .. v12}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 374
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v4, "transcode success!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    move/from16 v0, p5

    invoke-static {v0, v3}, Lcom/sec/android/app/videoplayer/videowall/HandleVWLib;->MscEngine_setThumbnail(ILjava/lang/String;)I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1

    .line 376
    const/4 v13, 0x1

    .line 377
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "success to set Chapter : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 379
    :cond_1
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fail to set Chapter : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 382
    :cond_2
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v4, "transcode fail!!"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mFileMgr:Lcom/sec/android/videowall/FileMgr;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$100(Lcom/sec/android/app/videoplayer/preview/ChapterManager;)Lcom/sec/android/videowall/FileMgr;

    move-result-object v5

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-wide/from16 v8, p3

    move/from16 v10, p5

    invoke-virtual/range {v5 .. v10}, Lcom/sec/android/videowall/FileMgr;->deleteLastThumbnail(Ljava/lang/String;Ljava/lang/String;JI)V

    goto/16 :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 327
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->start:J

    .line 329
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    sget-object v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FilePath:Ljava/lang/String;

    sget-object v3, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->FileName:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    iget-wide v4, v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    iget v6, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->index:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->duration:I

    sget v8, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TimeInterval:I

    move-object v1, p0

    invoke-virtual/range {v1 .. v8}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->CreateLiveThumbnailofThisChapter(Ljava/lang/String;Ljava/lang/String;JIII)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 332
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 304
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 3

    .prologue
    .line 349
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCancelled. chapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 337
    if-nez p1, :cond_0

    .line 345
    :goto_0
    return-void

    .line 340
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 341
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    iget-object v0, v0, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    iget-object v1, v1, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    iget v3, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->index:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 343
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->end:J

    .line 344
    # getter for: Lcom/sec/android/app/videoplayer/preview/ChapterManager;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->this$0:Lcom/sec/android/app/videoplayer/preview/ChapterManager;

    iget-wide v2, v2, Lcom/sec/android/app/videoplayer/preview/ChapterManager;->mVideoId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : chapter transcode End!! :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->end:J

    iget-wide v4, p0, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->start:J

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 304
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/videoplayer/preview/ChapterManager$ChapterTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
