.class Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;
.super Landroid/os/Handler;
.source "VideoSubviewBtnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)V
    .locals 0

    .prologue
    .line 1159
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x1

    .line 1161
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1327
    :cond_0
    :goto_0
    return-void

    .line 1164
    :cond_1
    iget v7, p1, Landroid/os/Message;->what:I

    sparse-switch v7, :sswitch_data_0

    goto :goto_0

    .line 1166
    :sswitch_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbProgressDragStatus:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$900(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mCtrlLayoutShow:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1000(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mbResume:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$800(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    sget-object v7, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->sVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/Video360/Video360XMPUtil;->getIsPlayChanging()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1168
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->setProgress()I

    .line 1169
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 1170
    invoke-virtual {p0, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 1171
    const-wide/16 v8, 0x1f4

    invoke-virtual {p0, p1, v8, v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1176
    :sswitch_1
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v7

    const/16 v8, 0x1e

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1177
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1178
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0a0192

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1180
    :cond_4
    iget v7, p1, Landroid/os/Message;->arg1:I

    const/16 v8, 0x8

    if-ne v7, v8, :cond_5

    .line 1181
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0a0074

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1183
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0a0104

    invoke-static {v7, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 1189
    :sswitch_2
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mPlayPauseButton:Landroid/widget/ImageButton;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/widget/ImageButton;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 1191
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isStateViewVisible()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1192
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    iget-object v7, v7, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideStateView()V

    .line 1195
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mIsAlreadyPauseState:Z
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1300(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1196
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$200(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->start()V

    goto/16 :goto_0

    .line 1201
    :sswitch_3
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v6

    .line 1202
    .local v6, "mPrefMgr":Lcom/sec/android/app/videoplayer/db/SharedPreference;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;

    move-result-object v5

    .line 1204
    .local v5, "mDrmUtil":Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->CHINA:Z

    if-eqz v7, :cond_7

    if-eqz v6, :cond_7

    const-string v7, "showwifipopup_changeplayer"

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->loadBooleanKey(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_7

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Lcom/sec/android/app/videoplayer/util/VideoDRMUtil;->checkIsWifiEnabled()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1207
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 1208
    .local v3, "inflate":Landroid/view/LayoutInflater;
    const v7, 0x7f030004

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1210
    .local v1, "PopupView":Landroid/view/View;
    const v7, 0x7f0d005a

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1211
    .local v0, "Checkbox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1212
    new-instance v7, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$1;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$1;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1245
    const v7, 0x7f0d005b

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1246
    .local v4, "mCheckText":Landroid/widget/TextView;
    new-instance v7, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$2;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$2;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;Landroid/widget/CheckBox;)V

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1279
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    invoke-direct {v2, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1280
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a003b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1281
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1283
    const v7, 0x7f0a00dd

    new-instance v8, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$3;

    invoke-direct {v8, p0, v0, v6}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$3;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;Landroid/widget/CheckBox;Lcom/sec/android/app/videoplayer/db/SharedPreference;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1298
    const v7, 0x7f0a0026

    new-instance v8, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$4;

    invoke-direct {v8, p0}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11$4;-><init>(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1304
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1502(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;

    .line 1305
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/app/AlertDialog;

    move-result-object v7

    sget-boolean v8, Lcom/sec/android/app/videoplayer/common/feature/Feature;->DISMISS_POPUP_TOUCH_OUTSIDE:Z

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 1306
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mConnectionCheckPopup:Landroid/app/AlertDialog;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1500(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 1309
    .end local v0    # "Checkbox":Landroid/widget/CheckBox;
    .end local v1    # "PopupView":Landroid/view/View;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "inflate":Landroid/view/LayoutInflater;
    .end local v4    # "mCheckText":Landroid/widget/TextView;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v7

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 1311
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->getInstance()Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v8

    # setter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1402(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 1312
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1100(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Landroid/os/Handler;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->setContext(Landroid/content/Context;Landroid/os/Handler;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    .line 1313
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController$11;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->mChangePlayerPopup:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoSubviewBtnController;)Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerPopup;->createDevicePopup()V

    goto/16 :goto_0

    .line 1164
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x1e -> :sswitch_1
        0x1f -> :sswitch_2
    .end sparse-switch
.end method
