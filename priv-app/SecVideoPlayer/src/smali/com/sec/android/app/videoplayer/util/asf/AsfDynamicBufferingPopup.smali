.class public Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;
.super Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;
.source "AsfDynamicBufferingPopup.java"


# static fields
.field private static sAsfDynamicBufferingPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final UPDATE_POPUP_OK_BUTTON:I

.field private mDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    invoke-direct {v0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->sAsfDynamicBufferingPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/IChangePlayerInfoPopup;-><init>()V

    .line 12
    const-class v0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->TAG:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->UPDATE_POPUP_OK_BUTTON:I

    .line 100
    new-instance v0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$5;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mHandler:Landroid/os/Handler;

    .line 20
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 11
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private create()V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    .local v0, "dialog":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0a0189

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 70
    const v1, 0x7f0a0184

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 72
    const v1, 0x104000a

    new-instance v2, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$1;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    const/high16 v1, 0x1040000

    new-instance v2, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$2;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$3;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    new-instance v1, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup$4;-><init>(Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 97
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    .line 98
    return-void
.end method

.method public static getInstance()Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->sAsfDynamicBufferingPopup:Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 29
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 31
    :cond_0
    return-void
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    .line 48
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 53
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    .line 55
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 56
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    if-nez v1, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->create()V

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 61
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mDialog:Landroid/app/AlertDialog;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 62
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/util/asf/AsfDynamicBufferingPopup;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 65
    :cond_1
    return-void
.end method
