.class Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;
.super Ljava/lang/Object;
.source "ChangePlayerScanGuidePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)V
    .locals 0

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 180
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mCheckbox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$000(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    const/4 v0, 0x0

    .line 183
    .local v0, "keyString":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 202
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 203
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$200(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/videoplayer/db/SharedPreference;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/videoplayer/db/SharedPreference;->saveState(Ljava/lang/String;Z)V

    .line 206
    .end local v0    # "keyString":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 213
    :goto_1
    return-void

    .line 185
    .restart local v0    # "keyString":Ljava/lang/String;
    :pswitch_1
    const-string v0, "showscreenmirroringguildepopup_hotspot"

    .line 186
    goto :goto_0

    .line 188
    :pswitch_2
    const-string v0, "showscreenmirroringguildepopup_wifidirect"

    .line 189
    goto :goto_0

    .line 191
    :pswitch_3
    const-string v0, "showscreenmirroringguildepopup_sidesync_running"

    .line 192
    goto :goto_0

    .line 194
    :pswitch_4
    const-string v0, "showscreenmirroringguildepopup_power_saving_mode"

    .line 195
    goto :goto_0

    .line 197
    :pswitch_5
    const-string v0, "showscreenmirroringguildepopup_limited_contents"

    .line 198
    goto :goto_0

    .line 208
    .end local v0    # "keyString":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mExceptionCase:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_2

    .line 209
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x1f4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 211
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup$3;->this$0:Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;->access$300(Lcom/sec/android/app/videoplayer/popup/changeplayer/ChangePlayerScanGuidePopup;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
