.class public final Lcom/sec/android/app/videoplayer/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final ChapterLine:I = 0x7f070000

.field public static final Line:I = 0x7f070001

.field public static final No_item:I = 0x7f070002

.field public static final Separate_Color:I = 0x7f070003

.field public static final Separate_Text:I = 0x7f070004

.field public static final action_bar_bg_color:I = 0x7f070005

.field public static final action_bar_title_color:I = 0x7f070006

.field public static final allshare_sublable_text_color:I = 0x7f070007

.field public static final animated_play_pause_button_edge:I = 0x7f070008

.field public static final black:I = 0x7f070009

.field public static final bookmark_black_background:I = 0x7f07000a

.field public static final brightness_text_color:I = 0x7f07000b

.field public static final brightness_text_level:I = 0x7f07000c

.field public static final button_pressed:I = 0x7f07000d

.field public static final check_videoListTitle:I = 0x7f07000e

.field public static final color2:I = 0x7f07000f

.field public static final contextual_text_color:I = 0x7f070010

.field public static final contextual_title_date_color:I = 0x7f070011

.field public static final contextual_title_location_color:I = 0x7f070012

.field public static final contextual_title_split_color:I = 0x7f070013

.field public static final contextual_title_weather_color:I = 0x7f070014

.field public static final controller_bg:I = 0x7f070015

.field public static final count_BG:I = 0x7f070016

.field public static final current_time_text:I = 0x7f070017

.field public static final defaultBackground:I = 0x7f070018

.field public static final detail_seek_text:I = 0x7f070019

.field public static final details_id:I = 0x7f07001a

.field public static final details_title:I = 0x7f07001b

.field public static final details_value:I = 0x7f07001c

.field public static final dialog_section_color:I = 0x7f07001d

.field public static final duration_text:I = 0x7f07001e

.field public static final end_time_text:I = 0x7f07001f

.field public static final folder_count:I = 0x7f070020

.field public static final font_edge_shadowcolor:I = 0x7f070021

.field public static final font_edge_shadoweffectcolor:I = 0x7f070022

.field public static final hover_preview_text_color:I = 0x7f070023

.field public static final keyboard_transparent_background:I = 0x7f070024

.field public static final label_color:I = 0x7f070025

.field public static final list_divider:I = 0x7f070026

.field public static final list_duration_text:I = 0x7f070027

.field public static final list_elapsed_text:I = 0x7f070028

.field public static final main_list_text_color:I = 0x7f070029

.field public static final menu_subtitle_text_color:I = 0x7f07002a

.field public static final menu_text_color:I = 0x7f07002b

.field public static final menu_text_color_disabled:I = 0x7f07002c

.field public static final menu_text_color_pressed:I = 0x7f07002d

.field public static final menu_value_color:I = 0x7f07002e

.field public static final menu_value_color_press:I = 0x7f07002f

.field public static final micro_text:I = 0x7f070030

.field public static final mosaic_background:I = 0x7f070031

.field public static final mosaic_black_background:I = 0x7f070032

.field public static final movie_background:I = 0x7f070033

.field public static final not_transparency:I = 0x7f070034

.field public static final opaque_black:I = 0x7f070035

.field public static final opaque_black_widget:I = 0x7f070036

.field public static final play_speed_mode_text:I = 0x7f070037

.field public static final play_speed_text:I = 0x7f070038

.field public static final play_speed_text_press:I = 0x7f070039

.field public static final playerlist_first_row_text_color:I = 0x7f07003a

.field public static final playerlist_first_row_text_color_playing:I = 0x7f07003b

.field public static final playerlist_progressbar_bg_color:I = 0x7f07003c

.field public static final playerlist_progressbar_color:I = 0x7f07003d

.field public static final playerlist_scroll_color:I = 0x7f07003e

.field public static final playerlist_second_row_text_color:I = 0x7f07003f

.field public static final playerlist_second_row_text_color_pressed:I = 0x7f070040

.field public static final playerlist_seleted_bg_color:I = 0x7f070041

.field public static final playerlist_thumbnail_stroke_color:I = 0x7f070042

.field public static final playerlist_time_divider_color:I = 0x7f070043

.field public static final playerlist_title_divider_color:I = 0x7f070044

.field public static final playerlist_title_shadow_color:I = 0x7f070045

.field public static final playerlist_title_text_color:I = 0x7f070046

.field public static final popup_color:I = 0x7f070047

.field public static final popup_sharevia_color:I = 0x7f070048

.field public static final popup_title:I = 0x7f070049

.field public static final popup_value_color:I = 0x7f07004a

.field public static final popup_value_color_changeable:I = 0x7f07004b

.field public static final quick_panel_background_color:I = 0x7f07004c

.field public static final quick_panel_text_color:I = 0x7f07004d

.field public static final recent_filename_color:I = 0x7f07004e

.field public static final ripple_bg_color:I = 0x7f07004f

.field public static final scrubbing_progressbar_toastdisplay:I = 0x7f070050

.field public static final scrubbingdetail_progressbar_toastdisplay:I = 0x7f070051

.field public static final search_result_color:I = 0x7f070052

.field public static final search_speed_text:I = 0x7f070053

.field public static final select_all_text_color:I = 0x7f070054

.field public static final selected_item:I = 0x7f070055

.field public static final send_dialog_text_color:I = 0x7f070056

.field public static final sidechapter_popup_color:I = 0x7f070057

.field public static final split_foler_selected_text:I = 0x7f070058

.field public static final split_foler_text:I = 0x7f070059

.field public static final split_left_background:I = 0x7f07005a

.field public static final split_list_background_color:I = 0x7f07005b

.field public static final sub_list_text_color:I = 0x7f07005c

.field public static final subtitle_activity_statusbar_:I = 0x7f07005d

.field public static final subtitle_color_opacity_divider:I = 0x7f07005e

.field public static final subtitle_menu_value_color:I = 0x7f07005f

.field public static final tab_disabled:I = 0x7f070060

.field public static final tab_enable:I = 0x7f070061

.field public static final tab_focus_font_color:I = 0x7f070062

.field public static final tab_font_color:I = 0x7f070063

.field public static final tab_font_shadow_color:I = 0x7f070064

.field public static final tag_buddy_text_shadow:I = 0x7f070065

.field public static final text_color_5:I = 0x7f070066

.field public static final text_color_6:I = 0x7f070067

.field public static final text_color_7:I = 0x7f070068

.field public static final thumbnail_bkgnd:I = 0x7f070069

.field public static final thumbnail_duration_text:I = 0x7f07006a

.field public static final thumbnail_elapsed_text:I = 0x7f07006b

.field public static final time_indicator:I = 0x7f07006c

.field public static final title_text_color:I = 0x7f07006d

.field public static final title_text_shadow_color:I = 0x7f07006e

.field public static final transparency:I = 0x7f07006f

.field public static final videoListNewDuration:I = 0x7f070070

.field public static final video_count:I = 0x7f070071

.field public static final video_tb_controller_bg:I = 0x7f070072

.field public static final video_tb_progress_bg:I = 0x7f070073

.field public static final video_widget_filename_text_color:I = 0x7f070074

.field public static final video_widget_novideos2_text_color:I = 0x7f070075

.field public static final volume_text:I = 0x7f070076

.field public static final white:I = 0x7f070077

.field public static final widget_progressbar_bar:I = 0x7f070078

.field public static final widget_progressbar_bg:I = 0x7f070079

.field public static final winset3_whith_selected:I = 0x7f07007a


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
