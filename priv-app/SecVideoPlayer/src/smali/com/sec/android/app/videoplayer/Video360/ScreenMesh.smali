.class Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;
.super Ljava/lang/Object;
.source "ScreenMesh.java"


# static fields
.field private static final BYTESPERFLOAT:I = 0x4

.field private static final BYTESPERSHORT:I = 0x2

.field public static final CYLINDER:I = 0x1

.field public static final FLAT:I = 0x2

.field public static final IMAGE:I = 0x1

.field public static final NO_SHAPE:I = -0x1

.field public static final NO_TYPE:I = -0x1

.field private static final NUM_FLAT_VERTS:I = 0x6

.field public static final SPHERE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "ScreenMesh"

.field public static final VIDEO:I

.field private static mImageHeight:F

.field private static mImageWidth:F

.field public static final mQuadTextureCoordinates:[F

.field public static final mQuadVertices:[F

.field static offsets:[F


# instance fields
.field private mCenterPointHandle:I

.field private mIndicesCount:I

.field private mIndicesFB:Ljava/nio/ShortBuffer;

.field private mMVPMatrix:[F

.field private mOffsetBufferFB:Ljava/nio/FloatBuffer;

.field private mOffsetsHandle:I

.field private mPositionsFB:Ljava/nio/FloatBuffer;

.field private mScreenSizeHandle:I

.field private mShape:I

.field private mTexCoordsFB:Ljava/nio/FloatBuffer;

.field private mTextureMatHandle:I

.field private mUsedProgram:I

.field private mVMatrix:[F

.field private maPositionHandle:I

.field private maTextureHandle:I

.field private mbVisible:Z

.field private msTextureHandle:I

.field private muMVPMatrixHandle:I

.field private muVMatrixHandle:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x12

    const/16 v4, 0xc

    const/4 v3, 0x0

    .line 53
    const v0, 0x3b088889

    sput v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    .line 54
    const v0, 0x3aa3d70a    # 0.00125f

    sput v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    .line 55
    new-array v0, v5, [F

    const/4 v1, 0x0

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v3, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    neg-float v2, v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v3, v0, v1

    const/16 v1, 0x8

    aput v3, v0, v1

    const/16 v1, 0x9

    aput v3, v0, v1

    const/16 v1, 0xa

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    neg-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0xb

    aput v3, v0, v1

    sget v1, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    aput v1, v0, v4

    const/16 v1, 0xd

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    neg-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0xe

    aput v3, v0, v1

    const/16 v1, 0xf

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    neg-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageWidth:F

    neg-float v2, v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget v2, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mImageHeight:F

    neg-float v2, v2

    aput v2, v0, v1

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->offsets:[F

    .line 430
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mQuadVertices:[F

    .line 441
    new-array v0, v4, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mQuadTextureCoordinates:[F

    return-void

    .line 430
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 441
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v1, 0x10

    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    .line 37
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    .line 42
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTextureMatHandle:I

    .line 43
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetsHandle:I

    .line 44
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mScreenSizeHandle:I

    .line 45
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mCenterPointHandle:I

    .line 46
    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muVMatrixHandle:I

    .line 48
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mMVPMatrix:[F

    .line 49
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mVMatrix:[F

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mbVisible:Z

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mMVPMatrix:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mVMatrix:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 96
    sget-object v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->offsets:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    .line 97
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    sget-object v1, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->offsets:[F

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 98
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 99
    return-void
.end method

.method private initCylinder(IIF)V
    .locals 20
    .param p1, "stacks"    # I
    .param p2, "slices"    # I
    .param p3, "radius"    # F

    .prologue
    .line 310
    const-string v15, "ScreenMesh"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "initCylinder() stacks:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", slices:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", radius:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    mul-int/lit8 v15, p1, 0x3

    add-int/lit8 v16, p2, 0x1

    mul-int v15, v15, v16

    mul-int/lit8 v5, v15, 0x2

    .line 313
    .local v5, "indexCount":I
    add-int/lit8 v15, p1, 0x1

    add-int/lit8 v16, p2, 0x1

    mul-int v11, v15, v16

    .line 316
    .local v11, "vertexCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    if-eqz v15, :cond_0

    .line 317
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    invoke-virtual {v15}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 318
    :cond_0
    mul-int/lit8 v15, v5, 0x2

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    .line 319
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesCount:I

    .line 321
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    if-eqz v15, :cond_1

    .line 322
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 323
    :cond_1
    mul-int/lit8 v15, v11, 0x3

    mul-int/lit8 v15, v15, 0x4

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    .line 325
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    if-eqz v15, :cond_2

    .line 326
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 327
    :cond_2
    mul-int/lit8 v15, v11, 0x2

    mul-int/lit8 v15, v15, 0x4

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    .line 335
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, p1

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v8, v0

    .line 336
    .local v8, "stackAngle":F
    const-wide v16, 0x401921fb54442d18L    # 6.283185307179586

    move/from16 v0, p2

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v7, v0

    .line 337
    .local v7, "sliceAngle":F
    const/4 v2, 0x0

    .line 340
    .local v2, "curIndex":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    add-int/lit8 v15, p1, 0x1

    if-ge v4, v15, :cond_5

    .line 341
    int-to-float v15, v4

    mul-float/2addr v15, v8

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v3, v0

    .line 342
    .local v3, "d":F
    mul-int v15, p1, p2

    div-int/lit8 v15, v15, 0x2

    mul-int v16, v4, p2

    sub-int v15, v15, v16

    int-to-float v15, v15

    const v16, 0x3a378034    # 7.0E-4f

    mul-float v13, v15, v16

    .line 344
    .local v13, "y":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    add-int/lit8 v15, p2, 0x1

    if-ge v6, v15, :cond_4

    .line 345
    int-to-float v15, v6

    mul-float/2addr v15, v7

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 346
    .local v12, "x":F
    int-to-float v15, v6

    mul-float/2addr v15, v7

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    neg-double v0, v0

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    double-to-float v14, v0

    .line 349
    .local v14, "z":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    mul-float v16, v12, p3

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 350
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15, v13}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 351
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    mul-float v16, v14, p3

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 356
    int-to-float v15, v6

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v9, v15, v16

    .line 357
    .local v9, "u":F
    int-to-float v15, v4

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v10, v15, v16

    .line 358
    .local v10, "v":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 359
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 367
    add-int/lit8 v15, p1, -0x1

    if-eq v4, v15, :cond_3

    .line 368
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, p2, 0x1

    add-int v16, v16, v2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 369
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    int-to-short v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 370
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, v2, 0x1

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 371
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int v16, v2, p2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 372
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    int-to-short v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 373
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, p2, 0x1

    add-int v16, v16, v2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 374
    add-int/lit8 v2, v2, 0x1

    .line 344
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 340
    .end local v9    # "u":F
    .end local v10    # "v":F
    .end local v12    # "x":F
    .end local v14    # "z":F
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 379
    .end local v3    # "d":F
    .end local v6    # "j":I
    .end local v13    # "y":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 380
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 381
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 383
    return-void
.end method

.method private initFlat()V
    .locals 0

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->initFlatVerts()V

    .line 389
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->initFlatTex()V

    .line 390
    return-void
.end method

.method private initFlatTex()V
    .locals 4

    .prologue
    .line 413
    const/16 v1, 0xc

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 423
    .local v0, "flatTexCoords":[F
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    if-nez v1, :cond_0

    .line 424
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    .line 425
    :cond_0
    const-string v1, "ScreenMesh"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 427
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 428
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 429
    return-void

    .line 413
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data
.end method

.method private initFlatVerts()V
    .locals 4

    .prologue
    .line 394
    const/16 v1, 0x12

    new-array v0, v1, [F

    fill-array-data v0, :array_0

    .line 404
    .local v0, "cylinderVerts":[F
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    if-nez v1, :cond_0

    .line 405
    array-length v1, v0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    .line 406
    :cond_0
    const-string v1, "ScreenMesh"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 408
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 409
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 410
    return-void

    .line 394
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private initSphere(IIF)V
    .locals 20
    .param p1, "stacks"    # I
    .param p2, "slices"    # I
    .param p3, "radius"    # F

    .prologue
    .line 232
    const-string v15, "ScreenMesh"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "initSphere() stacks:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", slices:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", radius:"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    mul-int/lit8 v15, p1, 0x3

    add-int/lit8 v16, p2, 0x1

    mul-int v15, v15, v16

    mul-int/lit8 v5, v15, 0x2

    .line 235
    .local v5, "indexCount":I
    add-int/lit8 v15, p1, 0x1

    add-int/lit8 v16, p2, 0x1

    mul-int v11, v15, v16

    .line 238
    .local v11, "vertexCount":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    if-eqz v15, :cond_0

    .line 239
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    invoke-virtual {v15}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 240
    :cond_0
    mul-int/lit8 v15, v5, 0x2

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    .line 241
    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesCount:I

    .line 243
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    if-eqz v15, :cond_1

    .line 244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 245
    :cond_1
    mul-int/lit8 v15, v11, 0x3

    mul-int/lit8 v15, v15, 0x4

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    .line 247
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    if-eqz v15, :cond_2

    .line 248
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 249
    :cond_2
    mul-int/lit8 v15, v11, 0x2

    mul-int/lit8 v15, v15, 0x4

    invoke-static {v15}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v15

    invoke-virtual {v15}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    .line 257
    const-wide v16, 0x400921fb54442d18L    # Math.PI

    move/from16 v0, p1

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v8, v0

    .line 258
    .local v8, "stackAngle":F
    const-wide v16, 0x401921fb54442d18L    # 6.283185307179586

    move/from16 v0, p2

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v7, v0

    .line 259
    .local v7, "sliceAngle":F
    const/4 v2, 0x0

    .line 262
    .local v2, "curIndex":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    add-int/lit8 v15, p1, 0x1

    if-ge v4, v15, :cond_5

    .line 263
    int-to-float v15, v4

    mul-float/2addr v15, v8

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v3, v0

    .line 264
    .local v3, "d":F
    int-to-float v15, v4

    mul-float/2addr v15, v8

    float-to-double v0, v15

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v13, v0

    .line 266
    .local v13, "y":F
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    add-int/lit8 v15, p2, 0x1

    if-ge v6, v15, :cond_4

    .line 267
    float-to-double v0, v3

    move-wide/from16 v16, v0

    int-to-float v15, v6

    mul-float/2addr v15, v7

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v12, v0

    .line 268
    .local v12, "x":F
    neg-float v15, v3

    float-to-double v0, v15

    move-wide/from16 v16, v0

    int-to-float v15, v6

    mul-float/2addr v15, v7

    float-to-double v0, v15

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-float v14, v0

    .line 271
    .local v14, "z":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    mul-float v16, v12, p3

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 272
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    mul-float v16, v13, p3

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 273
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    mul-float v16, v14, p3

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 278
    int-to-float v15, v6

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v9, v15, v16

    .line 279
    .local v9, "u":F
    int-to-float v15, v4

    move/from16 v0, p1

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v10, v15, v16

    .line 280
    .local v10, "v":F
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 281
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v15, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 289
    add-int/lit8 v15, p1, -0x1

    if-eq v4, v15, :cond_3

    .line 290
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, p2, 0x1

    add-int v16, v16, v2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 291
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    int-to-short v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 292
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, v2, 0x1

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 293
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int v16, v2, p2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 294
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    int-to-short v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 295
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    add-int/lit8 v16, p2, 0x1

    add-int v16, v16, v2

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 296
    add-int/lit8 v2, v2, 0x1

    .line 266
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 262
    .end local v9    # "u":F
    .end local v10    # "v":F
    .end local v12    # "x":F
    .end local v14    # "z":F
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 301
    .end local v3    # "d":F
    .end local v6    # "j":I
    .end local v13    # "y":F
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 302
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 303
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 304
    return-void
.end method


# virtual methods
.method public draw(II[F)V
    .locals 11
    .param p1, "textureId"    # I
    .param p2, "mediaType"    # I
    .param p3, "tm"    # [F

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 172
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mbVisible:Z

    if-nez v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 178
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maPositionHandle:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 179
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 182
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maTextureHandle:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    move v1, v9

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 183
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maTextureHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 185
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muMVPMatrixHandle:I

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mMVPMatrix:[F

    invoke-static {v0, v8, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 187
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 188
    if-nez p2, :cond_9

    .line 189
    const v0, 0x8d65

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 193
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->msTextureHandle:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 196
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetsHandle:I

    if-eq v0, v7, :cond_2

    .line 197
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetsHandle:I

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glUniform2fv(IILjava/nio/FloatBuffer;)V

    .line 199
    :cond_2
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mScreenSizeHandle:I

    if-eq v0, v7, :cond_3

    .line 200
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mScreenSizeHandle:I

    const/high16 v1, 0x44960000    # 1200.0f

    const/high16 v2, 0x44480000    # 800.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 202
    :cond_3
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mCenterPointHandle:I

    if-eq v0, v7, :cond_4

    .line 203
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mCenterPointHandle:I

    const/high16 v1, 0x43c80000    # 400.0f

    const/high16 v2, 0x43480000    # 200.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 205
    :cond_4
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muVMatrixHandle:I

    if-eq v0, v7, :cond_5

    .line 206
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muVMatrixHandle:I

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mVMatrix:[F

    invoke-static {v0, v8, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 208
    :cond_5
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTextureMatHandle:I

    if-eq v0, v7, :cond_6

    if-eqz p3, :cond_6

    .line 209
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTextureMatHandle:I

    invoke-static {v0, v8, v3, p3, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 214
    :cond_6
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    if-ne v0, v8, :cond_a

    .line 215
    :cond_7
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesCount:I

    const/16 v1, 0x1403

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    invoke-static {v10, v0, v1, v2}, Landroid/opengl/GLES20;->glDrawElements(IIILjava/nio/Buffer;)V

    .line 221
    :cond_8
    :goto_1
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maPositionHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 222
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maTextureHandle:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 224
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v6

    .line 225
    .local v6, "error":I
    if-eqz v6, :cond_0

    .line 226
    const-string v0, "ScreenMesh"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GLES20 error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    .end local v6    # "error":I
    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid screen mesh media type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_a
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    if-ne v0, v9, :cond_8

    .line 217
    const/4 v0, 0x6

    invoke-static {v10, v3, v0}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_1
.end method

.method public getMVPMatrix()[F
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mMVPMatrix:[F

    return-object v0
.end method

.method public getShape()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    return v0
.end method

.method public getVMatrix()[F
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mVMatrix:[F

    return-object v0
.end method

.method public init(ILandroid/content/Context;)V
    .locals 5
    .param p1, "shapeHint"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v3, 0x40

    .line 106
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mShape:I

    .line 108
    const-string v0, "ScreenMesh"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init() shapeHint:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    packed-switch p1, :pswitch_data_0

    .line 121
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported renderer shape"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :pswitch_0
    invoke-direct {p0, v3, v3, v4}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->initSphere(IIF)V

    .line 123
    :goto_0
    return-void

    .line 115
    :pswitch_1
    invoke-direct {p0, v3, v3, v4}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->initCylinder(IIF)V

    goto :goto_0

    .line 118
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->initFlat()V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public releaseBuffer()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mPositionsFB:Ljava/nio/FloatBuffer;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 76
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTexCoordsFB:Ljava/nio/FloatBuffer;

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    if-eqz v0, :cond_2

    .line 80
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    invoke-virtual {v0}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mIndicesFB:Ljava/nio/ShortBuffer;

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 86
    iput-object v1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetBufferFB:Ljava/nio/FloatBuffer;

    .line 88
    :cond_3
    return-void
.end method

.method public setProgram(I)Z
    .locals 4
    .param p1, "progHandle"    # I

    .prologue
    const/4 v3, -0x1

    .line 127
    const-string v0, "ScreenMesh"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProgram() progHandle:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    .line 130
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maPositionHandle:I

    .line 131
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maTextureHandle:I

    .line 132
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muMVPMatrixHandle:I

    .line 133
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->msTextureHandle:I

    .line 134
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetsHandle:I

    .line 135
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mScreenSizeHandle:I

    .line 136
    iput v3, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mCenterPointHandle:I

    .line 138
    if-gtz p1, :cond_0

    .line 139
    const-string v0, "ScreenMesh"

    const-string v1, "progHandle is not valid!"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    const/4 v0, 0x0

    .line 153
    :goto_0
    return v0

    .line 143
    :cond_0
    iput p1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    .line 144
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "aPosition"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maPositionHandle:I

    .line 145
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "aTexCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->maTextureHandle:I

    .line 146
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uMVPMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muMVPMatrixHandle:I

    .line 147
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uTexture"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->msTextureHandle:I

    .line 148
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uOffsets"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mOffsetsHandle:I

    .line 149
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uResolution"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mScreenSizeHandle:I

    .line 150
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uCenterPoint"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mCenterPointHandle:I

    .line 151
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uVMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->muVMatrixHandle:I

    .line 152
    iget v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mUsedProgram:I

    const-string v1, "uTextureMat"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mTextureMatHandle:I

    .line 153
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 157
    const-string v0, "ScreenMesh"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisible() visible:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iput-boolean p1, p0, Lcom/sec/android/app/videoplayer/Video360/ScreenMesh;->mbVisible:Z

    .line 160
    return-void
.end method
