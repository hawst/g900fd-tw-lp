.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;
.super Landroid/os/Handler;
.source "MiniVideoBtnController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V
    .locals 0

    .prologue
    .line 490
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 492
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 494
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mCtrlLayoutShow:Z
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setProgress()I

    .line 497
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 498
    invoke-virtual {p0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->obtainMessage(I)Landroid/os/Message;

    move-result-object p1

    .line 499
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 504
    :sswitch_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 505
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->hide(Z)V

    goto :goto_0

    .line 510
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$600(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 511
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mOnCommandListener:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$700(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;->performCommand(I)V

    .line 513
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v3, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->show(ZI)V

    goto :goto_0

    .line 518
    :sswitch_3
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setSpeedTextVisibility(I)V

    goto :goto_0

    .line 522
    :sswitch_4
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->setSpeedTextVisibility(I)V

    goto :goto_0

    .line 526
    :sswitch_5
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 527
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->ffSeek()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$900(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    goto :goto_0

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->mLongSeekMode:I
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 529
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$5;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->rewSeek()V
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;->access$1000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController;)V

    goto/16 :goto_0

    .line 492
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_5
        0xb -> :sswitch_3
        0xc -> :sswitch_4
        0x20 -> :sswitch_2
        0x64 -> :sswitch_1
    .end sparse-switch
.end method
