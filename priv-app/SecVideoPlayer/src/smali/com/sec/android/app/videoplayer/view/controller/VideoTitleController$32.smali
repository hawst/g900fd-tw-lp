.class Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;
.super Ljava/lang/Object;
.source "VideoTitleController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V
    .locals 0

    .prologue
    .line 2300
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromTouch"    # Z

    .prologue
    .line 2303
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMoviePlayerHasWindowFocus()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 2304
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mPopupVolBar:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2317
    :goto_0
    return-void

    .line 2308
    :cond_0
    if-eqz p3, :cond_1

    .line 2309
    invoke-static {}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getInstance()Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;

    move-result-object v0

    .line 2310
    .local v0, "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    invoke-virtual {v0, p2}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->setVolume(I)V

    .line 2311
    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;->getCurrentVolume()I

    move-result v1

    .line 2312
    .local v1, "vol":I
    if-eq p2, v1, :cond_1

    .line 2313
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    iget-object v2, v2, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mVolumeSeekBarPopup:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 2315
    .end local v0    # "audioUtil":Lcom/sec/android/app/videoplayer/util/VideoAudioUtil;
    .end local v1    # "vol":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->setVolumeBtnPopup()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$1400(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)V

    .line 2316
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->access$300(Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 2322
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1
    .param p1, "arg0"    # Landroid/widget/SeekBar;

    .prologue
    .line 2326
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController$32;->this$0:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->hideVolumeBarPopup()V

    .line 2327
    return-void
.end method
