.class Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;
.super Ljava/lang/Object;
.source "SubtitleSelectPopup.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SearchOnSingleThreadTask"
.end annotation


# instance fields
.field final mFile:Ljava/io/File;

.field final mFileFilter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;

.field final synthetic this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;Ljava/io/File;)V
    .locals 2
    .param p2, "f"    # Ljava/io/File;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 510
    new-instance v0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;-><init>(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->mFileFilter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;

    .line 513
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->mFile:Ljava/io/File;

    .line 514
    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->mFile:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->search(Ljava/io/File;)V

    .line 519
    return-void
.end method

.method public search(Ljava/io/File;)V
    .locals 6
    .param p1, "directory"    # Ljava/io/File;

    .prologue
    .line 522
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->mFileFilter:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$FileFilter;

    invoke-virtual {p1, v5}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 523
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 524
    array-length v5, v2

    if-nez v5, :cond_1

    .line 535
    :cond_0
    return-void

    .line 527
    :cond_1
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 528
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 529
    invoke-virtual {p0, v1}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->search(Ljava/io/File;)V

    .line 527
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 531
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch$SearchOnSingleThreadTask;->this$1:Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;

    # getter for: Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->mVectorList:Ljava/util/Vector;
    invoke-static {v5}, Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;->access$1200(Lcom/sec/android/app/videoplayer/subtitle/SubtitleSelectPopup$FileSearch;)Ljava/util/Vector;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method
