.class Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;
.super Ljava/lang/Object;
.source "VideoInvalidSubtitlePopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 38
    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VideoInvalidSubtitlePopup - Positive onClick()"

    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 40
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$1;->this$0:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;

    # getter for: Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->mOnDismissListener:Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;->access$100(Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup;)Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/app/videoplayer/popup/VideoInvalidSubtitlePopup$OnPopupDismissListener;->onDismiss()V

    .line 42
    :cond_0
    return-void
.end method
