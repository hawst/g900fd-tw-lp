.class Lcom/sec/android/app/videoplayer/view/MainVideoView$1;
.super Ljava/lang/Object;
.source "MainVideoView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/MainVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V
    .locals 0

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private on360Touch(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 647
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 665
    :cond_0
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 649
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 650
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v3, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I
    invoke-static {v3, v4}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    move-result v3

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    goto :goto_0

    .line 654
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 655
    .local v1, "y":I
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v2

    add-int/lit8 v3, v0, 0xa

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v2

    add-int/lit8 v3, v0, -0xa

    if-le v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v2

    add-int/lit8 v3, v1, 0xa

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v2

    add-int/lit8 v3, v1, -0xa

    if-le v2, v3, :cond_0

    .line 657
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    .line 658
    const/4 v2, 0x1

    goto :goto_1

    .line 647
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private onZoomTouch(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 616
    sget-boolean v7, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 617
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mServiceUtil:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->getPlayerState()I

    move-result v1

    .line 618
    .local v1, "playerState":I
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isPossibleZoom()Z

    move-result v7

    if-eqz v7, :cond_5

    if-eqz v1, :cond_0

    if-ne v1, v6, :cond_5

    .line 620
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mZoomEventHandler:Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/android/app/videoplayer/view/ZoomEventHandler;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 621
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 643
    .end local v1    # "playerState":I
    :cond_2
    :goto_0
    return v6

    .line 623
    .restart local v1    # "playerState":I
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    .line 624
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v8, v8

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 625
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-object v8, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v9, v9

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I
    invoke-static {v8, v9}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    move-result v8

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v7, v8}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    goto :goto_0

    .line 629
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v5, v7

    .local v5, "y":I
    const/16 v0, 0xf

    .line 630
    .local v0, "deltaPos":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v10

    sub-long v2, v8, v10

    .line 631
    .local v2, "timeDelta":J
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v7

    add-int v8, v4, v0

    if-ge v7, v8, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v7

    sub-int v8, v4, v0

    if-le v7, v8, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v7

    add-int v8, v5, v0

    if-ge v7, v8, :cond_3

    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v7

    sub-int v8, v5, v0

    if-gt v7, v8, :cond_4

    :cond_3
    const-wide/16 v8, 0xc8

    cmp-long v7, v2, v8

    if-gez v7, :cond_2

    .line 632
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto :goto_0

    .line 643
    .end local v0    # "deltaPos":I
    .end local v1    # "playerState":I
    .end local v2    # "timeDelta":J
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_5
    const/4 v6, 0x0

    goto :goto_0

    .line 621
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 441
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->getLockState()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 442
    const-string v11, "MainVideoView"

    const-string v12, "mTouchListener. LockState true"

    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 445
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoLockCtrl:Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/VideoLockCtrl;->showLockIcon()V

    .line 447
    :cond_0
    const/4 v11, 0x1

    .line 612
    :goto_0
    return v11

    .line 450
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 451
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->on360Touch(Landroid/view/MotionEvent;)Z

    .line 452
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceViewGL:Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/sec/android/app/videoplayer/view/VideoSurfaceGL;->touchEvent(Landroid/view/MotionEvent;)V

    .line 453
    const/4 v11, 0x1

    goto :goto_0

    .line 456
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 457
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mTitleController:Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/controller/VideoTitleController;->checkShowingAndDismissPopupVolBar()Z

    .line 460
    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 462
    .local v2, "action":I
    const/4 v11, 0x2

    if-ne v2, v11, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mChangeViewDone:Z
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 463
    const-string v11, "MainVideoView"

    const-string v12, "mChangeViewDone false"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    const/4 v11, 0x1

    goto :goto_0

    .line 467
    :cond_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoSurfaceView:Lcom/sec/android/app/videoplayer/view/VideoSurface;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoSurface;

    move-result-object v11

    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->onZoomTouch(Landroid/view/MotionEvent;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 468
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$702(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z

    .line 469
    const/4 v11, 0x1

    goto :goto_0

    .line 472
    :cond_5
    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v11

    if-nez v11, :cond_7

    .line 473
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$800(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    move-result-object v11

    if-eqz v11, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureDetector:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$800(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 474
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-boolean v11, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    if-nez v11, :cond_6

    .line 475
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 476
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 477
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 479
    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOnFlingFlag:Z

    .line 487
    :cond_7
    :goto_1
    sparse-switch v2, :sswitch_data_0

    .line 612
    :cond_8
    :goto_2
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 481
    :cond_9
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1000(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/view/ScaleGestureDetector;

    move-result-object v11

    if-eqz v11, :cond_7

    .line 482
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/high16 v12, 0x3f800000    # 1.0f

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleFactor:F
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1102(Lcom/sec/android/app/videoplayer/view/MainVideoView;F)F

    .line 483
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mScaleDetector:Landroid/view/ScaleGestureDetector;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1000(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/view/ScaleGestureDetector;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_1

    .line 490
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 492
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isPossibleZoom()Z

    move-result v11

    if-nez v11, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    if-eqz v11, :cond_8

    .line 493
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    const v12, 0x7f0a01cb

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->showToast(I)V

    goto :goto_2

    .line 498
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 499
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v12, v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 500
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v12, v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    goto :goto_2

    .line 505
    :cond_a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1400(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    int-to-float v11, v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f080089

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    const/high16 v13, 0x40000000    # 2.0f

    mul-float/2addr v12, v13

    cmpg-float v11, v11, v12

    if-ltz v11, :cond_8

    .line 509
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z

    move-result v11

    if-nez v11, :cond_11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-boolean v11, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    if-eqz v11, :cond_11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    move-result-object v11

    sget-object v12, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->MODE_UNDEFINED:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v11, v12, :cond_11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isVolumeCtrlShowing()Z

    move-result v11

    if-nez v11, :cond_11

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_11

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isDLNAmode()Z

    move-result v11

    if-nez v11, :cond_11

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->isEmergencymode(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_11

    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_CAPTURE:Z

    if-eqz v11, :cond_b

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1700(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    move-result-object v11

    if-eqz v11, :cond_b

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoCaptureView:Lcom/sec/android/app/videoplayer/view/VideoCaptureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1700(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoCaptureView;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/VideoCaptureView;->isShowing()Z

    move-result v11

    if-nez v11, :cond_11

    :cond_b
    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    if-eqz v11, :cond_c

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v11

    if-nez v11, :cond_11

    :cond_c
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v11

    const/4 v12, 0x2

    if-ge v11, v12, :cond_11

    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v11, :cond_d

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v11, v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    if-eqz v11, :cond_d

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    iget-object v11, v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->mSideChapterViewDialog:Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/videowall/SideChapterViewDialog;->isShowing()Z

    move-result v11

    if-nez v11, :cond_11

    .line 522
    :cond_d
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    float-to-int v9, v11

    .line 523
    .local v9, "ty":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    float-to-int v8, v11

    .line 524
    .local v8, "tx":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    sub-int/2addr v11, v9

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v7

    .line 525
    .local v7, "tmoveYVal":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    sub-int/2addr v11, v8

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 526
    .local v6, "tmoveXVal":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mGestureThreshold:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1800(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    if-lt v7, v11, :cond_f

    mul-int/lit8 v11, v6, 0x2

    if-le v7, v11, :cond_f

    .line 527
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 528
    .local v5, "screenWidth":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1200(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    div-int/lit8 v13, v5, 0x2

    if-le v11, v13, :cond_10

    sget-object v11, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    :goto_3
    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    invoke-static {v12, v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1602(Lcom/sec/android/app/videoplayer/view/MainVideoView;Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    .line 530
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    move-result-object v11

    sget-object v12, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v11, v12, :cond_e

    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/common/VUtils;->isCriticalLowBatteryStatus()Z

    move-result v11

    if-nez v11, :cond_8

    .line 533
    :cond_e
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->startShowGesture()V

    .line 534
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 535
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/4 v12, 0x4

    const-wide/16 v14, 0x32

    invoke-virtual {v11, v12, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 548
    .end local v5    # "screenWidth":I
    .end local v6    # "tmoveXVal":I
    .end local v7    # "tmoveYVal":I
    .end local v8    # "tx":I
    .end local v9    # "ty":I
    :cond_f
    :goto_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v12, v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 549
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v12, v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 552
    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->VIDEO_ZOOM:Z

    if-eqz v11, :cond_8

    invoke-static {}, Lcom/sec/android/app/videoplayer/view/PanRectView;->isZoomEnabled()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 553
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 554
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->hideController()V

    goto/16 :goto_2

    .line 528
    .restart local v5    # "screenWidth":I
    .restart local v6    # "tmoveXVal":I
    .restart local v7    # "tmoveYVal":I
    .restart local v8    # "tx":I
    .restart local v9    # "ty":I
    :cond_10
    sget-object v11, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    goto :goto_3

    .line 537
    .end local v5    # "screenWidth":I
    .end local v6    # "tmoveXVal":I
    .end local v7    # "tmoveYVal":I
    .end local v8    # "tx":I
    .end local v9    # "ty":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mIsVideoGestureStart:Z
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1500(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z

    move-result v11

    if-eqz v11, :cond_f

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    move-result-object v11

    if-eqz v11, :cond_f

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->isShowing()Z

    move-result v11

    if-eqz v11, :cond_f

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isControlsShowing()Z

    move-result v11

    if-nez v11, :cond_f

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isVolumeCtrlShowing()Z

    move-result v11

    if-nez v11, :cond_f

    .line 538
    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v11

    float-to-int v10, v11

    .line 539
    .local v10, "y":I
    const/high16 v11, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v12

    iget v12, v12, Landroid/util/DisplayMetrics;->density:F

    div-float v3, v11, v12

    .line 540
    .local v3, "adjValue":F
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1300(Lcom/sec/android/app/videoplayer/view/MainVideoView;)I

    move-result v11

    sub-int/2addr v11, v10

    int-to-float v11, v11

    mul-float/2addr v11, v3

    float-to-int v4, v11

    .line 542
    .local v4, "moveVal":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    move-result-object v11

    sget-object v12, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->VOLUME_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v11, v12, :cond_12

    .line 543
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    move-result-object v11

    invoke-virtual {v11, v4}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setVolume(I)V

    goto/16 :goto_4

    .line 544
    :cond_12
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureMode:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1600(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    move-result-object v11

    sget-object v12, Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;->BRIGHTNESS_MODE:Lcom/sec/android/app/videoplayer/view/MainVideoView$VideoGestureMode;

    if-ne v11, v12, :cond_f

    .line 545
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mVideoGestureView:Lcom/sec/android/app/videoplayer/view/VideoGestureView;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Lcom/sec/android/app/videoplayer/view/VideoGestureView;

    move-result-object v11

    invoke-virtual {v11, v4}, Lcom/sec/android/app/videoplayer/view/VideoGestureView;->setBrightness(I)V

    goto/16 :goto_4

    .line 559
    .end local v3    # "adjValue":F
    .end local v4    # "moveVal":I
    .end local v10    # "y":I
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 560
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x8

    const-wide/16 v14, 0x64

    invoke-virtual {v11, v12, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 561
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$702(Lcom/sec/android/app/videoplayer/view/MainVideoView;Z)Z

    .line 563
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getX()F

    move-result v12

    float-to-int v12, v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mXTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1202(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 564
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v13

    float-to-int v13, v13

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mDownYPos:I
    invoke-static {v12, v13}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1402(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    move-result v12

    # setter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mYTouchPos:I
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$1302(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)I

    .line 566
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->isMWTrayOpen(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 567
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 572
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 573
    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v11, :cond_13

    .line 574
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    .line 576
    :cond_13
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    move-result v11

    if-eqz v11, :cond_8

    .line 577
    const-string v11, "MainVideoView"

    const-string v12, "mTouchListener - VideoGesture is cancel"

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 582
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$900(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Landroid/os/Handler;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeMessages(I)V

    .line 583
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mLongtouchflag:Z

    .line 584
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # getter for: Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOpenPopupPlayer:Z
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$700(Lcom/sec/android/app/videoplayer/view/MainVideoView;)Z

    move-result v11

    if-eqz v11, :cond_14

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->isPossibleZoom()Z

    move-result v11

    if-eqz v11, :cond_14

    .line 585
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->isTalkBackOn(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 586
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    # invokes: Lcom/sec/android/app/videoplayer/view/MainVideoView;->createTalkBackOffDialog()V
    invoke-static {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2000(Lcom/sec/android/app/videoplayer/view/MainVideoView;)V

    .line 592
    :cond_14
    :goto_5
    sget-boolean v11, Lcom/sec/android/app/videoplayer/common/feature/Feature;->SIDE_MIRRORING:Z

    if-eqz v11, :cond_15

    .line 593
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/activity/MoviePlayer;->rotateScreen()V

    .line 595
    :cond_15
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->finishUpVideoGesture()Z

    move-result v11

    if-eqz v11, :cond_18

    .line 596
    const-string v11, "MainVideoView"

    const-string v12, "mTouchListener - VideoGesture is finishing up"

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    :cond_16
    :goto_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOnFlingFlag:Z

    goto/16 :goto_2

    .line 588
    :cond_17
    invoke-static {}, Lcom/sec/android/app/videoplayer/common/VUtils;->getInstance()Lcom/sec/android/app/videoplayer/common/VUtils;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/videoplayer/common/VUtils;->openPopupPlayer(Landroid/content/Context;)V

    goto :goto_5

    .line 597
    :cond_18
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual/range {p2 .. p2}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    float-to-int v12, v12

    # invokes: Lcom/sec/android/app/videoplayer/view/MainVideoView;->isNotificationAreaTouched(I)Z
    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->access$2100(Lcom/sec/android/app/videoplayer/view/MainVideoView;I)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 598
    const-string v11, "MainVideoView"

    const-string v12, "mTouchListener - Notification Area Touched"

    invoke-static {v11, v12}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 600
    :cond_19
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    iget-boolean v11, v11, Lcom/sec/android/app/videoplayer/view/MainVideoView;->mOnFlingFlag:Z

    if-nez v11, :cond_16

    .line 601
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/videoplayer/view/MainVideoView$1;->this$0:Lcom/sec/android/app/videoplayer/view/MainVideoView;

    invoke-virtual {v11}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->toggleControlsVisiblity()V

    goto :goto_6

    .line 487
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_4
        0x2 -> :sswitch_1
        0x3 -> :sswitch_3
        0x105 -> :sswitch_0
    .end sparse-switch
.end method
