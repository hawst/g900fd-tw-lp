.class public Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;
.super Ljava/lang/Object;
.source "VideoGestureDetectorWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;,
        Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$OnFinishedListener;
    }
.end annotation


# instance fields
.field private mDetector:Landroid/view/GestureDetector;

.field private mListener:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->mListener:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;

    .line 26
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->mListener:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->mDetector:Landroid/view/GestureDetector;

    .line 27
    return-void
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 30
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->mDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 31
    .local v0, "onTouchEvent":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 32
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper;->mListener:Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/videoplayer/view/VideoGestureDetectorWrapper$VideoOnGestureListener;->onFinished(Landroid/view/MotionEvent;)V

    .line 34
    :cond_0
    return v0
.end method
