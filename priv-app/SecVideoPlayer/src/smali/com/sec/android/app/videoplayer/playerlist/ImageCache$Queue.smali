.class Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;
.super Ljava/lang/Object;
.source "ImageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/playerlist/ImageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Queue"
.end annotation


# static fields
.field private static final QUEUE_SIZE:I = 0x1e

.field private static mQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mQueueInProcess:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    .line 129
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized QueueInProcessContain(Ljava/lang/String;)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 163
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 166
    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized get()Ljava/lang/String;
    .locals 3

    .prologue
    .line 139
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x0

    .line 143
    :goto_0
    monitor-exit v1

    return-object v0

    .line 142
    :cond_0
    :try_start_1
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    sget-object v2, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 143
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peekLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isEmpty()Z
    .locals 2

    .prologue
    .line 152
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized put(Ljava/lang/String;)V
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 132
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x1e

    if-le v0, v2, :cond_0

    .line 134
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    :cond_0
    monitor-exit v1

    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized removeQueueInProcess(Ljava/lang/String;)Z
    .locals 2
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 156
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueueInProcess:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :cond_0
    const/4 v0, 0x1

    monitor-exit v1

    return v0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized size()I
    .locals 2

    .prologue
    .line 148
    const-class v1, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/app/videoplayer/playerlist/ImageCache$Queue;->mQueue:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
