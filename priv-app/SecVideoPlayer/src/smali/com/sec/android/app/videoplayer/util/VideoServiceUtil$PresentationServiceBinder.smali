.class Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;
.super Ljava/lang/Object;
.source "VideoServiceUtil.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PresentationServiceBinder"
.end annotation


# instance fields
.field mCallback:Landroid/content/ServiceConnection;

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Landroid/content/ServiceConnection;)V
    .locals 0
    .param p2, "callback"    # Landroid/content/ServiceConnection;

    .prologue
    .line 2115
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2116
    iput-object p2, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    .line 2117
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 2120
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->this$0:Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;

    invoke-static {p2}, Lcom/sec/android/app/videoplayer/service/IPresentationService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    move-result-object v1

    # setter for: Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->sPresentationService:Lcom/sec/android/app/videoplayer/service/IPresentationService;
    invoke-static {v0, v1}, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;->access$902(Lcom/sec/android/app/videoplayer/util/VideoServiceUtil;Lcom/sec/android/app/videoplayer/service/IPresentationService;)Lcom/sec/android/app/videoplayer/service/IPresentationService;

    .line 2122
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 2123
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 2125
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 2128
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 2129
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/util/VideoServiceUtil$PresentationServiceBinder;->mCallback:Landroid/content/ServiceConnection;

    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V

    .line 2131
    :cond_0
    return-void
.end method
