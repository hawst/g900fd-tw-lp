.class Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;
.super Ljava/lang/Object;
.source "ControllerUtils.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 135
    const/4 v1, 0x0

    .line 137
    .local v1, "retVal":Z
    sparse-switch p2, :sswitch_data_0

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->commonKeyListener(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 164
    .local v0, "isReturn":Z
    if-eqz v0, :cond_2

    .line 182
    .end local v0    # "isReturn":Z
    :goto_0
    return v1

    .line 140
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 142
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$000(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    const/16 v3, 0xbb8

    invoke-virtual {v2, v3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->showController(I)V

    goto :goto_0

    .line 146
    :pswitch_1
    sget-boolean v2, Lcom/sec/android/app/videoplayer/common/feature/Feature;->ROTATE_SECOND_SCREEN:Z

    if-eqz v2, :cond_1

    .line 147
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$100(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/app/videoplayer/common/VUtils;->isFolderClose(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$200(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 150
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateSecondScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$300(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 153
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # invokes: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->toggleRotateScreen()V
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$200(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)V

    goto :goto_0

    .line 167
    .restart local v0    # "isReturn":Z
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 169
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$000(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 170
    goto :goto_0

    .line 173
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils$2;->this$0:Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;

    # getter for: Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->mMainVideoView:Lcom/sec/android/app/videoplayer/view/MainVideoView;
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;->access$000(Lcom/sec/android/app/videoplayer/view/controller/ControllerUtils;)Lcom/sec/android/app/videoplayer/view/MainVideoView;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Lcom/sec/android/app/videoplayer/view/MainVideoView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 174
    goto :goto_0

    .line 137
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 167
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
