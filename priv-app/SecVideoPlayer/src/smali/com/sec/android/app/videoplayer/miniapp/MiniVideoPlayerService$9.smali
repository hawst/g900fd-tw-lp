.class Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;
.super Ljava/lang/Object;
.source "MiniVideoPlayerService.java"

# interfaces
.implements Lcom/sec/android/app/videoplayer/miniapp/MiniVideoBtnController$OnCommandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V
    .locals 0

    .prologue
    .line 1535
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public performCommand(I)V
    .locals 5
    .param p1, "cmd"    # I

    .prologue
    const/4 v4, 0x0

    .line 1539
    packed-switch p1, :pswitch_data_0

    .line 1576
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1541
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->doPauseResume()V
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$500(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)V

    goto :goto_0

    .line 1546
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isShortSeekDisalbedCase()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1547
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1550
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1557
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # invokes: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->isShortSeekDisalbedCase()Z
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3000(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1558
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->getCurrentPosition()I

    move-result v0

    .line 1559
    .local v0, "cpos":I
    const-string v1, "MiniVideoPlayerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mOnCommandListener CMD_PREV. cpos = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/app/videoplayer/common/wrapper/LogS;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1560
    const/16 v1, 0xbb8

    if-le v0, v1, :cond_1

    .line 1561
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->setSeekPosition(I)V

    .line 1562
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mMiniPlayer:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$800(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoSurface;->openVideo()V

    goto :goto_0

    .line 1564
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->setVisibilityLoadingDialog(I)V

    .line 1566
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1567
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService$9;->this$0:Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;

    # getter for: Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;->access$3100(Lcom/sec/android/app/videoplayer/miniapp/MiniVideoPlayerService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1539
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
