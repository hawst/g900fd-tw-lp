.class public Lcom/sec/android/app/videoplayer/cmd/MenuCmdFactory;
.super Ljava/lang/Object;
.source "MenuCmdFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create()Landroid/util/SparseArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 13
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 15
    .local v0, "menuCmdlist":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/android/app/videoplayer/cmd/IMenuCmd;>;"
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/cmd/NxpEditCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 16
    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/cmd/VideoMakerCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 17
    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/SStudioCmd;

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/cmd/SStudioCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 18
    const/4 v1, 0x3

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/cmd/SAppsCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 19
    const/4 v1, 0x4

    new-instance v2, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;

    invoke-direct {v2}, Lcom/sec/android/app/videoplayer/cmd/ShareVideoCmd;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 21
    return-object v0
.end method
